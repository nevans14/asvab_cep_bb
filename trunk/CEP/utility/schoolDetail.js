const occupationsUrl = "http://legacy-dev.asvabprogram.com/CEP/rest/occufind/getOccufindSearch"
const schoolDetailUrl = "http://legacy-dev.asvabprogram.com/CEP/rest/occufind/SchoolMoreDetails/"
const educationLevelUrl = "http://legacy-dev.asvabprogram.com/CEP/rest/careerCluster/occupationEducationLevel/"

// const occupationsUrl = "http://localhost:8080/CEP/rest/occufind/getOccufindSearch"
// const schoolDetailUrl = "http://localhost:8080/CEP/rest/occufind/SchoolMoreDetails/"
// const educationLevelUrl = "http://localhost:8080/CEP/rest/careerCluster/occupationEducationLevel/"

const axios = require('axios')

let occupations = [];
let occupationsWithoutSchool = [];
let occupationsWithoutEducationLevel = [];

const fs = require('fs')
if (fs.existsSync("occupationsWithoutSchool.txt")) {
  fs.unlinkSync("occupationsWithoutSchool.txt")
}
if (fs.existsSync("occupationsWithoutEducationLevel.txt")) {
  fs.unlinkSync("occupationsWithoutEducationLevel.txt")
}

let params = {"brightOccupationFlag":false,"greenOccupationFlag":false,"stemOccupationFlag":false,"hotOccupationFlag":false};

axios.post(occupationsUrl, params).then(res => {
  occupations = res.data.map(data => data.onetSoc + "\t" + data.occupationTitle);

  let schoolDetailPromises = []
  for (i = 0; i < occupations.length; ++i) {
    schoolDetailPromises.push(axios.get(schoolDetailUrl + occupations[i].split('\t')[0] + '/'))
  }
  Promise.all(schoolDetailPromises).then(res => {
    for (j = 0; j < res.length; ++j) {
      if (res[j].data.length === 0) {
        occupationsWithoutSchool.push(occupations[j]);
      }
    }
  }).then(res => {
    occupationsWithoutSchool = occupationsWithoutSchool.join('\n')
    fs.appendFileSync("occupationsWithoutSchool.txt", occupationsWithoutSchool, "UTF-8", { 'flags': 'a' });

  }).catch(err => {
    console.log("promise all error " + err.message)
  })

  let educationLevelPromises = []
  for (i = 0; i < occupations.length; ++i) {
    educationLevelPromises.push(axios.get(educationLevelUrl + occupations[i].split('\t')[0] + '/'))
  }
  Promise.all(educationLevelPromises).then(res => {
    for (j = 0; j < res.length; ++j) {
      if (res[j].data.length === 0) {
        occupationsWithoutEducationLevel.push(occupations[j]);
      }
    }
  }).then(res => {
    occupationsWithoutEducationLevel = occupationsWithoutEducationLevel.join('\n')
    fs.appendFileSync("occupationsWithoutEducationLevel.txt", occupationsWithoutEducationLevel, "UTF-8", { 'flags': 'a' });

  }).catch(err => {
    console.log("promise all error " + err.message)
  })
}).catch(err => {
  console.log("occupations error " + err.message)
})
