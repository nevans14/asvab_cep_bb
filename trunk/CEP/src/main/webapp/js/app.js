$(document).foundation();

$(document).ready(function () {
	redirectToHttps();
	$('.modal .close').click(function(){
		$(this).closest('.modal').hide();
	});

	$('.columns.score').click(function(){
		$('.columns.score').removeClass('selected');
		$(this).addClass('selected');
	});
	
});

/**
 * Prevents CloudFlare's redirect to https
 * @returns
 */
function redirectToHttps() {
	// localhost, no need to redirect
	if (location.href.indexOf('localhost') > -1) return;
	// legacy, www, redirect to https
	if ((location.href.indexOf('legacy') > -1 || location.href.indexOf('www.asvabprogram.com') > -1) &&
		 location.protocol === 'http:') {
		location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
	}
	// prod with no https, redirect to www and https
	if (location.href.indexOf('legacy') === -1 &&
		location.href.indexOf('www.asvabprogram.com') === -1 &&
		location.protocol === 'http:') {
		location.href = 'https://www.' + window.location.href.substring(window.location.protocol.length + 2);
	}
}

/**
 * Prevents returning an empty search results page 
 **/
$(document).on('submit', 'ul.menu form#search', function (e) {
	if ($(this).find('input[name=q]').val() == '') {
		e.preventDefault();
		alert("Please enter a keyword to search by.");
	}
});

/**
 * Look for an id named scrollToItem via jQuery.
 * 
 * Wait 350 milliseconds for frame work to collapse / expand 
 * the menus.
 * 
 * Then scroll to item in page.
 * 
 */
function scrollToItem( scrollLocation){

	
	var elm = $("#" + scrollLocation);
		
	
	  timeOut = setTimeout(function() {
          $("body").animate({scrollTop: elm.offset().top}, "slow");
	  }, 500);
};

/**
 *  Generate a unique identifier.
 *  Note: This is a random number that may produce the same id multiple times.
 *  Even though this is extremely unlikely this algorithm is not a guaranteed unique identify 
 *  across all computers and across all time. Since are keys are transient, are only valid
 *  for one hour, and limited number, only hundreds of users at a time, the utilization of this
 *  method is sufficient.
 *  
 */
function guid() {
	function s4() {
	return Math.floor((1 + Math.random()) * 0x10000)
		.toString(16)
		.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	s4() + '-' + s4() + s4() + s4();
};

$(document).ready(function () {
	$(document).on('click', '.interest-nav-tabs .interest-lists li a, .interest_triangle li a', function (e) {
		$('.interest-nav-tabs .interest-lists li, .interest_triangle li').removeClass('active');
		var tagid = $(this).data('toggle');
		$('.interest-lists li a[data-toggle=' + tagid + '], .interest_triangle li a[data-toggle=' + tagid + ']')
			.parent().addClass('active');
		$('.tab-pane').removeClass('active');
		$('#' + tagid).addClass('active');
	});
});

$(document).ready(function () {
	$(document).on('click', '.cep_tabs a', function (e) {
		var $_cep_tabs = $(this) .closest('.cep_tabs')
		$_cep_tabs.find('.nav-tabs li').removeClass('active');
		var tagid = $(this).data('toggle');
		$_cep_tabs.find('.nav-tabs a[data-toggle=' + tagid + ']').parent().addClass('active');
		$_cep_tabs.find('.tab-pane').removeClass('active');
		$_cep_tabs.find('#' + tagid).addClass('active');
	});
});

$(window).scroll(function () {
	var HeaderHeight = $('#header').height(),
		HomePageSlides = $('#home #slides').height();
	// if ( $('.dropdown-menu').is(':visible') )
	// 	return;
	
	if($('.swlFlyout').length > 0) {
		return
	}

	if($(window).outerWidth() > 767) {
		if ($(window).scrollTop() >= 10) {
			$('#header').addClass('scrolled');
			HeaderHeight = $('#header').height();
			$('#header').parent().parent().css('padding-top',HeaderHeight);
			$('#banner').addClass('banner_sticky').css('top',HeaderHeight);
			var BannerHeight = $('#banner').height();
			$('#main').css('padding-top',BannerHeight );
		} else {
			$('#header').removeClass('scrolled');
			$('#header').parent().parent().css('padding-top','');
			$('#banner').removeClass('banner_sticky').css('top','');
			$('#main').css('padding-top','');
			if($('.scroll-to-fixed-fixed').length > 0){
				$('.top-blue-panel, #sidebar.not-logged-in, #sidebar.logged-in').removeClass('scroll-to-fixed-fixed').removeAttr('style');
				$('.top-blue-panel+div, #sidebar.not-logged-in+div, #sidebar.logged-in+div').hide();
			}
		}
	}

	if($(window).scrollTop() === 0) {
		if ( $('#home').length > 0 ) {
			$('#header:not(.login) .dropdown-menu').show();
		} else {
			$('#header:not(.login) .dropdown-menu').hide();
        }
	} else {
        $('#header:not(.login) .dropdown-menu').hide();
	}
    
	if ($(window).scrollTop() >= HomePageSlides) {
		$('#mega-menu-buttons').addClass('mega-menu-sticky').css('top',HeaderHeight);
		if($('#mega-menu-buttons').length > 0 && $('#mega-menu-buttons+.mega-menu-buttons-spacing').length==0){
			$('#mega-menu-buttons').after('<div class="mega-menu-buttons-spacing"></div>');
			$('#mega-menu-buttons+.mega-menu-buttons-spacing').css('height', $('#mega-menu-buttons').outerHeight() );
		}
	} else {
		$('#mega-menu-buttons').removeClass('mega-menu-sticky').css('top','');
		$('#mega-menu-buttons+.mega-menu-buttons-spacing').remove();
	}
});

$(window).on("resize", function () {
	$(window).trigger('scroll');
});

$(document).ready(function () {
	$(document).on('click', '.title-bar .menu-icon', function (e) {
		$(window).trigger('scroll');
	});
});

var touchstart = new Date();
$(document).ready(function () {
	$(document).on('touchstart', 'body[data-whatinput="touch"] header nav .menu li.dropdown > a' , function (e) {
		touchstart = new Date();
	});
	$(document).on('click touchend', 'body[data-whatinput="touch"] header nav .menu li.dropdown > a' , function (e) {
		if(e.event == 'click' || touchstart.getTime()>(new Date()).getTime()-300) {
			e.preventDefault();
			if ($(this).closest('li').hasClass('dropdown-active')) {
				$(this).closest('li').removeClass('dropdown-active');
			} else {
				$('header nav .menu li.dropdown > a').closest('li').removeClass('dropdown-active');
				$(this).closest('li').addClass('dropdown-active');
			}
		}
	});
});

// For sidebar sticky issue fixes
var lastScrollTop = 0;
window.addEventListener("scroll", function(){
   var st = window.pageYOffset || document.documentElement.scrollTop;
   if (st > lastScrollTop){
      // downscroll code
        $('#sidebar + div:empty').show();
   } else {
      // upscroll code
        $('#sidebar + div:empty').hide();
   }
   lastScrollTop = st <= 0 ? 0 : st;
}, false); 