cepApp.controller('PasswordRestController',	function($route, $rootScope, $scope, $location, $http, $q, AuthenticationService) {
    // Controller Init function .....
    const BASE_URL = "/CEP/";   
	$scope.restCredentials = {};
	$scope.restCredentials.resetId = '';
	$scope.restCredentials.username = '';
	$scope.registrationLabel = '';

	function getParameterByName(name) {
		var absoluteUrl = $location.absUrl();
		absoluteUrl = absoluteUrl = absoluteUrl.replace("%20","+")
			.replace("%3D","=");
		absoluteUrl = absoluteUrl.substr(absoluteUrl.indexOf('?'));
		absoluteUrl = absoluteUrl.replace('?', ',')
			.replace('&', ',');
		var parts = absoluteUrl.split(',');
		for (var i = 0; i < parts.length; i++) {
			var subParts = parts[i].split('=');
			if (subParts[0] === name) {
				return decodeURIComponent(subParts[1]);
			}
		}
	}

	$scope.restCredentials.resetId = getParameterByName('resetId');
	$scope.restCredentials.username = getParameterByName('username');


	// var questionMarkPosition = absoluteUrl.indexOf("?");
	// var urlParametersString = absoluteUrl.substr(questionMarkPosition + 1 );
	// var urlParameters = urlParametersString.split("+");
	// var resetParameters = {};
	// for ( i = 0 ; i < urlParameters.length; i++ ) {
	// 	var nameValue = urlParameters[i].split("=");
	// 	resetParameters[nameValue[0]] = decodeURIComponent(nameValue[1]);
	// 	$scope.restCredentials[nameValue[0]] = decodeURIComponent(nameValue[1]);
	// }

	$scope.ResetPassword = function() {
		// Register Modal
		var data = {
			email 		: $scope.restCredentials.username,
			accessCode 	: "",
			password 	: "",
			resetKey 	: $scope.restCredentials.resetId
		};

		var pass1 = $scope.restCredentials.Password1;
		var pass2 = $scope.restCredentials.Password2;

		if (pass1.length <= 7) {
			$scope.registrationLabel = "Password too short!";
			return;
		}
		if (pass1 !== pass2) {
			$scope.registrationLabel = "Passwords don't match!";
			$scope.restCredentials.Password1 = '';
			$scope.restCredentials.Password2 = '';
			return;
		}

		var url = BASE_URL + 'rest/applicationAccess/passwordReset';

		data.password = $scope.restCredentials.Password2;
		$http.post(url, data).then(function(response) {
			$scope.registrationLoginStatus = response.data;
			// Success ..
			if ($scope.registrationLoginStatus.statusNumber === 0) {
				// Set the location to dashboard
				$location.path('/');
				$location.replace();
				$location.url($location.path());
			} else {
				// display error message
				$scope.registrationLabel = $scope.registrationLoginStatus.status
			}
		});
	};
});