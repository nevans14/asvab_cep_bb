cepApp.controller('WorkValuesTestResultsController', [ '$scope', '$window', '$location', function($scope, $window, $location) {

	$scope.workValueTopResults = angular.fromJson($window.sessionStorage.getItem('workValueTopResults'));
	
	$scope.retakeWorkValues = function(){
		$location.path('/work-values-test');
	}

	$scope.seoTitle = 'Work Values Quiz | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Take the work values quiz to help you determine the aspects of work that are important to you. Then, explore careers based on your work values.';
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
