cepApp.controller('WorkValuesIntroController', [ '$scope',function($scope) {

	$scope.seoTitle = 'What are Work Values | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Discover what are work values with this situational judgement activity. 16 scenarios help you determine work values that are important to you.';

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
