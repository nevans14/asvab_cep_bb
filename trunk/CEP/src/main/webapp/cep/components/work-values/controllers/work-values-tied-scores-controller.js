/**
 * $scope.tiedScores stores the tied set(s). It's used to display the tied options. For
 * each selection, the selection get's added to $scope.tieScoresQueue. Once the
 * final selection is made, we iterate through the original
 * $scope.workValueTiedScores and overwrite the array position with the
 * selections stored in $scope.tieScoresQueue in the ordered fashion (FIFO).
 * 
 * Modified 12/10/2020 to meet new requirement of only breaking tie breaks for top three values.
 */
cepApp.controller('WorkValuesTiedScoresController', [ '$scope', '$window', '$location', 'WorkValuesAPI', '$q', function($scope, $window, $location, WorkValuesAPI, $q) {

	$scope.workValueTiedResults = angular.fromJson($window.sessionStorage.getItem('workValueTiedResults'));
	$scope.tiedScores = [];
	$scope.tieSelectionIndex;
	$scope.availableSlots = 3
	$scope.topThreeWorkValues = [];
	$scope.tieScoresQueue = [];

	$scope.seoTitle = 'Work Values Quiz | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Take the work values quiz to help you determine the aspects of work that are important to you. Then, explore careers based on your work values.';

	// only keep TypeId and Score keys
	const filterProperties = function(obj) {
		return {
			TypeId: obj.TypeId,
			Score: obj.Score,
		}
	}

	var testObject = {
			TestId: undefined, 
			Responses: undefined, 
			Scores: undefined, 
			TopThree: {WorkValueOne: undefined, 
				WorkValueTwo: undefined, 
				WorkValueThree: undefined}}

	
	/**
	 * Setup $scope.tiedScores to use to display tied selections.
	 */
	$scope.setupTiedScores = function() {

		var isNextScoreTied = false;
		var tiedScoreSet = [];
		for (var i = 0; i < $scope.workValueTiedResults.length; i++) {
			
			if ((i != $scope.workValueTiedResults.length - 1) && $scope.workValueTiedResults[i].Score == $scope.workValueTiedResults[i + 1].Score) {
				tiedScoreSet.push($scope.workValueTiedResults[i]);
				$scope.workValueTiedResults[i].tied = true;
				isNextScoreTied = true;
			}else if (isNextScoreTied) {
				isNextScoreTied = false
				tiedScoreSet.push($scope.workValueTiedResults[i]);
				$scope.workValueTiedResults[i].tied = true;
				$scope.tiedScores.push(tiedScoreSet);
				tiedScoreSet = [];
			} else {
				$scope.workValueTiedResults[i].tied = false;
			}
			
		}
		
		for (var i = 0; i < $scope.workValueTiedResults.length; i++) {
			if(!$scope.workValueTiedResults[i].tied){
				$scope.availableSlots--;
			} else {
				break;
			}
		}
	}
	
	
	$scope.selectTieScore = function() {
		$scope.postError = false;
		
		if($scope.tiedScores[0].length == 2) {
				$scope.tieScoresQueue.push($scope.tiedScores[0].splice([$scope.tieSelectionIndex], 1)[0]);
				$scope.tieScoresQueue.push($scope.tiedScores[0].splice(0, 1)[0]); //push last in list
				$scope.tiedScores.splice(0, 1);
				$scope.availableSlots = $scope.availableSlots - 2;
		}else{
			$scope.tieScoresQueue.push($scope.tiedScores[0].splice([$scope.tieSelectionIndex], 1)[0]);
			$scope.availableSlots--;
		}
		
		
		// last tie break slot
		if($scope.availableSlots<1 || ($scope.availableSlots == 1 && !$scope.workValueTiedResults[2].tied)) {
			$scope.$root.isRouteLoading = true;
			$scope.tiedSelectionCompleted = true;
			
			
			var tiedScoresSelected = {
						FirstTypeSelected: undefined,
						SecondTypeSelected: undefined,
						ThirdTypeSelected: undefined,
			}
			
			// sync tie selections into results
			for (var i = 0; i < $scope.workValueTiedResults.length - 3; i++) {
				if($scope.workValueTiedResults[i].tied) {
					$scope.workValueTiedResults[i] = $scope.tieScoresQueue.shift();
					
					// setup user tied selected
					if(tiedScoresSelected.FirstTypeSelected == undefined) {
						tiedScoresSelected.FirstTypeSelected = $scope.workValueTiedResults[i].TypeId;
					} else if(tiedScoresSelected.SecondTypeSelected == undefined) {
						tiedScoresSelected.SecondTypeSelected = $scope.workValueTiedResults[i].TypeId;
					} else if(tiedScoresSelected.ThirdTypeSelected == undefined) {
						tiedScoresSelected.ThirdTypeSelected = $scope.workValueTiedResults[i].TypeId;
					}
				}
			}
			
			var index = 0;
			while (index < $scope.workValueTiedResults.length) { 
				$scope.workValueTiedResults[index] = filterProperties($scope.workValueTiedResults[index]);
			    index++; 
			}
			$window.sessionStorage.setItem('workValueTopResults', JSON.stringify($scope.workValueTiedResults.slice(0,3)));
			
			
			var index = 0;
			var workValueTiedScoresUnsorted = angular.fromJson($window.sessionStorage.getItem('workValueTiedResults'));
			while (index < workValueTiedScoresUnsorted.length) { 
				workValueTiedScoresUnsorted[index] = filterProperties(workValueTiedScoresUnsorted[index]);
			    index++; 
			}
			
			// setup testObject used for post request body			
			testObject.Scores = workValueTiedScoresUnsorted;
			testObject.TopThree = {WorkValueOne: $scope.workValueTiedResults[0].TypeId, WorkValueTwo: $scope.workValueTiedResults[1].TypeId, WorkValueThree: $scope.workValueTiedResults[2].TypeId}
			testObject.Responses = $window.sessionStorage.getItem('workValueTestResponses');
			testObject.TiedScores = tiedScoresSelected;
			
			$scope.postWorkValues();
			
		}
		
		$scope.tieSelectionIndex = undefined;
	}
	
	/*
	 * Post work value testObject.
	 */
	$scope.postWorkValues = function(){
		
		$scope.isDisabled = true
		WorkValuesAPI.getTestId().then(function(result) {
		    testObject.TestId = result.data;
		    return WorkValuesAPI.postResults(testObject);
		  }, function(e){
			  console.log(e);
			  $scope.$root.isRouteLoading = false;
			  $scope.isDisabled = false;
			  $scope.postError = true;
		  }).then(function(result) {
			  $window.sessionStorage.removeItem('workValueTestResponses');
			  $window.sessionStorage.removeItem('workValueTiedResults');
		    $location.path('/work-values-test-results');
		  }, function(e){
			  console.log(e);
			  $scope.$root.isRouteLoading = false;
			  $scope.isDisabled = false;
			  $scope.postError = true;
		  });
	}
	
	$scope.setupTiedScores();

	$scope.tiedNumText = function() {
		var number = $scope.tiedScores[0].length > 1 
			? $scope.tiedScores[0].length 
			: $scope.tiedScores[1].length;
		switch(number) {
			case 2:
				return 'two';
			case 3:
				return 'three';
			case 4:
				return 'four';
			default:
				return '';
		}
	}
	
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);