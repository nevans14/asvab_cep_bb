cepApp.controller('WorkValuesTestController', [ '$scope', '$location', '$window', '$q', '$route', '$uibModalStack', 'testData', 'modalService', 'WorkValuesAPI', 'workValuesRetakeModal',function($scope, $location, $window, $q, $route, $uibModalStack, testData, modalService, WorkValuesAPI, workValuesRetakeModal) {

	var isTestInProgress = $window.sessionStorage.getItem('workValueUserSelections') ? true : false;
	$scope.testData =  isTestInProgress ? angular.fromJson($window.sessionStorage.getItem('workValueUserSelections')) : testData.data;
	$scope.progressPercentage = 0;
	$scope.activeQuestion = isTestInProgress ? $window.sessionStorage.getItem('workValueActiveQuestion') : 0;
	$scope.lastQuestion = $scope.testData.length - 1;
	$scope.workValueSortedScores = [];
	
	$uibModalStack.dismissAll();

	$scope.seoTitle = 'Work Values Quiz | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Take the work values quiz to help you determine the aspects of work that are important to you. Then, explore careers based on your work values.';
	
	/*
	 * Type ID definitions
	 * 1: Achievement 
	 * 2: Independence 
	 * 3: Recognition 
	 * 4: Relationships 
	 * 5: Support 
	 * 6: Working Conditions
	 */
	$scope.rawScores = {
			"1": 0,
			"2": 0,
			"3": 0,
			"4": 0,
			"5": 0,
			"6": 0
	}
	
	/**
	 * Setup next question.
	 */
	$scope.nextQuestion = function () {
		
		if(!$scope.testData[$scope.activeQuestion].answer) {
			var customModalOptions = {
					headerText : 'Work Values',
					bodyText : '<p>Make a selection.</p>'
				};
				var customModalDefaults = {size : 'sm'};
				modalService.show(customModalDefaults, customModalOptions);
			return;
		}

		if($scope.activeQuestion < $scope.lastQuestion) {
			$scope.activeQuestion++;
			updateProgressBar();
			
			$window.sessionStorage.setItem('workValueUserSelections', angular.toJson($scope.testData));
			$window.sessionStorage.setItem('workValueActiveQuestion', $scope.activeQuestion);
			
		} else {
			$scope.submit(); // last question submit answers
		}
	}
	
	/**
	 * Setup previous question.
	 */
	$scope.previousQuestion = function () {

		if($scope.activeQuestion > 0) {
			$scope.activeQuestion--;
			
			updateProgressBar();
			$window.sessionStorage.setItem('workValueUserSelections', angular.toJson($scope.testData));
			$window.sessionStorage.setItem('workValueActiveQuestion', $scope.activeQuestion);
		}
	}
	
	updateProgressBar = function () {
		$scope.progressPercentage = $scope.activeQuestion / $scope.lastQuestion * 100;
	}
	updateProgressBar();
	
	// only keep TypeId and Score keys
	const filterProperties = function(obj) {
		return {
			TypeId: obj.TypeId, 
			Score: obj.Score,
		}
	} 

	var testObject = {
			TestId: undefined, 
			Responses: undefined, 
			Scores: undefined, 
			TopThree: {WorkValueOne: undefined, 
				WorkValueTwo: undefined, 
				WorkValueThree: undefined}}
	
	$scope.submit = function () {
		
		$scope.isDisabled = true;
		
		// reset values
		$scope.rawScores = {
				"1": 0,
				"2": 0,
				"3": 0,
				"4": 0,
				"5": 0,
				"6": 0
		}
		$scope.workValueSortedScores = []
		
		// tally scores
		for(var i = 0; i < $scope.testData.length; i++){
			var typeId = $scope.testData[i].answer.typeId;
			$scope.rawScores[typeId]++;
		}
		
		for (var typeId in $scope.rawScores) {
		    $scope.workValueSortedScores.push({TypeId: typeId, Score: $scope.rawScores[typeId]});
		}
		
		//For testing purpose only
		/*$scope.workValueSortedScores = [{TypeId: "1", Score: 5},
			{TypeId: "2", Score: 5},
			{TypeId: "3", Score: 5},
			{TypeId: "4", Score: 5},
			{TypeId: "5", Score: 5},
			{TypeId: "6", Score: 1}];*/

		// create score list and sort scores from highest to lowest
		$scope.workValueSortedScores.sort(function(a, b) {
		    return b.Score - a.Score;
		});
		
		setupTiedScores($scope.workValueSortedScores);
		var isTiedScore = $scope.isTiedScore();
		
		// setup test response string
		var testResponses = '';
		for(var i = 0; i < $scope.testData.length; i++){
			var typeId = $scope.testData[i].answer.typeId;
			var optionId = $scope.testData[i].answer.optionId;
			$scope.rawScores[typeId]++;
			testResponses = testResponses + optionId + '|';
		}
		
		
		if(isTiedScore){
			
			$window.sessionStorage.setItem('workValueTestResponses', testResponses);
			
			$window.sessionStorage.removeItem('workValueUserSelections');
			 $window.sessionStorage.removeItem('workValueActiveQuestion');
			
			// Show retake test alert if there are more that 4 way ties and if not redirect user to tie break page.
			if(!maxAllowedTiesAlert()){
				$window.sessionStorage.setItem('workValueTiedResults', JSON.stringify($scope.workValueSortedScores));
				$location.path('/work-values-tied-scores');
			}
			
			
		} else {
			
			var index = 0;
			
			// keep only TypeId and Scores properties
			while (index < $scope.workValueSortedScores.length) { 
				$scope.workValueSortedScores[index] = filterProperties($scope.workValueSortedScores[index]);
			    index++; 
			}
			
			// setup testObject used for post request body
			testObject.Scores = $scope.workValueSortedScores
			testObject.TopThree = {WorkValueOne: $scope.workValueSortedScores[0].TypeId, WorkValueTwo: $scope.workValueSortedScores[1].TypeId, WorkValueThree: $scope.workValueSortedScores[2].TypeId}
			testObject.Responses = testResponses;
			
			
			$scope.postWorkValues();
		}
	}
	
	$scope.postWorkValues = function(){
		$scope.isDisabled = true;
		WorkValuesAPI.getTestId().then(function(result) {
		    testObject.TestId = result.data;
		    return WorkValuesAPI.postResults(testObject);
		  }, function(e){
			  console.log(e);
			  $scope.isDisabled = false;
		  }).then(function(result) {
			  $window.sessionStorage.setItem('workValueTopResults', JSON.stringify($scope.workValueSortedScores.slice(0,3)));
			  $window.sessionStorage.removeItem('workValueUserSelections');
			  $window.sessionStorage.removeItem('workValueActiveQuestion');
			  $location.path('/work-values-test-results');
		  }, function(e){
			  $scope.isDisabled = false;
			  var customModalOptions = {
						headerText : 'Error',
						bodyText : 'An error has occured, please try again.'
					};
					var customModalDefaults = {size : 'sm'};
					modalService.show(customModalDefaults, customModalOptions);
		  });
	}
	

	$scope.isTiedScore = function() {
		index = 0;
		while(index < 3){
			if($scope.workValueSortedScores[index].tied == true) {
				return true
			}
			index++;
		}
		return false;
		
	}
	
	
	var tiedScores = [];
	
	/**
	 * Setup $scope.tiedScores to use to display tied selections.
	 */
	setupTiedScores = function(workValueTiedResults) {

		var isNextScoreTied = false;
		var tiedScoreSet = [];
		for (var i = 0; i < workValueTiedResults.length; i++) {
			
			if ((i != workValueTiedResults.length - 1) && workValueTiedResults[i].Score == workValueTiedResults[i + 1].Score) {

				tiedScoreSet.push(workValueTiedResults[i]);
				workValueTiedResults[i].tied = true;
				isNextScoreTied = true;
				
			}else if (isNextScoreTied) {
				isNextScoreTied = false
				tiedScoreSet.push(workValueTiedResults[i]);
				workValueTiedResults[i].tied = true;
				tiedScores.push(tiedScoreSet);
				tiedScoreSet = [];
			} else {
				workValueTiedResults[i].tied = false;
			}
			
		}
	}
	
	/**
	 * If there are 5 or more ties for one particular score, alert user to give
	 * them option to retake test or proceed with selecting from their tied
	 * scores.
	 */
	maxAllowedTiesAlert = function() {
		for (var i = 0; i < tiedScores.length; i++) {
			if(tiedScores[i].length >= 5){
				var customModalOptions = {
						headerText : '',
						bodyText : ''
					};
					var customModalDefaults = {size : 'md', backdrop: 'static'};
					workValuesRetakeModal.show(customModalDefaults, customModalOptions);
					
					return true;
			}
		}
		return false;
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
