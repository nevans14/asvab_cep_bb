cepApp.service('workValuesRetakeModal', [ '$uibModal', '$sce', '$window', function($uibModal, $sce, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/work-values/page-partials/work-values-retake-modal.html',
		windowClass : 'asvab-modal',
		size : 'md'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyImage != undefined) {
				customModalOptions.bodyImage = $sce.trustAsHtml(customModalOptions.bodyImage);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$window', function($scope, $uibModalInstance, $window) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.retakeTest = function() {
					$uibModalInstance.close();
					$window.location.reload();
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

			}];
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);