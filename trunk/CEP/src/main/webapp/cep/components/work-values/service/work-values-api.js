cepApp.factory('WorkValuesAPI', [ '$http', 'appUrl', 'awsApiFullUrl', '$q', 'AuthenticationService', function($http, appUrl, awsApiFullUrl, $q, AuthenticationService) {

	var workValuesAPI = {};

	workValuesAPI.getTestInformation = function() {
		//return $http.get(appUrl + 'fyi/test');

		var xsrfCookie = AuthenticationService.getAwsXsrfCookie();

		if (!xsrfCookie) { 
			console.log("Missing AWS XSRF cookie!")
		}
		
		return $http({
			method: 'POST',
			url: awsApiFullUrl + '/api/WorkValues/get-items',
			headers: {
				'X-XSRF-TOKEN': xsrfCookie
			},
		});
	};
	
	workValuesAPI.postResults = function(results){
		return $http.post(awsApiFullUrl + "/api/WorkValues/save-test", results);
	};
	
	workValuesAPI.getTestId = function(){
		return $http.post(appUrl + "work-values/save-test");
	};
	
	workValuesAPI.getWorkValues = function(){
		return $http.get(appUrl + "work-values/get-profile");
	};

	
	return workValuesAPI;

} ]);