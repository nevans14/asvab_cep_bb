cepApp.factory('FaqRestFactory', [ '$http', '$q', 'appUrl', 'awsApiFullUrl', function($http, $q, appUrl, awsApiFullUrl) {

	var faqRestFactory = {};

	faqRestFactory.emailUs = function (emailObject) {
		console.log(emailObject);
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: awsApiFullUrl + '/api/contactus/email/', 
			data: emailObject,
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			dataType: 'json'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response)
		});
		console.log(emailObject);
		return deferred.promise;
	}

	faqRestFactory.scheduleEmail = function(emailObject) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: awsApiFullUrl + '/api/contactus/scheduleEmail/', 
			data: emailObject,
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			dataType: 'json'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response)
		});
		return deferred.promise;
	}

	faqRestFactory.shareWithCounselor = function(emailObject) {
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: awsApiFullUrl + '/api/contactus/shareWithCounselor/', 
			data: emailObject,
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			dataType: 'json'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response)
		});
		return deferred.promise;
	}

	faqRestFactory.scoreRequest = function(emailObject) {
		return $http({
			method: 'POST',
			url: awsApiFullUrl + '/api/ContactUs/scoreRequest', 
			data: emailObject,
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			dataType: 'json'
		});
	}

	return faqRestFactory;

} ]);