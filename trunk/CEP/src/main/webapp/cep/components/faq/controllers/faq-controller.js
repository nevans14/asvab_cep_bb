cepApp.directive('dir', function($compile, $parse) {
	return {
		restrict : 'E',
		link : function(scope, element, attr) {
			scope.$watch(attr.content, function() {
				element.html($parse(attr.content)(scope));
				$compile(element.contents())(scope);
			}, true);
		}
	}
})
.controller('FaqController', [ '$scope', 'FaqRestFactory', 'modalService', 'ScoreRequestModalService', 'vcRecaptchaService', 'recaptchaKey', '$location', '$window', 'BringToSchoolModalService', function($scope, FaqRestFactory, modalService, ScoreRequestModalService, vcRecaptchaService, recaptchaKey, $location, $window, BringToSchoolModalService) {
	
	$scope.about = [ {
		title : 'What is the ASVAB? ',
		description : 'The ASVAB is the Armed Services Vocational Aptitude Battery. It is an aptitude test that measures developed abilities and helps predict future academic and occupational success. '
	}, {
		title : 'What does ASVAB CEP stand for? ',
		description : 'Armed Services Vocational Aptitude Battery (ASVAB) Career Exploration Program (CEP). Pronounced "as-vab c.e.p."'
	}, {
		title : 'What is the purpose of the ASVAB CEP? ',
		description : 'The Department of Defense sponsors the ASVAB CEP with a two-part mission, to provide: a career exploration service to U.S. youth and qualified leads to military recruiters. The ASVAB CEP offers students a chance to explore all paths to careers - college, certifications, apprenticeships, licensure programs, and the Military - in one place.'
	}, {
		title : 'When did the ASVAB CEP begin? ',
		description : 'The ASVAB has been used in the Student Testing Program (STP) since 1968. The Career Exploration Program (CEP) component was added in 1992.'
	}, {
		title : 'What is the difference between ASVAB CEP and the enlistment ASVAB? ',
		description : 'The ASVAB CEP is a career planning and exploration program offered free to participating schools and students. ' +
		'The enlistment version of the ASVAB is primarily given at a Military Entrance Processing Station (MEPS) and is used for recruiting purposes only. '
	}, {
		title : 'If I take the ASVAB, will recruiters contact me? ',
		description : "Military and college recruiters obtain student directory information from multiple sources to identify high-quality talent. Students may be contacted by college and military recruiters regardless of ASVAB CEP participation or the score release option your school chooses.<br><br>" +
		"Schools determine if ASVAB results are released to the Military. However, you can <a ng-click='optInLink()'>opt in</a> to or <a ng-click='optOutLink()'>opt out</a> of the release option your school chooses. Be sure to coordinate with your school counselor. Regardless, students participating in the ASVAB Career Exploration Program have no obligation to the Military or to talk to a military recruiter."
	}, {
		title : 'Is the ASVAB CEP available outside of the United States? ',
		description : 'The ASVAB CEP is administered to high school and post-secondary students across the United States, Puerto Rico, Guam and the Marianas, and Department of Defense Education Activities (DODEA) overseas. The program is not available in other areas.'
	}, {
		title : 'What is the ASVAB CEP connection to the Military? ',
		description : 'ASVAB CEP test sessions are administered by DoD civilians but are supported by a military personnel proctors (one proctor for every 40 students). Proctors are required to ensure test security and validity. Also, 11th and 12th grade student ASVAB CEP scores are valid for enlistment for two years after test date. Students interested in military service can explore their career options at <a ng-click="CITMLink()">careersinthemilitary.com</a>.'
	} ];
	
	$scope.testing = [ {
		title : 'When is my high school testing? ',
		description : 'Contact your school counselor to find out when the ASVAB CEP is coming to your school. If it\'s not already scheduled, tell your counselor you\'re interested. <a ng-click="asvabModal()"> BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
	}, {
		title : 'Can reasonable accommodations be requested for the ASVAB CEP? ',
		description : 'Accommodations are provided for in the ASVAB CEP, including reading the test aloud, extra time, and enlarged print tests. However, all testing completed using an accommodation is coded Option 7 (Invalid for enlistment purposes). Official ASVAB testing sites (Military Entrance Processing Stations and Military Entrance Test sites) do not provide accommodations during testing.'
	}, {
		title : 'Can students who are homeschooled take the ASVAB CEP? ',
		description : 'The ASVAB CEP is offered in schools. If affiliated with a Homeschool Association or Network that can provide a testing facility, we can provide the ASVAB CEP to the group. If not affiliated with an Association or Network, you can contact a local Education Services Specialist at 1-800-323-0513 to find out if area schools provide the program. If they do, you can make arrangements with the school counselor to participate. There is no individual administration of the program.'
	}, {
		title : 'How do I request the ASVAB CEP for my school? ',
		description : 'To contact the nearest office, please dial 1-800-323-0513. Your call will automatically be connected to the closest ASVAB CEP office. Calls are routed based on the area code and number from which you are dialing. Caution: If you are calling from a cell phone number that you obtained in another state, you will be connected to an Education Services Specialist in that area. As an alternative, <a ng-click="asvabModal()">BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
	}, {
		title : 'How do I sign up for the ASVAB CEP? ',
		description : 'High school and post-secondary students across the United States can participate in the ASVAB CEP. Testing schedules are based on school requests rather than individual requests. Your school counselor or our local representative will be able to provide information on school testing in your area. To contact the nearest office, please dial 1-800-323-0513. You will be connected to the closest ASVAB CEP office. Calls are routed based on the area code and number from which you are dialing. (Caution: If you are calling from a cell phone number that you obtained in another state, you will be connected to an Education Services Specialist in that area.) As an alternative, <a ng-click="asvabModal()"> BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
	} ];

	$scope.score = [ {
		title : 'I took the test at my high school, when do I get my scores? ',
		description : "High schools tests are scored within two weeks of testing and returned to the school counselor with career exploration materials. You will receive your scores from your school counselor or an ASVAB Career Exploration Program Education Services Specialist. If you haven't received your scores, first check with your counselor for assistance. If your school counselor has not received the scores after 30 days, <a ng-click='openScoreRequestModal()'>click here to request your scores.</a>"
	}, {
		title : 'I took the test in high school three years ago, can you send me my scores? ',
		description : "We maintain ASVAB test scores for two years after the date of testing. If you took the test within the last two years, we can retrieve your scores using your name, school name, school city/state, and estimated test date (month/year). <a ng-click='openScoreRequestModal()'>Click here to request your scores.</a>\n" +
		"<br><br>If you tested over two years ago, we can no longer retrieve your scores."
	}, {
		title : 'Can I view my scores online?',
		description : 'Yes. Create an account using the access code found on your ASVAB Summary Results (ASR) sheet.<br>'+
		'<br><ul><li>Your career exploration scores are available at <a ng-click="asvabprogramLink()">asvabprogram.com</a>.</i>'+
		'<li>Your ASVAB CEP scores are converted to military line scores, <a ng-click="CITMCompositeScoreLink()">here</a>.</i></ul>' +
		'The username and password you create gives you access to both websites.'
	}, {
		title : 'The scores I received in school are different from the scores my recruiter has. Which scores are correct? ',
		description : 'Scores on the ASVAB CEP ASVAB Summary Results (ASR) sheet are provided for career exploration purposes only. The only score on the ASR that is the same for military recruiting use is the AFQT or Military Entrance Score. <br><br>' +
		'ASVAB CEP participants are encouraged to create an account using their access code to view their career exploration scores at <a ng-click="asvabprogramLink()">asvabprogram.com</a>. ASVAB CEP scores are converted to military line scores, <a ng-click="CITMCompositeScoreLink()">here</a>.<br><br>' +
		'You must create an account using the access code found on your ASR to view your scores online. The username and password you create gives you access to both websites.<br><br>' + 
		'Note: MEPCOM can only retrieve scores from tests taken in 11th and 12th grade and post-secondary schools. 11th and 12th grade and post-secondary students can use their ASVAB CEP AFQT scores to enlist for up to two years after testing. All 10th grade scores are invalid for enlistment purposes (regardless of age) and are not available through the USMIRS database.',
	} ];

	$scope.access = [ {
		title : "I lost my access code. Can you send it to me?",
		description : "Yes, ASVAB CEP participants can request your access code, <a ng-click='openScoreRequestModal()'>here</a>."
	},{
		title : "Can I request an access code?",
		description : "Parties interested in the career exploration activities ASVAB CEP offers can request a temporary access code, <a ng-click='openScoreRequestModal()'>here</a>."
	}];

	$scope.military = [ {
		title : "Why was there a recruiter at my testing session? ",
		description : 'ASVAB CEP test sessions are administered by Department of Defense civilians but are supported by a military personnel proctors (one proctor for every 40 students). Proctors are required to ensure test security and validity. Most schools elect to use military proctors rather than tying up school staff during test sessions.'
	}, {
		title : "Where can I find military career information? ",
		description : '<a ng-click="CITMLink()">Careersinthemilitary.com</a> is a comprehensive online resource powered by ASVAB CEP that allows students to discover extensive details about military career opportunities across all Services, their Service-specific ASVAB line scores and which Services offer which jobs.'
	}, {
		title : "I'm interested in the Military, where can I take an ASVAB test? ",
		description : 'High school and post-secondary students may be able to test at their local school. Check with your school counselor to find out if the ASVAB CEP is offered. Talk to a recruiter to find out if your scores qualify you for service.<br><br>'+
		'If you\'re not in school, contact a military recruiter to request testing. The test is administered at 65 Military Entrance Processing Stations (MEPS) and over 300 Military Entrance Test (MET) Sites nationwide. You must be sponsored by a recruiter to test at these facilities. <br><br>'+
		'Find the nearest MEPS, <a ng-click=mepcomLink()>here</a>. After selecting the MEPS, select the link to "Military Entrance Test (MET) Sites" under the dark blue bar to find the nearest MET sites for testing.'
	}, {
		title : "Which occupations do I qualify for in the Military? ",
		description : 'The Military Entrance Score, or AFQT, is what the Military will use to determine enlistment eligibility. Each Service sets its own AFQT score requirement. Then, each military job has its own unique composite score requirement.*<br><br>'+
		'Your ASVAB CEP scores are converted to military lines scores <a ng-click="CITMCompositeScoreLink()">here</a>. For the most up-to-date requirements or to find out if you qualify for military service, <a ng-click="contactRecruiter()">contact a recruiter</a>. <br><br>'+
		'If you are in 11<sup>th</sup> grade or beyond and want to enlist using these scores, log in to <a ng-click="asvabprogramLink()">asvabprogram.com</a> or <a ng-click="CITMLink()">careersinthemilitary.com</a> to show recruiters your scores. Recruiters will then submit a formal request to pull your scores using FORM 680-3A. <br><br>'+
		'* These score requirements are subject to change without notice and vary depending on the recruiting environment and the need for new military applicants.'
	}, {
		title : "I want to join the Air Force/Army/Marine Corps/National Guard/Navy. Can you help me? ",
		description : 'The ASVAB CEP helps students search the world of work for occupations that they may find satisfying. Some of these careers are found in the Military. You can use your ASVAB Career Exploration Scores to find military careers for which you would be best suited at <a ng-click="CITMLink()">careersinthemilitary.com</a>. However, ASVAB CEP personnel are not involved in recruiting activities. For information on how to join the Military, click <a ng-click="CITMOptionsLink()">here</a>. Then, <a ng-click="contactRecruiter()">contact a military recruiter</a> in your area.'
	}, {
		title : "I want to join the Military, how do I improve my score? ",
		description : "We recommend the Army's <a ng-click='march2successLink()'>March2Success</a> program for ASVAB test score improvement. This resource provides general test preparation materials for ACT, SAT, and high school completion. As the AFQT score is a combination of mathematics knowledge, arithmetic reasoning, paragraph comprehension, and vocabulary skills, studying the related material at this site in any of those areas may help you improve your qualifying score."
	}, {
		title : "How do I contact a recruiter? ",
		description : 'Contact a military recruiter in your area <a ng-click="contactRecruiter()">here</a>.'
	}, {
		title : "Where can I find more information about the ASVAB? ",
		description : 'Be sure to review the sample test items and test taking strategies <a ng-click="asvbprogramStudentLink()">here</a>.<br>'+
		'Then, you can find more information and more sample test questions at <a ng-click="officialasvabLink()">officialasvab.com</a>. For even more practice questions, visit <a ng-click="todaymilitaryLink()">todaysmilitary.com</a>.'
	} ];

	$scope.userType;
	$scope.firstName;
	$scope.lastName;
	$scope.email;
	$scope.topics;
	$scope.message;
	$scope.formDisabled = false;
	$scope.recaptchaResponse = '';
	$scope.recaptchaKey = recaptchaKey;

	$scope.emailUs = function() {
		$scope.validation = undefined;
		$scope.formDisabled = true;
		var emailObject = {
			userType : $scope.userType,
			firstName : $scope.firstName,
			lastName : $scope.lastName,
			email : $scope.email,
			topics : $scope.topics,
			message : $scope.message,
			recaptchaResponse : $scope.recaptchaResponse
		}

		if ($scope.userType == undefined ||$scope.firstName == undefined || $scope.lastName == undefined || $scope.email == undefined || $scope.topics == undefined || $scope.message == undefined) {
			$scope.validation = "Fill out all fields.";
			$scope.formDisabled = false;
			return false;
		}

		if ($scope.recaptchaResponse === "") { // if string
			// is empty
			$scope.validation = "Please resolve the captcha and submit!";
			$scope.formDisabled = false;
			return false;
		} else {

			FaqRestFactory.emailUs(emailObject).then(function(success) {
				
				// Track event in Google Analytics.
				var path = $location.path();
				$window.ga('create', 'UA-83809749-1', 'auto');
				$window.ga('send', 'pageview', path + '#'+ emailObject.userType + '#' + emailObject.topics + '#contact-form-submitted' );

				$scope.userType = undefined;
				$scope.firstName = undefined;
				$scope.lastName = undefined;
				$scope.email = undefined;
				$scope.topics = undefined;
				$scope.message = undefined;
				$scope.recaptchaResponse = '';
				vcRecaptchaService.reload();

				var modalOptions = {
					headerText : 'Contact Us',
					bodyText : 'Thank you for contacting us!'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);

				$scope.formDisabled = false;
			}, function(error) {
				
				var errorData = error.data ? error.data : undefined;
				var errorMessage = error.data.errorMessage ? error.data.errorMessage : undefined;
				
				if (errorMessage) { // robot error message
					var modalOptions = {
							headerText : 'Error',
							bodyText : errorMessage
						};

						modalService.showModal({
							size : 'sm'
						}, modalOptions);
				} else if (errorData){ // display back end validation messages
					
					var message = '';
					for (var property in errorData) {
						message+= errorData[property] + '\n';
					}
					
					var modalOptions = {
						headerText : 'Error',
						bodyText : message
					};

					modalService.showModal({
						size : 'md'
					}, modalOptions);
				} else {
					
					var modalOptions = {
							headerText : 'Contact Us',
							bodyText : 'There was an error when trying to contact us. Please try again.'
						};

						modalService.showModal({
							size : 'sm'
						}, modalOptions);
						vcRecaptchaService.reload();
				}

				$scope.formDisabled = false;
			});
		}

	}

	$scope.openScoreRequestModal = function() {
		ScoreRequestModalService.showModal({
			size : 'lg'
		}, {});
	}

	$scope.optInLink = function(){
		var url = "https://www.asvabprogram.com/pdf/ASVAB_CEP_Opt_In_Letter.docx"
			window.open(url,'_blank')
	}
	
	$scope.optOutLink = function(){
		var url = "https://www.asvabprogram.com/pdf/ASVAB_CEP_Opt_Out_Letter.docx"
			window.open(url,'_blank')
	}
	
	$scope.CITMLink = function(){
		var url = "https://www.careersinthemilitary.com"
			window.open(url,'_blank')
	}

	$scope.asvabprogramLink = function(){
		var url = "https://www.asvabprogram.com"
			window.open(url,'_blank')
	}
	$scope.asvbprogramStudentLink = function(){
		var url = "https://www.asvabprogram.com/student-program"
			window.open(url,'_blank')
	}
	$scope.CITMCompositeScoreLink = function(){
		var url = "https://www.careersinthemilitary.com/composite-scores"
			window.open(url,'_blank')
	}
	$scope.CITMOptionsLink = function(){
		var url = "https://www.careersinthemilitary.com/options"
			window.open(url,'_blank')
	}

	$scope.contactRecruiter = function(){
		var url = "https://www.careersinthemilitary.com/contact-us"
			window.open(url,'_blank')
	}
	
	$scope.officialasvabLink = function(){
		var url = "http://www.officialasvab.com/faq_app.htm"
			window.open(url,'_blank')
	}

	$scope.mepcomLink = function(){
		var url = "http://www.mepcom.army.mil/Units/"
			window.open(url,'_blank')
	}
	
	$scope.march2successLink = function(){
		var url = "http://www.march2success.com"
			window.open(url,'_blank')
	}
	$scope.todaymilitaryLink = function(){
		var url = "https://todaysmilitary.com/joining/asvab-test-sample-questions"
			window.open(url,'_blank')
	}
	
	$scope.asvabModal = function() {
		BringToSchoolModalService.show({}, {});
	}

	$scope.metaDescription = "Frequently asked questions about the ASVAB Career Exploration Program. For a faster response, please see the commonly asked questions & answers.";
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
