cepApp.controller('ScoreRequestController', [ '$scope', 'FaqRestFactory', 'modalService', 'ScoreRequestModalService', 'vcRecaptchaService', 'recaptchaKey', '$location', '$window', 'BringToSchoolModalService', function($scope, FaqRestFactory, modalService, ScoreRequestModalService, vcRecaptchaService, recaptchaKey, $location, $window, BringToSchoolModalService) {
  $scope.seoTitle = 'Find my ASVAB Score | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Lost your ASVAB score sheet? Find your ASVAB score through the ASVAB score request form. Your career exploration results are available at asvabprogram.com.';

  ScoreRequestModalService.showModal({
    size : 'lg'
  }, {});
  
} ]);
