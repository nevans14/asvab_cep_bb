cepApp.service('ScoreRequestModalService', [ '$uibModal', '$sce', 'modalService', '$templateCache', 'FaqRestFactory', 'vcRecaptchaService', 'recaptchaKey', '$location', '$window', function($uibModal, $sce, modalService, $templateCache, FaqRestFactory, vcRecaptchaService, recaptchaKey, $location, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/faq/page-partials/score-request-modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : '',
		windowClass : 'asvab-modal'
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

				// date picker template override
				$templateCache.put("uib/template/datepickerPopup/popup.html", "<div>\n" + "  <ul class=\"uib-datepicker-popup dropdown-menu uib-position-measure\" dropdown-nested ng-if=\"isOpen\" ng-keydown=\"keydown($event)\" ng-click=\"$event.stopPropagation()\">\n" + "    <li ng-transclude></li>\n" + "    <li ng-show=\"$parent.$parent.showErrorOne\" class=\"uib-button-bar\" style=\"background-color: #f9e3e3; padding: 10px; font-size: smaller; width: 220px;\">You cannot select a date in the future. Please try again.</li>\n" + "    <li ng-show=\"$parent.$parent.showErrorTwo\" class=\"uib-button-bar\" style=\"background-color: #f9e3e3; padding: 10px; font-size: smaller; width: 220px;\">Test scores more than two years old are removed from the ASVAB CEP system.</li>\n" + "    <li ng-if=\"showButtonBar\" class=\"uib-button-bar\">\n" + "      <span class=\"btn-group pull-left\">\n test" + "        <button type=\"button\" class=\"btn btn-sm btn-info uib-datepicker-current\" ng-click=\"select('today', $event)\" ng-disabled=\"isDisabled('today')\">{{ getText('current') }}</button>\n" + "        <button type=\"button\" class=\"btn btn-sm btn-danger uib-clear\" ng-click=\"select(null, $event)\">{{ getText('clear') }}</button>\n" + "      </span>\n" + "      <button type=\"button\" class=\"btn btn-sm btn-success pull-right uib-close\" ng-click=\"close($event)\">{{ getText('close') }}</button>\n" + "    </li>\n" + "  </ul>\n" + "</div>\n" + "");

				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.notAllowedPopup = function() {
					$uibModalInstance.dismiss('cancel');
					var modalOptions = {
						headerText : 'Score Request',
						bodyText : 'MEPCOM cannot release test scores to parents of children ages 18 and over.'
					};

					modalService.showModal({
						size : 'sm'
					}, modalOptions);
				}				
				
				$scope.resetStateSelection = function () {
					if ($scope.country != 'US') {
						$scope.state = '';						
					} 
				}
				
				$scope.formDisabled = false;
				$scope.userType;
				$scope.firstName;
				$scope.lastName;
				$scope.email;
				$scope.schoolName;
				$scope.city;
				$scope.county;
				$scope.state;
				$scope.country;
				$scope.regGradeLevel;
				$scope.message;
				$scope.recaptchaResponse = '';
				$scope.recaptchaKey = recaptchaKey;
				$scope.widgetId;
				$scope.onWidgetCreate = function(_widgetId) {
					$scope.widgetId = _widgetId;
				};
				$scope.maxDate = new Date();

				$scope.submit = function() {
					$scope.formDisabled = true;
					$scope.working = true;
					$scope.validation = undefined;
					$scope.showDateError = false;

					if (!($scope.dt instanceof Date)) {
						$scope.validation = "Invalid Date.";
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					}
					
					if ($scope.message == undefined){
						$scope.message = null;
						console.log("message: " + $scope.message);
					}
					
					var emailObject = {
						userType		: $scope.userType,
						firstName 	: $scope.firstName,
						lastName 		: $scope.lastName,
						email 			: $scope.email,
						schoolName 	: $scope.schoolName,
						city 				: $scope.city,
						county 			: $scope.county,
						state 			: $scope.state,
						country			: $scope.country,
						gradeLevel	: $scope.regGradeLevel,
						message 		: $scope.message,
						dt 					: ($scope.dt.getMonth() + 1) + '/' + $scope.dt.getDate() + '/' + $scope.dt.getFullYear(),
						recaptchaResponse : $scope.recaptchaResponse
					}

					var currentTimeStamp = new Date();
					var twoYearJulyTimeStamp = new Date(currentTimeStamp.getFullYear()-2, 6);
					var selectedTimeStamp = $scope.dt;

					if ($scope.userType == undefined || 
							$scope.firstName == undefined || 
							$scope.lastName == undefined || 
							$scope.email == undefined || 
							$scope.schoolName == undefined || 
							$scope.city == undefined || 
							($scope.county == undefined && $scope.country == 'US') || 
							$scope.state == undefined ||
							$scope.country == undefined ||
							$scope.dt == undefined ||
							$scope.regGradeLevel == undefined) {
						$scope.validation = "Fill out all fields.";
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					}  	//user cannot select a date prior to July 1 from 2 years ago
					else if (twoYearJulyTimeStamp >= selectedTimeStamp) {  
						$scope.validation = "Invalid Date cannot select date before " + twoYearJulyTimeStamp.toLocaleDateString();
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					}else if (selectedTimeStamp > currentTimeStamp){
						$scope.validation = "Invalid Date cannot select a date in the future.";
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					}
					if ($scope.recaptchaResponse === "") { // if string
						// is empty
						$scope.validation = "Please resolve the captcha and submit!";
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					} else {

						FaqRestFactory.scoreRequest(emailObject).then(function(success) {
							
							// Track event in Google Analytics.
							var path = $location.path();
							$window.ga('create', 'UA-83809749-1', 'auto');
							$window.ga('send', 'pageview', path + '#' + emailObject.userType + '#score-request' + '#contact-form-submitted' );

							if (success.data &&
									success.data.resultCode &&
									(success.data.resultCode == 1 ||
									success.data.resultCode == 2) && 
									success.data.resultMessage) {
								$scope.scoreRequestResponse = success.data.resultMessage;
							} else {
								$scope.scoreRequestResponse = "Someone will email you soon.";
							}

							$scope.userType = undefined;
							$scope.firstName = undefined;
							$scope.lastName = undefined;
							$scope.email = undefined;
							$scope.schoolName = undefined;
							$scope.city = undefined;
							$scope.county = undefined;
							$scope.state = undefined;
							$scope.country = undefined;
							$scope.regGradeLevel = undefined;
							$scope.message = undefined;
							$scope.recaptchaResponse = '';
							vcRecaptchaService.reload();

							$scope.stepTwo = false;
							$scope.stepThree = true;
							$scope.formDisabled = false;
							$scope.working = false;
						}, function(error) {

							var error = error.data.errorMessage;
							if (error == undefined) {
								$scope.validation = "There was an error proccessing your request. Please try submitting again.";
								vcRecaptchaService.reload($scope.widgetId);
							} else {
								$scope.validation = error;
							}
							$scope.formDisabled = false;
							$scope.working = false;
						});
					}
				}

				/**
				 * Date picker popup
				 */
				$scope.$watch('dt', function() {

					//user cannot select a date prior to July 1 from 2 years ago
					var currentTimeStamp = new Date();
					var twoYearJulyTimeStamp = new Date(currentTimeStamp.getFullYear()-2, 6);
					var selectedTimeStamp = $scope.dt;

					if (twoYearJulyTimeStamp >= selectedTimeStamp) {
						$scope.showErrorTwo = true;
						$scope.showErrorOne = false;
					} else if (selectedTimeStamp > currentTimeStamp){
						$scope.showErrorTwo = false;
						$scope.showErrorOne = true;
					}else {
						$scope.showErrorOne = false;
						$scope.showErrorTwo = false;
					}
				});

				$scope.today = function() {
					$scope.dt = undefined;
				};

				$scope.today();

				$scope.clear = function() {
					$scope.dt = null;
				};

				$scope.inlineOptions = {
					customClass : getDayClass,
					minDate : new Date(),
				};

				$scope.dateOptions = {
					formatYear : 'yy',
					minDate : new Date(),
					showWeeks : false,
				};

				// Disable weekend selection
				function disabled(data) {
					var date = data.date, mode = data.mode;
					return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
				}

				$scope.toggleMin = function() {
					$scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
					$scope.dateOptions.minDate = $scope.inlineOptions.minDate;
				};

				$scope.toggleMin();

				$scope.open1 = function() {
					$scope.popup1.opened = true;
				};

				$scope.open2 = function() {
					$scope.popup2.opened = true;
				};

				$scope.setDate = function(year, month, day) {
					$scope.dt = new Date(year, month, day);
				};

				$scope.formats = [ 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate' ];
				$scope.format = $scope.formats[0];
				$scope.altInputFormats = [ 'M!/d!/yyyy' ];

				$scope.popup1 = {
					opened : false
				};

				$scope.popup2 = {
					opened : false
				};

				var tomorrow = new Date();
				tomorrow.setDate(tomorrow.getDate() + 1);
				var afterTomorrow = new Date();
				afterTomorrow.setDate(tomorrow.getDate() + 1);
				$scope.events = [ {
					date : tomorrow,
					status : 'full'
				}, {
					date : afterTomorrow,
					status : 'partially'
				} ];

				function getDayClass(data) {
					var date = data.date, mode = data.mode;
					if (mode === 'day') {
						var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

						for (var i = 0; i < $scope.events.length; i++) {
							var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

							if (dayToCheck === currentDay) {
								return $scope.events[i].status;
							}
						}
					}

					return '';
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);