cepApp.controller('MyAsvabSummaryController', [ 
	'$scope', '$timeout', '$q', 'FYIScoreService', 'FYIRestFactory', '$rootScope', '$window', '$location', '$anchorScroll', '$route', '$templateCache', 'MediaCenterService', 'modalService', 'youTubeModalService', 'scoreSummary', 'cepManualInput', 'ctmUrl', 'asvabScore', 'UtilityService',
	function($scope, $timeout, $q, FYIScoreService, FYIRestFactory, $rootScope, $window, $location, $anchorScroll, $route, $templateCache, MediaCenterService, modalService, youTubeModalService, scoreSummary, cepManualInput, ctmUrl, asvabScore, UtilityService) {
	
	$scope.userRole = $rootScope.globals.currentUser.role;
	$scope.scoreSummary = scoreSummary.data;
	$scope.asvabScore = asvabScore.data; // manually entered score data
	$scope.haveAsvabTestScores = (angular.isObject($scope.scoreSummary) 
		&& $scope.scoreSummary.hasOwnProperty('verbalAbility') 
		&& $scope.scoreSummary.verbalAbility.hasOwnProperty('va_SGS')
		&& $scope.scoreSummary.verbalAbility.va_SGS !== null); // do we have actual test scores
	$scope.haveManualAsvabTestScores = (!$scope.haveAsvabTestScores && ($window.sessionStorage.getItem('manualScores') == "true")); // do we have manual test scores
	$scope.ssoUrl = ctmUrl;
	$scope.genderFull = "";
	$scope.testDate = {
		"month" : "",
		"day" : "",
		"year" : ""
	}
	$scope.mediaCenterArticle = 'media-center-article/';
	$scope.resultsSelection = "student";

	MediaCenterService.getMediaCenterList().then(function(s) {
		$scope.mediaCenterList = s;
	})

	$scope.showYouTubeModal = function(videoName) {
		youTubeModalService.show({
			size : 'lg'
		}, {
			videoName : videoName
		});
	}

	$scope.mobileOrTablet = UtilityService.mobileOrTablet();
	
	/***
	 * Set session for books
	 */
	$scope.getScore = function(area) {
		var score = 0;
		switch (area) {
			case "verbal":
				if ($scope.haveAsvabTestScores) {
					score = $scope.scoreSummary ? Number($scope.scoreSummary.verbalAbility.va_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore);
				}
				break;
			case "math":
				if ($scope.haveAsvabTestScores) {
					score = $scope.scoreSummary ? Number($scope.scoreSummary.mathematicalAbility.ma_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore);
				}
				break;
			case "science":
				if ($scope.haveAsvabTestScores) {
					score = $scope.scoreSummary ? Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore);
				}
				break;
			case "afqt": // no manual entry for AFQT
				score = $scope.scoreSummary ? Number($scope.scoreSummary.aFQTRawSSComposites.afqt) : 0;
				break;
			default:
				break;
		}
		return score;
	}
	
	// Add scores to session.
	if ($scope.haveAsvabTestScores) {
		$window.sessionStorage.setItem('manualScores', false);
		$window.sessionStorage.setItem('sV', $scope.getScore('verbal'));
		$window.sessionStorage.setItem('sM', $scope.getScore('math'));
		$window.sessionStorage.setItem('sS', $scope.getScore('science'));
		$window.sessionStorage.setItem('sA', $scope.getScore('afqt'));
	} else if ($scope.haveManualAsvabTestScores) {
		$window.sessionStorage.setItem('manualScores', true);
		$window.sessionStorage.setItem('sV', Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore));
		$window.sessionStorage.setItem('sM', Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore));
		$window.sessionStorage.setItem('sS', Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore));
	}
	
	// Create book objects for sort/ranking - display gray book (score: 0) for manual scores
	$scope.vBooks = { 
		name: "verbal",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sV') : 0
		score: $window.sessionStorage.getItem('sV')
	}

	$scope.mBooks = {
		name: "math",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sM'): 0
		score: $window.sessionStorage.getItem('sM')
	}

	$scope.sBooks = {
		name: "science",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sS') : 0
		score: $window.sessionStorage.getItem('sS')
	}	
	
	/**
	 * Calculate book sprite x pos from ranked score
	 * 1 book = lowest score
	 * 2 books = middle score
	 * 3 books = highest score
	 * Ties would use just 2- and 3-book stacks, with the duplicate 
	 * determined by whether the tie was with the highest or the lowest score
	 */
	$scope.scoreSorting = function() {
		$scope.scoresToSort = [$scope.vBooks, $scope.mBooks, $scope.sBooks];
		$scope.sortedScores = [];

		for (var i = 0; i < $scope.scoresToSort.length; i++) {
			$scope.sortedScores.push($scope.scoresToSort[i]);
		}

		$scope.sortedScores.sort(function(a, b) {
			return a.score-b.score;
		});

		for(var i = 0; i < $scope.sortedScores.length; i++) {
			$scope.sortedScores[i].rank = i + 1;
			$scope.sortedScores[i].tie = false;
		}
		$scope.scoreRanking();
	}

	$scope.scoreRanking = function() {
		for (var k = 0; k < $scope.sortedScores.length; k++) {
			for (var h = 1; h < $scope.sortedScores.length + 1; h++) {
				if ($scope.sortedScores[k+h] !== undefined) {
					if ($scope.sortedScores[k+h].tie !== true) {
						if ($scope.sortedScores[k].score === $scope.sortedScores[h + k].score) {
							$scope.sortedScores[k].rank = k + 1;
							$scope.sortedScores[h + k].rank = k + 1;
							$scope.sortedScores[k].tie = true;
							$scope.sortedScores[h + k].tie = true;
						}
					}
				}    
			}			
			// if tie scores, 2 books for low score tie, 3 books for high score tie
			if ($scope.sortedScores[k].tie) {
				if ($scope.sortedScores[k].rank == 1) {
					$scope.sortedScores[k].rank = 2; // low tie
				} else {
					$scope.sortedScores[k].rank = 3; // high tie
				}
			}			
			// no books for 0 score
			if ($scope.sortedScores[k].score == 0) {
				$scope.sortedScores[k].rank = 0; // no score
			}
			// set xPos based on rank, -60px incraments
			$scope.sortedScores[k].xPos = $scope.sortedScores[k].rank * -32; // larger book stack (on dash)
			$scope.sortedScores[k].xPosSmall = $scope.sortedScores[k].rank * -32; // smaller book stack (in left nav)
			
		}
		// Add book objects to session
		$window.sessionStorage.setItem('vBooks', angular.toJson($scope.vBooks));
		$window.sessionStorage.setItem('mBooks', angular.toJson($scope.mBooks));
		$window.sessionStorage.setItem('sBooks', angular.toJson($scope.sBooks));
	}
	// Initial book sorting/ranking
	$scope.scoreSorting();

	$scope.cepManualInput = function(cepManualMode) {
		var enterMode = cepManualMode == 'enter' ? true : false;
		var retrieveMode = cepManualMode == 'retrieve' ? true : false;
		cepManualInput.show({}, {enter: enterMode, retrieve: retrieveMode}).then(function(result) {
			//console.log(result);
			if (result) {
				if (result.hasOwnProperty('afqtRawComposites')) {
					// Retrieved asvab scores
					$scope.scoreSummary = result;
					//console.log($scope.scoreSummary);
					$scope.haveAsvabTestScores = true;
					$scope.haveManualAsvabTestScores = false;
				} else {
					// Entered asvab scores
					$scope.haveAsvabTestScores = false;
					$scope.haveManualAsvabTestScores = true;
					$scope.asvabScore.push({verbalScore : result.verbalScore, mathScore : result.mathScore, scienceScore : result.scienceScore});
				}
				
				// Update session storage
				$window.sessionStorage.setItem('manualScores', $scope.haveManualAsvabTestScores);
				//$window.sessionStorage.setItem('sV', $scope.getScore('verbal'));
				//$window.sessionStorage.setItem('sM', $scope.getScore('math'));
				//$window.sessionStorage.setItem('sS', $scope.getScore('science'));
				//$window.sessionStorage.setItem('sA', $scope.getScore('afqt'));

				// Update book stacks
				//$scope.vBooks.score = $scope.getScore('verbal');
				//$scope.mBooks.score = $scope.getScore('math');
				//$scope.sBooks.score = $scope.getScore('science');
				//$scope.scoreSorting();
				
				//update charts on page
				/*
				console.log($scope.myCharts["cesScores1_1"]);
				$scope.myCharts["cesScores1_1"].update();
				*/
				
			}
		});
	}

	$scope.scrollTo = function(id) {
		$location.hash(id);
		$anchorScroll();
	}

	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			$('.goToTop').fadeIn();
		} else {
			$('.goToTop').fadeOut();
		}
	});

	$('.goToTop').click(function () {
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});

	$scope.hiliteItems = function(area) {
		/**
		 * TODO: hilite areas of chart canvas
		 */
		var returnVal;
		if (area == "none") {
			returnVal = "Clear Hilites";
		} 
		else {
			switch(area) {
				case "verbal":
					returnVal = "Hilite Verbal Skills";
					break;
				case "math":
					returnVal = "Hilite Math Skills";
					break;
				case "science":
					returnVal = "Hilite Science Skills";
					break;
				case "other":
					returnVal = "Hilite Other Skills";
					break;
				default:
					break;
			};
		}
	}

	$scope.getLow = function(num1, num2) {
		return Math.min(num1, num2);
	}

	$scope.getHigh = function(num1, num2) {
		return Math.max(num1, num2);
	}

	/**
	 * On chart create event add all charts to object for reference
	 */
	$scope.myCharts = [];
	$scope.$on('chart-create', function (evt, chart) {
			$scope.myCharts[chart.chart.canvas.id] = chart;
			
			//chart.update();
	});

	/**
	 * CES Percentile Scores
	 * display order female, male, all
	 */
	$scope.chartRef = $scope.scoreSummary.verbalAbility
	if($scope.scoreSummary.controlInformation.gender != undefined)
		$scope.gender = $scope.scoreSummary.controlInformation.gender.substring(0,1).toLowerCase();
	else
		$scope.gender = 'm';
	
	$scope.cesData2_1 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.va_GS_Percentage) :  Number($scope.chartRef.va_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.va_GS_Percentage) :  Number($scope.chartRef.va_GOS_Percentage), 
			Number($scope.chartRef.va_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.mathematicalAbility
	$scope.cesData2_2 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.ma_GS_Percentage) :  Number($scope.chartRef.ma_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.ma_GS_Percentage) :  Number($scope.chartRef.ma_GOS_Percentage), 
			Number($scope.chartRef.ma_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.scienceTechnicalAbility
	$scope.cesData2_3 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.tec_GS_Percentage) :  Number($scope.chartRef.tec_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.tec_GS_Percentage) :  Number($scope.chartRef.tec_GOS_Percentage), 
			Number($scope.chartRef.tec_COMP_Percentage)
		]
	];

	/**
	 * ASVAB Percentile Scores
	 * display order femmale, male, all
	 */
	$scope.chartRef = $scope.scoreSummary.generalScience
	$scope.asvabData2_1 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.gs_GS_Percentage) :  Number($scope.chartRef.gs_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.gs_GS_Percentage) :  Number($scope.chartRef.gs_GOS_Percentage), 
			Number($scope.chartRef.gs_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.arithmeticReasoning
	$scope.asvabData2_2 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.ar_GS_Percentage) :  Number($scope.chartRef.ar_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.ar_GS_Percentage) :  Number($scope.chartRef.ar_GOS_Percentage), 
			Number($scope.chartRef.ar_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.wordKnowledge
	$scope.asvabData2_3 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.wk_GS_Percentage) :  Number($scope.chartRef.wk_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.wk_GS_Percentage) :  Number($scope.chartRef.wk_GOS_Percentage), 
			Number($scope.chartRef.wk_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.paragraphComprehension
	$scope.asvabData2_4 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.pc_GS_Percentage) :  Number($scope.chartRef.pc_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.pc_GS_Percentage) :  Number($scope.chartRef.pc_GOS_Percentage), 
			Number($scope.chartRef.pc_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.mathematicsKnowledge
	$scope.asvabData2_5 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.mk_GS_Percentage) :  Number($scope.chartRef.mk_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.mk_GS_Percentage) :  Number($scope.chartRef.mk_GOS_Percentage), 
			Number($scope.chartRef.mk_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.electronicsInformation
	$scope.asvabData2_6 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.ei_GS_Percentage) :  Number($scope.chartRef.ei_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.ei_GS_Percentage) :  Number($scope.chartRef.ei_GOS_Percentage), 
			Number($scope.chartRef.ei_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.autoShopInformation
	$scope.asvabData2_7 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.as_GS_Percentage) :  Number($scope.chartRef.as_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.as_GS_Percentage) :  Number($scope.chartRef.as_GOS_Percentage), 
			Number($scope.chartRef.as_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.mechanicalComprehension
	$scope.asvabData2_8 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.mc_GS_Percentage) :  Number($scope.chartRef.mc_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.mc_GS_Percentage) :  Number($scope.chartRef.mc_GOS_Percentage), 
			Number($scope.chartRef.mc_COMP_Percentage)
		]
	];

	$scope.greenColors = [
		{
			backgroundColor: "#29afb5",
			borderColor: "#29afb5",
			hoverBackgroundColor: "#29afb5",
			hoverBorderColor: "#29afb5",
		}
	];
	
	$scope.redColors = [
		{
			backgroundColor: "#8b2e33",
			borderColor: "#8b2e33",
			hoverBackgroundColor: "#8b2e33",
			hoverBorderColor: "#8b2e33"
		}
	];
	
	$scope.purpleColors = [
		{
			backgroundColor: "#413a92",
			borderColor: "#413a92",
			hoverBackgroundColor: "#413a92",
			hoverBorderColor: "#413a92"
		}
	];
	$scope.blackColors = [
		{
			backgroundColor: "#333333",
			borderColor: "#333333",
			hoverBackgroundColor: "#333333",
			hoverBorderColor: "#333333"
		}
	];
	
	/**
	 * All/Global Percent chart labels/options
	 */
	$scope.percentChartLabels = ["Female", "Male", "All"];
	$scope.percentChartOptions = {
		events: [],
		title: {
			display: false
		},
		legend: {
			display: false
		},
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero: true,
					min: 0,
					max: 100,
					stepSize: 20
				},
				gridLines: {
					zeroLineColor: "transparent",
					zeroLineWidth: 0
				}
			}],
			yAxes: [{
				barThickness: 16,
				gridLines: {
					zeroLineColor: "transparent",
					zeroLineWidth: 0
				}
			}]
		},
		tooltips: {
			enabled: false
		},
		animation: {
			duration: 250,
			onProgress: modifyChartBars,
			onComplete: modifyChartBars
		}
	};

	/**
	 * Standard Scores - Actual
	 */
	$scope.standardScores = {
		cesScores1_1: [ // actual scores
			Number($scope.scoreSummary.verbalAbility.va_SGS), 
			Number($scope.scoreSummary.mathematicalAbility.ma_SGS), 
			Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS)
		],
		asvabScores1_1: [ // actual scores
			Number($scope.scoreSummary.generalScience.gs_SGS) > 0 ? Number($scope.scoreSummary.generalScience.gs_SGS) : 0, 
			Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) : 0, 
			Number($scope.scoreSummary.wordKnowledge.wk_SGS) > 0 ? Number($scope.scoreSummary.wordKnowledge.wk_SGS) : 0, 
			Number($scope.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? Number($scope.scoreSummary.paragraphComprehension.pc_SGS) : 0, 
			Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) : 0, 
			Number($scope.scoreSummary.electronicsInformation.ei_SGS) > 0 ? Number($scope.scoreSummary.electronicsInformation.ei_SGS) : 0, 
			Number($scope.scoreSummary.autoShopInformation.as_SGS) > 0 ? Number($scope.scoreSummary.autoShopInformation.as_SGS) : 0, 
			Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) : 0,
		]
	};
	
	/**
	 * CES Standard Scores Chart Data
	 * chart.js does not have a range bar chart
	 * Using a stacked horz bar chart to create a range type chart
	 * inital bar is set to transparent
	 * LSL = lower score limit; USL = high score limit
	 * added getLow/getHigh due to USL coming in lower than LSL in some data
	 */
	$scope.lowVals = [];
	$scope.lowVals = [
		$scope.getLow(Number($scope.scoreSummary.verbalAbility.va_LSL), Number($scope.scoreSummary.verbalAbility.va_USL)),
		$scope.getLow(Number($scope.scoreSummary.mathematicalAbility.ma_LSL), Number($scope.scoreSummary.mathematicalAbility.ma_USL)),
		$scope.getLow(Number($scope.scoreSummary.scienceTechnicalAbility.tec_LSL), Number($scope.scoreSummary.scienceTechnicalAbility.tec_USL)),
	];
	$scope.highVals = [];
	$scope.highVals = [
		$scope.getHigh(Number($scope.scoreSummary.verbalAbility.va_LSL), Number($scope.scoreSummary.verbalAbility.va_USL)),
		$scope.getHigh(Number($scope.scoreSummary.mathematicalAbility.ma_LSL), Number($scope.scoreSummary.mathematicalAbility.ma_USL)),
		$scope.getHigh(Number($scope.scoreSummary.scienceTechnicalAbility.tec_LSL), Number($scope.scoreSummary.scienceTechnicalAbility.tec_USL)),
	];

	$scope.cesData1_1 = [
		[ // score low range bar start value < actual score or 0 if no score
			Number($scope.scoreSummary.verbalAbility.va_SGS) > 0 ? $scope.lowVals[0]+2 : 0,
			Number($scope.scoreSummary.mathematicalAbility.ma_SGS) > 0 ? $scope.lowVals[1]+2 : 0,
			Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? $scope.lowVals[2]+2 : 0
		],
		[ // score total bar range (high - low) > actual score or 0 if no score
			Number($scope.scoreSummary.verbalAbility.va_SGS) > 0 ? $scope.highVals[0] - $scope.lowVals[0]+2 : 0,
			Number($scope.scoreSummary.mathematicalAbility.ma_SGS) > 0 ? $scope.highVals[1] - $scope.lowVals[1]+2 : 0,
			Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? $scope.highVals[2] - $scope.lowVals[2]+2 : 0
		]
	];

	$scope.cesLabels1_1 = [
		"Verbal Skills", 
		"Math Skills", 
		["Science/", "Technical Skills"]
	];

	$scope.standardScoreColors1 = [
		{
			backgroundColor: ["transparent", "transparent", "transparent"],
			borderColor: ["transparent", "transparent", "transparent"],
			hoverBackgroundColor: ["transparent", "transparent", "transparent"],
			hoverBorderColor: ["transparent", "transparent", "transparent"]
		},
		{
			backgroundColor: [
				$scope.greenColors[0].backgroundColor,
				$scope.redColors[0].backgroundColor,
				$scope.purpleColors[0].backgroundColor
			],
			borderColor: [
				$scope.greenColors[0].borderColor,
				$scope.redColors[0].borderColor,
				$scope.purpleColors[0].borderColor
			]//,
			//hoverBackgroundColor: "#333333",
			//hoverBorderColor: "#333333"
		}
	];

	/**
	 * ASVAB Standard Scores Chart Data
	 * chart.js does not have a range bar chart
	 * Using a stacked horz bar chart to create a range type chart
	 * inital bar is set to transparent 
	 * LSL = lower score limit; USL = high score limit
	 * added getLow/getHihg due to USL coming in lower than LSL in some data
	 */
	$scope.lowVals = [];
	$scope.lowVals = [
		$scope.getLow(Number($scope.scoreSummary.generalScience.gs_LSL), Number($scope.scoreSummary.generalScience.gs_USL)),
		$scope.getLow(Number($scope.scoreSummary.arithmeticReasoning.ar_LSL), Number($scope.scoreSummary.arithmeticReasoning.ar_USL)),
		$scope.getLow(Number($scope.scoreSummary.wordKnowledge.wk_LSL), Number($scope.scoreSummary.wordKnowledge.wk_USL)),
		$scope.getLow(Number($scope.scoreSummary.paragraphComprehension.pc_LSL), Number($scope.scoreSummary.paragraphComprehension.pc_USL)),
		$scope.getLow(Number($scope.scoreSummary.mathematicsKnowledge.mk_LSL), Number($scope.scoreSummary.mathematicsKnowledge.mk_USL)),
		$scope.getLow(Number($scope.scoreSummary.electronicsInformation.ei_LSL), Number($scope.scoreSummary.electronicsInformation.ei_USL)),
		$scope.getLow(Number($scope.scoreSummary.autoShopInformation.as_LSL), Number($scope.scoreSummary.autoShopInformation.as_USL)),
		$scope.getLow(Number($scope.scoreSummary.mechanicalComprehension.mc_LSL), Number($scope.scoreSummary.mechanicalComprehension.mc_USL)),
	];
	$scope.highVals = [];
	$scope.highVals = [
		$scope.getHigh(Number($scope.scoreSummary.generalScience.gs_LSL), Number($scope.scoreSummary.generalScience.gs_USL)),
		$scope.getHigh(Number($scope.scoreSummary.arithmeticReasoning.ar_LSL), Number($scope.scoreSummary.arithmeticReasoning.ar_USL)),
		$scope.getHigh(Number($scope.scoreSummary.wordKnowledge.wk_LSL), Number($scope.scoreSummary.wordKnowledge.wk_USL)),
		$scope.getHigh(Number($scope.scoreSummary.paragraphComprehension.pc_LSL), Number($scope.scoreSummary.paragraphComprehension.pc_USL)),
		$scope.getHigh(Number($scope.scoreSummary.mathematicsKnowledge.mk_LSL), Number($scope.scoreSummary.mathematicsKnowledge.mk_USL)),
		$scope.getHigh(Number($scope.scoreSummary.electronicsInformation.ei_LSL), Number($scope.scoreSummary.electronicsInformation.ei_USL)),
		$scope.getHigh(Number($scope.scoreSummary.autoShopInformation.as_LSL), Number($scope.scoreSummary.autoShopInformation.as_USL)),
		$scope.getHigh(Number($scope.scoreSummary.mechanicalComprehension.mc_LSL), Number($scope.scoreSummary.mechanicalComprehension.mc_USL)),
	];

	$scope.asvabData1_1 = [
		[ // // score low range bar start value < actual score
			Number($scope.scoreSummary.generalScience.gs_SGS) > 0 ? $scope.lowVals[0]+2 : 0,
			Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? $scope.lowVals[1]+2 : 0,
			Number($scope.scoreSummary.wordKnowledge.wk_SGS) > 0 ? $scope.lowVals[2]+2 : 0,
			Number($scope.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? $scope.lowVals[3]+2 : 0,
			Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? $scope.lowVals[4]+2 : 0,
			Number($scope.scoreSummary.electronicsInformation.ei_SGS) > 0 ? $scope.lowVals[5]+2 : 0,
			Number($scope.scoreSummary.autoShopInformation.as_SGS) > 0 ? $scope.lowVals[6]+2 : 0,
			Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? $scope.lowVals[7]+2 : 0,
		],
		[ // score total bar range (high - low) > actual score
			Number($scope.scoreSummary.generalScience.gs_SGS) > 0 ? $scope.highVals[0] - $scope.lowVals[0]+2 : 0,
			Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? $scope.highVals[1] - $scope.lowVals[1]+2 : 0,
			Number($scope.scoreSummary.wordKnowledge.wk_SGS) > 0 ? $scope.highVals[2] - $scope.lowVals[2]+2 : 0,
			Number($scope.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? $scope.highVals[3] - $scope.lowVals[3]+2 : 0,
			Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? $scope.highVals[4] - $scope.lowVals[4]+2 : 0,
			Number($scope.scoreSummary.electronicsInformation.ei_SGS) > 0 ? $scope.highVals[5] - $scope.lowVals[5]+2 : 0,
			Number($scope.scoreSummary.autoShopInformation.as_SGS) > 0 ? $scope.highVals[6] - $scope.lowVals[6]+2 : 0,
			Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? $scope.highVals[7] - $scope.lowVals[7]+2 : 0,
		]
	];
	
	$scope.asvabLabels1_2 = [
		["General", "Science"],
		["Arithmetic", "Reasoning"],
		["Word", "Knowledge"],
		["Paragraph", "Comprehension"],
		["Mathematics", "Knowledge"],
		["Electronics", "Information"],
		["Auto and", "Shop Information"],
		["Mechanical", "Comprehension"],
	];

	$scope.standardScoreColors2 = [
		{
			backgroundColor: ["transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent"],
			borderColor: ["transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent"],
			hoverBackgroundColor: ["transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent"],
			hoverBorderColor: ["transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent"]
		},
		{
			backgroundColor: [
				$scope.purpleColors[0].backgroundColor,
				$scope.redColors[0].backgroundColor,
				$scope.greenColors[0].backgroundColor,
				$scope.greenColors[0].backgroundColor,
				$scope.redColors[0].backgroundColor,
				$scope.purpleColors[0].backgroundColor,
				$scope.blackColors[0].backgroundColor,
				$scope.purpleColors[0].backgroundColor,
				$scope.blackColors[0].backgroundColor
			],
			borderColor: [
				$scope.purpleColors[0].borderColor,
				$scope.redColors[0].borderColor,
				$scope.greenColors[0].borderColor,
				$scope.greenColors[0].borderColor,
				$scope.redColors[0].borderColor,
				$scope.purpleColors[0].borderColor,
				$scope.blackColors[0].borderColor,
				$scope.purpleColors[0].borderColor,
				$scope.blackColors[0].backgroundColor
			]//,
			//hoverBackgroundColor: "#333333",
			//hoverBorderColor: "#333333"
		}
	];

	$scope.scoreChartOptions = {
		events: [],
		title: {
			display: false
		},
		legend: {
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true,
				ticks: {
					beginAtZero: true,
					min: 0,
					max: 100,
					stepSize: 20
				},
				gridLines: {
					zeroLineColor: "transparent",
					zeroLineWidth: 0
				}
			}],
			yAxes: [{
				stacked: true,
				barThickness: 16,
				gridLines: {
					zeroLineColor: "transparent",
					zeroLineWidth: 0
				}
			}]
		},
		tooltips: {
			enabled: false
		},
		animation: {
			duration: 250,
			onProgress: modifyChartBars,
			onComplete: modifyChartBars
		}
	};

	/**
	 * AFQT doughnut chart
	 */
	$scope.getAfqtScore = function() {
		var afqtScore = Number($scope.scoreSummary.aFQTRawSSComposites.afqt);
		return afqtScore
	}

	$scope.afqtChartData = [$scope.getAfqtScore(), (100-$scope.getAfqtScore())];
	$scope.afqtChartLabels = ["AFQT Score", "Total"];
	$scope.afqtChartColors = ['#4c44b4', '#f2f2f2'];
	$scope.afqtChartOptions = {
		events: [],
		segmentShowStroke : false,
		animation: false,
		tooltips: {
			enabled: false
		},
		elements: {
			arc: {
				borderWidth: 0
			}
		}
	};

	/**
	 * Chart helper function to modify bar chart bar displays
	 */
	function modifyChartBars() {
		var txt = "";
		var txtW = 0;
		var chartId = "";
		var chartInstance = this.chart;
		var ctx = chartInstance.ctx;
		var prevX = 0;
		var sX = 0;
		var sW = 0;
		var rem = [];
		
		ctx.font = Chart.helpers.fontString(10, 'normal', Chart.defaults.global.defaultFontFamily);
		ctx.fillStyle = chartInstance.config.options.defaultFontColor;
		ctx.textAlign = "center";
		ctx.textBaseline = 'middle';

		Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
			var meta = chartInstance.controller.getDatasetMeta(i);
			var stacked = meta.data[i]._yScale.options.stacked ? true : false; // detect if chart is stacked

			Chart.helpers.each(meta.data.forEach(function (bar, index) {
				chartId = chartInstance.canvas.id;
				txt = dataset.data[index].toString();
				txtW = Math.ceil(ctx.measureText(txt).width);
				if (dataset.backgroundColor[i] != "transparent") { // don't label transparent bars
					if (stacked) { // using stacked chart to display score range chart
						if ((dataset.data[index] <= 0) || ($scope.standardScores[chartId][index] <= 0)) { // if no data
							txt = "No Data";
						} else {
							txt = $scope.standardScores[chartId][index]; // replace bar label with actual score
						}
						sX = bar._model.base;
						sW = bar._model.x - sX;
					}

					if (dataset.data[index] <= 0) { // prevent label from displaying on bar if bar length is too narrow
						ctx.fillStyle = 'rgba(21, 21, 21, 1)'; // dark grey
						ctx.fillText("No Data", (bar._model.x + 20), bar._model.y);
					} else if (dataset.data[index] < 5) { // prevent label from displaying on bar if bar length is too narrow
						ctx.fillText(txt, (bar._model.x + 4), bar._model.y);
					} else {
						ctx.fillStyle = 'rgba(242, 242, 242, 1)'; // very light grey
						if (stacked && txt != "No Data") {
							ctx.fillText(txt, (sX + (sW/2)), bar._model.y);// center on score band
						} else {
							ctx.fillText(txt, (bar._model.x - txtW), bar._model.y); // place at end
						}
					}
				}
			}), this);
		}), this);
	}

	$scope.moreInfo = function(area) {
		var modalOptions;
		switch (area) {
		case "ces":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<p>Consider these scores when exploring careers in the OCCU-Find. Use your highest score to review occupations based on the corresponding skill importance rating.</p>' + 
					'<p>ASVAB test scores are combined to yield three Career Exploration Scores: Verbal Skills, Math Skills, and Science/Technical Skills. Each of these scores is made up of a combination of some of the individual ASVAB tests.</p>' + 
					'<p><strong>Verbal Skills</strong> - estimates your potential for verbal activities; derived from the Word Knowledge (WK) and Paragraph Comprehension (PC) tests.</p>' + 
					'<p><strong>Math Skills</strong> - estimates your potential for mathematical activities; derived from the Mathematics Knowledge (MK) and Arithmetic Reasoning (AR) tests.</p>' + 
					'<p><strong>Science/Technical Skills</strong> - estimates your potential for science and technical activities, derived from the General Science (GS), Mechanical Comprehension (MC), and Electronics Information (EI) tests.</p>' + 
					'<p>These three Career Exploration Scores are good indicators of your potential for success in further education, training, and in occupations. They focus on skills (which are learned and modifiable) rather than on abilities (which are often seen as fixed).</p>'
			};
			break;
		case "cesStandard":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<h4>Standard Scores</h4>' +
					'<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
			};
			break;
		case "cesPercent":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<h4>Percentile Scores</h4>' +
					'<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
			};
			break;
		case "scoreBands":
			modalOptions = {
				headerText : 'Score Bands',
				bodyText : '<p>On the ASVAB Summary Results sheet, the standard scores are shown in a graph with corresponding bands illustrating the range within which your scores would likely fall should you take the test again. You might want to focus your attention to any score bands that stand out (i.e., are located to the left or right of the other score bands). Such scores would suggest either a strength (to the right of the others) or a weakness (to the left of the others).</p>'
			};
			break;
		case "asvabStandard":
			modalOptions = {
				headerText : 'ASVAB Test Scores',
				bodyText : '<h4>Standard Scores</h4>' +
				'<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
			};
			break;
		case "asvabPercent":
			modalOptions = {
				headerText : 'ASVAB Test Scores',
				bodyText : '<h4>Percentile Scores</h4>' +
				'<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
			};
			break;
		case "afqt":
			modalOptions = {
				headerText : 'Military Entrance Score (AFQT)',
				bodyText : '<p>Participation in the ASVAB CEP does not obligate you to talk with military recruiters or consider a military career.</p>' + 
				'<p>The AFQT score is what the Military will use to determine enlistment eligibility.</p>' + 
				'<p>If you are interested in joining the Military it is best to discuss this score with a recruiter.</p>'
			};
			break;
		}
		
		// For ASVAB open CITM Composites Score page in new tab/window not modal win
		if (area == "asvab") {
			//$window.open('http://www.citm.com/CITM/composite-scores', '_blank');
            $window.open($scope.ssoUrl + 'composite-scores', '_blank');
		} else {
			modalService.showModal({
				size : 'md'
			}, modalOptions);
		}
	}
	/***
	 * Returns the Abbreviated Month with the corresponding month
	 * Last Updated: 2/4/2019 - Changed from comparing string to int for cases
	 */
	$scope.getAbbrMonth = function(monthStr) {		
		if (typeof monthStr === 'string') {
			monthStr = parseInt(monthStr);
		}
		switch (monthStr) {
			case 1: return "Jan"; 
			case 2: return "Feb";
			case 3: return "Mar"; 
			case 4: return "Apr";
			case 5: return "May";
			case 6: return "Jun";
			case 7: return "Jul"; 
			case 8: return "Aug";
			case 9: return "Sep";
			case 10: return "Oct";
			case 11: return "Nov"; 
			case 12: return "Dec";
			default:
				return "";
		}
	}

	if ($scope.haveAsvabTestScores) {
		// Set test full gender title
		$scope.genderFull = $scope.gender == "f" ? "Female" : "Male";
		// Set test date
		$scope.testDate.year = $scope.scoreSummary.controlInformation.administeredYear;
		$scope.testDate.day = $scope.scoreSummary.controlInformation.administeredDay;
		$scope.testDate.month = $scope.getAbbrMonth($scope.scoreSummary.controlInformation.administeredMonth);
	}

	// Score to plot mapping:
	// 11 - 20: -1 - 11
	// 21 - 30: 12 - 24
	// 31 - 40: 25 - 37
	// 41 - 50: 38 - 50
	// 51 - 60: 51 - 63
	// 61 - 70: 64 - 76
	// 71 - 80: 77 - 89
	// 81 - 90: 90 - 102
	function scoreInterpolation(score) {
		switch (true) {
			case score < 11:
				return -1;
			case 11 <= score && score <= 20:
				return Math.ceil((score - 11)/10*13 - 1);
			case 21 <= score && score <= 30:
				return Math.ceil((score - 21)/10*13 + 12);
			case 31 <= score && score <= 40:
				return Math.ceil((score - 31)/10*13 + 25);
			case 41 <= score && score <= 50:
				return Math.ceil((score - 41)/10*13 + 38);
			case 51 <= score && score <= 60:
				return Math.ceil((score - 51)/10*13 + 51);
			case 61 <= score && score <= 70:
				return Math.ceil((score - 61)/10*13 + 64);
			case 71 <= score && score <= 80:
				return Math.ceil((score - 71)/10*13 + 77);
			case 81 <= score && score <= 90:
				return Math.ceil((score - 81)/10*13 + 90);
			case score > 90:
				return 102;
		}
	}

	/**
	 * Print and download PDF.
	 */
	function createHTML() {
		/**
		 * CES Standard Scores Chart Data
		 */
		$scope.cesData1 = [
			$scope.getScore('verbal'),
			$scope.getScore('math'),
			$scope.getScore('science')
		]

		/**
		 * CES Percentile Scores
		 * display order female, male, all
		 * all others data use existing, e.g. asvabData2_1
		 */
		$scope.asvabData1 = [ // actual scores
			Number($scope.scoreSummary.generalScience.gs_SGS) > 0 ? Number($scope.scoreSummary.generalScience.gs_SGS) : 0, 
			Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) : 0, 
			Number($scope.scoreSummary.wordKnowledge.wk_SGS) > 0 ? Number($scope.scoreSummary.wordKnowledge.wk_SGS) : 0, 
			Number($scope.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? Number($scope.scoreSummary.paragraphComprehension.pc_SGS) : 0, 
			Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) : 0, 
			Number($scope.scoreSummary.electronicsInformation.ei_SGS) > 0 ? Number($scope.scoreSummary.electronicsInformation.ei_SGS) : 0, 
			Number($scope.scoreSummary.autoShopInformation.as_SGS) > 0 ? Number($scope.scoreSummary.autoShopInformation.as_SGS) : 0, 
			Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) : 0,
		]

		$scope.testDate.year = ($scope.haveAsvabTestScores && !$scope.scoreSummary.controlInformation || !$scope.scoreSummary.controlInformation.administeredYear) ? '' : $scope.scoreSummary.controlInformation.administeredYear;
		$scope.testDate.day = ($scope.haveAsvabTestScores && !$scope.scoreSummary.controlInformation || !$scope.scoreSummary.controlInformation.administeredDay) ? '' : $scope.scoreSummary.controlInformation.administeredDay;
		$scope.testDate.month = ($scope.haveAsvabTestScores && !$scope.scoreSummary.controlInformation || !$scope.scoreSummary.controlInformation.administeredMonth) ? '' : $scope.getAbbrMonth($scope.scoreSummary.controlInformation.administeredMonth);

		$scope.testSchool = {
			"name" : "",
			"city" : "",
			"state" : ""
		}
		$scope.testSchool.name = ($scope.haveAsvabTestScores && !$scope.scoreSummaryStudent.highSchoolInformation || !$scope.scoreSummaryStudent.highSchoolInformation.schoolSiteName) ? '' : $scope.scoreSummaryStudent.highSchoolInformation.schoolSiteName;
		$scope.testSchool.city = ($scope.haveAsvabTestScores && !$scope.scoreSummaryStudent.highSchoolInformation || !$scope.scoreSummaryStudent.highSchoolInformation.schoolCityName) ? '' : $scope.scoreSummaryStudent.highSchoolInformation.schoolCityName;
		$scope.testSchool.state = ($scope.haveAsvabTestScores && !$scope.scoreSummaryStudent.highSchoolInformation || !$scope.scoreSummaryStudent.highSchoolInformation.schoolStateName) ? '' : $scope.scoreSummaryStudent.highSchoolInformation.schoolStateName;

		$scope.educationLevel = (!$scope.scoreSummary.controlInformation || !$scope.scoreSummary.controlInformation.educationLevel) ? '' : $scope.scoreSummary.controlInformation.educationLevel;
		$scope.educationLevelGr = !$scope.educationLevel ? '' : $scope.scoreSummary.controlInformation.educationLevel + 'th Gr ';

		$scope.accessCodeExpiration = UtilityService.convertUTCDateToLocalDate(new Date($scope.scoreSummaryStudent.accessCodes.expireDate));
		
		// Top level element need some margin. Border should be more than 1px or else print/download won't show it correctly.
		// Adjust following attributes to have image showup when download or print:
		// * scoreHTML's top level table font-size.
		// * $scope.downloadPdf's image size. This determines how big the image is inside the pdf/print page.
		// * style.css' .extra-large-modal > .modal-dialog. This determines how big the modal is to fit the image to convert to pdf/print.
		var scoreHTML = '<div id="asr" class="asr" style="background-color: white">' +
			'<div class="print-box">' +
				'<div class="print-header">' +
					'<div class="print-header-info">' +
						'<strong>Student</strong>'+
						'<p>' + $scope.educationLevelGr + $scope.genderFull + '</p>'+
						'<p>Test Date: '+ $scope.testDate.month + ' ' + $scope.testDate.day + ', ' + $scope.testDate.year + '<br>'+
						UtilityService.capitalizeFirstLetter($scope.testSchool.name) + '<br>'+
						UtilityService.capitalizeFirstLetter($scope.testSchool.city) + '&nbsp' + $scope.testSchool.state +'</p>'+
					'</div>'+
					'<div class="print-header-title">' +
						'<h2><span>ASVAB</span> Summary Results</h2>'+
					'</div>'+
				'</div>'+
				'<div class="print-content">'+
					'<div class="print-content-main">' +
						'<div class="print-content-results">' +
							'<div class="print-content-row">' +
								'<div class="print-content-col print-content-col-1">' +
									'<div class="print-content-col-head">' +
										'<h3>ASVAB Results</h3>' +
									'</div>' +
									'<h5>Career Exploration Scores</h5>' +
									'<ul>' +
										'<li>Verbal Skills</li>' +
										'<li>Math Skills</li>' +
										'<li>Science and Technical Skills</li>' +
									'</ul>' +
									'<h5>ASVAB Tests <span class="print-dashed">' +
									'</span></h5>' +
									'<ul>' +
										'<li>General Science</li>' +
										'<li>Arithmetic Reasoning</li>' +
										'<li>Word Knowledge</li>' +
										'<li>Paragraph Comprehension</li>' +
										'<li>Mathematics Knowledge</li>' +
										'<li>Electronics Information</li>' +
										'<li>Auto and Shop Information</li>' +
										'<li>Mechanical Comprehension</li>' +
									'</ul>' +
									'<h5>Military Entrance Score (AFQT)&nbsp; ' + $scope.getScore('afqt') + '</h5>' +
								'</div>' +
								'<div class="print-content-col print-content-col-2">' +
									'<div class="print-content-col-head">' +
										'<h5>Percentile Scores</h5>' +
										'<div class="print-content-col-scores-wrap">' +
											'<div class="print-content-col-scores col-1">' +
												'<h6>' + $scope.educationLevel + 'th Grade Males</h6>' +
											'</div>' +
											'<div class="print-content-col-scores col-2">' +
												'<h6>' + $scope.educationLevel + 'th Grade Females</h6>' +
											'</div>' +
											'<div class="print-content-col-scores col-3">' +
												'<h6>' + $scope.educationLevel + 'th Grade Students</h6>' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<div class="print-content-col-scores-wrap">' +
										'<div class="print-content-col-scores col-1">' +
											'<h5>&nbsp;</h5>' +
											'<ul>' +
												'<li>' + $scope.cesData2_1[0][1] + '</li>' +
												'<li>' + $scope.cesData2_2[0][1] + '</li>' +
												'<li>' + $scope.cesData2_3[0][1] + '</li>' +
											'</ul>' +
											'<h5>&nbsp;</h5>' +
											'<ul>' +
												'<li>' + $scope.asvabData2_1[0][1] + '</li>' +
												'<li>' + $scope.asvabData2_2[0][1] + '</li>' +
												'<li>' + $scope.asvabData2_3[0][1] + '</li>' +
												'<li>' + $scope.asvabData2_4[0][1] + '</li>' +
												'<li>' + $scope.asvabData2_5[0][1] + '</li>' +
												'<li>' + $scope.asvabData2_6[0][1] + '</li>' +
												'<li>' + $scope.asvabData2_7[0][1] + '</li>' +
												'<li>' + $scope.asvabData2_8[0][1] + '</li>' +
											'</ul>' +
											'<h5>&nbsp;</h5>' +
										'</div>' +
										'<div class="print-content-col-scores col-2">' +
											'<h5>&nbsp;</h5>' +
											'<ul>' +
												'<li>' + $scope.cesData2_1[0][0] + '</li>' +
												'<li>' + $scope.cesData2_2[0][0] + '</li>' +
												'<li>' + $scope.cesData2_3[0][0] + '</li>' +
											'</ul>' +
											'<h5>&nbsp;</h5>' +
											'<ul>' +
												'<li>' + $scope.asvabData2_1[0][0] + '</li>' +
												'<li>' + $scope.asvabData2_2[0][0] + '</li>' +
												'<li>' + $scope.asvabData2_3[0][0] + '</li>' +
												'<li>' + $scope.asvabData2_4[0][0] + '</li>' +
												'<li>' + $scope.asvabData2_5[0][0] + '</li>' +
												'<li>' + $scope.asvabData2_6[0][0] + '</li>' +
												'<li>' + $scope.asvabData2_7[0][0] + '</li>' +
												'<li>' + $scope.asvabData2_8[0][0] + '</li>' +
											'</ul>' +
											'<h5>&nbsp;</h5>' +
										'</div>' +
										'<div class="print-content-col-scores col-3">' +
											'<h5>&nbsp;</h5>' +
											'<ul>' +
												'<li>' + $scope.cesData2_1[0][2] + '</li>' +
												'<li>' + $scope.cesData2_2[0][2] + '</li>' +
												'<li>' + $scope.cesData2_3[0][2] + '</li>' +
											'</ul>' +
											'<h5>&nbsp;</h5>' +
											'<ul>' +
												'<li>' + $scope.asvabData2_1[0][2] + '</li>' +
												'<li>' + $scope.asvabData2_2[0][2] + '</li>' +
												'<li>' + $scope.asvabData2_3[0][2] + '</li>' +
												'<li>' + $scope.asvabData2_4[0][2] + '</li>' +
												'<li>' + $scope.asvabData2_5[0][2] + '</li>' +
												'<li>' + $scope.asvabData2_6[0][2] + '</li>' +
												'<li>' + $scope.asvabData2_7[0][2] + '</li>' +
												'<li>' + $scope.asvabData2_8[0][2] + '</li>' +
											'</ul>' +
											'<h5>&nbsp;</h5>' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div class="print-content-col print-content-col-3">' +
									'<div class="print-content-col-head">' +
										'<h4>' + $scope.educationLevel + 'th Grade Standard Score Bands</h4>' +
									'</div>' +
									'<div class="print-content-col-bands">' +
										'<div class="print-content-col-range top">' +
											'<ul>' +
												'<li></li>' +
												'<li>20</li>' +
												'<li>30</li>' +
												'<li>40</li>' +
												'<li>50</li>' +
												'<li>60</li>' +
												'<li>70</li>' +
												'<li>80</li>' +
												'<li></li>' +
											'</ul>' +
										'</div>' +
										'<div class="print-content-col-range top">' +
											'<ul>' +
												'<span class="verticle-class"></span>' + 
												'<span class="verticle-class"></span>' + 
												'<span class="verticle-class"></span>' + 
												'<span class="verticle-class"></span>' + 
												'<span class="verticle-class"></span>' + 
												'<span class="verticle-class"></span>' + 
												'<span class="verticle-class"></span>' + 
											'</ul>' +
										'</div>' +
										'<h5>&nbsp;</h5>' +
										'<ul>' +
											'<li><span style="left: ' + scoreInterpolation($scope.cesData1[0]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.cesData1[1]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.cesData1[2]) + '%;">X</span></li>' +
										'</ul>' +
										'<h5>&nbsp;</h5>' +
										'<ul>' +
											'<li><span style="left: ' + scoreInterpolation($scope.asvabData1[0]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.asvabData1[1]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.asvabData1[2]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.asvabData1[3]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.asvabData1[4]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.asvabData1[5]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.asvabData1[6]) + '%;">X</span></li>' +
											'<li><span style="left: ' + scoreInterpolation($scope.asvabData1[7]) + '%;">X</span></li>' +
										'</ul>' +
										'<h5>&nbsp;</h5>' +
										'<div class="print-content-col-range bottom">' +
											'<ul>' +
												'<li></li>' +
												'<li>20</li>' +
												'<li>30</li>' +
												'<li>40</li>' +
												'<li>50</li>' +
												'<li>60</li>' +
												'<li>70</li>' +
												'<li>80</li>' +
												'<li></li>' +
											'</ul>' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div class="print-content-col print-content-col-4">' +
									'<div class="print-content-col-head">' +
										'<h6>' + $scope.educationLevel + 'th Grade Standard Score</h6>' +
									'</div>' +
									'<h5>&nbsp;</h5>' +
									'<ul>' +
										'<li>' + $scope.cesData1[0] + '</li>' +
										'<li>' + $scope.cesData1[1] + '</li>' +
										'<li>' + $scope.cesData1[2] + '</li>' +
									'</ul>' +
									'<h5>&nbsp;</h5>' +
									'<ul>' +
										'<li>' + $scope.asvabData1[0] + '</li>' +
										'<li>' + $scope.asvabData1[1] + '</li>' +
										'<li>' + $scope.asvabData1[2] + '</li>' +
										'<li>' + $scope.asvabData1[3]+ '</li>' +
										'<li>' + $scope.asvabData1[4] + '</li>' +
										'<li>' + $scope.asvabData1[5]+ '</li>' +
										'<li>' + $scope.asvabData1[6] + '</li>' +
										'<li>' + $scope.asvabData1[7] + '</li>' +
									'</ul>' +
									'<h5>&nbsp;</h5>' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<div class="print-content-widget">' +
							'<h5 class="print-content-title">Explanation Of Your <br>ASVAB Percentile Scores</h5>' +
							'<p class="print-content-text">Your ASVAB results are reported as percentile scores in the three highlighted' +
								'columns to the left of the graph. Percentile scores show how you compare to other students - males and' +
								'females, and for all students - in your grade. For example, a percentile score of 65 foran 11th grade' +
								'female would mean she scored the same or better than 65 out of every 100 females in the 11th grade.</p>' +
							'<p class="print-content-text">For purposes of career planning, knowing your relative standing in these' +
								'comparison groups is important.Being male or female does not limit your career or educational choices.' +
								'There are noticeable differences in how men and women score in some areas. Viewing your scores in light of' +
								'your relative standing both to men and women may encourage you to explore areas that you might otherwise' +
								'overlook.</p>' +
							'<p class="print-content-text">You can use the <strong>Career Exploration Scores</strong> to evaluate your' +
								'knowledge and skills in three general areas (Verbal, Math, and Science and Technical Skills). You can use' +
								'the <strong>ASVAB Test Scores</strong> to gather information on specific skill areas. <i>Together, these' +
									'scores provide a snapshot of your current knowledge and skills.</i> This information will help you' +
								'develop and review your career goals and plans.</p>' +
							'<h5 class="print-content-title">Explanation Of Your <br>ASVAB Standard Scores</h5>' +
							'<p class="print-content-text">Your ASVAB results are reported as standard scores in the above graph.Your' +
								'score on each test is identified by the "<strong>X</strong>" in the corresponding bar graph. You should' +
								'view these scores as estimates of yourtrue skill level in that area. If you took the test again, you' +
								'probably would receive a somewhat different score. Many things, such as how you were feeling during' +
								'testing, contribute to this difference. This difference is shown with gray score bands in the graph of' +
								'your results. Your standard scores are based on the ASVAB tests and composites based onyour grade level.' +
							'</p>' +
							'<p class="print-content-text">The score bands provide a way to identify some of your strengths. Overlapping' +
								'score bands mean yourtrue skill level is similarin both areas, so the real difference between specific' +
								'scores might not be meaningful. If the score bands do not overlap, you probably are stronger in the area' +
								'that has the higher score band.</p>' +
							'<p class="print-content-text">The ASVAB is an aptitude test. It is neither an absolute measure of your' +
								'skills and abilities nor a perfect predictor of your success or failure. A high score does not guarantee' +
								'success, and a low score does not guarantee failure, in a future educational program or occupation. For' +
								'example, if you have never worked with shop equipment or cars, you may not be familiar with the terms and' +
								'concepts assessed by the Auto and Shop Information test. Taking a course or obtaining a part-time job in' +
								'this area would increase your knowledge and improve your score if you were to take it again.</p>' +
							'<h5 class="print-content-title">Using ASVAB Results in <br>Career Exploration</h5>' +
							'<p class="print-content-text">Your career and educational plans may change over time as you gain more' +
								'experience and learn more about your interests. <i>Exploring Careers: The ASVAB Career Exploration' +
									'Guide</i> can help you learn more about yourself and the world of work,to identify and explore potential' +
								'goals, and develop an effective strategy to realize your goals. The Guide will help you identify occupa-' +
								'tions in line with your interests and skills. As you explore potentially satisfying careers, you will' +
								'develop your career exploration and planning skills.</p>' +
							'<p class="print-content-text">Meanwhile, your ASVAB results can help you in making well-informed choices' +
								'about future high school courses.</p>' +
							'<p class="print-content-text">We encourage you to discuss your ASVAB re- sults with a teacher, counselor,' +
								'parent, family member or other interested adult. These individuals can help you to view your ASVAB results' +
								'in light of other important information, such as your interests, school grades, motivation, and personal' +
								'goals.</p>' +
						'</div>' +
					'</div>' +
					'<div class="print-content-sidebar">' +
						'<div class="print-content-sidebar-top">' +
							'<div class="print-content-sidebar-widget">' +
								'<h5 class="print-content-title">Use Of Information</h5>' +
								'<p class="print-content-text">Personal identity information (name, so- cial security number, street' +
									'address, and telephonenumber) and test scores will not be released to any agency outside of the' +
									'Department of Defense (DoD), the Armed Forces, the Coast Guard, and your school. Your school or local' +
									'school system can determine any further release of informa- tion. The DoD will use your scores for' +
									'recruiting and research purposes for up to two years.After that the information will beused by the DoD' +
									'for research purposes only.</p>' +
							'</div>' +
							'<div class="print-content-sidebar-widget">' +
								'<h5 class="print-content-title">Military <br>Entrance&nbspScores</h5>' +
								'<p class="print-content-text">The <strong>Military Entrance Score</strong> (also called AFQT, which stands' +
									'for the Armed Forces Qualification Test) is the score used to determine your qualifications for entry' +
									'into any branch of the United States Armed Forces orthe Coast Guard. The <strong>Military Entrance' +
									' Score</strong> predicts in a general way how well you might do in training and on the job in military' +
									'occupations.Your score reflects your standing compared to Ameri- can men and women 18 to 23 years of' +
									'age.</p>' +
							'</div>' +
						'</div>' +
						'<div class="print-content-sidebar-bottom">' +
							'<p style="padding-right: 10px">Use Access Code: ' + $scope.scoreSummaryStudent.accessCodes.accessCode + '</p>' +
							'<p>(for online Occu-Find and FYI)</p>' +
							'<p style="padding-right: 10px">Access code expires: ' + $scope.getAbbrMonth($scope.accessCodeExpiration.getMonth()+1) + ' ' + $scope.accessCodeExpiration.getDate() + ', ' + $scope.accessCodeExpiration.getFullYear() + '</p>' +
							'<div class="print-access">Explore career possibilities by using your Access Code at</div>' +
							'<div class="print-site-link"><i><a href="//www.asvabprogram.com">www.asvabprogram.com</a></i></div>' +
							'<div class="print-information"><i>See Your Counselor For Further Information</i></div>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' +
			'<div class="print-footer">Dd Form 1304-5, Jul 05 - Previous Editions Of This Form Are Obsolete</div>'+
		'</div>'

		return scoreHTML;
	}

	function getCanvas() {
		var w = $("#asr")[0].ownerDocument.defaultView.innerWidth;
		$("#asr")[0].ownerDocument.defaultView.innerWidth = $("#asr").width()*1.3;
		var defer = $q.defer();
		var t = html2canvas( $("#asr"), {
			imageTimeout : 2000,
			removeContainer : true,
			backgroundColor: "rgba(0,0,0,0)"
		}).then(function(canvas) {
			// $("#asr")[0].ownerDocument.defaultView.innerWidth = w;
			var img = canvas.toDataURL("image/jpeg", 1);
			defer.resolve(img);
		}, function(error) {
			defer.reject(error);
		});

		return defer.promise;
	}

	$scope.print = function() {
		FYIRestFactory.getStudentScoreResults().then(function (data) {
			$scope.scoreSummaryStudent = data.data;

			var modalOptions = {
				headerText : 'Creating ASR...',
				bodyText : createHTML(),
				disabledSeconds: navigator.platform.toLowerCase().indexOf('win') < 0 ? 2 : 8,
				selfClose: true,
			}
			
			modalService.showModal({
				size: 'lg',
				windowClass: 'asr-modal',
			}, modalOptions);
	
			$timeout(function() {
				// IE, Edge, and Safari support
				if (navigator.appName == 'Microsoft Internet Explorer' 
					|| !!(navigator.userAgent.match(/Trident/) 
					|| navigator.userAgent.match(/rv 11/))
					|| (!!navigator.userAgent.match(/Safari/) && !navigator.userAgent.match(/Chrome/))
					|| !!navigator.userAgent.match(/Edge/)) {
					printIE(getCanvas());
					return false;
				}
	
				getCanvas().then(function(canvas) {
					var docDefinition = {
						pageOrientation: 'landscape',
						pageSize: 'A2',
						content : [{
							image : canvas,
							alignment: 'center',
							width : 1300,
						}]
					};
		
					pdfMake.createPdf(docDefinition).print();
				}, function(error) {
					console.log(error);
					alert(error);
				});
	
			}, 500) // Need timeout for modal content to load before get element ID on document
		});
	};
	

	/**
	 * IE support for printing element.
	 */
	function printIE(canvas) {
		canvas.then(function(img){
			var popup = window.open();
			popup.document.write('<img src=' + img + '>');
			popup.document.close();
			popup.focus();
			popup.print();			
		}, function(error){
			
			alert('Printing failed.');
		});
	}

	$scope.downloadPdf = function() {
		FYIRestFactory.getStudentScoreResults().then(function (data) {
			$scope.scoreSummaryStudent = data.data;

			var modalOptions = {
				headerText : 'Creating ASR...',
				bodyText : createHTML(),
				disabledSeconds: navigator.platform.toLowerCase().indexOf('win') < 0 ? 2 : 8,
				selfClose: true,
			}
			
			modalService.showModal({
				size: 'lg',
				windowClass: 'asr-modal',
			}, modalOptions);

			$timeout(function() {
				getCanvas().then(function(canvas) {
					var docDefinition = {
						pageOrientation: 'landscape',
						pageSize: 'A2',
						content : [{
							image : canvas,
							alignment: 'center',
							width : 1300,
						}]
					};
		
					pdfMake.createPdf(docDefinition).download('ASR.pdf');
				}, function(error) {
					console.log(error);
					alert(error);
				});

			}, 500) // Need timeout for modal content to load before get element ID on document
		})
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
