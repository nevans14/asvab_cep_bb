cepApp.controller('FindYourInterestTestController', [ '$scope', '$http', '$location', 'FYIScoreService', 'FYIRestFactory', 'appUrl', '$q', 'questions', '$rootScope', 'modalService', '$window', function($scope, $http, $location, FYIScoreService, FYIRestFactory, appUrl, $q, questions, $rootScope, modalService, $window) {

	$scope.validation = undefined;
	$scope.gender = "";
	$scope.submitted = false;
	$scope.disableSubmit = false;

	(function getQuestionList() {
		const savedFYIQuestions = $window.sessionStorage.getItem('FYIQuestions');
		$scope.gender = $window.sessionStorage.getItem('gender');

		if (savedFYIQuestions) {
			$scope.questionList = JSON.parse(savedFYIQuestions);
		} else {
			$scope.questionList = questions[0].data;
			// $scope.questionList = questions[0].data.slice(0,3);  // for testing
			$window.sessionStorage.setItem('FYIQuestions', JSON.stringify($scope.questionList));
		}
	})();

	$scope.resetValidation = function() {
		$scope.validation = undefined;
	}

	$scope.saveFYIQustions = function() {
		$window.sessionStorage.removeItem("FYIQuestions");
		$window.sessionStorage.removeItem("gender");
		$window.sessionStorage.setItem('FYIQuestions', JSON.stringify($scope.questionList));
		if ($scope.gender) {
			$window.sessionStorage.setItem('gender', $scope.gender);
		}
	}

	$scope.calculateThreshold = function () {
		var scoredItems = 0;
		for (var i = 0; i < $scope.questionList.length; i++) {
			if (!$scope.questionList[i].isSeeded) {
				scoredItems++;
			}
		}
		// scored items * 90% / 100 = threshold
		$scope.threshold = scoredItems * 90 / 100;
		// $scope.threshold = 100;  // for testing
	}

	$scope.calculateThreshold();
	$scope.submitAnswer = function() {

		$scope.submitted = true;
		$scope.disableSubmit = true;

		// this will be used for post call
		var fyiObject = {
			gender : $scope.gender,
			testId : undefined,
			rawScores : undefined,
			responses : undefined,
		};

		// set complete questionAnswerList in service
		FYIScoreService.questionAnswerList = $scope.questionList;

		// form validation
		var len = $scope.questionList.length;
		for (var i = 0; i < len; i++) {
			if ($scope.questionList[i].answer === undefined) {
				$scope.validation = 'Please answer all questions';
				$scope.disableSubmit = false;
				return false;
			}
		}

		if (!$scope.gender) {
			$scope.validation = 'Please select gender';
			$scope.disableSubmit = false;
			return false;
		}

		// reset values
		FYIScoreService.resetValues();
		
		// calculate score
		FYIScoreService.calculateRawScore();

		var likes = FYIScoreService.getInterestLikes();
		var indifferents = FYIScoreService.getInterestIndifferents();
		var dislikes = FYIScoreService.getInterestDislikes();

		// if 81 or more of the same responses alert user to vary response
		if (likes >= $scope.threshold || indifferents >= $scope.threshold || dislikes >= $scope.threshold) {

			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : '<p><b>Your response did not show a sufficient pattern of likes and dislikes for us to score your responses and provide you with your three Top Interest Codes.</b></p>' + '<p><b>Try retaking the inventory and using more varied responses.</b></p>' + '<ul><li>If you used a lot of Likes. try being more selective.</li><li>If you used a lot of Dislike, try being less selective.</li><li>If you used a lot of Indifferents, move off the fence</li></ul>' + '<hr>' + '<b>Tips for retaking the FYI:</b>' + '<ul><li>Read each interest item carefully before responding.</li><li>Make sure that your responses accurately reflect your interests.</li>' + '<li>Think about hobbies that you enjoy and relate them to the items in the FYI.</li><li>Keep in mind that you are not selecting an occupation. You are selecting activities that you either like, dislike, or are indifferent to doing</li></ul>'
			};

			modalService.showModal({}, modalOptions);
			$scope.disableSubmit = false;
			return false;
		}

		var calcScore = FYIScoreService.getInterestCodeRawScore();

		fyiObject.rawScores = [ {
			interestCd : 'R',
			rawScore : calcScore.rRawScore
		}, {
			interestCd : 'I',
			rawScore : calcScore.iRawScore
		}, {
			interestCd : 'A',
			rawScore : calcScore.aRawScore
		}, {
			interestCd : 'S',
			rawScore : calcScore.sRawScore
		}, {
			interestCd : 'E',
			rawScore : calcScore.eRawScore
		}, {
			interestCd : 'C',
			rawScore : calcScore.cRawScore
		} ];

		/*
		 * $scope.interestCodeRawScore =
		 * FYIScoreService.getInterestCodeRawScore();
		 * 
		 * var genderRest =
		 * FYIRestFactory.getGenderScore(angular.toJson($scope.interestCodeRawScore)).then(function
		 * successCallback(response) { FYIScoreService.genderScore =
		 * response.data; }, function errorCallback(response) { alert(response); })
		 * 
		 * var combinedRest =
		 * FYIRestFactory.getCombinedScore(angular.toJson($scope.interestCodeRawScore)).then(function
		 * successCallback(response) { FYIScoreService.combinedScore =
		 * response.data; }, function errorCallback(response) { alert(response); })
		 */
		var answers = [];
		// copy the original question list to temp variable
		angular.copy($scope.questionList, answers);
		// delete properties not being used anymore before posting data
		for (var i = 0; i < answers.length; i++) {
			delete answers[i].stem;
		}

		FYIRestFactory.saveTest().then(function(success) {
			fyiObject.testId = success.data;
			// angular.toJson removes the $$hashKey from serialized array
			fyiObject.responses = angular.toJson(answers);

			FYIScoreService.setFyiObject(fyiObject);
			$location.path('/find-your-interest-score');
		}, function errorCallback(response) {
			$scope.validation = response && response.status 
				&& response.status >= 400 && response.status < 500 
				? 'Error: Bad request' 
				: 'Error: Server Error';
			$scope.disableSubmit = false;
		});
	};

	// show or hide buttons depending if question object is populated
	$scope.button = function() {
		if ($scope.questionList !== undefined) {
			return true;
		} else {
			return false;
		}
	};

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
