cepApp.controller('FindYourInterestScoreSharedController', [ '$route', '$rootScope', '$scope', 'modalService', 'UtilityService', function($route, $rootScope, $scope, modalService, UtilityService) {
	// helper functions
	$scope.getCodeName = function (code) {
		switch (code) {
			case 'R' : return 'Realistic';
			case 'I' : return 'Investigative';
			case 'A' : return 'Artistic';
			case 'S' : return 'Social';
			case 'E' : return 'Enterprising';
			case 'C' : return 'Conventional';
			default:
				return '';
		}
	}
	
	$scope.getCodeDefinition = function(code) {
		switch (code) {
			case 'R' : return 'activities that often involve practical, hands-on problems and solutions';
			case 'I' : return 'activities that involve learning about new subject areas or ideas and allows me to use my knowledge to solve problems';
			case 'A' : return 'activities that allows me to be creative and use my imagination to do original work';
			case 'S' : return 'activities that allows me to use my skills and talents to interact effectively with others';
			case 'E' : return 'activities that allows me to take a leadership role';
			case 'C' : return 'activities that require attention to accuracy and detail';
			default:
				return '';
		}
	}
	// canonical url
	$scope.absUrl = UtilityService.getCanonicalUrl();
	$scope.codeOne = $route.current.params.iCodeOne;
	$scope.codeTwo = $route.current.params.iCodeTwo;
	$scope.isUserLoggedIn = ($rootScope.globals.currentUser !== undefined ? true : false);
	$scope.description = undefined;
	// sets up the image
	var topScores = $scope.codeOne + $scope.codeTwo;	
	$scope.imageUrl = 'https://asvabprogram.com/media-center-content/thumbnails/social-share/fyi-score/FYI_facebook_' + topScores + '.jpg';
	$scope.description = "My interests are " + 
		$scope.getCodeName($scope.codeOne) + 
		" and " + 
		$scope.getCodeName($scope.codeTwo) + 
		" which means I like " + 
		$scope.getCodeDefinition($scope.codeOne) + 
		" and " + 
		$scope.getCodeDefinition($scope.codeTwo) + ".";
	
	$scope.interestCodeInfo = function(interestCode) {
		var modalOptions;
		switch (interestCode) {
		case "R":
			modalOptions = {
				headerText : 'Realistic',
				bodyText : '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
				'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
				'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
			};
			break;
		case "I":
			modalOptions = {
				headerText : 'Investigative',
				bodyText : '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
				'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
				'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
			};
			break;
		case "A":
			modalOptions = {
				headerText : 'Artistic',
				bodyText : '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
				'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
				'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
			};
			break;
		case "S":
			modalOptions = {
				headerText : 'Social',
				bodyText : '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
				'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
				'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
			};
			break;
		case "E":
			modalOptions = {
				headerText : 'Enterprising',
				bodyText : '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
				'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
				'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
			};
			break;
		case "C":
			modalOptions = {
				headerText : 'Conventional',
				bodyText : '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
				'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
				'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
			};
			break;
		}
		

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}
}]);
