cepApp.controller('FindYourInterestTiedScoreController', [ '$scope', 'FYIScoreService', 'FYIRestFactory', '$rootScope', '$window', '$location', 'modalService', function($scope, FYIScoreService, FYIRestFactory, $rootScope, $window, $location, modalService) {

	$scope.tiedScoreObject = FYIScoreService.getTiedScoreObject();

	/**
	 * Keep track and limit number of selections that can be made.
	 */
	$scope.limit = $scope.tiedScoreObject.numSlotsLeft;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Submit score selection.
	 */
	$scope.submit = function() {
		var topInterestCodes = [];
		var scoreList = $scope.tiedScoreObject.scoreList;

		// determine the top three interest scores
		var numSelection = 0;
		for (var i = 0; i < scoreList.length; i++) {
			if (topInterestCodes.length < 3) {
				if (scoreList[i].checkbox != true) {
					topInterestCodes.push(scoreList[i]);
				} else {
					if (scoreList[i].selected == true) {
						topInterestCodes.push(scoreList[i]);
						numSelection++;
					}
				}
			}
		}

		// validate selections are made
		if (numSelection != $scope.limit) {

			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : 'Select ' + $scope.tiedScoreObject.numSlotsLeft + ' of the ' + $scope.tiedScoreObject.numTiedScore + ' tied interests.'
			};

			modalService.showModal({}, modalOptions);
			return false;
		}

		// setup score object to post
		var scoreObject = {
			scoreChoice : $scope.scoreSelected,
			interestCodeOne : topInterestCodes[0].interestCd,
			interestCodeTwo : topInterestCodes[1].interestCd,
			interestCodeThree : topInterestCodes[2].interestCd
		}

		// post user score selection
		FYIRestFactory.postUserScores(angular.toJson(scoreObject)).then(function successCallback(response) {

			// erase session storage for interest codes
			$window.sessionStorage.removeItem("interestCodeOne");
			$window.sessionStorage.removeItem("interestCodeTwo");
			$window.sessionStorage.removeItem("interestCodeThree");

			// reset selected property
			var i = 0;
			while (i < $scope.tiedScoreObject.scoreList.length) {
				$scope.tiedScoreObject.scoreList[i].selected = false;
				i++;
			}

			$location.path('/dashboard');
		}, function errorCallback(response) {
			
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'Failed to save test scores. Try submitting again. If Error continues, please inform site administrator.'
			};

			modalService.showModal({}, modalOptions);
			return false;
			
		});
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
