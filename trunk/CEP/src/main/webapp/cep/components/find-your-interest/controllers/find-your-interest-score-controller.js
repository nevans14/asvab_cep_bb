cepApp.controller('FindYourInterestScoreController', [ '$route', 'resource', '$rootScope', '$scope', '$http', '$location', 'FYIScoreService', 'CombinedAndGenderScores', 'FYIRestFactory', '$window', 'modalService', 'scoreChoice', 'UserFactory', 'accessCode', function($route, resource, $rootScope, $scope, $http, $location, FYIScoreService, CombinedAndGenderScores, FYIRestFactory, $window, modalService, scoreChoice, UserFactory, accessCode) {
	
	$scope.accessCode = accessCode;
	$scope.scores = CombinedAndGenderScores[0].data;
	var topCombinedInterestCodes = FYIScoreService.getTopCombinedInterestCodes();
	var topGenderInterestCodes = FYIScoreService.getTopGenderInterestCodes();
	$scope.numberTestTaken = CombinedAndGenderScores[1].data;
	$scope.scoreChoice = scoreChoice.data.scoreChoice;
	$window.sessionStorage.setItem('numberTestTaken', $scope.numberTestTaken);	
	$scope.baseUrl = resource;
	$scope.domainUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port();
	$scope.topScores = [];
	
	$scope.limit = 2; // by default, the limit is two fyi tests
	$scope.numberOfTestsLeft = 2;
	$scope.hasReachedLimit = false;
	$scope.isAbleToRetakeTest = false;

	if($scope.accessCode.unlimitedFyi === 1) {
		$scope.isAbleToRetakeTest = true;
	} else {
		if ($scope.numberTestTaken < $scope.limit) {
			$scope.isAbleToRetakeTest = true;
			$scope.numberOfTestsLeft = ($scope.limit - $scope.numberTestTaken);
		} else {
			$scope.hasReachedLimit = true;
		}
	}
	
	$scope.isUnlimitedFyi = function () {
		return $scope.accessCode.unlimitedFyi === 1 ? true : false;
	}

	// If score choice is undefined then default to combined.
	$scope.scoreSelected = $scope.scoreChoice == undefined ? 'combined' : $scope.scoreChoice;

	// If score selection changes then update FYI scores asynchronously.
	$scope.$watch('scoreSelected', function(newValue, oldValue) {
		if ($scope.scoreSelected != undefined) {
			// Setup score object.
			var scoreObject = {
				scoreChoice : $scope.scoreSelected,
				interestCodeOne : $scope.scoreSelected == 'gender' ? $scope.genderScoreSort[0].interestCd : $scope.combinedScoreSort[0].interestCd,
				interestCodeTwo : $scope.scoreSelected == 'gender' ? $scope.genderScoreSort[1].interestCd : $scope.combinedScoreSort[1].interestCd,
				interestCodeThree : $scope.scoreSelected == 'gender' ? $scope.genderScoreSort[2].interestCd : $scope.combinedScoreSort[2].interestCd
			}

			// Rest call to post users FYI scores.
			FYIRestFactory.postUserScores(angular.toJson(scoreObject)).then(function successCallback(response) {

				// Remove session storage for interest codes.
				$window.sessionStorage.removeItem("interestCodeOne");
				$window.sessionStorage.removeItem("interestCodeTwo");
				$window.sessionStorage.removeItem("interestCodeThree");
				
				// Reset values
				FYIScoreService.setCombinedTiedScoreObject(undefined);
				FYIScoreService.setGenderTiedScoreObject(undefined);
				$window.sessionStorage.removeItem("combinedTiedScoreObject");
				$window.sessionStorage.removeItem("genderTiedScoreObject");

				// Enables going back to the tied break page if user returns to
				// FYI page.
				/*FYIScoreService.setTopCombinedInterestCodes(undefined);
				FYIScoreService.setTopGenderInterestCodes(undefined);*/
				
				// Determines which section to highlight using CSS.
				$scope.myClassCombined = $scope.scoreSelected == 'combined' ? {
					selected : true
				} : {
					selected : false
				};
				$scope.myClassGender = $scope.scoreSelected == 'gender' ? {
					selected : true
				} : {
					selected : false
				};

				
				UserFactory.getUserInterestCodes().then(function(response) {
					// Update left navigation interest codes.
					$rootScope.$emit("updateInterestCodes", {});
				}, function(error){
				});
				
				
				

			}, function errorCallback(response) {
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'Failed to save test scores. Try submitting again. If Error continues, please inform site administrator.'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
			});
		}
	});
	
	
	
	// Determines which section to highlight using CSS.
	$scope.myClassCombined = $scope.scoreSelected == 'combined' ? {
		selected : true
	} : {
		selected : false
	};
	$scope.myClassGender = $scope.scoreSelected == 'gender' ? {
		selected : true
	} : {
		selected : false
	};


	// sort gender score list in desc order
	var genderScoreSort = [];
	for (var i = 0; i < $scope.scores.length; i++) {
		genderScoreSort.push($scope.scores[i]);
	}
	genderScoreSort.sort(function(a, b) {
		return b.genderScore - a.genderScore
	});

	// When topCombinedInterestCodes is undefined, this means there are no tied
	// scores for combined.
	if (topGenderInterestCodes == undefined) {
		$scope.genderScoreSort = genderScoreSort;
	} else {

		// There are tied combined scores so need to order scores depending on
		// user's selections.
		var tempArray = [];

		// Use the user's tied score selection and move it to the top 3.
		for (var i = 0; i < topGenderInterestCodes.length; i++) {
			for (var f = 0; f < genderScoreSort.length; f++) {
				if (topGenderInterestCodes[i].interestCd == genderScoreSort[f].interestCd) {
					tempArray.push(genderScoreSort[f]);
					break;
				}
			}
		}

		// All other scores will be added to the bottom three.
		for (var f = 0; f < genderScoreSort.length; f++) {
			var isMatch = false;
			for (var i = 0; i < topGenderInterestCodes.length; i++) {
				if (topGenderInterestCodes[i].interestCd == genderScoreSort[f].interestCd) {
					isMatch = true;
					break;
				}
			}

			if (!isMatch) {
				tempArray.push(genderScoreSort[f]);
			}
		}

		$scope.genderScoreSort = tempArray;
	}

	// Setup message for social media sharing.
	$scope.genderShareText = "\n\nGender Score:";
	$scope.genderShareTextEmail = '%0D%0A %0D%0AGender Score:';
	for (var i = 0; i < $scope.genderScoreSort.length; i++) {
		$scope.genderShareText = $scope.genderShareText + '\n' + ($scope.genderScoreSort[i].interestCd == 'C' ? 'Conventional' : $scope.genderScoreSort[i].interestCd == 'S' ? 'Social' : $scope.genderScoreSort[i].interestCd == 'E' ? 'Enterprising' : $scope.genderScoreSort[i].interestCd == 'A' ? 'Artistic' : $scope.genderScoreSort[i].interestCd == 'R' ? 'Realistic' : $scope.genderScoreSort[i].interestCd == 'I' ? 'Investigative' : $scope.genderScoreSort[i].interestCd) + ": " + Math.floor($scope.genderScoreSort[i].genderScore * 100);
		$scope.genderShareTextEmail = $scope.genderShareTextEmail + '%0D%0A' + ($scope.genderScoreSort[i].interestCd == 'C' ? 'Conventional' : $scope.genderScoreSort[i].interestCd == 'S' ? 'Social' : $scope.genderScoreSort[i].interestCd == 'E' ? 'Enterprising' : $scope.genderScoreSort[i].interestCd == 'A' ? 'Artistic' : $scope.genderScoreSort[i].interestCd == 'R' ? 'Realistic' : $scope.genderScoreSort[i].interestCd == 'I' ? 'Investigative' : $scope.genderScoreSort[i].interestCd) + ": " + Math.floor($scope.genderScoreSort[i].genderScore * 100);
	}

	// sort combined score list in desc order
	var combinedScoreSort = [];
	for (var i = 0; i < $scope.scores.length; i++) {
		combinedScoreSort.push($scope.scores[i]);
	}
	combinedScoreSort.sort(function(a, b) {
		return b.combinedScore - a.combinedScore
	});

	// When topCombinedInterestCodes is undefined, this means there are no tied
	// scores for combined.
	if (topCombinedInterestCodes == undefined) {
		$scope.combinedScoreSort = combinedScoreSort;
	} else {

		// There are tied combined scores so need to order scores depending on
		// user's selections.
		var tempArray = [];

		// Use the user's tied score selection and move it to the top 3.
		for (var i = 0; i < topCombinedInterestCodes.length; i++) {
			for (var f = 0; f < combinedScoreSort.length; f++) {
				if (topCombinedInterestCodes[i].interestCd == combinedScoreSort[f].interestCd) {
					tempArray.push(combinedScoreSort[f]);
					break;
				}
			}
		}

		// All other scores will be added to the bottom three.
		for (var f = 0; f < combinedScoreSort.length; f++) {
			var isMatch = false;
			for (var i = 0; i < topCombinedInterestCodes.length; i++) {
				if (topCombinedInterestCodes[i].interestCd == combinedScoreSort[f].interestCd) {
					isMatch = true;
					break;
				}
			}

			if (!isMatch) {
				tempArray.push(combinedScoreSort[f]);
			}
		}

		$scope.combinedScoreSort = tempArray;
	}

	// Setup message for social media sharing.
	$scope.combinedShareText = "\n\nCombined Score:";
	$scope.combinedShareTextEmail = '%0D%0A %0D%0ACombined Score:';
	for (var i = 0; i < $scope.combinedScoreSort.length; i++) {
		$scope.combinedShareText = $scope.combinedShareText + '\n' + ($scope.combinedScoreSort[i].interestCd == 'C' ? 'Conventional' : $scope.combinedScoreSort[i].interestCd == 'S' ? 'Social' : $scope.combinedScoreSort[i].interestCd == 'E' ? 'Enterprising' : $scope.combinedScoreSort[i].interestCd == 'A' ? 'Artistic' : $scope.combinedScoreSort[i].interestCd == 'R' ? 'Realistic' : $scope.combinedScoreSort[i].interestCd == 'I' ? 'Investigative' : $scope.combinedScoreSort[i].interestCd) + ": " + Math.floor($scope.combinedScoreSort[i].combinedScore * 100);
		$scope.combinedShareTextEmail = $scope.combinedShareTextEmail + '%0D%0A' + ($scope.combinedScoreSort[i].interestCd == 'C' ? 'Conventional' : $scope.combinedScoreSort[i].interestCd == 'S' ? 'Social' : $scope.combinedScoreSort[i].interestCd == 'E' ? 'Enterprising' : $scope.combinedScoreSort[i].interestCd == 'A' ? 'Artistic' : $scope.combinedScoreSort[i].interestCd == 'R' ? 'Realistic' : $scope.combinedScoreSort[i].interestCd == 'I' ? 'Investigative' : $scope.combinedScoreSort[i].interestCd) + ": " + Math.floor($scope.combinedScoreSort[i].combinedScore * 100);
	}
	$scope.combinedShareText = $scope.combinedShareText + "\n\n";
	
	// for sharing scores
	$scope.combinedShareUrl = $scope.domainUrl + '/find-your-interest-score-shared/' + $scope.combinedScoreSort[0].interestCd + '/' + $scope.combinedScoreSort[1].interestCd;
	$scope.genderShareUrl = $scope.domainUrl + '/find-your-interest-score-shared/' + $scope.genderScoreSort[0].interestCd + '/' + $scope.genderScoreSort[1].interestCd;
	/**
	 * Gender information popup.
	 */
	$scope.genderInfoModal = function() {
		var modalOptions = {
			headerText : 'Gender Specific',
			bodyText : 'Growing up, males and females get different messages from their parents, schools, and the media about what careers are appropriate for them, resulting in exposure to different experiences. Three interest areas, Realistic, Artistic, and Social, tend to have significant difference in how males and females respond.  Gender-Specific results show your interests as they compare to those in your gender, and the scores may offer you a different set of careers to explore.'
		};

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}

	$scope.interestCodeInfo = function(interestCode) {
		var modalOptions;
		switch (interestCode) {
		case "R":
			modalOptions = {
				headerText : 'Realistic',
				bodyText : '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
				'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
				'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
			};
			break;
		case "I":
			modalOptions = {
				headerText : 'Investigative',
				bodyText : '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
				'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
				'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
			};
			break;
		case "A":
			modalOptions = {
				headerText : 'Artistic',
				bodyText : '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
				'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
				'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
			};
			break;
		case "S":
			modalOptions = {
				headerText : 'Social',
				bodyText : '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
				'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
				'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
			};
			break;
		case "E":
			modalOptions = {
				headerText : 'Enterprising',
				bodyText : '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
				'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
				'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
			};
			break;
		case "C":
			modalOptions = {
				headerText : 'Conventional',
				bodyText : '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
				'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
				'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
			};
			break;
		}
		

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
} ]);
