cepApp.service('FYIScoreService', [ '$window', function($window) {

	var topCombinedInterestCodes;
	this.getTopCombinedInterestCodes = function() {
		if (topCombinedInterestCodes === undefined) {
			if (typeof (Storage) !== "undefined") {
				if ($window.sessionStorage.getItem("topCombinedInterestCodes") === null || $window.sessionStorage.getItem("topCombinedInterestCodes") === 'undefined') {
					// TODO: redirect or get value from database
				} else {
					topCombinedInterestCodes = angular.fromJson($window.sessionStorage.getItem('topCombinedInterestCodes'));
				}
				return topCombinedInterestCodes;
			} else {
				// TODO: redirect or get value
			}
		} else {
			return topCombinedInterestCodes;
		}
	};

	this.setTopCombinedInterestCodes = function(topInterestCodes) {
		topCombinedInterestCodes = topInterestCodes;

		if (topInterestCodes === undefined) {
			$window.sessionStorage.removeItem("topCombinedInterestCodes");
		}

		if (typeof (Storage) !== "undefined" && topInterestCodes !== undefined) {
			$window.sessionStorage.setItem('topCombinedInterestCodes', JSON.stringify(topInterestCodes));
		}
	};

	/**
	 * Setup tied score object for combined tied score.
	 */
	var combinedTiedScoreObject;

	this.setCombinedTiedScoreObject = function(object) {

		combinedTiedScoreObject = object;

		if (object === undefined) {
			$window.sessionStorage.removeItem("combinedTiedScoreObject");
		}
		if (typeof (Storage) !== "undefined" && object !== undefined) {
			$window.sessionStorage.setItem('combinedTiedScoreObject', JSON.stringify(combinedTiedScoreObject));
			combinedTiedScoreObject = angular.fromJson($window.sessionStorage.getItem('combinedTiedScoreObject'));
		}
	};

	this.getCombinedTiedScoreObject = function() {
		if (combinedTiedScoreObject === undefined) {
			if (typeof (Storage) !== "undefined") {
				if ($window.sessionStorage.getItem("combinedTiedScoreObject") === null || $window.sessionStorage.getItem("combinedTiedScoreObject") === 'undefined') {
					// TODO: redirect or get value from database
				} else {
					combinedTiedScoreObject = angular.fromJson($window.sessionStorage.getItem('combinedTiedScoreObject'));
				}
				return combinedTiedScoreObject;
			} else {
				// TODO: redirect or get value
			}
		} else {
			return combinedTiedScoreObject;
		}
	};

	var topGenderInterestCodes;
	this.getTopGenderInterestCodes = function() {
		if (topGenderInterestCodes === undefined) {
			if (typeof (Storage) !== "undefined") {
				if ($window.sessionStorage.getItem("topGenderInterestCodes") === null || $window.sessionStorage.getItem("topGenderInterestCodes") === 'undefined') {
					// TODO: redirect or get value from database
				} else {
					topGenderInterestCodes = angular.fromJson($window.sessionStorage.getItem('topGenderInterestCodes'));
				}
				return topGenderInterestCodes;
			} else {
				// TODO: redirect or get value
			}
		} else {
			return topGenderInterestCodes;
		}
	};

	this.setTopGenderInterestCodes = function(topInterestCodes) {
		topGenderInterestCodes = topInterestCodes;

		if (topInterestCodes === undefined) {
			$window.sessionStorage.removeItem("topGenderInterestCodes");
		}
		if (typeof (Storage) !== "undefined" && topInterestCodes !== undefined) {
			$window.sessionStorage.setItem('topGenderInterestCodes', JSON.stringify(topInterestCodes));
		}
	};

	/**
	 * Setup tied score object for gender tied score.
	 */
	var genderTiedScoreObject;

	this.setGenderTiedScoreObject = function(object) {

		genderTiedScoreObject = object;

		if (object === undefined) {
			$window.sessionStorage.removeItem("genderTiedScoreObject");
		}
		if (typeof (Storage) !== "undefined" && object !== undefined) {
			$window.sessionStorage.setItem('genderTiedScoreObject', JSON.stringify(genderTiedScoreObject));
			genderTiedScoreObject = angular.fromJson($window.sessionStorage.getItem('genderTiedScoreObject'));
		}
	};

	this.getGenderTiedScoreObject = function() {
		if (genderTiedScoreObject === undefined) {
			if (typeof (Storage) !== "undefined") {
				if ($window.sessionStorage.getItem("genderTiedScoreObject") === null || $window.sessionStorage.getItem("genderTiedScoreObject") === 'undefined') {
					// TODO: redirect or get value from database
				} else {
					genderTiedScoreObject = angular.fromJson($window.sessionStorage.getItem('genderTiedScoreObject'));
				}
				return genderTiedScoreObject;
			} else {
				// TODO: redirect or get value
			}
		} else {
			return genderTiedScoreObject;
		}
	};

	/**
	 * Used to POST test information to database.
	 */
	var fyiObject;

	this.setFyiObject = function(object) {
		fyiObject = object;
	};
	this.getFyiObject = function() {
		return fyiObject;
	};

	// holds questions and answers
	var questionAnswerList;
	var interestLikes = 0;
	var interestDislikes = 0;
	var interestIndifferents = 0;
	var userGender = 'M';
	var genderScore;
	var combinedScore;

	var interestCodeRawScore = {
		rRawScore : 0,
		iRawScore : 0,
		aRawScore : 0,
		sRawScore : 0,
		eRawScore : 0,
		cRawScore : 0
	};

	// reset values
	this.resetValues = function() {
		interestLikes = 0;
		interestDislikes = 0;
		interestIndifferents = 0;
		interestCodeRawScore = {
			rRawScore : 0,
			iRawScore : 0,
			aRawScore : 0,
			sRawScore : 0,
			eRawScore : 0,
			cRawScore : 0
		};
	};

	this.setRawScores = function(rawScores) {
		interestCodeRawScore = rawScores;
	};

	this.getCombinedScore = function() {
		return combinedScore;
	};

	this.getGenderScore = function() {
		return genderScore;
	};

	this.getInterestCodeRawScore = function() {
		return interestCodeRawScore;
	};

	this.getInterestLikes = function() {
		return interestLikes;
	};

	this.getInterestDislikes = function() {
		return interestDislikes;
	};

	this.getInterestIndifferents = function() {
		return interestIndifferents;
	};

	/*
	 * Calculate raw score for each interest code (R, I, A, S, E, C). Like = 2
	 * Indifferent = 1 Dislike = 0
	 * 
	 * Calculate the total number of likes, indifferents and dislikes for each
	 * interest code. This is not being used.
	 * 
	 * Calculate the overall total number of likes, indifferents and dislikes.
	 * 
	 * 
	 * What is tied score and how is it calculated?
	 * 
	 * How is Combined Score calculated?
	 * 
	 * How is Gender Score calculated?
	 */
	this.calculateRawScore = function() {

		var len = this.questionAnswerList.length;

		// loop through answer object
		for (var i = 0; i < len; i++) {
			var interestCd = this.questionAnswerList[i].interestCd;
			var answer = parseInt(this.questionAnswerList[i].answer);
			var isSeeded = this.questionAnswerList[i].isSeeded;
			// if seeded, continue to the next item
			if (isSeeded) {continue;}

			if (answer !== undefined && answer === 2) {
				// track likes
				interestLikes++;

				// add 2 to raw score for likes per interest
				// code
				switch (interestCd) {
				case "R":
					interestCodeRawScore.rRawScore = interestCodeRawScore.rRawScore + 2;
					break;
				case "I":
					interestCodeRawScore.iRawScore = interestCodeRawScore.iRawScore + 2;
					break;
				case "A":
					interestCodeRawScore.aRawScore = interestCodeRawScore.aRawScore + 2;
					break;
				case "S":
					interestCodeRawScore.sRawScore = interestCodeRawScore.sRawScore + 2;
					break;
				case "E":
					interestCodeRawScore.eRawScore = interestCodeRawScore.eRawScore + 2;
					break;
				case "C":
					interestCodeRawScore.cRawScore = interestCodeRawScore.cRawScore + 2;
					break;
				}

			} else if (answer !== undefined && answer === 1) {
				// track indifferents
				interestIndifferents++;

				// add 1 to raw score for dislikes per interest
				// code
				switch (interestCd) {
				case "R":
					interestCodeRawScore.rRawScore++;
					break;
				case "I":
					interestCodeRawScore.iRawScore++;
					break;
				case "A":
					interestCodeRawScore.aRawScore++;
					break;
				case "S":
					interestCodeRawScore.sRawScore++;
					break;
				case "E":
					interestCodeRawScore.eRawScore++;
					break;
				case "C":
					interestCodeRawScore.cRawScore++;
					break;
				}
			} else {
				// track dislikes only if item is not seeded
				interestDislikes++;
			}
		}
		
	};

	/**
	 * Returns sorted combined score list in desc order.
	 */
	this.getSortedCombinedScore = function(scores) {
		var combinedScoreSort = [];
		for (var i = 0; i < scores.length; i++) {
			combinedScoreSort.push(scores[i]);
		}
		combinedScoreSort.sort(function(a, b) {
			return b.combinedScore - a.combinedScore
		});
		return combinedScoreSort;
	};

	/**
	 * Returns sorted gender score list in desc order.
	 */
	this.getSortedGenderScore = function(scores) {

		var genderScoreSort = [];
		for (var i = 0; i < scores.length; i++) {
			genderScoreSort.push(scores[i]);
		}
		genderScoreSort.sort(function(a, b) {
			return b.genderScore - a.genderScore
		});
		return genderScoreSort;
	};

	/**
	 * Check to see if there are tied scores that requires the user's input.
	 */
	this.isTiedScore = function(selectedScoreList, scoreType) {
		var selectedScoreListCopy = angular.copy(selectedScoreList);
		var numberOfSlotsLeft = 3;
		var len = selectedScoreListCopy.length;
		var tiedScoreList = [];

		for (var i = 0; i < len; i = i + tiedScoreList.length) {
			tiedScoreList = [];

			for (var k = 0 + i; k < len; k++) {

				if (scoreType === 'gender') {
					if (selectedScoreListCopy[i].genderScore === selectedScoreListCopy[k].genderScore) {
						tiedScoreList.push(selectedScoreListCopy[k]);
					}
				} else {
					if (selectedScoreListCopy[i].combinedScore === selectedScoreListCopy[k].combinedScore) {
						tiedScoreList.push(selectedScoreListCopy[k]);
					}
				}
			}

			// if number of slots left is less than 0 then user needs to choose
			// tied score
			if (numberOfSlotsLeft - tiedScoreList.length < 0) {

				// setup checkbox input
				for (var i = 0; i < len; i++) {
					for (var k = 0; k < tiedScoreList.length; k++) {
						if (selectedScoreListCopy[i].interestCd === tiedScoreList[k].interestCd) {

							selectedScoreListCopy[i].checkbox = true;
						}

					}
				}

				var tiedScoreObject = {
					numSlotsLeft : numberOfSlotsLeft,
					scoreList : selectedScoreListCopy,
					scoreType : scoreType,
					numTiedScore : tiedScoreList.length
				};

				if (scoreType === 'gender') {
					this.setGenderTiedScoreObject(tiedScoreObject);
				} else {
					this.setCombinedTiedScoreObject(tiedScoreObject);
				}

				return true;
			}

			// track number of slots left
			numberOfSlotsLeft = numberOfSlotsLeft - tiedScoreList.length;

			// if number of slots is 0 then dont need input from user
			if (numberOfSlotsLeft === 0) {
				return false;
			}
		}

		// if number of slots left is less than zero then needs input from user
		// return numberOfSlotsLeft
	};

	/**
	 * Track if tied link is clicked.
	 */
	var isTiedLinkClicked = false;
	this.setTiedLinkClicked = function(isTiedLinkClicked) {
		this.isTiedLinkClicked = isTiedLinkClicked;
	};
	this.getTiedLinkClicked = function() {
		return this.isTiedLinkClicked;
	}
} ]);