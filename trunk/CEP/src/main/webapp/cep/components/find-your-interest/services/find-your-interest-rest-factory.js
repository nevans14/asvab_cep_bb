cepApp.factory('FYIRestFactory', [ '$http', 'appUrl', 'awsApiFullUrl', '$q', 'AuthenticationService', function($http, appUrl, awsApiFullUrl, $q, AuthenticationService) {

	var fyiRestFactory = {};

	fyiRestFactory.getTestQuestions = function() {
		//return $http.get(appUrl + 'fyi/test');

		var xsrfCookie = AuthenticationService.getAwsXsrfCookie();

		if (!xsrfCookie) { 
			console.log("Missing AWS XSRF cookie!")
		}
		
		return $http({
			method: 'POST',
			url: awsApiFullUrl + '/api/fyi/get-items',
			headers: {
				'X-XSRF-TOKEN': xsrfCookie
			},
		});
	};

	fyiRestFactory.postManualAnswers = function (fyiObject) {
		return $http.post(awsApiFullUrl + '/api/fyi/save-manual-responses', fyiObject);
	}

	fyiRestFactory.postAnswers = function(answerList){
		return $http.post(awsApiFullUrl + "/api/fyi/save-responses", answerList);
	};

	fyiRestFactory.saveTest = function(){
		// return $http({method: 'POST', url: awsApiFullUrl + '/api/fyi/save-test'});
		return $http.post(appUrl + 'fyi/save-test/');
	};


	fyiRestFactory.getGenderScore = function(rawScores, gender) {
		return $http.post(appUrl + 'fyi/getGenderScore/' + gender + '/', rawScores);
	};

	fyiRestFactory.getCombinedScore = function(rawScores) {
		return $http.post(appUrl + 'fyi/getCombinedScore', rawScores);
	};

	fyiRestFactory.getNumberTestTaken = function() {
		return $http.get(appUrl + 'fyi/NumberTestTaken');
	};

	fyiRestFactory.getCombinedAndGenderScores = function() {
		return $http.get(appUrl + 'fyi/CombinedAndGenderScores');
	};

	fyiRestFactory.postUserScores = function(scoreObject) {
		return $http.post(appUrl + 'fyi/PostScore', scoreObject);
	};

	fyiRestFactory.getUserInteresteCodes = function() {
		return $http.get(appUrl + 'fyi/UserInterestCodes');
	};

	fyiRestFactory.getProfileExist = function() {
		return $http.get(appUrl + 'fyi/CheckProfileExists');
	};

	fyiRestFactory.getScoreChoice = function() {
		return $http.get(appUrl + 'fyi/ScoreChoice');
	};

	fyiRestFactory.getTiedSelection = function() {
		var defer = $q.defer();
		$http.get(appUrl + 'fyi/get-tied-selection').then(function(success){
			defer.resolve(success);
		}, function(error){
			defer.reject(error);
		});
		return defer.promise;
	};

	fyiRestFactory.insertTiedSelection = function(tiedObject) {
		var deferred = $q.defer();
		$http.post(appUrl + 'fyi/insert-tied-selection', tiedObject).then(function(success) {
			deferred.resolve(success);
		}, function(error){
			console.log(error);
			deferred.reject(error);
		});
		return deferred.promise;
	};

	fyiRestFactory.getTestScore = function () {
		return $http.get(appUrl + 'testScore/get-test-score');
	};

	fyiRestFactory.getStudentScoreResults = function () {
		return $http.get(appUrl + 'testScore/get-student-score-results');
	};

	fyiRestFactory.getTestScoreByAccessCode = function(accessCode) {
		return $http.get(appUrl + 'getTestScoreByAccessCode/' + accessCode + '/');
	};

	
	return fyiRestFactory;

} ]);