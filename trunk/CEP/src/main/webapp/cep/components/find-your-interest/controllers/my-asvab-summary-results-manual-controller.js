cepApp.controller('FindYourInterestSummaryManualController', [ 
	'$scope', 'FYIScoreService', 'FYIRestFactory', '$rootScope', '$window', '$location', '$anchorScroll', '$route', '$templateCache', 'MediaCenterService', 'modalService', 'youTubeModalService', 'cepManualInput', 'asvabScore', 'ctmUrl',
	function($scope, FYIScoreService, FYIRestFactory, $rootScope, $window, $location, $anchorScroll, $route, $templateCache, MediaCenterService, modalService, youTubeModalService, cepManualInput, asvabScore, ctmUrl) {
	
	$scope.scoreSummary;
	$scope.asvabScore = asvabScore.data; // manually entered score data

	$scope.genderFull = "";
	$scope.testDate = {
		"month" : "",
		"day" : "",
		"year" : ""
	}

    // Retrieve scores from session.
	$scope.verbalScore = Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore);
	$scope.mathScore = Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore);
	$scope.scienceScore = Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore);
	$scope.haveManualAsvabTestScores = $window.sessionStorage.getItem('manualScores') == "true";
	
	// set hs grade level
	$scope.hsGrade = $scope.asvabScore[$scope.asvabScore.length-1].grade; //$scope.asvabScore.hsGrade;
	// Set test gender (full) title

	$scope.getGenderFull = function() {
		var genderCode = $scope.asvabScore[$scope.asvabScore.length-1].gender;
		if (genderCode != null)
			genderCode = genderCode.toLowerCase();
		switch (genderCode) {
			case "f":
				genderCode = "Female";
				break;
			case "m":
				genderCode = "Male";
				break;
			default:
				genderCode = null;
				break;
		}
		return genderCode;
	}
	
	$scope.gender  = $scope.getGenderFull();

	$scope.isGrade = function(){
		if($scope.hsGrade != null)
			return true;
		return false;
	}
	
	$scope.isGender = function(){
		if($scope.gender != null)
			return true;
		return false;
	}
	
	$scope.isDate = function(){
		if($scope.testDate.month != null && $scope.testDate.day != null && $scope.testDate.year != null)
			return true;
		return false;
	}
	
	$scope.isLocation = function(){
		if($scope.testLocation != null)
			return true;
		return false;
	}
		
	
	// Set test date
	$scope.testDate.year = $scope.asvabScore[$scope.asvabScore.length-1].testYear; //$scope.asvabScore.testYear;
	$scope.testDate.day = $scope.asvabScore[$scope.asvabScore.length-1].testDay; //$scope.asvabScore.testDay;
	$scope.testDate.month = $scope.asvabScore[$scope.asvabScore.length-1].testMonth; //$scope.asvabScore.testMonth;
	$scope.testLocation = $scope.asvabScore[$scope.asvabScore.length-1].testLocation;
	
	$scope.cepManualInput = function(cepManualMode) {
		var enterMode = cepManualMode == 'enter' ? true : false;
		var retrieveMode = cepManualMode == 'retrieve' ? true : false;
		cepManualInput.show({}, {enter: enterMode, retrieve: retrieveMode}).then(function(result) {
			//console.log(result);
			if (result) {
				if (result.hasOwnProperty('aFQTRawSSComposites')) {
					// Retrieved asvab scores
					$scope.scoreSummary = result;
					console.log("TODO: nav to summary with new score data with access code param???");
				} else {
					// Entered asvab scores
					$scope.haveManualAsvabTestScores = true;
					$scope.asvabScore.push({verbalScore : result.verbalScore, mathScore : result.mathScore, scienceScore : result.scienceScore});
				}
				
				// Update session storage
				$window.sessionStorage.setItem('manualScores', $scope.haveManualAsvabTestScores);
				$window.sessionStorage.setItem('sV', $scope.asvabScore.verbalScore);
				$window.sessionStorage.setItem('sM', $scope.asvabScore.mathScore);
				$window.sessionStorage.setItem('sS', $scope.asvabScore.scienceScore);

				// Update book stacks
				//$scope.vBooks.score = $scope.getScore('verbal');
				//$scope.mBooks.score = $scope.getScore('math');
				//$scope.sBooks.score = $scope.getScore('science');
				//$scope.scoreSorting();
			}
		});
	}

	$scope.moreInfo = function(area) {
		var modalOptions;
		switch (area) {
		case "ces":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<p>Consider these scores when exploring careers in the OCCU-Find. Use your highest score to review occupations based on the corresponding skill importance rating.</p>' + 
					'<p>ASVAB test scores are combined to yield three Career Exploration Scores: Verbal Skills, Math Skills, and Science/Technical Skills. Each of these scores is made up of a combination of some of the individual ASVAB tests.</p>' + 
					'<p><strong>Verbal Skills</strong> - estimates your potential for verbal activities; derived from the Word Knowledge (WK) and Paragraph Comprehension (PC) tests.</p>' + 
					'<p><strong>Math Skills</strong> - estimates your potential for mathematical activities; derived from the Mathematics Knowledge (MK) and Arithmetic Reasoning (AR) tests.</p>' + 
					'<p><strong>Science/Technical Skills</strong> - estimates your potential for science and technical activities, derived from the General Science (GS), Mechanical Comprehension (MC), and Electronics Information (EI) tests.</p>' + 
					'<p>These three Career Exploration Scores are good indicators of your potential for success in further education, training, and in occupations. They focus on skills (which are learned and modifiable) rather than on abilities (which are often seen as fixed).</p>'
			};
			break;
		case "cesStandard":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<h4>Standard Scores</h4>' +
					'<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
			};
			break;
		case "cesPercent":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<h4>Percentile Scores</h4>' +
					'<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
			};
			break;
		case "asvab":
			modalOptions = {
				headerText : 'ASVAB Test Scores',
				bodyText : '<p>Need content or correct link.</p>'
			};
			break;
		case "afqt":
			modalOptions = {
				headerText : 'Military Entrance Score (AFQT)',
				bodyText : '<p>Participation in the ASVAB CEP does not obligate you to talk with military recruiters or consider a military career.</p>' + 
				'<p>The AFQT score is what the Military will use to determine enlistment eligibility.</p>' + 
				'<p>If you are interested in joining the Military it is best to discuss this score with a recruiter.</p>'
			};
			break;
		}
		

		// For ASVAB open CITM Composites Score page in new tab/window not modal win
		if (area == "asvab") {
			//$window.open('http://www.citm.com/CITM/composite-scores', '_blank');
			//alert(ssoUrl + 'composite-scores');
            $window.open(ctmUrl + 'composite-scores', '_blank');
		} else {
			modalService.showModal({
				size : 'md'
			}, modalOptions);
		}
	}

	$scope.scrollTo = function(id) {
		$location.hash(id);
		$anchorScroll();
	}

	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			$('.goToTop').fadeIn();
		} else {
			$('.goToTop').fadeOut();
		}
	})

	$('.goToTop').click(function () {
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	})

} ]);