cepApp.controller('FindYourInterestTiedController', [ '$scope', '$q', 'FYIScoreService', 'FYIRestFactory', '$rootScope', '$window', '$location', 'modalService', function($scope, $q, FYIScoreService, FYIRestFactory, $rootScope, $window, $location, modalService) {

	/**
	 * 
	 * Combined Tie Score
	 * 
	 */
	$scope.combinedTiedScoreObject = FYIScoreService.getCombinedTiedScoreObject();
	$scope.showGenderTieSelection = false;

	/**
	 * Keep track and limit number of selections that can be made.
	 */
	if ($scope.combinedTiedScoreObject !== undefined) {
		$scope.combinedLimit = $scope.combinedTiedScoreObject.numSlotsLeft;
		$scope.combinedChecked = 0;
		$scope.combinedCheckChanged = function(item) {
			if (item.selected)
				$scope.combinedChecked++;
			else
				$scope.combinedChecked--;
		}
	} else {
		$scope.showGenderTieSelection = true;
	}

	/**
	 * Submit score selection.
	 */
	$scope.combinedSubmit = function() {
		var topInterestCodes = [];
		var scoreList = $scope.combinedTiedScoreObject.scoreList;

		// determine the top three interest scores
		var numSelection = 0;
		for (var i = 0; i < scoreList.length; i++) {
			if (topInterestCodes.length < 3) {
				if (scoreList[i].checkbox != true) {
					topInterestCodes.push(scoreList[i]);
				} else {
					if (scoreList[i].selected == true) {
						topInterestCodes.push(scoreList[i]);
						numSelection++;
					}
				}
			}
		}

		// validate selections are made
		if (numSelection != $scope.combinedLimit) {

			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : 'Select ' + $scope.combinedTiedScoreObject.numSlotsLeft + ' of the ' + $scope.combinedTiedScoreObject.numTiedScore + ' tied interests.'
			};

			modalService.showModal({}, modalOptions);
			return false;
		}

		FYIScoreService.setTopCombinedInterestCodes(topInterestCodes);

		// Determines if gender scores need a tie breaker.
		if ($scope.genderTiedScoreObject != undefined) {
			var numMatch = 0;
			var combinedNumberCheckboxes = 0;
			var genderNumberCheckboxes = 0;

			for (i in $scope.combinedTiedScoreObject.scoreList) {
				if ($scope.combinedTiedScoreObject.scoreList[i].checkbox == true) {
					combinedNumberCheckboxes++;
				}
			}

			for (i in $scope.genderTiedScoreObject.scoreList) {
				if ($scope.genderTiedScoreObject.scoreList[i].checkbox == true) {
					genderNumberCheckboxes++;
				}
			}
			if (combinedNumberCheckboxes == genderNumberCheckboxes) {
				for (var i = 0; i < scoreList.length; i++) {
					for (var f = 0; f < $scope.genderTiedScoreObject.scoreList.length; f++) {
						if (scoreList[i].checkbox == true && $scope.genderTiedScoreObject.scoreList[f].checkbox == true && scoreList[i].interestCd == $scope.genderTiedScoreObject.scoreList[f].interestCd) {
							numMatch++;
							break;
						}
					}
				}
			}

			// If interest codes are not the same then display gender score tie
			// breaker.
			if (numMatch != combinedNumberCheckboxes) {
				$scope.showGenderTieSelection = true;
				return false;
			} else {

				// Gender tie score breaker are the same for combined, so reuse
				// combined FYI selection for gender scores.
				var genderScoreList = $scope.genderTiedScoreObject.scoreList;
				var tempGenderScoreList = [];
				for (var f = 0; f < topInterestCodes.length; f++) {
					for (var i = 0; i < genderScoreList.length; i++) {
						if (topInterestCodes[f].interestCd == genderScoreList[i].interestCd) {
							tempGenderScoreList.push(genderScoreList[i]);
							break;
						}
					}
				}

				FYIScoreService.setTopGenderInterestCodes(tempGenderScoreList);
			}

		}

		// Save Top interest code to databse
		saveTopInterestCodeToDatabase();

		// Redirect to score page.
		$location.path('/find-your-interest-score');

	}

	/**
	 * 
	 * Gender Tie Score
	 * 
	 */
	$scope.genderTiedScoreObject = FYIScoreService.getGenderTiedScoreObject();

	/**
	 * Keep track and limit number of selections that can be made.
	 */
	if ($scope.genderTiedScoreObject !== undefined) {
		$scope.genderLimit = $scope.genderTiedScoreObject.numSlotsLeft;
		$scope.genderChecked = 0;
		$scope.genderCheckChanged = function(item) {
			if (item.selected)
				$scope.genderChecked++;
			else
				$scope.genderChecked--;
		}
	}

	/**
	 * Submit score selection.
	 */
	$scope.genderSubmit = function() {
		var topInterestCodes = [];
		var scoreList = $scope.genderTiedScoreObject.scoreList;

		// determine the top three interest scores
		var numSelection = 0;
		for (var i = 0; i < scoreList.length; i++) {
			if (topInterestCodes.length < 3) {
				if (scoreList[i].checkbox != true) {
					topInterestCodes.push(scoreList[i]);
				} else {
					if (scoreList[i].selected == true) {
						topInterestCodes.push(scoreList[i]);
						numSelection++;
					}
				}
			}
		}

		// validate selections are made
		if (numSelection != $scope.genderLimit) {

			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : 'Select ' + $scope.genderTiedScoreObject.numSlotsLeft + ' of the ' + $scope.genderTiedScoreObject.numTiedScore + ' tied interests.'
			};

			modalService.showModal({}, modalOptions);
			return false;
		}

		FYIScoreService.setTopGenderInterestCodes(topInterestCodes);

		// Save Top interest code to database
		saveTopInterestCodeToDatabase();

		// Redirect to score page.
		$location.path('/find-your-interest-score');
	}

	/**
	 * Saves the top selected interest codes for gender and combine to the
	 * database.
	 */
	saveTopInterestCodeToDatabase = function() {
		
		// Reset values
		FYIScoreService.setCombinedTiedScoreObject(undefined);
		FYIScoreService.setGenderTiedScoreObject(undefined);
		$window.sessionStorage.removeItem("combinedTiedScoreObject");
		$window.sessionStorage.removeItem("genderTiedScoreObject");
		
		var topGScore = FYIScoreService.getTopGenderInterestCodes();
		var topCScore = FYIScoreService.getTopCombinedInterestCodes();
		var tiedObject = {
			userId : $rootScope.globals.currentUser.userId,
			combineInterestOne : topCScore ? topCScore[0].interestCd : null,
			combineInterestTwo : topCScore ? topCScore[1].interestCd : null,
			combineInterestThree : topCScore ? topCScore[2].interestCd : null,
			genderInterestOne : topGScore ? topGScore[0].interestCd : null,
			genderInterestTwo : topGScore ? topGScore[1].interestCd : null,
			genderInterestThree : topGScore ? topGScore[2].interestCd : null,
		}
		FYIRestFactory.insertTiedSelection(tiedObject);
		
		// Reset value
		FYIScoreService.setTiedLinkClicked(false);
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
