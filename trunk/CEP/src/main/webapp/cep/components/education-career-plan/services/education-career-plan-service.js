cepApp.service("EducationCareerPlanService", function (EducationCareerPlanRestService, $q) {
    var service = {};

    service.getItems = function () {
      var deferred = $q.defer();

        EducationCareerPlanRestService.getItems().success(function (results) {
         deferred.resolve(results);
      }).error(function (error) {
          deferred.reject(error);
      });

      return deferred.promise;
    };

    service.getResponses = function () {
      var deferred = $q.defer();

        EducationCareerPlanRestService.getResponses().success(function (results) {
          deferred.resolve(results.responses);
      }).error(function (error) {
         deferred.reject(error);
      });

      return deferred.promise;
    };

    service.updateResponses = function(obj) {
      var deferred = $q.defer();

        EducationCareerPlanRestService.updateResponses(obj).success(function (results) {
          deferred.resolve(results);
        }).error(function (error) {
         deferred.reject(error);
        });

      return deferred.promise;
    };

    return service;
});