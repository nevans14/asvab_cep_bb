cepApp
    .directive("confirmOnExit", function () {
        return {
            scope: {
              confirmOnExit: '&',
              confirmMessageWindow: '@',
              confirmMessageRoute: '@',
              confirmMessage: '@'
            },
            link: function ($scope, elem, attrs) {
                $scope.hasConfirmed = false;
                window.onbeforeunload = function () {
                    if ($scope.confirmOnExit()) {
                        return $scope.confirmMessageWindow || $scope.confirmMessage;
                    }
                };
                var $locationChangeStartUnbind = $scope.$on('$locationChangeStart', function (e, next, curr) {
                   if ($scope.confirmOnExit() && !$scope.hasConfirmed) {
                       if (!confirm($scope.confirmMessageRoute || $scope.confirmMessage)) {
                           e.preventDefault();
                           $scope.$root.isRouteLoading = false;
                       } else {
                           $scope.hasConfirmed = true;
                       }
                   }
                });
                $scope.$on('$destroy', function () {
                    window.onbeforeunload = null;
                    $locationChangeStartUnbind();
                });
            }
        }
    })
    .controller("EducationCareerPlanController", function ($scope, EducationCareerPlanService, sections, responses, modalService) {
    $scope.responses = {};
    $scope.additionalOccs = [];
    $scope.disableSave = false;
    $scope.sections = sections;
    $scope.itemTypes = {
        'TextBox': 1,
        'TextArea': 2,
        'MultiItem': 3,
        'Info': 4
    };
    $scope.maxCharLimitPerTextarea = 800;
    $scope.numberOfOccupations = 1;
    $scope.showModal = function (headerText, message) {
        var modalOptions = {
            headerText : headerText,
            bodyText : message
        };

        modalService.showModal({
            size : 'sm'
        }, modalOptions);
    };

    $scope.init = function () {
        if (typeof responses === 'undefined' ||
            responses === '' ||
            responses === "null") {
            $scope.responses = {
                studentInfo: {
                    name: '',
                    counselor: '',
                    grade: '',
                    dateCompleted: '',
                    dateReviewed: ''
                }
            };
        } else {
            $scope.responses = angular.fromJson(responses);
            $scope.additionalOccs = [];
            angular.forEach($scope.responses, function (value, key) {
               if (key.indexOf('Occupation_') > -1) {
                   var currOcc = {},
                       index = key.substring((key.indexOf('_') + 1), (key.lastIndexOf('_')));
                       if (parseInt(index, 10) > $scope.numberOfOccupations) {
                           $scope.numberOfOccupations = parseInt(index, 10);
                       }
                   if (index !== "1") {
                       currOcc.index = index;
                       if (key.indexOf('Locations2') > -1) {
                           $scope.additionalOccs.push(currOcc);
                       }
                   }
               }
            });
        }
    };

    $scope.init();
    $scope.updateResponses = function () {
        $scope.disableSave = true;
        if ($scope.addingNewOcc) {
            $scope.addOccupations();
        }
        var obj = {
          responses: JSON.stringify($scope.responses)
        };
        EducationCareerPlanService.updateResponses(obj).then(function (results) {
            $scope.disableSave = false;
            if ($scope.addingNewOcc) {
                $scope.addingNewOcc = !$scope.addingNewOcc;
            }
            responses = obj.responses;
            $scope.init();
            // reset occ details obj
            $scope.newOccDetailsObj = {
                title: '',
                courses: '',
                locations: '',
                extracurricular: '',
                locations2: ''
            };
            $scope.careerPlanForm.$setPristine();
        }, function (reason) {
          $scope.showModal('Oops', 'Something went wrong when saving your data. Please try again later.');
        });
    };

    $scope.addingNewOcc = false;
    $scope.newOccDetailsObj = {
        title: '',
        courses: '',
        locations: '',
        extracurricular: '',
        locations2: ''
    };

    $scope.canAddNewOccupation = function () {
      // determines if the user can add more occupations (10 is the max)
      for (var i = 2; i <= 10; i++) {
          if ($scope.responses["Occupation_" + i + "_OccupationName"] === undefined)
              return true;
      }
      // means that they are at 10 occupations
      return false;
    };
    $scope.removeOcc = function(name) {

        var index = name.split('_');
        index = index[1];

        delete $scope.responses["Occupation_"+index+"_OccupationName"];
        delete $scope.responses["Occupation_"+index+"_Courses"];
        delete $scope.responses["Occupation_"+index+"_Extracurricular"];
        delete $scope.responses["Occupation_"+index+"_Extracurricular "]; // this fixes existing DB error 
        delete $scope.responses["Occupation_"+index+"_Locations"];
        delete $scope.responses["Occupation_"+index+"_Locations2"];

        var obj = {
            responses: JSON.stringify($scope.responses)
        };
        EducationCareerPlanService.updateResponses(obj).then(function (results) {
            if (results > 0) {
                // update responses
                responses = obj.responses;
                $scope.init();
                $scope.careerPlanForm.$setPristine();
            } else {
                $scope.showModal('Oops', 'Something went wrong when saving your data. Please try again later.');
            }
        }, function (reason) {
            $scope.showModal('Oops', 'Something went wrong when saving your data. Please try again later.');
        });
    };

    $scope.cancelAddedOcc = function() {
        $scope.addingNewOcc = false;
        $scope.numberOfOccupations--;
        $scope.newOccDetailsObj = {
            title: '',
            courses: '',
            locations: '',
            extracurricular: '',
            locations2: ''
        };
    };

    $scope.addOccupations = function () {
        var nextIndex = $scope.getNextOccupationIndex();

        if ($scope.addingNewOcc) {
            if (nextIndex == undefined) {
                nextIndex = 10;
            } else {
                nextIndex;
            }
            
            $scope.responses["Occupation_"+nextIndex+"_OccupationName"] = $scope.newOccDetailsObj.title;
            $scope.responses["Occupation_"+nextIndex+"_Courses"] = $scope.newOccDetailsObj.courses;
            $scope.responses["Occupation_"+nextIndex+"_Extracurricular"] = $scope.newOccDetailsObj.extracurricular;
            $scope.responses["Occupation_"+nextIndex+"_Locations"] = $scope.newOccDetailsObj.locations;
            $scope.responses["Occupation_"+nextIndex+"_Locations2"] = $scope.newOccDetailsObj.locations2;
        }

    }

    $scope.addOccupation = function () {
        var canAddNewOccupation = $scope.canAddNewOccupation();
        if (!canAddNewOccupation) {
            $scope.disableSave = false;
            // prepare and show modal
            $scope.showModal('Adding Occupation', '<p>You can reached the limit of adding a new occupation (10).</p>');
            return;
        }
        if ($scope.addingNewOcc === false) {
            $scope.numberOfOccupations++;
        }
        $scope.addingNewOcc = true;
    };

    $scope.getNextOccupationIndex = function () {
        // determines if the user can add more occupations (10 is the max)
        for (var i = 2; i <= 10; i++) {
            if ($scope.responses["Occupation_" + i + "_OccupationName"] === undefined)
                return i;
        }
        return undefined;
    };

    function htmlToPlaintext(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    }

    function entitiesToPlainText(text) {
        return text ? String(text)
            .replace('&rdquo;', '"')
            .replace('&ldquo;', '"')
            .replace('&rsquo;', "'")
            .replace('&amp;', "&")
            .replace('&hellip;', '...')
            : '';
    }

    var docDefinition;
    var setupContent = function () {
        var content = [];
        // User input for name and grade.
        var nameAndGrade = [{
                text : 'Name: ' + ($scope.responses.studentInfo.name ? $scope.responses.studentInfo.name : "N/A"),
                alignment : 'left',
                fontSize : 18,
                bold : true
            }, {
                text : 'Counselor: ' + ($scope.responses.studentInfo.counselor ? $scope.responses.studentInfo.counselor : "N/A"),
                alignment : 'left',
                fontSize: 18,
                bold : true
            },{
                text : 'Grade: ' + ($scope.responses.studentInfo.grade ? $scope.responses.studentInfo.grade : "N/A"),
                alignment : 'left',
                fontSize : 18,
                bold : true
            },{
                text: 'Date Completed: ' + ($scope.responses.studentInfo.dateCompleted ? $scope.responses.studentInfo.dateCompleted : "N/A"),
                alignment: 'left',
                fontSize: 18,
                bold: true
            }, {
                text: 'Date Reviewed: ' + ($scope.responses.studentInfo.dateReviewed ? $scope.responses.studentInfo.dateReviewed : "N/A"),
                alignment: 'left',
                fontSize: 18,
                bold: true
            }, {
                canvas : [ {
                    type : 'line',
                    x1 : 0,
                    y1 : 5,
                    x2 : 595 - 2 * 40,
                    y2 : 5,
                    lineWidth : 1
            } ],
            style : 'horizontalLine'
        } ];
        content.push(nameAndGrade);
        // print sections
        angular.forEach($scope.sections, function (section) {
            var sectionContent = [{ text: section.name, style: 'sectionTitle'}];
            angular.forEach(section.items, function (item) {
                if (item.children.length > 0) {
                    angular.forEach(item.children, function (child) {
                        sectionContent.push({
                            text: entitiesToPlainText(htmlToPlaintext(child.stem)),
                            bold: true
                        }, {
                            text: ($scope.responses[child.name]) ? $scope.responses[child.name] : "N/A"
                        }, {
                            text: ' '
                        })
                    });
                } else {
                    sectionContent.push({
                        text: entitiesToPlainText(htmlToPlaintext(item.stem)),
                        bold: true
                    }, {
                        text: ($scope.responses[item.name]) ? $scope.responses[item.name] : "N/A"
                    }, {
                        text: ' '
                    })
                }
            });

            content.push(sectionContent);
            content.push({
                canvas : [ {
                    type : 'line',
                    x1 : 0,
                    y1 : 5,
                    x2 : 595 - 2 * 40,
                    y2 : 5,
                    lineWidth : 1
                } ],
                style : 'horizontalLine'
            });
        });

        // go through all of the additional occupations
        for (var i = 2; i <= 10; i++) {
            if ($scope.responses["Occupation_"+i+"_OccupationName"] !== undefined) {
                content.push({
                    text: 'OCCUPATION TITLE',
                    bold: true
                }, {
                    text: $scope.responses["Occupation_"+i+"_OccupationName"]
                }, {
                    text: ' '
                }, {
                    text: 'Courses Needed To Graduate',
                    bold: true
                }, {
                    text: $scope.responses["Occupation_"+i+"_Courses"]
                }, {
                    text: ' '
                }, {
                    text: 'Location(s) Offering this Opportunity',
                    bold: true
                }, {
                    text: $scope.responses["Occupation_"+i+"_Locations"]
                }, {
                    text: ' '
                }, {
                    text: 'Other Helpful Courses & Extracurricular Activities',
                    bold: true
                }, {
                    text: $scope.responses["Occupation_"+i+"_Extracurricular"]
                }, {
                    text: ' '
                },{
                    text: 'Location(s) Offering this Opportunity',
                    bold: true
                }, {
                    text: $scope.responses["Occupation_"+i+"_Locations2"]
                }, {
                    text: ' '
                })
            }
        }

        // set content
        docDefinition = {
            content : content,
            styles : {
                headerTitle : {
                    fontSize : 22,
                    bold : true,
                    alignment : 'center'
                },
                sectionTitle : {
                    margin : [ 0, 0, 0, 0 ],
                    bold : true,
                    alignment : 'left',
                    fontSize : 12
                },
                horizontalLine : {
                    margin : [ 0, 20, 0, 0 ]
                }
            }
        };
    };

    $scope.print = function() {
        setupContent();

        // IE support
        if (navigator.appName === 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {
            $scope.showModal('Alert', 'Your browser does not support this feature. Alternatively, you can download as PDF and then print.');

            return false;
        }

        pdfMake.createPdf(docDefinition).print();
    };

    $scope.downloadPdf = function() {
        setupContent();
        pdfMake.createPdf(docDefinition).download('ASVAB_MyCareerPlan.pdf');
    };
});