cepApp.service("EducationCareerPlanRestService", function (appUrl, $http) {
    var service = {};

    service.getItems = function () {
      return $http.get(appUrl + 'career-plan/get-items');
    };

    service.getResponses = function () {
      return $http.get(appUrl + 'career-plan/get-responses');
    };

    service.updateResponses = function (obj) {
      return $http.post(appUrl + 'career-plan/update-responses', obj);
    };

    return service;
});