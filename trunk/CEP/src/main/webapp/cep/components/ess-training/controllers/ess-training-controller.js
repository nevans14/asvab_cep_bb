cepApp.controller('EssTrainingController', function ($scope, $route, $location, $window , EssTrainingService, modalService, trainingData, alternateTraining, awsMediaUrl, alternateTrainingModalService, $sce) {
	
	$scope.training = trainingData;
	$scope.validation;
	$scope.todayDate = new Date();
	$scope.regId = undefined;
	$scope.regFirstName = undefined;
	$scope.regLastName = undefined;
	$scope.regEmailAddress = undefined;
	$scope.regAddress = undefined;
	$scope.regOrganization = undefined;
	$scope.regRole = undefined;
	$scope.regComment = undefined;
	$scope.showDietaryRestrictions = undefined;
	$scope.regDietaryRestrictions = undefined;
	$scope.showMobilityRestrictions = undefined;
	$scope.regMobilityRestrictions = undefined;
	$scope.locationName = undefined;
	$scope.addressOne = undefined;
	$scope.addressTwo = undefined;
	$scope.attn = undefined;
	$scope.city = undefined;

	$scope.isESS = trainingData.typeId === 1;
	$scope.isVirtual = trainingData.typeId === 2 || trainingData.typeId === 3;
	$scope.isVirtualPTI = trainingData.typeId === 5;
	$scope.isTroopsToTeachers = trainingData.typeId === 4;
	$scope.regSchoolName = undefined;
	$scope.regSchoolCity = undefined;
	$scope.regSchoolCounty = undefined;
	$scope.regState = undefined;
	$scope.regCountry = "US";
	$scope.regZip = undefined;
	$scope.hasResultsSheet = undefined;
	$scope.dt = undefined;
	$scope.datePopup = false;
	$scope.regGradeLevel = undefined;
	$scope.alternateTraining = alternateTraining.filter(function(x) { return x.essTrainingId !== $scope.training.essTrainingId});
	$scope.alternateTrainingSelected = undefined;

	$scope.virtualTrainingBarPath = awsMediaUrl + 'TRAINING/Testimonial.png';
	$scope.virtualTrainingAccessCodeLookupPath = awsMediaUrl + 'TRAINING/AccessCodeLookup.png';

	$scope.title1 = $scope.training.essTrainingTitle;
	$scope.title2 = $scope.training.essTrainingSubTitle;
	$scope.essTrainingDescription = $sce.trustAsHtml($scope.training.essTrainingDescription);

	function convertUTCDateToLocalDate(date) {
		var year = date.getFullYear();
		var month = date.getMonth();
		var day = date.getDate();
		var hour = date.getHours();
		var minute = date.getMinutes();
		var second = date.getSeconds();
		return new Date(Date.UTC(year, month, day, hour, minute, second));		
	}

	/***
	 * Determines if the training registration is closed
	 */
	$scope.isClosed = function() {		
		return ($scope.isEmpty() || $scope.isPassedCutOffDate() || $scope.isFull());
	}
	
	$scope.isEmpty = function() {
		return ($scope.training === undefined || $scope.training === "");
	}
	
	$scope.isFull = function() {
		return (!$scope.isEmpty() && ($scope.training.essMaxNumberOfParticipants <= $scope.training.numberOfParticipants));
	}
	
	$scope.isPassedCutOffDate = function () {
		return (!$scope.isEmpty() && ($scope.todayDate > convertUTCDateToLocalDate(new Date($scope.training.essCutOffDate))));
	}

	$scope.openDatePicker = function() {
		$scope.datePopup = true;
	};

	$scope.altInputFormats = [ 'M!/d!/yyyy' ];

	$scope.dateOptions = {
		// dateDisabled: disabled,

		// formatYear : 'yy',
		// maxDate : new Date(2020, 5, 22),
		// minDate : new Date(),
		// showWeeks : false,

		// startingDay: 1
	};

	$scope.errorHandlingModal = function(error) {
		var errorMsg = undefined;
		if (error.data && error.data.resultMessage) {
			errorMsg = error.data.resultMessage;
		} else {
			errorMsg = "It's taking longer than expected to find what you're looking for. Please try again.";
		}

		var modalOptions = {
			headerText: 'Training Session Registration',
			bodyText: errorMsg
		}
		
		modalService.showModal({
			size: 'sm'
		}, modalOptions);
	}

	$scope.selectAlternateTraining = function() {
		var modalOptions = {
			headerText : 'SELECT ANOTHER SESSION',
			bodyText : "Registration for this event is unavailable at this time because the session is full or the date has passed. Please select from the options below. Submit inquiries to <a href=\"mailto:asvabcep@gmail.com\">asvabcep@gmail.com</a>.",
			alternateTraining: $scope.alternateTraining
		}
		
		var result = alternateTrainingModalService.showModal({
			size: 'md'
		}, modalOptions);
		result.then(function(modalSelection) {
			$scope.alternateTrainingSelected = modalSelection;
			if (!isNaN($scope.alternateTrainingSelected)) {
				$location.path('/training-registration-form/' + $scope.alternateTrainingSelected);						
			} else {
				$scope.alternateTraining = [];
				$scope.formDisabled = true;
			}
		})
	}

	if (($scope.isEmpty() || $scope.isPassedCutOffDate() || $scope.isFull()) && $scope.alternateTraining.length > 0) {
		$scope.selectAlternateTraining();
	}

	$scope.AccessCodeLookupModal = function() {

		var modalOptions = {
			headerText : 'Access Code Lookup',
			bodyText : '<img src=' + $scope.virtualTrainingAccessCodeLookupPath + '></img>',
			windowClass: 'extra-large-modal'
		}
		
		modalService.showModal({
			size: 'lg'
		}, modalOptions);
	}
	
	$scope.submitVirtualRegistrationLookup = function () {
		$scope.validation = undefined;
		$scope.formDisabled = true;

		var ptiTrainingRegistration = undefined;

		if ($scope.isTroopsToTeachers) {
			ptiTrainingRegistration = {
				essTrainingId : $scope.training.essTrainingId,
				essTrainingRegistrationId : $scope.regId,
				registrationFirstName : $scope.regFirstName,
				registrationLastName : $scope.regLastName,
				registrationEmailAddress : $scope.regEmailAddress,
				registrationRole : '',
				registrationTypeId : $scope.training.typeId,
				troopSchool : {
					city : $scope.regSchoolCity,
					state: $scope.regState,
					zipCode : $scope.regZip,
					name : $scope.regSchoolName,
					country: $scope.regCountry,
					address: $scope.regAddress,
				}
			}
		}
		
		if ($scope.isVirtual) {
			ptiTrainingRegistration = {
				essTrainingId : $scope.training.essTrainingId,
				essTrainingRegistrationId : $scope.regId,
				registrationFirstName : $scope.regFirstName,
				registrationLastName : $scope.regLastName,
				registrationEmailAddress : $scope.regEmailAddress,
				registrationRole : $scope.regRole,
				doesKnowAccessCode: $scope.hasResultsSheet,
				registrationTypeId : $scope.training.typeId,
				testScoreRequest : {
					schoolName : $scope.regSchoolName,
					schoolCity : $scope.regSchoolCity,
					schoolCounty : $scope.regSchoolCounty,
					schoolState : $scope.regState,
					schoolCountry : $scope.regCountry,
					testMonth : !$scope.dt ? undefined : $scope.dt.getMonth() + 1,
					testYear : !$scope.dt ? undefined : $scope.dt.getFullYear(),
					gradeLevel : !$scope.regGradeLevel ? undefined : parseInt($scope.regGradeLevel),
				},
			};
			
			if ($scope.regRole == "Student" && !$scope.hasResultsSheet && 
				($scope.regFirstName == undefined ||
				$scope.regLastName == undefined ||
				$scope.regEmailAddress == undefined ||
				$scope.regSchoolName == undefined ||
				$scope.regSchoolCity == undefined ||
				$scope.regSchoolCounty == undefined ||
				$scope.regCountry == undefined ||
				($scope.regState == undefined && $scope.regCountry === 'US')  ||
				$scope.dt == undefined ||
				$scope.regRole == undefined ||
				($scope.regGradeLevel == undefined && $scope.hasResultsSheet === false)
				)) {
				$scope.validation = 'Please fill out all required fields.';
				$scope.formDisabled = false;
				return false;
			}
		}
		
		EssTrainingService.addTrainingRegistration(ptiTrainingRegistration).then(function (success) {
			var path = $location.path();
			$window.ga('create', 'UA-83809749-1', 'auto');
			$window.ga('send', 'pageview', path + '#' + ptiTrainingRegistration.essTrainingId + '#training-registration-submitted' );
			
			$scope.regFirstName = undefined;
			$scope.regLastName = undefined;
			$scope.regEmailAddress = undefined;
			$scope.regRole = undefined;
			$scope.regSchoolName = undefined;
			$scope.regSchoolCity = undefined;
			$scope.regSchoolCounty = undefined;
			$scope.regState = undefined;
			$scope.regCountry = undefined;
			$scope.hasResultsSheet = undefined;
			$scope.dt = undefined;
			$scope.regGradeLevel = undefined;
			$scope.regAddress = undefined;
			$scope.regZip = undefined;
			
			var bodyText = '';
			switch(success.data.resultCode) {
				case 1:
				case 2:
					if (success.data.resultMessage) {
						bodyText = success.data.resultMessage;
						break;
					}
				default:
					bodyText = 'Thank you for registering for this session! A confirmation email has been sent. Please check for you inbox (or possibly junk mail) for more information.';
					break;
			}

			var modalOptions = {
				headerText : 'Registration Confirmation',
				bodyText : bodyText
			}
			
			var response = modalService.showModal({
				size: 'sm'
			}, modalOptions);
			
			response.then(function() {
				if ($scope.alternateTrainingSelected) {
					$scope.formDisabled = true;
				} else {
					$scope.formDisabled = false;
					window.location.reload();
				}
			});
		}, function (error) {
			if (error == undefined) {
				console.log('an unexpected error has occurred.')
			} else {
				// Check if has open spot
				if (error.data.resultCode === 410) {
					EssTrainingService.getOpenSlots($scope.training.essTrainingId).then(function (success) {
						$scope.alternateTraining = success.data;
						if ($scope.alternateTraining.length > 0) {
							var modalOptions = {
								headerText : 'SELECT ANOTHER SESSION',
								bodyText : "Registration for this event is unavailable at this time because the session is full or the date has passed. Please select from the options below. Submit inquiries to <a href=\"mailto:asvabcep@gmail.com\">asvabcep@gmail.com</a>.",
								alternateTraining: $scope.alternateTraining
							}
							
							var result = alternateTrainingModalService.showModal({
								size: 'md'
							}, modalOptions);
							result.then(function(modalSelection) {
								$scope.alternateTrainingSelected = modalSelection;
								if (!isNaN($scope.alternateTrainingSelected)) {
									var currentTrainingId = $scope.training.essTrainingId;
									$scope.training.essTrainingId = parseInt($scope.alternateTrainingSelected);
									$scope.submitVirtualRegistrationLookup();
									$scope.training.essTrainingId = currentTrainingId;
									$scope.formDisabled = true;						
								} else {
									$scope.training = undefined;
									$scope.formDisabled = false;
								}
							})
						} else {
							window.location.reload();
						}
					}).catch(function(error) {
						$scope.errorHandlingModal(error);
					})
				} else {
					$scope.errorHandlingModal(error);
				}
			}
			$scope.formDisabled = false;
		});
	}

	$scope.submitRegistration = function () {
		$scope.validation = undefined;
		$scope.formDisabled = true;
		
		var essTrainingRegistration = {
			essTrainingId : $scope.training.essTrainingId,
			essTrainingRegistrationId : $scope.regId,
			registrationFirstName : $scope.regFirstName,
			registrationLastName : $scope.regLastName,
			registrationEmailAddress : $scope.regEmailAddress,
			registrationOrganization : $scope.regOrganization,
			registrationRole : $scope.regRole,
			registrationComment : $scope.regComment,
			registrationDietaryRestrictions : ($scope.regDietaryRestrictions == undefined ? "" : $scope.regDietaryRestrictions) ,
			registrationMobilityRestrictions : ($scope.regMobilityRestrictions == undefined ? "" : $scope.regMobilityRestrictions)
		};
		
		if ($scope.regFirstName == undefined ||
			$scope.regLastName == undefined ||
			$scope.regEmailAddress == undefined ||
			$scope.regOrganization == undefined ||
			$scope.regRole == undefined) {
			$scope.validation = 'Fill out all required fields.';
			$scope.formDisabled = false;
			return false;
		}
		
		EssTrainingService.addTrainingRegistration(essTrainingRegistration).then(function (success) {
			var path = $location.path();
			$window.ga('create', 'UA-83809749-1', 'auto');
			$window.ga('send', 'pageview', path + '#' + essTrainingRegistration.essTrainingId + '#training-registration-submitted' );
			
			$scope.regFirstName = undefined;
			$scope.regLastName = undefined;
			$scope.regEmailAddress = undefined;
			$scope.regOrganization = undefined;
			$scope.regComment = undefined;
			$scope.regDietaryRestrictions = undefined;
			$scope.regMobilityRestrictions = undefined;
			$scope.regRole = undefined;
			
			var modalOptions = {
				headerText : 'Training Session Registration',
				bodyText : 'Thank you for registering for the training session! A confirmation email has been sent. Please check for you inbox (or possibly junk mail) for more information.'
			}
			
			modalService.showModal({
				size: 'sm'
			}, modalOptions);
			
			$scope.formDisabled = false;
			$route.reload();
		}, function (error) {
			var modalOptions;
			if (error == undefined) {
				modalOptions = {
					headerText: 'Training Session Registration',
					bodyText: 'An unexpected error occured during registration.'
				}
			} else {
				modalOptions = {
					headerText: 'Training Session Registration',
					bodyText: error && error.data && error.data.resultMessage ? error.data.resultMessage : 'Error occured during registration.'
				}
			}
			modalService.showModal({
				size: 'sm'
			}, modalOptions);

			$scope.formDisabled = false;
		});
	}

	$scope.submitVirtualPTIRegistration = function () {
		$scope.validation = undefined;
		$scope.formDisabled = true;
		
		var essTrainingRegistration = {
			essTrainingId : $scope.training.essTrainingId,
			registrationTypeId : trainingData.typeId,
			registrationFirstName : $scope.regFirstName,
			registrationLastName : $scope.regLastName,
			registrationEmailAddress : $scope.regEmailAddress,
			registrationOrganization : $scope.regOrganization,
			registrationRole : $scope.regRole,
			mailingAddress : {
        locationName : $scope.locationName,
        addressOne : $scope.addressOne,
				addressTwo : $scope.addressTwo,
        attn : $scope.attn,
        city : $scope.city,
        state : $scope.regState,
        zip : $scope.regZip
   		}
		};
		
		if ($scope.regFirstName == undefined ||
			$scope.regLastName == undefined ||
			$scope.regEmailAddress == undefined ||
			$scope.regOrganization == undefined ||
			$scope.regRole == undefined ||
			$scope.locationName == undefined ||
			$scope.addressOne == undefined ||
			$scope.attn == undefined ||
			$scope.city == undefined ||
			$scope.regState == undefined ||
			$scope.regZip == undefined
			) {
			$scope.validation = 'Fill out all required fields.';
			$scope.formDisabled = false;
			return false;
		}
		
		EssTrainingService.addTrainingRegistration(essTrainingRegistration).then(function (success) {
			var path = $location.path();
			$window.ga('create', 'UA-83809749-1', 'auto');
			$window.ga('send', 'pageview', path + '#' + essTrainingRegistration.essTrainingId + '#training-registration-submitted' );
			
			$scope.regFirstName = undefined;
			$scope.regLastName = undefined;
			$scope.regEmailAddress = undefined;
			$scope.regOrganization = undefined;
			$scope.regRole = undefined;
			$scope.locationName = undefined;
			$scope.addressOne = undefined;
			$scope.addressTwo = undefined;
			$scope.attn = undefined;
			$scope.city = undefined;
			$scope.regState = undefined;
			$scope.regZip = undefined;
			
			var modalOptions = {
				headerText : 'Training Session Registration',
				bodyText : 'Thank you for registering for the training session! A confirmation email has been sent. Please check for you inbox (or possibly junk mail) for more information.'
			}
			
			modalService.showModal({
				size: 'sm'
			}, modalOptions);
			
			$scope.formDisabled = false;
			$route.reload();
		}, function (error) {
			var modalOptions;
			if (error == undefined) {
				modalOptions = {
					headerText: 'Training Session Registration',
					bodyText: 'An unexpected error occured during registration.'
				}
			} else {
				modalOptions = {
					headerText: 'Training Session Registration',
					bodyText: error && error.data && error.data.resultMessage ? error.data.resultMessage : 'Error occured during registration.'
				}
			}
			modalService.showModal({
				size: 'sm'
			}, modalOptions);

			$scope.formDisabled = false;
		});
	}
});