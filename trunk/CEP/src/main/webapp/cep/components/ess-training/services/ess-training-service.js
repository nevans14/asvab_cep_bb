cepApp.factory('EssTrainingService', ['appUrl', 'awsApiFullUrl', '$http', '$q', 'EssTrainingRestFactory', function (appUrl, awsApiFullUrl, $http, $q, EssTrainingRestFactory) {
	var service = {};
	
	var essTrainingData = undefined;
	
	service.getEssTrainingById = function (essTrainingId) {
		var deferred = $q.defer();
		EssTrainingRestFactory.getEssTrainingById(essTrainingId).success(function (data) {
			essTrainingData = data;
			deferred.resolve(essTrainingData);
		}).error(function (data, status, headers, config) {
			deferred.reject('error: ' + data);
		})
		return deferred.promise;
	}

	service.addTrainingRegistration = function (trainingRegistration) {
		console.log(trainingRegistration);
		var deferred = $q.defer();
		$http({
			method: 'POST',
			url: awsApiFullUrl + '/api/ess-training/add-ess-training-registration', 
			data: trainingRegistration,
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			dataType: 'json'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response)
		});
		console.log(trainingRegistration);
		return deferred.promise;
	}

	service.getOpenSlots = function(trainingId) {
		return $http({url: awsApiFullUrl + '/api/ess-training/get-available-sessions/' + trainingId, method: 'GET'});
	}
	
	return service;
}]);