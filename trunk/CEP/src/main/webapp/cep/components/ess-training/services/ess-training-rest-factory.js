cepApp.factory('EssTrainingRestFactory', ['$http', 'appUrl', 'awsApiFullUrl', function ($http, appUrl, awsApiFullUrl) {
	
	var essTrainingRestFactory = {};
	
	essTrainingRestFactory.getEssTrainingById = function (essTrainingId) {
		return $http({url: awsApiFullUrl + '/api/ess-training/get-ess-training-by-id/' + essTrainingId + '/', method: 'GET'});
	}
	
	essTrainingRestFactory.addEssTrainingRegistration = function (essTrainingRegistration) {
		return $http({
			method: 'POST',
			url: appUrl + 'ess-training/add-ess-training-registration', 
			data: essTrainingRegistration,
			headers: {'Content-Type': 'application/json;charset=utf-8'},
			dataType: 'json'
		});		
	}
	
	return essTrainingRestFactory;
	
}]);