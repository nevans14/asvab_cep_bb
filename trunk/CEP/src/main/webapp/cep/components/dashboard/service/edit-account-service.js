cepApp.service('editAccount', [
  '$uibModal', '$q', 'AuthenticationService', 'UserFactory', '$http',
  function ($uibModal, $q, AuthenticationService, UserFactory, $http) {

    var modalDefaults = {
      animation: true,
      templateUrl: 'cep/components/dashboard/page-partials/edit-account.html',
      size: 'md',
      windowClass: 'new-modal edit-account-modal'
    };

    var modalOptions = {};

    this.showModal = function (customModalDefaults, customModalOptions) {
      if (!customModalDefaults)
        customModalDefaults = {};
      customModalDefaults.backdrop = 'static';
      return this.show(customModalDefaults, customModalOptions);
    };

    this.show = function (customModalDefaults, customModalOptions) {
      // Create temp objects to work with since we're in a singleton service
      var tempModalDefaults = {};
      var tempModalOptions = {};

      // Map angular-ui modal custom defaults to modal defaults defined in
      // service
      angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

      // Map modal.html $scope custom properties to defaults defined in
      // service
      angular.extend(tempModalOptions, modalOptions, customModalOptions);

      if (!tempModalDefaults.controller) {
        tempModalDefaults.controller = ['$scope', '$uibModalInstance', '$rootScope', function ($scope, $uibModalInstance, $rootScope) {
          $scope.modalOptions = tempModalOptions;
          $scope.currentUsername = customModalOptions.parentScope.userName;
          $scope.form = {
            username: $scope.currentUsername,
            pw1: '',
            pw2: '',
            pwCurrent: '',
          };
          $scope.isSaving = false;

          $scope.clearError = function () {
            $scope.errorMsg = '';
          };

          $scope.responseHandling = function (response) {
            if (response.data.statusCode === 0) {
              if (response.data.isUsernameUpdated) {
                AuthenticationService.resetUserName($scope.form.username);
                $rootScope.$emit("ResetUserInfo", $scope.form);
              }

              if (response.data.isPasswordUpdated) {
                AuthenticationService.resetPassword($scope.form.pw1);
              }

              $uibModalInstance.dismiss('cancel');
            } else {
              $scope.errorMsg = response.data.statusMessage;
            }

            $scope.isSaving = false;
          };

          $scope.modalOptions.save = function (result) {

            var passCurrent = $scope.form.pwCurrent;
            var pass1 = $scope.form.pw1;
            var pass2 = $scope.form.pw2;
            var url = '/CEP/rest/user/updateUsername';
            var previousUsername = $scope.currentUsername;
            $scope.data = {};

            if (passCurrent === '') {
              $scope.errorMsg = "Current password missing!";
              return;
            }
            if (pass1.length <= 7 &&
              pass1 !== '') {
              $scope.errorMsg = "Password too short!";
              return;
            }
            if (pass1 !== pass2) {
              $scope.errorMsg = "Passwords don't match!";
              $scope.pw1 = '';
              $scope.pw2 = '';
              return;
            }

            $scope.data.updatedUsername = $scope.form.username;
            $scope.data.password = passCurrent;
            $scope.data.updatedPassword = pass2;

            $scope.isSaving = true;
            $http.post(url, $scope.data).then(function (response) {
                $scope.responseHandling(response);
            });
          };

          $scope.modalOptions.close = function (result) {
            $uibModalInstance.dismiss('cancel');
          };
        }]
      }
      return $uibModal.open(tempModalDefaults).result;
    };
  }]);