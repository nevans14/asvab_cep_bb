cepApp.service('registerAccessCodeReminder', function($uibModal, $q, $window, RegisterModalService) {
	
	var modalDefaults = {
		animation : true,
		size : 'md',
		templateUrl : 'cep/components/dashboard/page-partials/register-accesscode-reminder.html',
		windowClass: 'new-modal account-benefits-modal'
	};

	var modalOptions = {};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {
		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
        $scope.modalOptions = tempModalOptions;
        
        $scope.modalOptions.register = function(result) {
          $uibModalInstance.dismiss('cancel');
					RegisterModalService.show({}, {});
				};

				$scope.modalOptions.close = function(result) {
					$window.sessionStorage.setItem('AccessCodeReminder', 'clicked');
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}
		return $uibModal.open(tempModalDefaults).result;
	};
});