cepApp.controller('ManualInputController', [ 
	'$scope', 'asvabScore', 
	function($scope, asvabScore) {
		
		$scope.verbalScore = Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore);
		$scope.mathScore = Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore);
		$scope.scienceScore = Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore);
		console.log($scope.asvabScore[$scope.asvabScore.length-1].testLocation)
		// set hs grade level
		$scope.hsGrade = $scope.asvabScore[$scope.asvabScore.length-1].grade; //$scope.asvabScore.hsGrade;
		// Set test gender (full) title

		$scope.getGenderFull = function() {
			var genderCode = $scope.asvabScore[$scope.asvabScore.length-1].gender.toLowerCase();
			switch (genderCode) {
				case "f":
					genderCode = "Female";
					break;
				case "m":
					genderCode = "Male";
					break;
				default:
					genderCode = "Not Available";
					break;
			}
			return genderCode;
		}
		
		$scope.gender  = $scope.getGenderFull();

		$scope.isGrade = function(){
			if($scope.hsGrade != null)
				return true;
			return false;
		}
		
		$scope.testDate.year = $scope.asvabScore[$scope.asvabScore.length-1].testYear; //$scope.asvabScore.testYear;
		$scope.testDate.day = $scope.asvabScore[$scope.asvabScore.length-1].testDay; //$scope.asvabScore.testDay;
		$scope.testDate.month = $scope.asvabScore[$scope.asvabScore.length-1].testMonth; //$scope.asvabScore.testMonth;
		$scope.testLocation = $scope.asvabScore[$scope.asvabScore.length-1].testLocation;
		
}]);