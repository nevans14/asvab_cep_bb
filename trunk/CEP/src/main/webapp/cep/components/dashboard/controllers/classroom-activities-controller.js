cepApp.controller('ClassroomActivitiesController', [ '$scope', '$rootScope', '$location', '$sce', 'ViewDetailModalService', 'classroomActivities', 'classroomActivityCategories', 'awsMediaUrl', 'ClassroomRestFactory', function($scope, $rootScope, $location, $sce, ViewDetailModalService, classroomActivities, classroomActivityCategories, awsMediaUrl, ClassroomRestFactory) {

  $scope.isLoggedIn = $rootScope.globals.currentUser !== undefined;
  if ($scope.isLoggedIn) {
    $location.url('/classroom-activities');
  } else {
    $location.url('/');
    ViewDetailModalService.show({}, {});
	}
	
	$scope.classroomActivities = classroomActivities.data;
	$scope.classroomActivitiesFiltered = $scope.classroomActivities;

	$scope.activeCategory = {
		categoryId: '',
	}

	$scope.activityCategories = classroomActivityCategories.data.filter(function(d) { 
		return !d.title.includes("All Categories")
	});

	$scope.isNotStudent = $scope.classroomActivities.find(function(d) {
		return d.pdfs.find(function(p) {
			// comparing with not since student S role label doesn't change as often as teacher's role
			return p.role.toLowerCase() !== "s";
		})
	})

	$scope.modalSelector = !$scope.isNotStudent ? '#ActivityModal1' : '#ActivityModal2';

	$scope.onCategoryChange = function(category) {
		$scope.activeCategory.categoryId = category;
		$scope.classroomActivitiesFiltered = $scope.classroomActivities.filter(function(d) {
			if (d.categoryIds.some(function(c) { return c.toString() === category }) || category === "") {
				return true;
			}
		})
	}

	$scope.showActivity = function(activity) {
		alert('Download the PDF to complete the activity.');
		var win = window.open(activity, '_blank');
		win.focus();
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

	$scope.showModal = function (activity, modalSelector) {
		
		$scope.myStyle = {'cursor': 'wait'};
		$scope.isLoadingPopup = true;
		ClassroomRestFactory.getSubmissions(activity.id).then(function(results){
			$scope.selectedActivity = results.data;
			$scope.selectedActivity['longDescriptionHtml'] = 
				$sce.trustAsHtml($scope.selectedActivity.longDescription);

			$scope.studentActivity = $scope.selectedActivity.pdfs.length > 0 
				? awsMediaUrl + 'CEP_PDF_Contents/' + $scope.selectedActivity.pdfs.filter(function(x) { return x.role === 'S' })[0].s3Filename 
				: '';

			if ($scope.isNotStudent) {
				$scope.teacherGuide = $scope.selectedActivity.pdfs.length >= 2 
					? awsMediaUrl + 'CEP_PDF_Contents/' + $scope.selectedActivity.pdfs.filter(function(x) { return x.role !== 'S' })[0].s3Filename 
					: '';
			}

			$(modalSelector + ', .modal-backdrop').addClass('in').show();
	        $('body').addClass('modal-open');
	        $scope.myStyle = {'cursor': 'pointer'};
	        $scope.isLoadingPopup = false;
		}, function(error){
			console.log(error);
			alert('There was an error getting data. Please try again.');
			$scope.myStyle = {'cursor': 'pointer'};
			$scope.isLoadingPopup = false;
		});
		
	};

	$(document).on('click touchend', '.modal-footer .button, .modal .close' , function (e) {
		$(this).closest('.modal').hide();
		$('body').removeClass('modal-open');
		$('.modal-backdrop').removeClass('in').hide();
	});
} ]);


