cepApp.service('fyiManualInput', [ '$uibModal', 'FYIScoreService', '$location', 'FYIRestFactory', function($uibModal, FYIScoreService, $location, FYIRestFactory) {

	var modalDefaults = {
		animation : true,
		size : 'lg',
		templateUrl : 'cep/components/dashboard/page-partials/fyi-manual-input.html'
	};

	var modalOptions = {};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.interestCodeRawScore = {
					rRawScore : undefined,
					iRawScore : undefined,
					aRawScore : undefined,
					sRawScore : undefined,
					eRawScore : undefined,
					cRawScore : undefined
				};
				$scope.gender = '';

				$scope.modalOptions.submit = function(result) {

					// validate inputs
					if ($scope.interestCodeRawScore.rRawScore === undefined ||
						$scope.interestCodeRawScore.iRawScore === undefined ||
						$scope.interestCodeRawScore.aRawScore === undefined ||
						$scope.interestCodeRawScore.sRawScore === undefined ||
						$scope.interestCodeRawScore.eRawScore === undefined ||
						$scope.interestCodeRawScore.cRawScore === undefined) {
						$scope.rawScoreValidate = "Enter a score for all interest codes with a range from 0 to 30";
					} else {
						$scope.rawScoreValidate = undefined;
					}

					if ($scope.gender === '') {
						$scope.genderValidate = "Select gender";
					} else {
						$scope.genderValidate = undefined;
					}

					if ($scope.genderValidate !== undefined || $scope.rawScoreValidate !== undefined) {
						return false;
					}

					var rawScore = $scope.interestCodeRawScore;

					// create profile
					var fyiObject = {
						testId : undefined,
						gender : $scope.gender,
						rawScores : undefined
					};

					fyiObject.rawScores = [ {
						interestCd : 'R',
						rawScore : rawScore.rRawScore
					}, {
						interestCd : 'I',
						rawScore : rawScore.iRawScore
					}, {
						interestCd : 'A',
						rawScore : rawScore.aRawScore
					}, {
						interestCd : 'S',
						rawScore : rawScore.sRawScore
					}, {
						interestCd : 'E',
						rawScore : rawScore.eRawScore
					}, {
						interestCd : 'C',
						rawScore : rawScore.cRawScore
					} ];

					FYIRestFactory.saveTest().then(function(success) {
						// set the testId from the service
						fyiObject.testId = success.data;
						FYIScoreService.setFyiObject(fyiObject);

						if($location.path() === '/find-your-interest-score'){
							$location.path('/find-your-interest-score-reload');
						} else {
							$location.path('/find-your-interest-score');
						}

						$uibModalInstance.close(result);
					});
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);