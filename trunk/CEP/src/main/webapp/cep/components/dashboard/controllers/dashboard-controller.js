cepApp.controller('DashboardController', [ 
	'resource', '$scope', '$timeout', '$rootScope', '$route', '$templateCache', 'PortfolioRestFactory', 'numberTestTaken', '$window', 'UserFactory', 'fyiManualInput', 'cepManualInput', 'profileExist', 'userInterestCodes', 
	'PortfolioService', '$location', 'brightCareers', 'stemCareers', 'mediaCenterList', 'noteList', 'modalService', 'youTubeModalService', 'tiedLink', 'FYIScoreService', 'scoreSummary', 'asvabScore', 'isDemoTaken', 
	'accessCode', 'onetYearUpdated', 'registerAccessCodeReminder', 'AuthenticationService', 'editAccount', 'username', 'isUsingUnregisterableAccesscode','workValueScores',
	function(resource, $scope, $timeout, $rootScope, $route, $templateCache, PortfolioRestFactory, numberTestTaken, $window, UserFactory, fyiManualInput, cepManualInput, profileExist, userInterestCodes, 
	PortfolioService, $location, brightCareers, stemCareers, mediaCenterList, noteList, modalService, youTubeModalService, tiedLink, FYIScoreService, scoreSummary, asvabScore, isDemoTaken, 
	accessCode, onetYearUpdated, registerAccessCodeReminder, AuthenticationService, editAccount, username, isUsingUnregisterableAccesscode, workValueScores) {

	$scope.userName = username;
	$scope.accessCode = accessCode;
	$scope.currentUser = $rootScope.globals.currentUser;
	$scope.numberTestTaken = numberTestTaken;
	$scope.profileExist = profileExist.data;
	$scope.brightCareers = brightCareers[0];
	$scope.stemCareers = stemCareers[0];
	$scope.mediaCenterList = mediaCenterList;
	$scope.noteList = noteList;
	$scope.domainName = resource;
	$scope.tiedLink = tiedLink;
	$scope.isDemoTaken = isDemoTaken.data.r;
	$scope.onetYearUpdated = onetYearUpdated.data;
	
	$scope.userRole = $rootScope.globals.currentUser.role;

	$scope.interestCodeOne = userInterestCodes.interestCodeOne;
	$scope.interestCodeTwo = userInterestCodes.interestCodeTwo;
	$scope.interestCodeThree = userInterestCodes.interestCodeThree;
	$scope.scoreChoice = userInterestCodes.scoreChoice;
	
	$scope.scoreSummary = scoreSummary.data; // actual test score data
	$scope.asvabScore = asvabScore.data; // manually entered score data

	$scope.haveAsvabTestScores = (angular.isObject($scope.scoreSummary) && $scope.scoreSummary.hasOwnProperty('verbalAbility')); // do we have actual test scores
	$scope.haveManualAsvabTestScores = ($scope.asvabScore.length > 0 && !$scope.haveAsvabTestScores); // do we have manual test scores
	
	$scope.workValueScores = workValueScores.data;
	if ($scope.workValueScores) {
		$scope.workValueTopResults = [
				{
					"TypeId" : $scope.workValueScores.workValueOne
				},
				{
					"TypeId" : $scope.workValueScores.workValueTwo
				},
				{
					"TypeId" : $scope.workValueScores.workValueThree
				} ]
		$window.sessionStorage
				.setItem(
						'workValueTopResults',
						JSON
								.stringify($scope.workValueTopResults));
	}
	$scope.isWorkValueResults = angular.fromJson($window.sessionStorage.getItem('workValueTopResults')) ? true : false;

	// Show initial option to enter scores full width (no interest codes, no actual scores, no entered scores)
	$scope.showInitial = ($scope.interestCodeOne === undefined && !$scope.haveAsvabTestScores && !$scope.haveManualAsvabTestScores);
	if (!$scope.currentUser.isRegistered 
		&& !$window.sessionStorage.getItem('AccessCodeReminder')
		&& !isUsingUnregisterableAccesscode) {
		registerAccessCodeReminder.show({}, {});
	}

	/**** For Testing ****/
	//console.log($scope.scoreSummary);
	//console.log($scope.asvabScore);
	//$scope.haveAsvabTestScores = false;
	//$scope.haveManualAsvabTestScores = false;
	/*********************/

	$scope.editAccount = function() {
		var customOptions = {
			parentScope: $scope
		};
		editAccount.show({}, customOptions);
	};

	$rootScope.$on("ResetUserInfo", function(evt, data){ 
		$scope.userName = data.username;
	});

	$scope.getGenderCode = function() {
		// data should be f or m, but had m1 at one point hence substring
		var genderCode = ""; // none
		if ($scope.haveAsvabTestScores) {
			genderCode = $scope.scoreSummary.controlInformation.gender.substring(0,1).toLowerCase();
		}
		return genderCode;
	};

	$scope.getGenderFull = function(genderCode) {
		switch (genderCode) {
			case "f":
				genderCode = "Female";
				break;
			case "m":
				genderCode = "Male";
				break;
			default:
				genderCode = "Not Available";
				break;
		}
		return genderCode;
	};

	$scope.tiedLinkClick = function (){
		FYIScoreService.setTiedLinkClicked(true);
		$location.path('find-your-interest-score');
	};
	
	$scope.isAbleToReTakeTest = function () {
		var numOfTestTaken = parseInt($scope.numberTestTaken);
		// if the user has taken the FYI, show the "Re-Take the FYI"
		if (numOfTestTaken > 0) {
			// if the user is an Admin or PTI user, allow unlimited FYI tests
			if ($scope.isUnlimitedFyi()) {
				return true;
			}
			// if the user has taken 1 test, then the user is able to take 1 more
			if (numOfTestTaken < 2) {
				return true;
			}			
		}
		return false;
	};
	
	// Only Admins and PTI users can have unlimited FYI Tests
	$scope.isUnlimitedFyi = function () {
		return $scope.accessCode.unlimitedFyi === 1;
	};

	$scope.fyiManualInput = function() {
		fyiManualInput.show({}, {});
	};

	$scope.cepManualInput = function() {
		cepManualInput.show({}, {}).then(function(result) {
			//console.log(result);
			if (result) {
				if (result.hasOwnProperty('aFQTRawSSComposites')) {
					// Retrieved asvab scores
					$scope.scoreSummary = result;
					$scope.haveAsvabTestScores = true;
					$scope.haveManualAsvabTestScores = false;
					$scope.showInitial = false;
				} else {
					// Entered asvab scores
					$scope.haveAsvabTestScores = false;
					$scope.haveManualAsvabTestScores = true;
					$scope.showInitial = false;
					$scope.asvabScore.push({verbalScore : result.verbalScore, mathScore : result.mathScore, scienceScore : result.scienceScore});
				}
				
				// Update session storage
				$window.sessionStorage.setItem('manualScores', $scope.haveManualAsvabTestScores);
				$window.sessionStorage.setItem('sV', $scope.getScore('verbal'));
				$window.sessionStorage.setItem('sM', $scope.getScore('math'));
				$window.sessionStorage.setItem('sS', $scope.getScore('science'));
				$window.sessionStorage.setItem('sA', $scope.getScore('afqt'));

				// Update book stacks
				$scope.vBooks.score = $scope.getScore('verbal');
				$scope.mBooks.score = $scope.getScore('math');
				$scope.sBooks.score = $scope.getScore('science');
				$scope.scoreSorting();
			}
		});
	};

	/**
	 * Route user to portfolio page.
	 */
	$scope.routeToPortfolio = function() {
		var promise = PortfolioService.getIsPortfolioStarted();
		promise.then(function(success) {

			// if portfolio exists then skip instructions
			if (success > 0) {
				$location.path('portfolio');
			} else {
				$location.path('portfolio-directions');
			}
		}, function(error) {
			console.log(error);
		});

	};

	$scope.resultsInfo = function() {
		var customModalOptions = {
				headerText : 'ASVAB Results',
				bodyText : '<p>ASVAB Results More Information Coming Soon!</p>' + 
				'<p>Need content to display here.</p>'
			};
			var customModalDefaults = {size : 'lg'};
			modalService.show(customModalDefaults, customModalOptions);
	}
	
	$scope.interestCodes = function() {
		var customModalOptions = {
				headerText : 'Interest Code Legend',
				bodyText : ' <center><img src="/CEP/images/interest_codes_computer.png"></center> '
			};
			var customModalDefaults = {};
			modalService.show(customModalDefaults, customModalOptions);
	};
	
	
	$scope.selectedOccupation = function(onetSoc) {
		$location.url('/occufind-occupation-details/' + onetSoc);
	};
	
	$scope.interestCodeInfo = function(interestCode) {
		var modalOptions;
		switch (interestCode) {
		case "R":
			modalOptions = {
				headerText : 'Realistic',
				bodyText : '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
				'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
				'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
			};
			break;
		case "I":
			modalOptions = {
				headerText : 'Investigative',
				bodyText : '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
				'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
				'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
			};
			break;
		case "A":
			modalOptions = {
				headerText : 'Artistic',
				bodyText : '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
				'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
				'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
			};
			break;
		case "S":
			modalOptions = {
				headerText : 'Social',
				bodyText : '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
				'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
				'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
			};
			break;
		case "E":
			modalOptions = {
				headerText : 'Enterprising',
				bodyText : '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
				'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
				'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
			};
			break;
		case "C":
			modalOptions = {
				headerText : 'Conventional',
				bodyText : '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
				'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
				'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
			};
			break;
		}
		

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	};
	
	$scope.showYouTubeModal = function(videoName) {
		youTubeModalService.show({
			size : 'lg'
		}, {
			videoName : videoName
		});
	};

	/**
	 * Get scores for display
	 */
	$scope.getScore = function(area) {
		var score = 0;

		switch (area) {
			case "verbal":
				if ($scope.haveAsvabTestScores) {
					score = ($scope.scoreSummary 
						&& $scope.scoreSummary.verbalAbility
						&& $scope.scoreSummary.verbalAbility.va_SGS) ? Number($scope.scoreSummary.verbalAbility.va_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore);
				}
				break;
			case "math":
				if ($scope.haveAsvabTestScores) {
					score = ($scope.scoreSummary
						&& $scope.scoreSummary.mathematicalAbility
						&& $scope.scoreSummary.mathematicalAbility.ma_SGS) ? Number($scope.scoreSummary.mathematicalAbility.ma_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore);
				}
				break;
			case "science":
				if ($scope.haveAsvabTestScores) {
					score = ($scope.scoreSummary
						&& $scope.scoreSummary.scienceTechnicalAbility
						&& $scope.scoreSummary.scienceTechnicalAbility.tec_SGS) ? Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore);
				}
				break;
			case "afqt": // no manual entry for AFQT
				score = ($scope.scoreSummary
					&& $scope.scoreSummary.aFQTRawSSComposites
					&& $scope.scoreSummary.aFQTRawSSComposites.afqt) ? Number($scope.scoreSummary.aFQTRawSSComposites.afqt) : 0;
				break;
			default:
				break;
		}
		return score;
	};
	
	// Add scores to session.
	if ($scope.haveAsvabTestScores) {
		$window.sessionStorage.setItem('manualScores', false);
		$window.sessionStorage.setItem('sV', $scope.getScore('verbal'));
		$window.sessionStorage.setItem('sM', $scope.getScore('math'));
		$window.sessionStorage.setItem('sS', $scope.getScore('science'));
		$window.sessionStorage.setItem('sA', $scope.getScore('afqt'));
	} else if ($scope.haveManualAsvabTestScores) {
		$window.sessionStorage.setItem('manualScores', true);
		$window.sessionStorage.setItem('sV', Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore));
		$window.sessionStorage.setItem('sM', Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore));
		$window.sessionStorage.setItem('sS', Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore));
	}

	// Create book objects for sort/ranking - display gray book (score: 0) for manual scores
	$scope.vBooks = { 
		name: "verbal",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sV') : 0
		score: $window.sessionStorage.getItem('sV')
	};

	$scope.mBooks = {
		name: "math",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sM'): 0
		score: $window.sessionStorage.getItem('sM')
	};

	$scope.sBooks = {
		name: "science",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sS') : 0
		score: $window.sessionStorage.getItem('sS')
	};

	/**
	 * Calculate book sprite x pos from ranked score
	 * 1 book = lowest score
	 * 2 books = middle score
	 * 3 books = highest score
	 * Ties would use just 2- and 3-book stacks, with the duplicate 
	 * determined by whether the tie was with the highest or the lowest score
	 */
	$scope.scoreSorting = function() {
		$scope.scoresToSort = [$scope.vBooks, $scope.mBooks, $scope.sBooks];
		$scope.sortedScores = [];

		for (var i = 0; i < $scope.scoresToSort.length; i++) {
			$scope.sortedScores.push($scope.scoresToSort[i]);
		}

		$scope.sortedScores.sort(function(a, b) {
			return a.score-b.score;
		});

		for(var i = 0; i < $scope.sortedScores.length; i++) {
			$scope.sortedScores[i].rank = i + 1;
			$scope.sortedScores[i].tie = false;
		}
		$scope.scoreRanking();
	};

	$scope.scoreRanking = function() {
		for (var k = 0; k < $scope.sortedScores.length; k++) {
			for (var h = 1; h < $scope.sortedScores.length + 1; h++) {
				if ($scope.sortedScores[k+h] !== undefined) {
					if ($scope.sortedScores[k+h].tie !== true) {
						if ($scope.sortedScores[k].score === $scope.sortedScores[h + k].score) {
							$scope.sortedScores[k].rank = k + 1;
							$scope.sortedScores[h + k].rank = k + 1;
							$scope.sortedScores[k].tie = true;
							$scope.sortedScores[h + k].tie = true;
						}
					}
				}    
			}
			
			// if tie scores, 2 books for low score tie, 3 books for high score tie
			if ($scope.sortedScores[k].tie) {
				if ($scope.sortedScores[k].rank == 1) {
					$scope.sortedScores[k].rank = 2; // low tie
				} else {
					$scope.sortedScores[k].rank = 3; // high tie
				}
			}
			
			// no books for 0 score
			if ($scope.sortedScores[k].score == 0) {
				$scope.sortedScores[k].rank = 0; // no score
			}

			// set xPos based on rank, -60px incraments
			$scope.sortedScores[k].xPos = $scope.sortedScores[k].rank * -32; // larger book stack (on dash)
			$scope.sortedScores[k].xPosSmall = $scope.sortedScores[k].rank * -32; // smaller book stack (in left nav)
			
		}


		// Add book objects to session
		$window.sessionStorage.setItem('vBooks', angular.toJson($scope.vBooks));
		$window.sessionStorage.setItem('mBooks', angular.toJson($scope.mBooks));
		$window.sessionStorage.setItem('sBooks', angular.toJson($scope.sBooks));
	};

	/**
	 * Get book sprite x pos
	 */
	$scope.getBookPos = function(area) {
		// return bsackground style position object
		switch (area) {
			case "verbal":
				return {"background-position": $scope.vBooks.xPos + "px 0px"};
			case "math":
				return {"background-position": $scope.mBooks.xPos + "px 0px"};
			case "science":
				return {"background-position": $scope.sBooks.xPos + "px 0px"}; 
		}
	};

	/**
	 * Toggle score book/table display
	 */
	$scope.showScoresTable = false;
	$scope.toggleScoreView = function() {
		$scope.showScoresTable = $scope.showScoresTable === false;
	};

	// Get gender from asvab scor data if we have it, store it globally
	$rootScope.globals.currentUser.gender = $scope.getGenderFull($scope.getGenderCode());
	
	// Initial book sorting/ranking
	$scope.scoreSorting();

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
		
		/**
		 * Flyout JQuery. Teacher demo.
		 */
		if(($scope.userRole === 'E' || $scope.userRole === 'D') 
			&& $scope.isDemoTaken === 0) {
			
			$(function() {
				/**
				 * Flyout modals
				 */
				$('.flyout_step_1').flyout({
					content: '<h2 id="tour-title" class="swlFlyout_title">Get familiar with<br/> MY ASVAB CEP</h2><button type="button" class="next1 next-step" data-step="1">TAKE THE TOUR</button><i class="click1">&times;</i>',
					html: true,
					placement: 'top'
				});

				$('.flyout_step_2').flyout({
					title: 'UNDERSTAND ASVAB SCORES',
					content: '<p>Career Exploration Scores represent strengths in Verbal, Math, and Science/Technical skills. </p><button type="button" class="next2 next-step" data-step="2">next</button><i class="click2">&times;</i>',
					html: true,
					placement: 'left',
					trigger: 'manual'
				});

				$('.flyout_step_3').flyout({
					title: 'UNDERSTAND ASVAB SCORES',
					content: '<p>The most books signify skill strength.</p><button type="button" class="next3 next-step" data-step="3">next</button><i class="click3">&times;</i>',
					html: true,
					placement: 'right',
					trigger: 'manual'
				});

				$('.flyout_step_4').flyout({
					title: 'UNDERSTAND ASVAB SCORES',
					content: '<p>Access online score report and Understanding Scores tutorial.</p><button type="button" class="next4 next-step" data-step="4">next</button><i class="click4">&times;</i>',
					html: true,
					placement: 'left',
					trigger: 'manual'
				});

				$('.flyout_step_5').flyout({
					title: 'FIND YOUR INTERESTS ',
					content: '<p>90 item interest inventory based on John Holland<em>s</em> codes of career choice.</p><button type="button" class="next5 next-step" data-step="5">next</button><i class="click5">&times;</i>',
					html: true,
					placement: 'top',
					trigger: 'manual'
				});

				$('.flyout_step_6').flyout({
					title: 'SEARCH THE OCCU-FIND',
					content: '<p>Sort 1,000+ careers using your unique skill & interest combination to find careers that match. Learn about the variety of ways to get started in any career.</p><button type="button" class="next6 next-step" data-step="6">next</button><i class="click6">&times;</i>',
					html: true,
					placement: 'top',
					trigger: 'manual'
				});

				$('.flyout_step_7').flyout({
					title: 'INTEGRATE CAREER EXPLORATION INTO YOUR CURRICULUM',
					content: '<p>Create a plan, track accomplishments, and favorite careers. </p><button type="button" class="next7 next-step" data-step="7">next</button><i class="click7">&times;</i>',
					html: true,
					placement: 'top',
					trigger: 'manual'
				});

				$('.flyout_step_8').flyout({
					title: 'INTEGRATE CAREER EXPLORATION INTO YOUR CURRICULUM',
					content: '<p>Access activities you can use now to connect your classroom to the real world.</p><button type="button" class="next8 final-step">Finished</button><i class="click8">&times;</i>',
					html: true,
					trigger: 'manual'
				});

				/**
				 * Click events for flyout modals
				 */
				$("document").ready(function(){
					$(".swlFlyout ").addClass("hiddan_arrow");
				});

				$(document).on('click', '.next-step', function (e) {
					var thisStep = $(this).data('step');
					$('.flyout_step_' + thisStep).trigger('click');
					$('.flyout_step_' + (thisStep + 1)).flyout('show');
					$('html, body').animate({scrollTop: $('.flyout_step_' + (thisStep + 1)).offset().top - 300}, 600);
				});

				function hideFlyout() {
					$(this).flyout('hide');
					$(document).off('click', '.' + this.className, hideFlyout);
				}

				for (var i = 1; i < 9; i++) {
					$(document).on('click', '.flyout_step_' + i, hideFlyout);
					$(document).on('click', '.click' + i, function () {
						var className = this.className;
						var index = className.substr(5, (className.length - 5));
						$('.flyout_step_' + index).trigger('click');
					});
				}

		        $scope.demoButtonDisable = false;
				$(document).on('click', '.click8,.next8,.final-step', function(e) {
					// Store teacher demo completion information to database.
					if (!$scope.demoButtonDisable) { // disable if but was clicked
						$scope.demoButtonDisable = true; // disable button
						var promise = PortfolioRestFactory.insertTeacherDemoIsTaken();
						promise.then(function(response) {
							$("#classroom_activities").flyout('hide');
						}, function(reason) {
							$scope.demoButtonDisable = false; // enable button if failed
						}, function(update) {
						});
					}
				});
		    });
			
			$(function() {
			    if ($(window).width() < 640) {
					
					$("document").ready(function(){
						$('html, body').animate({scrollTop: $('.flyout_step_1').offset().top -260 });
					});

					// modify each flyout to be at the bottom
					$('.flyout_step_2').flyout({
						title: 'UNDERSTAND ASVAB SCORES',
						content: '<p>Career Exploration Scores represent strengths in Verbal, Math, and Science/Technical skills. </p><button type="button" class="next2 next-step" data-step="2">next</button><i class="click2">&times;</i>',
						html: true,
						placement: 'top',
						trigger: 'manual'
					});

					$('.flyout_step_3').flyout({
						title: 'UNDERSTAND ASVAB SCORES',
						content: '<p>The most books signify skill strength.</p><button type="button" class="next3 next-step" data-step="3">next</button><i class="click3">&times;</i>',
						html: true,
						placement: 'bottom',
						trigger: 'manual'
					});

					$('.flyout_step_4').flyout({
						title: 'UNDERSTAND ASVAB SCORES',
						content: '<p>Access online score report and Understanding Scores tutorial.</p><button type="button" class="next4 next-step" data-step="4">next</button><i class="click4">&times;</i>',
						html: true,
						placement: 'bottom',
						trigger: 'manual'
					});
				}
			});

			$(document).on('click', '.next-step', function (e) {
				e.stopPropagation();
				e.preventDefault();
			});

			$(document).ready(function () {
				$('.flyout_step_1').flyout('show');
			});
		}
	});
} ]);
