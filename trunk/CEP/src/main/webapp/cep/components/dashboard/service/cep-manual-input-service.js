cepApp.service('cepManualInput', [ 
	'$uibModal', 'FYIScoreService', '$location', 'FYIRestFactory', '$rootScope', '$q', 'PortfolioRestFactory', 
	function($uibModal, FYIScoreService, $location, FYIRestFactory, $rootScope, $q, PortfolioRestFactory) {
	
	var modalDefaults = {
		animation : true,
		size : 'lg',
		templateUrl : 'cep/components/dashboard/page-partials/cep-manual-input.html'
	};

	var modalOptions = {};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {
		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$rootScope', '$uibModalInstance', '$route', function($scope, $rootScope, $uibModalInstance, $route) {
				$scope.modalOptions = tempModalOptions;

				$scope.enter = $scope.modalOptions.enter;
				$scope.retrieve = $scope.modalOptions.retrieve;

				$scope.displayOptions = function(viewFor) {
					switch (viewFor) {
						case 'enter':
							$scope.enter = true;
							$scope.retrieve = false;
							break;
						case 'retrieve':
							$scope.retrieve = true;
							$scope.enter = false;
							break;
						case 'initial':
							$scope.enter = false;
							$scope.retrieve = false;
							break;
					}
				}
				

				$scope.modalOptions.submitAsvabScores = function(scoreObj) {
					// add userId - only insert scores not update from dahsboard
					var promise = PortfolioRestFactory.insertAsvabScore(scoreObj);
					//var promise = PortfolioRestFactory.updateAsvabScore(scoreObj); // returns bad request
					promise.then(function(response) {
						$route.reload();
						$uibModalInstance.close(scoreObj);
					}, function(reason) {
						console.log(reason);
						alert("Failed to save ASVAB score. Try again. If error persist, please contact site administrator.");
					}, function(update) {
						alert('Got notification: ' + update);
					});
					
				};
				$scope.modalOptions.check = function() {
					var accessCode = $scope.accessCode;
					//var userId = $rootScope.globals.currentUser.userId;
					
					var promise = FYIRestFactory.getTestScoreWithAccessCode(accessCode);
					promise.then(function(response) {
						$uibModalInstance.close(response.data);
					}, function(reason) {
						console.log(reason);
						alert("Failed to retrieve your ASVAB scores. Try again. If error persist, please contact site administrator.");
					}, function(update) {
						alert('Got notification: ' + update);
					});
				};
				$scope.modalOptions.getDmdcScore = function() {
					var accessCode = $scope.accessCode;
					//var userId = $rootScope.globals.currentUser.userId;
					
					var promise = FYIRestFactory.getTestScoreByAccessCode(accessCode);
					promise.then(function(response) {
						$uibModalInstance.close(response.data);
						$route.reload();
					}, function(reason) {
						console.log(reason);
						alert("Failed to retrieve your ASVAB scores. Try again. If error persist, please contact site administrator.");
					}, function(update) {
						alert('Got notification: ' + update);
					});
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}
		return $uibModal.open(tempModalDefaults).result;
	};

} ]);