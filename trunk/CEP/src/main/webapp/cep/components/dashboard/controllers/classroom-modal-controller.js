cepApp.controller('ClassroomModalController', [ '$scope', 'ClassroomModalService', '$uibModal', function($scope, ClassroomModalService, $uibModal) {

		 $scope.classroomModal = function(type) {
			 ClassroomModalService.showModal({
					size : 'lg'
				}, {
				 type: type
			 });
		 }

} ]);