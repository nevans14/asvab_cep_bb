cepApp.service("UserService", function (appUrl, $http) {
    var userService = {};

    userService.getUsername = function () {
      return $http.get(appUrl + 'user/getUsername');
    };

    userService.getKeepAlivePing = function () {
      return $http.get(appUrl + 'user/ping');
    };

    userService.getIsUsingUnregisterableAccesscode = function () {
      return $http.get(appUrl + 'user/isUsingUnregistrableAccesscode');
    };

    return userService;
});