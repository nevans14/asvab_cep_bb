cepApp.factory('ClassroomRestFactory', [ '$http', 'appUrl',
		function($http, appUrl) {

			var classroomRestFactory = {};

			classroomRestFactory.getClassroomActivities = function() {
				return $http.get(appUrl + 'classroom-activity/get-activities');
			}

			classroomRestFactory.getClassroomActivityCategories = function() {
				return $http.get(appUrl + 'classroom-activity/get-categories');
			}
			
			classroomRestFactory.getSubmissions = function(activityId) {
				return $http.get(appUrl + 'classroom-activity/' + activityId + '/submissions');
			}

			return classroomRestFactory;

		} ]);