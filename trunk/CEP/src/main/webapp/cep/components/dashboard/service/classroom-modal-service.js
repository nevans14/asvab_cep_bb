cepApp.service('ClassroomModalService', [ '$uibModal', 'resource', 'ssoUrl', function($uibModal, resource, ssoUrl) {	
	
	var getCepLink = function () {
		if (location.href.indexOf('localhost') > -1) {
			return "http://localhost:8080/CEP/";
		} else if (location.href.indexOf('asvabcep.humrro.org') > -1) {
			return "http://asvabcep.humrro.org/";
		} else if (location.href.indexOf('asvabcep-stg.humrro.org') > -1) {
			return "http://asvabcep-stg.humrro.org/";
		} else {
			return "http://asvabprogram.com/";
		}
	}
	
	var modalDefaults = {
			animation : true,
			size : 'lg',
			templateUrl : 'cep/components/dashboard/page-partials/classroom-modal.html'
		};

	var modalOptions = {};
	
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				
				$scope.documents = resource + 'pdf/';

				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				
				switch (customModalOptions.type) {
					case "inspire":
						$scope.title = "Share to Inspire";
						$scope.text =  " <p>After participating in the ASVAB CEP, make a short video about how the program helped you.  Inspire other high school students to take the" +
									   " time to learn about careers, ways to get there, and how important self-knowledge is while making these types of decisions.  Identify which" +
									   " pieces of information were most helpful.</p><strong>Don&#39;t forget to #whatsyourdreamjob #ASVABCEP</strong>";
						$scope.link =  "ASVAB_CEP_Share_to_Inspire.pdf"
						break;
					case "listen":
						$scope.title = "Listen Before You Launch";	
						$scope.text =  "<p>For this project, you will conduct an interview with someone who has retired from his or her profession.  Ask them the following questions:</p>"+
								" <p>1.	How did you choose your career?</p>"+
								" <p>2. 	When did you decide this is the career you wanted?</p>"+
								" <p>3. 	How did you prepare for your first job?</p>"+
								" <p>4.	What was the first thing you learned on the job?</p>"+
								" <p>5.	Tell me about how your career field has changed over the years.  Can you tell me about a task you did every day and how it changed?</p>"+
								" <p>6. 	What are the pros and cons of being in this career?</p>"+
								" <p>7.	Who helped you the most in your career?</p>"+
								" <p>8.	What is the best advice you can give to me about choosing this career path?</p>"+
								" <p>9. 	Can you teach me something related to this type of work?</p>"+	
								" <p>Now, using the information you received, create a presentation about the person&#39;s life and profession.  How did this help you prepare for your" +
								" career goals. How will you adapt to the changes that will change your job over time?  What does the OCCU-Find say about the national employment" +
								" outlook for your career?  (to find this information, use the OCCU-Find search tool to locate your occupation, then under employment statistics," +
								" where the map is located, click more details.)</p>";
						$scope.link = 	"ASVAB_CEP_Listen_Before_you_Launch.pdf"							
						break;
					case "cte":
						$scope.title = "CTE Reel Review";				
						$scope.text =  "<p>Identify a CTE career you are interested in, and find a movie or TV show that portrays someone in that occupation.  Using the OCCU-Find, under &#34;What" +
								" they do&#34;, check out the job tasks that are listed.  Which of these job tasks are portrayed in the movie or show?  What tools were used to help them" +
								" in their job?  Do these tools actually exist?  (Find reference) Was this career presented realistically?  Create a YouTube video of your review of" +
								" the movie in terms of how it depicts career information.</p> <strong>Don&#39;t forget to #ASVABCEP</strong>";
						$scope.link = 	"ASVAB_CEP_CTE_Reel_Review.pdf"							
						break;
					case "essay":
						$scope.title = "Career Exploration Essay";				
						$scope.text =  "<p>The OCCU-Find contains many data points that are important to explore while you are thinking about careers.  Choose three jobs you have identified" +
								" as a potential career choice. Explain why they are interesting to you. Your essay should include a plan A and and plan B of how to reach each of" +
								" the career goals.  For instance, if you chose a job that requires an advanced degree, but do not have money for an advanced degree, write about" +
								" how you can still reach you career goals. Remember, there are many pathways, many ways to get experience, and many related jobs.</p>";
						$scope.link = 	"ASVAB_CEP_Career_Exploration_Essay.pdf"
						break;
					case "education":
						$scope.title = "My Education and Career Plans";				
						$scope.text =  "<p>Complete My Education and Career Plans worksheet. Then complete portfolio and print or email to your counselor.</p>";
						$scope.link = 	"ASVAB_CEP_Edu_and_Career_Plans.pdf"
						break;
					case "benefit":
						$scope.title = "Career Decisions: Cost Benefit Analysis";				
						$scope.text =  "<p>You are 24 years old and looking forward to being a financially independent individual. You will need to create a detailed future financial plan and" +
								" monthly budget to live the kind of life you have always dreamed of! You currently have $6500 in a Savings account. Since you are independent, you" +
								" have no support from roommates or parents, but if you are going to be married or have children, you will need to add in all the additional costs." +
								" First, you will need to use the OCCU-Find to choose a career, and determine how you will reach this career goal by age 24. Also, you will need to" +
								" take into account the cost of living differences.  If you live in CA vs. TN, the salaries are higher, but so is the cost of living. "+
							    " Your presentation will need to include the following elements (and references to sites you used for figures):  Career earnings and federal and state" +
							    " taxes deducted from your pay check, how you got there and any costs associated with that path (school loans, etc), geographic location, vehicle," +
							    " housing, food, personal expenses (cell phone, cable, utilities, gas, clothes, makeup, deodorant, etc.), gifts, donations, vacations, etc.</p>";
						$scope.link = 	"ASVAB_CEP_Career_Decisions-Cost_Benefit_Analysis.pdf"
						break;
					case "collage":
						$scope.title = "Career Collage Project";				
						$scope.text = "<p>Think back on all the times someone has asked you about your future career goals:  What do you want to be when you grow up?</p>" +
								" <p>You will be using poster board to make a collage of all the jobs you have ever answered that question with&#46;&#46;&#46;. ballet dancer," +
								" race car driver, doctor, trash collector, bagel shop owner, etc. Find a picture of each one (you will need at least 25 pictures).</p>" +
								" <p>Now, they need to be organized in some way. What was appealing about each of these jobs to you?  Were your answers related to" +
								" your aptitude? or your skills?  Were your answers influenced by others? Were your answers related to your interests? What do all" +
								" these careers have in common?  Look up the careers using the OCCU-Find tool, is the description of the job what you thought it" +
								" would be like?  Now, ask yourself which of these careers would be attainable for you.  Present your Career Collage project to your" +
								" class.  Make sure you can answer questions like:  What is a typical day like for each of the jobs you have considered throughout your" +
								" life?  What classes in high school would you take to prepare for each of these careers?  What is the outlook like for each of these" +
								" careers?  What was the most interesting thing you learned about each of your careers?</p>";
						$scope.link = 	"ASVAB_CEP_Career_Collage_Project.pdf"
						break;
					case "story":
						$scope.title = "Write Your Story";				
						$scope.text =  "<p>Create a picture book (paper and pencil or electronic) about a career you are exploring.  This book will need to contain pictures that demonstrate what you" +
								" like about this career.  The book will be for children 3 and up and should contain important information to help the child understand the career, explain" +
								" the day to day tasks, and share your excitement.  The story should not be complicated, but should contain at least 10 pages.  You can draw, use clip art," +
								" photographs, magazine clippings, or paint in order to create your illustrations for your story.  Don&#39;t forget to include an &#39;about the author&#39; page with" +
								" your short bio and how you identified this career as one worth pursuing.</p>"; 
						$scope.link = 	"ASVAB_CEP_Write_Your_Story.pdf"
						break;
					case "reel":
						$scope.title = "Reel Career";				
						$scope.text =  "<p>Identify an occupation you are interested in, and find a movie or TV show that portrays someone in that occupation.  Using the OCCU-Find, under &#34;What they do&#34;," +
								" check out the job tasks that are listed.  Which of these job tasks are portrayed in the movie or show?  Was this career presented realistically?  Create a" +
								" YouTube video of your review of the movie in terms of how it depicts career information.</p> <strong>Don&#39;t forget to #ASVABCEP</strong>";
						$scope.link = 	"ASVAB_CEP_Reel_Career.pdf"
						break;
					case "history":
						$scope.title = "History and New Found Knowledge";				
						$scope.text =  "<p>For this project, you will conduct an interview with someone who has retired from his or her profession.  Ask them the following questions:</p>"+
								" <p>1.	 How did you choose your career?  When did you decide this is the career you wanted?</p>"+
								" <p>2.	How did you prepare for your first job interview in this career?</p>"+
								" <p>3.	What was the first thing you learned on the job?</p>"+
								" <p>4.	Tell me about how your career field has changed over the years.  Can you tell me about a task you did every day and how it changed?</p>"+
								" <p>5.	What are the pros and cons of being in this career?</p>"+
								" <p>6.	Who helped you the most in your career?</p>"+
								" <p>7.	What is the best advice you can give to me about choosing a career path?</p>"+
								" <p>Now, using the information you received, create a power point presentation about the person&#39;s life and profession.  Be sure to include slides about how" +
								" you will use this information to prepare for your career goals.  Other slides to include:  How will you adapt to the changes that are most likely going" +
								" to change your job over time?  What does the OCCU-Find say about the national employment outlook for your career?  (to find this information, use the" +
								" OCCU-Find search tool to locate your occupation, then under employment statistics, where the map is located, click more details.)</p>";
						$scope.link = 	"ASVAB_CEP_History_and_New_Found_Knowledge.pdf"
						break;
					case "family":
						$scope.title = "Family Career Tree";				
						$scope.text =  "<p>Research your family&#39;s career history and create a family tree tracing back four or more generations.  Include at least 2 stories about your family" +
								" members and why they chose a certain profession.  Include names, birthdays, locations, career information (where they worked, what they did, how long" +
								" they worked, how they got there etc).  If you have family members who served in the military, remember to ask them about their specific jobs." +
								"  Do you think it is easier or more difficult to find a career now?  Use information form the OCCU-Find to back up your answer.</p>";
						$scope.link = 	"ASVAB_CEP_Familiy_Career_Tree.pdf"
						break;
					case "plan":
						$scope.title = "Plan Your Adventure";				
						$scope.text =  "<p>The OCCU-Find contains many data points that are important to explore while you are thinking about careers.  Choose three STEM jobs you have identified" +
								" as a potential career choice. Explain why they are interesting to you. Your essay should include a plan A and and plan B of how to reach each of the" +
								" career goals.  For instance, if you chose a job that requires an advanced degree, but do not have money for an advanced degree, write about how you" +
								" can still reach you career goals. Remember, there are many pathways, many ways to get experience, and many related jobs.</p>";
						$scope.link = 	"ASVAB_CEP_Plan_Your_Adventure.pdf"
						break;
					case "old":
						$scope.title = "Learn from the Old Win with STEM";				
						$scope.text =  "<p>For this project, you will conduct an interview with someone who has retired from a STEM career.  Ask them the following questions:</p>" +
								" <p>1.   How did you choose your career?</p>" +
								" <p>2.   When did you decide this is the career you wanted?</p>" +
								" <p>3.   How did you prepare for your first job?</p>" +
								" <p>4.   What was the first thing you learned on the job?</p>" +
								" <p>5.   Tell me about how your career field has changed over the years.  Can you tell me about a task you did every day and how it has changed?</p>" +
								" <p>6.   What are the pros and cons of being in this career?</p>" +
								" <p>7.   Who helped you the most in your career?</p>" +
								" <p>8.   What is the best advice you can give to me about choosing this career path?</p>" +
								" <p>9.   How do you see this job changing in the future?</p>" +
								" <p>Now, using the information you received, create a presentation about the person&#39;s life and profession.  How did this help you prepare for your" +
								" career goals.  How will you adapt to the changes that will change your job over time?  What does the OCCU-Find say about the national employment" +
								" outlook for your career?  (to find this information, use the OCCU-Find search tool to locate your occupation, then under employment statistics," +
								" where the map is located, click more details.)</p>";
						$scope.link = 	"ASVAB_CEP_Learn_from_the_Old_Win_with_STEM.pdf"
						break;
					case "freshman":
						$scope.title = "Freshman Profile";				
						$scope.text =  "<p>Research schools providing training for your career of choice by selecting College Info under the HOW TO GET THERE section on each OCCU-Find career" +
								" detail page. For your schools of choice, prepare a collage depicting the profile of the ideal freshman: ACT/SAT score, class rank, GPA, etc. Include" +
								" admission requirements and deadlines.</p>";
						$scope.link = 	"ASVAB_CEP_Freshman_Profile.pdf"
						break;
					case "real":
						$scope.title = "What do real jobs look like?";				
						$scope.text =  "<p>After exploring the OCCU-Find, identify 5 careers that interest you.  Look for 2 job postings on <a target=\"_blank\" href=\"http://www.usajobs.gov\">www.usajobs.gov</a> for one of the careers you have chosen" +
									   " to explore.  After reading the job postings, complete the following:</p>"+
									   " <p>1.	Under the Duties is a Summary section that explains a little about the agency and where you would be working.  Of the two job ads, which agency or location would you prefer?  Why? (Hint:  Sometimes there are additional links to learn more about the agency under Agency contact information)</p>" +
									   " <p>2.	Draft 5 interview questions you would need to be prepared to answer before you applied to this job.</p>" +
									   " <p>3.	How would you be evaluated for these jobs?</p>" +
									   " <p>4.	What benefits are included for the jobs?</p>" +
									   " <p>5.	What are 5 questions you would need to ask the employer to help you make your decision to work with them?</p>" ;
						$scope.link = 	"ASVABCEP_What_Do_Real_Jobs_Look_Like.pdf"
						break;
					case "interest":
						$scope.title = "Interests and Needs";				
						$scope.text =  "<p>After taking the FYI, your interests were identified, and you used your interests to look for a career.  Now that you have identified some careers to" +
									   " explore, let&#39;s look at how your career choices will help others.  Abraham Maslow published a theory on human motivation.  His theory outlines certain" +
									   " needs that must be met before someone can fulfill their greatest potential.  After you have read about Maslow&#39;s theory, take a look at 5 of the jobs you" +
									   " have chosen to explore.  Write an essay that explains:</p>"+
									   " <p>o	What needs will you be fulfilling for others in each of those careers?" +
									   " <p>o	How will having those careers help you fulfill your needs?" +
									   " <p>o	How important are your interests when choosing a career?  Are they more, less, or the same importance as your aptitude?  Why? (there is no &#34;right&#34; or &#34;wrong&#34; answer to this question).";
						$scope.link = 	"ASVABCEP_Interests_Needs.pdf"
						break;
					case "giving":
						$scope.title = "Giving Back";				
						$scope.text =  "<p>Social science is the study of human society and social relationships.  After using the OCCU-Find to explore careers, choose two careers and create a volunteer" +
									   " project that would allow you to gain skills in helping you for both careers.  Tale a look at the &#34;What they do&#34; section and see where these careers overlap.  For" +
									   " example, if I was interested in becoming an Oral and Maxillofacial Surgeon and a Forest Fighter, I could create a volunteer project that involved communicating with" +
									   " others since both jobs require communication.  My project could focus on communicating information about fire prevention or the importance of taking care of your teeth" +
									   " and gums.  Your project should include an impact statement (how helpful or important this project is), a goal (what you hope to accomplish), a list of materials needed," +
									   " where this will be held, what audience you are focusing on, and a plan of action.  After the class presents their projects, take a vote and pick the one that the class is" +
									   " most excited about doing.  Keep us posted and tag @ASVABCEP so we can see how you are giving back to your community while gaining skills!</p>";
						$scope.link = 	"ASVABCEP_Giving_Back.pdf"
						break;
					case "video":
						$scope.title = "Elevator Pitch Video";				
						$scope.text =  "<p>In the business world, an &#34;elevator pitch&#34; is a quick, passionately delivered description of something you want to do and why. You never know when you&#39;ll be standing in" +
									   " line or sharing an elevator ride with someone who can help or even make your dream job happen!  To make the most of such an opportunity, you must have an elevator pitch ready to roll" +
									   " at a moment&#39;s notice.</p>"+
									   "</br>"+
									   "<p>Prepare your #whatsyourdreamjob elevator pitch!  And record it!</p>"+
									   "</br>"+
									   "<p>Your video <b>must</b> include: </p>"+
									   "<p>&#8226;	Your name</p>"+
									   "<p>&#8226;	What you want to do in your career</p>"+
									   "<p>&#8226;	Why you chose this career (Tell a little story, why are you passionate about this)</p>"+
									   "<p>&#8226;	What materials/resources you&#39;ll need (college money?  Tools? Access to agents?)</p>"+
									   "<p>&#8226;	What obstacles you anticipate and your plan to navigate those obstacles </p>"+
									   "<p>&#8226;	Why someone should help you reach these goals</p>"+
									   "<p>&#8226;	Graciously thank the person for listening to you</p>"+
									   "<p>Be organized with your thoughts and speak passionately about your career goals.  Don&#39;t forget to tag @ASVABCEP and #youdecide #whatsyourdreamjob so we can highlight" +
									   " your elevator pitch!</p>";
						$scope.link = 	"ASVABCEP_Elevator_Pitch_Video.pdf"
						break;
					case "cost":
						$scope.title = "Cost of a Career";
						$scope.text =  " <p>Choose a career using the OCCU-Find. Identify two paths to outline how you could reach this career goal. Make a list of costs associated" +
									   " with each path. Include cost of living, location, the amount of time that it will take to reach your goal, plus any other factors that will affect" +
									   " the cost. Create a comparison chart to show the total amount of cost associated with each path. *Tip: Use the living wage calculator to develop your" +
									   " chart: <a target=\"_blank\" href=\"http://livingwage.mit.edu\">http://livingwage.mit.edu/</a></p>";
						$scope.link =  "ASVAB_CEP_Cost_of_a_Career.pdf"
						break;
					case "math":
						$scope.title = "Math Skills Analysis";
						$scope.text =  "<p>Using the OCCU-Find, search for jobs that match your interest codes and math skills. Identify five careers that interest you.</p>" +
									   "<p>What type of math is important for each of these jobs? How is math used in these jobs? Make a chart like the one below:</p>"+
									   "<img src=\"images/ASVAB_CEP_Math_Skills_Analysis_table.jpg\" alt=\"Math Skills Analysis Table\" style=\"max-width: 100%\">";
						$scope.link =  "ASVAB_CEP_Math_Skills_Analysis.pdf"
						break;
					case "scientific":
						$scope.title = "Scientific Leaps and Bounds";
						$scope.text =  " <p>Choose a scientific advancement made in the last five years. Do some online research to answer these questions.</p>" +
						   			   " <p>1.	Why do you think this advancement is important?</p>" +
							           " <p>2.	How was this advancement made?</p>" +
								       " <p>3.	What specific jobs does this discovery impact?  How?</p>" +
								       " <p>4.	Make a video explaining why you think this advancement is important.</p>" +
								       " <p>5.	Would you rather be on the discovery side or the application side of this discovery? Why?</p>" ;
						$scope.link =  "ASVAB_CEP_Scientific_Leaps_and_Bounds.pdf"
						break;
					case "stem":
						$scope.title = "Which STEM Field is for Me?";
						$scope.text =  " <p>Choose two subfields in science you are most interested in learning about: Biology, Chemistry, Physics, Health, Psychology, etc. </p>" +
									   " <p>Using the OCCU-Find, identify three careers in each of the subfields that interest you. </p>" +
									   " <p>What classes/training could you complete in high school to help you prepare for these careers?  </p>" +
									   " <p>Make an infograph about one of these subfields to encourage other students to explore careers in this area </p>";
						$scope.link =  "ASVAB_CEP_Which_STEM_Field_is_for_Me.pdf"
						break;
					case "linkedin":
						$scope.title = "Create a LinkedIn Profile Using My Portfolio";
						$scope.text = "<p>Did you know LinkedIn is the most widely used social media platform for professionals?  It&#39;s for everyone to use &#8211; you don&#39;t have to have years of experience to set up a LinkedIn profile.</p>" + 
									  "<p>Set up a LinkedIn profile using your ASVAB CEP portfolio (see Populating My Portfolio for guidance). Take your Work Experience and Education directly from the portfolio and copy into your new LinkedIn Profile. Use your Skills and Achievements to build out your profile. You can share information both ways. For example, LinkedIn gives you suggested skills based on your experience and education. You can also add these to My Portfolio and share it with your counselor or a college or military recruiter.</p>";
						$scope.link = "ASVAB_CEP_Create_a_Linkedin_Profile.pdf";
						break;
					case "action":
						$scope.title = "Create an Action Plan";
						$scope.text = "<p>Career satisfaction is not one size fits all. There are multiple paths you can take to achieve your career goals. Choose one (or a combination) of the following pathways:</p>" +
									  "<p><strong>College</strong>:  Using your list of favorite jobs, identify five colleges that you may want to apply to in the future.  Add these colleges to your favorites and add notes about why you are considering these colleges.  Reflect on tuition costs, required test scores and returning student numbers.  For these five institutions, write a plan of action to include all materials needed for the application process.  Discuss your plan of action and college/university choices with your parents, career counselor, or teacher. </p>" +
									  "<p><strong>Certification/Licenses/Apprenticeship</strong>:  Using your list of favorite jobs, identify the certifications/licenses/or apprenticeships you are interested in pursuing.  Who are the certifying organizations?  What do you have to complete in high school in order to qualify for these opportunities?  Make a plan of action to include all materials needed for the application process.  Discuss your plan of action and certification/license/or apprenticeship choices with your parents, career counselor, or teacher. </p>" +
									  "<p><strong>Military</strong>:  Using your list of favorite jobs, identify the military services offering these occupations.  (If you are interested in an Officer level career, note that college is required, you will want to chart your college path too.)  Using <a target='_blank' href='" + ssoUrl + "'>www.careersinthemilitary.com</a>, identify your favorite Services that offer the job opportunities that interest you.  What requirements will you need to meet in order to attain this goal?  (ASVAB scores, physical readiness, etc.)  Make a plan of action to include materials needed for the application process.  Discuss your plan of action and military choices with your parents, career counselor, teacher, or a recruiter.</p>";
						$scope.link = "ASVAB_CEP_Create_an_Action_Plan.pdf";
						break;
					case "portfolio":
						$scope.title = "Populating My Portfolio";
						$scope.text = "<p>ASVAB CEP participants can use My Portfolio as a living resume to keep track of accomplishments, aspirations, and career exploration activities. Here, they can track their experience, skills and interests, as well as document future plans. Users can also save favorite careers, colleges, and career clusters of interest.  Follow this step-by-step guide.</p>" +
									  "<p><strong>Step One</strong>:  Capture Your Future Plan(s).</p>" + 
									  "<p>Select from the list of postsecondary intentions (e.g., college, work, military) that you are considering right now. It&#39;s natural for plans to change. You can update this at any time.</p>" + 
									  "<p><strong>Step Two</strong>:  Document Your Accomplishments.</p>" +
									  "<p>Use each section to outline your work experience, education, achievements, skills, and volunteer activities. You can use this information when it&#39;s time to write a formal resume, and you can add information as you accumulate more experiences.</p>" +
									  "<br />" + 
									  "<p>Not sure what to write? Check out the Tips provided under Work Experience and Volunteer Activities, and also the article, <a target='_blank' href='" + getCepLink() + "media-center-article/64'>Writing a High School Resume that Stands Out</a>. The <a target='_blank' href='" + getCepLink() + "media-center-article-list/1'>Media Center</a> is full of useful tips.</p>" +
									  "<p><strong>Step Three</strong>:  Track Your Interests, Values, and Favorites.</p>" +
									  "<p>My Portfolio is a perfect place to keep track of what you like and value. If you have taken the FYI, your work-related RIASEC interests will show up here. You can jot down details about why these interests appeal to you.</p>" +
									  "<p>You can also track your work values. What&#39;s important to you in a job?  Do you want to work in a field where you help others?  Do you value job security?  Benefits? Good income? Use this space to write down what you value in a job.</p>" +
									  "<br />" +
									  "<p>As you explore the OCCU-Find, you can save notes on careers and colleges and add the ones you like to your favorites.</p>";
						$scope.link = "ASVAB_CEP_Populating_My_Portfolio.pdf";
						break;
					case "portfolio_citm":
						$scope.title = "Populating the Portfolio at careersinthemilitary.com";
						$scope.text = "<p>ASVAB CEP participants who are exploring military career options can add additional military-specific items to their portfolio at <a target='_blank' href='" + ssoUrl + "'>careersinthemilitary.com</a>.</p>" + 
									  "<p>When you add information to My Portfolio at asvabprogram.com, your information is also saved at <a target='_blank' href='" + ssoUrl + "'>careersinthemilitary.com</a>.</p>" +
									  "<p>The two sites work together. When you explore military-specific careers, you are taken to <a target='_blank' href='" + ssoUrl + "'>careersinthemilitary.com</a>, where you are automatically logged in.</p>" + 
									  "<p>At <a target='_blank' href='" + ssoUrl + "'>careersinthemilitary.com</a>, go to My Profile and you will see any information you saved in My Portfolio at asvabprogram.com. You will notice additional military-specific content fields related to Composite Score Favorites and Physical Fitness. Populate these fields if you want to share this portfolio with a recruiter.</p>" + 
									  "<p>At <a target='_blank' href='" + ssoUrl + "'>careersinthemilitary.com</a> you can make notes and add military careers to your favorites the same way you did at asvabprogram.com. You will also see suggested careers tailored to your interests. Don&#39;t forget to review the guided exploration activity for more suitable careers.</p>";
						$scope.link = "ASVAB_CEP_Populating_the_Portfolio_citm.pdf";
						break;
					case "virtual":
						$scope.title = "Virtual Job Shadow";
						$scope.text = "<p>Job shadowing is a popular on-the-job learning experience. It involves observing someone working so you can understand firsthand what they actually do at work. Job shadowing can help you to get a better sense of the options available in the career field you&#39;re considering and the competencies required for careers that interest you.</p>" +
									  "<p>Select your favorite career cluster (it will be added to My Portfolio). Review details from at least two career clusters included below. We included links to details about one career from each career cluster that is available in both the civilian and military world. Consider the videos and descriptions. What is similar? What is different? What have you already done to prepare you for entry into this career field? What paths can you take to gain entry into this career field? What do you need to do next? You can add your favorite careers and institutions to your My Portfolio and make notes about what you are planning. Then, populate your My Portfolio with your personal details relevant to the entry requirements or competencies required for the career you are exploring.</p>" +
									  "<p>(If you&#39;re exploring careers not included on this list, more videos and descriptions are available at asvabprogram.com and <a href='" + ssoUrl + "'>careersinthemilitary.com</a>.) </p>" +
									  "<p>*The idea for this activity was submitted by Katie Coble, Career Development Coordinator, CTE Department Chair, and Internship Coordinator at East Wake High School in Wendell, NC.</p>";
						$scope.link = "ASVAB_CEP_Virtual_Job_Shadow.pdf";
						break;
					default:
						$scope.title = "Not Available";
						$scope.text =  "Not Available";
						$scope.link = 	"Not Available.pdf"
						break;
					}
				
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};
} ]);