cepApp.controller('OccufindEmploymentDetailsController', [ '$scope', '$rootScope', '$route', 'stateSalaryYear', 'occupationTitleDescription', 'employmentMoreDetails', 'favorites', function($scope, $rootScope, $route, stateSalaryYear, occupationTitleDescription, employmentMoreDetails, favorites) {

	$scope.stateSalaryYear = stateSalaryYear.data;
	$scope.occupationTitle = occupationTitleDescription.data.title;
	$scope.employementMoreDetails = employmentMoreDetails.data;
	$scope.socId = $route.current.params.socId;
	$scope.favorites = undefined;
	
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ;
		$scope.favorites = favorites.data;
	}
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
		
		// tools mobile
		function topBluePanelMobile() {
			if($(window).outerWidth() < 640) {
				if($('.top-blue-panel .tools:not(.ng-hide)').length > 0 && $('.top-blue-panel+.top-blue-panel-spacing').length==0){
					$('.top-blue-panel').after('<div class="top-blue-panel-spacing"></div>');
					$('.top-blue-panel+.top-blue-panel-spacing').css('height', $('.top-blue-panel .tools:not(.ng-hide)').outerHeight() );
				}
			} else {
				$('.top-blue-panel+.top-blue-panel-spacing').remove();
			}
		}
		topBluePanelMobile();
		$(window).resize(function() {
			topBluePanelMobile();
		});
		
		if (window.outerWidth < 768) return;

		// sticky header for occupations && sticky sidebar
		var allSection = $('.top-blue-panel, #sidebar.logged-in, #sidebar.not-logged-in');
		allSection.each(function(i) {
			var section = $(allSection[i]);
			section.scrollToFixed({
				marginTop: function() {
					var HeaderHeight = $('#header').outerHeight(true) + $('#banner').outerHeight(true);
					if (HeaderHeight) {
						var marginTop = HeaderHeight;
					}
					return marginTop;
				},
				limit: function() {
					var footerOne = $('#footerOne').offset();
					var footerTwo = $('#footerTwo').offset();
					var limit = 0;
					if(footerOne && footerTwo){
						var footerHeight = footerOne.top + footerTwo.top;
						if (footerHeight) {
							limit = footerHeight - $(this).outerHeight(true) - 10;
						} else {
							limit = footerHeight;
						}
					}
					return limit;
				},
				zIndex: 1
			});
		});
	});
	
} ]);