cepApp.factory('CareerOneStopFactory', [ '$http', 'appUrl', 'awsApiFullUrl', function($http, appUrl, awsApiFullUrl) {

	var careerOneStopFactory = {};

	careerOneStopFactory.getOccupationCertificates = function(onetSoc) {
		var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
		return $http({url: awsApiFullUrl + '/api/CareerOneStop/cert/' + onetOccupationCode + '/', method: 'GET'})
	}
	
	careerOneStopFactory.getOccupationCertificateDetails = function(certId) {		
		return $http({url: awsApiFullUrl + '/api/CareerOneStop/certDetail/' +  certId.trim() + '/', method: 'GET'})
	}

	careerOneStopFactory.getOccupationLicenses = function(onetSoc, state) {
		var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
		return $http({url: awsApiFullUrl + '/api/CareerOneStop/license/' + onetOccupationCode + '/' + state.trim() , method: 'GET'})
	}
	
	careerOneStopFactory.getOccupationLicensesDetail  = function(onetSoc, licenseId , state) {
		var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
		return $http({url: awsApiFullUrl + '/api/CareerOneStop/licenseDetail/' + onetOccupationCode + '/' + licenseId.trim()  + '/' + state.trim() + '/', method: 'GET'});
	}
	
	
	careerOneStopFactory.getOccupationApprenticeship  = function(onetSoc, state) {
		var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
		return $http.get(appUrl + 'CareerOneStop/apprenticeship/'  + onetOccupationCode + '/' + state.trim() );
	}

	careerOneStopFactory.getOccupationSocCrosswalk = function(onetSoc) {
		return $http({url: awsApiFullUrl + '/api/OccuFind/2019SocCrosswalk/' + onetSoc + '/', method: 'GET'});
	}
	
	return careerOneStopFactory;

} ]);