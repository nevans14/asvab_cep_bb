cepApp.controller('OccufindEducationDetailsController', [ '$scope', '$rootScope', 'schoolDetails', 'schoolMajors', 'occupationTitleDescription', '$route', 'UtilityService', 'favorites', 'DbInfoService', function($scope, $rootScope, schoolDetails, schoolMajors, occupationTitleDescription, $route, UtilityService, favorites, DbInfoService) {

	$scope.occupationTitle = occupationTitleDescription.data.title;
	$scope.schoolDetails = schoolDetails.data;
	console.log($scope.schoolDetails);
	$scope.sort;
	$scope.socId = $route.current.params.socId;
	$scope.absUrl = UtilityService.getCanonicalUrl();
	$scope.favorites = undefined;
	
	$scope.schoolMajors = [];
	for(var i = 0; i < schoolMajors.data.length; i++) {
		$scope.schoolMajors.push(schoolMajors.data[i]);
	}

	$scope.showMore = false;
	$scope.showMoreText = function() {
		if(!$scope.showMore) {
			return "More";
		} else {
			return "Less";
		}
	}
	$scope.hasMore= function(){
		if($scope.schoolMajors.length <= 3)
			return false;
		else
			return true;	
	}

	
	//pagination
	$scope.totalItems = $scope.schoolDetails.length;
	$scope.currentPage = 1;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 20;
	
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ; 
		$scope.favorites = favorites.data;
	}
	
	//filter school list by state
	$scope.$watch('search', function(term) {
        $scope.filtered = stateFilter($scope.schoolDetails, term);
        $scope.totalItems = $scope.filtered.length;
    });
	
	function stateFilter(list, term) {
		var filterList = [];
		if (term == '' || term == undefined) {
			return list;
		}
		for (var i = 0; i < list.length; i++) {
			if (list[i].state == term) {
				filterList.push(list[i]);
			}
		}
		return filterList;
	}

	DbInfoService.getByName('ipeds_data_disclaimer').then(function (res) {
		$scope.ipedsDisclaimer = res.data.value;
	});	
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
		
		// tools mobile
		function topBluePanelMobile() {
			if($(window).outerWidth() < 640) {
				if($('.top-blue-panel .tools:not(.ng-hide)').length > 0 && $('.top-blue-panel+.top-blue-panel-spacing').length==0){
					$('.top-blue-panel').after('<div class="top-blue-panel-spacing"></div>');
					$('.top-blue-panel+.top-blue-panel-spacing').css('height', $('.top-blue-panel .tools:not(.ng-hide)').outerHeight() );
				}
			} else {
				$('.top-blue-panel+.top-blue-panel-spacing').remove();
			}
		}
		topBluePanelMobile();
		$(window).resize(function() {
			topBluePanelMobile();
		});
		
		if (window.outerWidth < 768) return;

		// sticky header for occupations && sticky sidebar
		var allSection = $('.top-blue-panel, #sidebar.logged-in, #sidebar.not-logged-in');
		allSection.each(function(i) {
			var section = $(allSection[i]);
			section.scrollToFixed({
				marginTop: function() {
					var HeaderHeight = $('#header').outerHeight(true) + $('#banner').outerHeight(true);
					if (HeaderHeight) {
						var marginTop = HeaderHeight;
					}
					return marginTop;
				},
				limit: function() {
					
					var footerOne = $('#footerOne').offset();
					var footerTwo = $('#footerOne').offset();
					if(footerOne != undefined && footerTwo != undefined){
						var footerHeight = $('#footerOne').offset().top + $('#footerTwo').offset().top;
						if (footerHeight) {
							var limit = footerHeight - $(this).outerHeight(true) - 10;
						}
						return limit;
					}
				},
				zIndex: 1
			});
		});
	});

} ]);