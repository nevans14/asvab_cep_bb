cepApp.factory('OccufindRestFactory', [ '$http','appUrl', 'awsApiFullUrl', function($http, appUrl, awsApiFullUrl) {

	var occufindRestFactory = {};

	occufindRestFactory.getOccupationResults = function(occufindSearchJSON) {
		return $http.get(awsApiFullUrl + '/api/occufind/getOccufindSearch/', occufindSearchJSON);
	}

	occufindRestFactory.getOccupationTitleDescription = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/occupationTitleDescription/' + onetSoc + '/'});
	}

	occufindRestFactory.getOccupationWorkValues = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/work-values/' + onetSoc + '/'});
	}

	occufindRestFactory.getOccupationInterestCodes = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/OccupationInterestCodes/' + onetSoc + '/'});
	}

	occufindRestFactory.getHotGreenStemFlags = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/HotGreenStemFlags/' + onetSoc + '/'});
	}

	occufindRestFactory.getServiceOfferingCareer = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/ServiceOfferingCareer/' + onetSoc + '/'});
	}

	occufindRestFactory.getRelatedCareerCluster = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/RelatedCareerCluster/' + onetSoc + '/'});
	}

	occufindRestFactory.getSchoolMoreDetails = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/SchoolMoreDetails/' + onetSoc + '/'});
	}

	occufindRestFactory.getSchoolMajors = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/SchoolMajors/' + onetSoc + '/'});
	}

	occufindRestFactory.getStateSalaryYear = function() {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/StateSalaryYear'});
	}

	occufindRestFactory.getNationalPovertyLevel = function() {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/NationalPovertyLevel'});
	}

	occufindRestFactory.getBlsProjectionYearUpdated = function() {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/BlsProjectionYearUpdated'});
	}

	occufindRestFactory.getEmploymentMoreDetails = function(trimmedOnetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/EmploymentMoreDetails/' + trimmedOnetSoc + '/'});
	}

	occufindRestFactory.getMilitaryMoreDetails = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/MilitaryMoreDetails/' + onetSoc + '/'});
	}

	occufindRestFactory.getMilitaryHotJobs = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/MilitaryHotJobs/' + onetSoc + '/'});
	}

	occufindRestFactory.getNationalSalary = function(trimmedOnetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/NationalSalary/' + trimmedOnetSoc + '/'});
	}
	
	occufindRestFactory.getNationalEntrySalary = function(trimmedOnetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/NationalEntrySalary/' + trimmedOnetSoc + '/'});
	}
	
	occufindRestFactory.getBLSTitle = function(trimmedOnetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/BLSTitle/' + trimmedOnetSoc + '/'});
	}

	occufindRestFactory.getMoreResources = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/OccufindMoreResources/' + onetSoc + '/'});
	}
	
	occufindRestFactory.getAlternateView = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/OccufindAlternativeView/' + onetSoc + '/'});
	}
	
	occufindRestFactory.getImageAltText = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/ImageAltText/' + onetSoc + '/'});
	}
	
	occufindRestFactory.getJobZone = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/job-zone/' + onetSoc + '/'});
	}
	
	occufindRestFactory.getCertificateExplanation = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/certificateExplanation/' + onetSoc + '/'});
	}
		
	occufindRestFactory.getAlternateTitles = function(onetSoc) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/alternateTitles/' + onetSoc + '/'});
	}
	
	occufindRestFactory.getSchoolProfile = function(unitId) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/SchoolProfile/' + unitId + '/'});
	}

	occufindRestFactory.getIpedsYearUpdated = function() {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/IpedsYearUpdated/'});
	}
	
	return occufindRestFactory;

} ]);