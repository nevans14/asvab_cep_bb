cepApp.controller('OccufindOccupationSearchResultsController', [ '$scope', '$rootScope', '$window', 'OccufindSearchService', '$http', '$location', '$q', 'appUrl', 'occupationResults', '$route', '$routeParams', '$cookieStore', '$filter', 'FavoriteRestFactory', 'favorites', 'onetVersion', function($scope, $rootScope, $window, OccufindSearchService, $http, $location, $q, appUrl, occupationResults, $route, $routeParams, $cookieStore, $filter, FavoriteRestFactory, favorites, onetVersion) {
	
	$scope.onetVersion = onetVersion.data;
	$scope.filter = function (items, checkbox) {
		var size = 0;
		for (var i = 0; i < checkbox.length; i++) {
			if (items[checkbox[i].code]) {
				size++;
			}
		}
		return size;
	}
	/**
	 * set the titles of what the selected interest codes have been selected
	 */
	$scope.getSelectedInterestCodes = function () {
		$scope.selectedTitleOne = undefined;
		$scope.selectedTitleTwo = undefined;
		var isFirstTitleFilled = false;
		for (var i = 0; i < $scope.checkboxInterest.length; i++) {
			if ($scope.filterItems[$scope.checkboxInterest[i].code]) {
				if (!isFirstTitleFilled) {
					$scope.selectedTitleOne = $scope.checkboxInterest[i].name;
					isFirstTitleFilled = true;
				} else {
					$scope.selectedTitleTwo = $scope.checkboxInterest[i].name;
				}
			}
		}
	}
	/**
	 * returns what categories have been selected
	 */
	$scope.displaySelectedCategories = function () {
		var message = '';
		for (var i = 0; i < $scope.checkboxCategory.length; i++) {
			if ($scope.categoryItems[$scope.checkboxCategory[i].code]) {
				switch ($scope.checkboxCategory[i].code) {
					case 'H': message += (message == '' ? 'Hot' 	: ' | Hot'); break;
					case 'B': message += (message == '' ? 'Bright' 	: ' | Bright'); break;
					case 'G': message += (message == '' ? 'Green' 	: ' | Green'); break;
					case 'S': message += (message == '' ? 'STEM' 	: ' | STEM'); break;
					case 'M': message += (message == '' ? 'Available in the Military' : ' | Available in the Military'); break;
				default:
					break;
				}
			}
		}
		return message;
	}

	/**
	 * returns what work value have been selected
	 */
	 $scope.displaySelectedWorkValues = function () {
		var message = '';
		for (var i = 0; i < $scope.checkboxWorkValue.length; i++) {
			if ($scope.workValueItems[$scope.checkboxWorkValue[i].code]) {
				switch ($scope.checkboxWorkValue[i].code) {
					case 'A': message += (message == '' ? 'Achievement' 	: ' | Achievement'); break;
					case 'I': message += (message == '' ? 'Independence' 	: ' | Independence'); break;
					case 'REC': message += (message == '' ? 'Recognition' 	: ' | Green'); break;
					case 'REL': message += (message == '' ? 'Relationships' 	: ' | STEM'); break;
					case 'S': message += (message == '' ? 'Support' 	: ' | Support'); break;
					case 'W': message += (message == '' ? 'Working Conditions' 	: ' | Working Conditions'); break;
				default:
					break;
				}
			}
		}
		return message;
	}	

	/**
	 * returns what interest codes have been selected
	 */
	$scope.displaySelectedInterestCodes = function () {
		var message = '';
		for (var i = 0; i < $scope.checkboxInterest.length; i++) {
			if ($scope.filterItems[$scope.checkboxInterest[i].code]) {
				switch ($scope.checkboxInterest[i].code) {
					case 'R' : message += (message == '' ? 'Realistic' : ' and Realistic'); break;
					case 'I' : message += (message == '' ? 'Investigative' : ' and Investigative'); break;
					case 'A' : message += (message == '' ? 'Artistic' : ' and Artistic'); break;
					case 'S' : message += (message == '' ? 'Social' : ' and Social'); break;
					case 'C' : message += (message == '' ? 'Conventional' : ' and Conventional'); break;
					case 'E' : message += (message == '' ? 'Enterprising' : ' and Enterprising'); break;
					default:
						break;
				}
			}
		}
		// if no interest codes have been selected, then the filter returns 'All' jobs
		if (message == '') message += 'All';
		return message += ' Occupations';
	}
	
	$scope.selectedInterestCodeOne = OccufindSearchService.userSearch.interestCodeOne;
	$scope.selectedInterestCodeTwo = OccufindSearchService.userSearch.interestCodeTwo;
	$scope.selectedTitleOne = OccufindSearchService.getSelectedTitleOne(OccufindSearchService.userSearch.interestCodeOne);
	$scope.selectedTitleTwo = OccufindSearchService.getSelectedTitleTwo(OccufindSearchService.userSearch.interestCodeTwo);
	$scope.brightOccupationFlagDisplay = OccufindSearchService.userSearch.brightOccupationFlag;
	$scope.greenOccupationFlagDisplay = OccufindSearchService.userSearch.greenOccupationFlag;
	$scope.stemOccupationFlagDisplay = OccufindSearchService.userSearch.stemOccupationFlag;
	$scope.hotOccupationFlagDisplay = OccufindSearchService.userSearch.hotOccupationFlag;
	$scope.userSearchStringDisplay = OccufindSearchService.userSearch.userSearchString;
	$scope.favorites = undefined;
	
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ;		
		$scope.favorites = favorites.data;		
	}	
	
	/**
	 * Keeps track of pagination
	 */
	$scope.pageChanged = function(e) {
		$scope.currentPage = e.currentPage;
	}
	
	/**
	 * Save user search
	 */
	$scope.saveSearch = function () {
		var currentSearchSelections = {
			categoryItems 	: $scope.categoryItems,
			workValueItems 	: $scope.workValueItems,
			skillItems 		: $scope.skillItems,
			filterItems 	: $scope.filterItems,
			keyword			: $scope.keyword,
			currentPage 	: $scope.currentPage,
			skillImportance : $scope.skillImportance,
			skillView 		: $scope.skillView			
		}
		$window.sessionStorage.setItem('currentSearchSelections', JSON.stringify(currentSearchSelections));
		/**
		 * update interest codes selected		 * 
		 */
		$scope.getSelectedInterestCodes();
	}
	
	/**
	 * Reset tab index
	 */
	$scope.tab = {};
	$scope.tab.index = 0; //'hi'
	$scope.resetTabs = function () {
		$scope.skillView = 'hi';
		$scope.tab.index = 0;
		$scope.currentPage = 1;
		$scope.itemsPerPage = 20;
	}
	
	$scope.keyword = '';	
	$scope.occupationResults = occupationResults.data;

	/**
	 * Pagination
	 */
	$scope.totalItems = $scope.occupationResults.length;
	$scope.currentPage = 1;	
	$scope.maxSize = 4;
	$scope.itemsPerPage = 20;	
	
	/**
	 * Clear selections
	 */
	$scope.clearSelections = function () {
		// reset keyword, skillImportance, and tabs
		$scope.keyword = '';
		$scope.skillImportance = '';
		$scope.resetTabs();
		// reset checkboxes
		$scope.filterItems = {
			'R' : false,
			'I' : false,
			'A' : false,
			'S' : false,
			'E' : false,
			'C' : false
		};
		$scope.categoryItems = {
			'B' : false,
			'S' : false,
			'G' : false,
			'H' : false,
			'M' : false
		};
		$scope.workValueItems = {
			'A' : false,
			'I' : false,
			'REC' : false,
			'REL' : false,
			'S' : false,
			'W' : false,
		};
		$scope.skillItems = {
			'V' : false,
			'M' : false,
			'S' : false
		}
		// reset checkbox checked markers
		$scope.interestChecked = 0;
		$scope.skillChecked = 0;
		$scope.workValueChecked = 0;
		$scope.categoryChecked = 0;
		// save
		$scope.saveSearch();
	}
	
	$scope.skillImportance = '';
	$scope.skillView = 'hi';
	$scope.tab = {};
	
	$scope.skillViewSelected = function(selected) {
		$scope.skillView = selected;
		$scope.currentPage = 1;
		$scope.tab.index = 
			$scope.skillView == 'hi' ? 0 : 
			$scope.skillView == 'md' ? 1 : 
			$scope.skillView == 'lw' ? 2 : 
			0;
		$scope.saveSearch();
	}	
	
	/**
	 * Tracks how many selections are made for skill selection section. Limit user selection to one.
	 */
	// for skill selection checkbox 
	$scope.checkboxSkill = [ {
		name : 'Verbal',
		code : 'V'
	}, {
		name : 'Math',
		code : 'M'
	}, {
		name : 'Science/Technical',
		code : 'S'
	}];
	$scope.skillItems = {
		'V' : false,
		'M' : false,
		'S' : false
	};	
	
	$scope.onSkillChange = function(item) {		
		$scope.skillImportance = ($scope.skillItems[item.code] == true ? item.code : '');
		if ($scope.skillImportance == '')
			$scope.resetTabs();
		$scope.saveSearch();
	}
	
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.limitOne = function(item) {
		if ($scope.skillItems[item.code])
			$scope.skillChecked++;
		else
			$scope.skillChecked--;
	}

	$scope.selectedOccupation = function(onetSoc, title, stem, bright, green, hot, interestCdOne, interestCdTwo, interestCdThree) {
		OccufindSearchService.selectedOnetSoc.push(onetSoc);
		OccufindSearchService.selectedOcupationTitle = title;
		OccufindSearchService.occupationGreen = green;
		OccufindSearchService.occupationStem = stem;
		OccufindSearchService.occupationBright = bright;
		OccufindSearchService.occupationHot = hot;
		OccufindSearchService.occupationInterestOne = interestCdOne;
		OccufindSearchService.occupationInterestTwo = interestCdTwo;
		OccufindSearchService.occupationInterestThree = interestCdThree;
		$location.url('/occufind-occupation-details/' + onetSoc);
	};
	
	/***
	 * Interest checkbox selections
	 */
	$scope.checkboxInterest = [{
		name : 'Realistic',
		code : 'R'
	}, {
		name : 'Investigative',
		code : 'I'
	}, {
		name : 'Artistic',
		code : 'A'
	}, {
		name : 'Social',
		code : 'S'
	}, {
		name : 'Enterprising',
		code : 'E'
	}, {
		name : 'Conventional',
		code : 'C'
	} ];
	
	$scope.filterItems = {
		'A': false,
		'C': false,
		'E': false,
		'I': false,
		'R': false,
		'S': false
	};
	
	$scope.interestFilter = function(item) {
		for (var i = 0; i < $scope.checkboxInterest.length; i++) {
			if ($scope.filterItems[$scope.checkboxInterest[i].code]) {
				switch ($scope.checkboxInterest[i].code) {
					case 'A':
						if (item.interestCdOne != 'A' && item.interestCdTwo != 'A' && item.interestCdThree != 'A') return false;
						break;
					case 'C':
						if (item.interestCdOne != 'C' && item.interestCdTwo != 'C' && item.interestCdThree != 'C') return false;
						break;
					case 'E':
						if (item.interestCdOne != 'E' && item.interestCdTwo != 'E' && item.interestCdThree != 'E') return false;
						break;
					case 'I':
						if (item.interestCdOne != 'I' && item.interestCdTwo != 'I' && item.interestCdThree != 'I') return false;
						break;
					case 'R':
						if (item.interestCdOne != 'R' && item.interestCdTwo != 'R' && item.interestCdThree != 'R') return false;
						break;
					case 'S':
						if (item.interestCdOne != 'S' && item.interestCdTwo != 'S' && item.interestCdThree != 'S') return false;
						break;
				}
			}
		}
		return true;
	}
	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.interestLimit = 2;
	$scope.interestChecked = 0;
	$scope.limitTwo = function(item) {
		if ($scope.filterItems[item.code])
			$scope.interestChecked++;
		else
			$scope.interestChecked--;		
	}

	/***
	 * Category checkbox selections
	 */
	$scope.checkboxCategory = [{
		name: 'Bright Outlook',
		code: 'B',
		iconClass: 'icon hot',
		rel: 'BRIGHT'
	// }, {
	// 	name: 'Green Careers',
	// 	code: 'G',
	// 	iconClass: 'icon green',
	// 	rel: 'GREEN'
	}, {
		name: 'STEM Careers',
		code: 'S',
		iconClass: 'icon stem',
		rel: 'STEM'
	}, {
		name: 'Hot Military Careers',
		code: 'H',
		iconClass: 'icon-hotjob',
		rel: 'HOT'
	}, {
		name: 'Available in the Military',
		code: 'M'
	}];
	
	$scope.categoryItems = {
		'B' : false,
		'H' : false,
		'G' : false,
		'S' : false,
		'M' : false
	};

	$scope.checkboxWorkValue = [{
		name: 'Achievement',
		code: 'A',
		image: 'images/WV-1.png',
		rel: 'ACHIEVEMENT'
	}, {
		name: 'Independence',
		code: 'I',
		image: 'images/WV-2.png',
		rel: 'INDEPENDENCE'
	}, {
		name: 'Recognition',
		code: 'REC',
		image: 'images/WV-3.png',
		rel: 'RECOGNITION'
	}, {
		name: 'Relationships',
		code: 'REL',
		image: 'images/WV-4.png',
		rel: 'RELATIONSHIPS'
	}, {
		name: 'Support',
		code: 'S',
		image: 'images/WV-5.png',
		rel: 'SUPPORT'
	}, {
		name: 'Working Conditions',
		code: 'W',
		image: 'images/WV-6.png',
		rel: 'WORKING CONDITION'
	}];
	
	$scope.workValueItems = {
		'A' : false,
		'I' : false,
		'REC' : false,
		'REL' : false,
		'S' : false,
		'W' : false,
	};

	$scope.checkWorkValueType = function(item, workValueCode) {

		var workValueType = undefined;

		switch(workValueCode) {
			case 'A':
				workValueType = 1;
				break;
			case 'I':
				workValueType = 2;
				break;
			case 'REC':
				workValueType = 3;
				break;
			case 'REL':
				workValueType = 4;
				break;
			case 'S':
				workValueType = 5;
				break;
			case 'W':
				workValueType = 6;
				break;
		}

		return item.workValueOne === workValueType
			|| item.workValueTwo === workValueType
			|| item.workValueThree === workValueType
	}

	$scope.workValueLimit = 3;
	$scope.workValueChecked = 0;
	$scope.limitThree = function(item) {
		if ($scope.workValueItems[item.code])
			$scope.workValueChecked++;
		else
			$scope.workValueChecked--;
	}

	$scope.handleClick = function( $event ) {
		if (($scope.workValuesToggle || $scope.categoriesToggle)
			&& $event.srcElement.className.indexOf('custom_select_dropdown') < 0
			&& $event.srcElement.className.indexOf('work-value-element') < 0
			&& $event.srcElement.className.indexOf('category-element') < 0
			&& $event.srcElement.id.indexOf('category-element') < 0
		) {
			$scope.workValuesToggle = false;
			$scope.categoriesToggle = false;
		}
	}
	
	$scope.categoryFilter = function(item) {
		for (var i = 0; i < $scope.checkboxCategory.length; i++) {
			if ($scope.categoryItems[$scope.checkboxCategory[i].code]) {
				switch ($scope.checkboxCategory[i].code) {
					case 'B':
						if (!item.isBrightOccupation) return false;
						break;
					case 'H':
						if (!item.isHotOccupation) return false;
						break;
					case 'S':
						if (!item.isStemOccupation) return false;
						break;
					case 'G':
						if (!item.isGreenOccupation) return false;
						break;
					case 'M':
						if (!item.isAvailableInTheMilitary) return false;
						break;
				}
			}
		}		
		return true;
	}

	$scope.categoryChecked = 0;
	$scope.categoryCount = function(item) {
		if ($scope.categoryItems[item.code])
			$scope.categoryChecked++;
		else
			$scope.categoryChecked--;
	}

	$scope.workValueFilter = function(item) {
		for (var i = 0; i < $scope.checkboxWorkValue.length; i++) {
			if ($scope.workValueItems[$scope.checkboxWorkValue[i].code]) {
				switch ($scope.checkboxWorkValue[i].code) {
					case 'A':
						if (!$scope.checkWorkValueType(item, 'A')) return false;
						break;
					case 'I':
						if (!$scope.checkWorkValueType(item, 'I')) return false;
						break;
					case 'REC':
						if (!$scope.checkWorkValueType(item, 'REC')) return false;
						break;
					case 'REL':
						if (!$scope.checkWorkValueType(item, 'REL')) return false;
						break;
					case 'S':
						if (!$scope.checkWorkValueType(item, 'S')) return false;
						break;
					case 'W':
						if (!$scope.checkWorkValueType(item, 'W')) return false;
						break;
				}
			}
		}		
		return true;
	}
	
	$scope.hideFilter = function(item) {
		return item.show;
	}

	$scope.workValuesToggle = false;
	$scope.categoriesToggle = false;
	
	$scope.search = function (row) {
		return (angular.lowercase(row.occupationTitle).indexOf(angular.lowercase($scope.keyword) || '') !== -1)
				|| row.alternateTitles.some(function(title) { return angular.lowercase(title).includes(angular.lowercase($scope.keyword) || '')});
	}
	
	var currentSearchSelections = angular.fromJson($window.sessionStorage.getItem('currentSearchSelections'));
	$scope.skillImportance = currentSearchSelections ? currentSearchSelections.skillImportance : $scope.skillImportance;
	$scope.skillView = currentSearchSelections ? currentSearchSelections.skillView : $scope.skillView;
	$scope.skillItems = currentSearchSelections ? currentSearchSelections.skillItems : $scope.skillItems;
	$scope.filterItems = currentSearchSelections ? currentSearchSelections.filterItems : $scope.filterItems;
	$scope.categoryItems = currentSearchSelections ? currentSearchSelections.categoryItems : $scope.categoryItems;
	$scope.workValueItems = currentSearchSelections ? currentSearchSelections.workValueItems : $scope.workValueItems;
	$scope.keyword = currentSearchSelections ? currentSearchSelections.keyword : $scope.keyword;
	$scope.currentPage = currentSearchSelections ? currentSearchSelections.currentPage : $scope.currentPage;
	$scope.tab.index = 
		$scope.skillView == 'hi' ? 0 : 
		$scope.skillView == 'md' ? 1 : 
		$scope.skillView == 'lw' ? 2 : 
		0; // Set default active tab.
	/**
	 * initialize how many items have been checked
	 */
	$scope.interestChecked = $scope.filter($scope.filterItems, $scope.checkboxInterest);
	$scope.skillChecked = $scope.filter($scope.skillItems, $scope.checkboxSkill);
	$scope.workValueChecked = $scope.filter($scope.workValueItems, $scope.checkboxWorkValue);
	$scope.categoryChecked = $scope.filter($scope.categoryItems, $scope.checkboxCategory);

	/**
	 * initialize selected interest codes
	 */
	$scope.getSelectedInterestCodes();
	/**
	 * initialize to show categories
	 */
	$scope.categoryShow = $scope.categoryItems.H || $scope.categoryItems.B || $scope.categoryItems.G || $scope.categoryItems.S || $scope.categoryItems.M;
	$scope.workValueShow = $scope.workValueItems.A || $scope.workValueItems.I || $scope.workValueItems.REC || $scope.workValueItems.REL || $scope.workValueItems.S || $scope.workValueItems.W;
	/**
	 * Scroll to search box.
	 */
	$scope.scrollTo = function(id) {
		var elmnt = document.getElementById("search-sidebar");
	    elmnt.scrollIntoView();
	}
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

/**
 * This filters careers by skill importance.
 */
cepApp.filter('rangeFilter', function() {
	return function(items, info) {
		var filtered = [];

		if (info.skillImportance == 'M') {
			if (info.view == 'hi') {
				angular.forEach(items, function(item) {
					if ((item.mathSkill >= 4 && item.mathSkill <= 5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'md') {
				angular.forEach(items, function(item) {
					if ((item.mathSkill >= 2.5 && item.mathSkill <= 3.5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'lw') {
				angular.forEach(items, function(item) {
					if ((item.mathSkill >= 0 && item.mathSkill <= 2)) {
						filtered.push(item);
					}
				});
			}
		} else if (info.skillImportance == 'V') {
			if (info.view == 'hi') {
				angular.forEach(items, function(item) {
					if ((item.verbalSkill >= 4 && item.verbalSkill <= 5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'md') {
				angular.forEach(items, function(item) {
					if ((item.verbalSkill >= 2.5 && item.verbalSkill <= 3.5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'lw') {
				angular.forEach(items, function(item) {
					if ((item.verbalSkill >= 0 && item.verbalSkill <= 2)) {
						filtered.push(item);
					}
				});
			}
		} else if (info.skillImportance == 'S') {
			if (info.view == 'hi') {
				angular.forEach(items, function(item) {
					if ((item.sciTechSkill >= 4 && item.sciTechSkill <= 5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'md') {
				angular.forEach(items, function(item) {
					if ((item.sciTechSkill >= 2.5 && item.sciTechSkill <= 3.5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'lw') {
				angular.forEach(items, function(item) {
					if ((item.sciTechSkill >= 0 && item.sciTechSkill <= 2)) {
						filtered.push(item);
					}
				});
			}
		} else {
			return items;
		}

		return filtered;
	};
});