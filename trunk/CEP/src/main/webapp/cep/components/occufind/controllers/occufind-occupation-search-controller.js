/**
 * Controller for occufind.html. This is the main search page for Occu-find.
 * Users can select up to two interest codes to search and filter by skill level
 * importance, hot jobs, STEM jobs, green jobs and search by occupation title
 */
cepApp.controller('OccufindOccupationSearchController', [ '$scope', '$http', '$location', '$window', 'OccufindSearchService', 'appUrl', '$rootScope', '$cookieStore','mediaCenterList', function($scope, $http, $location, $window, OccufindSearchService, appUrl, $rootScope, $cookieStore, mediaCenterList) {

	$scope.filter = function(items, checkbox) {
		var size = 0;
		for (var i = 0; i < checkbox.length; i++) {
			if (items[checkbox[i].code]) {
				size++;
			}
		}
		return size;
	}
	
	$scope.mediaCenterList = mediaCenterList;
	$scope.keyword = '';
	$scope.skillImportance = '';
	$scope.skillView = 'hi'; // by default, high importance will be shown
	$scope.currentPage = 1; // by default, pagination will be on the first page

	$scope.stemOccupationFlag;
	$scope.brightOccupationFlag;
	$scope.greenOccupationFlag;
	$scope.hotOccupationFlag;
	$scope.workValuesToggle = false;
	$scope.categoriesToggle = false;

	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ; 
	}

	$scope.seoTitle = 'Career Search | OCCU-Find | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Conduct a career search through the OCCU-Find. The OCCU-Find catalogs 1000+ career options that are available to you to search!';

	/**
	 * Save search criteria
	 */
	$scope.saveSearch = function () {
		var currentSearchSelections = {
			filterItems 	: $scope.filterItems,
			skillItems  	: $scope.skillItems,
			workValueItems: $scope.workValueItems,
			categoryItems	: $scope.categoryItems,
			keyword	    	: $scope.keyword,
			skillImportance	: $scope.skillImportance,
			skillView		: $scope.skillView,
			currentPage		: $scope.currentPage
		}
		$window.sessionStorage.setItem('currentSearchSelections', JSON.stringify(currentSearchSelections));
	}
	
	$scope.checkboxInterest = [{
		name : 'Realistic',
		code : 'R'
	}, {
		name : 'Investigative',
		code : 'I'
	}, {
		name : 'Artistic',
		code : 'A'
	}, {
		name : 'Social',
		code : 'S'
	}, {
		name : 'Enterprising',
		code : 'E'
	}, {
		name : 'Conventional',
		code : 'C'
	} ];
	
	$scope.filterItems = {
		'A': false,
		'C': false,
		'E': false,
		'I': false,
		'R': false,
		'S': false
	};

	$scope.checkboxSkill = [ {
		name : 'Verbal',
		code : 'V',
		image : 'images/small-book1.png'
	}, {
		name : 'Math',
		code : 'M',
		image : 'images/small-book2.png'
	}, {
		name : 'Science/Technical',
		code : 'S',
		image : 'images/small-book3.png'
	}];
	
	$scope.skillItems = {
		'V' : false,
		'M' : false,
		'S' : false
	}
	
	$scope.checkboxCategory = [{
		name: 'Bright Outlook',
		code: 'B',
		iconClass: 'icon hot',
		rel: 'BRIGHT'
	// }, {
	// 	name: 'Green Careers',
	// 	code: 'G',
	// 	iconClass: 'icon green',
	// 	rel: 'GREEN'
	}, {
		name: 'STEM Careers',
		code: 'S',
		iconClass: 'icon stem',
		rel: 'STEM'
	}, {
		name: 'Hot Military Careers',
		code: 'H',
		iconClass: 'icon-hotjob',
		rel: 'HOT'
	}, {
		name: 'Available in the Military',
		code: 'M'
	}];
	
	$scope.categoryItems = {
		'B' : false,
		'H' : false,
		'G' : false,
		'S' : false,
		'M' : false
	};

	$scope.categoryChecked = 0;
	$scope.categoryCount = function(item) {
		if ($scope.categoryItems[item.code])
			$scope.categoryChecked++;
		else
			$scope.categoryChecked--;
	}

	$scope.checkboxWorkValue = [{
		name: 'Achievement',
		code: 'A',
		image: 'images/WV-1.png',
		rel: 'ACHIEVEMENT'
	}, {
		name: 'Independence',
		code: 'I',
		image: 'images/WV-2.png',
		rel: 'INDEPENDENCE'
	}, {
		name: 'Recognition',
		code: 'REC',
		image: 'images/WV-3.png',
		rel: 'RECOGNITION'
	}, {
		name: 'Relationships',
		code: 'REL',
		image: 'images/WV-4.png',
		rel: 'RELATIONSHIPS'
	}, {
		name: 'Support',
		code: 'S',
		image: 'images/WV-5.png',
		rel: 'SUPPORT'
	}, {
		name: 'Working Conditions',
		code: 'W',
		image: 'images/WV-6.png',
		rel: 'WORKING CONDITIONS'
	}];
	
	$scope.workValueItems = {
		'A' : false,
		'I' : false,
		'REC' : false,
		'REL' : false,
		'S' : false,
		'W' : false,
	};

	$scope.workValueLimit = 3;
	$scope.workValueChecked = 0;
	$scope.limitThree = function(item) {
		if ($scope.workValueItems[item.code])
			$scope.workValueChecked++;
		else
			$scope.workValueChecked--;
	}

	$scope.handleClick = function( $event ) {
		if (($scope.workValuesToggle || $scope.categoriesToggle)
			&& $event.srcElement.className.indexOf('custom_select_dropdown') < 0
			&& $event.srcElement.className.indexOf('work-value-element') < 0
			&& $event.srcElement.className.indexOf('category-element') < 0
			&& $event.srcElement.id.indexOf('category-element') < 0
		) {
			$scope.workValuesToggle = false;
			$scope.categoriesToggle = false;
		}
	}

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 2;
	$scope.checked = 0;
	$scope.limitTwo = function(item) {
		if ($scope.filterItems[item.code])
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Tracks how many skill selections are made to limit user selection to one
	 */
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.limitOne = function(item) {
		if ($scope.skillItems[item.code])
			$scope.skillChecked++;
		else
			$scope.skillChecked--;
	}
	$scope.onSkillChange = function(item) {
		$scope.skillImportance = ($scope.skillItems[item.code] ? item.code : '');
		$scope.saveSearch();
	}
	/**
	 * Search button sets up the search service.
	 */
	$scope.occufindSearch = function() {		
		$location.path('/occufind-occupation-search-results');
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
	// initialize search parameters
	var currentSearchSelections = angular.fromJson($window.sessionStorage.getItem('currentSearchSelections'));
	$scope.skillImportance = currentSearchSelections ? currentSearchSelections.skillImportance : $scope.skillImportance;
	$scope.skillView = currentSearchSelections ? currentSearchSelections.skillView : $scope.skillView;
	$scope.skillItems = currentSearchSelections ? currentSearchSelections.skillItems : $scope.skillItems;
	$scope.filterItems = currentSearchSelections ? currentSearchSelections.filterItems : $scope.filterItems;
	$scope.categoryItems = currentSearchSelections ? currentSearchSelections.categoryItems : $scope.categoryItems;	
	$scope.workValueItems = currentSearchSelections ? currentSearchSelections.workValueItems : $scope.workValueItems;
	$scope.keyword = currentSearchSelections ? currentSearchSelections.keyword : $scope.keyword;
	/**
	 * initialize how many items have been checked
	 */
	$scope.checked = $scope.filter($scope.filterItems, $scope.checkboxInterest);
	$scope.skillChecked = $scope.filter($scope.skillItems, $scope.checkboxSkill);
	$scope.workValueChecked = $scope.filter($scope.workValueItems, $scope.checkboxWorkValue);
	$scope.categoryChecked = $scope.filter($scope.categoryItems, $scope.checkboxCategory);
} ]);
