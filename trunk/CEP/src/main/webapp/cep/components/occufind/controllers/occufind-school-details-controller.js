cepApp.controller('OccufindSchoolDetailsController', [ '$scope', '$rootScope', 'schoolDetails', 'occupationTitleDescription', '$route', 'modalService', 'UtilityService', 'favoriteSchools', 'ctmUrl', 'ipedsYearUpdated', function($scope, $rootScope, schoolDetails, occupationTitleDescription, $route, modalService, UtilityService, favoriteSchools, ctmUrl, ipedsYearUpdated) {

	$scope.occupationTitle = occupationTitleDescription.data.title;
	$scope.schoolDetails = schoolDetails.data;
	$scope.schoolTitle = $scope.schoolDetails.institutionName;
	$scope.socId = $route.current.params.socId;
	$scope.unitId = $route.current.params.unitId;
	$scope.favoriteSchools = (favoriteSchools) ? favoriteSchools.data : undefined;
	$scope.ipedsYearUpdated = ipedsYearUpdated.data;
	
	$scope.percentageAdmitted = $scope.schoolDetails.totalAdmitted / $scope.schoolDetails.totalApplicants * 100;

	if (typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ;				
	} else {
		$scope.occuFindUserLoggedIn = true ;
	}

	$scope.openDescription = function(section) {
		var headerTitle, bodyText;
		if(section == 'rotc'){
			headerTitle = 'Reserve Officers Training Corps (ROTC)';
			bodyText = 'Reserve Officers Training Corps (ROTC) is a college scholarship program that prepares students to become military officers. Offered at more than'+
				' 1,000 colleges and universities, it can help cover the costs of tuition and expenses.  '+
				' <a href="' + ctmUrl + 'options-becoming-an-officer" target="_blank">Learn more about ROTC</a>.';
		} else{
			return 0;
		}
		
		var modalOptions = {
			headerText : headerTitle,
			bodyText : bodyText
		};
		
		var screenSize = 'sm';
		if(section == 'education') {
			screenSize = 'md';
		}

		modalService.showModal({
			size : screenSize
		}, modalOptions);
	}
	
	
	/**
	 * Pie chart
	 */
	$scope.myChartObject = {};

	$scope.myChartObject.type = "PieChart";


	$scope.myChartObject.data = {
		"cols" : [
			{
				id : "t",
				label : "Topping",
				type : "string"
			},
			{
				id : "s",
				label : "Slices",
				type : "number"
			}
		],
		"rows" : [
			{
				c : [
					{
						v : "2015"
					},
					{
						v : $scope.schoolDetails.retentionRate
					},
				]
			},
			{
				c : [
					{
						v : "2016"
					},
					{
						v : 100 - $scope.schoolDetails.retentionRate
					},
				]
			}
		]
	};

	$scope.myChartObject.options = {
		'legend' : 'none',
		tooltip : {
			trigger : 'none'
		},
		slices : {
			0 : {
				color : '00acad'
			},
			1 : {
				color : 'transparent'
			}
		}
	};
	
	/**
	 * Test score graph
	 */
	$scope.satCritica25Percent = 268*$scope.schoolDetails.satCritica25Percent/800;
	$scope.satCritica75Percent = 268*$scope.schoolDetails.satCritica75Percent/800;
	
	$scope.satMath25Percent = 268*$scope.schoolDetails.satMath25Percent/800;
	$scope.satMath75Percent = 268*$scope.schoolDetails.satMath75Percent/800;

	$scope.actCritical25Percent = 268*$scope.schoolDetails.actCritical25Percent/31;
	$scope.actCritical75Percent = 268*$scope.schoolDetails.actCritical75Percent/31;
	
	/**
	 * Degrere and major modal popup.
	 */
	$scope.majorPopup = function(title, description) {
		var customModalOptions = {
				headerText : title,
				bodyText : description
			};
			var customModalDefaults = {size : 'lg'};
			modalService.show(customModalDefaults, customModalOptions);
	}
	

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
		
		/*$(function() {
			if (window.outerWidth < 500) return;
			$('.top-blue-panel').scrollToFixed();
			$('#sidebar.not-logged-in').scrollToFixed({
				limit: function () {
					var limit = $('#footerOne').offset().top - $(this).outerHeight(true) - 10;
					return limit;
				}
			})
			$('#sidebar.logged-in').scrollToFixed({
				limit: function () {
					var limit = $('#footerTwo').offset().top - $(this).outerHeight(true) - 10;
					return limit;
				}
			});
		});*/
		// tools mobile
		function topBluePanelMobile() {
			if($(window).outerWidth() < 640) {
				if($('.top-blue-panel .tools:not(.ng-hide)').length > 0 && $('.top-blue-panel+.top-blue-panel-spacing').length==0){
					$('.top-blue-panel').after('<div class="top-blue-panel-spacing"></div>');
					$('.top-blue-panel+.top-blue-panel-spacing').css('height', $('.top-blue-panel .tools:not(.ng-hide)').outerHeight() );
				}
			} else {
				$('.top-blue-panel+.top-blue-panel-spacing').remove();
			}
		}
		topBluePanelMobile();
		$(window).resize(function() {
			topBluePanelMobile();
		});
		
		if (window.outerWidth < 768) return;

		// sticky header for occupations && sticky sidebar
		var allSection = $('.top-blue-panel, #sidebar.logged-in, #sidebar.not-logged-in');
		allSection.each(function(i) {
			var section = $(allSection[i]);
			section.scrollToFixed({
				marginTop: function() {
					var HeaderHeight = $('#header').outerHeight(true) + $('#banner').outerHeight(true);
					if (HeaderHeight) {
						var marginTop = HeaderHeight;
					}
					return marginTop;
				},
				limit: function() {
					
					var footerOne = $('#footerOne').offset();
					var footerTwo = $('#footerOne').offset();
					if(footerOne != undefined && footerTwo != undefined){
						var footerHeight = $('#footerOne').offset().top + $('#footerTwo').offset().top;
						if (footerHeight) {
							var limit = footerHeight - $(this).outerHeight(true) - 10;
						}
						return limit;
					}
				},
				zIndex: 1
			});
		});
	});

} ]);