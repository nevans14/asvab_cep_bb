cepApp.controller('OccufindOccupationDetailsController', [ '$sce', '$scope', '$rootScope', '$location', 'OccufindSearchService', 'occupationTitleDescription', 'workValues', 'stateSalaryYear', 'nationalPovertyLevel', 'stateSalary', 'stateEntrySalary', 'AuthenticationService', 'skillImportance', 'tasks', 'occupationInterestCodes', 'educationLevel', 'relatedOccupations', 'hotGreenStemFlags', 'servicesOffering', 'relatedCareerCluster', '$q', '$route', 'UserFactory', 'modalService', 'ApprenticeshipModalService', 'CertificationListModalService', 'CertificationDetailsService', 'LicensesListModalService', 'LicenseDetailsModalService', 'occupationCertificates', 'moreResources', 'schoolDetails', 'nationalSalary', 'nationalEntrySalary', 'LoginModalService', 'blsTitle', 'getAlternateView', 'imageAltText', 'jobZone', 'certificateExplanation', 'alternateTitles', 'UtilityService', 'favorites', 'awsMediaUrl', function($sce, $scope, $rootScope, $location, OccufindSearchService, occupationTitleDescription, workValues, stateSalaryYear, nationalPovertyLevel, stateSalary, stateEntrySalary, AuthenticationService, skillImportance, tasks, occupationInterestCodes, educationLevel, relatedOccupations, hotGreenStemFlags, servicesOffering, relatedCareerCluster, $q, $route, UserFactory, modalService, ApprenticeshipModalService, CertificationListModalService, CertificationDetailsService, LicensesListModalService, LicenseDetailsModalService, occupationCertificates, moreResources, schoolDetails, nationalSalary, nationalEntrySalary, LoginModalService, blsTitle, getAlternateView, imageAltText, jobZone, certificateExplanation, alternateTitles, UtilityService, favorites, awsMediaUrl) {

	$q.all([ occupationTitleDescription, workValues, skillImportance, stateSalaryYear, nationalPovertyLevel, stateSalary, tasks, occupationCertificates ]).then(function successCallback(response) {
		$scope.socId = $route.current.params.socId;
		$scope.occupationTitle = occupationTitleDescription.data.title;
		$scope.occupationDescription = occupationTitleDescription.data.description;
		$scope.skillImportance = skillImportance.data;
		$scope.stateSalaryYear = stateSalaryYear.data;
		$scope.nationalPovertyLevel = nationalPovertyLevel.data;
		$scope.stateSalary = stateSalary.data;
		$scope.stateEntrySalary = stateEntrySalary.data;
		$scope.nationalSalary = nationalSalary.data;
		$scope.nationalEntrySalary = nationalEntrySalary.data;		
		$scope.blsTitle = blsTitle.data;
		$scope.tasks = tasks.data;
		$scope.occupationInterestCodes = occupationInterestCodes.data;
		$scope.educationLevel = educationLevel.data;
		$scope.relatedOccupations = relatedOccupations.data;
		$scope.servicesOffering = servicesOffering.data;
		$scope.relatedCareerCluster = relatedCareerCluster.data;
		$scope.moreResources = moreResources.data;
		$scope.alternateView = getAlternateView.data;
		$scope.imageUrl = awsMediaUrl + 'OCCUFIND_IMGS/' + $scope.socId + '.jpg';		
		$scope.imageAltText = imageAltText.data;
		$scope.absUrl = UtilityService.getCanonicalUrl();
		$scope.domainUrl = 'https://asvabprogram.com'
		$scope.jobZone = jobZone.data;
		$scope.alternateTitles = alternateTitles.data;
		$scope.certificateExplanation = certificateExplanation.data.licensing;
		$scope.favorites = undefined;
		$scope.workValues = undefined;
		$scope.screenWidth = screen.width;
		$scope.socId = $route.current.params.socId;
		
		if ($rootScope.globals.currentUser) {
			$scope.favorites = favorites.data;
			$scope.workValues = workValues.data;
		}

		$scope.showImageUrl = function() {
			return !($scope.occupationTitle.toLowerCase().includes('all other')
				|| $scope.socId === 55-2011.00
				|| $scope.socId === 55-2012.00);
		}
		
		$scope.showModifiedMsg = function() {
			var socId = $route.current.params.socId;
			if (socId != undefined) {
				var firstThreePositions = socId.substring(0, 3);
				if (firstThreePositions == '55-') {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}

		$scope.showBLSStatement = function() {
			var socId = $route.current.params.socId;
			if (socId != undefined) {
				var lastTwoPositions = socId.slice(-2);
				if (lastTwoPositions == '00') {
					return false;
				} else {
					return true;
				}
			}
			return false;
		}

		$scope.ShrinkDescription = function(description, size) {
			if (description.length <= size) {
				return description;
			} else {
				return description.substring(0, size) + "...";
			}
		};
		
		$scope.occupationGreen = hotGreenStemFlags.data.isGreenOccupation == "1" ? true : false;
		$scope.occupationStem = hotGreenStemFlags.data.isStemOccupation == "1" ? true : false;
		$scope.occupationBright = hotGreenStemFlags.data.isBrightOccupation == "1" ? true : false;

		if (typeof occupationInterestCodes.data[0] != 'undefined') {
			$scope.occupationInterestOne = occupationInterestCodes.data[0].interestCd;
			$scope.occupationInterestTwo = occupationInterestCodes.data[1].interestCd;
			$scope.occupationInterestThree = occupationInterestCodes.data[2].interestCd;
		}
		if (IsJsonString(occupationCertificates.data.payload))
			$scope.occupationCertificates = JSON.parse(occupationCertificates.data.payload);
		else
			$scope.occupationCertificates = occupationCertificates.data.payload;
		
		$scope.schoolDetails = schoolDetails.data;

		if($scope.occupationCertificates)
			if (IsJsonString($scope.occupationCertificates)) {
				$scope.occupationCertificates.TotalItems = 0;
			} else {
				$scope.occupationCertificates.TotalItems = $scope.occupationCertificates.RecordCount; // occupationCertificates.CertificationInfo.CertificationList.length;
			}
		
		if (typeof $rootScope.globals.currentUser === 'undefined') {
			$scope.occuFindUserLoggedIn = false;
		} else {
			$scope.occuFindUserLoggedIn = true;
		}
		/**
		 * Skill rating
		 */
		angular.element(document).ready(function() {
			$('.customrating').each(function() {
				var rating = $(this).data('customrating');
				var width = $(this).width() / 5 * rating;
				if (rating.toString().indexOf('.') !== -1) {
					width += 1;
				}
				$(this).width(width);
			});
			
			
			/**
			 * New star rating.
			 */
			$(function() {
			    $( '.ratebox' ).raterater({ 

			    // allow the user to change their mind after they have submitted a rating
			    allowChange: false,

			    // width of the stars in pixels
			    starWidth: 12,

			    // spacing between stars in pixels
			    spaceWidth: 0,

			    numStars: 5,
			    isStatic: true,
			    step: false,
			    });

			});
		});

		
		function IsJsonString(str) {
		    try {
		        JSON.parse(str);
		    } catch (e) {
		        return false;
		    }
		    return true;
		}

		/**
		 * Salary map implementation
		 */
		var outerArray = [];
		var innerArray = [ 'State', 'AverageSalary', {
			type : 'string',
			role : 'tooltip'
		} ];
		outerArray[0] = innerArray;
		for (var i = 0; i < $scope.stateSalary.length; i++) {
			if ($scope.stateSalary[i].avgSalary != -1) {
				var innerArray = [ $scope.stateSalary[i].state, $scope.stateSalary[i].avgSalary, 'Average Salary: $' + $scope.stateSalary[i].avgSalary.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') ];
			} else {
				var innerArray = [ $scope.stateSalary[i].state, 187200, 'Average Salary: $' + '187,200 or more' ];
			}

			outerArray[i + 1] = innerArray;
		}

		var chart1 = {};
		chart1.type = "GeoChart";
		chart1.data = outerArray;
		chart1.options = {
			width : 480,
			/*
			 * width : 406, height : 297,
			 */
			region : "US",
			resolution : "provinces",
			colors : [ '#ffffff', '#02767b' ]
		};
		$scope.chart = chart1;
		
		var entryOuterArray = [];
		var entryInnerArray = [ 'State', 'EntrySalary', {
			type : 'string',
			role : 'tooltip'
		} ];
		entryOuterArray[0] = entryInnerArray;
		for (var i = 0; i < $scope.stateEntrySalary.length; i++) {
			if ($scope.stateEntrySalary[i].entrySalary == undefined) {
				var entryInnerArray = [ $scope.stateEntrySalary[i].state, 0, 'Entry Salary: N/A' ];
			} else if ($scope.stateEntrySalary[i].entrySalary != -1) {
				var entryInnerArray = [ $scope.stateEntrySalary[i].state, $scope.stateEntrySalary[i].entrySalary, 'Entry Salary: $' + $scope.stateEntrySalary[i].entrySalary.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') ];
			} else {
				var entryInnerArray = [ $scope.stateEntrySalary[i].state, 208000, 'Entry Salary: $208000 or more' ];
			}
			entryOuterArray[i + 1] = entryInnerArray;
		}
		
		// entry-level state salary
		var entryLevelSalaryChart = {};
		entryLevelSalaryChart.type = "GeoChart";
		entryLevelSalaryChart.data = entryOuterArray;
		entryLevelSalaryChart.options = {
				width: 480,
				region: "US",
				resolution: "provinces",
				colors: [ '#fff', '#02737b' ]
		}
		$scope.entryLevelSalaryChart = entryLevelSalaryChart;

		// mobile version
		var chart2 = {};
		chart2.type = "GeoChart";
		chart2.data = outerArray;
		chart2.options = {
			width : 300,
			/*
			 * width : 406, height : 297,
			 */
			region : "US",
			resolution : "provinces",
			colors : [ '#ffffff', '#02767b' ]
		};
		$scope.chartMobile = chart2;
		
		var entryLevelSalaryChartMobile = {};
		entryLevelSalaryChartMobile.type = "GeoChart";
		entryLevelSalaryChartMobile.data = entryOuterArray;
		entryLevelSalaryChartMobile.options = {
			width: 300,
			region: "US",
			resolution: "provinces",
			colors: [ '#fff', '#02767b' ]
		}
		$scope.entryLevelSalaryChartMobile = entryLevelSalaryChartMobile;
		
		$scope.logout = function() {
			AuthenticationService.ClearCredentials();
		};

		$scope.showLoginModal = function() {
			LoginModalService.show({}, {});
		}

		/**
		 * Bar chart Implementation
		 */
		
		var barColor = ['blue1', 'blue2', 'blue3', 'blue4', 'purple1', 'purple2', 'purple3'];

		function shortenEducationName(education) {
//			Delete “or the equivalent” after High School Diploma. 
			if(education.indexOf("High School Diploma or the equivalent") != -1) {
				return "High School Diploma";
			}
//			Change "Less than a high school diploma" to "Some High School"
			if(education.indexOf("Less than a High School") != -1) {
				return "Some High School";
			}
//			Change “Some College Education” to “Some College"
			if(education.indexOf("Some College Courses") != -1) {
				return "Some College";
			}
//			Change "Post Baccalaureate Certificate" to "Post Baccalaureate"
			if(education.indexOf("Post Baccalaureate Certificate") != -1) {
				return "Post Baccalaureate";
			}
//			Change “Associate's Degree (or other 2-year degree)” to "Associate's Degree"
			if(education.indexOf("Associate's Degree (or other 2-year degree)") != -1) {
				return "Associate's Degree";
			}
			return education;
		}
		
		
		$scope.barList= [];
		for (var i = 0; i < $scope.educationLevel.length; i++) {
			var educationValue = Math.round($scope.educationLevel[i].educationLvlVal);
			if(educationValue === 0) {
				console.log(educationValue);
				continue;
			}
			
			var data = {};
			var education = shortenEducationName($scope.educationLevel[i].educationDesc);
			
			data['school'] = education;
			data['value'] = educationValue;
			data['color'] = barColor.shift();
			

			// Just in case there are more than 7 education, re-use color
			barColor.push(data['color']);

			$scope.barList.push(data);
		}
		


		/**
		 * Pie chart Implementation
		 */

		
//		$scope.chartObject = {};
//		var col = [ {
//			id : "e",
//			label : "Education",
//			type : "string"
//		}, {
//			id : "v",
//			label : "Value",
//			type : "number"
//		} ];
//		var row = [];
//
//		for (var i = 0; i < $scope.educationLevel.length; i++) {
//			var c = {
//				c : [ {
//					v : $scope.educationLevel[i].educationDesc
//				}, {
//					v : $scope.educationLevel[i].educationLvlVal
//				} ]
//			};
//			row.push(c);
//		}
//
//		$scope.chartObject.data = {
//			"cols" : col,
//			"rows" : row
//		};
//
//		$scope.chartObject.type = "PieChart";
//		$scope.chartObject.options = {
//			colors : [ '#02767b', '#29afb5', '#8bd6d9', '#cee3e7', '#9593b8', '#2f2a6a', '#5c5792', '#8c5050', '#885a4c', '#d69785' ]
//		};
//
//		$scope.chartPie = $scope.chartObject;

	}, function errorCallback(response) {
		alert("error, try refreshing");
	});

	$scope.relatedCareers = function(onetSoc) {
		OccufindSearchService.selectedOnetSoc.push(onetSoc);
		/* $route.reload(); */
		$location.url('/occufind-occupation-details/' + onetSoc);

	}

	// direct to military more details page
	$scope.militaryMoreDetails = function() {
		$location.url('/occufind-military-details/' + $route.current.params.socId);
	}

	// direct to school more details page
	$scope.schoolMoreDetails = function() {
		$location.url('/occufind-education-details/' + $route.current.params.socId);
	}

	// direct to school more details page
	$scope.employmentMoreDetails = function() {
		$location.url('/occufind-employment-details/' + $route.current.params.socId);
	}

	$scope.findCertifications = function() {
		CertificationListModalService.show({
			certs : $scope.occupationCertificates,
			occupationTitle : $scope.occupationTitle
		}, {});
	}

	$scope.findLicenses = function() {
		LicensesListModalService.show({
			socId : $scope.socId,
			occupationTitle : $scope.occupationTitle
		}, {});
	}

	$scope.findApprenticeship = function() {
		ApprenticeshipModalService.show({
			socId : $route.current.params.socId,
			occupationTitle : $scope.occupationTitle
		}, {});
	}
	

	$scope.orderByService = function(item) {
		if (item.serviceCode == 'A') {
			return 1;
		} else if (item.serviceCode == 'M') {
			return 2;
		} else if (item.serviceCode == 'N') {
			return 3;
		} else if (item.serviceCode == 'F') {
			return 4;
		} else if (item.serviceCode == 'S') {
			return 5;
		} else if (item.serviceCode == 'C') {
			return 6;
		} else if (item.serviceCode == 'G') {
			return 7;
		} else {
			return 8;
		}
	}

	$scope.checkWorkValueType = function(workValueCode) {

		var workValueType = undefined;

		switch(workValueCode) {
			case 'A':
				workValueType = 1;
				break;
			case 'I':
				workValueType = 2;
				break;
			case 'REC':
				workValueType = 3;
				break;
			case 'REL':
				workValueType = 4;
				break;
			case 'S':
				workValueType = 5;
				break;
			case 'W':
				workValueType = 6;
				break;
		}

		return $scope.workValues.some(function(i) {
			return i.typeId === workValueType;
		})
	}
	

	/**
	 * Opens modal popup for description for each section.
	 */
	$scope.openDescription = function(section) {

		var headerTitle, bodyText;
		if (section == 'interest-codes') {
			headerTitle = "Interest Codes";
			bodyText = "RIASEC codes represent the interest codes most closely related to each occupation. Evaluate how well the interest codes for this occupation match your interests."
		} else if(section == 'skill-importance'){
			headerTitle = 'Skill Importance Ratings';
			bodyText = 'These ratings show the relative importance of Verbal, Math, and Science/Technical skills for performing the job. The Skill Importance Ratings are not the same as Career Exploration Scores, but can be used to evaluate suitability for the job.';
		} else if(section == 'what-they-do'){
			headerTitle = 'What They Do';
			bodyText = 'This is a list of tasks performed on the job.';
		} else if(section == 'how-to-get-there'){
			headerTitle = 'How to Get There';
			bodyText = 'There may be different ways to get started in the career you want.  Learn about the education, credentials, licenses, apprenticeships, and military opportunities below.';
		} else if(section == 'education'){
			headerTitle = 'Education';
			bodyText = ' This section describes the levels of education seen as necessary to get this job as reported by people who work in the job. '+
					'  You can find a list of institutions that offer degrees related to this occupation under College Info.  '+
						' These totals are rounded to the nearest percent. The actual descriptions of levels of education included are as follows:<br/><br/>  '+
						' <ul> '+
							' <li>Less than a High School Diploma </li> '+
							' <li>High School Diploma or the equivalent (for example GED)</li> '+
							' <li>Post-Secondary Certificate</li> '+
							' <li>Some College Courses</li> '+
							' <li>Associate\'s Degree (or other 2-year degree)</li> '+
							' <li>Bachelor\'s Degree</li> '+
							' <li>Post-Baccalaureate Certificate</li> '+
							' <li>Master\'s Degree</li> '+
							' <li>Post-Master\'s Certificate</li> '+
							' <li>First Professional Degree</li> '+
							' <li>Doctoral Degree</li> '+
							' <li>Post-Doctoral Training</li> '+
						' </ul>';
		} else if(section == 'credentials'){
			headerTitle = 'Credentials';
			bodyText = 'Certification, licensure, and/or apprenticeships are possible paths to this career. Select Find Certification, Find Licenses, and Find Apprenticeships to explore these options.';
		} else if(section == 'mil-offering'){
			headerTitle = 'Military Services Offering this Occupation';
			bodyText = 'This job is available in the Military.  Select Military Info to learn more about which branches offer related opportunities.';
		} else if(section == 'emp-stats'){
			headerTitle = 'Employment Statistics';
			bodyText = 'This section illustrates the average salary for this occupation by state. Select More Details to learn more about the national employment outlook.';
		} else if(section == 'poverty-level'){
			headerTitle = 'Poverty Level';
			bodyText = 'This number is issued each year by the Department of Health and Human Services (HHS). It represents the poverty guidelines for a single individual living in the 48 contiguous states. The number varies for citizens of Alaska and Hawaii, and increases with each additional member of the household.';
		} else if(section == 'work-values'){
			headerTitle = 'Work Values';
			bodyText = 'The work values listed represent those most closely related to this occupation. Evaluate how well the work values for this occupation match your own.';
		} else if(section == 'achievement'){
			headerTitle = 'ACHIEVEMENT';
			bodyText = 'Those who score high on Achievement are results-oriented. These workers often pursue jobs where employees are able to apply their strengths and abilities. This gives the employee a sense of accomplishment.';
		} else if(section == 'independence'){
			headerTitle = 'INDEPENDENCE';
			bodyText = 'Those who score high on Independence value the ability to approach work activities with creativity. These workers want to make their own decisions and plan their work with little supervision from a manager.';
		} else if(section == 'recognition'){
			headerTitle = 'RECOGNITION';
			bodyText = 'Those who score high on Recognition pursue jobs with opportunities for advancement and leadership responsibilities that allow them to give direction and instruction to others. These workers are often considered prestigious by their peers and others in their organization and receive recognition for the work they contribute.';
		} else if(section == 'relationships'){
			headerTitle = 'RELATIONSHIPS';
			bodyText = 'Those who score high on Relationships prefer jobs that provide services to others and work with others in a friendly, non-competitive environment. Workers in these jobs value getting along well with others and do not like to be pressured to do things that go against their morals or sense of what is right and wrong.';
		} else if(section == 'support'){
			headerTitle = 'SUPPORT';
			bodyText = 'Those who score high on Support appreciate when their company\'s leadership stands behind and supports their employees. People in these types of jobs like to feel like they are being treated fairly by the company and have supervisors who spend time and effort training their workers to perform well.';
		} else if(section == 'working conditions'){
			headerTitle = 'WORKING CONDITIONS';
			bodyText = 'Those who score high on Working Conditions value job security and pleasant working conditions. These workers enjoy being busy and want to be paid well for the work they do. They enjoy developing ways of doing things with little or no supervision and depend on themselves to get the work done. These workers pursue steady employment that offers something different to do on a daily basis.';
		} else {
			return 0;
		}

		var modalOptions = {
			headerText : headerTitle,
			bodyText : bodyText
		};
		
		var screenSize = 'sm';
		if(section == 'education') {
			screenSize = 'md';
		}

		modalService.showModal({
			size : screenSize
		}, modalOptions);
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
		
		$('a.chart-link').click(function (e) {
			e.preventDefault();
		});
		
		// tools mobile
		function topBluePanelMobile() {
			if($(window).outerWidth() < 640) {
				if($('.top-blue-panel .tools:not(.ng-hide)').length > 0 && $('.top-blue-panel+.top-blue-panel-spacing').length==0){
					$('.top-blue-panel').after('<div class="top-blue-panel-spacing"></div>');
					$('.top-blue-panel+.top-blue-panel-spacing').css('height', $('.top-blue-panel .tools:not(.ng-hide)').outerHeight() );
				}
			} else {
				$('.top-blue-panel+.top-blue-panel-spacing').remove();
			}
		}
		topBluePanelMobile();
		$(window).resize(function() {
			topBluePanelMobile();
		});
		
		if (window.outerWidth < 768) return;

		// sticky header for occupations && sticky sidebar
		var allSection = $('.top-blue-panel, #sidebar.logged-in, #sidebar.not-logged-in');
		allSection.each(function(i) {
			var section = $(allSection[i]);
			section.scrollToFixed({
				marginTop: function() {
					var HeaderHeight = $('#header').outerHeight(true) + $('#banner').outerHeight(true);
					if (HeaderHeight) {
						var marginTop = HeaderHeight;
					}
					return marginTop;
				},
				limit: function() {
					var limit = 0;
					if ($('#footerOne').offset() && $('#footerTwo').offset()) {
						var footerHeight = $('#footerOne').offset().top + $('#footerTwo').offset().top;
						if (footerHeight) {
							limit = footerHeight - $(this).outerHeight(true) - 10;
						} else {
							limit = footerHeight;
						}
					}
					return limit;
				},
				zIndex: 1
			});
		});
	});

} ]).directive('showMore', function() {
	  return {
	        restrict: 'A',
	        transclude: true,
	        template: [
	            '<div class="show-more-container"><ng-transclude></ng-transclude></div>',
	            '<a href="#" class="show-more-expand">Show More ...</a>',
	            '<a href="#" class="show-more-collapse">... Show Less</a>',
	        ].join(''),
	        link: function(scope, element, attrs, controller) {
	            var maxHeight = 45;
	            var initialized = null;
	            var containerDom = element.children()[0];
	            var $showMore = angular.element(element.children()[1]);
	            var $showLess = angular.element(element.children()[2]);

	            scope.$watch(function () {
	                // Watch for any change in the innerHTML. The container may start off empty or small,
	                // and then grow as data is added.
	                return containerDom.innerHTML;
	            }, function () {
	                if (null !== initialized) {
	                    // This collapse has already been initialized.
	                    return;
	                }

	                if (containerDom.clientHeight <= maxHeight) {
	                    // Don't initialize collapse unless the content container is too tall.
	                    return;
	                }

	                $showMore.on('click', function () {
	                    element.removeClass('show-more-collapsed');
	                    element.addClass('show-more-expanded');
	                    containerDom.style.height = null;
	                });

	                $showLess.on('click', function () {
	                    element.removeClass('show-more-expanded');
	                    element.addClass('show-more-collapsed');
	                    containerDom.style.height = maxHeight + 'px';
	                });

	                initialized = true;
	                $showLess.triggerHandler('click');
	            });
	        },
	  };
});