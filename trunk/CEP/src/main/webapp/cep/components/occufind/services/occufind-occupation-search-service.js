cepApp.service('OccufindSearchService', function($cookieStore, $rootScope) {
	
	var occufindSearchService = {
		userSearch : {
			interestCodeOne : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.interestCodeOne ? $cookieStore.get('globals').userSearch.interestCodeOne : undefined,
			interestCodeTwo : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.interestCodeTwo ? $cookieStore.get('globals').userSearch.interestCodeTwo : undefined,
			brightOccupationFlag : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.brightOccupationFlag ? $cookieStore.get('globals').userSearch.brightOccupationFlag : false,
			greenOccupationFlag : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.greenOccupationFlag ? $cookieStore.get('globals').userSearch.greenOccupationFlag : false,
			stemOccupationFlag : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.stemOccupationFlag ? $cookieStore.get('globals').userSearch.stemOccupationFlag : false,
			hotOccupationFlag : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.hotOccupationFlag ? $cookieStore.get('globals').userSearch.hotOccupationFlag : false,
			userSearchString : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.userSearchString ? $cookieStore.get('globals').userSearch.userSearchString : undefined
		},
		skillSelected : undefined,
		selectedOnetSoc : [],
		selectedOcupationTitle : undefined,
		occupationGreen : false,
		occupationStem : false,
		occupationBright : false,
		occupationHot : false,
		occupationInterestOne : undefined,
		occupationInterestTwo: undefined,
		occupationInterestThree: undefined,
		currentPage : 1,
		relatedCarreers : []
	};
	
	occufindSearchService.getOnetSoc = function(){
		return occufindSearchService.selectedOnetSoc[occufindSearchService.selectedOnetSoc.length - 1];
	};

	occufindSearchService.getSelectedTitleOne = function(interestCd) {
		if (interestCd != null) {
			switch (interestCd) {
			case "R":
				return "Realistic";
			case "I":
				return "Investigative";
			case "A":
				return "Artistic";
			case "S":
				return "Social";
			case "E":
				return "Enterprising";
			case "C":
				return "Conventional";
			}
		}
	};

	occufindSearchService.getSelectedTitleTwo = function(interestCd) {
		if (interestCd != null) {
			switch (interestCd) {
			case "R":
				return "Realistic";
			case "I":
				return "Investigative";
			case "A":
				return "Artistic";
			case "S":
				return "Social";
			case "E":
				return "Enterprising";
			case "C":
				return "Conventional";
			}
		}
	};

	return occufindSearchService;

	/*
	 * var occupationList; var interestSelected; var skillSelected; var
	 * skillFilterView; var selectedOccupationTitle; var selectedOnetSoc; var
	 * occupationDescriptionASK; var militaryResource; var oohResource; var
	 * stateSalary;
	 * 
	 * this.getStateSalary = function() { return stateSalary; }
	 * 
	 * this.getOohResource = function() { return oohResource; }
	 * 
	 * this.getmilitaryResource = function() { return militaryResource; }
	 * 
	 * this.getOccupationDescriptionASK = function() { return
	 * occupationDescriptionASK; }
	 * 
	 * this.getSelectedOnetSoc = function() { return selectedOnetSoc; }
	 * 
	 * this.getSelectedOccupationTitle = function() { return
	 * selectedOccupationTitle; }
	 * 
	 * this.getOccupationList = function() { return occupationList; }
	 * 
	 * this.getInterestSelected = function() { return interesteSelected; }
	 * 
	 * this.getSkillSelected = function() { return skillSelected; }
	 * 
	 * this.getSkillFilterText = function() { switch (this.skillSelected) { case
	 * "V": return "verbalSkill"; case "M": return "mathSkill"; case "S": return
	 * "sciTechSkill"; } }
	 * 
	 * this.getSkillFilterView = function() {
	 * 
	 * if (this.skillSelected === undefined) { skillFilterView = false; } else {
	 * skillFilterView = true; }
	 * 
	 * return skillFilterView; }
	 * 
	 * this.getInterestText = function() { switch (this.interestSelected) { case
	 * "R": return "Realistic"; case "I": return "Investigative"; case "A":
	 * return "Artistic"; case "S": return "Social"; case "E": return
	 * "Enterprising"; case "C": return "Conventional"; } }
	 */
});
