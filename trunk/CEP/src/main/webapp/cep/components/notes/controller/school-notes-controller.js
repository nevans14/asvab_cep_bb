cepApp.controller('SchoolNotesController', [ '$scope', 'UserFactory', 'modalService', '$route', 'SchoolNotesModalService', function($scope, UserFactory, modalService, $route, SchoolNotesModalService) {

	/**
	 * Used on school details page.
	 */
	$scope.showSchoolNotes = function() {
		SchoolNotesModalService.show({
			size : 'lg'
		}, {
			title : $scope.schoolTitle,
			unitId : $route.current.params.unitId
		});
	}

	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotes = function(title, unitId) {
		SchoolNotesModalService.show({
			size : 'lg'
		}, {
			title : title,
			unitId : unitId
		});
	}

} ]);
