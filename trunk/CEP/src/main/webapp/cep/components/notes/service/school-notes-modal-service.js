cepApp.service('SchoolNotesModalService', [ '$uibModal', '$sce', 'UserFactory', 'SchoolNotesService', '$rootScope', '$q', function($uibModal, $sce, UserFactory, SchoolNotesService, $rootScope, $q) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/notes/page-partials/school-notes-modal.html'
	};

	var modalOptions = {};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {

			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.noteList = [];
				$scope.initialNotesObject = $scope.selectedNotesObject = {};
				
				$scope.getNote = function (unitId) {
					// iterates over list to see if we find a match
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].unitId === parseInt(unitId)) {
							// return the match
							return {
								userId	: $scope.noteList[i].userId,
								unitId	: $scope.noteList[i].unitId,
								title	: $scope.noteList[i].title,
								notes	: $scope.noteList[i].notes,
								noteId	: $scope.noteList[i].noteId
							}
						}
					}
					// return default
					return {
						unitId	: $scope.modalOptions.unitId,
						title	: $scope.modalOptions.title,
						notes	: undefined
					}
				}
				
				$scope.getNoteByNoteId = function (noteId) {
					// iterates over list to see if we find a match
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].noteId === noteId) {
							// return the match
							return {
								userId	: $scope.noteList[i].userId,
								unitId	: $scope.noteList[i].unitId,
								title	: $scope.noteList[i].title,
								notes	: $scope.noteList[i].notes,
								noteId	: $scope.noteList[i].noteId
							}
						}
					}
					// return default
					return {
						unitId	: $scope.modalOptions.unitId,
						title	: $scope.modalOptions.title,
						notes	: undefined
					}
				}
				
				$scope.initializeSetup = function () {
					SchoolNotesService.getNotes().then(function (data) {
						$scope.noteList = data;
						
						if ($scope.modalOptions.unitId === undefined && $scope.noteList.length > 0) {
							$scope.modalOptions.unitId = $scope.noteList[0].unitId;
							$scope.modalOptions.title = $scope.noteList[0].title;
						}
						
						$scope.initialNotesObject = $scope.selectedNotesObject = 
							$scope.getNote($scope.modalOptions.unitId);
					});
				}				

				/**
				 * Reset to current occupation object.
				 */
				$scope.resetObject = function() {					
					$scope.initialNotesObject = $scope.selectedNotesObject = 
						$scope.getNote($scope.selectedNotesObject.unitId);
				}

				/**
				 * Save Notes
				 */
				$scope.modalOptions.save = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					$scope.isPerformingAction = true;
					var notesExist = false;

					if ($scope.selectedNotesObject.notes == undefined || $scope.selectedNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						$scope.isPerformingAction = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].unitId == $scope.selectedNotesObject.unitId) {
							notesExist = true;
						}
					}

					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						SchoolNotesService.insertNote($scope.selectedNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {
						SchoolNotesService.updateNote($scope.selectedNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};
				
				/**
				 * deleting note
				 */
				$scope.modalOptions.deleteNote = function () {
					var isConfirmed = confirm('Do you want to remove this note?');
					if (!isConfirmed) return;
					$scope.error = '';
					$scope.deletingNote = true;
					$scope.isPerformingAction = true;
					SchoolNotesService.deleteNote($scope.selectedNotesObject.noteId).then(function (success) {
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.initializeSetup();
						if ($scope.noteList.length === 0) {
							$uibModalInstance.dismiss('cancel');
						}
					}, function (error) {
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.error = 'There was an error processing your request. Please try again.';
					});
				}

				/**
				 * Within the modal, user can toggle between occupations that
				 * they saved notes for.
				 */
				$scope.selectSchool = function(noteId) {
					// set the information to display to the user depending on
					// their selection
					$scope.selectedNotesObject = $scope.getNoteByNoteId(noteId);
				}
				
				$scope.initializeSetup();

				$scope.modalOptions.close = function() {
					$uibModalInstance.dismiss('cancel');
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};
} ]);