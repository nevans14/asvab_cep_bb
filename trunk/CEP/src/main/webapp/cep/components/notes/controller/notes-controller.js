cepApp.controller('NotesController', [ '$scope', 'UserFactory', 'modalService', '$route', 'NotesModalService', 'SchoolNotesModalService', function($scope, UserFactory, modalService, $route, NotesModalService,SchoolNotesModalService) {

	/**
	 * Used on Occu-find details page.
	 */
	$scope.showOccufindNotes = function() {
		NotesModalService.show({
			size : 'lg'
		}, {
			title : $scope.occupationTitle,
			socId : $route.current.params.socId
		});
	}

	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotes = function(title, socId) {
		NotesModalService.show({
			size : 'lg'
		}, {
			title : title,
			socId : socId
		});
	}
	
	/**
	 * Used on school details page.
	 */
	$scope.showSchoolNotes = function() {
		NotesModalService.show({
			size : 'lg'
		}, {
			schoolName : $scope.schoolTitle,
			unitId : $route.current.params.unitId
		});
	}

	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotesSchool = function(title, unitId) {
		NotesModalService.show({
			size : 'lg'
		}, {
			schoolName : title,
			unitId : unitId
		});
	}

	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotesCC = function(title, ccId) {
		NotesModalService.show({
			size 	: 'lg'
		}, {
			ccTitle : title,
			ccId	: ccId
		})
	}

} ]);
