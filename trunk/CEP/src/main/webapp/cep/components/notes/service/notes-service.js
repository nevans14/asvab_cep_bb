cepApp.factory('NotesService', [ '$q', '$window', '$rootScope', 'NotesRestFactory', function($q, $window, $rootScope, NotesRestFactory) {

	var notesService = {};

	var noteList = undefined;

	/**
	 * Uses notes rest service to insert notes into database. Utilize session
	 * storage. Returns a promise
	 */
	notesService.insertNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.insertNote(selectedNotesObject).success(function(data) {

			// push newly added occupation to property
			noteList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('noteList', JSON.stringify(noteList));

				// sync with in memory property
				noteList = angular.fromJson($window.sessionStorage.getItem('noteList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to update user's note information. Utilize
	 * session storage. Returns a promise.
	 */
	notesService.updateNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.updateNote(selectedNotesObject).success(function(data) {

			if (data !== undefined) {
				// sync noteList
				var length = noteList.length;
				for (var i = 0; i < length; i++) {
					if (noteList[i].noteId == selectedNotesObject.noteId) {
						noteList[i] = selectedNotesObject;
					}
				}

				// sync with session storage
				if (typeof (Storage) !== "undefined") {
					$window.sessionStorage.setItem('noteList', JSON.stringify(noteList));

					// sync with in memory property
					noteList = angular.fromJson($window.sessionStorage.getItem('noteList'));
				}

				deferred.resolve("Update successful.");
			}
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}
	
	notesService.deleteNote = function(noteId) {
		var deferred = $q.defer();		
		NotesRestFactory.deleteNote(noteId).success(function(data) {
			// sync noteList
			for (var i = 0; i < noteList.length; i++) {
				if (noteList[i].noteId === noteId) {
					noteList.splice(i, 1);
					break;
				}
			}
			// sync with session storage
			if (typeof(Storage) !== undefined) {
				$window.sessionStorage.setItem('noteList', JSON.stringify(noteList));
			}
			deferred.resolve(noteList);
		}).error(function (data, status, headers, config) {
			deferred.reject('Error: request returned status ' + status);
		});		
		return deferred.promise;
	}

	/**
	 * Uses notes rest service to get all user's notes. Utilizes session
	 * storage if user refreshes page. If browser doesn't support session storage, then data will be
	 * obtained by using rest service for each call.
	 */
	notesService.getNotes = function() {
		if ($rootScope.globals.currentUser === undefined) return;
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (noteList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("noteList") === null || $window.sessionStorage.getItem("noteList") == 'undefined') {

					// recover data using database
					NotesRestFactory.getNotes().success(function(data) {
						if (data !== undefined) {
							// success now store in session object
							$window.sessionStorage.setItem('noteList', JSON.stringify(data));

							// sync with in memory property
							noteList = angular.fromJson($window.sessionStorage.getItem('noteList'));
						}
						deferred.resolve(noteList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					noteList = angular.fromJson($window.sessionStorage.getItem('noteList'));
					deferred.resolve(noteList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getNotes().success(function(data) {

					// success now store in session object
					noteList = data;
					deferred.resolve(noteList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(noteList);
		}

		return deferred.promise;
	}

	return notesService;

} ]);