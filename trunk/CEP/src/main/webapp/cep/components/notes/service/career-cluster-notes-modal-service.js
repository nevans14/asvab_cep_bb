cepApp.service('CareerClusterNotesModalService', [ '$uibModal', '$sce', 'UserFactory', 'CareerClusterNotesService', '$rootScope', '$q', function($uibModal, $sce, UserFactory, CareerClusterNotesService, $rootScope, $q) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/notes/page-partials/career-cluster-notes-modal.html'
	};

	var modalOptions = {};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {

			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				
				$scope.initialNotesObject = $scope.selectedNotesObject = {};
				$scope.careerClusterNoteList = [];
				
				$scope.initializeSetup = function () {
					CareerClusterNotesService.getCareerClusterNotes().then(function (data) {
						$scope.careerClusterNoteList = data;
						
						$scope.initialNotesObject = $scope.selectedNotesObject = {
							ccId	: $scope.modalOptions.ccId,
							title	: $scope.modalOptions.title,
							notes	: undefined
						};
						
						for (var i = 0; i < $scope.careerClusterNoteList.length; i++) {
							if ($scope.careerClusterNoteList[i].ccId === parseInt($scope.modalOptions.ccId)) {
								$scope.initialNotesObject = $scope.selectedNotesObject = {
									userId	: $scope.careerClusterNoteList[i].userId,
									ccId	: $scope.careerClusterNoteList[i].ccId,
									title	: $scope.careerClusterNoteList[i].title,
									notes	: $scope.careerClusterNoteList[i].notes,
									noteId	: $scope.careerClusterNoteList[i].noteId
								}
							}
						}
					})
				}

				/**
				 * Reset to current career cluster object.
				 */
				$scope.resetObject = function() {
					$scope.initialNotesObject = $scope.selectedNotesObject = {
						ccId : $scope.modalOptions.ccId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.careerClusterNoteList.length; i++) {
						if ($scope.careerClusterNoteList[i].ccId === $scope.selectedNotesObject.ccId) {
							$scope.initialNotesObject = $scope.selectedNotesObject = {
								ccId : $scope.careerClusterNoteList[i].ccId,
								title : $scope.careerClusterNoteList[i].title,
								notes : $scope.careerClusterNoteList[i].notes,
								noteId : $scope.careerClusterNoteList[i].noteId
							};
						}
					}
				}

				/**
				 * Save Notes
				 */
				$scope.modalOptions.save = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					$scope.isPerformingAction = true;
					var notesExist = false;

					if ($scope.selectedNotesObject.notes == undefined || $scope.selectedNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						$scope.isPerformingAction = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.careerClusterNoteList.length; i++) {
						if ($scope.careerClusterNoteList[i].ccId == $scope.selectedNotesObject.ccId) {
							notesExist = true;
						}
					}

					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						CareerClusterNotesService.insertCareerClusterNote($scope.selectedNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPeformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {
						CareerClusterNotesService.updateCareerClusterNote($scope.selectedNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};
				
				$scope.modalOptions.deleteNote = function () {
					var isConfirmed = confirm('Do you want to remove this note?');
					if (!isConfirmed) return;
					$scope.error = '';
					$scope.deletingNote = true;
					$scope.isPerformingAction = true;
					
					CareerClusterNotesService.deleteNote($scope.selectedNotesObject.noteId).then(function(success) {
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.initializeSetup();
						if ($scope.careerClusterNoteList.length === 0) {
							$uibModalInstance.dismiss('cancel');
						}
					}, function (error) {
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.error = 'There was an error processing your request. Please try again.';
					})
					
				}

				/**
				 * Within the modal, user can toggle between career clusters
				 * that they saved notes for.
				 */
				$scope.selectCareerCluster = function(noteId) {

					// set the information to display to the user depending on
					// their selection
					for (var i = 0; i < $scope.careerClusterNoteList.length; i++) {
						if ($scope.careerClusterNoteList[i].noteId === noteId) {
							$scope.selectedNotesObject = {
								ccId : $scope.careerClusterNoteList[i].ccId,
								title : $scope.careerClusterNoteList[i].title,
								notes : $scope.careerClusterNoteList[i].notes,
								noteId : $scope.careerClusterNoteList[i].noteId
							};
						}
					}
				}				

				$scope.initializeSetup();

				$scope.modalOptions.close = function() {
					$uibModalInstance.dismiss('cancel');
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};
} ]);