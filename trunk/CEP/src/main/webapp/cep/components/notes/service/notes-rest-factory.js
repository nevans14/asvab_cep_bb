cepApp.factory('NotesRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var notesRestFactory = {};

	notesRestFactory.getNotes = function() {
		return $http.get(appUrl + 'notes/getNotes');
	}

	notesRestFactory.insertNote = function(noteObject) {
		return $http.post(appUrl + 'notes/insertNote', noteObject);
	}

	notesRestFactory.updateNote = function(noteObject) {
		return $http.put(appUrl + 'notes/updateNote', noteObject);
	}
	
	notesRestFactory.deleteNote = function(noteId) {
		return $http.delete(appUrl + 'notes/deleteNote/' + noteId + '/');
	}

	notesRestFactory.getOccupationNotes = function(noteObject) {
		return $http.get(appUrl + 'notes/getOccupationNotes/' + noteObject.socId + '/');
	}
	
	notesRestFactory.getCareerClusterNotes = function() {
		return $http.get(appUrl + 'notes/getCareerClusterNotes');
	}

	notesRestFactory.insertCareerClusterNote = function(noteObject) {
		return $http.post(appUrl + 'notes/insertCareerClusterNote', noteObject);
	}

	notesRestFactory.updateCareerClusterNote = function(noteObject) {
		return $http.put(appUrl + 'notes/updateCareerClusterNote', noteObject);
	}
	
	notesRestFactory.deleteCareerClusterNote = function(ccNoteId) {
		return $http.delete(appUrl + 'notes/deleteCareerClusterNote/' + ccNoteId + '/');
	}

	notesRestFactory.getCareerClusterNote = function(noteObject) {
		return $http.get(appUrl + 'notes/getCareerClusterNote/' + noteObject.ccId + '/');
	}
	
	notesRestFactory.getSchoolNotes = function() {
		return $http.get(appUrl + 'notes/getSchoolNotes');
	}

	notesRestFactory.insertSchoolNote = function(noteObject) {
		return $http.post(appUrl + 'notes/insertSchoolNote', noteObject);
	}

	notesRestFactory.updateSchoolNote = function(noteObject) {
		return $http.put(appUrl + 'notes/updateSchoolNote', noteObject);
	}
	
	notesRestFactory.deleteSchoolNote = function(schoolNoteId) {
		return $http.delete(appUrl + 'notes/deleteSchoolNote/' + schoolNoteId + '/');
	}

	notesRestFactory.getSchoolNote = function(noteObject) {
		return $http.get(appUrl + 'notes/getSchoolNotes/' + noteObject.unitId + '/');
	}

	return notesRestFactory;

} ]);
