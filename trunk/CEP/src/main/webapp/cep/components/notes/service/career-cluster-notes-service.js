cepApp.factory('CareerClusterNotesService', [ '$q', '$window', '$rootScope', 'NotesRestFactory', function($q, $window, $rootScope, NotesRestFactory) {

	var notesService = {};

	var careerClusterNoteList = undefined;

	/**
	 * Uses notes rest service to insert notes into database. Utilize session
	 * storage. Returns a promise
	 */
	notesService.insertCareerClusterNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.insertCareerClusterNote(selectedNotesObject).success(function(data) {

			// push newly added career cluster to property
			careerClusterNoteList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(careerClusterNoteList));

				// sync with in memory property
				careerClusterNoteList = angular.fromJson($window.sessionStorage.getItem('careerClusterNoteList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to update user's note information. Utilize
	 * session storage. Returns a promise.
	 */
	notesService.updateCareerClusterNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.updateCareerClusterNote(selectedNotesObject).success(function(data) {

			// sync careerClusterNoteList
			var length = careerClusterNoteList.length;
			for (var i = 0; i < length; i++) {
				if (careerClusterNoteList[i].noteId == selectedNotesObject.noteId) {
					careerClusterNoteList[i] = selectedNotesObject;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(careerClusterNoteList));

				// sync with in memory property
				careerClusterNoteList = angular.fromJson($window.sessionStorage.getItem('careerClusterNoteList'));
			}

			deferred.resolve("Update successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}
	
	notesService.deleteNote = function(noteId) {
		var deferred = $q.defer();		
		NotesRestFactory.deleteCareerClusterNote(noteId).success(function(data) {
			// sync noteList
			for (var i = 0; i < careerClusterNoteList.length; i++) {
				if (careerClusterNoteList[i].noteId === noteId) {
					careerClusterNoteList.splice(i, 1);
					break;
				}
			}
			// sync with session storage
			if (typeof(Storage) !== undefined) {
				$window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(careerClusterNoteList));
			}
			deferred.resolve(careerClusterNoteList);
		}).error(function (data, status, headers, config) {
			deferred.reject('Error: request returned status ' + status);
		});		
		return deferred.promise;
	}

	/**
	 * Uses notes rest service to get all user's notes. Utilizes session
	 * storage if user refreshes page. If browser doesn't support session storage, then data will be
	 * obtained by using rest service for each call.
	 */
	notesService.getCareerClusterNotes = function() {
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (careerClusterNoteList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("careerClusterNoteList") === null || $window.sessionStorage.getItem("careerClusterNoteList") == 'undefined') {

					// recover data using database
					NotesRestFactory.getCareerClusterNotes().success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(data));

						// sync with in memory property
						careerClusterNoteList = angular.fromJson($window.sessionStorage.getItem('careerClusterNoteList'));

						deferred.resolve(careerClusterNoteList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					careerClusterNoteList = angular.fromJson($window.sessionStorage.getItem('careerClusterNoteList'));
					deferred.resolve(careerClusterNoteList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getCareerClusterNotes().success(function(data) {

					// success now store in session object
					careerClusterNoteList = data;
					deferred.resolve(careerClusterNoteList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(careerClusterNoteList);
		}

		return deferred.promise;
	}

	return notesService;

} ]);