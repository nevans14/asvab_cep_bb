cepApp.factory('SchoolNotesService', [ '$q', '$window', '$rootScope', 'NotesRestFactory', function($q, $window, $rootScope, NotesRestFactory) {

	var notesService = {};

	var schoolNoteList = undefined;

	/**
	 * Uses notes rest service to insert notes into database. Utilize session
	 * storage. Returns a promise
	 */
	notesService.insertNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.insertSchoolNote(selectedNotesObject).success(function(data) {

			// push newly added occupation to property
			schoolNoteList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('schoolNoteList', JSON.stringify(schoolNoteList));

				// sync with in memory property
				schoolNoteList = angular.fromJson($window.sessionStorage.getItem('schoolNoteList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to update user's note information. Utilize
	 * session storage. Returns a promise.
	 */
	notesService.updateNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.updateSchoolNote(selectedNotesObject).success(function(data) {

			// sync noteList
			var length = schoolNoteList.length;
			for (var i = 0; i < length; i++) {
				if (schoolNoteList[i].noteId == selectedNotesObject.noteId) {
					schoolNoteList[i] = selectedNotesObject;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('schoolNoteList', JSON.stringify(schoolNoteList));

				// sync with in memory property
				schoolNoteList = angular.fromJson($window.sessionStorage.getItem('schoolNoteList'));
			}

			deferred.resolve("Update successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}
	
	notesService.deleteNote = function(noteId) {
		var deferred = $q.defer();		
		NotesRestFactory.deleteSchoolNote(noteId).success(function(data) {
			// sync noteList
			for (var i = 0; i < schoolNoteList.length; i++) {
				if (schoolNoteList[i].noteId === noteId) {
					schoolNoteList.splice(i, 1);
					break;
				}
			}
			// sync with session storage
			if (typeof(Storage) !== undefined) {
				$window.sessionStorage.setItem('schoolNoteList', JSON.stringify(schoolNoteList));
			}
			deferred.resolve(schoolNoteList);
		}).error(function (data, status, headers, config) {
			deferred.reject('Error: request returned status ' + status);
		});		
		return deferred.promise;
	}

	/**
	 * Uses notes rest service to get all user's notes. Utilizes session
	 * storage if user refreshes page. If browser doesn't support session storage, then data will be
	 * obtained by using rest service for each call.
	 */
	notesService.getNotes = function() {
		if ( typeof $rootScope.globals.currentUser === 'undefined') return;
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (schoolNoteList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("schoolNoteList") === null || $window.sessionStorage.getItem("schoolNoteList") == 'undefined') {

					// recover data using database
					NotesRestFactory.getSchoolNotes().success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('schoolNoteList', JSON.stringify(data));

						// sync with in memory property
						schoolNoteList = angular.fromJson($window.sessionStorage.getItem('schoolNoteList'));

						deferred.resolve(schoolNoteList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					schoolNoteList = angular.fromJson($window.sessionStorage.getItem('schoolNoteList'));
					deferred.resolve(schoolNoteList);
				}

			} else {

				// no session storage support so get recover data using database
				NotesRestFactory.getSchoolNotes().success(function(data) {

					// success now store in session object
					schoolNoteList = data;
					deferred.resolve(schoolNoteList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(schoolNoteList);
		}

		return deferred.promise;
	}

	return notesService;

} ]);