cepApp.service('NotesModalService', [ '$uibModal', '$sce', 'UserFactory', 'NotesService', '$rootScope', '$q', 'SchoolNotesService', 'CareerClusterNotesService', '$location', function($uibModal, $sce, UserFactory, NotesService, $rootScope, $q, SchoolNotesService, CareerClusterNotesService, $location) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/notes/page-partials/notes-modal.html'
	};

	var modalOptions = {};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {

			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				// views
				$scope.view = {
					'Occupation': 1,
					'School'	: 2,
					'CC'		: 3
				}
				/**
				 * Toggle view
				 */
				$scope.toggleView = function(view) {
					switch (view) {
						case $scope.view.Occupation:
							$scope.viewSetting.showOccupationNotes = true;
							$scope.viewSetting.showSchoolNotes = $scope.viewSetting.showCCNotes = false;
							break;
						case $scope.view.School:
							$scope.viewSetting.showSchoolNotes = true;
							$scope.viewSetting.showOccupationNotes = $scope.viewSetting.showCCNotes = false;
							break;
						case $scope.view.CC:
							$scope.viewSetting.showCCNotes = true;
							$scope.viewSetting.showSchoolNotes = $scope.viewSetting.showOccupationNotes = false;
							break;
						default:
							$scope.viewSetting.showOccupationNotes = true;
							$scope.viewSetting.showSchoolNotes = $scope.viewSetting.showCCNotes = false;
							break;
					}
				};
				
				$scope.hasAnyNotes = function () {
					return $scope.noteList.length > 0 ||
						   $scope.noteListSchool.length > 0 ||
						   $scope.noteListCC.length > 0;
				}
				
				$scope.setView = function () {
					if ($scope.noteList.length > 0)
						$scope.toggleView($scope.view.Occupation);
					if ($scope.noteListSchool.length > 0)
						$scope.toggleView($scope.view.School);
					if ($scope.noteListCC.length > 0)
						$scope.toggleView($scope.view.CC);
				}
				
				// set the view setting obj
				$scope.viewSetting = {};	
				$scope.noteList = $scope.noteListSchool = $scope.noteListCC = [];
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.tempSocId = undefined;
				$scope.modalOptions.tempUnitId = undefined;
				$scope.modalOptions.tempCcId = undefined;
				
				/**
				 * Initialize setup (occupation notes)
				 */
				$scope.initializeSetup = function () {
					NotesService.getNotes().then(function (data) {
						// set note list to the results
						$scope.noteList = data;
						// if on the dashboard, assign the first SOC as the title/socid
						if ($scope.modalOptions.socId === undefined && $scope.noteList.length > 0) {
							$scope.modalOptions.tempSocId = $scope.noteList[0].socId;
							$scope.modalOptions.tempSocTitle = $scope.noteList[0].title;
						} else if ($scope.noteList.length === 0) {
							$scope.modalOptions.tempSocId = undefined;
							$scope.modalOptions.tempSocTitle = undefined;
						}
						// set default
						$scope.initialNotesObject = $scope.selectedNotesObject = {
							socId	: ($scope.modalOptions.tempSocId ? $scope.modalOptions.tempSocId : $scope.modalOptions.socId),
							title	: ($scope.modalOptions.tempSocTitle ? $scope.modalOptions.tempSocTitle : $scope.modalOptions.title),
							notes	: undefined
						};
						// if list has data, assign the initial/selected notes object (if applicable)
						for (var i = 0; i < $scope.noteList.length; i++) {
							if ($scope.noteList[i].socId === $scope.selectedNotesObject.socId) {
								$scope.initialNotesObject = $scope.selectedNotesObject = {
									userId	: $scope.noteList[i].userId,
									socId	: $scope.noteList[i].socId,
									title	: $scope.noteList[i].title,
									notes	: $scope.noteList[i].notes,
									noteId	: $scope.noteList[i].noteId
								};
								break;
							}
						}						
					})
				}

				/**
				 * Reset to current occupation object.
				 */
				$scope.resetObject = function() {
					$scope.initialNotesObject = $scope.selectedNotesObject = {
						socId	: ($scope.modalOptions.tempSocId ? $scope.modalOptions.tempSocId : $scope.modalOptions.socId),
						title	: ($scope.modalOptions.tempSocTitle ? $scope.modalOptions.tempSocTitle : $scope.modalOptions.title),
						notes	: undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].socId === $scope.selectedNotesObject.socId) {
							$scope.initialNotesObject = $scope.selectedNotesObject = {
								userId	: $scope.noteList[i].userId,
								socId	: $scope.noteList[i].socId,
								title	: $scope.noteList[i].title,
								notes	: $scope.noteList[i].notes,
								noteId	: $scope.noteList[i].noteId
							};
						}
					}
				}

				/**
				 * Save Notes
				 */
				$scope.modalOptions.save = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					$scope.isPerformingAction = true;
					var notesExist = false;

					if ($scope.selectedNotesObject.notes == undefined || $scope.selectedNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						$scope.isPerformingAction = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].socId == $scope.selectedNotesObject.socId) {
							notesExist = true;
						}
					}
					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						NotesService.insertNote($scope.selectedNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {
						NotesService.updateNote($scope.selectedNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};
				
				/**
				 * delete note
				 */
				$scope.modalOptions.deleteNote = function () {
					var isConfirmed = confirm('Do you want to remove this note?');
					if (!isConfirmed) return;
					$scope.error = '';					
					$scope.deletingNote = true;
					$scope.isPerformingAction = true;
					NotesService.deleteNote($scope.selectedNotesObject.noteId).then(function (success) {
						$scope.noteList = success;						
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.initializeSetup();
						// determine which view to see 
						if ($scope.hasAnyNotes()) {
							// if the current note list is empty, select a different view
							if ($scope.noteList.length === 0)
								$scope.setView();
						} else {
							// close modal
							$uibModalInstance.dismiss('cancel');
						}
					}, function (error) {
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.error = 'There was an error processing your request. Please try again.';						
					});
				};

				/**
				 * Within the modal, user can toggle between occupations that
				 * they saved notes for.
				 */
				$scope.selectOccupation = function(noteId) {

					// set the information to display to the user depending on
					// their selection
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].noteId == noteId) {
							$scope.selectedNotesObject = {
								userId	: $scope.noteList[i].userId,
								socId	: $scope.noteList[i].socId,
								title	: $scope.noteList[i].title,
								notes	: $scope.noteList[i].notes,
								noteId	: $scope.noteList[i].noteId
							};
						}
					}
				}
				
				/**
				 * Initialize setup (school notes)
				 */
				$scope.initializeSchoolSetup = function () {
					SchoolNotesService.getNotes().then(function (data) {
						// set school notes from results (db/localstorage)
						$scope.noteListSchool = data;
						// assign default unitId/schoolName if on dashboard
						if ($scope.modalOptions.unitId === undefined && $scope.noteListSchool.length > 0) {
							$scope.modalOptions.tempUnitId = $scope.noteListSchool[0].unitId;
							$scope.modalOptions.tempSchoolName = $scope.noteListSchool[0].schoolName;
						} else if ($scope.noteListSchool.length === 0) {
							$scope.modalOptions.tempUnitId = undefined;
							$scope.modalOptions.tempSchoolName = undefined;
						}
						// initialize initial/selected school notes object
						$scope.initialSchoolNotesObject = $scope.selectedSchoolNotesObject = {
							unitId		: ($scope.modalOptions.tempUnitId ? $scope.modalOptions.tempUnitId : $scope.modalOptions.unitId),
							schoolName	: ($scope.modalOptions.tempSchoolName ? $scope.modalOptions.tempSchoolName : $scope.modalOptions.schoolName),
							notes		: undefined
						};
						// assign initial/selected school notes object, if applicable
						for (var i = 0; i < $scope.noteListSchool.length; i++) {
							if ($scope.noteListSchool[i].unitId === parseInt($scope.selectedSchoolNotesObject.unitId)) {
								$scope.initialSchoolNotesObject = $scope.selectedSchoolNotesObject = {
									userId		: $scope.noteListSchool[i].userId,
									unitId		: $scope.noteListSchool[i].unitId,
									schoolName	: $scope.noteListSchool[i].schoolName,
									notes		: $scope.noteListSchool[i].notes,
									noteId		: $scope.noteListSchool[i].noteId
								}
							}
						}
					})
				}

				/**
				 * Reset to current school object.
				 */
				$scope.resetSchoolObject = function() {
					$scope.initialSchoolNotesObject = $scope.selectedSchoolNotesObject = {
						unitId		: ($scope.modalOptions.tempUnitId ? $scope.modalOptions.tempUnitId : $scope.modalOptions.unitId),
						schoolName	: ($scope.modalOptions.tempSchoolName ? $scope.modalOptions.tempSchoolName : $scope.modalOptions.schoolName),
						notes		: undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteListSchool.length; i++) {
						if ($scope.noteListSchool[i].unitId == $scope.selectedSchoolNotesObject.unitId) {
							$scope.initialSchoolNotesObject = $scope.selectedSchoolNotesObject = {
								unitId		: $scope.noteListSchool[i].unitId,
								schoolName	: $scope.noteListSchool[i].schoolName,
								notes		: $scope.noteListSchool[i].notes,
								noteId		: $scope.noteListSchool[i].noteId
							};
						}
					}
				}

				/**
				 * Save Notes
				 */
				$scope.modalOptions.saveSchool = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					$scope.isPerformingAction = true;
					var notesExist = false;

					if ($scope.selectedSchoolNotesObject.notes == undefined || $scope.selectedSchoolNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						$scope.isPerformingAction = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.noteListSchool.length; i++) {
						if ($scope.noteListSchool[i].unitId == $scope.selectedSchoolNotesObject.unitId) {
							notesExist = true;
						}
					}
					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						SchoolNotesService.insertNote($scope.selectedSchoolNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {
						SchoolNotesService.updateNote($scope.selectedSchoolNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};
				
				$scope.modalOptions.deleteSchoolNote = function () {
					var isConfirmed = confirm('Do you want to remove this note?');
					if (!isConfirmed) return;
					$scope.error = '';					
					$scope.deletingNote = true;
					$scope.isPerformingAction = true;
					SchoolNotesService.deleteNote($scope.selectedSchoolNotesObject.noteId).then(function (success) {
						$scope.noteListSchool = success;						
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.initializeSchoolSetup();
						// determine which view to see 
						if ($scope.hasAnyNotes()) {
							if ($scope.noteListSchool.length === 0)
								$scope.setView();
						} else {
							// close modal
							$uibModalInstance.dismiss('cancel');
						}
					}, function (error) {
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.error = 'There was an error processing your request. Please try again.';						
					});
				};

				/**
				 * Within the modal, user can toggle between schools that
				 * they saved notes for.
				 */
				$scope.selectSchool = function(noteId) {

					// set the information to display to the user depending on
					// their selection
					for (var i = 0; i < $scope.noteListSchool.length; i++) {
						if ($scope.noteListSchool[i].noteId == noteId) {
							$scope.selectedSchoolNotesObject = {
								unitId		: $scope.noteListSchool[i].unitId,
								schoolName	: $scope.noteListSchool[i].schoolName,
								notes		: $scope.noteListSchool[i].notes,
								noteId		: $scope.noteListSchool[i].noteId
							};
						}
					}
				}
				
				/**
				 * Career Clusters Notes
				 */
				
				/**
				 * Initialize setup (career cluster notes)
				 */
				$scope.initializeCCSetup = function () {
					CareerClusterNotesService.getCareerClusterNotes().then(function (data) {
						// set notes for career clusters from result (db/localstorage)
						$scope.noteListCC = data;
						// assign default ccId/title if on dashboard
						if ($scope.modalOptions.ccId === undefined && $scope.noteListCC.length > 0) {
							$scope.modalOptions.tempCcId = $scope.noteListCC[0].ccId;
							$scope.modalOptions.tempCcTitle = $scope.noteListCC[0].title;
						} else if ($scope.noteListCC.length === 0) {
							$scope.modalOptions.tempCcId = undefined;
							$scope.modalOptions.tempCcTitle = undefined;
						}
						// assign initial/selected notes object
						$scope.initialCCNotesObject = $scope.selectedCCNotesObject = {
							ccId	: ($scope.modalOptions.tempCcId ? $scope.modalOptions.tempCcId : $scope.modalOptions.ccId),
							title	: ($scope.modalOptions.tempCcTitle ? $scope.modalOptions.tempCcTitle : $scope.modalOptions.ccTitle),
							notes	: undefined
						};						
						// set notes if saved previously
						for (var i = 0; i < $scope.noteListCC.length; i++) {
							if ($scope.noteListCC[i].ccId === $scope.selectedCCNotesObject.ccId) {
								$scope.initialCCNotesObject = $scope.selectedCCNotesObject = {
									userId	: $scope.noteListCC[i].userId,
									ccId	: $scope.noteListCC[i].ccId,
									title	: $scope.noteListCC[i].title,
									notes	: $scope.noteListCC[i].notes,
									noteId	: $scope.noteListCC[i].noteId
								};
							}
						}
					});
				}
				
				/**
				 * Reset to current occupation object.
				 */
				$scope.resetCCObject = function() {
					$scope.initialCCNotesObject = $scope.selectedCCNotesObject = {
						ccId	: ($scope.modalOptions.tempCcId ? $scope.modalOptions.tempCcId : $scope.modalOptions.ccId),
						title	: ($scope.modalOptions.tempCcTitle ? $scope.modalOptions.tempCcTitle : $scope.modalOptions.ccTitle),
						notes	: undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteListCC.length; i++) {
						if ($scope.noteListCC[i].ccId === $scope.selectedCCNotesObject.ccId) {
							$scope.initialCCNotesObject = $scope.selectedCCNotesObject = {
								userId	: $scope.noteListCC[i].userId,
								ccId	: $scope.noteListCC[i].ccId,
								title	: $scope.noteListCC[i].title,
								notes	: $scope.noteListCC[i].notes,
								noteId	: $scope.noteListCC[i].noteId
							};
						}
					}
				}
				
				/**
				 * Save Notes
				 */
				$scope.modalOptions.saveCC = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					var notesExist = false;
					$scope.isPerformingAction = true;

					if ($scope.selectedCCNotesObject.notes == undefined || $scope.selectedCCNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						$scope.isPerformingAction = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.noteListCC.length; i++) {
						if ($scope.noteListCC[i].ccId == $scope.selectedCCNotesObject.ccId) {
							notesExist = true;
							break;
						}
					}
					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						CareerClusterNotesService.insertCareerClusterNote($scope.selectedCCNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.isPerformingAction = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {
						CareerClusterNotesService.updateCareerClusterNote($scope.selectedCCNotesObject).then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};
				
				$scope.modalOptions.deleteCCNote = function () {
					var isConfirmed = confirm('Do you want to remove this note?');
					if (!isConfirmed) return;
					$scope.error = '';					
					$scope.deletingNote = true;
					$scope.isPerformingAction = true;
					CareerClusterNotesService.deleteNote($scope.selectedCCNotesObject.noteId).then(function (success) {
						$scope.noteListCC = success;						
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.initializeCCSetup();
						// determine which view to see 
						if ($scope.hasAnyNotes()) {
							if ($scope.noteListCC.length === 0)
								$scope.setView();
						} else {
							// close modal
							$uibModalInstance.dismiss('cancel');
						}
					}, function (error) {
						$scope.deletingNote = false;
						$scope.isPerformingAction = false;
						$scope.error = 'There was an error processing your request. Please try again.';						
					});
				};
				
				/**
				 * Within the modal, user can toggle between occupations that
				 * they saved notes for.
				 */
				$scope.selectCC = function(noteId) {

					// set the information to display to the user depending on
					// their selection
					for (var i = 0; i < $scope.noteListCC.length; i++) {
						if ($scope.noteListCC[i].noteId == noteId) {
							$scope.selectedCCNotesObject = {
								ccId	: $scope.noteListCC[i].ccId,
								title	: $scope.noteListCC[i].title,
								notes	: $scope.noteListCC[i].notes,
								noteId	: $scope.noteListCC[i].noteId
							};
						}
					}
				}
				
				/**
				 * Set the initial view setting
				 */
				if ($scope.modalOptions.unitId != undefined) {
					$scope.viewSetting.showSchoolNotes = true;
					$scope.viewSetting.showOccupationNotes = $scope.viewSetting.showCCNotes = false;					
				} else if ($scope.modalOptions.ccId != undefined) { 
					$scope.viewSetting.showCCNotes = true;
					$scope.viewSetting.showOccupationNotes = $scope.viewSetting.showSchoolNotes = false;
				} else {
					$scope.viewSetting.showOccupationNotes = true;
					$scope.viewSetting.showSchoolNotes = $scope.viewSetting.showCCNotes = false;					
				}
				
				$scope.initializeSetup();
				$scope.initializeSchoolSetup();
				$scope.initializeCCSetup();
				
				$scope.modalOptions.close = function() {
					$uibModalInstance.dismiss('cancel');
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};
} ]);