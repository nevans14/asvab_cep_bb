cepApp.controller('CareerClusterNotesController', [ '$scope', 'UserFactory', 'modalService', '$route', 'CareerClusterNotesModalService', function($scope, UserFactory, modalService, $route, CareerClusterNotesModalService) {

	/**
	 * Used on career cluster results page.
	 */
	$scope.showOccufindNotes = function() {
		CareerClusterNotesModalService.show({
			size : 'lg'
		}, {
			title : $scope.careerCluster.ccTitle,
			ccId : $route.current.params.ccId
		});
	}
	
	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotes = function(title, ccId) {
		CareerClusterNotesModalService.show({
			size : 'lg'
		}, {
			title : title,
			ccId : ccId
		});
	}

} ]);
