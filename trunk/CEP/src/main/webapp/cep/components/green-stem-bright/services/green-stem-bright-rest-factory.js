cepApp.factory('GreenStemBrightRestFactory', [ '$http', 'appUrl', 'awsApiFullUrl', function($http, appUrl, awsApiFullUrl) {

	var greenStemBrightRestFactory = {};

	greenStemBrightRestFactory.getBrightOutlook = function(interestCodeOne, interestCodeTwo, interestCodeThree) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/DashboardBrightOutlook/' + interestCodeOne + '/' + interestCodeTwo + '/' + interestCodeThree + '/'} );
	}

	greenStemBrightRestFactory.getStemCareers = function(interestCodeOne, interestCodeTwo, interestCodeThree) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/DashboardStemCareers/' + interestCodeOne + '/' + interestCodeTwo + '/' + interestCodeThree + '/'});
	}
	
	greenStemBrightRestFactory.getGreenCareers = function(interestCodeOne, interestCodeTwo, interestCodeThree) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/DashboardGreenCareers/' + interestCodeOne + '/' + interestCodeTwo + '/' + interestCodeThree + '/'});
	}
	
	greenStemBrightRestFactory.getAllCareers = function() {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/ViewAllOccupations/'});
	}
	
	greenStemBrightRestFactory.getMilitaryCareers = function() {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/ViewMilitaryOccupations/'});
	}

	greenStemBrightRestFactory.getHotCareers = function() {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/ViewHotOccupations/'});
	}
	
	return greenStemBrightRestFactory;

} ]);