cepApp.controller('GreenStemBrightOccupationController', [ '$scope', '$rootScope', '$route', '$cookieStore', '$location', 'careerClusterService', 'AuthenticationService', 'occupationResults', 'OccufindSearchService', 'FavoriteRestFactory', 'favorites', function($scope, $rootScope, $route, $cookieStore, $location, careerClusterService, AuthenticationService, occupationResults, OccufindSearchService, FavoriteRestFactory, favorites) {

	$scope.favorites = undefined;
	$scope.category = $route.current.params.category;
	$scope.occupationResults = occupationResults.data;
	$scope.skillSelected;
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ;		
		$scope.favorites = favorites.data;		
	}
	
	/*$scope.startsWith = function (actual, expected) {
	    var lowerStr = (actual + "").toLowerCase();
	    return lowerStr.indexOf(expected.toLowerCase()) === 0;
	}*/
	if (!String.prototype.startsWith) {
	    String.prototype.startsWith = function(searchString, position){
	      position = position || 0;
	      return this.substr(position, searchString.length) === searchString;
	  };
	}
	
	$scope.hot = function(){
		if ($route.current.params.category =='hot')
			return true;
		else
			return false;
	} 

	$scope.filterAlphabet = function(letter) {
		var masterCopy = angular.copy(occupationResults.data);
		var copy = [];

		for (var i = 0; i < masterCopy.length; i++) {
			if (masterCopy[i].occupationTitle.startsWith(letter)) {
				copy.push(masterCopy[i]);
			}
		}
		$scope.occupationResults = copy;
		$scope.totalItems = $scope.occupationResults.length;
		$scope.currentPage = 1;
		$scope.maxSize = 4;
		$scope.itemsPerPage = 20;
	}


	/**
	 * Pagination
	 */
	$scope.totalItems = $scope.occupationResults.length;
	$scope.currentPage = 1;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 20;
	
	/**
	 * Tracks how many selections are made for skill selection section. Limit user selection to one.
	 */
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.checkChangedLimitOne = function(item) {
		if (item.selected)
			$scope.skillChecked++;
		else
			$scope.skillChecked--;
	}
	
	// for skill selection checkbox 
	$scope.skillSelectionOptions = [ {
		name : 'Verbal',
		value : 'verbalSkill',
		selected : false
	}, {
		name : 'Math',
		value : 'mathSkill',
		selected : false
	}, {
		name : 'Science/Technical',
		value : 'sciTechSkill',
		selected : false
	} ];

	$scope.selectedOccupation = function(onetSoc, title, stem, bright, green, hot, interestCdOne, interestCdTwo, interestCdThree) {
		OccufindSearchService.selectedOnetSoc.push(onetSoc);
		OccufindSearchService.selectedOcupationTitle = title;
		OccufindSearchService.occupationGreen = green;
		OccufindSearchService.occupationStem = stem;
		OccufindSearchService.occupationBright = bright;
		OccufindSearchService.occupationHot = hot;
		OccufindSearchService.occupationInterestOne = interestCdOne;
		OccufindSearchService.occupationInterestTwo = interestCdTwo;
		OccufindSearchService.occupationInterestThree = interestCdThree;
		$location.url('/occufind-occupation-details/' + onetSoc);
	};

	/**
	 * Search feature
	 */
	$scope.stemOccupationFlag;
	$scope.brightOccupationFlag;
	$scope.greenOccupationFlag;
	$scope.hotOccupationFlag;

	$scope.interestCodes = [ {
		name : 'Realistic',
		value : 'R',
		selected : false
	}, {
		name : 'Investigative',
		value : 'I',
		selected : false
	}, {
		name : 'Artistic',
		value : 'A',
		selected : false
	}, {
		name : 'Social',
		value : 'S',
		selected : false
	}, {
		name : 'Enterprising',
		value : 'E',
		selected : false
	}, {
		name : 'Conventional',
		value : 'C',
		selected : false
	}, ];

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 2;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Search button sets up the search service.
	 */
	$scope.occufindSearch = function() {

		OccufindSearchService.userSearch = {
			userSearch : {
				interestCodeOne : undefined,
				interestCodeTwo : undefined,
				brightOccupationFlag : false,
				greenOccupationFlag : false,
				stemOccupationFlag : false,
				hotOccupationFlag : false,
				userSearchString : undefined
			}
		}

		// push skill selection value service object
		var isSkillSelected = false;
		for (var i = 0; i < $scope.skillSelectionOptions.length; i++) {

			if ($scope.skillSelectionOptions[i].selected == true) {
				isSkillSelected = true;
				OccufindSearchService.skillSelected = $scope.skillSelectionOptions[i].value;
			}
		}
		if (!isSkillSelected) {
			OccufindSearchService.skillSelected = undefined;
		}

		// push all interest code selection(s) to array
		var arraySelectedInterestCd = [];
		for (var i = 0; i < $scope.interestCodes.length; i++) {

			if ($scope.interestCodes[i].selected == true) {
				arraySelectedInterestCd.push($scope.interestCodes[i].value);
			}
		}

		// validate selection
		if (arraySelectedInterestCd < 1) {
			/*alert("Please select interest code(s)");
			return false;*/
		} else {

			// assign interest code selection(s) to occufind search service
			OccufindSearchService.userSearch.interestCodeOne = arraySelectedInterestCd[0];
			if (arraySelectedInterestCd.length == 2) {
				OccufindSearchService.userSearch.interestCodeTwo = arraySelectedInterestCd[1];
			}
		}

		// assign user's search selections to occufind search object
		OccufindSearchService.userSearch.brightOccupationFlag = $scope.brightOccupationFlag;
		OccufindSearchService.userSearch.greenOccupationFlag = $scope.greenOccupationFlag;
		OccufindSearchService.userSearch.stemOccupationFlag = $scope.stemOccupationFlag;
		OccufindSearchService.userSearch.userSearchString = $scope.searchString;
		OccufindSearchService.userSearch.hotOccupationFlag = $scope.hotOccupationFlag;

		$rootScope.globals.userSearch = OccufindSearchService.userSearch;
		$cookieStore.put('globals', $rootScope.globals);

		$location.path('/occufind-occupation-search-results');

	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

	$scope.logout = function() {
		AuthenticationService.ClearCredentials();
	};
} ]);

