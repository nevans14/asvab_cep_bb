cepApp.controller('CompareOccupationController', [ '$scope', 'occupationOne', 'occupationTwo', 'occupationThree', '$q', '$window', function($scope, occupationOne, occupationTwo, occupationThree, $q, $window) {

	// first occupation
	$scope.occupationOneTitle = occupationOne[0].data.title;
	$scope.occupationOneInterestCdOne = (occupationOne[2].data.length > 0 ? occupationOne[2].data[0].interestCd : "");
	$scope.occupationOneInterestCdTwo = (occupationOne[2].data.length > 1 ? occupationOne[2].data[1].interestCd : "");
	$scope.occupationOneInterestCdThree = (occupationOne[2].data.length > 2 ? occupationOne[2].data[2].interestCd : "");
	$scope.occupationOneSkillRating = occupationOne[1].data;
	$scope.occupationOneEducation = occupationOne[3].data;
	$scope.occupationOneServiceOffering = occupationOne[4].data;
	$scope.occupationOneNationalSalary = occupationOne[5].data;
	$scope.occupationOneNotes = occupationOne[6].data.note;

	// second occupation
	$scope.occupationTwoTitle = occupationTwo[0].data.title;
	$scope.occupationTwoInterestCdOne = (occupationTwo[2].data.length > 0 ? occupationTwo[2].data[0].interestCd : "");
	$scope.occupationTwoInterestCdTwo = (occupationTwo[2].data.length > 1 ? occupationTwo[2].data[1].interestCd : "");
	$scope.occupationTwoInterestCdThree = (occupationTwo[2].data.length > 2 ? occupationTwo[2].data[2].interestCd : "");
	$scope.occupationTwoSkillRating = occupationTwo[1].data;
	$scope.occupationTwoEducation = occupationTwo[3].data;
	$scope.occupationTwoServiceOffering = occupationTwo[4].data;
	$scope.occupationTwoNationalSalary = occupationTwo[5].data;
	$scope.occupationTwoNotes = occupationTwo[6].data.note;

	// third occupation
	if (occupationThree != undefined) {
		$scope.occupationThreeTitle = occupationThree[0].data.title;
		$scope.occupationThreeInterestCdOne = (occupationThree[2].data.length > 0 ? occupationThree[2].data[0].interestCd : "");
		$scope.occupationThreeInterestCdTwo = (occupationThree[2].data.length > 1 ? occupationThree[2].data[1].interestCd : "");
		$scope.occupationThreeInterestCdThree = (occupationThree[2].data.length > 2 ? occupationThree[2].data[2].interestCd : "");
		$scope.occupationThreeSkillRating = occupationThree[1].data;
		$scope.occupationThreeEducation = occupationThree[3].data;
		$scope.occupationThreeServiceOffering = occupationThree[4].data;
		$scope.occupationThreeNationalSalary = occupationThree[5].data;
		$scope.occupationThreeNotes = occupationThree[6].data.note;
	}

	$scope.occupationThree = occupationThree;

	/**
	 * Skill rating
	 */
	angular.element(document).ready(function() {
		$('.customrating').each(function() {
			var rating = $(this).data('customrating');
			var width = $(this).width() / 5 * rating;
			if (rating.toString().indexOf('.') !== -1) {
				width += 1;
			}
			$(this).width(width);
		});
		
		
		/**
		 * New star rating.
		 */
		$(function() {
		    $( '.ratebox' ).raterater({ 

		    // allow the user to change their mind after they have submitted a rating
		    allowChange: false,

		    // width of the stars in pixels
		    starWidth: 12,

		    // spacing between stars in pixels
		    spaceWidth: 0,

		    numStars: 5,
		    isStatic: true,
		    step: false,
		    });

		});
	});

	/**
	 * Print and download PDF.
	 */
	var div = document.getElementById("printPdf");

	function getCanvas() {
		var defer = $q.defer();
		var t = html2canvas(div, {
			imageTimeout : 2000,
			removeContainer : true,
			background : undefined
		}).then(function(canvas) {
			var img = canvas.toDataURL("image/jpeg");
			defer.resolve(img);
		}, function(error) {
			defer.reject(error);
		});

		return defer.promise;
	}

	$scope.printPdf = function() {
		
		var t = getCanvas();
		
		// IE support
		if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {
			printIE(t);
			return false;
		}

		t.then(function(s) {

			var docDefinition = {
				content : [ {
					text : 'Occupation Comparison\n\n',
					alignment : 'center',
					fontSize : 18,
					bold : true
				}, {
					image : s,
					width : 520
				} ]
			};

			pdfMake.createPdf(docDefinition).print();
		}, function(error) {
			console.log(error);
			alert(error);
		});

	};
	

	/**
	 * IE support for printing element.
	 */
	function printIE(canvas) {
		
		canvas.then(function(img){
			var popup = window.open();
			popup.document.write('<img src=' + img + '>');
			popup.document.close();
			popup.focus();
			popup.print();
			popup.close();
			
		}, function(error){
			
			alert('Printing failed.');
		});
		
	}

	$scope.downloadPdf = function() {
		var t = getCanvas();

		t.then(function(s) {
			var docDefinition = {
				content : [ {
					text : 'Occupation Comparison\n\n',
					alignment : 'center',
					fontSize : 18,
					bold : true
				}, {
					image : s,
					width : 520
				} ]
			};

			pdfMake.createPdf(docDefinition).download('compare.pdf');
		}, function(error) {
			console.log(error);
			alert(error);
		});
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
