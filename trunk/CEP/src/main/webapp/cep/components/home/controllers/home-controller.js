cepApp.controller('HomeController',
	function($route, $rootScope, $scope, $location, $http, $q, $interval,
			AuthenticationService, appUrl, LoginModalService, mediaCenterList, learnMoreModalService, resource, youTubeModalService, currHomepageImage) {
	
	$scope.resources = resource;
	$scope.mediaCenterList = mediaCenterList;
	$scope.currImgName = currHomepageImage.data.imgName;

	$scope.seoTitle = 'What career is right for me? | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Find what career is right for you with the ASVAB CEP career exploration & planning tools. Find career satisfaction & your dream job.';

	/**
	 * Login Modal Service
	 */
	$scope.showLoginModal = function(){
		LoginModalService.show({},{});
	}

	/**
	 * Learn more modal service
	 */
	$scope.showLearnMoreModal = function() {
		learnMoreModalService.show({}, {});
	}

	angular.element(document).ready(function() {

		if (typeof slideShowTimer !== 'undefined') {
			$interval.cancel(slideShowTimer);
		}

		$('#slides .career').hide().eq(0).addClass('active').show();

		slideShowTimer = $interval(function() {
			rotateCareers();
		}, 5000);

		function rotateCareers() {
			var $current = $('#slides .career.active');
			var $next = $current.next();

			if (!$next.length)
				$next = $('#slides .career').eq(0);

			$current.removeClass('active').fadeOut();
			$next.addClass('active').fadeIn();
		}
	});

	$scope.userLogin = true;
	$scope.loginAccessCodeLabel = '';
	var LOGIN_BASE_URL = '/CEP/rest/' + 'applicationAccess/';

	/**
	 *
	 */
	$scope.clearLoginAccessCodeLabel = function() {
		$scope.loginAccessCodeLabel = '';
	};

	$scope.clearLoginEmailLabel = function() {
		$scope.loginEmailLabel = '';
	};

	$scope.clearRegistrationLabel = function() {
		$scope.registrationLabel = '';
	};

	/**
	 * AccessCodeLogin - When Access Code Login button pressed
	 * in Login Modal!
	 *
	 *
	 */
	$scope.AccessCodeLogin = function() {
		// Login Modal
		var data = {
			email : "",
			accessCode : $scope.accessCode,
			password : "",
			resetKey : ""
		};

		var url = LOGIN_BASE_URL + 'loginAccessCode';

		$http.post(url, data).then(function(response) {
			var userData = response.data;

			// If success then
			if (userData.statusNumber === 0) {
				// Set the cookie and global
				// values
				AuthenticationService.SetCredentials(
							userData.user_id,
							userData.role,
							userData.schoolCode,
							userData.schoolName,
							userData.city,
							userData.state,
							userData.zipCode,
							userData.mepsId,
							userData.mepsName)
				// Close Dialog
				$("#LoginClose").click();
				// Set the location to dashboard
				$location.path('/dashboard');
			} else { // Otherwise set error
				// flag
				$scope.loginAccessCodeLabel = "No Access Code Found";
			}
		}, function(response) {
			  var repoData  = response.data || "Request failed";
			  console.log(repoData);
		});
	};
   /**
	* EmailLogin - Login utilizing email and password.
	*
	*
	*
	*/
	$scope.EmailLogin = function() {
		var data = {
			email : $scope.email,
			accessCode : "",
			password : $scope.password,
			resetKey : ""
		};
		$scope.password = '';
		var LOGIN_BASE_URL = 'rest/applicationAccess/';
		var url = LOGIN_BASE_URL + 'loginEmail';
		$http.post(url, data).then(function(response) {
			var userData = response.data;
			if ( userData.statusNumber === 1002 || userData.statusNumber === 1008 ) {
				$scope.loginEmailLabel ='Username / Password combination is invalid';
				return;
			} else if ( userData.statusNumber === 1010 ) {
				$scope.loginEmailLabel ='Username has been locked, try resetting your password.';
				return;
			} else if ( userData.statusNumber !== 0  ){
				var serverErrorMessage = userData.status.substring(userData.status.indexOf(":") + 1 , userData.status.length + 1);
				$scope.loginEmailLabel = serverErrorMessage;
				return;
			}
			AuthenticationService.SetCredentials(
					userData.user_id,
					userData.role,
					userData.schoolCode,
					userData.schoolName,
					userData.city,
					userData.state,
					userData.zipCode,
					userData.mepsId,
					userData.mepsName);
			// Close Dialog
			$("#LoginClose").click();
			// Set the location to dashboard
			$location.path('/dashboard');
		});
	};

	// Closes Login and pops up Registration
	$scope.PopUpRegistration = function() {
		$("#LoginClose").click();
		$("#openRegistrationButton").click();
	};

	$scope.RegisterStudent = function() {
		$scope.registerAccessCode

		// Register Modal
		var data = {
			email : $scope.registerEmail, // document.getElementById("registerEmail").value,
			accessCode : $scope.registerAccessCode, // document.getElementById("registerAccessCode").value,
			password : "",
			resetKey : ""
		};

		var pass1 = $scope.registerPassword1;
		var pass2 = $scope.registerPassword2;

		if (pass1.length <= 7) {
			$scope.registrationLabel = "Password too short!";
			return;
		}

		if (pass1 !== pass2) {
			$scope.registrationLabel = "Passwords don't match!";
			return;
		}

		var url = LOGIN_BASE_URL + 'register';

		data.password = $scope.registerPassword2;
		$http.post(url, data).then(function(response) {
			$scope.registrationLoginStatus = response.data;
			// Success
			if ($scope.registrationLoginStatus.statusNumber === 0) {
				$("#RegistrationClose").click();
				$("#cepLogin").click();
			} else {
				// display error message
				$scope.registrationLabel = $scope.registrationLoginStatus.status
			}
		});
	};

	// Forgot your password
	$scope.RequestResetPassword = function() {
		var data = {
			email : document.getElementById("resetEmail").value, // document.getElementById("registerEmail").value,
			accessCode : "", // document.getElementById("registerAccessCode").value,
			password : "",
			resetKey : ""
		};

		var url = LOGIN_BASE_URL + 'passwordResetRequest';

		$http.post(url, data).then(function(response) {
			$scope.registrationLoginStatus = response.data;
		});
	};

	$scope.ResetPassword = function() {
		data.email = '';
		data.accessCode = '';
		data.password = '';
		data.resetKey = 'aaa';

		var url = LOGIN_BASE_URL + 'passwordReset';

		$http.post(url, data).then(function(response) {
			$scope.registrationLoginStatus = response.data;
		});
	};

	$scope.showYouTubeModal = function(videoName) {
		youTubeModalService.show({
			size : 'lg'
		}, {
			videoName : videoName
		});
	}
});