cepApp.factory('HomeRestFactory', function ($http, appUrl) {
	var factory = {};
	
	factory.getCurrentHomepageImg = function () {
		return $http.get(appUrl + 'home/get-current-homepage-img/');
	}
	
	return factory;
});