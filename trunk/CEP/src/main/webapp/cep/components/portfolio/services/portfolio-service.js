cepApp.factory('PortfolioService', [ '$rootScope', 'PortfolioRestFactory', '$q', '$location', '$window', function($rootScope, PortfolioRestFactory, $q, $location, $window) {

	var portfolioService = {};

	var isPortfolioStarted = undefined;

	portfolioService.setIsPortfolioStarted = function(value) {
		isPortfolioStarted = value;
	};

	portfolioService.getIsPortfolioStarted = function() {

		var deferred = $q.defer();

		// if isPortfolioStarted is undefined then recover data from database
		if (isPortfolioStarted === undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("isPortfolioStarted") === null || $window.sessionStorage.getItem("isPortfolioStarted") == 'undefined') {

					// recover data using database
					PortfolioRestFactory.isPortfolioStarted().success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('isPortfolioStarted', JSON.stringify(data));

						// sync with in memory property
						isPortfolioStarted = angular.fromJson($window.sessionStorage.getItem('isPortfolioStarted'));

						deferred.resolve(isPortfolioStarted);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					isPortfolioStarted = angular.fromJson($window.sessionStorage.getItem('isPortfolioStarted'));
					deferred.resolve(isPortfolioStarted);
				}

			} else {

				// no session storage support so get recover data using database
				PortfolioRestFactory.isPortfolioStarted().success(function(data) {

					// success now store in session object
					isPortfolioStarted = data;
					deferred.resolve(isPortfolioStarted);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(isPortfolioStarted);
		}

		return deferred.promise;
	};

	return portfolioService;
} ]);