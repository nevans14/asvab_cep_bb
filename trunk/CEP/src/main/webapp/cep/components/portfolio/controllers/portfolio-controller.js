cepApp.controller('PortfolioController', [ 
	'$scope', '$rootScope', '$window', '$q', 'PortfolioRestFactory', 'workExperience', 'education', 'achievement', 'interest', 'skill', 'workValue', 'volunteer', 
	'actScore', 'asvabScore', 'fyiScore', 'satScore', 'otherScore', 'UserFactory', 'favoriteList', 'portfolioModal', 'careerClusterFavoriteList', 'plan', 
	'resource', 'modalService', 'UserInfo', 'citmFavoriteOccupations', 'schoolFavoriteList', 'ctmUrl', 'isRegistered',
	function($scope, $rootScope, $window, $q, PortfolioRestFactory, workExperience, education, achievement, interest, skill, workValue, volunteer, 
	actScore, asvabScore, fyiScore, satScore, otherScore, UserFactory, favoriteList, portfolioModal, careerClusterFavoriteList, plan, resource, modalService, UserInfo, citmFavoriteOccupations, schoolFavoriteList, ctmUrl, isRegistered) {

	if(UserInfo.data.length > 0){
		$scope.fullName = UserInfo.data[0].name;
		$scope.grade = UserInfo.data[0].gpa;
	} else {
		$scope.fullName = '';
		$scope.grade = '';
	}
	
	$scope.ssoUrl = ctmUrl;
	$scope.isRegistered = isRegistered.data;
	
	$scope.citmFavoriteOccupations = citmFavoriteOccupations.data;

	/**
	 * Saves name and grade to database
	 */
	$scope.syncData = function() {
		$scope.userInfo = {
			name : $scope.fullName,
			gpa : $scope.grade
		};

		var promise = PortfolioRestFactory.updateInsertUserInfo($scope.userInfo);
		promise.then(function(response) {
		}, function(reason) {
			console.log("userinfo failed to saved");
		}, function(update) {
		});
	};

	$scope.getInterest = function (code) {
		switch (code) {
			case 'R': return 'Realistic';
			case 'I': return 'Investigative';
			case 'A': return 'Artistic';
			case 'S': return 'Social';
			case 'E': return 'Enterprising';
			case 'C': return 'Conventional';
			default:
				return '';
		}
	};
	
	$scope.domainName = resource;

	$scope.favoriteList = favoriteList;
	$scope.careerClusterFavoriteList = careerClusterFavoriteList;
	$scope.schoolFavoriteList = schoolFavoriteList;
	

	/**
	 * User's selection for sections to include in print.
	 */
	$scope.workExperiencePrint = true;
	$scope.educationPrint = true;
	$scope.testScorePrint = true;
	$scope.achievementPrint = true;
	$scope.skillsPrint = true;
	$scope.volunteerPrint = true;
	$scope.interestPrint = true;
	$scope.workValuesPrint = true;
	$scope.favoriteOccupationPrint = true;
	$scope.favoriteCitmOccupationPrint = true;
	$scope.favoriteCareerClusterPrint = true;
	$scope.favoriteSchoolPrint = true;
	$scope.planPrint = true;

	/**
	 * Check all sections to print.
	 */
	$scope.selectAll = function() {
		$scope.workExperiencePrint = true;
		$scope.educationPrint = true;
		$scope.testScorePrint = true;
		$scope.achievementPrint = true;
		$scope.skillsPrint = true;
		$scope.volunteerPrint = true;
		$scope.interestPrint = true;
		$scope.workValuesPrint = true;
		$scope.favoriteOccupationPrint = true;
		$scope.favoriteCitmOccupationPrint = true;
		$scope.favoriteCareerClusterPrint = true;
		$scope.favoriteSchoolPrint = true;
		$scope.planPrint = true;
	};

	/**
	 * Uncheck all sections.
	 */
	$scope.selectNone = function() {
		$scope.workExperiencePrint = false;
		$scope.educationPrint = false;
		$scope.testScorePrint = false;
		$scope.achievementPrint = false;
		$scope.skillsPrint = false;
		$scope.volunteerPrint = false;
		$scope.interestPrint = false;
		$scope.workValuesPrint = false;
		$scope.favoriteOccupationPrint = false;
		$scope.favoriteCitmOccupationPrint = false;
		$scope.favoriteCareerClusterPrint = false;
		$scope.favoriteSchoolPrint = false;
		$scope.planPrint = false;
	};

	/**
	 * Tips modal popup
	 */
	$scope.tipsModal = function() {
		portfolioModal.show({}, {});
	};

	/*
	 * Work experience
	 */

	$scope.showWorkExperienceInputs = false;
	$scope.showUpdateWorkExperienceInputs = false;
	$scope.workExperience = workExperience.data;
	$scope.savingWorkExperience = false;
	$scope.workExperienceEndDateRequired = true;
	$scope.updateWorkExperienceEndDateDisabled = true;
	$scope.workExperiencObject = {
		companyName : undefined,
		jobTitle : undefined,
		startDate : undefined,
		endDate : undefined,
		jobDescription : undefined,
		id : undefined
	};
	$scope.updateWorkExperienceObject;

	/**
	 * Setup for updating work experience input.
	 */
	$scope.setupUpdateWorkExperience = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedWorkEditIndex = index;
		$scope.showWorkExperienceInputs = true;
		$scope.showUpdateWorkExperienceInputs = true;
		var length = $scope.workExperience.length;
		for (var i = 0; i < length; i++) {
			if ($scope.workExperience[i].id === id) {
				$scope.updateWorkExperienceObject = angular.copy($scope.workExperience[i]);
			}
		}
		$scope.updateWorkExperienceEndDateDisabled = $scope.updateWorkExperienceObject.endDate === 'Present';
		console.log($scope.updateWorkExperienceObject);
	};

	/**
	 * Update work experience.
	 */
	$scope.updateWorkExperience = function() {
		$scope.savingWorkExperience = true;
		var promise = PortfolioRestFactory.updateWorkExperience($scope.updateWorkExperienceObject);
		promise.then(function(response) {
			$scope.savingWorkExperience = false;
			$scope.workExperienceEndDateRequired = true;
			$scope.updateWorkExperienceEndDateDisabled = true;
			$scope.showWorkExperienceInputs = false;
			$scope.showUpdateWorkExperienceInputs = false;
			$scope.selectedWorkEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.workExperience.length;
			for (var i = 0; i < length; i++) {
				if ($scope.workExperience[i].id === $scope.updateWorkExperienceObject.id) {
					$scope.workExperience[i] = $scope.updateWorkExperienceObject;
				}
			}
		}, function(reason) {
			$scope.savingWorkExperience = false;
			console.log(reason);
			alert("Failed to update work experience. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert work experience.
	 */
	$scope.insertWorkExperience = function(form) {
		
		if(form.$invalid){
			return false;
		}
		form.$setPristine();
		$scope.savingWorkExperience = true;
		var newObject = angular.copy($scope.workExperiencObject);
		var promise = PortfolioRestFactory.insertWorkExperience(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingWorkExperience = false;
			$scope.workExperienceEndDateRequired = true;
			$scope.showWorkExperienceInputs = false;
			$scope.workExperience.push(response.data);

			// clear form data
			$scope.workExperiencObject = {
				companyName : undefined,
				jobTitle : undefined,
				startDate : undefined,
				endDate : undefined,
				jobDescription : undefined,
				id : undefined
			};
		}, function(reason) {
			$scope.savingWorkExperience = false;
			console.log(reason);
			alert("Failed to save work experience. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete work experience
	 */
	$scope.deleteWorkExperience = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteWorkExperience(id);
		promise.then(function(response) {
			console.log(response);
			// remove work experience locally
			var length = $scope.workExperience.length;
			for (var i = 0; i < length; i++) {
				if ($scope.workExperience[i].id === id) {
					$scope.workExperience.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateWorkExperienceInputs = false;
			$scope.showWorkExperienceInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete work experience. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Education
	 */
	$scope.gpaRegEx = '\^((\d+\.?|\.(?=\d))?\d{0,3})$';
	$scope.showEducationInputs = false;
	$scope.education = education.data;
	$scope.savingEducation = false;
	$scope.educationObject = {
		schoolName : undefined,
		gpa : undefined,
		startDate : undefined,
		endDate : undefined,
		activities : undefined,
		achievements : undefined,
		id : undefined
	};
	$scope.updateWorkExperienceObject;

	/**
	 * Setup for updating education input.
	 */
	$scope.setupUpdateEducation = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedEducationEditIndex = index;
		$scope.showEducationInputs = true;
		$scope.showUpdateEducationInputs = true;
		var length = $scope.education.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.education[i]);
			if ($scope.education[i].id === id) {
				$scope.updateEducationObject = angular.copy($scope.education[i]);
			}
		}
		console.log($scope.updateEducationObject);
	};

	/**
	 * Update education.
	 */
	$scope.updateEducation = function() {
		$scope.savingEducation = true;
		var promise = PortfolioRestFactory.updateEducation($scope.updateEducationObject);
		promise.then(function(response) {
			$scope.savingEducation = false;
			// $scope.workExperienceEndDateRequired = true;
			$scope.showEducationInputs = false;
			$scope.showUpdateEducationInputs = false;
			$scope.selectedEducationEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.education.length;
			for (var i = 0; i < length; i++) {
				if ($scope.education[i].id === $scope.updateEducationObject.id) {
					$scope.education[i] = $scope.updateEducationObject;
				}
			}
		}, function(reason) {
			$scope.savingEducation = false;
			console.log(reason);
			alert("Failed to update education. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert education.
	 */
	$scope.insertEducation = function() {
		$scope.savingEducation = true;
		var newObject = angular.copy($scope.educationObject);
		var promise = PortfolioRestFactory.insertEducation(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingEducation = false;
			$scope.showEducationInputs = false;
			$scope.education.push(response.data);

			// clear form data
			$scope.educationObject = {
				schoolName : undefined,
				gpa : undefined,
				startDate : undefined,
				endDate : undefined,
				activities : undefined,
				achievements : undefined,
				id : undefined
			};

		}, function(reason) {
			$scope.savingEducation = false;
			console.log(reason);
			alert("Failed to save education data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete education
	 */
	$scope.deleteEducation = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteEducation(id);
		promise.then(function(response) {
			console.log(response);
			// remove education locally
			var length = $scope.education.length;
			for (var i = 0; i < length; i++) {
				if ($scope.education[i].id === id) {
					$scope.education.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateEducationInputs = false;
			$scope.showEducationInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete education data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Achievements
	 */
	$scope.showAchievementInputs = false;
	$scope.achievement = achievement.data;
	$scope.savingAchievement = false;
	$scope.achievementObject = {
		description : undefined
	};
	$scope.updateAchievementObject;

	/**
	 * Setup for updating achievement input.
	 */
	$scope.setupUpdateAchievement = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedAchievementEditIndex = index;
		$scope.showAchievementInputs = true;
		$scope.showUpdateAchievementInputs = true;
		var length = $scope.achievement.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.achievement[i]);
			if ($scope.achievement[i].id === id) {
				$scope.updateAchievementObject = angular.copy($scope.achievement[i]);
			}
		}
		console.log($scope.updateAchievementObject);
	};

	/**
	 * Update education.
	 */
	$scope.updateAchievement = function() {
		$scope.savingAchievement = true;
		var promise = PortfolioRestFactory.updateAchievement($scope.updateAchievementObject);
		promise.then(function(response) {
			$scope.savingAchievement = false;
			$scope.showAchievementInputs = false;
			$scope.showUpdateAchievementInputs = false;
			$scope.selectedAchievementEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.achievement.length;
			for (var i = 0; i < length; i++) {
				if ($scope.achievement[i].id === $scope.updateAchievementObject.id) {
					$scope.achievement[i] = $scope.updateAchievementObject;
				}
			}
		}, function(reason) {
			$scope.savingAchievement = false;
			console.log(reason);
			alert("Failed to update achievement. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert achievement.
	 */
	$scope.insertAchievement = function() {
		$scope.savingAchievement = true;
		var newObject = angular.copy($scope.achievementObject);
		var promise = PortfolioRestFactory.insertAchievement(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingAchievement = false;
			$scope.showAchievementInputs = false;
			$scope.achievement.push(response.data);

			// clear form data
			$scope.achievementObject = {
				description : undefined
			};

		}, function(reason) {
			$scope.savingAchievement = false;
			console.log(reason);
			alert("Failed to save achievement data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete achievement
	 */
	$scope.deleteAchievement = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteAchievement(id);
		promise.then(function(response) {
			console.log(response);
			// remove achievement locally
			var length = $scope.achievement.length;
			for (var i = 0; i < length; i++) {
				if ($scope.achievement[i].id === id) {
					$scope.achievement.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateAchievementInputs = false;
			$scope.showAchievementInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete achievement data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Interests.
	 */
	$scope.interestSelected = 'interest';
	$scope.showInterestInputs = false;
	$scope.interest = interest.data;
	$scope.savingInterest = false;
	$scope.interestObject = {
		description : undefined
	};
	$scope.updateInterestObject;

	/**
	 * Setup for updating interest input.
	 */
	$scope.setupInterestSkill = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedInterestEditIndex = index;
		$scope.showInterestInputs = true;
		$scope.showUpdateInterestInputs = true;
		var length = $scope.interest.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.interest[i]);
			if ($scope.interest[i].id === id) {
				$scope.updateInterestObject = angular.copy($scope.interest[i]);
			}
		}
		console.log($scope.updateInterestObject);
	}

	/**
	 * Update interest.
	 */
	$scope.updateInterest = function() {
		$scope.savingInterest = true;
		var promise = PortfolioRestFactory.updateInterest($scope.updateInterestObject);
		promise.then(function(response) {
			$scope.savingInterest = false;
			$scope.showInterestInputs = false;
			$scope.showUpdateInterestInputs = false;
			$scope.selectedInterestEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.interest.length;
			for (var i = 0; i < length; i++) {
				if ($scope.interest[i].id === $scope.updateInterestObject.id) {
					$scope.interest[i] = $scope.updateInterestObject;
				}
			}
		}, function(reason) {
			$scope.savingInterest = false;
			console.log(reason);
			alert("Failed to update interest. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert interest.
	 */
	$scope.insertInterest = function() {
		$scope.savingInterest = true;
		var newObject = angular.copy($scope.interestObject);
		var promise = PortfolioRestFactory.insertInterest(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingInterest = false;
			$scope.showInterestInputs = false;
			$scope.interest.push(response.data);

			// clear form data
			$scope.interestObject = {
				description : undefined
			};

		}, function(reason) {
			$scope.savingInterest = false;
			console.log(reason);
			alert("Failed to save interest data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete interest.
	 */
	$scope.deleteInterest = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteInterest(id);
		promise.then(function(response) {
			console.log(response);
			// remove interest locally
			var length = $scope.interest.length;
			for (var i = 0; i < length; i++) {
				if ($scope.interest[i].id === id) {
					$scope.interest.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateInterestInputs = false;
			$scope.showInterestInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete interest data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Skills.
	 */
	$scope.showSkillInputs = false;
	$scope.skill = skill.data;
	$scope.savingSkill = false;
	$scope.skillObject = {
		description : undefined
	};
	$scope.updateSkillObject;

	/**
	 * Setup for updating skill input.
	 */
	$scope.setupUpdateSkill = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedSkillsEditIndex = index;
		$scope.showSkillInputs = true;
		$scope.showUpdateSkillInputs = true;
		var length = $scope.skill.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.skill[i]);
			if ($scope.skill[i].id === id) {
				$scope.updateSkillObject = angular.copy($scope.skill[i]);
			}
		}
		console.log($scope.updateSkillObject);
	};

	/**
	 * Update skill.
	 */
	$scope.updateSkill = function() {
		$scope.savingSkill = true;
		var promise = PortfolioRestFactory.updateSkill($scope.updateSkillObject);
		promise.then(function(response) {
			$scope.savingSkill = false;
			$scope.showSkillInputs = false;
			$scope.showUpdateSkillInputs = false;
			$scope.selectedSkillsEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.skill.length;
			for (var i = 0; i < length; i++) {
				if ($scope.skill[i].id === $scope.updateSkillObject.id) {
					$scope.skill[i] = $scope.updateSkillObject;
				}
			}
		}, function(reason) {
			$scope.savingSkill = false;
			console.log(reason);
			alert("Failed to update skill. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert skills.
	 */
	$scope.insertSkill = function() {
		$scope.savingSkill = true;
		var newObject = angular.copy($scope.skillObject);
		var promise = PortfolioRestFactory.insertSkill(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingSkill = false;
			$scope.showSkillInputs = false;
			$scope.skill.push(response.data);

			// clear form data
			$scope.skillObject = {
				description : undefined
			};

		}, function(reason) {
			$scope.savingSkill = false;
			console.log(reason);
			alert("Failed to save skill data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete skill.
	 */
	$scope.deleteSkill = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteSkill(id);
		promise.then(function(response) {
			console.log(response);
			// remove skill locally
			var length = $scope.skill.length;
			for (var i = 0; i < length; i++) {
				if ($scope.skill[i].id === id) {
					$scope.skill.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateSkillInputs = false;
			$scope.showSkillInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete skill data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Work Values.
	 */
	$scope.showWorkValueInputs = false;
	$scope.workValue = workValue.data;
	$scope.savingWorkValue = false;
	$scope.workValueObject = {
		description : undefined
	};
	$scope.updateWorkValueObject;

	/**
	 * Setup for updating work value input.
	 */
	$scope.setupUpdateWorkValue = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedValuesEditIndex = index;
		$scope.showWorkValueInputs = true;
		$scope.showUpdateWorkValueInputs = true;
		var length = $scope.workValue.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.workValue[i]);
			if ($scope.workValue[i].id === id) {
				$scope.updateWorkValueObject = angular.copy($scope.workValue[i]);
			}
		}
		console.log($scope.updateWorkValueObject);
	};

	/**
	 * Update work value.
	 */
	$scope.updateWorkValue = function() {
		$scope.savingWorkValue = true;
		var promise = PortfolioRestFactory.updateWorkValue($scope.updateWorkValueObject);
		promise.then(function(response) {
			$scope.savingWorkValue = false;
			$scope.showWorkValueInputs = false;
			$scope.showUpdateWorkValueInputs = false;
			$scope.selectedValuesEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.workValue.length;
			for (var i = 0; i < length; i++) {
				if ($scope.workValue[i].id === $scope.updateWorkValueObject.id) {
					$scope.workValue[i] = $scope.updateWorkValueObject;
				}
			}
		}, function(reason) {
			$scope.savingWorkValue = false;
			console.log(reason);
			alert("Failed to update work value. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert work value.
	 */
	$scope.insertWorkValue = function() {
		$scope.savingWorkValue = true;
		var newObject = angular.copy($scope.workValueObject);
		var promise = PortfolioRestFactory.insertWorkValue(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingWorkValue = false;
			$scope.showWorkValueInputs = false;
			$scope.workValue.push(response.data);

			// clear form data
			$scope.workValueObject = {
				description : undefined
			};

		}, function(reason) {
			$scope.savingWorkValue = false;
			console.log(reason);
			alert("Failed to save work value data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete work value.
	 */
	$scope.deleteWorkValue = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteWorkValue(id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.workValue.length;
			for (var i = 0; i < length; i++) {
				if ($scope.workValue[i].id === id) {
					$scope.workValue.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateWorkValueInputs = false;
			$scope.showWorkValueInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete work value data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Volunteer
	 */
	$scope.showVolunteerInputs = false;
	$scope.volunteer = volunteer.data;
	$scope.savingVolunteer = false;
	$scope.volunteerEndDateDisabled = true;
	$scope.updateVolunteerEndDateDisabled = true;
	$scope.volunteerObject = {
		organizationName : undefined,
		startDate : undefined,
		endDate : undefined,
		description : undefined
	};
	$scope.updateVolunteerObject;

	/**
	 * Setup for updating volunteer input.
	 */
	$scope.setupUpdateVolunteer = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedVolunteerEditIndex = index;
		$scope.showVolunteerInputs = true;
		$scope.showUpdateVolunteerInputs = true;
		var length = $scope.volunteer.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.volunteer[i]);
			if ($scope.volunteer[i].id === id) {
				$scope.updateVolunteerObject = angular.copy($scope.volunteer[i]);
			}
		}
		$scope.updateVolunteerEndDateDisabled = $scope.updateVolunteerObject.endDate === 'Present';
		console.log($scope.updateVolunteerObject);
	};

	/**
	 * Update volunteer.
	 */
	$scope.updateVolunteer = function() {
		$scope.savingVolunteer = true;
		var promise = PortfolioRestFactory.updateVolunteer($scope.updateVolunteerObject);
		promise.then(function(response) {
			$scope.savingVolunteer = false;
			$scope.showVolunteerInputs = false;
			$scope.showUpdateVolunteerInputs = false;
			$scope.updateVolunteerEndDateDisabled = true;
			$scope.selectedVolunteerEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.volunteer.length;
			for (var i = 0; i < length; i++) {
				if ($scope.volunteer[i].id === $scope.updateVolunteerObject.id) {
					$scope.volunteer[i] = $scope.updateVolunteerObject;
				}
			}
		}, function(reason) {
			$scope.savingVolunteer = false;
			console.log(reason);
			alert("Failed to update volunteer. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert volunteer.
	 */
	$scope.insertVolunteer = function() {
		$scope.savingVolunteer = true;
		var newObject = angular.copy($scope.volunteerObject);
		var promise = PortfolioRestFactory.insertVolunteer(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingVolunteer = false;
			$scope.showVolunteerInputs = false;
			$scope.volunteerEndDateDisabled = true;
			$scope.volunteer.push(response.data);

			// clear form data
			$scope.volunteerObject = {
				organizationName : undefined,
				startDate : undefined,
				endDate : undefined,
				description : undefined
			};

		}, function(reason) {
			$scope.savingVolunteer = false;
			console.log(reason);
			alert("Failed to save volunteer work. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete volunteer.
	 */
	$scope.deleteVolunteer = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteVolunteer(id);
		promise.then(function(response) {
			console.log(response);
			// remove volunteer locally
			var length = $scope.volunteer.length;
			console.log($scope.volunteer);
			for (var i = 0; i < length; i++) {
				if ($scope.volunteer[i].id === id) {
					$scope.volunteer.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateVolunteerInputs = false;
			$scope.showVolunteerInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete volunteer data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Test scores.
	 */
	$scope.showScoreInputs = false; // used for all scores
	$scope.testSelected = actScore.length === 0 ? 'ACT' : asvabScore.length === 0 ? 'ASVAB' : satScore.length === 0 ? 'SAT' : 'OTHER'; // ACT
	// default
	// selection
	$scope.testEditDisable = false;

	// reset so only one section is open at a time.
	function resetScoreEditFlags() {
		$scope.showUpdateFyiScoreInputs = false;
		$scope.showUpdateAsvabScoreInputs = false;
		$scope.showUpdateActScoreInputs = false;
		$scope.showUpdateSatScoreInputs = false;
		$scope.showUpdateOtherScoreInputs = false;
	}

	/**
	 * ACT
	 */
	$scope.actScore = actScore.data;
	$scope.savingActScore = false;
	$scope.actObject = {
		readingScore : undefined,
		mathScore : undefined,
		writingScore : undefined,
		englishScore : undefined,
		scienceScore : undefined
	};
	$scope.updateActObject;

	/**
	 * Setup for updating ACT input.
	 */
	$scope.setupUpdateActScore = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedACTEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateActScoreInputs = true;
		var length = $scope.actScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.actScore[i]);
			if ($scope.actScore[i].id === id) {
				$scope.updateActObject = angular.copy($scope.actScore[i]);
				break;
			}
		}
		console.log($scope.updateActObject);
	};

	/**
	 * Update ACT.
	 */
	$scope.updateActScore = function() {
		$scope.savingActScore = true;
		var promise = PortfolioRestFactory.updateActScore($scope.updateActObject);
		promise.then(function(response) {
			$scope.savingActScore = false;
			$scope.showScoreInputs = false;
			$scope.showUpdateActScoreInputs = false;
			$scope.selectedACTEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.actScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.actScore[i].id === $scope.updateActObject.id) {
					$scope.actScore[i] = $scope.updateActObject;
					break;
				}
			}
		}, function(reason) {
			$scope.savingActScore = false;
			console.log(reason);
			alert("Failed to update ACT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert ACT score.
	 */
	$scope.insertActScore = function() {
		$scope.savingActScore = true;
		var newObject = angular.copy($scope.actObject);
		var promise = PortfolioRestFactory.insertActScore(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingActScore = false;
			$scope.showScoreInputs = false;
			$scope.actScore.push(response.data);

			// clear form data
			$scope.actObject = {
				readingScore : undefined,
				mathScore : undefined,
				writingScore : undefined,
				englishScore : undefined,
				scienceScore : undefined
			};

		}, function(reason) {
			$scope.savingActScore = false;
			console.log(reason);
			alert("Failed to save ACT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete ACT score.
	 */
	$scope.deleteActScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteActScore(id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.actScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.actScore[i].id === id) {
					$scope.actScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete ACT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * ASVAB
	 */
	$scope.asvabScore = asvabScore.data;
	$scope.haveAsvabTestScores = false;
	$scope.asvabTestScores = {
		verbalScore: undefined,
		mathScore: undefined,
		scienceScore: undefined,
		afqtScore: undefined
	};
	
	// Retrieve scores from session
	if ($window.sessionStorage.getItem('sV') && $window.sessionStorage.getItem('manualScores') === "false") {
		$scope.haveAsvabTestScores = true;
		$scope.asvabTestScores = {
			verbalScore: $window.sessionStorage.getItem('sV'),
			mathScore: $window.sessionStorage.getItem('sM'),
			scienceScore: $window.sessionStorage.getItem('sS'),
			afqtScore: $scope.afqtScore = $window.sessionStorage.getItem('sA')
		};
		// if the asvab score list has no scores, then add it from session
		if ($scope.asvabScore.length === 0) {
			$scope.asvabScore.push($scope.asvabTestScores);	
		}		
	}
	
	$scope.savingAsvabScore = false;
	$scope.asvabObject = {
		verbalScore : undefined,
		mathScore : undefined,
		afqtScore : undefined,
		scienceScore : undefined
	};
	$scope.updateAsvabObject;

	/**
	 * Setup for updating ASVAB input.
	 */
	$scope.setupUpdateAsvabScore = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedASVABEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateAsvabScoreInputs = true;
		var length = $scope.asvabScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.asvabScore[i]);
			if ($scope.asvabScore[i].id === id) {
				$scope.updateAsvabObject = angular.copy($scope.asvabScore[i]);
				break;
			}
		}
		//console.log($scope.updateAsvabObject);
	};

	/**
	 * Update ASVAB.
	 */
	$scope.updateAsvabScore = function() {
		$scope.savingAsvabScore = true;
		$window.sessionStorage.setItem('manualScores', true);
		$window.sessionStorage.setItem('sV', $scope.asvabObject.verbalScore);
		$window.sessionStorage.setItem('sM', $scope.asvabObject.mathScore);
		$window.sessionStorage.setItem('sS', $scope.asvabObject.scienceScore);
		$window.sessionStorage.setItem('sA', $scope.asvabObject.afqtScore);

		var promise = PortfolioRestFactory.updateAsvabScore($scope.updateAsvabObject);
		promise.then(function(response) {
			$scope.savingAsvabScore = false;
			$scope.showScoreInputs = false;
			$scope.showUpdateAsvabScoreInputs = false;
			$scope.selectedASVABEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.asvabScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.asvabScore[i].id === $scope.updateAsvabObject.id) {
					$scope.asvabScore[i] = $scope.updateAsvabObject;
					break;
				}
			}

		}, function(reason) {
			$scope.savingAsvabScore = false;
			console.log(reason);
			alert("Failed to update ASVAB score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert ASVAB score.
	 */
	$scope.insertAsvabScore = function() {
		$scope.savingAsvabScore = true;
		$window.sessionStorage.setItem('manualScores', true);
		$window.sessionStorage.setItem('sV', $scope.asvabObject.verbalScore);
		$window.sessionStorage.setItem('sM', $scope.asvabObject.mathScore);
		$window.sessionStorage.setItem('sS', $scope.asvabObject.scienceScore);
		$window.sessionStorage.setItem('sA', $scope.asvabObject.afqtScore);

		var newObject = angular.copy($scope.asvabObject);
		var promise = PortfolioRestFactory.insertAsvabScore(newObject);
		promise.then(function(response) {
			//console.log(response);
			$scope.savingAsvabScore = false;
			$scope.showScoreInputs = false;
			$scope.asvabScore.push(response.data);

			// clear form data
			$scope.asvabObject = {
				verbalScore : undefined,
				mathScore : undefined,
				afqtScore : undefined,
				scienceScore : undefined
			};
		}, function(reason) {
			$scope.savingAsvabScore = false;
			console.log(reason);
			alert("Failed to save ASVAB score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete ASVAB score.
	 */
	$scope.deleteAsvabScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteAsvabScore(id);
		promise.then(function(response) {
			// remove scores from webstorage
			$window.sessionStorage.removeItem('sV');
			$window.sessionStorage.removeItem('sM');
			$window.sessionStorage.removeItem('sS');
			$window.sessionStorage.removeItem('sA');
			// remove books
			$window.sessionStorage.removeItem('vBooks');
			$window.sessionStorage.removeItem('mBooks');
			$window.sessionStorage.removeItem('sBooks');
			
			$window.sessionStorage.removeItem('manualScores', false);
			
			//console.log(response);
			// remove work value locally
			var length = $scope.asvabScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.asvabScore[i].id === id) {
					$scope.asvabScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete ASVAB score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * SAT
	 */
	$scope.satScore = satScore.data;
	$scope.savingSatScore = false;
	$scope.satObject = {
		readingScore : undefined,
		mathScore : undefined,
		writingScore : undefined,
		subject : undefined
	};
	$scope.updateSatObject;

	/**
	 * Setup for updating SAT input.
	 */
	$scope.setupUpdateSatScore = function(id, index) {
		console.log('setupUpdateSatScore');
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedSATEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateSatScoreInputs = true;
		var length = $scope.satScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.satScore[i]);
			if ($scope.satScore[i].id === id) {
				$scope.updateSatObject = angular.copy($scope.satScore[i]);
				break;
			}
		}
		console.log($scope.updateSatObject);
	};

	/**
	 * Update SAT.
	 */
	$scope.updateSatScore = function() {
		console.log('updateSatScore');
		$scope.savingSatScore = true;
		var promise = PortfolioRestFactory.updateSatScore($scope.updateSatObject);
		promise.then(function(response) {
			$scope.savingSatScore = false;
			$scope.showSatInputs = false;
			$scope.showUpdateSatScoreInputs = false;
			$scope.selectedSATEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.satScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.satScore[i].id === $scope.updateSatObject.id) {
					$scope.satScore[i] = $scope.updateSatObject;
					break;
				}
			}
		}, function(reason) {
			$scope.savingSatScore = false;
			console.log(reason);
			alert("Failed to update SAT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert SAT score.
	 */
	$scope.insertSatScore = function() {
		console.log('insertSatScore');
		$scope.savingSatScore = true;
		var newObject = angular.copy($scope.satObject);
		var promise = PortfolioRestFactory.insertSatScore(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingSatScore = false;
			$scope.showScoreInputs = false;
			$scope.satScore.push(response.data);

			// clear form data
			$scope.satObject = {
				readingScore : undefined,
				mathScore : undefined,
				writingScore : undefined,
				subject : undefined
			};

		}, function(reason) {
			$scope.savingSatScore = false;
			console.log(reason);
			alert("Failed to save SAT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete SAT score.
	 */
	$scope.deleteSatScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteSatScore(id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.satScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.satScore[i].id === id) {
					$scope.satScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete SAT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * FYI. If user has FYI scores then use it as default.
	 */

	// Use FYI test results if exists.
	UserFactory.getUserInterestCodes().then(function(response) {
		$scope.scoreChoice = response.scoreChoice;
		
		if (response.length === 0) {
			$scope.hideFyi = false;
			$scope.fyiScore = fyiScore.data;
		} else {
			$scope.hideFyi = true; // hide fyi inputs
			var array = [];
			array.push(response);
			$scope.fyiScore = array;
		}

	}, function(reason) {
		$scope.hideFyi = false;
		if (reason.length === 0) {
			$scope.fyiScore = fyiScore.data;
		} else {
			alert("Error fetching FYI test result." + reason);
		}
	});

	
	$scope.interestCodesInfo = function() {
		console.log('Here I am');
		var score = $scope.scoreChoice === 'gender' ? 'Gender-Specific' : 'Combined';
		var modalOptions = {
			headerText : 'Interest Codes',
			bodyText : "You're currently using your <strong>" + score + "</strong> scores for career exploration. You have two sets of FYI results, Gender-Specific and Combined. To change the set you're using for career exploration and to learn more about FYI results, go to Return to FYI Results under Step 1."
		};

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	};
	$scope.savingFyiScore = false;
	$scope.fyiObject = {
		interest_code_one : undefined,
		interest_code_two : undefined,
		interest_code_three : undefined
	};
	$scope.updateFyiObject;

	/**
	 * Setup for updating FYI input.
	 */
	$scope.setupUpdateFyiScore = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedFYIEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateFyiScoreInputs = true;
		var length = $scope.fyiScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.fyiScore[i]);
			if ($scope.fyiScore[i].id === id) {
				$scope.updateFyiObject = angular.copy($scope.fyiScore[i]);
				break;
			}
		}
		console.log($scope.updateFyiObject);
	};

	/**
	 * Update FYI.
	 */
	$scope.updateFyiScore = function() {
		$scope.savingFyiScore = true;
		var promise = PortfolioRestFactory.updateFyiScore($scope.updateFyiObject);
		promise.then(function(response) {
			$scope.savingFyiScore = false;
			$scope.showFyiInputs = false;
			$scope.showUpdateFyiScoreInputs = false;
			$scope.selectedFYIEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.fyiScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.fyiScore[i].id === $scope.updateFyiObject.id) {
					$scope.fyiScore[i] = $scope.updateFyiObject;
					break;
				}
			}
		}, function(reason) {
			$scope.savingFyiScore = false;
			console.log(reason);
			alert("Failed to update FYI score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert FYI score.
	 */
	$scope.insertFyiScore = function() {
		$scope.savingFyiScore = true;
		var newObject = angular.copy($scope.fyiObject);
		var promise = PortfolioRestFactory.insertFyiScore(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingFyiScore = false;
			$scope.showInterestInputs = false;
			$scope.interestSelected = 'interest';
			$scope.fyiScore.push(response.data);

			// clear form data
			$scope.fyiObject = {
				interest_code_one : undefined,
				interest_code_two : undefined,
				interest_code_three : undefined
			}

		}, function(reason) {
			$scope.savingFyiScore = false;
			console.log(reason);
			alert("Failed to save FYI score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete FYI score.
	 */
	$scope.deleteFyiScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteFyiScore(id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.fyiScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.fyiScore[i].id === id) {
					$scope.fyiScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete FYI score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Other test scores.
	 */
	$scope.otherScore = otherScore.data;
	$scope.savingOtherScore = false;
	$scope.otherObject = {
		description : undefined
	};
	$scope.updateOtherObject;

	/**
	 * Setup for updating other input.
	 */
	$scope.setupUpdateOtherScore = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedOtherEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateOtherScoreInputs = true;
		var length = $scope.otherScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.otherScore[i]);
			if ($scope.otherScore[i].id === id) {
				$scope.updateOtherObject = angular.copy($scope.otherScore[i]);
				break;
			}
		}
		console.log($scope.updateOtherObject);
	};

	/**
	 * Update other scores.
	 */
	$scope.updateOtherScore = function() {
		$scope.savingOtherScore = true;
		var promise = PortfolioRestFactory.updateOtherScore($scope.updateOtherObject);
		promise.then(function(response) {
			$scope.savingOtherScore = false;
			$scope.showOtherInputs = false;
			$scope.showUpdateOtherScoreInputs = false;
			$scope.selectedOtherEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.otherScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.otherScore[i].id === $scope.updateOtherObject.id) {
					$scope.otherScore[i] = $scope.updateOtherObject;
					break;
				}
			}
		}, function(reason) {
			$scope.savingOtherScore = false;
			console.log(reason);
			alert("Failed to update other score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert other scores.
	 */
	$scope.insertOtherScore = function() {
		$scope.savingOtherScore = true;
		var newObject = angular.copy($scope.otherObject);
		var promise = PortfolioRestFactory.insertOtherScore(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingOtherScore = false;
			$scope.showScoreInputs = false;
			$scope.otherScore.push(response.data);

			// clear form data
			$scope.otherObject = {
				description : undefined
			};

		}, function(reason) {
			$scope.savingOtherScore = false;
			console.log(reason);
			alert("Failed to save score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete other score.
	 */
	$scope.deleteOtherScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteOtherScore(id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.otherScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.otherScore[i].id === id) {
					$scope.otherScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete other score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Plan of action implementation
	 */
	$scope.showPlanInputs = false;
	$scope.plan = plan.data;
	console.log($scope.plan);
	$scope.savingPlan = false;
	$scope.planObject = {
		plan : undefined,
		description : undefined
	};
	$scope.updatePlanObject;
	$scope.combinedPlanObj = {
		plan: {
			fourYearCollege: false,
			twoYearCollege: false,
			careerAndTech: false,
			military: false,
			work: false,
			gapYear: false,
			undecided: false,
		},
		description: "",
	};
	$scope.originalPlanObj = {};

	$scope.isJson = function(str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	}

	$scope.isCombinedPlanFormat = $scope.plan.length > 0 && $scope.isJson($scope.plan[0].plan);

	/**
	 * Setup for updating plan of interest input.
	 */
	$scope.setupPlanSkill = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedPlanEditIndex = index;
		$scope.showPlanInputs = true;
		$scope.showUpdatePlanInputs = true;
		var length = $scope.plan.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.plan[i]);
			if ($scope.plan[i].id === id) {
				$scope.updatePlanObject = angular.copy($scope.plan[i]);
			}
		}
		console.log($scope.updatePlanObject);
	};

	/**
	 * Update plan of action.
	 */
	$scope.updatePlan = function() {
		$scope.savingPlan = true;
		var promise = PortfolioRestFactory.updatePlan($scope.updatePlanObject);
		promise.then(function(response) {
			$scope.savingPlan = false;
			$scope.showPlanInputs = false;
			$scope.showUpdatePlanInputs = false;
			$scope.selectedPlanEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.plan.length;
			for (var i = 0; i < length; i++) {
				if ($scope.plan[i].id === $scope.updatePlanObject.id) {
					$scope.plan[i] = $scope.updatePlanObject;
				}
			}
		}, function(reason) {
			$scope.savingPlan = false;
			console.log(reason);
			alert("Failed to update plan of action. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Insert plan of action.
	 */
	$scope.insertPlan = function() {
		$scope.savingPlan = true;
		var newObject = angular.copy($scope.planObject);
		var promise = PortfolioRestFactory.insertPlan(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingPlan = false;
			$scope.showPlanInputs = false;
			$scope.plan.push(response.data);

			// clear form data
			$scope.planObject = {
				plan : undefined,
				description : undefined
			};

		}, function(reason) {
			$scope.savingPlan = false;
			console.log(reason);
			alert("Failed to save plan of action data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Delete plan of action.
	 */
	$scope.deletePlan = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deletePlan(id);
		promise.then(function(response) {
			console.log(response);
			// remove plan locally
			var length = $scope.plan.length;
			for (var i = 0; i < length; i++) {
				if ($scope.plan[i].id === id) {
					$scope.plan.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdatePlanInputs = false;
			$scope.showPlanInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete plan of action data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/**
	 * Print page as pdf. Utilizes pdfmake library.
	 */
	var docDefinition, len;

	/*
	 * function toDataUrl(url, callback, outputFormat){ var img = new Image();
	 * img.crossOrigin = 'Anonymous'; img.onload = function(){ var canvas =
	 * document.createElement('CANVAS'); var ctx = canvas.getContext('2d'); var
	 * dataURL; canvas.height = this.height; canvas.width = this.width;
	 * ctx.drawImage(this, 0, 0); dataURL = canvas.toDataURL(outputFormat);
	 * callback(dataURL); canvas = null; }; img.src = url; }
	 * 
	 * toDataUrl('images/logo.png', function(base64Img){ logoImg = base64Img;
	 * console.log(logoImg); });
	 */

	 function planNameLookup(varName) {
		 switch(varName) {
			case 'fourYearCollege':
				return '4 Year College';
			case 'twoYearCollege':
				return '2 Year College';
			case 'careerAndTech':
				return 'Career and Technical Education';
			case 'military':
				return 'Military';
			case 'work':
				return 'Work';
			case 'gapYear':
				return 'Gap Year';
			case 'undecided':
				return 'Undecided';
		 }
	 }

	/**
	 * Setup PDF and print content.
	 */
	function setupContent() {

		var content = [];

		// User input for name and grade.
		var nameAndGrade = [ {
			columns : [ {
				text : 'Name: ' + $scope.fullName,
				alignment : 'left',
				fontSize : 18,
				bold : true
			}, {
				text : 'Grade: ' + $scope.grade,
				alignment : 'right',
				fontSize : 18,
				bold : true
			} ]
		}, {
			canvas : [ {
				type : 'line',
				x1 : 0,
				y1 : 5,
				x2 : 595 - 2 * 40,
				y2 : 5,
				lineWidth : 1
			} ],
			style : 'horizontalLine'
		} ];
		content.push(nameAndGrade);

		// Interests content.
		if ($scope.planPrint && $scope.combinedPlanObj.plan) {
			// section title
			content.push({
				text : 'Future Plan:',
				style : 'sectionTitle'
			});
			for (var property in $scope.combinedPlanObj.plan) {
				if ($scope.combinedPlanObj.plan[property]) {
					content.push([ {
						text : "\n" + planNameLookup(property)
					}])
				}
			}
			if ($scope.combinedPlanObj.description) {
				content.push([ {
					text : "\n" + $scope.combinedPlanObj.description
				}])
			}
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Work experience content.
		if ($scope.workExperiencePrint && $scope.workExperience.length > 0) {
			// section title
			content.push({
				text : 'Work Experience:\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.workExperience, function (workExp) {
				content.push([{
					text: [{
						text: '\n' + workExp.jobTitle + ', ',
						bold: true
					}, workExp.companyName]
				}, {
					text: workExp.startDate + ' - ' + workExp.endDate
				}]);
				//convert html for pdfMake to injest
				content.push(htmlToPdfmake(workExp.jobDescription));
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Education content.
		if ($scope.educationPrint && $scope.education.length > 0) {
			// section title
			content.push({
				text : 'Education:\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.education, function (edu) {
				content.push([{
					text: '\n' + edu.schoolName + ',',
					bold: true
				}, {
					text: edu.startDate + ' - ' + edu.endDate + (edu.gpa > 0 ? ', GPA: ' + edu.gpa.toFixed(1) : '')
				}, {
					text: (edu.activities) ? ('\nActivities: ' + edu.activities) : ''
				}, {
					text: (edu.achievements) ? ('\nHonor, AP Courses, & AP Tests: ' + edu.achievements) : ''
				}]);
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Test score content.
		if ($scope.testScorePrint) {
			var actLen = $scope.actScore.length;
			var asvabLen = $scope.asvabScore.length;
			var satLen = $scope.satScore.length;
			var fyiLen = $scope.fyiScore.length;
			var otherLen = $scope.otherScore.length;

			if (actLen > 0 || asvabLen > 0 || satLen > 0 || fyiLen > 0 || otherLen > 0) {
				// section title
				content.push({
					text : 'Scores:',
					style : 'sectionTitle'
				});
			}

			// ASVAB score content
			if (asvabLen > 0) {
				for (var i = 0; i < asvabLen; i++) {

					/*
					 * content.push([ { text : 'Verbal Score:' +
					 * $scope.asvabScore[i].verbalScore },{ text : 'Math Score:' +
					 * $scope.asvabScore[i].mathScore },{ text : 'Science
					 * Score:' + $scope.asvabScore[i].scienceScore },{ text :
					 * 'AFQT Score:' + $scope.asvabScore[i].afqtScore } ]);
					 */

					content.push([ {
						style : 'scoreTable',
						table : {
							widths : [ 150, '*' ],
							headerRows : 1,
							keepWithHeaderRows : 1,
							dontBreakRows : true,
							body : [ [ {
								text : 'ASVAB Score',
								colSpan : 2,
								alignment : 'left',
								bold : true
							}, {} ], [ {
								text : 'Verbal Skills Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.asvabScore[i].verbalScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Math Skills Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.asvabScore[i].mathScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Science and Technical Skills Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.asvabScore[i].scienceScore,
								style : 'tableStyle'
							} ], [ {
								text : 'AFQT Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.asvabScore[i].afqtScore,
								style : 'tableStyle'
							} ] ]
						},
						layout : 'noBorders'
					} ]);
				}
			}

			// ACT score content
			if (actLen > 0) {
				for (var i = 0; i < actLen; i++) {

					content.push([ {
						style : 'scoreTable',
						table : {
							widths : [ 150, '*' ],
							headerRows : 1,
							keepWithHeaderRows : 1,
							dontBreakRows : true,
							body : [ [ {
								text : 'ACT Score',
								colSpan : 2,
								alignment : 'left',
								bold : true
							}, {} ], [ {
								text : 'Reading Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].readingScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Math Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].mathScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Writing Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].writingScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Science Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].scienceScore,
								style : 'tableStyle'
							} ], [ {
								text : 'English Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].englishScore,
								style : 'tableStyle'
							} ] ]
						},
						layout : 'noBorders'
					} ]);
				}
			}

			// SAT score content
			if (satLen > 0) {
				for (var i = 0; i < satLen; i++) {

					/*
					 * content.push([ { text : 'Reading Score:' +
					 * $scope.satScore[i].readingScore },{ text : 'Math Score:' +
					 * $scope.satScore[i].mathScore },{ text : 'Writing Score:' +
					 * $scope.satScore[i].writingScore },{ text :
					 * $scope.satScore[i].subject != null ? 'Subject Score:' +
					 * $scope.satScore[i].subject : '' } ]);
					 */

					

					content.push([ {
						style : 'scoreTable',
						table : {
							widths : [ 150, '*' ],
							headerRows : 1,
							keepWithHeaderRows : 1,
							dontBreakRows : true,
							body : [ [ {
								text : 'SAT Score',
								colSpan : 2,
								alignment : 'left',
								bold : true
							}, {} ], [ {
								text : 'Critical Reading',
								style : 'tableStyle'
							}, {
								text : '' + $scope.satScore[i].readingScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Mathematics',
								style : 'tableStyle'
							}, {
								text : '' + $scope.satScore[i].mathScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Writing',
								style : 'tableStyle'
							}, {
								text : '' + ($scope.satScore[i].writingScore !== 0 ? $scope.satScore[i].writingScore : 'None'),
								style : 'tableStyle'
							} ] ]
						},
						layout : 'noBorders'
					} ]);

					var otherSubjectScore = [];
					if ($scope.satScore[i].subject != null) {
						otherSubjectScore	= [ {
							text : 'Other Subject Score',
							style : 'tableStyle'
						}, {
							text : $scope.satScore[i].subject != null ? 'Subject Score:' + $scope.satScore[i].subject : 'None',
							style : 'tableStyle'
						} ];

						content[content.length - 1][0].table.body.push(otherSubjectScore);
					}
				}
			}

			// Other score content
			if (otherLen > 0) {

				for (var i = 0; i < otherLen; i++) {

					content.push([ {
						style : 'scoreTable',
						table : {
							widths : [ 150, '*' ],
							headerRows : 1,
							keepWithHeaderRows : 1,
							dontBreakRows : true,
							body : [ [ {
								text : 'Other Scores',
								colSpan : 2,
								alignment : 'left',
								bold : true
							}, {} ], [ {
								text : '',
								style : 'tableStyle'
							}, {
								text : '' + $scope.otherScore[i].description,
								style : 'tableStyle'
							} ] ]
						},
						layout : 'noBorders'
					} ]);
				}
			}

			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			})

		}

		// Achievement content.
		if ($scope.achievementPrint && $scope.achievement.length > 0) {
			// section title
			content.push({
				text : 'Achievement:\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.achievement, function (achievement) {
				content.push([{
					text: (!achievement.description ? '' : '\n' + achievement.description)
				}])
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Skill content.
		if ($scope.skillsPrint && $scope.skill.length > 0) {
			// section title
			content.push({
				text : 'Skill:\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.skill, function (skill) {
				content.push([{
					text: (!skill.description ? '' : '\n' + skill.description)
				}])
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Volunteer content.
		if ($scope.volunteerPrint && $scope.volunteer.length > 0) {
			// section title
			content.push({
				text : 'Volunteer:\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.volunteer, function (volunteer) {
				content.push([{
					text : '\n' + volunteer.organizationName + ', ',
					bold : true
				}, {
					text : volunteer.startDate + ' - ' + volunteer.endDate
				}]);
				//convert html for pdfMake to injest
				content.push(htmlToPdfmake(volunteer.description));
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Interests content.
		if ($scope.interestPrint && (fyiLen > 0 || $scope.interest.length > 0)) {
			// section title
			content.push({
				text : 'Interest:\n',
				style : 'sectionTitle'
			});

			// FYI score content
			if (fyiLen > 0) {
				content.push({
					text : '\nInterest Codes\n',
					style : 'sectionTitle'
				});

				// FYI
					angular.forEach($scope.fyiScore, function (fyiScore) {
						var interests = {
							textOne: $scope.getInterest(fyiScore.interestCodeOne),
							textTwo: $scope.getInterest(fyiScore.interestCodeTwo),
							textThree: $scope.getInterest(fyiScore.interestCodeThree)
						};
						content.push([{
							text: fyiScore.interestCodeOne + ' | ' + fyiScore.interestCodeTwo + ' | ' + fyiScore.interestCodeThree + ' ( ' + interests.textOne + ' | ' + interests.textTwo + ' | ' + interests.textThree + ')',
							style: 'tableStyle'
						}]);
					})
				}

			// Other interests
			if ($scope.interest.length > 0) {
				angular.forEach($scope.interest, function (interest) {
					content.push([ {
						text : (interest.description) ? '\n' + interest.description : ''
					} ]);
				});
			}

			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Work value content.
		if ($scope.workValuesPrint && $scope.workValue.length > 0) {
			// section title
			content.push({
				text : 'Work Value:\n\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.workValue, function (workVal) {
				content.push([ {
					text : (workVal.description) ? workVal.description : ''
				} ]);
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Favorite occupation content.
		if ($scope.favoriteOccupationPrint && $scope.favoriteList.length > 0) {
			// section title
			content.push({
				text : $scope.favoriteList.length === 1 ? 'Favorite Occupation:\n\n' : 'Favorite Occupations:\n\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.favoriteList, function (favOcc) {
				content.push([ {
					text : (favOcc.title) ? favOcc.title : ''
				} ]);
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Favorite career cluster content.
		if ($scope.favoriteCareerClusterPrint && $scope.careerClusterFavoriteList.length > 0) {
			// section title
			content.push({
				text : $scope.careerClusterFavoriteList.length === 1 ? 'Favorite Career Cluster:\n\n' : 'Favorite Career Clusters:\n\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.careerClusterFavoriteList, function (favCareerCluster) {
				content.push([ {
					text : (favCareerCluster.title) ? favCareerCluster.title : ''
				} ]);
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});
		}

		// Favorite school content.
		if ($scope.favoriteSchoolPrint && $scope.schoolFavoriteList.length > 0) {
			// section title
			content.push({
				text : $scope.schoolFavoriteList.length === 1 ? 'Favorite College:\n\n' : 'Favorite Colleges:\n\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.schoolFavoriteList, function (favSchool) {
				content.push([{
					text: (favSchool.schoolName) ? favSchool.schoolName : ''
				}]);
			});
			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			});

		}

		// Favorite occupation content.
		if ($scope.favoriteCitmOccupationPrint && $scope.citmFavoriteOccupations.length > 0) {
			// section title
			content.push({
				text : len === 1 ? 'Favorite Military Career:\n\n' : 'Favorite Military Careers:\n\n',
				style : 'sectionTitle'
			});
			angular.forEach($scope.citmFavoriteOccupations, function (favMilCareer) {
				content.push([ {
					text : (favMilCareer.title) ? favMilCareer.title : ''
				} ]);
			});
		}

		/**
		 * Set content.
		 */
		// [{text: 'ASVAB Portfolio', style:
		// 'headerTitle', margin: [72,40]},
		docDefinition = {
			/*
			 * header : [ { image : 'logo', margin : [ 0, 20, 0, 20 ], alignment :
			 * 'center' }, headerContent ],
			 */
			content : content,

			styles : {
				headerTitle : {
					fontSize : 22,
					bold : true,
					alignment : 'center'
				},
				sectionTitle : {
					margin : [ 0, 0, 0, 0 ],
					bold : true,
					alignment : 'left',
					fontSize : 12
				},
				sectionContent : {
				// margin: [0, 0, 0, 20]
				},
				tableStyle : {
					alignment : 'left'
				},
				scoreTable : {
					margin : [ 0, 30, 0, 0 ]
				},
				horizontalLine : {
					margin : [ 0, 20, 0, 0 ]
				}
			}
		};
	}

	$scope.printPdf = function() {
		setupContent();
		
		// IE support
		if (navigator.appName === 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {
			var modalOptions = {
					headerText : 'Alert',
					bodyText : 'Your browser does not support this feature. Alternatively, you can download as PDF and then print.'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
			return false;
		}
		
		pdfMake.createPdf(docDefinition).print();
	};

	$scope.downloadPdf = function() {
		setupContent();
		pdfMake.createPdf(docDefinition).download('ASVAB_Portfolio.pdf');
	};

	/*
	 * Add plan of action.
	 */
	$scope.addCombinedPlan = function() {
		$scope.savingPlan = true;
		var newObject = angular.copy($scope.combinedPlanObj);
		newObject.plan = JSON.stringify($scope.combinedPlanObj.plan);

		var promise = PortfolioRestFactory.insertPlan(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.originalPlanObj = angular.copy($scope.combinedPlanObj);
			$scope.plan.forEach(function(p) {
				$scope.deletePlan(p.id);
			})
			$scope.savingPlan = false;
		}, function(reason) {
			$scope.savingPlan = false;
			console.log(reason);
			alert("Failed to save plan of action data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	/*
	 * Update plan of action.
	 */
	$scope.updateCombinedPlan = function() {
		$scope.savingPlan = true;
		var newObject = angular.copy($scope.combinedPlanObj);
		newObject.plan = JSON.stringify($scope.combinedPlanObj.plan,
			Object.keys($scope.combinedPlanObj.plan));

		var promise = PortfolioRestFactory.updatePlan(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.originalPlanObj = angular.copy($scope.combinedPlanObj);
			$scope.savingPlan = false;
		}, function(reason) {
			$scope.savingPlan = false;
			console.log(reason);
			alert("Failed to save plan of action data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	};

	$scope.setupCombinedPlanObj = function() {
		if ($scope.plan.length > 0 && $scope.isCombinedPlanFormat) {
			// Update combined plan case
			$scope.combinedPlanObj.plan = JSON.parse($scope.plan[0].plan);
			$scope.combinedPlanObj.description = $scope.plan[0].description;
			$scope.combinedPlanObj.id = JSON.parse($scope.plan[0].id);
			//$scope.combinedPlanObj.userId = JSON.parse($scope.plan[0].userId);
		} else if ($scope.plan.length > 0 && !$scope.isCombinedPlanFormat) {
			// Migrate old to new format case
			$scope.plan.forEach(function(p) {
				switch(p.plan) {
					case '4 Year College/University':
						$scope.combinedPlanObj.plan.fourYearCollege = true;
						break;
					case '2 Year College':
						$scope.combinedPlanObj.plan.twoYearCollege = true;
						break;
					case 'Career and Technical Education':
						$scope.combinedPlanObj.plan.careerAndTech = true;
						break;
					case 'Military':
						$scope.combinedPlanObj.plan.military = true;
						break;
					case 'Work':
						$scope.combinedPlanObj.plan.work = true;
						break;
					case 'Gap Year':
						$scope.combinedPlanObj.plan.gapYear = true;
						break;
					case 'Undecided':
						$scope.combinedPlanObj.plan.undecided = true;
						break;
				}

				if (p.description) {
					$scope.combinedPlanObj.description =
						$scope.combinedPlanObj.description.concat(p.description, "\n");
				}
			})
		} // if neither, then it means we add combined plan

		$scope.originalPlanObj = angular.copy($scope.combinedPlanObj);
	}

	$scope.setupCombinedPlanObj();

	$scope.revertToOriginal = function() {
		$scope.combinedPlanObj = angular.copy($scope.originalPlanObj);
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
