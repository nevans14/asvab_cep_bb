/*******************************************************************************
 * Factory REST calls for Portfolio page. Does all the CRUD work.
 */
cepApp.factory('PortfolioRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var portfolioRestFactory = {};

	portfolioRestFactory.isPortfolioStarted = function() {
		return $http.get(appUrl + 'portfolio/PortfolioStarted');
	};

	portfolioRestFactory.isRegistered = function () {
		return $http.get(appUrl + 'portfolio/IsRegistered');
	};

	/**
	 * Work experience rest calls.
	 */
	portfolioRestFactory.getWorkExperience = function() {
		return $http.get(appUrl + 'portfolio/WorkExperience/');
	}

	portfolioRestFactory.insertWorkExperience = function(workExperienceObject) {
		console.log(workExperienceObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertWorkExperience', workExperienceObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(workExperienceObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteWorkExperience = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteWorkExperience/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateWorkExperience = function(workExperienceObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateWorkExperience', workExperienceObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Education rest calls.
	 */
	portfolioRestFactory.getEducation = function() {
		return $http.get(appUrl + 'portfolio/Education/');
	}

	portfolioRestFactory.insertEducation = function(educationObject) {
		console.log(educationObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertEducation', educationObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(educationObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteEducation = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteEducation/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateEducation = function(educationObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateEducation', educationObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Achievement rest calls.
	 */
	portfolioRestFactory.getAchievements = function() {
		return $http.get(appUrl + 'portfolio/Achievement/');
	}

	portfolioRestFactory.insertAchievement = function(achievementObject) {
		console.log(achievementObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertAchievement', achievementObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(achievementObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteAchievement = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteAchievement/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateAchievement = function(achievementObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateAchievement', achievementObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Interests rest calls.
	 */
	portfolioRestFactory.getInterests = function() {
		return $http.get(appUrl + 'portfolio/Interest/');
	}

	portfolioRestFactory.insertInterest = function(interestObject) {
		console.log(interestObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertInterest', interestObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(interestObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteInterest = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteInterest/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateInterest = function(interestObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateInterest', interestObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Skills rest calls.
	 */
	portfolioRestFactory.getSkills = function() {
		return $http.get(appUrl + 'portfolio/Skill/');
	}

	portfolioRestFactory.insertSkill = function(skillObject) {
		console.log(skillObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertSkill', skillObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(skillObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteSkill = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteSkill/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateSkill = function(skillObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateSkill', skillObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Work values rest calls.
	 */
	portfolioRestFactory.getWorkValues = function() {
		return $http.get(appUrl + 'portfolio/WorkValue/');
	}

	portfolioRestFactory.insertWorkValue = function(workValueObject) {
		console.log(workValueObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertWorkValue', workValueObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(workValueObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteWorkValue = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteWorkValue/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateWorkValue = function(workValueObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateWorkValue', workValueObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Volunteer rest calls.
	 */
	portfolioRestFactory.getVolunteers = function() {
		return $http.get(appUrl + 'portfolio/Volunteer/');
	}

	portfolioRestFactory.insertVolunteer = function(volunteerObject) {
		console.log(volunteerObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertVolunteer', volunteerObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(volunteerObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteVolunteer = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteVolunteer/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateVolunteer = function(volunteerObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateVolunteer', volunteerObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * ACT score rest calls.
	 */
	portfolioRestFactory.getActScore = function() {
		return $http.get(appUrl + 'portfolio/ACT/');
	}

	portfolioRestFactory.insertActScore = function(actObject) {
		console.log(actObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertACT', actObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteActScore = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteACT/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateActScore = function(actObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateACT', actObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * ASVAB score rest calls.
	 */
	portfolioRestFactory.getAsvabScore = function() {
		return $http.get(appUrl + 'portfolio/ASVAB/');
	}

	portfolioRestFactory.insertAsvabScore = function(asvabObject) {
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertASVAB', asvabObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteAsvabScore = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteASVAB/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateAsvabScore = function(asvabObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateASVAB', asvabObject).then(function successCallback(response) {
		//$http.put(appUrl + 'testScore/updateManualTestScore', asvabObject).then(function successCallback(response) { // returns bad request
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * FYI score rest calls.
	 */
	portfolioRestFactory.getFyiScore = function() {
		return $http.get(appUrl + 'portfolio/FYI/');
	}

	portfolioRestFactory.insertFyiScore = function(fyiObject) {
		console.log(fyiObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertFYI', fyiObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteFyiScore = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteFYI/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateFyiScore = function(fyiObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateFYI', fyiObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * SAT score rest calls.
	 */
	portfolioRestFactory.getSatScore = function() {
		return $http.get(appUrl + 'portfolio/SAT/');
	}

	portfolioRestFactory.insertSatScore = function(satObject) {
		console.log(satObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertSAT', satObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteSatScore = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteSAT/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateSatScore = function(satObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateSAT', satObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Other score rest calls.
	 */
	portfolioRestFactory.getOtherScore = function() {
		return $http.get(appUrl + 'portfolio/Other/');
	}

	portfolioRestFactory.insertOtherScore = function(otherObject) {
		console.log(otherObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertOther', otherObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteOtherScore = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteOther/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateOtherScore = function(otherObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateOther', otherObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}
	
	/**
	 * Plan of action rest calls.
	 */
	portfolioRestFactory.getPlan = function() {
		return $http.get(appUrl + 'portfolio/Plan/');
	}

	portfolioRestFactory.insertPlan = function(planObject) {
		console.log(planObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertPlan', planObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(planObject);
		return deferred.promise;
	}

	portfolioRestFactory.deletePlan = function(id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeletePlan/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updatePlan = function(planObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdatePlan', planObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}
	
	portfolioRestFactory.selectNameGrade = function() {
		return $http.get(appUrl + 'portfolio/SelectNameGrade/');
	}
	
	portfolioRestFactory.updateInsertUserInfo = function(userInfo) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateInsertUserInfo', userInfo).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}
	
	portfolioRestFactory.getCitmFavoriteOccupations = function() {
		return $http.get(appUrl + 'portfolio/get-citm-favorite-occupation/');
	}
	
	portfolioRestFactory.getAccessCode = function() {
		return $http.get(appUrl + 'portfolio/get-access-code/');
	}
	
	portfolioRestFactory.insertTeacherDemoIsTaken = function() {
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/insert-teacher-demo-taken').then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}
	
	portfolioRestFactory.getTeacherDemoIsTaken = function() {
		return $http.get(appUrl + 'portfolio/get-teacher-demo-taken/');
	}

	return portfolioRestFactory;

} ]);