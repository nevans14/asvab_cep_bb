cepApp.controller('SearchController', [ '$scope', '$rootScope', '$location', '$window', function($scope, $rootScope, $location, $window) {

	$scope.searchString = undefined;

	$scope.search = function() {
		// store search parameter into session storage
		if ($scope.searchString) {			
			$location.path('/search-result/keyword/' + $scope.searchString);	
		} else {
			// do nothing
		}
	}
	
	angular.element(document).ready(function () {
		function bootstrapGcseScript() {
		    var cx = '016306542328905981446:wkbq-87up34';
		    var gcse = document.createElement('script');
		    gcse.type = 'text/javascript';
		    gcse.async = true;
		    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(gcse, s);
		     setTimeout(function() {
		     	$(document).find(':input[id^=gsc-i-id]').prop('placeholder', '         ');
		     }, 200);
		}
		bootstrapGcseScript();
	})
} ]);