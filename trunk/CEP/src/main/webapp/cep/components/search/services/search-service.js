cepApp.service('SearchService', function ($http, $q, SearchRestFactory) {
	var service = {};
	var searchResult = undefined;
	
	service.getSearchResult = function (searchString) {
		var deferred = $q.defer();
		SearchRestFactory.getSearchResult(searchString).success(function (data) {
			searchResult = data;
			deferred.resolve(searchResult);
		}).error(function (data, status, headers, config) {
			deferred.reject('error: ' + data);
		})
		return deferred.promise;
	}
	
	service.getSearchResultWithStartIndex = function (searchString, startIndex) {
		var deferred = $q.defer();
		SearchRestFactory.getSearchResultWithStartIndex(searchString, startIndex).success(function (data) {
			searchResult = data;
			deferred.resolve(searchResult);
		}).error(function (data, status, headers, config) {
			deferred.reject('error: ' + data);
		})
		return deferred.promise;
	}
	
	return service;
})