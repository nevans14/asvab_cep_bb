cepApp.factory('SearchRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var searchRestFactory = {};

	searchRestFactory.getSearchResult = function(searchString) {
		return $http.get(appUrl + '/google-search/get-search-results/' + searchString + '/');
	}
	
	searchRestFactory.getSearchResultWithStartIndex = function(searchString, startIndex) {
		return $http.get(appUrl + '/google-search/get-search-results/' + searchString + '/' + startIndex + '/');
	}

	return searchRestFactory;

} ]);