cepApp.controller('SearchResultController', [ '$scope', '$rootScope', '$location', '$route', 'resource', 'searchResult', 'SearchService', function($scope, $rootScope, $location, $route, resource, searchResult, SearchService) {

	$scope.searchResult = searchResult;
	$scope.searchString = $route.current.params.searchString;
	$scope.pdfBaseURL = resource + 'pdf/';	
	$scope.showErrorMessage = false;
	
	if (!$scope.searchResult) {
		$scope.showErrorMessage = true;
		$scope.errorMessage = 'We are sorry, but we cannot process your search request at this moment. Please check again tomorrow.';
	}
	
	$scope.nextPage = function () {
		var startIndex = $scope.searchResult.queries.nextPage[0].startIndex;
		SearchService.getSearchResultWithStartIndex($scope.searchString, startIndex).then(function (data) {
			$scope.searchResult = data;
		});
	}

	$scope.prevPage = function() {
		var startIndex = $scope.searchResult.queries.previousPage[0].startIndex;
		SearchService.getSearchResultWithStartIndex($scope.searchString, startIndex).then(function (data) {
			$scope.searchResult = data;
		});
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);