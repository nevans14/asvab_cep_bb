cepApp.factory('MediaCenterRestFactory', function($http, awsApiFullUrl) {

	var mediaCenterRestFactory = {};
	
	mediaCenterRestFactory.getMediaCenterArticleById = function(mediaId) {
		return $http.get(awsApiFullUrl + '/api/media-center/getMediaCenterArticleById/' + mediaId);
	}

	mediaCenterRestFactory.getMediaCenterList = function() {
		return $http.get(awsApiFullUrl + '/api/media-center/getMediaCenterList');
	}
	
	mediaCenterRestFactory.getMediaCenterByCategoryId = function(categoryId) {
		return $http.get(awsApiFullUrl + '/api/media-center/getMediaCenterByCategoryId/' + categoryId);
	}

	mediaCenterRestFactory.getMediaCenterArticleByCategoryNameSlug = function(categoryName, slug) {
		return $http.get(awsApiFullUrl + '/api/media-center/articles/' + categoryName + '/' + slug);
	}
	
	return mediaCenterRestFactory;

});