cepApp.controller('MediaCenterArticleListController', [ '$scope', 'mediaCenterList', '$route', function($scope, mediaCenterList, $route) {

	$scope.mediaCenterList = mediaCenterList;
	$scope.categoryId = $route.current.params.categoryId
	$scope.articleList = [];
	
	$scope.title = ($scope.categoryId == 1 ? 'ASVAB CEP Resources for Students' :
					$scope.categoryId == 2 ? 'ASVAB CEP Resources for Parents' :
					$scope.categoryId == 4 ? 'ASVAB CEP Resources for Educators' :
					'Tutorials');
	$scope.description = ($scope.categoryId == 1 ? 'The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to assist students in career planning.' :
						  $scope.categoryId == 2 ? 'The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to help parents help their children plan for the future.' :
						  $scope.categoryId == 4 ? 'The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to help school counselors advise students on career planning.' :
						  'The ASVAB Career Exploration Program offers video tutorials to help students understand scores, find their interest, and explore careers in the OCCU-Find.');
	
	for(var i = 0; i < $scope.mediaCenterList.length; i++){
		if($scope.mediaCenterList[i].categoryId == $scope.categoryId){
			$scope.articleList.push($scope.mediaCenterList[i]);
		}
	}
	$scope.articleList.push($scope.articleList.splice(0, 1)[0]);
	
	/**
	 * Pagination
	 */
	$scope.totalItems = $scope.articleList.length;
	$scope.currentPage = 1;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 10;

	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
