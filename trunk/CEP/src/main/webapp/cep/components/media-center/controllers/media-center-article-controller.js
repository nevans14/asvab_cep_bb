cepApp
.directive('dynamic', function($compile) {
	return {
		restrict: 'A',
		replace: true,
		link: function(scope, ele, attrs) {
			scope.$watch(attrs.dynamic, function (html) {
				ele.html(html);
				$compile(ele.contents())(scope);
			});
		}
	};
})
.controller('MediaCenterArticleController', [ 'resource', '$scope', 'mediaCenterArticle', 'mediaCenterList', '$route', '$sce', '$location', 'BringToSchoolModalService', 'UtilityService', '$window', function(resource, $scope, mediaCenterArticle, mediaCenterList, $route, $sce, $location, BringToSchoolModalService, UtilityService, $window) {
	$scope.mediaId = $route.current.params.mediaId;
	$scope.mediaCenterList = mediaCenterList;	
	$scope.absUrl = $location.absUrl();
	$scope.domainName = resource;
	$scope.article = mediaCenterArticle;
	$scope.articleMetaTitle = mediaCenterArticle && mediaCenterArticle.seoTitle ? mediaCenterArticle.seoTitle : mediaCenterArticle.title; 
	$scope.articleMetaDescription = mediaCenterArticle && mediaCenterArticle.metaDescription ? mediaCenterArticle.metaDescription : mediaCenterArticle.articleDescription;
	$scope.trustedVideoUrl = $sce.trustAsResourceUrl($scope.article.videoImgUrl);
	$scope.thumbnailUrl = $scope.article.thumbnailUrl.replace(/(\n|\r)/gm, '');
	$scope.shareImgUrl = $scope.article.shareImgUrl.replace(/(\n|\r)/gm,'');
	$scope.mediaUrl = UtilityService.getMediaUrl();
	
	// get secured urls
	$scope.thumnailUrl = UtilityService.getSecuredUrl($scope.thumbnailUrl);
	$scope.shareImgUrl = UtilityService.getSecuredUrl($scope.shareImgUrl);
	var path = $location.path();
	$scope.absUrl = UtilityService.getCanonicalUrl();	
	
	$scope.ShrinkDescription = function(description, size) {
		if (!description || description.length <= size) {
			return description;
		} else {
			return description.substring(0, size) + "...";
		}
	};
	
	/**
	 * Bring ASVAB to your school modal
	 */
	$scope.asvabModal = function() {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_LINKED' );
		BringToSchoolModalService.show({}, {});
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
