cepApp.controller('MediaCenterController', [ '$scope', 'mediaCenterList', 'resource', 'inTheNewsList', 'testimonialsList', 'sharablesList', 'UtilityService',  function($scope, mediaCenterList, resource, inTheNewsList, testimonialsList, sharablesList, UtilityService) {

	$scope.inTheNewsList = inTheNewsList;
	$scope.testimonialsList = testimonialsList;
	$scope.sharablesList = sharablesList;
	$scope.mediaCenterList = mediaCenterList;
	$scope.documents = resource + 'pdf/';
	$scope.filteredMediaCenterList = [];
	$scope.awsMediaUrl = UtilityService.getMediaUrl();
	
	$timeout = twttr.widgets.load();

	$scope.seoTitle = 'Blog and News | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Find the latest blogs, news, media, and communications about the ASVAB Career Exploration Program for students, counselors, and parents.';

	$scope.filterMediaCenterList = function() {
		$scope.filteredMediaCenterList = $scope.mediaCenterList.filter(function(item) {
			return item.categoryId === 5;
		});
		$scope.filteredMediaCenterList.push($scope.filteredMediaCenterList.splice(0, 1)[0]);
	}

	$scope.filterMediaCenterList();

    angular.element(document).ready(function() {
		$('.sharables').slick({
		  dots: false,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
			{
			  breakpoint: 640,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		  ]
		});
      });
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
