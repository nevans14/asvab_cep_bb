cepApp.factory('MediaCenterService', [ '$q', '$window', 'MediaCenterRestFactory', function($q, $window, MediaCenterRestFactory) {

	var service = {};

	/**
	 * Media Center Session Storage
	 */
	var mediaCenterList = undefined;
	var mediaCenterArticle = undefined;
	
	service.getMediaCenterArticleById = function (mediaId) {
		var deferred = $q.defer();
		MediaCenterRestFactory.getMediaCenterArticleById(mediaId).success(function (data) {
			mediaCenterArticle = data;
			deferred.resolve(mediaCenterArticle);
		}).error(function (data, status, headers, config) {
			deferred.reject("error: " + data)
		});
		return deferred.promise;
	}
	
	/**
	 * Media Center by Category
	 */
	service.getMediaCenterByCategoryId = function(categoryId) {
		var deferred = $q.defer();
		MediaCenterRestFactory.getMediaCenterByCategoryId(categoryId).success(function (data) {
			var mediaCenterByCategory = data;
			deferred.resolve(mediaCenterByCategory);
		}).error(function (data, status, headers, config) {
			deferred.reject('error: ' + data);
		})
		return deferred.promise;
	}

	/**
	 * Media Center by Category name and slug
	 */
	service.getMediaCenterArticleByCategoryNameSlug = function(categoryName, slug) {
		var deferred = $q.defer();
		MediaCenterRestFactory.getMediaCenterArticleByCategoryNameSlug(categoryName, slug).success(function (data) {
			var mediaCenterByCategoryNameSlug = data;
			deferred.resolve(mediaCenterByCategoryNameSlug);
		}).error(function (data, status, headers, config) {
			deferred.reject('error: ' + data);
		})
		return deferred.promise;
	}
	
	/**
	 * Return favorite list.
	 */
	service.getMediaCenterList = function() {

		var deferred = $q.defer();

		// if mediaCenterList is undefined then recover data from database
		if (mediaCenterList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("mediaCenterList") === null || $window.sessionStorage.getItem("mediaCenterList") == 'undefined') {

					// recover data using database
					MediaCenterRestFactory.getMediaCenterList().success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('mediaCenterList', JSON.stringify(data));

						// sync with in memory property
						mediaCenterList = angular.fromJson($window.sessionStorage.getItem('mediaCenterList'));

						deferred.resolve(mediaCenterList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					mediaCenterList = angular.fromJson($window.sessionStorage.getItem('mediaCenterList'));
					deferred.resolve(mediaCenterList);
				}

			} else {

				// no session storage support so get recover data using database
				MediaCenterRestFactory.getMediaCenterList().success(function(data) {

					// success now store in session object
					mediaCenterList = data;
					deferred.resolve(mediaCenterList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(mediaCenterList);
		}

		return deferred.promise;
	}

	return service;

} ]);