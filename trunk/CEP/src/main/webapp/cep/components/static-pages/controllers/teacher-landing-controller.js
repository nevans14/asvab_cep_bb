cepApp.controller('TeacherLandingController', function($scope, $location, MediaCenterService, $http, BringToSchoolModalService, youTubeModalService, $location, modalService, $window, TeacherModalService, $anchorScroll) {
	
	$scope.openRegistrtionModal = function() {

		TeacherModalService.show({},{});
	}

	$scope.showYouTubeModal = function(videoURL) {
		youTubeModalService.show({
			size : 'lg'
		}, {
			videoName : videoURL
		});
	}
	/**
	 * Utilizes hash parameter to scroll to video section.
	 */
	if($location.hash() == 'videos'){
		$anchorScroll();
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

});
