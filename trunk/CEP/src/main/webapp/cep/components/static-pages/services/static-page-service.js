cepApp.factory('StaticPageService', function ($q, $http, StaticPageRestService) {
	var service = {};
	var page = undefined;
	
	service.getPageById = function (pageId) {
		var deferred = $q.defer();
		StaticPageRestService.getPageById(pageId).success(function (data) {
			page = data;
			deferred.resolve(page);
		}).error(function(data, status, headers, config) {
			deferred.reject('error: ' + data);
		});
		return deferred.promise;
	};
	
	service.getPageByPageName = function(pageName) {
		var deferred = $q.defer();
		StaticPageRestService.getPageByPageName(pageName).success(function (data) {
			page = data;
			deferred.resolve(page);
		}).error(function(data, status, headers, config) {
			deferred.reject('error: ' + data);
		});
		return deferred.promise;
	};
	
	return service;
})
