cepApp.service('ResourceService', function ($q, ResourceRestService) {
	var service = {};
	
	service.getResources = function() {
		var deferred = $q.defer();
		ResourceRestService.getResources().success(function (data) {
			deferred.resolve(data);
		}).error(function (data, status, headers, config) {
			deferred.reject('error: ' + data);
		});
		return deferred.promise;
	};
	
	service.getQuickLinks = function() {
		var deferred = $q.defer();		
		ResourceRestService.getQuickLinks().success(function (data) {
			deferred.resolve(data);
		}).error(function(data, status, headers, config) {
			deferred.reject('error: ' + data);
		});		
		return deferred.promise;
	}
	
	return service;
});