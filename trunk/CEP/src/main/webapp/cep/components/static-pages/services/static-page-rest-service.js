cepApp.factory('StaticPageRestService', ['$http', 'appUrl', 'awsApiFullUrl', function ($http, appUrl, awsApiFullUrl) {
	var service = {};
	
	service.getPageById = function (pageId) {
		return $http({url: awsApiFullUrl + '/api/pages/get-by-page-id/' + pageId + '/', method: 'GET'});
	}
	
	service.getPageByPageName = function(pageName) {
		return $http({url: awsApiFullUrl + '/api/pages/get-by-page-name/' + pageName + '/', method: 'GET'});
	}
	
	return service;
}]);