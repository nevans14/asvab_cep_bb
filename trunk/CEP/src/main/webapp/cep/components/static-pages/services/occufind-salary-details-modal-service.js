cepApp.service('OccuSalaryModalService', [ '$uibModal', '$sce', '$window','$q', 'careerClusterRestFactory', 'OccufindRestFactory', '$location', 'modalService',
    function($uibModal, $sce, $window,$q, careerClusterRestFactory, OccufindRestFactory, $location, modalService) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/static-pages/page-partials/occufind-salary-details.html',
		windowClass : 'asvab-modal',
		size : 'lg'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyImage != undefined) {
				customModalOptions.bodyImage = $sce.trustAsHtml(customModalOptions.bodyImage);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
                $scope.modalOptions = tempModalOptions;
				$scope.stateSalary = customModalOptions.stateSalary;
				$scope.stateEntrySalary = customModalOptions.stateEntrySalary;
                $scope.blsTitle = customModalOptions.blsTitle;
                $scope.nationalEntrySalary = customModalOptions.nationalEntrySalary;
                $scope.nationalSalary = customModalOptions.nationalSalary;

                //setup charts
                var outerArray = [];
                var innerArray = [ 'State', 'AverageSalary', {
                    type : 'string',
                    role : 'tooltip'
                } ];
                outerArray[0] = innerArray;
                for (var i = 0; i < $scope.stateSalary.length; i++) {
                    if ($scope.stateSalary[i].avgSalary != -1) {
                    var innerArray = [ $scope.stateSalary[i].state, $scope.stateSalary[i].avgSalary, 'Average Salary: $' + $scope.stateSalary[i].avgSalary.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') ];
                    } else {
                    var innerArray = [ $scope.stateSalary[i].state, 187200, 'Average Salary: $' + '187,200 or more' ];
                    }
            
                    outerArray[i + 1] = innerArray;
                }
                var chart1 = {};
                chart1.type = "GeoChart";
                chart1.data = outerArray;
                chart1.options = {
                    width : 480,
                    /*
                    * width : 406, height : 297,
                    */
                    region : "US",
                    resolution : "provinces",
                    colors : [ '#ffffff', '#02767b' ]
                };
                $scope.chart = chart1;
                
                var entryOuterArray = [];
                var entryInnerArray = [ 'State', 'EntrySalary', {
                    type : 'string',
                    role : 'tooltip'
                } ];
                entryOuterArray[0] = entryInnerArray;
                for (var i = 0; i < $scope.stateEntrySalary.length; i++) {
                    if ($scope.stateEntrySalary[i].entrySalary == undefined) {
                    var entryInnerArray = [ $scope.stateEntrySalary[i].state, 0, 'Entry Salary: N/A' ];
                    } else if ($scope.stateEntrySalary[i].entrySalary != -1) {
                    var entryInnerArray = [ $scope.stateEntrySalary[i].state, $scope.stateEntrySalary[i].entrySalary, 'Entry Salary: $' + $scope.stateEntrySalary[i].entrySalary.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') ];
                    } else {
                    var entryInnerArray = [ $scope.stateEntrySalary[i].state, 208000, 'Entry Salary: $208000 or more' ];
                    }
                    entryOuterArray[i + 1] = entryInnerArray;
                }

                // entry-level state salary
                var entryLevelSalaryChart = {};
                entryLevelSalaryChart.type = "GeoChart";
                entryLevelSalaryChart.data = entryOuterArray;
                entryLevelSalaryChart.options = {
                    width: 480,
                    region: "US",
                    resolution: "provinces",
                    colors: [ '#fff', '#02737b' ]
                }
                $scope.entryLevelSalaryChart = entryLevelSalaryChart;
            
                // mobile version
                var chart2 = {};
                chart2.type = "GeoChart";
                chart2.data = outerArray;
                chart2.options = {
                    width : 300,
                    /*
                    * width : 406, height : 297,
                    */
                    region : "US",
                    resolution : "provinces",
                    colors : [ '#ffffff', '#02767b' ]
                };
                $scope.chartMobile = chart2;
                
                var entryLevelSalaryChartMobile = {};
                entryLevelSalaryChartMobile.type = "GeoChart";
                entryLevelSalaryChartMobile.data = entryOuterArray;
                entryLevelSalaryChartMobile.options = {
                    width: 300,
                    region: "US",
                    resolution: "provinces",
                    colors: [ '#fff', '#02767b' ]
                }
                $scope.entryLevelSalaryChartMobile = entryLevelSalaryChartMobile;


				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

                // direct to  more details page
                $scope.employmentMoreDetails = function() {
					$uibModalInstance.dismiss('cancel');
                    $location.url('/occufind-employment-details/15-1199.02');
                }

                $scope.openDescription = function(section) {

                    var headerTitle, bodyText;
                    if(section == 'poverty-level'){
                        headerTitle = 'Poverty Level';
                        bodyText = 'This number is issued each year by the Department of Health and Human Services (HHS). It represents the poverty guidelines for a single individual living in the 48 contiguous states. The number varies for citizens of Alaska and Hawaii, and increases with each additional member of the household.';
                    } else{
                        return 0;
                    }
            
                    var modalOptions = {
                        headerText : headerTitle,
                        bodyText : bodyText
                    };
                    
                    var screenSize = 'sm';
            
                    modalService.showModal({
                        size : screenSize
                    }, modalOptions);
                }

			}];
		}

		return $uibModal.open(tempModalDefaults).result;
    };
    
    this.getStateSalary = function (id) {
        var deferred = $q.defer();
        var stateSalary;
		careerClusterRestFactory.getStateSalary(id).success(function (data) {
            stateSalary = data;
			deferred.resolve(stateSalary);
		}).error(function (data, status, headers, config) {
			deferred.reject("error: " + data);
        });
		return deferred.promise;
    };

    this.getStateEntrySalary = function (id) {
        var deferred = $q.defer();
        var stateEntrySalary;
		careerClusterRestFactory.getStateEntrySalary(id).success(function (data) {
            stateEntrySalary = data;
			deferred.resolve(stateEntrySalary);
		}).error(function (data, status, headers, config) {
			deferred.reject("error: " + data);
		});
		return deferred.promise;
    };

    this.getBLSTitle = function (id) {
        var deferred = $q.defer();
        var blsTitle;
		OccufindRestFactory.getBLSTitle(id).success(function (data) {
            blsTitle = data;
			deferred.resolve(blsTitle);
		}).error(function (data, status, headers, config) {
			deferred.reject("error: " + data);
		});
		return deferred.promise;
    };

    this.getNationalEntrySalary = function (id) {
        var deferred = $q.defer();
        var nationalEntrySalary;
		OccufindRestFactory.getNationalEntrySalary(id).success(function (data) {
            nationalEntrySalary = data;
			deferred.resolve(nationalEntrySalary);
		}).error(function (data, status, headers, config) {
			deferred.reject("error: " + data);
		});
		return deferred.promise;
    };

    this.getNationalSalary = function (id) {
        var deferred = $q.defer();
        var nationalSalary;
		OccufindRestFactory.getNationalSalary(id).success(function (data) {
            nationalSalary = data;
			deferred.resolve(nationalSalary);
		}).error(function (data, status, headers, config) {
			deferred.reject("error: " + data);
		});
		return deferred.promise;
    };

} ]);