cepApp.factory('ResourceRestService', ['$http', 'awsApiFullUrl', function ($http, awsApiFullUrl) {
	var factory = {};
	
	factory.getResources = function () {
		return $http({url: awsApiFullUrl + '/api/resources/get-resources/', method: 'GET'});
	};
	
	factory.getQuickLinks = function () {
		return $http({url: awsApiFullUrl + '/api/resources/get-quick-links/', method: 'GET'});
	};
	
	return factory;
}]);