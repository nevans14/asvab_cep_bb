cepApp.service('ASVABImageZoom', [ '$uibModal', '$sce', '$window', function($uibModal, $sce, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/static-pages/page-partials/asvab-zoom-modal.html',
		windowClass : 'asvab-modal',
		size : 'lg'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyImage != undefined) {
				customModalOptions.bodyImage = $sce.trustAsHtml(customModalOptions.bodyImage);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

			}];
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);