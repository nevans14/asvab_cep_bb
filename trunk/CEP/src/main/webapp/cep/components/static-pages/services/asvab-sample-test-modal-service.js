cepApp.service('ASVABSampleTestModalService', [ '$uibModal', '$sce', '$window', function($uibModal, $sce, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/static-pages/page-partials/asvab-sample-test-modal.html',
		windowClass : 'asvab-modal',
		size : 'lg'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.checkAnswers = function() {
					for (var i = 0; i < $scope.answers.length; i++) {
						if ($scope.answers[i].selection != undefined && $scope.answers[i].answer == $scope.answers[i].selection) {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: green">Correct!</span>');
						} else if ($scope.answers[i].selection != undefined && $scope.answers[i].answer != $scope.answers[i].selection) {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: red">Incorrect: The correct answer is ' + $scope.answers[i].answer + '!</span>');
						} else {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: orange">No answer selected.</span>');
						}
					}

					angular.element(document).ready(function() {

						// $(".modal").scrollTop(0);
						$('.modal').animate({
							scrollTop : $('.modal-content').offset().top - 80
						}, 500);
					});
				}

				$scope.answers = [ {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				} ];
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);