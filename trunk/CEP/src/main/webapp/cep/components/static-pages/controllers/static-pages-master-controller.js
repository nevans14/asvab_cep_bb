cepApp
.directive('repeatDone', function() {
  return function(scope, element, attrs) {
    if (scope.$last) {
      scope.$eval(attrs.repeatDone);
    }
  }
})
.directive('dynamic', function($compile) {
  return {
    restrict: 'A',
    replace: true,
    link: function(scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function (html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
    }
  };
})
.controller('StaticPageMasterController', function($scope, $route, $rootScope, $location, $anchorScroll, $timeout, MediaCenterService, $http, ASVABSampleTestModalService, ASVABICATSampleTestModalService, resource, BringToSchoolModalService, videoModalService, youTubeModalService, vcRecaptchaService, recaptchaKey, FaqRestFactory, modalService, $window, ASVABImageZoom, ctmUrl, ResourceService, StaticPageService, TeacherModalService, StrategyTakingAsvabModalService, ViewDetailModalService, RegisterModalService, UtilityService, awsApiFullUrl, baseUrl, OccuSalaryModalService) {
  
  var url = $location.url();

  if( url=='/general-resources#box-row4'){
    var BoxRowInterval = setInterval(function () {
      if ($("#box-row4").length > 0) {
          clearInterval(BoxRowInterval);
          setTimeout(function () {
            $('html, body').animate({
              scrollTop: $("#box-row4").offset().top - $('#header').height() - $('#banner').height() - 50
            }, 1000); 
        }, 1000);
      }
    }, 200); 
  }
  // remove the first slash of the page
  var pageName = url.substring((url.indexOf('/') + 1), url.length);
  // remove # if someone is trying to target an accordion section from an previous page
  if (pageName.indexOf('#') > -1) {
    pageName = pageName.substring(0, (pageName.indexOf('#')));
  }
  // remove ? if appending a query string (campaign)
  if (pageName.indexOf('?') > -1) {
    pageName = pageName.substring(0, pageName.indexOf('?'));
  }
  $scope.windowWidth = $window.innerWidth;
  // reference urls
  $scope.mediaUrl = UtilityService.getMediaUrl();
  $scope.resources = resource + 'static/';
  $scope.documents = $scope.mediaUrl + 'CEP_PDF_Contents/';
  $scope.media = resource + 'media-center-content/';
  $scope.mediaCenterArticle = 'media-center-article/';
  $scope.ssoUrl = ctmUrl;
  // objects
  $scope.mediaCenterList = undefined;
  $scope.page = undefined;
  $scope.resourceTopics = undefined;
  $scope.quickLinkTopics = undefined;
  $scope.isLoggedIn = $rootScope.globals.currentUser !== undefined;
  $scope.anchor = $route.current.params.anchor;
  $scope.baseUrl = baseUrl;
  $scope.stateSalary = undefined;
  $scope.stateEntrySalary = undefined;

  /***
   * Resource Types enums
   */
  $scope.resourceType = {
    'Document'   : 1,
    'Video'    : 2,
    'Link'    : 3,
    'ExtLink'  : 4,
    'PopupVideo': 5
  };  
  $scope.quickLinkType = {
    'Document'   : 1,
    'Link'     : 2,
    'JavaScript' : 3
  };  
  $scope.goToAnchor = function(id) {
    $anchorScroll(id);
  }
  var execAfterRender = function() {
    if ($scope.anchor) {
      $scope.goToAnchor($scope.anchor);
    }
  }

  $timeout(function() {
    execAfterRender();
  }, 500)
  
  
  /***
   * Get static page from DB only if the page is not general-resources
   */
  if (url.indexOf("general-resources") === -1) {
    StaticPageService.getPageByPageName(pageName).then(function (data) {
      $scope.page = data;
    });
  }
  /***
   * Get media center articles
   */
  MediaCenterService.getMediaCenterList().then(function(s) {
    $scope.mediaCenterList = s;
  });
  /***
   * Get resources from DB
   */
  ResourceService.getResources().then(function (data) {
    $scope.resourceTopics = data;
  });
  ResourceService.getQuickLinks().then(function (data) {
    $scope.quickLinkTopics = data;
  });  
  
  $scope.generalHelpGeneral = false ;
  $scope.generalHelpGender  = false ;
  $scope.generalHelpSkill   = false ;
  $scope.generalHelpRelease = false ;
  $scope.generalHelpFyi  = false ;
  
  if (!String.prototype.startsWith) {
      String.prototype.startsWith = function(searchString, position){
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
    };
  }
  console.log(url);
  if (  url.startsWith("/general-help") && url.indexOf("#") !== -1  ) {
    if ( -1 < url.indexOf("#general")) {
      $scope.generalHelpGeneral = true ;
    } else if ( -1 < url.indexOf("#gender") ) {
      $scope.generalHelpGender  = true ;
    } else if ( -1 < url.indexOf("#skill") ) {
      $scope.generalHelpSkill  = true ;
    } else if ( -1 < url.indexOf("#release") ) {
      $scope.generalHelpRelease  = true ;
    } else if ( -1 < url.indexOf("#fyi") ) {
      $scope.generalHelpFyi  = true ;
    }
  }  
  
  $scope.occupationPagination = {};
  $scope.occupationPagination.TotalItems = 0;
  $scope.occupationPagination.CurrentPage = 1;
  $scope.occupationPagination.MaxSize = 4;
  $scope.occupationPagination.ItemsPerPage = 10;
  $http({url: awsApiFullUrl + '/api/occufind/ViewAllOccupations/', method: 'GET'}).then(function(occupations) {
    $scope.occupationList = occupations.data;

    $scope.occupationPagination.TotalItems = $scope.occupationList.length;

  });
  /**
   * Utilizes hash parameter to scroll to video section.
   */
  if($location.hash() === 'videos'){
    $anchorScroll();
  }
  // scroll to hash
  $scope.goToBox = function(id) {
    var newHash = 'box-row' + id;
    if ($location.hash() !== newHash) {
      $location.hash(newHash);
    } else {
      $anchorScroll();
    }
  };
  
  /******************************************
   * MODALS                  *
   ******************************************/
  /**
   * Show view detail modal
   */
  $scope.showViewDetailModal = function() {
    $scope.isLoggedIn = $rootScope.globals.currentUser !== undefined;
    if ($scope.isLoggedIn) {
      $location.url('/occufind-occupation-search');
    } else {
      ViewDetailModalService.show({}, {});
    }
  };
  /**
   * Show view more modal
   */
  $scope.showViewMoreModal = function() {
    $scope.isLoggedIn = $rootScope.globals.currentUser !== undefined;
    if ($scope.isLoggedIn) {
      $location.url('/classroom-activities');
    } else {
      ViewDetailModalService.show({}, {});
    }
  };

    /**
   * Show salary modal
   */
  $scope.showSalaryModal = function() {
    OccuSalaryModalService.show({}, 
      {stateSalary: $scope.stateSalary, 
       stateEntrySalary: $scope.stateEntrySalary,
       blsTitle: $scope.blsTitle,
       nationalEntrySalary: $scope.nationalEntrySalary,
       nationalSalary: $scope.nationalSalary
      });
  };

  /**
   * Show student strategy taking ASVAB modal
   */
  $scope.showStrategyTakingAsvabModal = function() {
    StrategyTakingAsvabModalService.show({},{});
  };
  /**
   * Calls function based on literal string
   */
  $scope.showModal = function(func) {
    if (angular.isFunction($scope[func])) {
      $scope[func]();
    }
  };
  /**
   * Show teacher registration modal
   */
  $scope.openRegistrtionModal = function() {
    RegisterModalService.show({}, {}, undefined)
  };
  /**
   * Show Bring the ASVAB CEP to Your School modal
   */
  $scope.asvabModal = function(size) {
    $window.ga('create', 'UA-83809749-1', 'auto');
    $window.ga('send', 'pageview', url + '#BRING_ASVAB_FORM_LINKED' );
    BringToSchoolModalService.show({}, {});
  };
  /**
   * Show sample test modal.
   */
  $scope.showSampleTestModal = function() {
    ASVABSampleTestModalService.show({}, {});
  };
  
  $scope.showSampleIcatTestModal = function() {
    ASVABICATSampleTestModalService.show({}, {});
  };
  /**
   * Show video modal
   */
  $scope.showVideoModal = function(videoName) {
    videoModalService.show({
      size : 'lg'
    }, {
      videoName : videoName
    });
  };
  /**
   * Show YouTube modal
   */
  $scope.showYouTubeModal = function(videoName) {
    youTubeModalService.show({
      size : 'lg'
    }, {
      videoName : videoName
    });
  };

  $scope.showWorkValueModal = function(valueName) {
    var description = null;
    switch(valueName) {
      case 'Achievement':
        description = 'Workers who score high on Achievement are results-oriented. These workers often pursue jobs where employees are able to apply their strengths and abilities. This gives the employee a sense of accomplishment.';
        break;
      case 'Independence':
        description = 'Workers who score high on Independence value the ability to approach work activities with creativity. These workers want to make their own decisions and plan their work with little supervision from a manager.';
        break;
      case 'Recognition':
        description = 'Workers who score high on Recognition pursue jobs with opportunities for advancement and leadership responsibilities that allow them to give direction and instruction to others. These workers are often considered prestigious by their peers and others in their organization and receive recognition for the work they contribute.';
        break;
      case 'Relationships':
        description = 'Workers who score high on Relationships prefer jobs that provide services to others and working with co-workers in a friendly, non-competitive environment. Workers in these jobs value getting along well with others and do not like to be pressured to do things that go against their morals or sense of what is right and wrong.';
        break;
      case 'Support':
        description = "Workers who score high on Support appreciate when their company's leadership stands behind and supports their employees. People in these types of jobs like to feel like they are being treated fairly by the company and have supervisors who spend time and effort training their workers to perform well.";
        break;
      case 'Working Conditions':
        description = 'Workers who score high on Working Conditions value job security and pleasant working conditions. These workers enjoy being busy and want to be paid well for the work they do. They enjoy developing ways of doing things with little or no supervision and depend on themselves to get the work done. These workers pursue steady employment that offers something different to do on a daily basis.';
        break;
    }

    var modalOptions = {
      headerText : valueName,
      bodyText : description
    };

    modalService.showModal({
      size : 'md'
    }, modalOptions);
  }
  /**
   * Bring ASVAB to your school form implementation.
   */
  $scope.responseOne = '';
  $scope.responseTwo = '';
  $scope.recaptchaKey = recaptchaKey;
  
  $scope.counselorEmail;
  $scope.studentHeardUs;
  $scope.studentHeardUsOther;
  
  $scope.shareWithCounselor = function() {

    $scope.formDisabled = true;
    $scope.errorSharing = undefined;
    var email = $scope.counselorEmail;
    var studentHeardUs = $scope.studentHeardUs;
    var studentHeardUsOther = $scope.studentHeardUsOther;

    if ($scope.responseOne === "") { // if string
      // is empty
      $scope.errorSharing = "Please resolve the captcha and submit!";
      $scope.formDisabled = false;
      return false;
    } else {
        
      var emailObject = {
        email: email,
        studentHeardUs: studentHeardUs,
        studentHeardUsOther: studentHeardUsOther,
        recaptcha: $scope.responseOne
      };
      FaqRestFactory.shareWithCounselor(emailObject).then(function(success) {
        $window.ga('send', 'pageview', url + '#BRING_ASVAB_FORM_STUDENT_SHARE' );
        
        var modalOptions = {
          headerText : 'Schedule Notification',
          bodyText : 'Your information was submitted successfully.'
        };

        modalService.showModal({
          size : 'sm'
        }, modalOptions);

        $scope.counselorEmail = undefined;
        $scope.formDisabled = false;
        vcRecaptchaService.reload();

      }, function(error) {
        var errMsg = error.data.errorMessage;
        if (errMsg === undefined) {
          $scope.errorSharing = 'There was an error processing your request. Please try again.';
          vcRecaptchaService.reload();
        } else {
          $scope.errorSharing = errMsg;
        }
        $scope.formDisabled = false;
      });
    }

  };
  
  $scope.trackScheduleClick = function() {
    console.log("tracking");
    $window.ga('send', 'pageview', url + '#BRING_ASVAB_FORM_SCHEDULE' );
  };
  
  $scope.imageZoom = function() {
    ASVABImageZoom.show({},{});
  };
  
  /* Slick Slider */
  var loadSlider = function (slider) {
    $(slider).slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: false
          }
        }, {
          breakpoint: 639,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });    
  };
  
  $scope.layoutComplete = function() {
    $timeout(function() {
      /* Load slider */      
      var slider = $('.responsive');    
      
      loadSlider(slider);      
      
      /*slick disable on click */
      $('.m-click,.d-click').click(function (e) {
        var parent = $(this).parents('.box-row');
        // toggle to show/hide all boxes
        $(parent).toggleClass('allbox');
        // get slider
        var this_slider = $(parent).find('.responsive');        
        // toggle slick
        if (this_slider.hasClass('slick-initialized')) {
          this_slider.slick('unslick');
        } else {
          loadSlider(this_slider);
        }
      });      
    }, 0);    
  }

  $scope.bringToSchool = function() {
    $scope.isLoggedIn = $rootScope.globals.currentUser !== undefined;
    if ($scope.isLoggedIn) {
      $location.url('/dashboard');
    } else {
      $location.url('/asvab-cep-at-your-school');
    }
  }

  OccuSalaryModalService.getStateSalary('15-1199').then(function(s) {
    $scope.stateSalary = s;
  }); 

  OccuSalaryModalService.getStateEntrySalary('15-1199').then(function(s) {
    $scope.stateEntrySalary = s;
  });
  
  OccuSalaryModalService.getBLSTitle('15-1199').then(function(s) {
    $scope.blsTitle = s;
  });

  OccuSalaryModalService.getNationalEntrySalary('15-1199').then(function(s) {
    $scope.nationalEntrySalary = s;
  });

  OccuSalaryModalService.getNationalSalary('15-1199').then(function(s) {
    $scope.nationalSalary = s;
  });

  $scope.setMeta = (function() {
    if ($location.path().indexOf('general-resources') >= 0) {
      $scope.seoTitle = 'Career Planning Resources | ASVAB Career Exploration Program';
      $scope.metaDescription = 'Find guides, templates, activities, and other career planning resources for students to assist in career planning from the ASVAB Career Exploration Program.';
    }
  })();

  /**
   * Due to Foundation not playing nicely with Angular, this code instance is
   * used in all controllers using the Foundation menu widget. So if this code
   * changes, all instances of this code needs to be updated as well.
   */
  angular.element(document).ready(function() {
    $('.dropdown.menu > li > a').click(function() {
      var li = $(this).closest('li');
      if (li.hasClass('open')) {
        li.removeClass('open');
      } else {
        $('.dropdown.menu > li').removeClass('open');
        li.addClass('open');
      }
      return false;
    });
  });
});
