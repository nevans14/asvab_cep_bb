cepApp.controller('CareerClusterOccupationController', [ '$scope', '$rootScope', '$route', '$cookieStore', '$location', '$window', 'careerClusterService', 'AuthenticationService', 'occupationResults', 'OccufindSearchService', 'careerClusterById', 'favorites', function($scope, $rootScope, $route, $cookieStore, $location, $window, careerClusterService, AuthenticationService, occupationResults, OccufindSearchService, careerClusterById, favorites) {
	
	$scope.filter = function (items, checkbox) {
		var size = 0;
		for (var i = 0; i < checkbox.length; i++) {
			if (items[checkbox[i].code]) {
				size++;
			}
		}
		return size;
	}

	$scope.metaTitle = careerClusterById.data && careerClusterById.data.seoTitle
		? careerClusterById.data.seoTitle
		: careerClusterById.data.ccTitle;
	
	$scope.metaDescription = careerClusterById.data && careerClusterById.data.seoDescription
		? careerClusterById.data.seoDescription
		: '';

	
	$scope.keyword = '';
	$scope.ccId = $route.current.params.ccId;
	$scope.occupationResults = occupationResults.data;
	$scope.careerCluster = careerClusterById.data;
	$scope.favorites = undefined;
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ;		
		$scope.favorites = favorites.data;		
	}	
	
	/**
	 * Keeps track of pagination
	 */
	$scope.pageChanged = function(e) {
		$scope.currentPage = e.currentPage;
	}
	
	/**
	 * Saves current search selections
	 */
	$scope.saveSearch = function () {
		var currentSearchSelections = {
			categoryItems	: $scope.categoryItems,
			skillItems		: $scope.skillItems,
			filterItems		: $scope.filterItems,
			keyword			: $scope.keyword,
			currentPage		: $scope.currentPage,
			skillImportance : $scope.skillImportance,
			skillView 		: $scope.skillView			
		};
		$window.sessionStorage.setItem('currentSearchSelections', JSON.stringify(currentSearchSelections));
	};
	
	/**
	 * Reset tab index
	 */
	$scope.tab = {};
	$scope.tab.index = 0; //'hi'
	$scope.resetTabs = function () {
		$scope.skillView = 'hi';
		$scope.tab.index = 0;
		$scope.currentPage = 1;
		$scope.itemsPerPage = 20;
	}
	
	/**
	 * Clears selections
	 */
	$scope.clearSelections = function() {
		// reset keyword,skillImportance, and tabs
		$scope.keyword = '';		
		$scope.skillImportance = '';		
		$scope.resetTabs();
		// reset checkboxes
		$scope.categoryItems = {
			'B': false,
			'G': false,
			'S': false,
			'H': false,
			'A': false
		};
		$scope.filterItems = {
			'R': false,
			'I': false,
			'A': false,
			'S': false,
			'E': false,
			'C': false
		};
		$scope.skillItems = {
			'V': false,
			'M': false,
			'S': false
		};
		// reset checkbox checked markers
		$scope.interestChecked = 0;
		$scope.skillChecked = 0;	
		// save
		$scope.saveSearch();
	}

	/**
	 * Setting up views
	 */
	$scope.skillImportance = '';
	$scope.skillView = 'hi';
	$scope.tab = {};
	
	$scope.skillViewSelected = function(selected) {
		$scope.skillView = selected;
		$scope.saveSearch();
		$scope.currentPage = 1;
		$scope.tab.index = 
			$scope.skillView == 'hi' ? 0 : 
			$scope.skillView == 'md' ? 1 : 
			$scope.skillView == 'lw' ? 2 : 
			0;
	}
	/**
	 * Pagination
	 */
	$scope.totalItems = $scope.occupationResults.length;
	$scope.currentPage = 1;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 20;

	/**
	 * Redirects user to the occupation details page
	 */
	$scope.selectedOccupation = function(onetSoc, title, stem, bright, green, hot, interestCdOne, interestCdTwo, interestCdThree) {
		OccufindSearchService.selectedOnetSoc.push(onetSoc);
		OccufindSearchService.selectedOcupationTitle = title;
		OccufindSearchService.occupationGreen = green;
		OccufindSearchService.occupationStem = stem;
		OccufindSearchService.occupationBright = bright;
		OccufindSearchService.occupationHot = hot;
		OccufindSearchService.occupationInterestOne = interestCdOne;
		OccufindSearchService.occupationInterestTwo = interestCdTwo;
		OccufindSearchService.occupationInterestThree = interestCdThree;
		$location.url('/occufind-occupation-details/' + onetSoc);
	};

	/**
	 *  For Interests:
	 * $scope.checkboxInterest is for populating interest checkboxes on the form
	 * $scope.filterItems is for keeping track of which interest has been selected
	 * $scope.limitTwo is for allowing up to two interests to be selected at a time
	 * $scope.interestFilter is for dynamically filtering the result set based on what interest has been selected
	 */
	$scope.checkboxInterest = [{
		name : 'Realistic',
		code : 'R'
	}, {
		name : 'Investigative',
		code : 'I'
	}, {
		name : 'Artistic',
		code : 'A'
	}, {
		name : 'Social',
		code : 'S'
	}, {
		name : 'Enterprising',
		code : 'E'
	}, {
		name : 'Conventional',
		code : 'C'
	} ];

	$scope.filterItems = {
		'A': false,
		'C': false,
		'E': false,
		'I': false,
		'R': false,
		'S': false
	};
	
	$scope.interestLimit = 2;
	$scope.interestChecked = 0;
	$scope.limitTwo = function(item) {
		if ($scope.filterItems[item.code])
			$scope.interestChecked++;
		else
			$scope.interestChecked--;
	}
	
	$scope.interestFilter = function(item) {
		for (var i = 0; i < $scope.checkboxInterest.length; i++) {
			if ($scope.filterItems[$scope.checkboxInterest[i].code]) {
				switch ($scope.checkboxInterest[i].code) {
					case 'A':
						if (item.interestCdOne != 'A' && item.interestCdTwo != 'A' && item.interestCdThree != 'A') return false;
						break;
					case 'C':
						if (item.interestCdOne != 'C' && item.interestCdTwo != 'C' && item.interestCdThree != 'C') return false;
						break;
					case 'E':
						if (item.interestCdOne != 'E' && item.interestCdTwo != 'E' && item.interestCdThree != 'E') return false;
						break;
					case 'I':
						if (item.interestCdOne != 'I' && item.interestCdTwo != 'I' && item.interestCdThree != 'I') return false;
						break;
					case 'R':
						if (item.interestCdOne != 'R' && item.interestCdTwo != 'R' && item.interestCdThree != 'R') return false;
						break;
					case 'S':
						if (item.interestCdOne != 'S' && item.interestCdTwo != 'S' && item.interestCdThree != 'S') return false;
						break;
				}
			}
		}
		return true;
	}
	
	/**
	 * For Skills:
	 * $scope.checkboxSkill is for populating skill checkboxes on the form
	 * $scope.skillItems is for keeping track which skill has been selected
	 * $scope.limitOne is for allowing only one skill to be selected at a time
	 * $scope.onSkillChange sets the skillImportance based on which skill was selected. If a skill was un-selected, then resets the tabs
	 */ 
	$scope.checkboxSkill = [{
		name: 'Verbal',
		code: 'V'
	}, {
		name: 'Math',
		code: 'M'
	}, {
		name: 'Science/Technical',
		code: 'S'
	}];
	$scope.skillItems = {
		'V': false,
		'M': false,
		'S': false
	};
	
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.limitOne = function (item) {
		if ($scope.skillItems[item.code])
			$scope.skillChecked++;
		else 
			$scope.skillChecked--;
	};
	
	$scope.onSkillChange = function(item) {
		$scope.skillImportance = ($scope.skillItems[item.code] ? item.code : '');
		if ($scope.skillImportance == '')
			$scope.resetTabs();		
		$scope.saveSearch();
	};
	
	/**
	 * For Categories
	 * $scope.checkboxCategory is for populating category checkboxes on the form
	 * $scope.categoryItems is for keeping track which category has been selected
	 * $scope.categoryFilter is for dynamically filtering the result set based on what category has been selected
	 */
	$scope.checkboxCategory = [{
		name: 'Bright Outlook',
		code: 'B',
		iconClass: 'icon hot',
		rel: 'BRIGHT'
	// }, {
	// 	name: 'Green Careers',
	// 	code: 'G',
	// 	iconClass: 'icon green',
	// 	rel: 'GREEN'
	}, {
		name: 'STEM Careers',
		code: 'S',
		iconClass: 'icon stem',
		rel: 'STEM'
	}, {
		name: 'Hot Military Careers',
		code: 'H',
		iconClass: 'icon-hotjob',
		rel: 'HOT'
	}, {
		name: 'Available in the Military',
		code: 'M'
	}];
	$scope.categoryItems = {
		'B': false,
		'G': false,
		'S': false,
		'H': false,
		'M': false
	};
	
	$scope.categoryFilter = function(item) {
		for (var i = 0; i < $scope.checkboxCategory.length; i++) {
			if ($scope.categoryItems[$scope.checkboxCategory[i].code]) {
				switch ($scope.checkboxCategory[i].code) {
					case 'B':
						if (!item.isBrightOccupation) return false;
						break;
					case 'H':
						if (!item.isHotOccupation) return false;
						break;
					case 'S':
						if (!item.isStemOccupation) return false;
						break;
					case 'G':
						if (!item.isGreenOccupation) return false;
						break;
					case 'M':
						if (!item.isAvailableInTheMilitary) return false;
						break;
				}
			}
		}		
		return true;
	}
	
	$scope.search = function (row) {
		return (angular.lowercase(row.occupationTitle).indexOf(angular.lowercase($scope.keyword) || '') !== -1);
				//||angular.lowercase(row.alternateTitles).indexOf(angular.lowercase($scope.keyword) || '') !== -1);
	}
	
	/**
	 * initialize scoped variables
	 */
	var currentSearchSelections = angular.fromJson($window.sessionStorage.getItem('currentSearchSelections'));
	$scope.skillImportance = currentSearchSelections ? currentSearchSelections.skillImportance : $scope.skillImportance;
	$scope.skillView = currentSearchSelections ? currentSearchSelections.skillView : $scope.skillView;
	$scope.skillItems = currentSearchSelections ? currentSearchSelections.skillItems : $scope.skillItems;
	$scope.filterItems = currentSearchSelections ? currentSearchSelections.filterItems : $scope.filterItems;
	$scope.categoryItems = currentSearchSelections ? currentSearchSelections.categoryItems : $scope.categoryItems;	
	$scope.keyword = currentSearchSelections ? currentSearchSelections.keyword : $scope.keyword;
	$scope.currentPage = currentSearchSelections ? currentSearchSelections.currentPage : $scope.currentPage;
	$scope.tab.index = 
		$scope.skillView == 'hi' ? 0 : 
		$scope.skillView == 'md' ? 1 : 
		$scope.skillView == 'lw' ? 2 : 
		0; // Set default active tab.
	/**
	 * initialize how many items have been checked
	 */
	$scope.interestChecked = $scope.filter($scope.filterItems, $scope.checkboxInterest);
	$scope.skillChecked = $scope.filter($scope.skillItems, $scope.checkboxSkill);
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

	$scope.logout = function() {
		AuthenticationService.ClearCredentials();
	};
} ]);

/**
 * This filters careers by skill importance.
 */
cepApp.filter('rangeFilter', function() {
	return function(items, info) {
		var filtered = [];

		if (info.skillImportance == 'M') {
			if (info.view == 'hi') {
				angular.forEach(items, function(item) {
					if ((item.mathSkill >= 4 && item.mathSkill <= 5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'md') {
				angular.forEach(items, function(item) {
					if ((item.mathSkill >= 2.5 && item.mathSkill <= 3.5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'lw') {
				angular.forEach(items, function(item) {
					if ((item.mathSkill >= 0 && item.mathSkill <= 2)) {
						filtered.push(item);
					}
				});
			}
		} else if (info.skillImportance == 'V') {
			if (info.view == 'hi') {
				angular.forEach(items, function(item) {
					if ((item.verbalSkill >= 4 && item.verbalSkill <= 5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'md') {
				angular.forEach(items, function(item) {
					if ((item.verbalSkill >= 2.5 && item.verbalSkill <= 3.5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'lw') {
				angular.forEach(items, function(item) {
					if ((item.verbalSkill >= 0 && item.verbalSkill <= 2)) {
						filtered.push(item);
					}
				});
			}
		} else if (info.skillImportance == 'S') {
			if (info.view == 'hi') {
				angular.forEach(items, function(item) {
					if ((item.sciTechSkill >= 4 && item.sciTechSkill <= 5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'md') {
				angular.forEach(items, function(item) {
					if ((item.sciTechSkill >= 2.5 && item.sciTechSkill <= 3.5)) {
						filtered.push(item);
					}
				});
			} else if (info.view == 'lw') {
				angular.forEach(items, function(item) {
					if ((item.sciTechSkill >= 0 && item.sciTechSkill <= 2)) {
						filtered.push(item);
					}
				});
			}
		} else {
			return items;
		}

		return filtered;
	};
});