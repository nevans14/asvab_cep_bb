cepApp.controller('CareerClusterPathwayController', [ '$scope', '$location', 'resource', '$rootScope', '$window', '$route', 'ccPathways', 'awsMediaUrl', function($scope, $location, resource, $rootScope, $window, $route, ccPathways, awsMediaUrl) {

	$scope.urlPrefix = resource + 'static/';
	$scope.urlSuffix = '_IMG.jpg';

	var loggedIn = $rootScope.globals.currentUser;

	$scope.url = 'occufind-occupation-details/';

	$scope.back = function() {
		$window.history.back();
	}
	
	$scope.scrollTop = function(){
		$window.scrollTo(0, 0);
	}

	$scope.ccPathwayId = $route.current.params.ccPathwayId;
	$scope.awsMediaUrl = awsMediaUrl;

	(function() {
		$scope.showMore = [];
		$scope.ccPathways = ccPathways.data;
		$scope.ccPathways.pathways.forEach(function(pathway, i) {
			$scope.showMore[i] = false;
		})
	})()

	$scope.toggleShowOccupations = function(index, showMore) {
		$scope.showMore[index] = showMore;
	}

	// Using SEO title and description from API
	// $scope.setMeta = (function() {
	// 	var pathwayArray = $location.path().split('-');
	// 	switch (pathwayArray[pathwayArray.length - 1]) {
	// 		case '1':
	// 			$scope.seoTitle = 'Career Path | Architecture and Construction | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Architecture & Construction | There are 3 distinct career paths in the architecture and construction field. Find the right career path for you.';
	// 			break;
	// 		case '2':
	// 			$scope.seoTitle = 'Career Path | Arts, Audio/Video Technology & Communications | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Arts, Audio/Video Technology & Communications | Search career pathways in Arts, Audio/Video Technology & Communications.';
	// 			break;
	// 		case '3':
	// 			$scope.seoTitle = 'Career Path | Business Management and Administration | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Business Management and Administration | There are 5 career pathways in business management and administration. Search these occupations.';
	// 			break;
	// 		case '4':
	// 			$scope.seoTitle = 'Career Path | Education and Training | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Education and Training | There are 3 distinct career paths in the education and training field. Find a career pathway in education and training.';
	// 			break;
	// 		case '5':
	// 			$scope.seoTitle = 'Career Path | Hospitality and Tourism | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Hospitality and Tourism | There are 4 career paths in the hospitality and tourism field. Find a career pathway in hospitality and tourism.';
	// 			break;
	// 		case '6':
	// 			$scope.seoTitle = 'Career Path | Manufacturing | ASVAB Career Exploration Program';
	// 			$scope.metaDescription = 'Career Path Manufacturing | There are 6 career paths in the manufacturing field. Find a career pathway in manufacturing.';
	// 			break;
	// 		case '7':
	// 			$scope.seoTitle = 'Career Path | Marketing | ASVAB Career Exploration Program';
	// 			$scope.metaDescription = 'Career Path Marketing | There are 5 career paths in the marketing field. Learn about occupations and find a career pathway in marketing.';
	// 			break;
	// 		case '8':
	// 			$scope.seoTitle = 'Career Path | Science, Technology, Engineering & Math | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Science, Technology, Engineering and Math (STEM) | There are distinct career pathways in STEM. Learn about occupations in STEM.';
	// 			break;
	// 		case '9':
	// 			$scope.seoTitle = 'Career Path | Agriculture, Food & Natural Resources | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Agriculture, Food & Natural Resources | Learn about career pathways in Agriculture, Food & Natural Resources.';
	// 			break;
	// 		case '10':
	// 			$scope.seoTitle = 'Career Path | Finance | ASVAB Career Exploration Program';
	// 			$scope.metaDescription = 'Career Path Finance | Careers in finance offer 5 different industry pathways. Learn more about occupations and options in finance. ';
	// 			break;
	// 		case '11':
	// 			$scope.seoTitle = 'Career Path | Government and Public Administration | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Government and Public Administration | There are 7 career pathways in Government and Public Administration. Learn more about these careers.';
	// 			break;
	// 		case '12':
	// 			$scope.seoTitle = 'Career Path | Health Science | ASVAB Career Exploration Program';
	// 			$scope.metaDescription = 'Career Path Health Sciences | The health science industry has 5 career paths. Find the right career path for you.';
	// 			break;
	// 		case '13':
	// 			$scope.seoTitle = 'Career Path | Human Services | ASVAB Career Exploration Program';
	// 			$scope.metaDescription = 'Career Path Human Services | There are 5 distinct career paths in the human services field. Learn more about occupations in health sciences.';
	// 			break;
	// 		case '14':
	// 			$scope.seoTitle = 'Career Path | Law, Public Safety, Corrections & Security | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Law, Public Safety, Corrections & Security | There are 5 career pathways in Law, Public Safety, Corrections & Security. Search these occupations.';
	// 			break;
	// 		case '15':
	// 			$scope.seoTitle = 'Career Path | Transportation, Distribution & Logistics | ASVAB CEP';
	// 			$scope.metaDescription = 'CCareer Path Transportation, Distribution and Logistics  | There are 7 career pathways in Transportation, Distribution and Logistics. Search these occupations.';
	// 			break;
	// 		case '16':
	// 			$scope.seoTitle = 'Career Path | Information Technology | ASVAB CEP';
	// 			$scope.metaDescription = 'Career Path Information Technology | There are 4 career paths in the information technology field. Learn more about occupations in information technology.';
	// 			break;
	// 	}
	// })()

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);