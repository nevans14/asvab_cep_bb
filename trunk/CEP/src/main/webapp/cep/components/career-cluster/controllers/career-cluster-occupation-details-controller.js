cepApp.controller('CareerClusterOccupationDetailsController', [ '$scope', '$location', 'careerClusterService', 'occupationTitleDescription', 'stateSalary', 'AuthenticationService', 'skillImportance', 'tasks', 'occupationInterestCodes', 'educationLevel', 'relatedOccupations', 'militaryResources', 'oohResources', '$q', 'UtilityService', function($scope, $location, careerClusterService, occupationTitleDescription, stateSalary, AuthenticationService, skillImportance, tasks, occupationInterestCodes, educationLevel, relatedOccupations, militaryResources, oohResources, $q, UtilityService) {

	$q.all([ occupationTitleDescription, skillImportance, stateSalary, tasks ]).then(function successCallback(response) {
		$scope.occupationTitle = occupationTitleDescription.data.title;
		$scope.occupationDescription = occupationTitleDescription.data.description;
		$scope.skillImportance = skillImportance.data;
		$scope.stateSalary = stateSalary.data;
		$scope.tasks = tasks.data;
		$scope.occupationInterestCodes = occupationInterestCodes.data;
		$scope.educationLevel = educationLevel.data;
		$scope.relatedOccupations = relatedOccupations.data;
		$scope.militaryResources = militaryResources.data;
		$scope.oohResources = oohResources.data;

		$scope.selectedCareerCluster = careerClusterService.selectedCareerCluster;
		$scope.occupationTitle = careerClusterService.selectedOcupationTitle;
		$scope.absUrl = UtilityService.getCanonicalUrl();

		/**
		 * Skill rating
		 */
		angular.element(document).ready(function() {
			$('.customrating').each(function() {
				var rating = $(this).data('customrating');
				var width = $(this).width() / 5 * rating;
				if (rating.toString().indexOf('.') !== -1) {
					width += 1;
				}
				$(this).width(width);
			});
		});

		/**
		 * Salary map implementation
		 */
		var outerArray = [];
		var innerArray = [ 'State', 'AverageSalary' ];
		outerArray[0] = innerArray;
		for (var i = 0; i < $scope.stateSalary.length; i++) {
			var innerArray = [ 'US-' + $scope.stateSalary[i].state, $scope.stateSalary[i].avgSalary ];
			outerArray[i + 1] = innerArray;
		}

		var chart1 = {};
		chart1.type = "GeoChart";
		chart1.data = outerArray;

		chart1.options = {
			width : 406,
			height : 297,
			region : "US",
			resolution : "provinces",
			colors : [ '#e6eeff', '#0055ff' ]
		};

		/*
		 * chart1.formatters = { number : [{ columnNum: 1, pattern: "$ #,##0.00" }] };
		 */

		$scope.chart = chart1;

		$scope.logout = function() {
			AuthenticationService.ClearCredentials();
		};
		

		/**
		 * Pie chart Implementation
		 */
		$scope.chartObject = {};
		var col = [ {
			id : "e",
			label : "Education",
			type : "string"
		}, {
			id : "v",
			label : "Value",
			type : "number"
		} ];
		var row = [];

		for (var i = 0; i < $scope.educationLevel.length; i++) {
			var c = {
				c : [ {
					v : $scope.educationLevel[i].educationDesc
				}, {
					v : $scope.educationLevel[i].educationLvlVal
				} ]
			};
			row.push(c);
		}

		$scope.chartObject.data = {
			"cols" : col,
			"rows" : row
		};

		$scope.chartObject.type = "PieChart";
		$scope.chartObject.options = {
			'title' : 'EducationLevel',
			colors : [ '#0039b3', '#0051ff', '#4d85ff', '#99b9ff', '#e6eeff', '#001033', '#002880', '#0041cc', '#1a62ff', '#6696ff' ]
		};

		$scope.chartPie = $scope.chartObject;
	}, function errorCallback(response) {
		alert("error, try refreshing");
	});

} ]);
