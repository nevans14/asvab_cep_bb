cepApp.factory('careerClusterRestFactory', [ '$http', 'appUrl', 'awsApiFullUrl', function($http, appUrl, awsApiFullUrl) {

	var careerClusterRestFactory = {};

	careerClusterRestFactory.getCareerCluster = function() {
		return $http.get(appUrl + 'careerCluster/careerClusterList');
	}

	careerClusterRestFactory.getCareerClusterOccupation = function(ccId) {
		return $http.get(appUrl + 'careerCluster/careerClusterOccupationList/' + ccId);
	}
	
	careerClusterRestFactory.getCareerClusterOccupations = function(ccId) {
		return $http.get(appUrl + 'careerCluster/CareerClusterOccupations/' + ccId + '/');
	}
	
	careerClusterRestFactory.getCareerClusterById = function(ccId) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/careerCluster/CareerClusterById/' + ccId + '/'});
	}
	
	careerClusterRestFactory.getStateSalary = function(onetSocTrimmed){
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/StateSalary/' + onetSocTrimmed + '/'});
	}
	
	careerClusterRestFactory.getStateEntrySalary = function(onetSocTrimmed) {
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/StateEntrySalary/' + onetSocTrimmed + '/'});
	}
	
	careerClusterRestFactory.getSkillImportance = function(onetSoc){
		return $http({method: 'GET', url: awsApiFullUrl + '/api/careerCluster/occupationSkillImportance/' + onetSoc + '/'});
	}
	
	careerClusterRestFactory.getTasks = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/careerClusterOccupationTaskList/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getOccupationInterestCodes = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/occupationInterestCodes/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getEducationLevel = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/occupationEducationLevel/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getRelatedOccupations = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/relatedOccupation/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getMilitaryResources = function(onetSoc){
		return $http({method: 'GET', url: awsApiFullUrl + '/api/occufind/militaryCareerResource/' + onetSoc + '/'});
	}

	careerClusterRestFactory.getOOHResources = function(onetSoc){
		return $http({method:'GET', url: awsApiFullUrl + '/api/occufind/OOHResource/' + onetSoc + '/'});
	}

	return careerClusterRestFactory;

} ]);