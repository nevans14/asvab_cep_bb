cepApp.service('careerClusterService', function() {
	var selectedCareerCluster;

	this.getSelectedCareerCluster = function() {
		return selectedCareerCluster;
	}

	var careerClusters;

	this.getCareerClusters = function() {
		return careerClusters;
	}

	var selectedOnetSoc;

	this.getSelectedOnetSoc = function() {
		return selectedOnetSoc;
	}
	
	var selectedOcupationTitle;
	
	this.getSelectedOcupationTitle = function(){
		return selectedOcupationTitle;
	}
});