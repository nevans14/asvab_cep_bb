cepApp.controller('CareerClusterController', [ '$scope', '$rootScope', '$location', '$window', 'career', 'careerClusterService', 'OccufindSearchService', function($scope, $rootScope, $location, $window, career, careerClusterService, OccufindSearchService) {

	$scope.career = career.data;
	careerClusterService.careerClusters = career.data;

	$scope.isLoggedIn = $rootScope.globals.currentUser !== undefined;
	$scope.selectedCareerCluster = function(ccId) {
		careerClusterService.selectedCareerCluster = ccId;
		$location.url('/career-cluster-occupation-results/' + ccId);
	};

	$scope.seoTitle = 'Career Clusters | Occupation Search | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Career Clusters | Search 16 career clusters to find occupations based on similar skill sets, interests, abilities, and activities.';

	/**
	 * returns how many checkboxes have been selected
	 */
	$scope.filter = function (items, checkbox) {
		var size = 0;
		for (var i = 0; i < checkbox.length; i++) {
			if (items[checkbox[i].code]) {
				size++;
			}
		}
		return size;
	};
	
	/**
	 * initialize scope variables
	 */
	$scope.keyword = '';	
	$scope.skillImportance = '';
	$scope.skillView = 'hi';
	/**
	 * Reset tab index
	 */
	$scope.tab = {};
	$scope.tab.index = 0; //'hi'
	$scope.resetTabs = function () {
		$scope.skillView = 'hi';
		$scope.tab.index = 0;
		$scope.currentPage = 1;
		$scope.itemsPerPage = 20;
		$scope.saveSearch();
	}
	/**
	 * On change on anything, save to search criteria
	 */
	$scope.saveSearch = function() {
		var currentSearchSelections = {
			categoryItems 	: $scope.categoryItems,
			skillItems 		: $scope.skillItems,
			filterItems 	: $scope.filterItems,
			keyword			: $scope.keyword,
			currentPage 	: $scope.currentPage,
			skillImportance : $scope.skillImportance,
			skillView 		: $scope.skillView			
		}
		$window.sessionStorage.setItem('currentSearchSelections', JSON.stringify(currentSearchSelections));
	};	
	/**
	 * Clear selections
	 */
	$scope.clearSelections = function () {
		// reset keyword, skillImportance
		$scope.keyword = '';
		$scope.skillImportance = '';
		// reset checkboxes
		$scope.filterItems = {
			'R' : false,
			'I' : false,
			'A' : false,
			'S' : false,
			'E' : false,
			'C' : false
		};
		$scope.categoryItems = {
			'B' : false,
			'S' : false,
			'G' : false,
			'H' : false,
			'M' : false
		};
		$scope.skillItems = {
			'V' : false,
			'M' : false,
			'S' : false
		}
		$scope.itemsPerPage = 20;
		$scope.currentPage = 1;
		// reset checkbox checked markers
		$scope.checked = 0;
		$scope.skillChecked = 0;
		// save
		$scope.saveSearch();
	}
	/**
	 * For Skills:
	 * $scope.checkboxSkill is for populating skill checkboxes on the form
	 * $scope.skillItems is for keeping track which skill has been selected
	 */ 
	$scope.checkboxSkill = [ {
		name : 'Verbal',
		code : 'V'
	}, {
		name : 'Math',
		code : 'M'
	}, {
		name : 'Science/Technical',
		code : 'S'
	} ];
	
	$scope.skillItems = {
		'V' : false,
		'M' : false,
		'S' : false
	};
	/**
	 * limitOne - tracks how many skills has been selected. Only one skill can be selected at a time
	 */
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.limitOne = function(item) {
		if ($scope.skillItems[item.code])
			$scope.skillChecked++;
		else
			$scope.skillChecked--;
	}
	/**
	 * OnSkillChange - sets the skillImportance based on which skill was selected. If a skill was un-selected, then resets the tabs
	 */
	$scope.onSkillChange = function(item) {		
		$scope.skillImportance = ($scope.skillItems[item.code] == true ? item.code : '');
		if ($scope.skillImportance)
			$scope.resetTabs();
	}
	/**
	 * For Categories
	 * $scope.checkboxCategory is for populating category checkboxes on the form
	 * $scope.categoryItems is for keeping track which category has been selected
	 */
	$scope.checkboxCategory = [{
		name: 'Bright Outlook',
		code: 'B',
		iconClass: 'icon hot',
		rel: 'BRIGHT'
	}, {
		name: 'Green Careers',
		code: 'G',
		iconClass: 'icon green',
		rel: 'GREEN'
	}, {
		name: 'STEM Careers',
		code: 'S',
		iconClass: 'icon stem',
		rel: 'STEM'
	}, {
		name: 'Hot Military Careers',
		code: 'H',
		iconClass: 'icon-hotjob',
		rel: 'HOT'
	}, {
		name: 'Available in the Military',
		code: 'M'
	}];
	
	$scope.categoryItems = {
		'S': false,
		'B': false,
		'G': false,
		'H': false,
		'M': false
	};	

	/**
	 * For Interest
	 * $scope.checkboxInterest is for populating interest checkboxes on the form
	 * $scope.filterItems is for keeping track of which interest has been selected
	 */
	$scope.checkboxInterest = [{
		name : 'Realistic',
		code : 'R'
	}, {
		name : 'Investigative',
		code : 'I'
	}, {
		name : 'Artistic',
		code : 'A'
	}, {
		name : 'Social',
		code : 'S'
	}, {
		name : 'Enterprising',
		code : 'E'
	}, {
		name : 'Conventional',
		code : 'C'
	} ];
	
	$scope.filterItems = {
		'A': false,
		'C': false,
		'E': false,
		'I': false,
		'R': false,
		'S': false
	};

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 2;
	$scope.checked = 0;
	$scope.limitTwo = function(item) {
		if ($scope.filterItems[item.code])
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Search button sets up the search service.
	 */
	$scope.occufindSearch = function() {
		$location.path('/occufind-occupation-search-results');
	};
	
	/**
	 * initialize scope variables from session
	 */
	var currentSearchSelections = angular.fromJson($window.sessionStorage.getItem('currentSearchSelections'));
	$scope.skillImportance = currentSearchSelections ? currentSearchSelections.skillImportance : $scope.skillImportance;
	$scope.skillView = currentSearchSelections ? currentSearchSelections.skillView : $scope.skillView;
	$scope.skillItems = currentSearchSelections ? currentSearchSelections.skillItems : $scope.skillItems;
	$scope.filterItems = currentSearchSelections ? currentSearchSelections.filterItems : $scope.filterItems;
	$scope.categoryItems = currentSearchSelections ? currentSearchSelections.categoryItems : $scope.categoryItems;	
	$scope.keyword = currentSearchSelections ? currentSearchSelections.keyword : $scope.keyword;
	$scope.currentPage = currentSearchSelections ? currentSearchSelections.currentPage : $scope.currentPage;
	$scope.tab.index = 
		$scope.skillView == 'hi' ? 0 : 
		$scope.skillView == 'md' ? 1 : 
		$scope.skillView == 'lw' ? 2 : 
		0; // Set default active tab.
	/**
	 * initialize how many items have been checked
	 */
	$scope.checked = $scope.filter($scope.filterItems, $scope.checkboxInterest);
	$scope.skillChecked = $scope.filter($scope.skillItems, $scope.checkboxSkill);
	/**
	 * initialize to show categories if any has been selected
	 */
	$scope.categoryShow = $scope.categoryItems.B || $scope.categoryItems.G || $scope.categoryItems.S || $scope.categoryItems.H || $scope.categoryItems.M;
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
} ]);