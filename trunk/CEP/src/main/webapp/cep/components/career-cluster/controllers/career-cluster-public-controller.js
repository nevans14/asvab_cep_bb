cepApp.controller('CareerClusterPublicController', [ '$scope', '$location', 'career', 'careerClusterService', 'AuthenticationService', 'OccufindSearchService', function($scope, $location, career, careerClusterService, AuthenticationService, OccufindSearchService) {

	$scope.career = career.data;
	careerClusterService.careerClusters = career.data;

	$scope.selectedCareerCluster = function(ccId) {
		careerClusterService.selectedCareerCluster = ccId;
		$location.url('/career-cluster-pathway-' + ccId);
	}
	
	$scope.logout = function() {
		AuthenticationService.ClearCredentials();
	};

	$scope.seoTitle = 'Career Clusters | Career Search | ASVAB Career Exploration Program';
	$scope.metaDescription = 'Explore Career Clusters | Students explore 16 career clusters to find occupations based on similar skill sets, interests, and activities. ';
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);