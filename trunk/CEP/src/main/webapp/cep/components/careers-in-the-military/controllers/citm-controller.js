cepApp.controller("CitmController", function ($scope, $window, $sce, $location) {
	$scope.citmWebLink = undefined;
	
	var sessionLink = $window.sessionStorage.getItem('citmWebLink');
	if (sessionLink) {
		$scope.citmWebLink = $sce.trustAsResourceUrl(sessionLink);
	} else {
		// redirect to home
		console.log('no citm web link detected: redirecting to homepage')
		$location.path('home');
	}
	
	$scope.backFromHistory = function () {
		var previousPage = $window.sessionStorage.getItem('previousPage');
		if (previousPage) {
			location.href = previousPage;
		} else {
			window.history.back();
		}
	}
});