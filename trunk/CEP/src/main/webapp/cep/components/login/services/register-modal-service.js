cepApp.service('RegisterModalService', ['$uibModal', '$location', '$http', '$window', '$timeout', 'AuthenticationService', 'appUrl', 'modalService', 'ssoUrl', 'recaptchaKey', 'vcRecaptchaService', function ($uibModal, $location, $http, $window, $timeout, AuthenticationService, appUrl, modalService, ssoUrl, recaptchaKey,vcRecaptchaService) {
	var BASE_REGISTER_URL = appUrl + 'applicationAccess/';
	
	// endpoint enum
	var serviceEndpoint = {
		'Register': 'register',
		'TeacherRegister': 'teacherRegistration'
	};
	
	var modalDefaults = {
		animation: true,
		templateUrl: '/CEP/cep/components/login/page-partials/register-modal.html',
		size: 'md',
		windowClass: 'new-modal'
	};
	var modalOptions = {
		headerText: ''
	};
	
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};
	
	this.show = function(customModalDefaults, customModalOptions, accessCode) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', 'mergeAccountModalService', function($scope, $uibModalInstance, mergeAccountModalService) {
				$scope.recaptchaKey = recaptchaKey;
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.clearRegistrationLabel = function() {
					$scope.registrationLabel = '';
				};
				
				$scope.checkForPromoCode = function () {
					$scope.isUsingPromoCode = false;
					$scope.isStudentCode = false;
					var accessCode = $scope.registerAccessCode;
					if (accessCode == null) {
						return;
					}

					var url = BASE_REGISTER_URL + 'testTeacherAccessCode';
					$http.post(url, {accessCode: accessCode.toUpperCase()}).then(function(res) {
						if(res.data.statusNumber === 0) {
							$scope.isUsingPromoCode = true;
						} else if (accessCode.length === 10 && !angular.isNumber(accessCode[accessCode.length - 1])) {
							$scope.isStudentCode = true;
						}
					})
				};
				
				$scope.mergeAccounts = false;
				$scope.isUsingPromoCode = (accessCode) ? true : false;
				$scope.registerAccessCode = ($scope.isUsingPromoCode) ? accessCode : undefined;
				$scope.registerEmail = undefined;
				$scope.registerStudentId = undefined;
				$scope.registerSubject = undefined;
				$scope.registerPassword1 = undefined;
				$scope.registerPassword2 = undefined;
				$scope.optedInCommunications = true;
				$scope.isStudentCode = false;				
				$scope.disableForm = false;
				$scope.firstName = undefined;
				$scope.lastName = undefined;
				$scope.phoneNumber = undefined;
				$scope.functionOther = undefined;
				$scope.schoolName = undefined;
				$scope.schoolZipCode = undefined;
				$scope.schoolState = undefined;
				$scope.essName = undefined;
				$scope.eventName = undefined;
				$scope.functionId = undefined;
				$scope.conferenceId = undefined;
				$scope.functions = [];
				$scope.conferences = [];

				var url = BASE_REGISTER_URL + 'get-function-list';
				$http.get(url).then(function(res) {
					$scope.functions = res.data;
				})

				url = BASE_REGISTER_URL + 'get-conference-list';
				$http.get(url).then(function(res) {
					$scope.conferences = res.data;
				})

				$scope.RegisterStudent = function() {
					// disable the form
					$scope.disableForm = true;
					// Register Modal
					var data = {
						email					: $scope.registerEmail,
						accessCode 				: $scope.registerAccessCode,
						password 				: $scope.registerPassword2,
						resetKey 				: "",
						studentId				: $scope.registerStudentId,
						subject					: $scope.registerSubject,
						optedInCommunications	: $scope.optedInCommunications,
						recaptchaResponse		: $scope.recaptchaResponse,
						firstName				: $scope.firstName,
						lastName 				: $scope.lastName,
						phoneNumber 			: $scope.phoneNumber,
						functionOther 			: $scope.functionOther,
						schoolName 				: $scope.schoolName,
						schoolZipCode 			: $scope.schoolZipCode,
						schoolState 			: $scope.schoolState,
						essName 				: $scope.essName,
						eventName 				: $scope.eventName,
						conferenceId 			: parseInt($scope.conferenceId, 10),
						functionId				: parseInt($scope.functionId, 10),
					};

					if (!data.recaptchaResponse) {
						$scope.registrationLabel = 'Recaptcha is required!';
						$scope.disableForm = false;
						return;
					}

					var pass1 = $scope.registerPassword1;
					var pass2 = $scope.registerPassword2;
					
					if (pass1.length <= 7) {
						$scope.registrationLabel = "Password too short! Please have a password that is 8 characters or more.";
						$scope.disableForm = false;
						return;
					}
					
					if (pass1 !== pass2) {
						$scope.registrationLabel = "Passwords do not match!";
						$scope.disableForm = false;
						return;
					}					
					
					var url = BASE_REGISTER_URL + ($scope.isUsingPromoCode ? 
							serviceEndpoint.TeacherRegister : 
							serviceEndpoint.Register);
					if ($scope.isUsingPromoCode) {
						data.password = $scope.registerPassword2;
						$scope.post(url, data);
					} else {
						$scope.post(url, data);
					}						
				};
				
				/**
				 * Posts to service
				 */
				$scope.post = function(url, data) {
					$http.post(url, data).then(function(res) {
						$scope.registrationLoginStatus = res.data;
						var userData = res.data;
						if ($scope.registrationLoginStatus.statusNumber === 0) {	// success
							// Clear all the data 
							$scope.registerAccessCode = '';
							// $scope.registerEmail =  '';
							$scope.registerPassword1 =  '';
							$scope.registerPassword2 =  '';
							$scope.registerStudentId = '';
							$scope.registerSubject = '';
							//  Now log in the user ....
							AuthenticationService.SetCredentials(
									userData.user_id,
									userData.role,
									userData.accessCode,
									userData.schoolCode,
									userData.schoolName,
									userData.city,
									userData.state,
									userData.zipCode,
									userData.mepsId,
									userData.mepsName);
							// Close Dialog
							$uibModalInstance.dismiss('cancel');
							// SSO: login to the CTM website.
							$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
						} else { // Error
							// if the user already exists but not using a promo account, merge accounts
							if ($scope.registrationLoginStatus.statusNumber === 1003 && !$scope.isUsingPromoCode) {
								this.email = $scope.registerEmail;
								$scope.showMergeModal();
							} else {
								$scope.registrationLabel = $scope.registrationLoginStatus.status;
								vcRecaptchaService.reload();
								$scope.disableForm = false;
								$timeout(function () { $scope.registrationLabel  = ''; }, 5000);	
							}							
						}
					});
				}
				/**
				 * Show Merge Account Modal
				 */
				$scope.showMergeModal = function() {
					var modalInstance = mergeAccountModalService.show({
						email : $scope.registerEmail,
						accessCode : $scope.registerAccessCode,
						parentScope : $scope,
						vcRecaptchaService : vcRecaptchaService
					}, {});
					
					modalInstance.then(function (response) {
						if(response.statusNumber === 0) {
							$scope.modalOptions.close();
						}
					});
				}
			} ]
		}
		return $uibModal.open(tempModalDefaults).result;
	};
}])