cepApp.service('mergeAccountModalService', ['$http', '$uibModal', '$sce', '$window', 'AuthenticationService', 'ssoUrl', '$window', function ($http, $uibModal, $sce, $window, AuthenticationService, ssoUrl) {

	var modalDefaults = {
		animation: true,
		templateUrl: '/CEP/cep/components/login/page-partials/merge-account-modal.html'
	};

	var modalOptions = {
		headerText: '',
		bodyText: 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass: 'asvab-modal'
	};

	this.showModal = function (customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function (customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = ['$scope', '$uibModalInstance', 'appUrl', function ($scope, $uibModalInstance, appUrl) {
				$scope.successful = false;
				var LOGIN_BASE_URL = appUrl + 'applicationAccess/';
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function (result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function (result) {
					// enable form
					tempModalDefaults.parentScope.disableForm = false;
					// refresh recaptcha
					tempModalDefaults.vcRecaptchaService.reload();
					$uibModalInstance.close('cancel');
				};
				$scope.profileFlag = true;
				$scope.careerPlanFlag = true;
				$scope.email = tempModalDefaults.email;
				$scope.accessCode = tempModalDefaults.accessCode;
				//$scope.userId = $scope.registrationLoginStatus.user_id;
				//$scope.role = $scope.registrationLoginStatus.role
					
				$scope.mergeAccount = function() {
					var url = LOGIN_BASE_URL + 'merge/'+$scope.email+'/'+$scope.accessCode+'/'+$scope.profileFlag+'/'+$scope.careerPlanFlag+'/';
					
					$scope.loading = true;
					$scope.statusMessage = false;
					$http.get(url)
						.then(function (response) {
							var userData = response.data;
							if (userData.statusNumber == 0) {
								$scope.successful = true;
								//$scope.modalOptions.ok($scope.successful);
								$scope.modalOptions.ok(userData);
							} else if (userData.statusNumber == 1012) {
								$scope.message = "Merge Failed! Access Code is already registered";
								$scope.statusMessage = true;
							} else {
								$scope.message = "Unknown Status Error";
								$scope.statusMessage = true;
							}

							$scope.loading = false;
							AuthenticationService.SetCredentials(
									userData.user_id, 
									userData.role,
									userData.schoolCode,
									userData.schoolName,
									userData.city,
									userData.state,
									userData.zipCode,
									userData.mepsId,
									userData.mepsName);
							
							$uibModalInstance.dismiss('cancel');
							$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
						});
				};
			}]
		}
		return $uibModal.open(tempModalDefaults).result;
	};

}]);