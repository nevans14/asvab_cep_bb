cepApp.service('LoginModalService', [ '$uibModal', '$location', '$http', '$window','$timeout', 'AuthenticationService','appUrl','modalService', 'RegisterModalService', 'ssoUrl', 'recaptchaKey', function($uibModal, $location, $http, $window, $timeout, AuthenticationService, appUrl, modalService, RegisterModalService, ssoUrl, recaptchaKey) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/login/page-partials/login-modal2.html',
		size : 'sm',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};
		 
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', 'mergeAccountModalService', 'learnMoreModalService', function($scope, $uibModalInstance, mergeAccountModalService, learnMoreModalService) {
				$scope.recaptchaKey = recaptchaKey;
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				// toggle between views
				$scope.didForgetPass = false;
				$scope.isEmailLogin = true;
				$scope.isAccessCodeLogin = false;
				$scope.disabled = false;

				var serviceEndpoint = {
					'AccessCode':	'loginAccessCode',
					'Email':		'loginEmail',
					'Reset':		'passwordResetRequest',
					'CheckTeacherCode': 'testTeacherAccessCode'
				};
				
				$scope.toggleEmailLogin = function () {
					$scope.isEmailLogin = true;
					$scope.isAccessCodeLogin = false;
					$scope.didForgetPass = false;
					// clear properties
					$scope.accessCode = '';
					$scope.requestPasswordEmail = '';
				};
				
				$scope.toggleAccessCodeLogin = function () {
					$scope.isEmailLogin = false;
					$scope.isAccessCodeLogin = true;
					$scope.didForgetPass = false;
					
					$scope.requestPasswordEmail = '';
					$scope.email = '';
					$scope.password = '';
				};
				
				$scope.toggleForgotPass = function () {
					$scope.isEmailLogin = false;
					$scope.isAccessCodeLogin = false;
					$scope.didForgetPass = true;
					
					$scope.accessCode = '';
					$scope.email = '';
					$scope.password = '';
				};
				
				$scope.MergeAccounts = false;
				$scope.loginAccessCodeLabel = '';
				var LOGIN_BASE_URL = appUrl + 'applicationAccess/';				
				
				$scope.goToGeneralHelp = function(result){					
					$uibModalInstance.close(result);
					$location.path('general-help')
				};
				
				$scope.showRegisterModal = function(result, accessCode) {
					$uibModalInstance.close(result);
					RegisterModalService.show({}, {}, accessCode);
				};
				
				/**
				 * 
				 */
				$scope.clearLoginAccessCodeLabel = function() {
					$scope.loginAccessCodeLabel = '';
				};

				$scope.clearLoginEmailLabel = function() {
					$scope.loginEmailLabel = '';
				};

				$scope.clearRegistrationLabel = function() {
					$scope.registrationLabel = '';
				};

				/**
				 * AccessCodeLogin - When Access Code Login button pressed in
				 * Login Modal!
				 */
				$scope.AccessCodeLogin = function(result) {
					var accessCode = $scope.accessCode.toUpperCase();
					var url = LOGIN_BASE_URL + serviceEndpoint.CheckTeacherCode;
					// Checks to see if the access code is a promo code
					$http.post(url, {accessCode: accessCode }).then(function(res) {
						if (res.data.statusNumber === 0)
						{
							$scope.showRegisterModal(result, accessCode);
							return;
						}
						// disable the form
						$scope.disabled = true;
						// Login Modal
						var data = {
							email 		: "",
							accessCode 	: $scope.accessCode,
							password 	: "",
							resetKey 	: "",
							gResponse	: $scope.recaptchaResponse
						};

						url = LOGIN_BASE_URL + serviceEndpoint.AccessCode;

						$http.post(url, data).then(function(response) {
							// enable the form
							$scope.disabled = false;

							var userData = response.data;

							// If success then
							if (userData.statusNumber === 0) {
								// Set the cookie and global
								// values
								AuthenticationService.SetCredentials(
									userData.user_id,
									userData.role,
									userData.accessCode,
									userData.schoolCode,
									userData.schoolName,
									userData.city,
									userData.state,
									userData.zipCode,
									userData.mepsId,
									userData.mepsName);
								// Close Dialog
								$uibModalInstance.dismiss('cancel');
								$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
							} else { // Otherwise set error
								// flag
								$scope.loginAccessCodeLabel = userData.status;
								$timeout(function () { $scope.loginAccessCodeLabel = ''; }, 5000);
							}
						}, function(response) {
							var repoData = response.data || "Request failed";
							console.log(repoData);
						});
					});
				};
				
				/**
				 * EmailLogin - Login utilizing user credentials.
				 */
				$scope.EmailLogin = function() {
					var data = {
						email 		: $scope.email,
						accessCode 	: "",
						password 	: $scope.password,
						resetKey 	: "",
						gResponse	: $scope.recaptchaResponse
					};

					$scope.password = '';

					var url = LOGIN_BASE_URL + serviceEndpoint.Email;

					$http.post(url, data).then(function(response) {
						var userData = response.data;
						if (userData.statusNumber !== 0) {
							$scope.loginEmailLabel = userData.status;
					        $timeout(function () { $scope.loginEmailLabel = ''; }, 5000);   
							return;
						}
						AuthenticationService.SetCredentials(
								userData.user_id,
								userData.role,
								userData.accessCode,
								userData.schoolCode,
								userData.schoolName,
								userData.city,
								userData.state,
								userData.zipCode,
								userData.mepsId,
								userData.mepsName);
						// Close Dialog
						$uibModalInstance.dismiss('cancel');
						// Login to CITM.
						$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
					});
				};

				// Reset credentials
				$scope.RequestResetPassword = function() {

					var data = {
						email 		: $scope.requestPasswordEmail,
						accessCode 	: "",
						password 	: "",
						resetKey 	: "",
						gResponse	: $scope.recaptchaResponse
					};

					var url = LOGIN_BASE_URL + serviceEndpoint.Reset;

					$http.post(url, data).then(function(response) {
						$scope.registrationLoginStatus = response.data;
						
						if ($scope.registrationLoginStatus.statusNumber === 0) {
							// hide registration view which then shows login
							// view
							$scope.toggleEmailLogin();
							// Close Dialog
							$uibModalInstance.dismiss('cancel');
						} else { // Error
							// Pop up Modal with error message.
							$scope.forgetLabel = $scope.registrationLoginStatus.status;
							$timeout(function () { $scope.forgetLabel   = ''; }, 5000); 
						}
					});
				};
			} ]
		}
		return $uibModal.open(tempModalDefaults).result;
	};
} ]);