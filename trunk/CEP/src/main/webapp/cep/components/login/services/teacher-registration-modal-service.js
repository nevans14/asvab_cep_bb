cepApp.service('TeacherModalService', [ '$uibModal', '$location', '$http', '$window','$timeout', 'AuthenticationService','appUrl','modalService', 'ssoUrl', 'LoginModalService', 'recaptchaKey', 'vcRecaptchaService', function($uibModal, $location, $http, $window, $timeout, AuthenticationService, appUrl, modalService, ssoUrl, LoginModalService, recaptchaKey, vcRecaptchaService) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/login/page-partials/teacher-registration-modal.html',
		size : 'md',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};
		 
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				var LOGIN_BASE_URL = appUrl + 'applicationAccess/';
				// modal scopes
				$scope.recaptchaKey = recaptchaKey;
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.showLoginModal = function() {
					console.log('here');
					$uibModalInstance.dismiss('cancel');
					LoginModalService.show({}, {});
				};

				$scope.teacherAccessCode = '';
				$scope.registerPassword1 = '';
				$scope.registerPassword2 = '';
				$scope.registerEmail = '';

				$scope.validateTeacherPromoCode = function() {
					var data = {
						email : "",
						accessCode : $scope.teacherAccessCode,
						password : "",
						resetKey : ""
					};

					var url = LOGIN_BASE_URL + 'testTeacherAccessCode';

					$http.post(url, data).then(function(response) {
						var userData = response.data;
						if (userData.statusNumber !== 0) {
							$scope.loginEmailLabel = userData.status;
					        $timeout(function () { $scope.loginEmailLabel = ''; }, 5000);
						}
						$scope.create = true;
					});
				};
				
				// @RequestMapping(value = "/applicationAccess/teacherRegistration",
				// method = RequestMethod.POST)
				$scope.teacherRegistration = function() {
					// Register Modal
					var data = {
						email 				: $scope.registerEmail, // document.getElementById("registerEmail").value,
						subject				: $scope.registerSubject,
						accessCode 			: $scope.teacherAccessCode, // document.getElementById("registerAccessCode").value,
						password 			: "",
						resetKey 			: "",
						recaptchaResponse	: $scope.recaptchaResponse
					};

					var pass1 = $scope.registerPassword1;
					var pass2 = $scope.registerPassword2;

					if (!data.recaptchaResponse) {
						$scope.registrationLabel = 'Recaptcha is required!';
						return;
					}

					if (pass1.length <= 7) {
						$scope.registrationLabel = "Password too short!";
						return;
					}
					
					if (pass1 !== pass2) {
						$scope.registrationLabel = "Passwords don't match!";
						return;
					}

					var url = LOGIN_BASE_URL + 'teacherRegistration';

					data.password = $scope.registerPassword2;
					$http.post(url, data).then(function(response) {
						$scope.registrationLoginStatus = response.data;
						// Success ..
						var userData = response.data;

						// Open Login
						if ($scope.registrationLoginStatus.statusNumber === 0) {
							// hide registration view which then shows login
							// view
							$scope.isRegister = false;
							// Clear all the data
							$scope.registerAccessCode = '';
							$scope.registerEmail =  '';
							$scope.registerPassword1 =  '';
							$scope.registerPassword2 =  '';
							$scope.registerStudentId = '';

							//  Now log in the use ....
							AuthenticationService.SetCredentials(
								userData.user_id,
								userData.role,
								userData.accessCode,
								userData.schoolCode,
								userData.schoolName,
								userData.city,
								userData.state,
								userData.zipCode,
								userData.mepsId,
								userData.mepsName);
							// Close Dialog
							$uibModalInstance.dismiss('cancel');
							// Login to CITM.
							$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
						} else { // Error
							vcRecaptchaService.reload();
							$scope.registrationLabel = $scope.registrationLoginStatus.status;
							$timeout(function () { $scope.registrationLabel  = ''; }, 5000);
						}
					});
				};
				
				$scope.clearRegistrationLabel = function() {
					$scope.registrationLabel = '';
				};
			} ]
		}
		return $uibModal.open(tempModalDefaults).result;
	};
} ]);