cepApp.controller('LoginController', ['$scope', '$rootScope', '$location', '$http', '$timeout', '$window', 'appUrl', 'awsApiFullUrl', 'ssoUrl', 'AuthenticationService', 'RegisterModalService', 'recaptchaKey', 'vcRecaptchaService', function ($scope, $rootScope, $location, $http, $timeout, $window, appUrl, awsApiFullUrl, ssoUrl, AuthenticationService, RegisterModalService, recaptchaKey,vcRecaptchaService) {

	var LOGIN_BASE_URL = appUrl + 'applicationAccess/';
	// endpoint enum
	var serviceEndpoint = {
		'AccessCode':	'loginAccessCode',
		'Email':		'loginEmail',
		'Reset':		'passwordResetRequest',
		'CheckTeacherCode': 'testTeacherAccessCode'
	};
	
	var toggleService = {
		'AccessCode': 	1,
		'Email': 		2,
		'Reset':		3
	};

	($scope.getAwsXsrfCookie = function() {
		let awsXsrfCookie = AuthenticationService.getAwsXsrfCookie();
		if(!awsXsrfCookie){
			$http({method: 'GET', url: awsApiFullUrl + '/api/token'}).then(function(success) {
				if (success.data && success.data.token) {
					AuthenticationService.setAwsXsrfCookie(success.data.token);
				}
			});
		}
	})();
	
	
	/**
	 * Clears all properties based on the service that has been selected
	 */
	$scope.clearProperties = function(service) {
		switch (service) {
			case toggleService.AccessCode: // clears email/password reset properties
				$scope.email = $scope.password = $scope.resetPasswordEmail = '';
				break;
			case toggleService.Email: // clears access/password reset properties
				$scope.AccessCode = $scope.resetPasswordEmail = '';
				break;
			case toggleService.Reset: // clears access/email login properties
				$scope.accessCode = $scope.email = $scope.password = '';
				break;
			default: // clears all properties
				$scope.email = $scope.password = $scope.accessCode = $scope.resetPasswordEmail = '';
				break;
		}
	};
	
	/**
	 * Toggles views
	 */
	$scope.toggleAccessCodeLogin = function () {
		$scope.showAccessCodeLogin = true;
		$scope.showEmailLogin = $scope.showPasswordReset = false;
		$scope.clearProperties(toggleService.AccessCode);
	};
	
	$scope.toggleEmailLogin = function () {
		$scope.showEmailLogin = true;
		$scope.showAccessCodeLogin = $scope.showPasswordReset = false;
		$scope.clearProperties(toggleService.Email);
	};
	
	$scope.togglePasswordReset = function () {
		$scope.showPasswordReset = true;
		$scope.showAccessCodeLogin = $scope.showEmailLogin = false;
		$scope.clearProperties(toggleService.Reset);
	};
	$scope.recaptchaKey = recaptchaKey;
	// views, by default, show Email login
	$scope.showAccessCodeLogin = false;
	$scope.showEmailLogin = true;
	$scope.showPasswordReset = false;	
	// for access code login
	$scope.accessCode;
	// for email/password login
	$scope.email;
	$scope.password;
	// for resetting password
	$scope.resetPasswordEmail;
	// global message
	$scope.message;
	// toggles form
	$scope.disabled = false;
	/**
	 * on click for the "Where do I get my access code?"
	 */
	$scope.goToGeneralHelp = function(){
		window.open('general-help#general', '_self', 'resizable,location,menubar,toolbar,scrollbars,status');
	};
	/**
	 * access code login
	 */
	$scope.accessCodeLogin = function () {
		// disables form
		$scope.disabled = true;
		// set up data
		var data = {
			email				: "",
			accessCode			: $scope.accessCode,
			password			: "",
			resetKey			: "",
			recaptchaResponse	: $scope.recaptchaResponse
		};
		// reset access code
		$scope.accessCode = '';

		var url = LOGIN_BASE_URL + serviceEndpoint.CheckTeacherCode;
		$http.post(url, {accessCode: data.accessCode.toUpperCase()}).then(function(res) {
			if(res.data.statusNumber === 0) {
				$scope.disabled = false;
				// show the register modal and pass in the access code
				RegisterModalService.show({}, {}, data.accessCode.toUpperCase());
				// exit out of the login;
				return;
			}

			// set up url
			var url = LOGIN_BASE_URL + serviceEndpoint.AccessCode;
			// post request
			$http.post(url, data).then(function(response) {
				// enables form
				$scope.disabled = false;
				// set response object
				var res = response.data;
				// success, set credentials
				if (res.statusNumber === 0) {
					AuthenticationService.SetCredentials(
							res.user_id,
							res.role,
							res.accessCode,
							res.schoolCode,
							res.schoolName,
							res.city,
							res.state,
							res.zipCode,
							res.mepsId,
							res.mepsName);
					// SSO: login to the CTM website
					$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
				} else {
					// error, set error message
					$scope.message = res.status;
					//vcRecaptchaService.reload();
					$timeout(function () { $scope.message = '' }, 5000);
				}
			}, function (response) {
				var repoData = response.data || "Request failed.";
				console.log(repoData);
			});
		})
	};
	
	/**
	 * email/password login
	 */
	$scope.emailLogin = function () {
		$scope.disabled = true;
		// set up data		
		var data = {
			email				: $scope.email,
			accessCode			: "",
			password			: $scope.password,
			resetKey			: "",
			recaptchaResponse	: $scope.recaptchaResponse
		};
		// reset password
		$scope.password = '';
		// set up url
		var url = LOGIN_BASE_URL + serviceEndpoint.Email;
		// post request
		$http.post(url, data).then(function(response) {
			$scope.disabled = false;
			var res = response.data;			
			// if not successful, show the message and return out of method
			if (res.statusNumber !== 0) {
				$scope.message = res.status;
				//vcRecaptchaService.reload();
				// show message for 5 seconds and disappear
				$timeout(function () { $scope.message = ''; }, 5000);
				return;
			}		
			// set credentials
			AuthenticationService.SetCredentials(
					res.user_id,
					res.role,
					res.accessCode,
					res.schoolCode,
					res.schoolName,
					res.city,
					res.state,
					res.zipCode,
					res.mepsId,
					res.mepsName);
			// SSO: login to the CTM website
			$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
		})
	};
	
	/**
	 * reset password
	 */
	$scope.sendResetCode = function () {
		// set up data
		var data = {
			email				: $scope.resetPasswordEmail,
			accessCode			: "",
			password			: "",
			resetKey			: "",
			recaptchaResponse	: $scope.recaptchaResponse
		};
		// set up url
		var url = LOGIN_BASE_URL + serviceEndpoint.Reset;
		// post request
		$http.post(url, data).then(function(res){
			// success
			if (res.data.statusNumber === 0) {
				$scope.message = 'Email with Reset key has been sent.';
			} else {
				// error
				$scope.message = res.data.status;
				//vcRecaptchaService.reload();
			}
		})
	};
}]);