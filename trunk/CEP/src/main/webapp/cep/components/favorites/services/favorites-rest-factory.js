cepApp.factory('FavoriteRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var favoritesRestFactory = {};

	favoritesRestFactory.getFavoriteOccupation = function() {
		return $http.get(appUrl + 'favorite/getFavoriteOccupation');
	}

	favoritesRestFactory.insertFavoriteOccupation = function(favoriteObject) {
		return $http.post(appUrl + 'favorite/insertFavoriteOccupation', favoriteObject);
	}

	favoritesRestFactory.deleteFavoriteOccupation = function(id) {

		return $http({
			method : 'DELETE',
			url : appUrl + 'favorite/deleteFavoriteOccupation/' + id + '/'
		})
	}
	
	favoritesRestFactory.getFavoriteCareerCluster = function() {
		return $http.get(appUrl + 'favorite/getFavoriteCareerCluster');
	}

	favoritesRestFactory.insertFavoriteCareerCluster = function(favoriteObject) {
		return $http.post(appUrl + 'favorite/insertFavoriteCareerCluster', favoriteObject);
	}

	favoritesRestFactory.deleteFavoriteCareerCluster = function(id) {

		return $http({
			method : 'DELETE',
			url : appUrl + 'favorite/deleteFavoriteCareerCluster/' + id + '/'
		})
	}
	
	favoritesRestFactory.getFavoriteSchool = function() {
		return $http.get(appUrl + 'favorite/getFavoriteSchool');
	}

	favoritesRestFactory.insertFavoriteSchool = function(favoriteObject) {
		return $http.post(appUrl + 'favorite/insertFavoriteSchool', favoriteObject);
	}

	favoritesRestFactory.deleteFavoriteSchool = function(id) {

		return $http({
			method : 'DELETE',
			url : appUrl + 'favorite/deleteFavoriteSchool/' + id + '/'
		})
	}

	return favoritesRestFactory;

} ]);