cepApp.controller('FavoritesController', [ '$scope', 'UserFactory', 'modalService', 'favoriteList', 'NotesModalService', '$location', 'careerClusterFavoriteList', 'schoolFavoriteList', 'scoreSummary', function($scope, UserFactory, modalService, favoriteList, NotesModalService, $location, careerClusterFavoriteList, schoolFavoriteList, scoreSummary) {

	$scope.favoriteList = favoriteList;
	$scope.careerClusterFavoriteList = careerClusterFavoriteList;
	$scope.schoolFavoriteList = schoolFavoriteList;
	console.log($scope.schoolFavoriteList);

	var favoritelength = $scope.favoriteList.length

	// reset occupations selected
	for (var i = 0; i < favoritelength; i++) {
		$scope.favoriteList[i].selected = false;
	}

	$scope.scoreSummary = scoreSummary.data;

	$scope.haveAsvabTestScores = (angular.isObject($scope.scoreSummary) && $scope.scoreSummary.hasOwnProperty('verbalAbility'));
	
	// remove favorites using user factory service
	$scope.removeFavorites = function(id) {
		$scope.myStyle = {'cursor': 'wait'};

		var promise = UserFactory.deleteFavorite(id);
		promise.then(function(response) {
			$scope.favoriteList = response;
			$scope.myStyle = {'cursor': 'pointer'};
		}, function(reason) {
			$scope.myStyle = {'cursor': 'pointer'};

			for (var i = 0; i < $scope.favoriteList.length; i++) {
				if ($scope.favoriteList[i].id = id) {
					$scope.favoriteList[i].disabled = false;
				}
			}

			// inform user that there was an error and to try again.
			console.log(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
		})
	}

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 3;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	$scope.compareSelection = function() {
		var occupationSelected = [];

		for (var i = 0; i < favoritelength; i++) {

			if ($scope.favoriteList[i].selected == true) {
				occupationSelected.push($scope.favoriteList[i].onetSocCd);
			}
		}

		if (occupationSelected.length < 2) {
			var modalOptions = {
				headerText : 'Favorites',
				bodyText : 'Select at least 2 occupations.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
			return false;
		}
		
		//$location.path('/compare-occupation');
		$location.url('/compare-occupation/' + occupationSelected[0] + '/' + occupationSelected[1] + '/' + occupationSelected[2] );

	}
	
	// remove career cluster favorites using user factory service
	$scope.removeCareerClusterFavorites = function(id) {
		$scope.myStyle = {'cursor': 'wait'};

		var promise = UserFactory.deleteCareerClusterFavorite(id);
		promise.then(function(response) {
			$scope.careerClusterFavoriteList = response;
			$scope.myStyle = {'cursor': 'pointer'};
		}, function(reason) {
			$scope.myStyle = {'cursor': 'pointer'};

			for (var i = 0; i < $scope.careerClusterFavoriteList.length; i++) {
				if ($scope.careerClusterFavoriteList[i].id = id) {
					$scope.careerClusterFavoriteList[i].disabled = false;
				}
			}

			// inform user that there was an error and to try again.
			console.log(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
		})
	}
	
	// remove school favorites using user factory service
		$scope.removeSchoolFavorites = function(id) {
			$scope.myStyle = {'cursor': 'wait'};

			var promise = UserFactory.deleteSchoolFavorite(id);
			promise.then(function(response) {
				$scope.schoolFavoriteList = response;
				$scope.myStyle = {'cursor': 'pointer'};
			}, function(reason) {
				$scope.myStyle = {'cursor': 'pointer'};

				for (var i = 0; i < $scope.schoolFavoriteList.length; i++) {
					if ($scope.schoolFavoriteList[i].id = id) {
						$scope.schoolFavoriteList[i].disabled = false;
					}
				}

				// inform user that there was an error and to try again.
				console.log(reason);
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'There was an error proccessing your request. Please try again.'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
			})
		}

	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
