cepApp.service('CertificationListModalService', [ '$uibModal', '$http','CertificationDetailsService', function($uibModal, $http, CertificationDetailsService) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/certification-list-modal.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.occupationCertificates = tempModalDefaults.certs;
				$scope.occupationTitle = tempModalDefaults.occupationTitle;

				// Following line in occufind since we pass scope it should be
				// there.
				// $scope.occupationCertificates =
				// JSON.parse(occupationCertificates.data.payload);
				$scope.isCertificationListVisable = true;
				$scope.loadingCareerOneInfo = false;
				/**
				 * Pagination for Certifications, Licenses and Apprenticeship
				 * Modals
				 */
				$scope.certPagination = {};
				$scope.certPagination.TotalItems = $scope.occupationCertificates.RecordCount;
				$scope.certPagination.CurrentPage = 1;
				$scope.certPagination.MaxSize = 4;
				$scope.certPagination.ItemsPerPage = 10;
				
				$scope.openCertificateDetail = function(certificateDetail) {
					/* alert(certId); */
					CertificationDetailsService.show({
						certId : certificateDetail,
						occupationTitle: $scope.occupationTitle
					}, {});
				}

			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};  // End of this.show 

} ]);