cepApp.service('ApprenticeshipModalService', [ '$uibModal', '$location', '$http', 'modalService', function($uibModal, $location, $http, modalService) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/apprenticeship-list-modal.html',
		size : 'lg',                                                
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.isApprenticeshipsVisable = true;
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				$scope.loadingCareerOneInfo = false;
				$scope.apprenticeshipPagination = {};
				$scope.apprenticeshipPagination.TotalItems = 100; // occupationCertificates.CertificationInfo.CertificationList.length;
				$scope.apprenticeshipPagination.CurrentPage = 1;
				$scope.apprenticeshipPagination.MaxSize = 4;
				$scope.apprenticeshipPagination.ItemsPerPage = 10;
				$scope.onetOccupationCode = tempModalDefaults.socId
				$scope.occupationTitle = tempModalDefaults.occupationTitle;

				$scope.$watch('statesList.selectedOption', function() {
					console.log('the statesList.selectedOption has changed ....');
					// get licenses information ....
					var socId = $scope.onetOccupationCode;
					var location = $scope.statesList.selectedOption.value;
					$scope.loadingCareerOneInfo = true;
					$q.all([
					/* asyncLicense(socId, location), */
					asynApprenticeship(socId, location) ]).then(function(data) {
						$scope.loadingCareerOneInfo = false;

						if(IsJsonString(data[0].payload)){
							$scope.apprenticeshipList = JSON.parse(data[0].payload);
							// Apprenticeships ...
							var list = $scope.apprenticeshipList.ApprenticeshipSponsorList;
							$scope.apprenticeshipLabel = "";
							if (typeof list.length == 'undefined') {
								console.log('We have one ')
								$scope.apprenticeshipList = new Array();
								$scope.apprenticeshipList.push(list);
							} else {
								$scope.apprenticeshipList = list;
							}
							$scope.apprenticeshipPagination.TotalItems = $scope.apprenticeshipList.length;
						} else {
							$scope.apprenticeshipPagination.TotalItems = 0;
							$scope.apprenticeshipList = {};
							$scope.apprenticeshipLabel = "No apprenticeships for this state and occupation.";
						}
					}, function(reason) { // error
						$scope.loadingCareerOneInfo = false;
						$scope.licensesPagination.TotalItems = 0;
						$scope.apprenticeshipPagination.TotalItems = 0;
					}); // end of $q
				}); // end of watch state

				$scope.apprenticeship = {}
				$scope.findApprenticeship = function() {
					$scope.loadingCareerOneInfo = true;
					// get licenses information ....
					var socId = $scope.onetOccupationCode;
					var location = $scope.statesList.selectedOption.value;
					var promise = asynApprenticeship(socId, location);
					promise.then(function(greeting) {
						$scope.loadingCareerOneInfo = false;
						if(IsJsonString(greeting.payload)){
							conlose.log($scope.apprenticeshipList);
							$scope.apprenticeshipList = JSON.parse(greeting.payload);
							var list = $scope.apprenticeshipList;

							$scope.apprenticeshipLabel = "";

							if (typeof list.RecordCount < 2) {
								console.log('We have one ')
								$scope.apprenticeshipList = new Array();
								$scope.apprenticeshipList.push(list);
							} else {
								$scope.apprenticeshipList = list;
							}

							$scope.apprenticeshipPagination.TotalItems = $scope.apprenticeshipList.RecordCount;

							$("#apprenticeshipModalButton").click();
						} else {
							$scope.apprenticeshipList = greeting.payload;
							$scope.apprenticeshipPagination.TotalItems = 0;
							$scope.apprenticeshipList = {};
							$scope.apprenticeshipLabel = "No apprenticeships for this state and occupation.";

							return;
						}
					}, function(reason) {
						$scope.loadingCareerOneInfo = true;
						alert('Failed: ' + reason);
					}, function(update) {
						alert('Got notification: ' + update);
					});
				}

				function IsJsonString(str) {
				    try {
				        JSON.parse(str);
				    } catch (e) {
				        return false;
				    }
				    return true;
				}
				
				$scope.statesList = {
					availableOptions : [ {
						value : 'AL',
						name : 'ALABAMA'
					}, {
						value : 'AK',
						name : 'ALASKA'
					}, {
						value : 'AZ',
						name : 'ARIZONA'
					}, {
						value : 'AR',
						name : 'ARKANSAS'
					}, {
						value : 'CA',
						name : 'CALIFORNIA'
					}, {
						value : 'CO',
						name : 'COLORADO'
					}, {
						value : 'CT',
						name : 'CONNECTICUT'
					}, {
						value : 'DE',
						name : 'DELAWARE'
					}, {
						value : 'FL',
						name : 'FLORIDA'
					}, {
						value : 'GA',
						name : 'GEORGIA'
					}, {
						value : 'HI',
						name : 'HAWAII'
					}, {
						value : 'ID',
						name : 'IDAHO'
					}, {
						value : 'IL',
						name : 'ILLINOIS'
					}, {
						value : 'IN',
						name : 'INDIANA'
					}, {
						value : 'IA',
						name : 'IOWA'
					}, {
						value : 'KS',
						name : 'KANSAS'
					}, {
						value : 'KY',
						name : 'KENTUCKY'
					}, {
						value : 'LA',
						name : 'LOUISIANA'
					}, {
						value : 'ME',
						name : 'MAINE'
					}, {
						value : 'MD',
						name : 'MARYLAND'
					}, {
						value : 'MA',
						name : 'MASSACHUSETTS'
					}, {
						value : 'MI',
						name : 'MICHIGAN'
					}, {
						value : 'MN',
						name : 'MINNESOTA'
					}, {
						value : 'MS',
						name : 'MISSISSIPPI'
					}, {
						value : 'MO',
						name : 'MISSOURI'
					}, {
						value : 'MT',
						name : 'MONTANA'
					}, {
						value : 'NE',
						name : 'NEBRASKA'
					}, {
						value : 'NV',
						name : 'NEVADA'
					}, {
						value : 'NH',
						name : 'NEW HAMPSHIRE'
					}, {
						value : 'NJ',
						name : 'NEW JERSEY'
					}, {
						value : 'NM',
						name : 'NEW MEXICO'
					}, {
						value : 'NY',
						name : 'NEW YORK'
					}, {
						value : 'NC',
						name : 'NORTH CAROLINA'
					}, {
						value : 'ND',
						name : 'NORTH DAKOTA'
					}, {
						value : 'OH',
						name : 'OHIO'
					}, {
						value : 'OK',
						name : 'OKLAHOMA'
					}, {
						value : 'OR',
						name : 'OREGON'
					}, {
						value : 'PA',
						name : 'PENNSYLVANIA'
					}, {
						value : 'RI',
						name : 'RHODE ISLAND'
					}, {
						value : 'SC',
						name : 'SOUTH CAROLINA'
					}, {
						value : 'SD',
						name : 'SOUTH DAKOTA'
					}, {
						value : 'TN',
						name : 'TENNESSEE'
					}, {
						value : 'TX',
						name : 'TEXAS'
					}, {
						value : 'UT',
						name : 'UTAH'
					}, {
						value : 'VT',
						name : 'VERMONT'
					}, {
						value : 'VA',
						name : 'VIRGINIA'
					}, {
						value : 'WA',
						name : 'WASHINGTON'
					}, {
						value : 'WV',
						name : 'WEST VIRGINIA'
					}, {
						value : 'WI',
						name : 'WISCONSIN'
					}, {
						value : 'WY',
						name : 'WYOMING'
					}, {
						value : 'GU',
						name : 'GUAM'
					}, {
						value : 'PR',
						name : 'PUERTO RICO'
					}, {
						value : 'VI',
						name : 'VIRGIN ISLANDS'
					} ],
					selectedOption : {
						value : 'AL',
						name : 'ALABAMA'
					}
				};

				// /apprenticeship/{onet_soc}/{state}
				function asynApprenticeship(onetSoc, location) {
					var deferred = $q.defer();

					deferred.notify('Query for apprenticeship for ' + onetSoc + ' in ' + location);

					var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
					var careerOneStopUrl = '/CEP/rest/' + 'CareerOneStop/apprenticeship/' + onetOccupationCode + '/' + location;

					try {
						$http.get(careerOneStopUrl).then(function(response) {
							// $scope.Available = response.data; 
							//  window.alert(JSON.stringify(response.data));
							console.log('success');
							deferred.resolve(response.data);
						}, function(error) {

							console.log(error);
							deferred.reject('Greeting ' + name + ' is not allowed.');
						});
					} catch (err) {
						alert(err.message);
					}

					return deferred.promise;
				}

				$scope.findApprenticeship();
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);