cepApp.service('LicenseDetailsModalService', [ '$uibModal', '$location', '$http','modalService', '$sce', function($uibModal, $location, $http,modalService, $sce) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/license-details-modal.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				var row = tempModalDefaults.row

				$scope.licenseDetails = {};
				$scope.licenseDetails.LICENSENAME= row.Title;
				$scope.licenseDetails.LICENSINGAGENCY= row.LicenseAgency.Name;
				$scope.licenseDetails.LICTITLE= row.Title;
				$scope.licenseDetails.DESCRIPTION= $sce.trustAsHtml(row.Description);
				$scope.licenseDetails.LICENSEAGENCY= row.LicenseAgency.Name;
				$scope.licenseDetails.ADDRESS1= row.LicenseAgency.Address;
				$scope.licenseDetails.CITY= row.LicenseAgency.City;
				$scope.licenseDetails.STATE= row.LicenseAgency.State;
				$scope.licenseDetails.ZIP= row.LicenseAgency.Zip;
				$scope.licenseDetails.URL= row.LicenseAgency.Url
				$scope.licenseDetails.EMAIL= row.LicenseAgency.Email;
				$scope.licenseDetails.TELEPHONE= formatPhoneNumber(row.LicenseAgency.Phone);
				
				function formatPhoneNumber(number) {

			        if (!number) { return ''; }

			        number = String(number);

			        var formattedNumber = number;

			        var c = (number[0] == '1') ? '1 ' : '';
			        number = number[0] == '1' ? number.slice(1) : number;

			        var area = number.substring(0,3);
			        var front = number.substring(3, 6);
			        var end = number.substring(6, 10);

			        if (front) {
			            formattedNumber = (c + "(" + area + ") " + front);  
			        }
			        if (end) {
			            formattedNumber += ("-" + end);
			        }
			        return formattedNumber;
			    }

			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);