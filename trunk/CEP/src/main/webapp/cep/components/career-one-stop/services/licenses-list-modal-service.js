cepApp.service('LicensesListModalService', [ '$uibModal', '$location', '$http', 'LicenseDetailsModalService','modalService', 'CareerOneStopFactory', function($uibModal, $location, $http, LicenseDetailsModalService, modalService, CareerOneStopFactory) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/licenses-list-modal.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				$scope.licensesPagination = {};
				$scope.licensesPagination.TotalItems = 100; // occupationCertificates.CertificationInfo.CertificationList.length;
				$scope.licensesPagination.CurrentPage = 1;
				$scope.licensesPagination.MaxSize = 4;
				$scope.licensesPagination.ItemsPerPage = 10;
				$scope.onetOccupationCode = tempModalDefaults.socId
				$scope.occupationTitle = tempModalDefaults.occupationTitle;
				$scope.loadingCareerOneInfo = false;

				$scope.openLicenseDetail = function(record) {

					LicenseDetailsModalService.show({
						row :record
					}, {});


				}

				$scope.$watch('statesList.selectedOption', function() {
					console.log('the statesList.selectedOption has changed ....');
					// get licenses information ....

					var socId = $scope.onetOccupationCode;
					var location = $scope.statesList.selectedOption.value;
					$scope.loadingCareerOneInfo = true;

					var promise = asyncLicense(socId, location);
					promise.then(function(data) {
						$scope.loadingCareerOneInfo = false;

						if(data){
							if(IsJsonString(data.payload)){
								$scope.licensesList = JSON.parse(data.payload);
								var list = $scope.licensesList;
								$scope.licensesLabel = "";
								if (typeof list.length == 'undefined' && typeof list.LICENSEID != 'undefined') {
									console.log('We have one ')
									$scope.licensesList = new Array();
									$scope.licensesList.push(list);
								} else {
									$scope.licensesList = list.LicenseList;
								}
								$scope.licensesLabel = "";
								$scope.licensesPagination.TotalItems = $scope.licensesList.RecordCount;
							}else{
								$scope.licensesList = data.payload;
								$scope.licensesPagination.TotalItems = 0;
								$scope.licensesLabel = "No Licenses for this state and occupation.";
							}
						}

					}, function(reason) { // error
						$scope.loadingCareerOneInfo = false;
						$scope.licensesPagination.TotalItems = 0;
						$scope.apprenticeshipPagination.TotalItems = 0;
					}); // end of $q
				}); // end of watch

				$scope.openLicenses = function() {
					$scope.loadingCareerOneInfo = true;
					// get licenses information ....
					var socId = $route.current.params.socId;
					var location = $scope.statesList.selectedOption.value;
					var promise = asyncLicense(socId, location);
					promise.then(function(greeting) {
						$scope.loadingCareerOneInfo = false;
						$scope.licensesList = JSON.parse(greeting.payload);
						if ($scope.licensesList.Licenses.ErrorNumber) {
							$scope.licensesPagination.TotalItems = 0;
							var customModalOptions = {
								headerText : 'Licenses Error',
								bodyText : 'No Licenses found for this occupation.'
							};
							var customModalDefaults = {};
							modalService.show(customModalDefaults, customModalOptions);
							$("#licensesModalButton").click();
							return;
						}

						var list = $scope.licensesList.Licenses.LicenseList.License;

						if (typeof list.length == 'undefined' && typeof list.LICENSEID != 'undefined') {
							console.log('We have one ')
							$scope.licensesList = new Array();
							$scope.licensesList.push(list);
						} else {
							$scope.licensesList = list;
						}
						$scope.licensesPagination.TotalItems = $scope.licensesList.length;

						$("#licensesModalButton").click();
					}, function(reason) {
						$scope.loadingCareerOneInfo = false;
						alert('Failed: ' + reason);
					}, function(update) {
						alert('Got notification: ' + update);
					});
				}

				$scope.statesList = {
					availableOptions : [ {
						value : 'AL',
						name : 'ALABAMA'
					}, {
						value : 'AK',
						name : 'ALASKA'
					}, {
						value : 'AZ',
						name : 'ARIZONA'
					}, {
						value : 'AR',
						name : 'ARKANSAS'
					}, {
						value : 'CA',
						name : 'CALIFORNIA'
					}, {
						value : 'CO',
						name : 'COLORADO'
					}, {
						value : 'CT',
						name : 'CONNECTICUT'
					}, {
						value : 'DE',
						name : 'DELAWARE'
					}, {
						value : 'FL',
						name : 'FLORIDA'
					}, {
						value : 'GA',
						name : 'GEORGIA'
					}, {
						value : 'HI',
						name : 'HAWAII'
					}, {
						value : 'ID',
						name : 'IDAHO'
					}, {
						value : 'IL',
						name : 'ILLINOIS'
					}, {
						value : 'IN',
						name : 'INDIANA'
					}, {
						value : 'IA',
						name : 'IOWA'
					}, {
						value : 'KS',
						name : 'KANSAS'
					}, {
						value : 'KY',
						name : 'KENTUCKY'
					}, {
						value : 'LA',
						name : 'LOUISIANA'
					}, {
						value : 'ME',
						name : 'MAINE'
					}, {
						value : 'MD',
						name : 'MARYLAND'
					}, {
						value : 'MA',
						name : 'MASSACHUSETTS'
					}, {
						value : 'MI',
						name : 'MICHIGAN'
					}, {
						value : 'MN',
						name : 'MINNESOTA'
					}, {
						value : 'MS',
						name : 'MISSISSIPPI'
					}, {
						value : 'MO',
						name : 'MISSOURI'
					}, {
						value : 'MT',
						name : 'MONTANA'
					}, {
						value : 'NE',
						name : 'NEBRASKA'
					}, {
						value : 'NV',
						name : 'NEVADA'
					}, {
						value : 'NH',
						name : 'NEW HAMPSHIRE'
					}, {
						value : 'NJ',
						name : 'NEW JERSEY'
					}, {
						value : 'NM',
						name : 'NEW MEXICO'
					}, {
						value : 'NY',
						name : 'NEW YORK'
					}, {
						value : 'NC',
						name : 'NORTH CAROLINA'
					}, {
						value : 'ND',
						name : 'NORTH DAKOTA'
					}, {
						value : 'OH',
						name : 'OHIO'
					}, {
						value : 'OK',
						name : 'OKLAHOMA'
					}, {
						value : 'OR',
						name : 'OREGON'
					}, {
						value : 'PA',
						name : 'PENNSYLVANIA'
					}, {
						value : 'RI',
						name : 'RHODE ISLAND'
					}, {
						value : 'SC',
						name : 'SOUTH CAROLINA'
					}, {
						value : 'SD',
						name : 'SOUTH DAKOTA'
					}, {
						value : 'TN',
						name : 'TENNESSEE'
					}, {
						value : 'TX',
						name : 'TEXAS'
					}, {
						value : 'UT',
						name : 'UTAH'
					}, {
						value : 'VT',
						name : 'VERMONT'
					}, {
						value : 'VA',
						name : 'VIRGINIA'
					}, {
						value : 'WA',
						name : 'WASHINGTON'
					}, {
						value : 'WV',
						name : 'WEST VIRGINIA'
					}, {
						value : 'WI',
						name : 'WISCONSIN'
					}, {
						value : 'WY',
						name : 'WYOMING'
					}, {
						value : 'GU',
						name : 'GUAM'
					}, {
						value : 'PR',
						name : 'PUERTO RICO'
					}, {
						value : 'VI',
						name : 'VIRGIN ISLANDS'
					} ],
					selectedOption : {
						value : 'AL',
						name : 'ALABAMA'
					}
				};

				function asyncLicense(onetSoc, location) {
					var deferred = $q.defer();

					deferred.notify('About to query licenses for ' + onetSoc + ' in ' + location);

					try {
						CareerOneStopFactory.getOccupationLicenses(onetSoc, location).then(function(response) {
							console.log('Licenses fetch success');
							deferred.resolve(response.data);
						}, function(error) {
							console.log('Licenses fetch error' + error);
							deferred.reject(error);
						});
					} catch (err) {
						alert(err.message);
					}

					return deferred.promise;
				}
				function asyncLicenseDetail(onetSoc, licenseId, location) {
					var deferred = $q.defer();

					deferred.notify('About to query licenses for ' + licenseId);

					try {
						CareerOneStopFactory.getOccupationLicensesDetail(onetSoc, licenseId, location).then(function(response) {
							console.log('success');
							deferred.resolve(response.data);
						}, function(error) {

							console.log(error);
							deferred.reject('Greeting ' + name + ' is not allowed.');
						});
					} catch (err) {
						alert(err.message);
					}

					return deferred.promise;
				}
				/////////////////////////
				
				function IsJsonString(str) {
				    try {
				        JSON.parse(str);
				    } catch (e) {
				        return false;
				    }
				    return true;
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);