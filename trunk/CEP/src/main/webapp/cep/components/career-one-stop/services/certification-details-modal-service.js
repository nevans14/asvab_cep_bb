cepApp.service('CertificationDetailsService', [ '$uibModal', '$location', '$http', 'modalService','$sce', function($uibModal, $location, $http, modalService,$sce) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/certification-details-modal.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

  	
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				$scope.occupationTitle = tempModalDefaults.occupationTitle;
	
				$scope.certDetail = {};
				$scope.certDetail.Name;
				$scope.certDetail.OrgName;
				$scope.certDetail.OrgAddress;
				$scope.certDetail.OrgWebPage;
				$scope.certDetail.Description;
				$scope.certDetail.Experience;
				$scope.certDetail.ExamDetails;
				$scope.certDetail.Exam;
				$scope.certDetail.Renewal;
				$scope.certDetail.CEU;
				$scope.certDetail.ReExam;
				$scope.certDetail.CPD;
				$scope.certDetail.xxx;

				$scope.openCertificateDetail = function(certificateDetail) {
				
					// get detailed information ....
						$scope.certDetail.Name = certificateDetail.Name;
						$scope.certDetail.OrgName = certificateDetail.Organization;
						$scope.certDetail.OrgAddress = certificateDetail.OrganizationAddress;
						$scope.certDetail.OrgWebPage = certificateDetail.OrganizationUrl;
						if (jQuery.isEmptyObject(certificateDetail.Description)) {
							$scope.certDetail.Description = '';
						} else {
							$scope.certDetail.Description = certificateDetail.Description.replace('&#xd;','\n');
						}

						$scope.certDetail.CertDetailList = certificateDetail.CertDetailList;


				}

				function IsJsonString(str) {
				    try {
				        JSON.parse(str);
				    } catch (e) {
				        return false;
				    }
				    return true;
				}
				
				// /certDetail/{cert_id}
				function asyncCertificationDetail(certId) {
					var deferred = $q.defer();

					deferred.notify('About to query licenses for ' + certId);

					var careerOneStopUrl = '/CEP/rest/CareerOneStop/certDetail/' + certId;

					try {
						$http.get(careerOneStopUrl).then(function(response) {
							// $scope.Available = response.data;
							// window.alert(JSON.stringify(response.data));
							console.log('success');
							deferred.resolve(response.data);
						}, function(error) {

							console.log(error);
							deferred.reject('Greeting ' + name + ' is not allowed.');
						});
					} catch (err) {
						alert(err.message);
					}

					return deferred.promise;
				}

				// 
				$scope.openCertificateDetail(tempModalDefaults.certId);
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);