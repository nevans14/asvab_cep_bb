cepApp.factory('FlickrService', function ($q, $window, FlickrRestFactory) {
	var service = {};
	// session storage
	var galleries = undefined;
	
	service.getFlickrPublicPhotos = function () {
		var deferred = $q.defer();
		// check if galleries is not set yet
		if (galleries === undefined) {
			// check if user browser can support Storage
			if (typeof (Storage) !== undefined) {
				// check if user browser has galleries, if so, then get from storage
				if ($window.sessionStorage.getItem('option-ready-photos') !== null && $window.sessionStorage.getItem('option-ready-photos') !== undefined) {
					galleries = angular.fromJson($window.sessionStorage.getItem('option-ready-photos'));
					// resolve data
					deferred.resolve(galleries);
				} else {
					FlickrRestFactory.getFlickrPublicPhotos().success(function (res) {
						// set galleries to response data
						galleries = res;
						// store into session
						$window.sessionStorage.setItem('option-ready-photos', JSON.stringify(galleries));
						// resolve
						deferred.resolve(galleries);
					}).error(function(data, status, headers, config) {
						deferred.reject("Error: request returned status " + status);
					});
				}
			} else {
				FlickrRestFactory.getFlickrPublicPhotos().success(function (res) {
					// set galleries to response data
					galleries = res;
					// resolve
					deferred.resolve(galleries);
				}).error(function(data, status, headers, config) {
					deferred.reject("Error: request returned status " + status);
				});
			}
		} else {
			deferred.resolve(galleries);
		}
		return deferred.promise;
	}
	
	service.getFlickrGalleryPhotos = function (galleryId) {
		var deferred = $q.defer();
		
		FlickrRestFactory.getFlickrGalleryPhotos(galleryId).success(function (res) {
			deferred.resolve(res.data);
		}).error(function(data, status, headers, config) {
			deferred.reject("Error: request returned status " + status);
		});
		
		return deferred.promise;
	}
	
	service.getFlickrPhoto = function (photoId, photoSecret) {
		var deferred = $q.defer();
		
		FlickrRestFactory.getFlickrPhoto(photoId, photoSecret).success(function (res) {
			deferred.resolve(res.data);
		}).error(function(data, status, headers, config) {
			deferred.reject("Error: request returned status " + status);
		});
		
		return deferred.promise;
	}
	
	return service;
});