cepApp.factory('FlickrRestFactory', function ($http, awsApiFullUrl) {
	var factory = {};
	
	factory.getFlickrPublicPhotos = function () {
		return $http.get(awsApiFullUrl + '/api/flickr/get-public-photos');
	};
	
	factory.getFlickrGalleryPhotos = function (galleryId) {
		return $http.get(awsApiFullUrl + '/api/flickr/get-flickr-gallery-photos/' + galleryId + '/');
	};
	
	factory.getFlickrPhoto = function (photoId, photoSecret) {
		return $http.get(awsApiFullUrl + '/api/flickr/get-flickr-photo/' + photoId + '/' + photoSecret + '/');
	};
	
	return factory;
});