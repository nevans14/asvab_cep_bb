cepApp.controller('OptionReadyController', function ($http, $window, $location, $scope, sharablesList, photos, modalService, UtilityService, StaticPageService) {
	// metadata
	$scope.absUrl = UtilityService.getCanonicalUrl();
	$scope.mediaUrl = UtilityService.getMediaUrl();
	$scope.imageUrl = $scope.mediaUrl + 'CEP_PDF_Contents/sharables/Landing_Preview_2.jpg';
	// get page, title, and description from db
	StaticPageService.getPageByPageName("option-ready").then(function (results) {
		$scope.title = results.pageTitle;
		$scope.description = results.pageDescription;
		$scope.pageHtml = results.pageHtml;
	});
	// get sharables
	$scope.sharablesList = sharablesList;
	$scope.photos = photos;
	$scope.documents = $scope.mediaUrl + 'CEP_PDF_Contents/';
	$scope.resources = [
		{
			typeId: 1,
			resourceUrl: $scope.documents + 'ASVABCEP_Student_Flyer.pdf',
			resourceThumbnailUrl: $scope.documents + 'thumbnails/student_flyer_thumbnail.png',
			altText: 'Student Flyer',
			isPdf: true,
			title: 'Student Flyer'
		}, {
			typeId: 1,
			resourceUrl: $scope.documents + 'ASVABCEP_Parent_Flyer.pdf',
			resourceThumbnailUrl: $scope.documents + 'thumbnails/parent_flyer_thumbnail.png',
			altText: 'Parent Flyer',
			isPdf: true,
			title: 'Parent Flyer'
		}, {
			typeId: 1,
			resourceUrl: $scope.documents + 'ASVABCEP_Sign_Up_Poster.pdf',
			resourceThumbnailUrl: $scope.documents + 'thumbnails/Poster_thumbnail.jpg',
			altText: 'Sign Up Posters',
			isPdf: true,
			title: 'Sign Up Posters'
		}, {
			typeId: 2,
			resourceUrl: $scope.documents + 'ASVABCEP_Announcements.pdf',
			resourceThumbnailUrl: $scope.documents + 'thumbnails/announcement_thumbnail.png',
			altText: 'ASVAB CEP Announcements',
			isPdf: true,
			title: 'Announcements'
		}, {
			typeId: 2,
			resourceUrl: $scope.documents + 'ASVABCEP_Email_Template.docx',
			resourceThumbnailUrl: $scope.documents + 'thumbnails/email_template_thumbnail.jpg',
			altText: 'Email Template',
			isPdf: false,
			title: 'Email Template'
		}
	];
	$scope.sendToResource = function(url) {
		$window.open(url, '_blank');
	};
	var path = $location.path();
	// photo columns
	$scope.defaultLimit = 15;
	$scope.limit = $scope.defaultLimit; // default limit
	// show less images
	$scope.viewLess = function () {
		var val = $scope.limit - 5;
		if (val > $scope.defaultLimit) {
			$scope.limit -= 5;
		} else {
			val = $scope.limit - $scope.defaultLimit;
			$scope.limit -= val;
		}
	};
	// show more images
	$scope.viewMore = function () {
		$scope.limit += 5;
	};
	// show all
	$scope.viewAll = function () {
		// if one column has more than the other, then that will be the limit
		$scope.limit = $scope.photos.length;
	};
	// shows selected image in a modal
	$scope.showImage = function (index) {
		var selectedPhoto = $scope.photos[index];
		if (selectedPhoto === undefined) return;
		var src = 'https://farm' + selectedPhoto.farm + '.staticflickr.com/' + selectedPhoto.server + '/' + selectedPhoto.id + '_' + selectedPhoto.secret + '.' + selectedPhoto.originalFormat;
		var bodyText =
			'<p class="text-center"><img src="' + src + '" alt=""/></p>';
		var modalOptions = {
			headerText : '',
			bodyText: bodyText
		};
		modalService.show({}, modalOptions);
	};
	// social - snapchat modal
	$scope.snapChatModal = function () {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', path + '#CONNECT_WITH_US_SNAPCHAT' );
		var bodyText =			
			'<p class="text-center"><img style="width: 400px;" src="images/option-ready-snap.png" alt=""/></p>';
		var modalOptions = {
			headerText : 'Connect with us on SnapChat!',
			bodyText : bodyText
		};
		modalService.show({}, modalOptions);
	};
})
.directive('dynamic', function($compile) {
	return {
		restrict: 'A',
		replace: true,
		link: function(scope, ele, attrs) {
			scope.$watch(attrs.dynamic, function (html) {
				ele.html(html);
				$compile(ele.contents())(scope);
				// slick
				angular.element(document).ready(function() {
					$('.sharables').slick({
						dots: false,
						infinite: false,
						speed: 300,
						slidesToShow: 3,
						slidesToScroll: 3,
						responsive: [
							{
								breakpoint: 640,
								settings: {
									slidesToShow: 2,
									slidesToScroll: 2
								}
							},
							{
								breakpoint: 480,
								settings: {
									slidesToShow: 1,
									slidesToScroll: 1
								}
							}
						]
					});
				});
			});
		}
	};
});