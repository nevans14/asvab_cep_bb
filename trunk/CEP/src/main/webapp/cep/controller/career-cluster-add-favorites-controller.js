cepApp.controller('CareerClusterAddFavorite', [ '$scope', 'UserFactory', 'modalService', function($scope, UserFactory, modalService) {
	
	/**
	 * Add to favorites.
	 */
	$scope.addFavorite = function(ccId, title) {

		// get the current favorites list
		var favoritePromise = UserFactory.getCareerClusterFavoritesList();
		favoritePromise.then(function(response) {
			
			var len = response.length;
			
			// check to see if career cluster was already added because we don't want duplicates
			for (var i = 0; i < len; i++) {
				if (response[i].ccId == ccId) {

					// inform user that the career cluster was already added to the list
					var modalOptions = {
						headerText : 'Career Cluster Favorites',
						bodyText : '<b>' + title + '</b>' + ' was already added to the list. Select another career cluster to add.'
					};

					modalService.showModal({}, modalOptions);
					return false;	
				}
			}
			
			// limit 10 career clusters can be added
			if(len >= 2){
				
				// alert user that their favorites is full
				var modalOptions = {
					headerText : 'Career Cluster Favorites',
					bodyText : 'Your favorite career cluster list is full. This career cluster was not added.'
				};

				modalService.showModal({}, modalOptions);
				
				return false;
			}

			// save the career cluster id to the database if not yet saved
			var promise = UserFactory.insertFavoriteCareerCluster(ccId, title);
			promise.then(function(response) {

				//inform user that the career cluster is added to their favorites
				var modalOptions = {
					headerText : 'Career Cluster Favorites',
					bodyText : '<b>' + title + '</b> is added to your favorites.',
				};

				modalService.showModal({}, modalOptions);

			}, function(reason) {
				
				//inform user that there was an error and to try again.
				console.log(reason);
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'There was an error proccessing your request. Please try again.',
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
				return false;
			});
		}, function(reason) {
			console.log(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.',
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
			return false;
		});

	}
} ]);
