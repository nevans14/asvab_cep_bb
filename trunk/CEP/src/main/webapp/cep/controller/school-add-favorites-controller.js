cepApp.controller('SchoolAddFavorites', [ '$rootScope', '$scope', '$route', 'FavoriteRestFactory', 'modalService', 'UserFactory', function($rootScope, $scope, $route, FavoriteRestFactory, modalService, UserFactory) {
	/***
	 * Shows a modal with the given message
	 */
	$scope.showModal = function (message) {
		// prepare and show modal
		var modalOptions = {
			headerText : 'Favorite Schools',
			bodyText: message
		}
		modalService.showModal({}, modalOptions);
	}
	
	/***
	 * Add to favorites.
	 */
	$scope.addFavorite = function() {
		// check if the user already has 10 favorite schools
		if ($scope.favoriteSchools.length >= 10) {
			$scope.showModal('Favorite Schools list is full. Please remove a school before adding this one.');
		}
		// create request obj
		var requestObj = {
			unitId: $route.current.params.unitId
			
		};
		// insert record
		FavoriteRestFactory.insertFavoriteSchool(requestObj).then(function(success) {
			// add to the scope of favorite schools
			$scope.favoriteSchools.push({
				id: success.data.id,
				unitId: parseInt($route.current.params.unitId),
				schoolName: $scope.schoolTitle
			});
			// update local storage
			UserFactory.setSchoolFavorites($scope.favoriteSchools);
			// show message
			$scope.showModal('<strong>' + $scope.schoolTitle + '</strong> is added to your favorites.');
		}, function (error) {
			console.log(error);
		});
	}
	
	/***
	 * Checks if the school is favorited: Return boolean
	 */
	$scope.isFavorited = function (unitId) {		
		if ($rootScope.globals.currentUser) {
			var school = getSchoolInFavorites(unitId);
			return (school ? true : false);
		}
		return false;
	}
	
	/***
	 * Removes School from Favorites
	 */
	$scope.removeFromFavorites = function(unitId) {
		var favoriteSchoolObj = getSchoolInFavorites(unitId);
		if (favoriteSchoolObj) {
			FavoriteRestFactory.deleteFavoriteSchool(favoriteSchoolObj.id).then(function (success) {
				// remove school from favorites
				var index = getIndexOfSchoolInFavorites(unitId);
				$scope.favoriteSchools.splice(index, 1);
				// prepare and show modal
				UserFactory.setSchoolFavorites($scope.favoriteSchools);
				// show message
				$scope.showModal('<strong>' + $scope.schoolTitle + '</strong> was removed from your favorites.');
			}, function (error) {
				console.log(error);
			})
		}
	}
	
	//===============HELPERS=================//	
	/***
	 * Returns a favorited school by UnitID: Returns school (undefined if not found)
	 */
	var getSchoolInFavorites = function (unitId) {
		var school = undefined;
		for (var i = 0; i < $scope.favoriteSchools.length; i++) {
			if ($scope.favoriteSchools[i].unitId === parseInt(unitId)) {
				return school = $scope.favoriteSchools[i];				
			}
		}
		return school;
	}
	
	/***
	 * Returns the index in which the school was in the Favorites: return Int (-1 if not found)
	 */
	var getIndexOfSchoolInFavorites = function(unitId) {
		var index = -1;
		for (var i = 0; i < $scope.favoriteSchools.length; i++) {
			if ($scope.favoriteSchools[i].unitId === parseInt(unitId)) {
				return index = i;
			}
		}
		return index;
	}
} ]);
