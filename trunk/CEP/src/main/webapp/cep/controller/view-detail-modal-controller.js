cepApp.controller('ViewDetailController', [ '$scope', '$rootScope', '$location', 'LoginModalService', function($scope, $rootScope, $location, LoginModalService) {

	$scope.goToBringToSchool = function() {
		$location.path('/asvab-cep-at-your-school');
  }
  $scope.goToLogin = function() {
    LoginModalService.show({
      size : 'md'
    },{});
  }
	
} ]);