cepApp.controller('LeftNavigationController', [ 
	'$scope', '$rootScope', 'UserFactory', 'fyiManualInput', 'PortfolioService', '$location', 'modalService', '$window', 'NotesService', 'resource',
	function($scope, $rootScope, UserFactory, fyiManualInput, PortfolioService, $location, modalService, $window, NotesService, resource) {
	
	if ( typeof $rootScope.globals.currentUser === 'undefined') return;
	
	NotesService.getNotes().then(function(response) {
		$scope.noteList = response;
	});
	
	UserFactory.getNumberOfTestTaken().then(function(response) {
		$scope.numberTestTaken = response;
	});
	
	$scope.isWorkValueResults = angular.fromJson($window.sessionStorage.getItem('workValueTopResults')) ? true : false;
	
	$scope.showScoresTable = false;

	$scope.toggleScoreView = function() {
		$scope.showScoresTable = $scope.showScoresTable === false;
	}
	
	$scope.userRole = $rootScope.globals.currentUser.role;
	
	// Retrieve scores from session.
	$scope.verbalScore = $window.sessionStorage.getItem('sV');
	$scope.mathScore = $window.sessionStorage.getItem('sM');
	$scope.scienceScore = $window.sessionStorage.getItem('sS');
	$scope.afqtScore = $window.sessionStorage.getItem('sA');
	$scope.haveManualAsvabTestScores = $window.sessionStorage.getItem('manualScores') == "true";
	// Retrieve book objects from session.
	$scope.vBooks = angular.fromJson($window.sessionStorage.getItem('vBooks'));
	$scope.mBooks = angular.fromJson($window.sessionStorage.getItem('mBooks'));
	$scope.sBooks = angular.fromJson($window.sessionStorage.getItem('sBooks'));

	$scope.domainName = resource;

	/**
	 * Get scores for display
	 */
	$scope.getScore = function(area) {
		var score = 0;
		switch (area) {
			case "verbal":
				score = $scope.verbalScore;
				break;
			case "math":
				score = $scope.mathScore;
				break;
			case "science":
				score = $scope.scienceScore;
				break;
			case "afqt":
				score = $scope.afqtScore;
				break;
			default:
				break;
		}
		return score;
	}

	/**
	 * Get book sprite x pos
	 */
	$scope.getBookPos = function(area) {
		// return bsackground style position object
		switch (area) {
			case "verbal":
				return !$scope.vBooks ? {} : {"background-position": $scope.vBooks.xPosSmall + "px 0px"}
			case "math":
				return !$scope.mBooks ? {} : {"background-position": $scope.mBooks.xPosSmall + "px 0px"};
			case "science":
				return !$scope.sBooks ? {} : {"background-position": $scope.sBooks.xPosSmall + "px 0px"}; 
		}
	}
	
	$scope.isShowTest = function() {
		if($rootScope.globals.currentUser != undefined)
			var userRole = $rootScope.globals.currentUser.role;
		else
			var userRole = 'S';

		// student role can only
		if (userRole != 'A') {
			if ($scope.numberTestTaken < 2) {
				return true;
			}
			return false;
		}
		return true;
	}
	
	$scope.fyiManualInput = function() {
		fyiManualInput.show({}, {});
	}
	
	/**
	 * Route user to portfolio page.
	 */
	$scope.routeToPortfolio = function() {
		var promise = PortfolioService.getIsPortfolioStarted();
		promise.then(function(success) {

			// if portfolio exists then skip instructions
			if (success > 0) {
				$location.path('/portfolio');
			} else {
				$location.path('/portfolio-directions');
			}
		}, function(error) {
			console.log(error);
		});

	}
	
	// set user interest codes
	UserFactory.getUserInterestCodes().then(function(response) {
		$scope.interestCodeOne = response.interestCodeOne;
		$scope.interestCodeTwo = response.interestCodeTwo;
		$scope.interestCodeThree = response.interestCodeThree;
		$scope.scoreChoice = response.scoreChoice;
	});
	
	$rootScope.$on("updateInterestCodes", function(){
		$scope.loadingIndicator = true;
		UserFactory.getUserInterestCodes().then(function(response) {
			$scope.interestCodeOne = response.interestCodeOne;
			$scope.interestCodeTwo = response.interestCodeTwo;
			$scope.interestCodeThree = response.interestCodeThree;
			$scope.scoreChoice = response.scoreChoice;
			$scope.loadingIndicator = false;
		}, function(error){
			$scope.loadingIndicator = false;
		});
     });

	$scope.resultsInfo = function() {
		var customModalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<p>Consider these scores when exploring careers in the OCCU-Find. Use your highest score to review occupations based on the corresponding skill importance rating. Your top strength will have the most books.</p>' +
				'<p>ASVAB test scores are combined to yield three Career Exploration Scores: Verbal, Math, Science/Technical skills. Each of these scores is made up of a combination of some of the individual ASVAB tests.</p>' +
				'<p><b>Verbal Skills</b> - estimates your potential for verbal activities; derived from the Word Knowledge (WK) and Paragraph Comprehension (PC) test.</p>' +
				'<p><b>Math Skills</b> - estimates your potential for mathematical activities; derived from the Mathematics Knowledge (MK) and Arithmetic Reasoning (AR) tests.</p>' +
				'<p><b>Science/Technical Skills</b> - estimates your potential for science and technical activities, derived from the General Science (GS), Mechanical Comprehension (MC), and Electronics Information (EI) tests.</p>' +
				'<p>These three Career Exploration Scores are good indicators of your potential for success in further education, training, and in occupations. They focus on skills (which are learned and modifiable) rather than on abilities (which are often seen as fixed).</p>'
			};
			var customModalDefaults = {size : 'lg'};
			modalService.show(customModalDefaults, customModalOptions);
	}
	
	$scope.interestCodeInfo = function(interestCode) {
		var modalOptions;
		switch (interestCode) {
		case "R":
			modalOptions = {
				headerText : 'Realistic',
				bodyText : '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
				'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
				'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li></ul>'
			};
			break;
		case "I":
			modalOptions = {
				headerText : 'Investigative',
				bodyText : '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
				'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
				'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li></ul>'
			};
			break;
		case "A":
			modalOptions = {
				headerText : 'Artistic',
				bodyText : '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
				'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
				'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li></ul>'
			};
			break;
		case "S":
			modalOptions = {
				headerText : 'Social',
				bodyText : '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
				'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
				'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li></ul>'
			};
			break;
		case "E":
			modalOptions = {
				headerText : 'Enterprising',
				bodyText : '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
				'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
				'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li></ul>'
			};
			break;
		case "C":
			modalOptions = {
				headerText : 'Conventional',
				bodyText : '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
				'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
				'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li></ul>'
			};
			break;
		}
		

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}
	
	$scope.interestCodesInfo = function() {
		
		var score = $scope.scoreChoice == 'gender' ? 'Gender-Specific' : 'Combined';
		var modalOptions = {
			headerText : 'Interest Codes',
			bodyText : "You're currently using your <strong>" + score + "</strong> scores for career exploration. You have two sets of FYI results, Gender-Specific and Combined. To change the set you're using for career exploration and to learn more about FYI results, go to Return to FYI Results under Step 1."
			/*bodyText : '<p><strong style="color: #2279BF">Realistic</strong></p>' +
			'<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
			'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
			'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li></ul>' +
			'<p><strong style="color: #892C32">Investigative</strong></p>' +
			'<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
			'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
			'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li></ul>' +
			'<p><strong style="color: #F3913D">Artistic</strong></p>' +
			'<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
			'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
			'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li></ul>' +
			'<p><strong style="color: #174377">Social</strong></p>' +
			'<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
			'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
			'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li></ul>' +
			'<p><strong style="color: #ea292d">Enterprising</strong></p>' +
			'<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
			'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
			'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li></ul>' +
			'<p><strong style="color: #52843c">Conventional</strong></p>' +
			'<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
			'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
			'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li></ul>'*/
		};

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}
	
	$scope.showSortByStrength = function () {
		var modalOptions = {
			headerText : 'Career Exploration Scores',
			bodyText : '<p>You can explore occupations that fit your ASVAB strengths. Choose the skill you want to investigate. Consider if it is one of your strengths.</p>' +
			'<p>You\'ll see the occupations organized in three levels of importance: Most Important, Moderately Important, and Less Important. This has to do with how often the skills are used on the job. Explore the importance level that relates to your relative strength in that skill.</p>' + 
			'<p>(For example, is math your top ASVAB strength? Sort by math and explore occupations in the Most Important category. If verbal skills are your lowest strength, sort by verbal and explore the occupations in the less important category).</p>'
		}; 
		
		modalService.showModal({
			size : 'lg'
		}, modalOptions);
	}
	
	/**
	 * 
	 * Popup modal used on Occu-find pages.
	 * 
	 */
	$scope.careerDefinitionsNoCareerCluster = function() {
		var modalOptions = {
			headerText : 'Categories',
			bodyText : "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
			"<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
			// "<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" +
			"<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" + 
			"<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'lg'
		}, modalOptions);
	}
	
	$scope.careerDefinitions = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>Available in the Military</b> indicates occupations that are offered in one of more of the Military Services, each with its own unique requirements.</p>" +
			"<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" + 
			"<p><b>Career Clusters</b> contain occupations in the same field of work that require similar skills. You can use Career Clusters to help focus education plans towards obtaining the necessary knowledge, competencies, and training for success in a particular career pathway.</p>" +
			"<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
			// "<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" + 
			"<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" + 
			"<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'lg'
		}, modalOptions);
	}
	
	$scope.stemDefinition = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" + 
			"<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'md'
		}, modalOptions);
	}
	
	$scope.brightDefinition = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" + 
			"<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'md'
		}, modalOptions);
	}
	
	$scope.greenDefinition = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" +
			"<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'md'
		}, modalOptions);
	}
	
	$scope.returnResultsDisabled = function() {
	      return $location.path() === '/my-asvab-summary-results';
	};
	
	$scope.returnManualResultsDisabled = function() {
	      return $location.path() === '/my-asvab-summary-results-manual';
	};
	
	$scope.occufindDisabled = function() {
	      return $location.path() === '/occufind-occupation-search';
	};
	
	$scope.favoritesDisabled = function () {
		return $location.path() === '/favorites';
	}
	
	$scope.returnFyiDisabled = function() {
	      return $location.path() === '/find-your-interest-score';
	};
	
	$scope.takeFyiDisabled = function() {
	      return $location.path() === '/find-your-interest';
	};
	
	$scope.portfolioDisabled = function() {
	      return $location.path() === '/portfolio' || $location.path() === '/portfolio-directions';
	};

	$scope.educationCareerPlanDisabled = function() {
		return $location.path() === '/education-career-plan';
	};

	$scope.classroomActivitiesDisabled = function() {
		return $location.path() === '/classroom-activities';
	};

	$scope.workValuesHint = function() {
		var modalOptions = {
			headerText : 'Work Values',
			bodyText : "<p>Consider your top work values when exploring occupations and creating your action plan for the future. You can take the work values assessment <a href='work-values-test'>here</a>.</p>"
		};

		modalService.showModal({
			size : 'md'
		}, modalOptions);
	}

} ]);
