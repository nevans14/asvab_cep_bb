cepApp.controller('AddFavorites', [ '$rootScope', '$scope', 'FavoriteRestFactory', 'modalService', '$route', 'UserFactory', function($rootScope, $scope, FavoriteRestFactory, modalService, $route, UserFactory) {

	$scope.showModal = function (message) {
		// prepare and show modal
		var modalOptions = {
			headerText : 'Favorites',
			bodyText : message,
		};
		modalService.showModal({}, modalOptions);
	}
	/**
	 * Checks if the occupation is favorited
	 */
	$scope.isFavorited = function(socId) {
		if ($rootScope.globals.currentUser) {
			return doesOccExistInFavorites(socId);	
		}
		return false;
	}
	
	/**
	 * Remove from favorites
	 */
	$scope.removeFromFavorites = function(socId) {
		var favoriteObj = getOccInFavorites(socId);
		FavoriteRestFactory.deleteFavoriteOccupation(favoriteObj.id).then(function(success) {
			// remove get and remove occ from favorites
			var index = getIndexOfOccInFavorites(socId);
			$scope.favorites.splice(index, 1);
			//update favorites
			UserFactory.setFavorites($scope.favorites)
			// prepare and show modal
			$scope.showModal('<b>' + favoriteObj.title + '</b> was removed from your favorites.');
		}, function (error) {
			console.log(error);
		})
	}
	
	/**
	 * Add to favorites.
	 */
	$scope.addFavorite = function() {		
		if ($scope.favorites.length >= 10) {
			$scope.showModal('Favorite Occupation list is full.');
			return;
		}
		
		var favoriteObject = {
			onetSocCd: $scope.socId
		};
		
		FavoriteRestFactory.insertFavoriteOccupation(favoriteObject).then(function (success) {
			// add to the scope of favorites
			$scope.favorites.push({
				id: success.data.id,
				onetSocCd: $scope.socId,
				title: $scope.occupationTitle
			});
			//update favorites
			UserFactory.setFavorites($scope.favorites)
			$scope.showModal('<b>' + $scope.occupationTitle + '</b> is added to your favorites.');
		}, function (error) {
			console.log(error);
		});
	}
	
	/**
	 * Helpers methods
	 */
	/**
	 * Checks if the current occupation has been favorited
	 */
	var doesOccExistInFavorites = function(socId) {
		var isFound = false;
		for (var i = 0; i < $scope.favorites.length; i++) {
			if ($scope.favorites[i].onetSocCd == socId)
				return isFound = true;
		}
		return isFound;
	}
	
	/**
	 * Returns the occupation in the list of favorites
	 */
	var getOccInFavorites = function(socId) {
		var occ = undefined;
		for (var i = 0; i < $scope.favorites.length; i++) {
			if ($scope.favorites[i].onetSocCd == socId)
				return occ = $scope.favorites[i];
		}
		return occ;
	}
	
	/**
	 * Returns the index of the occupation in the list of favorites
	 */
	var getIndexOfOccInFavorites = function(socId) {
		var index = -1;
		for (var i = 0; i < $scope.favorites.length; i++) {
			if ($scope.favorites[i].onetSocCd == socId)
				return index = i;
		}
		return index;
	}
} ]);
