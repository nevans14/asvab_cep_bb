cepApp.controller('LogOutController', [ '$scope', '$window', '$rootScope', 'UserFactory', 'AuthenticationService', '$location', 'LoginModalService', 'RegisterModalService', 'modalService','ipCookie', 'ssoUrl', '$http', '$timeout', function($scope, $window, $rootScope, UserFactory, AuthenticationService, $location, LoginModalService, RegisterModalService, modalService, ipCookie, ssoUrl, $http, $timeout) {
	$scope.studentSubMenu = $scope.educatorSubMenu = $scope.parentSubMenu = 'none';
	$scope.menu = {
		'student'	: 1,
		'educator'	: 2,
		'parent'	: 3
	};
	
	$scope.toggleSubMenu = function (menu) {
		switch (menu) {
			case $scope.menu.student:
				$scope.studentSubMenu = $scope.studentSubMenu === 'none' ? 'block' : 'none';
				$scope.educatorSubMenu = $scope.parentSubMenu  = 'none';
				break;
			case $scope.menu.educator:
				$scope.educatorSubMenu = $scope.educatorSubMenu === 'none' ? 'block' : 'none';
				$scope.studentSubMenu = $scope.parentSubMenu  = 'none';
				break;
			case $scope.menu.parent:
				$scope.parentSubMenu = $scope.parentSubMenu === 'none' ? 'block' : 'none';
				$scope.studentSubMenu = $scope.educatorSubMenu  = 'none';
				break;
		}
	};
	
	$scope.logout = function() {
		//$window.location.href = '/CEP/perform_logout';
		//$window.location.href = ssoUrl + 'perform_logout';
		//$window.location.href = '/CEP/rest/auth/logout';
		$http.post('/CEP/rest/auth/logout').success(function () {
			// remove frontend session timer
			$timeout.cancel($rootScope.existingTimer);
			$rootScope.existingTimer = undefined;
			// clear credentials
			AuthenticationService.ClearCredentials();
			UserFactory.clearSessionStorage();
			// go home
			$location.path('/');
		});
	};
	
	$scope.goToDashoboard = function() {
		$location.path('/dashboard');
	};
	
	$scope.goTo = function (view) {
		$location.path(view);
	};

	$scope.showLoginModal = function() {
		LoginModalService.show({}, {});
	};
	
	$scope.showRegisterModal = function () {
		RegisterModalService.show({}, {}, undefined);
	};

	/**
	 * Returns boolean value if user is logged in or not.
	 */
	$scope.isLoggedIn = function() {
		return !!$rootScope.globals.currentUser;
	};

} ]);
