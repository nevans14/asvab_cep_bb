cepApp.controller('FooterController', [ '$rootScope', '$scope', '$http', 'UserFactory', 'BringToSchoolModalService', 'appUrl', '$window', '$location', 'modalService', 'awsApiFullUrl', function($rootScope, $scope, $http, UserFactory, BringToSchoolModalService, appUrl, $window, $location, modalService, awsApiFullUrl) {

	$scope.version = '';
	$scope.lastUpdated = '';
	var path = $location.path();
	$http.get(appUrl + "app-info").then(function (res) {
		$scope.version = res.data.version;
		$scope.lastUpdated = res.data.lastUpdated;
	});	
	
	/**
	 * Bring ASVAB to your school modal
	 */
	$scope.asvabModal = function() {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_LINKED' );
		BringToSchoolModalService.show({}, {});
	};
	
	$scope.asvabModal = function(showSchedule) {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_LINKED' );
		BringToSchoolModalService.showModal({}, {showSchedule : showSchedule}); // showModal() sets backdrop to static v.s. show() doesn't
	};

	// NOTE: Test code to auto launch bring cep to school modal
	// $scope.asvabModal();

	$scope.snapChatModal = function () {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', path + '#CONNECT_WITH_US_SNAPCHAT' );
		var bodyText =			
			'<p class="text-center"><img style="width: 400px;" src="images/option-ready-snap.png" alt=""/></p>';
		var modalOptions = {
			headerText : 'Connect with us on SnapChat!',
			bodyText : bodyText
		};
		modalService.show({}, modalOptions);
	};
	
	$scope.isLoggedIn = function() {
		return $rootScope.globals.currentUser;
	}
} ]);
