cepApp.controller('GA', [ '$scope', '$window', '$location', 'baseUrl', function($scope, $window, $location, baseUrl) {

	// google analytics
	$window.ga('create', 'UA-83809749-1', 'auto');

	/**
	 * Track click using Google Analytics.
	 */
	$scope.trackClick = function(path) {
		if (baseUrl.indexOf('www.asvabprogram.com') >= 0) {
			$window.ga('send', 'pageview', path);
		}
	}
	
	/**
	 * Tracks with Google Analytics when ICAT sample test is click and submitted.
	 */
	$scope.trackSampleTestClick = function(param) {
		if (baseUrl.indexOf('www.asvabprogram.com') >= 0) {
			var path = $location.path();
			$window.ga('send', 'pageview', path + param);
		}
	}

	$scope.trackSocialShare = function(socialPlug, value) {
		if (baseUrl.indexOf('www.asvabprogram.com') >= 0) {
			$window.ga('send', 'event', 'Share', socialPlug, value);
		}
	}

} ]);
