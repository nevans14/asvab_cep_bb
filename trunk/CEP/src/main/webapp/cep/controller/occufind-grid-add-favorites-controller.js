cepApp.controller('OccufindGridAddFavoriteController',['$rootScope','$scope', 'FavoriteRestFactory', 'modalService', 'UserFactory', function ($rootScope, $scope, FavoriteRestFactory, modalService, UserFactory) {
	$scope.showModal = function (message) {
		// prepare and show modal
		var modalOptions = {
			headerText : 'Favorites',
			bodyText : message,
		};
		modalService.showModal({}, modalOptions);
	}

	$scope.isUserLoggedIn = ($rootScope.globals.currentUser != undefined ? true : false);
	
	$scope.isFavorited = function (socId) {
		if ($scope.isUserLoggedIn) {
			return doesOccExistInFavorites(socId);			
		}
		return false;
	}
	
	$scope.addToFavorites = function (socId, title) {
		if ($scope.favorites.length >= 10) {
			$scope.showModal('Favorite Occupation list is full.');
			return;
		}
		
		var favoriteObject = {
			onetSocCd: socId,
			userId: $scope.userId
		}
		FavoriteRestFactory.insertFavoriteOccupation(favoriteObject).then(function (success) {
			// add to the scope of favorites
			$scope.favorites.push({
				id: success.data.id,
				userId: parseInt($scope.userId),
				onetSocCd: socId,
				title: title
			});
			//update favorites
			UserFactory.setFavorites($scope.favorites)
			$scope.showModal('<b>' + title + '</b> is added to your favorites.');			
		}, function (error) {
			console.log(error);
		});
	}
	
	$scope.removeFromFavorites = function (socId) {
		var favoriteObj = getOccInFavorites(socId);
		FavoriteRestFactory.deleteFavoriteOccupation(favoriteObj.id).then(function(success) {
			// remove get and remove occ from favorites
			var index = getIndexOfOccInFavorites(socId);
			$scope.favorites.splice(index, 1);
			// update favorites
			UserFactory.setFavorites($scope.favorites)
			// prepare and show modal
			$scope.showModal('<b>' + favoriteObj.title + '</b> was removed from your favorites.');			
		}, function (error) {
			console.log(error);
		})
	}
	
	var doesOccExistInFavorites = function(socId) {
		var isFound = false;
		for (var i = 0; i < $scope.favorites.length; i++) {
			if ($scope.favorites[i].onetSocCd == socId)
				return isFound = true;
		}
		return isFound;
	}
	
	var getOccInFavorites = function(socId) {
		var occ = undefined;
		for (var i = 0; i < $scope.favorites.length; i++) {
			if ($scope.favorites[i].onetSocCd == socId)
				return occ = $scope.favorites[i];
		}
		return occ;
	}
	
	var getIndexOfOccInFavorites = function(socId) {
		var index = -1;
		for (var i = 0; i < $scope.favorites.length; i++) {
			if ($scope.favorites[i].onetSocCd == socId)
				return index = i;
		}
		return index;
	}
}])