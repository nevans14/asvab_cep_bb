cepApp.controller('MobileInterestCodesController', [ '$scope', '$rootScope', 'UserFactory', '$window',function($scope, $rootScope, UserFactory, $window) {

	
	if ( typeof $rootScope.globals.currentUser === 'undefined') return;
	
	UserFactory.getNumberOfTestTaken().then(function(response) {
		$scope.numberTestTaken = response;
	});
	
	if (typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ;
	} else {
		$scope.occuFindUserLoggedIn = true ;
	}
	
	$scope.showScoresTable = false;

	$scope.toggleScoreView = function() {
		$scope.showScoresTable = $scope.showScoresTable === false;
	}
	
	// Retrieve scores from session.
	$scope.verbalScore = $window.sessionStorage.getItem('sV');
	$scope.mathScore = $window.sessionStorage.getItem('sM');
	$scope.scienceScore = $window.sessionStorage.getItem('sS');
	$scope.afqtScore = $window.sessionStorage.getItem('sA');
	$scope.haveManualAsvabTestScores = $window.sessionStorage.getItem('manualScores') == "true";
	// Retrieve book objects from session.
	$scope.vBooks = angular.fromJson($window.sessionStorage.getItem('vBooks'));
	$scope.mBooks = angular.fromJson($window.sessionStorage.getItem('mBooks'));
	$scope.sBooks = angular.fromJson($window.sessionStorage.getItem('sBooks'));

	/**
	 * Get scores for display
	 */
	$scope.getScore = function(area) {
		var score = 0;
		switch (area) {
			case "verbal":
				score = $scope.verbalScore;
				break;
			case "math":
				score = $scope.mathScore;
				break;
			case "science":
				score = $scope.scienceScore;
				break;
			case "afqt":
				score = $scope.afqtScore;
				break;
			default:
				break;
		}
		return score;
	}

	/**
	 * Get book sprite x pos
	 */
	$scope.getBookPos = function(area) {
		// return bsackground style position object
		switch (area) {
			case "verbal":
				return !$scope.vBooks ? {} : {"background-position": $scope.vBooks.xPosSmall + "px 0px"};
			case "math":
				return !$scope.mBooks ? {} : {"background-position": $scope.mBooks.xPosSmall + "px 0px"};
			case "science":
				return !$scope.sBooks ? {} : {"background-position": $scope.sBooks.xPosSmall + "px 0px"}; 
		}
	}
	// set user interest codes
	var promise = UserFactory.getUserInterestCodes();
	if ( typeof  promise == 'undefined' ) return;
	promise.then(function(response) {
		$scope.interestCodeOne = response.interestCodeOne;
		$scope.interestCodeTwo = response.interestCodeTwo;
		$scope.interestCodeThree = response.interestCodeThree;
	});
} ]);
