cepApp.controller('HeaderController', [ '$scope', '$rootScope','UserFactory', '$location', '$anchorScroll', 'NotesService', 'UtilityService', 'DbInfoService', function($scope, $rootScope, UserFactory, $location, $anchorScroll, NotesService, UtilityService, DbInfoService) {

	/***
	 * Get db info
	 */
	DbInfoService.getByName('show_maintenance_banner').then(function (res) {
		$scope.isDisplayBanner = res.data.value;
	});
	
	DbInfoService.getByName('cep_maintenance_banner_text').then(function (res) {
		$scope.maintenanceBannerText = res.data.value;
	})
	
	DbInfoService.getByName('disable_login').then(function (res) {
		$scope.disableLogin = res.data.value;
	});
	
	$scope.absUrl = UtilityService.getCanonicalUrl();
	$scope.isAtHomepage = $location.path() === '/';		
	$scope.status = {
		isopen : $scope.isAtHomepage ? true : false
	};
	// if the status is open and if the user is not logged in, open the login panel, else hide it
	$scope.toggleLogin = $scope.status.isopen && !$rootScope.globals.currentUser ? "block" : "none";
	
	/**
	 * Redirect users to the dashboard
	 */
	$scope.goToDashboard = function() {		
		$location.path('/dashboard');
	}
	/**
	 * Toggles the login dropdown panel
	 */
	$scope.toggleDropdown = function($event) {
		// only on the button to toggle the login
		// if ($event.target.id === 'header-login' && !$rootScope.globals.currentUser && !$scope.isAtHomepage) {
		if ($event.target.id === 'header-login' && !$rootScope.globals.currentUser) {
			$scope.status.isopen = !$scope.status.isopen;
			$scope.toggleLogin = ($scope.status.isopen? 'block' : 'none')
		}
	}
	/**
	 * Determines if the user is currently logged in and on the teachers page
	 */
	$scope.isTeacher = function() {
	    if ($location.path()=='/teachers' || $rootScope.globals.currentUser) {
	    	return true;
	    } else {
	    	return false;
	    }
	}
	/**
	 * Determines if the user is logged in
	 */
	$scope.isLoggedIn = function () {
		return $rootScope.globals.currentUser;
	}
	/**
	 * Redirects the user to the homepage
	 */
	$scope.goHome = function() {
		$location.path('/');
	}
	/**
	 * Redirects the user to the favorites page
	 */
	$scope.goFavorites = function() {
		$location.path('/favorites');
	}

	$scope.noteList = !$rootScope.globals.currentUser ? [] : NotesService.getNotes();

	/**
	 * Look for an id named schollLocation via jQuery.
	 * 
	 * Wait 350 milliseconds for frame work to collapse / expand 
	 * the menus.
	 * 
	 * Then scroll to item in page.
	 * 
	 */
	$scope.schroolToItem = function ( scrollLocation){

		var elm = $("#" + scrollLocation);

		  timeOut = setTimeout(function() {
	          $("body").animate({scrollTop: elm.offset().top}, "slow");
		  }, 350);


	}
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

	/**
	 * Allows header template that is using Foundation and Jquery to work with
	 * ng-include.
	 */
	$scope.$on('$includeContentLoaded', function(event) {
		$(document).foundation();

		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});

	});

} ]);
