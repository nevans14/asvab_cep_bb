cepApp.config(function($routeProvider, $httpProvider, $locationProvider) {
	$httpProvider.interceptors.push('responseObserver');
	$locationProvider.html5Mode(true);
	$routeProvider
	.when('/', {
		templateUrl : 'cep/components/home/page-partials/home.html',
		controller : 'HomeController',
		title: 'ASVAB Career Exploration Program',
		reloadOnSearch : false,
		resolve : {
			mediaCenterList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			},
			currHomepageImage: function(HomeRestFactory) {
				return HomeRestFactory.getCurrentHomepageImg();
			}
		}
	}).when('/passwordReset', {
		templateUrl : 'cep/components/password-reset/page-partials/password-reset.html',
		controller : 'PasswordRestController'
	}).when('/occufind-occupation-details-not-logged-in/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-occupation-details-not-logged-in.html',
		controller : 'OccufindOccupationDetailsNotLoggedInController',
		resolve : {

			occupationCertificates : function(CareerOneStopFactory, $route) {
				if ($route.current.params.socId != null) {
					return CareerOneStopFactory.getOccupationCertificates($route.current.params.socId);
				}
			},

			occupationTitleDescription : function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			},
			stateSalary : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return careerClusterRestFactory.getStateSalary(onetSocTrimmed);
				}
			},
			stateEntrySalary : function (careerClusterRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return careerClusterRestFactory.getStateEntrySalary(onetSocTrimmed);
				}
			},
			skillImportance : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getSkillImportance($route.current.params.socId);
				}
			},
			tasks : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getTasks($route.current.params.socId);
				}
			},
			occupationInterestCodes : function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationInterestCodes($route.current.params.socId);
				}
			},
			educationLevel : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getEducationLevel($route.current.params.socId);
				}
			},
			relatedOccupations : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getRelatedOccupations($route.current.params.socId);
				}
			},
			militaryResources : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getMilitaryResources($route.current.params.socId);
				}
			},
			oohResources : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getOOHResources($route.current.params.socId);
				}
			},
			hotGreenStemFlags : function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getHotGreenStemFlags($route.current.params.socId);
				}
			},
			servicesOffering : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getServiceOfferingCareer($route.current.params.socId);
				}
			},
			relatedCareerCluster : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getRelatedCareerCluster($route.current.params.socId);
				}
			}
		}
	}).when('/dashboard', {
		templateUrl : 'cep/components/dashboard/page-partials/dashboard.html',
		controller : 'DashboardController',
		resolve : {
			numberTestTaken : function(FYIRestFactory, $rootScope, UserFactory) {
				return UserFactory.getNumberOfTestTaken();
			},
			profileExist : function(FYIRestFactory) {
				return FYIRestFactory.getProfileExist();
			},
			userInterestCodes : function(UserFactory) {
				return UserFactory.getUserInterestCodes();
			},
			brightCareers : function(UserFactory, GreenStemBrightRestFactory, $q) {
				var d1 = $q.defer();
				var d2 = $q.defer();
				UserFactory.getUserInterestCodes().then(function(success) {

					GreenStemBrightRestFactory.getBrightOutlook(success.interestCodeOne, success.interestCodeTwo, success.interestCodeThree).then(function(success) {
						d2.resolve(success.data);
					}, function(error) {
						d2.reject(error);
					});
					d1.resolve(success);
				}, function(error) {
					d1.reject(error);
				});

				return $q.all([ d2.promise, d1.promise ]);
			},
			stemCareers : function(UserFactory, GreenStemBrightRestFactory, $q) {
				var d1 = $q.defer();
				var d2 = $q.defer();
				UserFactory.getUserInterestCodes().then(function(success) {

					GreenStemBrightRestFactory.getStemCareers(success.interestCodeOne, success.interestCodeTwo, success.interestCodeThree).then(function(success) {
						d2.resolve(success.data);
					}, function(error) {
						d2.reject(error);
					});
					d1.resolve(success);
				}, function(error) {
					d1.reject(error);
				});

				return $q.all([ d2.promise, d1.promise ]);
			},
			mediaCenterList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			},
			noteList : function(NotesService) {
				// preload notes
				return NotesService.getNotes();
			},
			schoolNoteList : function(SchoolNotesService) {
				// preload notes
				return SchoolNotesService.getNotes();
			},
			tiedLink : function(FYIScoreService, FYIRestFactory, $rootScope, $q) {
				// If there is no top interest codes then there is not a tied
				// score. Set top interest code to undefined.
				/**
				 * Check to see if a test with tied scores exist for user.
				 */
				var d2 = $q.defer();
				// Rest call that returns calculated combined and gender scores.
				FYIRestFactory.getCombinedAndGenderScores().then(function(success) {

					var scores = success.data;

					// Check to see if there are tied scores.
					var isCombinedScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedCombinedScore(scores), 'combined');
					var isGenderScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedGenderScore(scores), 'gender');

					// Reset values
					FYIScoreService.setCombinedTiedScoreObject(undefined);
					FYIScoreService.setGenderTiedScoreObject(undefined);

					var isTiedObject = {
						isCombinedScoresTied : isCombinedScoresTied,
						isGenderScoresTied : isGenderScoresTied
					};

					d2.resolve(isTiedObject);

				}, function(error) {
					d2.reject(error);
				});

				return d2.promise;
			},
			scoreSummary : function(FYIRestFactory, $rootScope, PortfolioRestFactory, $q) {

				var deferred = $q.defer();
				// get test scores
				FYIRestFactory.getTestScore().then(function (data) {
					deferred.resolve(data);
				}, function (e) {
					deferred.reject(e);
				});

				return deferred.promise;
			},
			asvabScore : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getAsvabScore();
			},
			isDemoTaken : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getTeacherDemoIsTaken();
			},
			accessCode : function(AccessCodeService) {
				return AccessCodeService.getAccessCode();
			},
			username : function (UserFactory) {
				return UserFactory.getUsername();
			},
			onetYearUpdated : function (DbInfoService) {
				return DbInfoService.getByName('onet_year_updated');
			},
			isUsingUnregisterableAccesscode : function(UserFactory) {
				return UserFactory.getIsUsingUnregisterableAccesscode();
			},
			workValueScores : function(WorkValuesAPI) {
				return WorkValuesAPI.getWorkValues();
			}
		}
	})
	.when('/education-career-plan', {
		templateUrl: 'cep/components/education-career-plan/page-partials/education-career-plan.html',
		controller: 'EducationCareerPlanController',
		title: 'My Education and Career | ASVAB Career Exploration Program',
		resolve: {
			sections: function (EducationCareerPlanService) {
				return EducationCareerPlanService.getItems();
			},
			responses: function (EducationCareerPlanService) {
				return EducationCareerPlanService.getResponses();
			}
		}
	})
	.when('/green-stem-bright/:category', {
		templateUrl : 'cep/components/green-stem-bright/page-partials/green-stem-bright-occupation-results.html',
		controller : 'GreenStemBrightOccupationController',
		resolve : {
			occupationResults : function(GreenStemBrightRestFactory, $route, $window) {
				var interestCodeOne = $window.sessionStorage.getItem('interestCodeOne');
				var interestCodeTwo = $window.sessionStorage.getItem('interestCodeTwo');
				var interestCodeThree = $window.sessionStorage.getItem('interestCodeThree');
				var category = $route.current.params.category;

				if (category === 'bright') {
					return GreenStemBrightRestFactory.getBrightOutlook(interestCodeOne, interestCodeTwo, interestCodeThree);
				} else if (category === 'stem') {
					return GreenStemBrightRestFactory.getStemCareers(interestCodeOne, interestCodeTwo, interestCodeThree);
				} else if (category === 'green') {
					return GreenStemBrightRestFactory.getGreenCareers(interestCodeOne, interestCodeTwo, interestCodeThree);
				} else if (category === 'all') {
					return GreenStemBrightRestFactory.getAllCareers();
				} else if (category === 'military') {
					return GreenStemBrightRestFactory.getMilitaryCareers();
				}else if (category === 'hot') {
					return GreenStemBrightRestFactory.getHotCareers();
				}
			}, 
			favorites : function($rootScope, $q, FavoriteRestFactory){
				if ($rootScope.globals.currentUser === 'undefined') {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteOccupation().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					console.log(error);
					deferred.reject(error)
				});
				return deferred.promise;
			}
		}
	})
	
	.when('/work-values-intro', {
		templateUrl : 'cep/components/work-values/page-partials/work-values-intro.html',
		controller : 'WorkValuesIntroController',
		resolve : {
		}
	})
	
	.when('/work-values-test', {
		templateUrl : 'cep/components/work-values/page-partials/work-values-test.html',
		controller : 'WorkValuesTestController',
		resolve : {
			testData : function(WorkValuesAPI) {
				return WorkValuesAPI.getTestInformation();
			}
		}
	})
	
	.when('/work-values-tied-scores', {
		templateUrl : 'cep/components/work-values/page-partials/work-values-tied-scores.html',
		controller : 'WorkValuesTiedScoresController',
		resolve : {
		}
	})
	
	.when('/work-values-test-results', {
		templateUrl : 'cep/components/work-values/page-partials/work-values-test-results.html',
		controller : 'WorkValuesTestResultsController',
		resolve : {
		}
	})

	.when('/find-your-interest', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest.html',
		controller : 'FindYourInterestController',
		resolve : {
			mediaCenterList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}
		}
	})

	.when('/find-your-interest-test', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-test.html',
		controller : 'FindYourInterestTestController',
		resolve : {
			questions : function(FYIRestFactory, AccessCodeService, $rootScope, $location, $q) {
				var d1 = $q.defer();
				var d2 = $q.defer();
				FYIRestFactory.getTestQuestions().then(function(success) {
					FYIRestFactory.getNumberTestTaken().then(function(success) {
						AccessCodeService.getAccessCode().then(function (data) {
							if (data.unlimitedFyi === 1) {
								d2.resolve(success);
							} else {
								if (success.data >= 2) {
									d2.reject({ 'reject_message' : 'You can only take two FYI Tests.'});
								}
								d2.resolve(success);
							}
						})						
					}, function(error) {
						d2.reject(error)
					});

					d1.resolve(success);
				}, function(error) {
					d1.reject(error);
				});

				return $q.all([ d1.promise, d2.promise ]);

			}
		}
	})

	.when('/find-your-interest-tied', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-tied.html',
		controller : 'FindYourInterestTiedController',
		resolve : {
			/**
			 * Prevent user from going back to the tied page if tied selection
			 * already made.
			 */
			redirect : function(FYIScoreService, $location, $q) {
				// Gets top combined and gender score from session storage.
				var topCombinedScores = FYIScoreService.getTopCombinedInterestCodes();
				var topGenderScores = FYIScoreService.getTopGenderInterestCodes();

				var defer = $q.defer();
				var istiedLinkClicked = FYIScoreService.getTiedLinkClicked();

				if ((topCombinedScores !== undefined && !istiedLinkClicked) || (topGenderScores !== undefined && !istiedLinkClicked)) {
					$location.path('/find-your-interest-score');
				} else {
					defer.resolve("success");
				}

				return $q.all([ defer.promise ]);
			}
		}
	})

	// This is needed to use manually fyi score input when on the score page.
	.when('/find-your-interest-score-reload', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-score.html',
		controller : 'FindYourInterestScoreController',
		resolve : {
			reload : function($location, $q) {
				var defer = $q.defer();
				$location.path('/find-your-interest-score');
				return defer.promise;
			}
		}
	})
	.when('/find-your-interest-score-shared/:iCodeOne/:iCodeTwo', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-score-shared.html',
		controller : 'FindYourInterestScoreSharedController'		
	})
	.when('/find-your-interest-score', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-score.html',
		controller : 'FindYourInterestScoreController',
		resolve : {
			CombinedAndGenderScores : function(FYIRestFactory, $rootScope, FYIScoreService, $q, $location, $window) {

				var fyiObject = FYIScoreService.getFyiObject();

				$window.sessionStorage.removeItem("FYIQuestions");
				$window.sessionStorage.removeItem("gender");

				// If user is coming from the FYI test page.
				if (fyiObject !== undefined) {
					var defer = $q.defer();
					var deferNumTestTaken = $q.defer();

					// Set tied top scores to default.
					FYIScoreService.setTopGenderInterestCodes(undefined);
					FYIScoreService.setTopCombinedInterestCodes(undefined);
					var tiedObject = {
						combineInterestOne : null,
						combineInterestTwo : null,
						combineInterestThree : null,
						genderInterestOne : null,
						genderInterestTwo : null,
						genderInterestThree : null,
					};
					FYIRestFactory.insertTiedSelection(tiedObject);

					var onSuccess = function(success) {
						// Rest call that returns calculated combined and gender
						// scores.
						FYIRestFactory.getCombinedAndGenderScores().then(function(success) {

							// Rest call to get number of FYI test user has
							// taken.
							FYIRestFactory.getNumberTestTaken().then(function(success) {
								deferNumTestTaken.resolve(success);

								// Reset FYI to enable users to retake the FYI.
								FYIScoreService.setFyiObject(undefined);

							}, function(error) {
								deferNumTestTaken.reject(error);
							});

							var scores = success.data;
							var isCombinedScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedCombinedScore(scores), 'combined');
							var isGenderScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedGenderScore(scores), 'gender');

							// If tied scores then redirect to tie breaker page.
							if (isCombinedScoresTied || isGenderScoresTied) {

								var topCombinedScores = FYIScoreService.getTopCombinedInterestCodes();
								var topGenderScores = FYIScoreService.getTopGenderInterestCodes();

								// If either combined or gender score is tied
								// and user did not break ties then redirect
								// them to tie break page.
								if ((isCombinedScoresTied && topCombinedScores === undefined) || (isGenderScoresTied && topGenderScores === undefined)) {
									$location.path('/find-your-interest-tied');
								}
							}
							defer.resolve(success);
						}, function(error) {
							defer.reject(error);
						});
					}

					var onError = function (error) {
						console.log(error);
					}

					if (!fyiObject.responses) {
						FYIRestFactory.postManualAnswers(fyiObject)
							.success(onSuccess)
							.error(onError);
					} else {
						FYIRestFactory.postAnswers(fyiObject)
							.success(onSuccess)
							.error(onError);
					}
					return $q.all([ defer.promise, deferNumTestTaken.promise ]);

				} else {

					var qScores = $q.defer();
					var tieSelectionPromise = $q.defer();

					// Uses user's data to calculate score and determine if
					// there are ties.
					FYIRestFactory.getCombinedAndGenderScores().then(function(success) {

						var scores = success.data;
						var isCombinedScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedCombinedScore(scores), 'combined');
						var isGenderScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedGenderScore(scores), 'gender');

						// If tied scores then redirect to tie breaker page.
						if (isCombinedScoresTied || isGenderScoresTied) {

							var istiedLinkClicked = FYIScoreService.getTiedLinkClicked();
							var s = undefined;

							FYIRestFactory.getTiedSelection().then(function(success) {

								var data = success.data;

								// If tied link is not clicked on the dashboard
								// page then use database values to recover tie
								// breaker values.
								// This enables the user to come back to the
								// score page without having to be asked to
								// break a tie again.
								if (!istiedLinkClicked) {
									if (data.genderInterestOne && data.genderInterestTwo && data.genderInterestThree) {
										var topGenderInterestCodes = [ {
											interestCd : data.genderInterestOne
										}, {
											interestCd : data.genderInterestTwo
										}, {
											interestCd : data.genderInterestThree
										} ];

										// Set the top gender scores to session
										// storage.
										FYIScoreService.setTopGenderInterestCodes(topGenderInterestCodes);
									}

									if (data.combineInterestOne && data.combineInterestTwo && data.combineInterestThree) {
										var topCombineInterestCodes = [ {
											interestCd : data.combineInterestOne
										}, {
											interestCd : data.combineInterestTwo
										}, {
											interestCd : data.combineInterestThree
										} ];

										// Set the top combine scores to session
										// storage.
										FYIScoreService.setTopCombinedInterestCodes(topCombineInterestCodes);
									}

								} else {

									// User clicked on tie breaker link on the
									// dashboard page.
									// This enables them to break the tie again.
									FYIScoreService.setTopGenderInterestCodes(undefined);
									FYIScoreService.setTopCombinedInterestCodes(undefined);
								}

								// Gets top combined and gender score from
								// session storage.
								var topCombinedScores = FYIScoreService.getTopCombinedInterestCodes();
								var topGenderScores = FYIScoreService.getTopGenderInterestCodes();

								// Determines if user is redirected to the tie
								// page or not.
								if ((isCombinedScoresTied && topCombinedScores === undefined) || (isGenderScoresTied && topGenderScores === undefined)) {
									$location.path('/find-your-interest-tied');
								} else {
									tieSelectionPromise.resolve(success);
								}

							}, function(error) {
								tieSelectionPromise.reject(error);
							});

						} else {
							tieSelectionPromise.resolve();
						}
						qScores.resolve(success);
					}, function(error) {
						qScores.reject(error)
					});

					// A promise for number of test taken.
					var numberTestTaken = $q.defer();
					FYIRestFactory.getNumberTestTaken().then(function(success) {
						numberTestTaken.resolve(success);
					}, function(error) {
						numberTestTaken.reject(error);
					});

					// Proceeds if all promises are resolved.
					return $q.all([ qScores.promise, numberTestTaken.promise, tieSelectionPromise.promise ]);

				}
			},
			scoreChoice : function(FYIRestFactory) {
				return FYIRestFactory.getScoreChoice();
			},
			accessCode : function(AccessCodeService) {
				return AccessCodeService.getAccessCode();
			}
		}
	})

	.when('/my-asvab-summary-results', {
		templateUrl : 'cep/components/find-your-interest/page-partials/my-asvab-summary-results.html',
		controller : 'MyAsvabSummaryController',
		resolve : {
			scoreSummary : function(FYIRestFactory, $q) {

				var deferred = $q.defer();

				FYIRestFactory.getTestScore().then(function (data) {
					deferred.resolve(data);
				}, function (e) {
					deferred.reject(e);
				});

				return deferred.promise;
			},
			asvabScore : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getAsvabScore();
			}
		}
	})

//	.when('/cep-manual-input', {
//		templateUrl : 'cep/components/dashboard/page-partials/cep-manual-input.html',
//		controller : 'ManualInputController',
//		resolve : {
//			asvabScore : function(PortfolioRestFactory, $rootScope) {
//				var userId = $rootScope.globals.currentUser.userId;
//				return PortfolioRestFactory.getAsvabScore(userId);
//			}
//		}
//	})
	
	.when('/my-asvab-summary-results-manual', {
		templateUrl : 'cep/components/find-your-interest/page-partials/my-asvab-summary-results-manual.html',
		controller : 'FindYourInterestSummaryManualController',
		resolve : {
			asvabScore : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getAsvabScore();
			}
		}
	})

	.when('/find-your-interest-tied-score', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-tied-score.html',
		controller : 'FindYourInterestTiedScoreController'
	})

	.when('/occufind-occupation-search', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-occupation-search.html',
		controller : 'OccufindOccupationSearchController',
		resolve : {
			mediaCenterList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}
		}
	})

	.when('/occufind-occupation-search-results', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-occupation-search-results.html',
		controller : 'OccufindOccupationSearchResultsController',
		reloadOnSearch : false,
		resolve : {
			occupationResults : function(OccufindRestFactory, OccufindSearchService) {
				return OccufindRestFactory.getOccupationResults(angular.toJson(OccufindSearchService.userSearch));
			},
			favorites : function($rootScope, $q, FavoriteRestFactory){
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteOccupation().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					console.log(error);
					deferred.reject(error)
				});
				return deferred.promise;
			},
			onetVersion : function(DbInfoService) {
				return DbInfoService.getByName('onet_version');
			}
		}
	})

	.when('/occufind-occupation-details/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-occupation-details.html',
		controller : 'OccufindOccupationDetailsController',
		resolve : {
			occupationCertificates : function(CareerOneStopFactory, $route) {
				if ($route.current.params.socId != null) {
					return CareerOneStopFactory.getOccupationCertificates($route.current.params.socId);
				}
			},
			occupationTitleDescription : function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			},
			stateSalaryYear : function(OccufindRestFactory) {
				return OccufindRestFactory.getStateSalaryYear();
			},
			nationalPovertyLevel : function(OccufindRestFactory) {
				return OccufindRestFactory.getNationalPovertyLevel();
			},
			stateSalary : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return careerClusterRestFactory.getStateSalary(onetSocTrimmed);
				}
			},
			stateEntrySalary : function (careerClusterRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return careerClusterRestFactory.getStateEntrySalary(onetSocTrimmed);
				}
			},
			nationalSalary : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return OccufindRestFactory.getNationalSalary(onetSocTrimmed);
				}
			},
			nationalEntrySalary : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return OccufindRestFactory.getNationalEntrySalary(onetSocTrimmed);
				}
			},
			blsTitle : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return OccufindRestFactory.getBLSTitle(onetSocTrimmed);
				}
			},
			skillImportance : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getSkillImportance($route.current.params.socId);
				}
			},
			tasks : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getTasks($route.current.params.socId);
				}
			},
			occupationInterestCodes : function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationInterestCodes($route.current.params.socId);
				}
			},
			educationLevel : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getEducationLevel($route.current.params.socId);
				}
			},
			relatedOccupations : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getRelatedOccupations($route.current.params.socId);
				}
			},
			hotGreenStemFlags : function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getHotGreenStemFlags($route.current.params.socId);
				}
			},
			servicesOffering : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getServiceOfferingCareer($route.current.params.socId);
				}
			},
			relatedCareerCluster : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getRelatedCareerCluster($route.current.params.socId);
				}
			},
			moreResources : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getMoreResources($route.current.params.socId);
				}
			},
			noteList : function(NotesService) {
				// preload notes
				NotesService.getNotes();
			},
			schoolDetails : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getSchoolMoreDetails($route.current.params.socId);
				}
			},
			getAlternateView : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getAlternateView($route.current.params.socId);
				}
			},
			imageAltText : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getImageAltText($route.current.params.socId);
				}
			},
			jobZone : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getJobZone($route.current.params.socId);
				}
			},
			certificateExplanation : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return OccufindRestFactory.getCertificateExplanation(onetSocTrimmed);
				}
			}
			,
			alternateTitles : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getAlternateTitles($route.current.params.socId);
				}
			}
			,
			favorites : function($rootScope, $q, FavoriteRestFactory){
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteOccupation().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					console.log(error);
					deferred.reject(error)
				});
				return deferred.promise;
			},
			workValues : function($rootScope, OccufindRestFactory, $route) {
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationWorkValues($route.current.params.socId);
				}
			}
		}
	})

	.when('/occufind-education-details/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-education-details.html',
		controller : 'OccufindEducationDetailsController',
		resolve : {
			occupationTitleDescription : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			},
			schoolDetails : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getSchoolMoreDetails($route.current.params.socId);
				}
			},
			schoolMajors : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getSchoolMajors($route.current.params.socId);
				}
			},
			favorites : function($rootScope, $q, FavoriteRestFactory){
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteOccupation().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					console.log(error);
					deferred.reject(error)
				});
				return deferred.promise;
			}
		}
	})
	
	.when('/occufind-school-details/:socId/:unitId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-school-details.html',
		controller : 'OccufindSchoolDetailsController',
		resolve : {
			occupationTitleDescription : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			},
			schoolDetails : function(OccufindRestFactory, $route) {
				if ($route.current.params.unitId != null) {
					return OccufindRestFactory.getSchoolProfile($route.current.params.unitId);
				}
			},
			favoriteSchools : function ($rootScope, $q, FavoriteRestFactory) {
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteSchool().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					deferred.reject(error);
				});
				return deferred.promise;
			},
			favorites : function($rootScope, $q, FavoriteRestFactory){
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteOccupation().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					console.log(error);
					deferred.reject(error)
				});
				return deferred.promise;
			},
			ipedsYearUpdated : function(OccufindRestFactory) {
					return OccufindRestFactory.getIpedsYearUpdated();
			},
		}
	})

	.when('/occufind-employment-details/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-employment-details.html',
		controller : 'OccufindEmploymentDetailsController',
		resolve : {
			stateSalaryYear : function(OccufindRestFactory) {
				return OccufindRestFactory.getBlsProjectionYearUpdated();
			},
			occupationTitleDescription : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			},
			employmentMoreDetails : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return OccufindRestFactory.getEmploymentMoreDetails(onetSocTrimmed);
				}
			},
			favorites : function($rootScope, $q, FavoriteRestFactory){
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteOccupation().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					console.log(error);
					deferred.reject(error)
				});
				return deferred.promise;
			}
		}
	})

	.when('/occufind-military-details/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-military-details.html',
		controller : 'OccufindMilitaryDetailsController',
		resolve : {
			occupationTitleDescription : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			},
			militaryMoreDetails : function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSoc = $route.current.params.socId;
					return OccufindRestFactory.getMilitaryMoreDetails(onetSoc);
				}
			},
			militaryResources : function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getMilitaryResources($route.current.params.socId);
				}
			},
			favorites : function($rootScope, $q, FavoriteRestFactory){
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteOccupation().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					console.log(error);
					deferred.reject(error)
				});
				return deferred.promise;
			}
		}
	})
	/**
	 * this view will render the CITM (Careers in the Military) website inside of an iframe
	 */
	.when('/citm', {
		templateUrl : 'cep/components/careers-in-the-military/page-partials/citm.html',
		controller : 'CitmController'
	})
	.when('/career-cluster', {
		templateUrl : 'cep/components/career-cluster/page-partials/career-cluster.html',
		controller : 'CareerClusterController',
		resolve : {
			career : function(careerClusterRestFactory) {
				return careerClusterRestFactory.getCareerCluster();
			}
		}
	})

	.when('/career-cluster-occupation-results/:ccId', {
		templateUrl : 'cep/components/career-cluster/page-partials/career-cluster-occupation-results.html',
		controller : 'CareerClusterOccupationController',
		resolve : {
			occupationResults : function(careerClusterRestFactory, $route) {
				return careerClusterRestFactory.getCareerClusterOccupations($route.current.params.ccId);
			},
			careerClusterById : function(careerClusterRestFactory, $route) {
				return careerClusterRestFactory.getCareerClusterById($route.current.params.ccId);
			},
			favorites : function($rootScope, $q, FavoriteRestFactory){
				if ($rootScope.globals.currentUser === undefined) {
					return undefined;
				}
				var deferred = $q.defer();
				FavoriteRestFactory.getFavoriteOccupation().then(function (success) {
					deferred.resolve(success);
				}, function (error) {
					console.log(error);
					deferred.reject(error)
				});
				return deferred.promise;
			}
		}
	})

	.when('/career-cluster-occupation-details', {
		templateUrl : 'cep/components/career-cluster/page-partials/career-cluster-occupation-details.html',
		controller : 'CareerClusterOccupationDetailsController',
		resolve : {
			occupationTitleDescription : function(OccufindRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					return OccufindRestFactory.getOccupationTitleDescription(careerClusterService.selectedOnetSoc);
				}
			},
			stateSalary : function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					var onetSocTrimmed = careerClusterService.selectedOnetSoc.slice(0, -3);
					return careerClusterRestFactory.getStateSalary(onetSocTrimmed);
				}
			},
			skillImportance : function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					return careerClusterRestFactory.getSkillImportance(careerClusterService.selectedOnetSoc);
				}
			},
			tasks : function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					return careerClusterRestFactory.getTasks(careerClusterService.selectedOnetSoc);
				}
			},
			occupationInterestCodes : function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					return careerClusterRestFactory.getOccupationInterestCodes(careerClusterService.selectedOnetSoc);
				}
			},
			educationLevel : function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					return careerClusterRestFactory.getEducationLevel(careerClusterService.selectedOnetSoc);
				}
			},
			relatedOccupations : function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					return careerClusterRestFactory.getRelatedOccupations(careerClusterService.selectedOnetSoc);
				}
			},
			militaryResources : function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					return careerClusterRestFactory.getMilitaryResources(careerClusterService.selectedOnetSoc);
				}
			},
			oohResources : function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc !== undefined) {
					return careerClusterRestFactory.getOOHResources(careerClusterService.selectedOnetSoc);
				}
			}
		}
	})

	.when('/career-cluster-public', {
		templateUrl : 'cep/components/career-cluster/page-partials/career-cluster-public.html',
		controller : 'CareerClusterPublicController',
		resolve : {
			career : function(careerClusterRestFactory) {
				return careerClusterRestFactory.getCareerCluster();
			}
		}
	})

	.when('/career-cluster-pathway-:ccPathwayId', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/pathway.html',
		controller : 'CareerClusterPathwayController',
		resolve : {
			ccPathways : function(careerClusterRestFactory, $route) {
				return careerClusterRestFactory.getCareerClusterById($route.current.params.ccPathwayId);
			}
		}
	})

	.when('/classroom-activities', {
		templateUrl : 'cep/components/dashboard/page-partials/classroom-activities.html',
		controller : 'ClassroomActivitiesController',
		resolve : {
			classroomActivities : function(ClassroomRestFactory) {
				return ClassroomRestFactory.getClassroomActivities();
			},
			classroomActivityCategories : function(ClassroomRestFactory) {
				return ClassroomRestFactory.getClassroomActivityCategories();
			}
		}
	})
	
	.when('/portfolio-directions', {
		templateUrl : 'cep/components/portfolio/page-partials/portfolio-directions.html',
		controller : 'PortfolioDirectionsController'
	})

	.when('/portfolio', {
		templateUrl : 'cep/components/portfolio/page-partials/portfolio.html',
		controller : 'PortfolioController',
		resolve : {
			workExperience : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getWorkExperience();
			},
			education : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getEducation();
			},
			achievement : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getAchievements();
			},
			interest : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getInterests();
			},
			skill : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getSkills();
			},
			workValue : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getWorkValues();
			},
			volunteer : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getVolunteers();
			},
			actScore : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getActScore();
			},
			asvabScore : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getAsvabScore();
			},
			fyiScore : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getFyiScore();
			},
			satScore : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getSatScore();
			},
			otherScore : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getOtherScore();
			},
			favoriteList : function(UserFactory) {
				return UserFactory.getFavoritesList();
			},
			careerClusterFavoriteList : function(UserFactory) {
				return UserFactory.getCareerClusterFavoritesList();
			},
			plan : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getPlan();
			},
			UserInfo : function(PortfolioRestFactory) {
				return PortfolioRestFactory.selectNameGrade();
			},
			citmFavoriteOccupations : function(PortfolioRestFactory) {
				return PortfolioRestFactory.getCitmFavoriteOccupations();
			},
			schoolFavoriteList : function(UserFactory) {
				return UserFactory.getSchoolFavoritesList();
			},
			isRegistered : function (PortfolioRestFactory) {
				return PortfolioRestFactory.isRegistered();
			}
		}
	})

	.when('/favorites', {
		templateUrl : 'cep/components/favorites/page-partials/favorites.html',
		controller : 'FavoritesController',
		resolve : {
			favoriteList : function(UserFactory) {
				return UserFactory.getFavoritesList();
			},
			careerClusterFavoriteList : function(UserFactory) {
				return UserFactory.getCareerClusterFavoritesList();
			},
			schoolFavoriteList : function(UserFactory) {
				return UserFactory.getSchoolFavoritesList();
			},
			scoreSummary : function(FYIRestFactory, $q) {
				var deferred = $q.defer();

				FYIRestFactory.getTestScore().then(function (data) {
					deferred.resolve(data);
				}, function (e) {
					deferred.reject(data);
				});

				return deferred.promise;
			}
		}
	})

	// Option Ready
	.when('/option-ready', {
		controller	: 	'OptionReadyController',
		title		:	'Option Ready | ASVAB Career Exploration Program',
		templateUrl	:	'cep/components/option-ready/page-partials/option-ready.html',
		resolve		: 	{
			sharablesList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterByCategoryId(9);
			},
			photos : function (FlickrService) {
				return FlickrService.getFlickrPublicPhotos();
			}
		}
	})

	.when('/continuing-education', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/ce-opportunity.html'
	})

	.when('/countdown', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/countdown.html'
	})
	
	// Media Center
	.when('/media-center', {
		controller 	:	'MediaCenterController',
		templateUrl :	'cep/components/media-center/page-partials/media-center.html',			
		resolve 	:	{
			mediaCenterList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			},
			inTheNewsList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterByCategoryId(7);
			},
			testimonialsList : function (MediaCenterService) {
				return MediaCenterService.getMediaCenterByCategoryId(8);
			},
			sharablesList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterByCategoryId(9);
			}
		}
	})

	.when('/media-center-article-list/:categoryId', {		
		templateUrl : 'cep/components/media-center/page-partials/media-center-article-list.html',
		controller : 'MediaCenterArticleListController',
		resolve : {
			mediaCenterList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}
		}
	})
	.when('/media-center-article/:mediaId', {
		templateUrl : 'cep/components/media-center/page-partials/media-center-article.html',
		controller : 'MediaCenterArticleController',
		resolve : {
			mediaCenterList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			},
			mediaCenterArticle : function(MediaCenterService, $route) {
				var mediaId = $route.current.params.mediaId;
				return MediaCenterService.getMediaCenterArticleById(mediaId);
			}
		}
	})
	.when('/media-center-article/:categoryName/:slug', {
		templateUrl : 'cep/components/media-center/page-partials/media-center-article.html',
		controller : 'MediaCenterArticleController',
		resolve : {
			mediaCenterList : function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			},
			mediaCenterArticle : function(MediaCenterService, $route) {
				var categoryName = $route.current.params.categoryName.toLowerCase();
				var slug = $route.current.params.slug.toLowerCase();
				return MediaCenterService.getMediaCenterArticleByCategoryNameSlug(categoryName, slug);
			}
		}
	})
	.when('/search-result/', {
		templateUrl : 'cep/components/search/page-partials/search-result.html'
		/*controller : 'SearchResultController',
		resolve : {
			searchResult : function(SearchService, $route) {
				var searchString = $route.current.params.searchString;
				if (searchString) {
					return SearchService.getSearchResult(searchString);	
				}
				// no search parameter, redirect back to home
				$location.url('/');
			}
		}*/
	})
	/* Training Session Registration */
	.when('/training-registration-form/:trainingId', {
		controller: 'EssTrainingController',
		title: 'Training Session Registration | ASVAB Career Exploration Program',
		templateUrl: 'cep/components/ess-training/page-partials/training-registration-form.html',
		resolve: {
			trainingData : function (EssTrainingRestFactory, $q, $route) {
				if ($route.current.params.trainingId === undefined) {
					return undefined;
				} 			
				var deferred = $q.defer();
				
				EssTrainingRestFactory.getEssTrainingById($route.current.params.trainingId).then(function (success) {
					deferred.resolve(success.data);
				}, function (error) {
					deferred.reject(error);
				});
				
				return deferred.promise;
			},
			alternateTraining: function(EssTrainingService, $q, $route) {
				if ($route.current.params.trainingId === undefined) {
					return undefined;
				} 			
				var deferred = $q.defer();

				EssTrainingService.getOpenSlots($route.current.params.trainingId).then(function (success) {
					deferred.resolve(success.data);
				}, function (error) {
					deferred.reject(error);
				});
				
				return deferred.promise;
			}
		}
	})
	
	.when('/compare-occupation/:socIdOne/:socIdTwo/:socIdThree', {
		templateUrl : 'cep/components/compare-occupation/page-partials/compare-occupation.html',
		controller : 'CompareOccupationController',
		resolve : {
			occupationOne : function(OccufindRestFactory, careerClusterRestFactory, NotesRestFactory, $route, $q, $rootScope) {

				var title = OccufindRestFactory.getOccupationTitleDescription($route.current.params.socIdOne);
				var skillImportance = careerClusterRestFactory.getSkillImportance($route.current.params.socIdOne);
				var occupationInterestCodes = OccufindRestFactory.getOccupationInterestCodes($route.current.params.socIdOne);
				var educationLevel = careerClusterRestFactory.getEducationLevel($route.current.params.socIdOne);
				var servicesOffering = OccufindRestFactory.getServiceOfferingCareer($route.current.params.socIdOne);
				var onetSocTrimmed = $route.current.params.socIdOne.slice(0, -3);
				var nationalSalary = OccufindRestFactory.getNationalSalary(onetSocTrimmed);
				var notes = NotesRestFactory.getOccupationNotes({
					socId : $route.current.params.socIdOne
				});

				return $q.all([ title, skillImportance, occupationInterestCodes, educationLevel, servicesOffering, nationalSalary, notes ]);
			},
			occupationTwo : function(OccufindRestFactory, careerClusterRestFactory, NotesRestFactory, $route, $q, $rootScope) {

				var title = OccufindRestFactory.getOccupationTitleDescription($route.current.params.socIdTwo);
				var skillImportance = careerClusterRestFactory.getSkillImportance($route.current.params.socIdTwo);
				var occupationInterestCodes = OccufindRestFactory.getOccupationInterestCodes($route.current.params.socIdTwo);
				var educationLevel = careerClusterRestFactory.getEducationLevel($route.current.params.socIdTwo);
				var servicesOffering = OccufindRestFactory.getServiceOfferingCareer($route.current.params.socIdTwo);
				var onetSocTrimmed = $route.current.params.socIdTwo.slice(0, -3);
				var nationalSalary = OccufindRestFactory.getNationalSalary(onetSocTrimmed);
				var notes = NotesRestFactory.getOccupationNotes({
					socId : $route.current.params.socIdTwo
				});

				return $q.all([ title, skillImportance, occupationInterestCodes, educationLevel, servicesOffering, nationalSalary, notes ]);
			},
			occupationThree : function(OccufindRestFactory, careerClusterRestFactory, NotesRestFactory, $route, $q, $rootScope) {

				if ($route.current.params.socIdThree !== "undefined") {
					var title = OccufindRestFactory.getOccupationTitleDescription($route.current.params.socIdThree);
					var skillImportance = careerClusterRestFactory.getSkillImportance($route.current.params.socIdThree);
					var occupationInterestCodes = OccufindRestFactory.getOccupationInterestCodes($route.current.params.socIdThree);
					var educationLevel = careerClusterRestFactory.getEducationLevel($route.current.params.socIdThree);
					var servicesOffering = OccufindRestFactory.getServiceOfferingCareer($route.current.params.socIdThree);
					var onetSocTrimmed = $route.current.params.socIdThree.slice(0, -3);
					var nationalSalary = OccufindRestFactory.getNationalSalary(onetSocTrimmed);
					var notes = NotesRestFactory.getOccupationNotes({
						socId : $route.current.params.socIdThree
					});

					return $q.all([ title, skillImportance, occupationInterestCodes, educationLevel, servicesOffering, nationalSalary, notes ]);
				} else {
					return undefined;

				}
			}
		}
	}).when('/faq', {
		title : 'FAQ ASVAB CEP',
		templateUrl : 'cep/components/faq/page-partials/faq.html',
		controller : 'FaqController'
	}).when('/score-request', {
		title : 'SCORE REQUEST',
		templateUrl : 'cep/components/home/page-partials/home.html',
		controller : 'ScoreRequestController'
	}).when('/register', {
		title : 'REGISTER',
		templateUrl : 'cep/components/home/page-partials/home.html',
		controller : 'RegisterModalController'
	})

	/**
	 * Static pages
	 */
	// general static pages
	.when('/general-help', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/general-help.html'
	}).when('/general-link', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/general-link.html'
	}).when('/general-occufind-view-all', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/general-occufind-view-all.html'
	}).when('/general-privacy-security', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/general-privacy-security.html'
	// }).when('/general-resources', {
	// 	controller: 'StaticPageMasterController',
	// 	title : 'Career Planning Resources | ASVAB Career Exploration Program',
	// 	templateUrl : 'cep/components/static-pages/page-partials/general-resources.html',
	// 	reloadOnSearch: false
	}).when('/general-resources', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/general-resources.html',
		reloadOnSearch: false
	}).when('/general-site', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/general-site.html'
	}).when('/asvab-cep-at-your-school', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/asvab-cep-at-your-school.html'
	}).when('/legislation', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/legislation.html'
	})
	// Teacher Static Pages
	.when('/federally-funded-sources', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/federally-funded-sources.html'
	}).when('/educators-post-test', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/educators-post-test.html'
	}).when('/educators-test', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/educators-test.html'
	}).when('/educators', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/educators.html'
	}).when('/teachers', {
		controller : 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/educators-teacher-landing.html'
	})
	// Parent Static Pages
	.when('/parents-information', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/parents-information.html'
	}).when('/parents-post-test', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/parents-post-test.html'
	}).when('/parents-test', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/parents-test.html'
	}).when('/parents', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/parents.html'
	})
	// Student Static Pages
	.when('/student', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/student.html'
	}).when('/student-explore', {
		controller: 'StaticPageMasterController',		
		templateUrl : 'cep/components/static-pages/page-partials/student-explore.html'
	})
	// .when('/student-explore-action-steps', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-explore-action-steps.html'
	// })
	// .when('/student-explore-occu-find', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-explore-occu-find.html'
	// })
	// .when('/student-explore-cc', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-explore-cc.html'
	// })
	.when('/student-learn', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/student-learn.html'
	})
	// .when('/student-learn-interest-area', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-learn-interest-area.html'
	// }).when('/student-learn-interests', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-learn-interests.html'
	// })
	// .when('/student-learn-skills', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-learn-skills.html'
	// })
	// .when('/student-learn-work-values', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-learn-work-values.html'
	// })
	.when('/student-plan', {
		controller: 'StaticPageMasterController',
		templateUrl : 'cep/components/static-pages/page-partials/student-plan.html'
	})
	// .when('/student-plan-tools', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-plan-tools.html'
	// })
	// .when('/student-plan-understanding', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-plan-understanding.html'
	// })
	// .when('/student-post-test', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-post-test.html'
	// })
	// .when('/student-program', {
	// 	controller: 'StaticPageMasterController',
	// 	templateUrl : 'cep/components/static-pages/page-partials/student-program.html'
	// })		
	//.otherwise({ redirectTo: '/' }); // removed because it was redirecting users to the homepage from outside links (e.g., from Google)
});

cepApp.run(function($rootScope, $location, ipCookie, $http, $timeout, UserFactory, modalService, sessionKeepaliveModalService, $window, $route, durationToTimeoutReminder) {

	// google analytics
	$window.ga('create', 'UA-83809749-1', 'auto');

	// initialize foundation for sites to work with angular
	$rootScope.$on('$viewContentLoaded', function() {
		$(document).foundation();
	});

	// keep user logged in after page refresh
	$rootScope.globals = ipCookie('globals') || {};
	if ($rootScope.globals.currentUser) {
		$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint
		// ignore:line
	}

	$rootScope.$on("$routeChangeStart", function(event, next, current) {
	});

	$rootScope.$on('$routeChangeSuccess', function() {
		if ($route.current && $route.current.title) {
			document.title = $route.current.title;
		} else {
			document.title = 'ASVAB Career Exploration Program';
		}
	});

	$rootScope.$on("$routeChangeError", function(event, current, previous, rejection, $window) {
		console.log("Failed to change routes.");
		var modalOptions = {};
		if (!!previous) {
			// Route to previous path.
			$location.path(previous.$$route.originalPath);
			// Alert user there was an error.
			modalOptions = {
				headerText : 'Error',
				bodyText : (rejection && rejection.reject_message) ? rejection.reject_message : 'There was an error processing your request! Please try again. If error persists, please contact the site administrator.'
			};
		} else {
			$location.path('/');
			// Alert user there was an error.
			modalOptions = {
				headerText : 'Error',
				bodyText : (rejection && rejection.reject_message) ? rejection.reject_message : 'There was an error processing your request! Please try again. If error persists, please contact the site administrator.'
			};
		}
		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	});

	$rootScope.existingTimer = undefined;
	$rootScope.sessionKeepAlive = function() {
			$rootScope.existingTimer = $timeout(function() {
				modalOptions = {
					headerText : 'Session Expiration',
					bodyText : 'Your session is about to expire. What would you like to do?',
				};
				sessionKeepaliveModalService.showModal({
					size : 'sm'
				}, modalOptions);
				$rootScope.existingTimer = undefined;
		}, durationToTimeoutReminder)
	}

	$rootScope.$on('$locationChangeStart', function(event, next, current) {
		
		// Setup keep alive modal for active session
		if ($rootScope.globals.currentUser) {
			if (!$rootScope.existingTimer) {
				$rootScope.sessionKeepAlive();
			} else {
				$timeout.cancel($rootScope.existingTimer);
				$rootScope.existingTimer = undefined;
				$rootScope.sessionKeepAlive();
			}
		}

		// google analytics track page
		$window.ga('send', 'pageview', $location.path());
		var firstSlashPos = $location.path().indexOf('/') + 1;
		var appParamPath = $location.path().substr(firstSlashPos, $location.path().lastIndexOf('/') - 1);
		var restrictedParamPage = $.inArray(appParamPath, [ 'training-registration-form', 'search-results', 'search-result-public', 'media-center-article-list', 'media-center-article' ]) === -1;

		var lastSlashPos = $location.path().lastIndexOf('/') + 1;
		var appPath = $location.path().substr(1);
		var numOfSlashes = (appPath.match(new RegExp("/", "g")) || []).length; // logs?
		if (numOfSlashes > 0) {
			var pathArray = appPath.split("/");
			appPath = pathArray[0];
		}
		// redirect to login page if not logged in and trying to access a
		// restricted page
		var restrictedPage = $.inArray(appPath, [ 'countdown', 'continuing-education', 'search-result', 'training-registration-form', 'passwordReset', 'occufind-occupation-details', 'occufind-education-details', 'occufind-school-details', 'occufind-military-details', 'occufind-employment-details', 'faq', 'media-center', 'career-cluster-public', 'career-cluster-pathway-1', 'career-cluster-pathway-2', 'career-cluster-pathway-3', 'career-cluster-pathway-4', 'career-cluster-pathway-5', 'career-cluster-pathway-6', 'career-cluster-pathway-7', 'career-cluster-pathway-8', 'career-cluster-pathway-9', 'career-cluster-pathway-10', 'career-cluster-pathway-11', 'career-cluster-pathway-12', 'career-cluster-pathway-13', 'career-cluster-pathway-14', 'career-cluster-pathway-15', 'career-cluster-pathway-16', 'federally-funded-sources', 'educators-post-test', 'educators-test', 'educators', 'teachers', 'general-help', 'general-link', 'general-occufind-view-all', 'general-privacy-security', 'general-resources', 'general-site', 'parents-information', 'parents-post-test', 'parents-test', 'parents', 'student-explore-action-steps', 'student-explore-cc', 'student-explore-occu-find', 'student-explore', 'student-learn-interest-area', 'student-learn-interests', 'student-learn-skills', 'student-learn-work-values', 'student-learn', 'student-plan-tools', 'student-plan-understanding', 'student-plan', 'student-post-test', 'student-program', 'student', 'asvab-cep-at-your-school', 'teacher', 'legislation', 'score-request', 'register', 'option-ready', 'media-center-article' ]) === -1;
		var loggedIn = $rootScope.globals.currentUser;

		if (restrictedPage && !loggedIn && restrictedParamPage) {

			UserFactory.clearSessionStorage();
			$location.path('/');
		}

	});
});
