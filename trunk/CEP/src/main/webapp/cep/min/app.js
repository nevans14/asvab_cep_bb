// create the module and name it cepApp
var cepApp = angular.module('cepApp', [ 'ngRoute', 'ngCookies', 'googlechart', 'ui.bootstrap', 'mm.foundation', 'ngSanitize', '720kb.socialshare','ipCookie', 'vcRecaptcha', 'chart.js', 'updateMeta'])
.filter('isAccPresent', ['$sce', function($sce) {
	  return function(input) {
		  var returnValue = '';
		  var accList;
		  if (typeof input === "undefined" || input.length == 0 ) {
			   return '';
		  }
		  console.log(typeof input);
		  if ( typeof input === "string" )  {
			  accList = input.split(",");
		  } else {
			  accList = new Array();
			  accList.push(input);
		  }

		  for (var i = 0; i < accList.length; i++) {
			  if ( accList[i] == 1 ) {  returnValue = returnValue + '<img src="/CEP/images/icons/ncca-accredited.png" height="16" width="16" >' }
			  if ( accList[i]== 2 ) {  returnValue = returnValue + '<img src="/CEP/images/icons/ansi-accredited.png" height="16" width="16" >' }
			  if ( accList[i] == 3 ) {  returnValue = returnValue + '<img src="/CEP/images/icons/job-corps.png" height="16" width="16"  >' }
			  if ( accList[i] == 4 || accList[i]== 5 || accList[i]== 9  ) {  returnValue = returnValue + '<img src="/CEP/images/icons/third-party-industry-endorsed-cert.png" height="16" width="16" >' }
			  if ( accList[i] == 6 ) {  returnValue = returnValue + '<img src="/CEP/images/icons/military-occ-specialties.png" height="16" width="16" >' }
			  if ( accList[i] == 7 ) {  returnValue = returnValue + '<img src="/CEP/images/icons/career-clusters.png" height="16" width="16" >' }
			  if ( accList[i] == 8 ) {  returnValue = returnValue + '<img src="/CEP/images/icons/in-demand.png" height="16" width="16" >' }
// alternate in demand icon		  <img src="/CEP/images/icons/in-demand-2.png" >
		  }

	    return  $sce.trustAsHtml(returnValue);
	  };
	}]);

cepApp.constant('appUrl', '/CEP/rest/');
cepApp.constant('resource', '/');
cepApp.constant('base', '/CEP');
cepApp.constant('recaptchaKey', '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN');
cepApp.constant('ssoUrl', 'http://asvabcitm.humrro.org/');
//cepApp.constant('ssoUrl', 'http://www.careersinthemilitary.com/');

cepApp.provider('$exceptionHandler', {
    $get: ['errorLogService', function( errorLogService ) {
        return( errorLogService );
    }]
});


(function(app) {




	var routeLoadingIndicator = function($rootScope) {
		return {
			restrict : 'E',
			template : "<div class='loading-indicator' ng-if='isRouteLoading'><h1><i class='fa fa-cog fa-spin fa-2x' style='color: #2f2a6a;;'></i></h1></div>",
			link : function(scope, elem, attrs) {
				scope.isRouteLoading = false;

				$rootScope.$on('$routeChangeStart', function() {
					scope.isRouteLoading = true;
				});

				$rootScope.$on('$routeChangeSuccess', function() {
					scope.isRouteLoading = false;
				});
			}
		};
	};
	routeLoadingIndicator.$inject = [ '$rootScope' ];

	app.directive('routeLoadingIndicator', routeLoadingIndicator);

}(angular.module('cepApp')));

cepApp.config(['$routeProvider', '$httpProvider', '$locationProvider', function($routeProvider, $httpProvider, $locationProvider) {
	$httpProvider.interceptors.push('responseObserver');

	$routeProvider

	.when('/', {
		templateUrl : 'cep/components/home/page-partials/home.html',
		controller : 'HomeController',
		reloadOnSearch : false,
		resolve : {
			mediaCenterList : ['MediaCenterService', function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}]
		}
	}).when('/passwordReset', {
		templateUrl : 'cep/components/password-reset/page-partials/password-reset.html',
		controller : 'PasswordRestController',
	}).when('/occufind-occupation-details-not-logged-in/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-occupation-details-not-logged-in.html',
		controller : 'OccufindOccupationDetailsNotLoggedInController',
		resolve : {

			occupationCertificates : ['CareerOneStopFactory', '$route', function(CareerOneStopFactory, $route) {
				if ($route.current.params.socId != null) {
					return CareerOneStopFactory.getOccupationCertificates($route.current.params.socId);
				}
			}],

			occupationTitleDescription : ['OccufindRestFactory', 'OccufindSearchService', '$route', function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			}],
			stateSalary : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return careerClusterRestFactory.getStateSalary(onetSocTrimmed);
				}
			}],
			skillImportance : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getSkillImportance($route.current.params.socId);
				}
			}],
			tasks : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getTasks($route.current.params.socId);
				}
			}],
			occupationInterestCodes : ['OccufindRestFactory', 'OccufindSearchService', '$route', function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationInterestCodes($route.current.params.socId);
				}
			}],
			educationLevel : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getEducationLevel($route.current.params.socId);
				}
			}],
			relatedOccupations : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getRelatedOccupations($route.current.params.socId);
				}
			}],
			militaryResources : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getMilitaryResources($route.current.params.socId);
				}
			}],
			oohResources : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getOOHResources($route.current.params.socId);
				}
			}],
			hotGreenStemFlags : ['OccufindRestFactory', 'OccufindSearchService', '$route', function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getHotGreenStemFlags($route.current.params.socId);
				}
			}],
			servicesOffering : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getServiceOfferingCareer($route.current.params.socId);
				}
			}],
			relatedCareerCluster : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getRelatedCareerCluster($route.current.params.socId);
				}
			}]
		}
	}).when('/dashboard', {
		templateUrl : 'cep/components/dashboard/page-partials/dashboard.html',
		controller : 'DashboardController',
		resolve : {
			numberTestTaken : ['FYIRestFactory', '$rootScope', 'UserFactory', function(FYIRestFactory, $rootScope, UserFactory) {
				/*
				 * var userId = $rootScope.globals.currentUser.userId; return
				 * FYIRestFactory.getNumberTestTaken(userId);
				 */
				return UserFactory.getNumberOfTestTaken();
			}],
			profileExist : ['FYIRestFactory', '$rootScope', function(FYIRestFactory, $rootScope) {
				var fyiObject = {
					userId : $rootScope.globals.currentUser.userId
				};
				return FYIRestFactory.getProfileExist(angular.toJson(fyiObject));
			}],
			userInterestCodes : ['UserFactory', function(UserFactory) {
				return UserFactory.getUserInterestCodes();
			}],
			brightCareers : ['UserFactory', 'GreenStemBrightRestFactory', 'UserFactory', '$q', function(UserFactory, GreenStemBrightRestFactory, UserFactory, $q) {
				var d1 = $q.defer();
				var d2 = $q.defer();
				UserFactory.getUserInterestCodes().then(function(success) {

					GreenStemBrightRestFactory.getBrightOutlook(success.interestCodeOne, success.interestCodeTwo, success.interestCodeThree).then(function(success) {
						d2.resolve(success.data);
					}, function(error) {
						d2.reject(error);
					});
					d1.resolve(success);
				}, function(error) {
					d1.reject(error);
				})

				return $q.all([ d2.promise, d1.promise ]);
			}],
			stemCareers : ['UserFactory', 'GreenStemBrightRestFactory', 'UserFactory', '$q', function(UserFactory, GreenStemBrightRestFactory, UserFactory, $q) {
				var d1 = $q.defer();
				var d2 = $q.defer();
				UserFactory.getUserInterestCodes().then(function(success) {

					GreenStemBrightRestFactory.getStemCareers(success.interestCodeOne, success.interestCodeTwo, success.interestCodeThree).then(function(success) {
						d2.resolve(success.data);
					}, function(error) {
						d2.reject(error);
					});
					d1.resolve(success);
				}, function(error) {
					d1.reject(error);
				})

				return $q.all([ d2.promise, d1.promise ]);
			}],
			mediaCenterList : ['MediaCenterService', function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}],
			noteList : ['NotesService', function(NotesService) {
				// preload notes
				return NotesService.getNotes();
			}],
			schoolNoteList : ['SchoolNotesService', function(SchoolNotesService) {
				// preload notes
				return SchoolNotesService.getNotes();
			}],
			tiedLink : ['FYIScoreService', 'FYIRestFactory', '$rootScope', '$q', function(FYIScoreService, FYIRestFactory, $rootScope, $q) {
				// If there is no top interest codes then there is not a tied
				// score. Set top interest code to undefined.
				var userId = $rootScope.globals.currentUser.userId;

				/**
				 * Check to see if a test with tied scores exist for user.
				 */
				var d2 = $q.defer();
				// Rest call that returns calculated combined and gender scores.
				FYIRestFactory.getCombinedAndGenderScores(userId).then(function(success) {

					var scores = success.data;

					// Check to see if there are tied scores.
					var isCombinedScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedCombinedScore(scores), 'combined');
					var isGenderScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedGenderScore(scores), 'gender');

					// Reset values
					FYIScoreService.setCombinedTiedScoreObject(undefined);
					FYIScoreService.setGenderTiedScoreObject(undefined);

					var isTiedObject = {
						isCombinedScoresTied : isCombinedScoresTied,
						isGenderScoresTied : isGenderScoresTied
					}

					d2.resolve(isTiedObject);

				}, function(error) {
					d2.reject(error);
				});

				return d2.promise;
			}],
			scoreSummary : ['FYIRestFactory', '$rootScope', 'PortfolioRestFactory', '$q', function(FYIRestFactory, $rootScope, PortfolioRestFactory, $q) {
				
				var userId = $rootScope.globals.currentUser.userId;
				var p = $q.defer();
				var pTwo = $q.defer();

				// If role is teacher or parent
				if ($rootScope.globals.currentUser.role == 'E'
					|| $rootScope.globals.currentUser.role == 'D') {

					// Get access code to pull teacher scores.
					PortfolioRestFactory.getAccessCode(userId).then(function(s) {
						var accessCode = s.data.r;
						var scoreAccessCode;

						// Uses teacher specific data.
						if (accessCode.indexOf('CEP123') !== -1) {
							scoreAccessCode = 'CEP123';
						} else {
							scoreAccessCode = 'CEP456';
						}

						// Get test score by access code.
						FYIRestFactory.getTestScoreWithAccessCode(scoreAccessCode).then(function(sTwo) {
							pTwo.resolve(sTwo);
						}, function(e) {
							pTwo.reject(e);
						})
						p.resolve(s);
					}, function(e) {
						p.reject(e);
					});

					return $q.all([ pTwo.promise, p.promise ]);

				} else {
					p.resolve(true);
					return $q.all([ FYIRestFactory.getTestScoreWithUserId(userId), p.promise ]);
				}
				
			}],
			asvabScore : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getAsvabScore(userId);
			}],
			isDemoTaken : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getTeacherDemoIsTaken(userId);
			}]
		}
	})

	.when('/green-stem-bright/:category', {
		templateUrl : 'cep/components/green-stem-bright/page-partials/green-stem-bright-occupation-results.html',
		controller : 'GreenStemBrightOccupationController',
		resolve : {
			occupationResults : ['GreenStemBrightRestFactory', '$route', '$window', function(GreenStemBrightRestFactory, $route, $window) {
				var interestCodeOne = $window.sessionStorage.getItem('interestCodeOne');
				var interestCodeTwo = $window.sessionStorage.getItem('interestCodeTwo');
				var interestCodeThree = $window.sessionStorage.getItem('interestCodeThree');
				var category = $route.current.params.category;

				if (category == 'bright') {
					return GreenStemBrightRestFactory.getBrightOutlook(interestCodeOne, interestCodeTwo, interestCodeThree);
				} else if (category == 'stem') {
					return GreenStemBrightRestFactory.getStemCareers(interestCodeOne, interestCodeTwo, interestCodeThree);
				} else if (category == 'green') {
					return GreenStemBrightRestFactory.getGreenCareers(interestCodeOne, interestCodeTwo, interestCodeThree);
				} else if (category == 'all') {
					return GreenStemBrightRestFactory.getAllCareers();
				} else if (category == 'military') {
					return GreenStemBrightRestFactory.getMilitaryCareers();
				}else if (category == 'hot') {
					return GreenStemBrightRestFactory.getHotCareers();
				}
			}]
		}
	})

	.when('/find-your-interest', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest.html',
		controller : 'FindYourInterestController',
		resolve : {
			mediaCenterList : ['MediaCenterService', function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}]
		}
	})

	.when('/find-your-interest-test', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-test.html',
		controller : 'FindYourInterestTestController',
		resolve : {
			questions : ['FYIRestFactory', '$rootScope', '$location', '$q', function(FYIRestFactory, $rootScope, $location, $q) {
				var userId = $rootScope.globals.currentUser.userId;
				var userRole = $rootScope.globals.currentUser.role;
				var d1 = $q.defer();
				var d2 = $q.defer();
				FYIRestFactory.getTestQuestions().then(function(success) {

					FYIRestFactory.getNumberTestTaken(userId).then(function(success) {
						if (userRole != 'A') {
							if (success.data >= 2) {
								$location.path('/find-your-interest-score');
								d2.reject(success);
							} else {
								d2.resolve(success);
							}
						} else {
							d2.resolve(success);
						}
					}, function(error) {
						d2.reject(error)
					});

					d1.resolve(success);
				}, function(error) {
					d1.reject(error);
				});

				return $q.all([ d1.promise, d2.promise ]);

			}]
		}
	})

	.when('/find-your-interest-tied', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-tied.html',
		controller : 'FindYourInterestTiedController',
		resolve : {
			/**
			 * Prevent user from going back to the tied page if tied selection
			 * already made.
			 */
			redirect : ['FYIScoreService', '$location', '$q', function(FYIScoreService, $location, $q) {
				// Gets top combined and gender score from session storage.
				var topCombinedScores = FYIScoreService.getTopCombinedInterestCodes();
				var topGenderScores = FYIScoreService.getTopGenderInterestCodes();

				var defer = $q.defer();
				var istiedLinkClicked = FYIScoreService.getTiedLinkClicked();

				if ((topCombinedScores != undefined && !istiedLinkClicked) || (topGenderScores != undefined && !istiedLinkClicked)) {
					$location.path('/find-your-interest-score');
				} else {
					defer.resolve("success");
				}

				return $q.all([ defer.promise ]);
			}]
		}
	})

	// This is needed to use manually fyi score input when on the score page.
	.when('/find-your-interest-score-reload', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-score.html',
		controller : 'FindYourInterestScoreController',
		resolve : {
			reload : ['$location', '$q', function($location, $q) {
				var defer = $q.defer();
				$location.path('/find-your-interest-score');
				return defer.promise;
			}]
		}
	})

	.when('/find-your-interest-score', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-score.html',
		controller : 'FindYourInterestScoreController',
		resolve : {
			CombinedAndGenderScores : ['FYIRestFactory', '$rootScope', 'FYIScoreService', '$q', '$location', '$window', function(FYIRestFactory, $rootScope, FYIScoreService, $q, $location, $window) {

				var userId = $rootScope.globals.currentUser.userId;
				var fyiObject = FYIScoreService.getFyiObject();

				// If user is coming from the FYI test page.
				if (fyiObject != undefined) {
					var defer = $q.defer();
					var deferNumTestTaken = $q.defer();

					// Set tied top scores to default.
					FYIScoreService.setTopGenderInterestCodes(undefined);
					FYIScoreService.setTopCombinedInterestCodes(undefined);
					var tiedObject = {
						userId : userId,
						combineInterestOne : null,
						combineInterestTwo : null,
						combineInterestThree : null,
						genderInterestOne : null,
						genderInterestTwo : null,
						genderInterestThree : null,
					}
					FYIRestFactory.insertTiedSelection(tiedObject);

					// Rest call to post user's FYI answers to database.
					FYIRestFactory.postAnswers(angular.toJson(fyiObject)).then(function(success) {

						// Rest call that returns calculated combined and gender
						// scores.
						FYIRestFactory.getCombinedAndGenderScores(userId).then(function(success) {

							// Rest call to get number of FYI test user has
							// taken.
							FYIRestFactory.getNumberTestTaken(userId).then(function(success) {
								deferNumTestTaken.resolve(success);

								// Reset FYI to enable users to retake the FYI.
								FYIScoreService.setFyiObject(undefined);

							}, function(error) {
								deferNumTestTaken.reject(error);
							});

							var scores = success.data;
							var isCombinedScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedCombinedScore(scores), 'combined');
							var isGenderScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedGenderScore(scores), 'gender');

							// If tied scores then redirect to tie breaker page.
							if (isCombinedScoresTied || isGenderScoresTied) {

								var topCombinedScores = FYIScoreService.getTopCombinedInterestCodes();
								var topGenderScores = FYIScoreService.getTopGenderInterestCodes();

								// If either combined or gender score is tied
								// and user did not break ties then redirect
								// them to tie break page.
								if ((isCombinedScoresTied && topCombinedScores == undefined) || (isGenderScoresTied && topGenderScores == undefined)) {
									$location.path('/find-your-interest-tied');
								}
							}

							defer.resolve(success);

						}, function(error) {
							defer.reject(error);
						});
					}, function(error) {
						console.log("error");
					});

					return $q.all([ defer.promise, deferNumTestTaken.promise ]);

				} else {

					var qScores = $q.defer();
					var tieSelectionPromise = $q.defer();

					// Uses user's data to calculate score and determine if
					// there are ties.
					FYIRestFactory.getCombinedAndGenderScores(userId).then(function(success) {

						var scores = success.data;
						var isCombinedScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedCombinedScore(scores), 'combined');
						var isGenderScoresTied = FYIScoreService.isTiedScore(FYIScoreService.getSortedGenderScore(scores), 'gender');

						// If tied scores then redirect to tie breaker page.
						if (isCombinedScoresTied || isGenderScoresTied) {

							var istiedLinkClicked = FYIScoreService.getTiedLinkClicked();
							var s = undefined;

							FYIRestFactory.getTiedSelection(userId).then(function(success) {

								var data = success.data;

								// If tied link is not clicked on the dashboard
								// page then use database values to recover tie
								// breaker values.
								// This enables the user to come back to the
								// score page without having to be asked to
								// break a tie again.
								if (!istiedLinkClicked) {
									if (data.genderInterestOne && data.genderInterestTwo && data.genderInterestThree) {
										var topGenderInterestCodes = [ {
											interestCd : data.genderInterestOne
										}, {
											interestCd : data.genderInterestTwo
										}, {
											interestCd : data.genderInterestThree
										} ];

										// Set the top gender scores to session
										// storage.
										FYIScoreService.setTopGenderInterestCodes(topGenderInterestCodes);
									}

									if (data.combineInterestOne && data.combineInterestTwo && data.combineInterestThree) {
										var topCombineInterestCodes = [ {
											interestCd : data.combineInterestOne
										}, {
											interestCd : data.combineInterestTwo
										}, {
											interestCd : data.combineInterestThree
										} ];

										// Set the top combine scores to session
										// storage.
										FYIScoreService.setTopCombinedInterestCodes(topCombineInterestCodes);
									}

								} else {

									// User clicked on tie breaker link on the
									// dashboard page.
									// This enables them to break the tie again.
									FYIScoreService.setTopGenderInterestCodes(undefined);
									FYIScoreService.setTopCombinedInterestCodes(undefined);
								}

								// Gets top combined and gender score from
								// session storage.
								var topCombinedScores = FYIScoreService.getTopCombinedInterestCodes();
								var topGenderScores = FYIScoreService.getTopGenderInterestCodes();

								// Determines if user is redirected to the tie
								// page or not.
								if ((isCombinedScoresTied && topCombinedScores == undefined) || (isGenderScoresTied && topGenderScores == undefined)) {
									$location.path('/find-your-interest-tied');
								} else {
									tieSelectionPromise.resolve(success);
								}

							}, function(error) {
								tieSelectionPromise.reject(error);
							});

						} else {
							tieSelectionPromise.resolve();
						}
						qScores.resolve(success);
					}, function(error) {
						qScores.reject(error)
					});

					// A promise for number of test taken.
					var numberTestTaken = $q.defer();
					FYIRestFactory.getNumberTestTaken(userId).then(function(success) {
						numberTestTaken.resolve(success);
					}, function(error) {
						numberTestTaken.reject(error);
					});

					// Proceeds if all promises are resolved.
					return $q.all([ qScores.promise, numberTestTaken.promise, tieSelectionPromise.promise ]);

				}
			}],
			scoreChoice : ['FYIRestFactory', '$rootScope', function(FYIRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return FYIRestFactory.getScoreChoice(userId);
			}]
		}
	})

	.when('/my-asvab-summary-results', {
		templateUrl : 'cep/components/find-your-interest/page-partials/my-asvab-summary-results.html',
		controller : 'MyAsvabSummaryController',
		resolve : {
			scoreSummary : ['FYIRestFactory', '$rootScope', '$q', 'PortfolioRestFactory', function(FYIRestFactory, $rootScope, $q, PortfolioRestFactory) {
				
				var userId = $rootScope.globals.currentUser.userId;
				var p = $q.defer();
				var pTwo = $q.defer();
				
				// If role is teacher or parent
				if ($rootScope.globals.currentUser.role == 'E'
					|| $rootScope.globals.currentUser.role == 'D') {
					
					// Get access code to pull teacher scores.
					PortfolioRestFactory.getAccessCode(userId).then(function(s) {
						var accessCode = s.data.r;
						var scoreAccessCode;

						// Uses teacher specific data.
						if (accessCode.indexOf('CEP123') !== -1) {
							scoreAccessCode = 'CEP123';
						} else {
							scoreAccessCode = 'CEP456';
						}

						// Get test score by access code.
						FYIRestFactory.getTestScoreWithAccessCode(scoreAccessCode).then(function(sTwo) {
							pTwo.resolve(sTwo);
						}, function(e) {
							pTwo.reject(e);
						})
						p.resolve(s);
					}, function(e) {
						p.reject(e);
					});
					
					return $q.all([pTwo.promise, p.promise]);
					
				} else {
					p.resolve(true);
					return $q.all([FYIRestFactory.getTestScoreWithUserId(userId), p.promise]);
				}
			}]
		}
	})

//	.when('/cep-manual-input', {
//		templateUrl : 'cep/components/dashboard/page-partials/cep-manual-input.html',
//		controller : 'ManualInputController',
//		resolve : {
//			asvabScore : function(PortfolioRestFactory, $rootScope) {
//				var userId = $rootScope.globals.currentUser.userId;
//				return PortfolioRestFactory.getAsvabScore(userId);
//			}
//		}
//	})
	
	.when('/my-asvab-summary-results-manual', {
		templateUrl : 'cep/components/find-your-interest/page-partials/my-asvab-summary-results-manual.html',
		controller : 'FindYourInterestSummaryManualController',
		resolve : {
			asvabScore : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getAsvabScore(userId);
			}]
		}
	})

	.when('/find-your-interest-tied-score', {
		templateUrl : 'cep/components/find-your-interest/page-partials/find-your-interest-tied-score.html',
		controller : 'FindYourInterestTiedScoreController'
	})

	.when('/occufind-occupation-search', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-occupation-search.html',
		controller : 'OccufindOccupationSearchController',
		resolve : {
			mediaCenterList : ['MediaCenterService', function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}]
		}
	})

	.when('/occufind-occupation-search-results', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-occupation-search-results.html',
		controller : 'OccufindOccupationSearchResultsController',
		reloadOnSearch : false,
		resolve : {
			occupationResults : ['OccufindRestFactory', 'OccufindSearchService', function(OccufindRestFactory, OccufindSearchService) {
				return OccufindRestFactory.getOccupationResults(angular.toJson(OccufindSearchService.userSearch));
			}]
		}
	})

	.when('/occufind-occupation-details/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-occupation-details.html',
		controller : 'OccufindOccupationDetailsController',
		resolve : {
			occupationCertificates : ['CareerOneStopFactory', '$route', function(CareerOneStopFactory, $route) {
				if ($route.current.params.socId != null) {
					return CareerOneStopFactory.getOccupationCertificates($route.current.params.socId);
				}
			}],
			occupationTitleDescription : ['OccufindRestFactory', 'OccufindSearchService', '$route', function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			}],
			stateSalary : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return careerClusterRestFactory.getStateSalary(onetSocTrimmed);
				}
			}],
			nationalSalary : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return OccufindRestFactory.getNationalSalary(onetSocTrimmed);
				}
			}],
			blsTitle : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return OccufindRestFactory.getBLSTitle(onetSocTrimmed);
				}
			}],
			skillImportance : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getSkillImportance($route.current.params.socId);
				}
			}],
			tasks : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getTasks($route.current.params.socId);
				}
			}],
			occupationInterestCodes : ['OccufindRestFactory', 'OccufindSearchService', '$route', function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationInterestCodes($route.current.params.socId);
				}
			}],
			educationLevel : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getEducationLevel($route.current.params.socId);
				}
			}],
			relatedOccupations : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getRelatedOccupations($route.current.params.socId);
				}
			}],
			hotGreenStemFlags : ['OccufindRestFactory', 'OccufindSearchService', '$route', function(OccufindRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getHotGreenStemFlags($route.current.params.socId);
				}
			}],
			servicesOffering : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getServiceOfferingCareer($route.current.params.socId);
				}
			}],
			relatedCareerCluster : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getRelatedCareerCluster($route.current.params.socId);
				}
			}],
			moreResources : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getMoreResources($route.current.params.socId);
				}
			}],
			noteList : ['NotesService', function(NotesService) {
				// preload notes
				NotesService.getNotes();
			}],
			schoolDetails : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getSchoolMoreDetails($route.current.params.socId);
				}
			}],
			getAlternateView : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getAlternateView($route.current.params.socId);
				}
			}],
			imageAltText : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getImageAltText($route.current.params.socId);
				}
			}],
			jobZone : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getJobZone($route.current.params.socId);
				}
			}],
			certificateExplanation : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getCertificateExplanation($route.current.params.socId);
				}
			}]
			,
			alternateTitles : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getAlternateTitles($route.current.params.socId);
				}
			}],
			workValues : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			}]
		}
	})

	.when('/occufind-education-details/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-education-details.html',
		controller : 'OccufindEducationDetailsController',
		resolve : {
			occupationTitleDescription : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			}],
			schoolDetails : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getSchoolMoreDetails($route.current.params.socId);
				}
			}],
			schoolMajors : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getSchoolMajors($route.current.params.socId);
				}
			}]
		}
	})
	
	.when('/occufind-school-details/:socId/:unitId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-school-details.html',
		controller : 'OccufindSchoolDetailsController',
		resolve : {
			occupationTitleDescription : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			}],
			schoolDetails : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.unitId != null) {
					return OccufindRestFactory.getSchoolProfile($route.current.params.unitId);
				}
			}]
		}
	})

	.when('/occufind-employment-details/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-employment-details.html',
		controller : 'OccufindEmploymentDetailsController',
		resolve : {
			stateSalaryYear : ['OccufindRestFactory', function(OccufindRestFactory) {
				return OccufindRestFactory.getStateSalaryYear();
			}],
			occupationTitleDescription : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			}],
			employmentMoreDetails : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSocTrimmed = $route.current.params.socId.slice(0, -3);
					return OccufindRestFactory.getEmploymentMoreDetails(onetSocTrimmed);
				}
			}]
		}
	})

	.when('/occufind-military-details/:socId', {
		templateUrl : 'cep/components/occufind/page-partials/occufind-military-details.html',
		controller : 'OccufindMilitaryDetailsController',
		resolve : {
			occupationTitleDescription : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					return OccufindRestFactory.getOccupationTitleDescription($route.current.params.socId);
				}
			}],
			militaryMoreDetails : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSoc = $route.current.params.socId;
					return OccufindRestFactory.getMilitaryMoreDetails(onetSoc);
				}
			}],
			militaryHotJobs : ['OccufindRestFactory', '$route', function(OccufindRestFactory, $route) {
				if ($route.current.params.socId != null) {
					var onetSoc = $route.current.params.socId;
					return OccufindRestFactory.getMilitaryHotJobs(onetSoc);
				}
			}],
			militaryResources : ['careerClusterRestFactory', 'OccufindSearchService', '$route', function(careerClusterRestFactory, OccufindSearchService, $route) {
				if ($route.current.params.socId != null) {
					return careerClusterRestFactory.getMilitaryResources($route.current.params.socId);
				}
			}]

		}
	})

	.when('/career-cluster', {
		templateUrl : 'cep/components/career-cluster/page-partials/career-cluster.html',
		controller : 'CareerClusterController',
		resolve : {
			career : ['careerClusterRestFactory', function(careerClusterRestFactory) {
				return careerClusterRestFactory.getCareerCluster();
			}]
		}
	})

	.when('/career-cluster-occupation-results/:ccId', {
		templateUrl : 'cep/components/career-cluster/page-partials/career-cluster-occupation-results.html',
		controller : 'CareerClusterOccupationController',
		resolve : {
			occupationResults : ['careerClusterRestFactory', '$route', function(careerClusterRestFactory, $route) {
				return careerClusterRestFactory.getCareerClusterOccupations($route.current.params.ccId);
			}],
			careerClusterById : ['careerClusterRestFactory', '$route', function(careerClusterRestFactory, $route) {
				return careerClusterRestFactory.getCareerClusterById($route.current.params.ccId);
			}]
		}
	})

	.when('/career-cluster-occupation-details', {
		templateUrl : 'cep/components/career-cluster/page-partials/career-cluster-occupation-details.html',
		controller : 'CareerClusterOccupationDetailsController',
		resolve : {
			occupationTitleDescription : ['OccufindRestFactory', 'careerClusterService', function(OccufindRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					return OccufindRestFactory.getOccupationTitleDescription(careerClusterService.selectedOnetSoc);
				}
			}],
			stateSalary : ['careerClusterRestFactory', 'careerClusterService', function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					var onetSocTrimmed = careerClusterService.selectedOnetSoc.slice(0, -3);
					return careerClusterRestFactory.getStateSalary(onetSocTrimmed);
				}
			}],
			skillImportance : ['careerClusterRestFactory', 'careerClusterService', function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					return careerClusterRestFactory.getSkillImportance(careerClusterService.selectedOnetSoc);
				}
			}],
			tasks : ['careerClusterRestFactory', 'careerClusterService', function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					return careerClusterRestFactory.getTasks(careerClusterService.selectedOnetSoc);
				}
			}],
			occupationInterestCodes : ['careerClusterRestFactory', 'careerClusterService', function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					return careerClusterRestFactory.getOccupationInterestCodes(careerClusterService.selectedOnetSoc);
				}
			}],
			educationLevel : ['careerClusterRestFactory', 'careerClusterService', function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					return careerClusterRestFactory.getEducationLevel(careerClusterService.selectedOnetSoc);
				}
			}],
			relatedOccupations : ['careerClusterRestFactory', 'careerClusterService', function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					return careerClusterRestFactory.getRelatedOccupations(careerClusterService.selectedOnetSoc);
				}
			}],
			militaryResources : ['careerClusterRestFactory', 'careerClusterService', function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					return careerClusterRestFactory.getMilitaryResources(careerClusterService.selectedOnetSoc);
				}
			}],
			oohResources : ['careerClusterRestFactory', 'careerClusterService', function(careerClusterRestFactory, careerClusterService) {
				if (careerClusterService.selectedOnetSoc != undefined) {
					return careerClusterRestFactory.getOOHResources(careerClusterService.selectedOnetSoc);
				}
			}]
		}
	})

	.when('/career-cluster-public', {
		templateUrl : 'cep/components/career-cluster/page-partials/career-cluster-public.html',
		controller : 'CareerClusterPublicController',
		resolve : {
			career : ['careerClusterRestFactory', function(careerClusterRestFactory) {
				return careerClusterRestFactory.getCareerCluster();
			}]
		}
	})

	.when('/career-cluster-pathway-1', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/architecture-construction.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-2', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/arts-av-technology-communications.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-3', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/business-management-administration.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-4', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/education.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-5', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/hospitality-tourism.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-6', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/manufacturing.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-7', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/marketing.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-8', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/science-technology-engineering-mathematics.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-9', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/agriculture-food-natural-resources.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-10', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/finance.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-11', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/government-and-public-administration.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-12', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/health-science.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-13', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/human-services.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-14', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/law-public-safety-corrections-security.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-15', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/transportation-distribution-logistics.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/career-cluster-pathway-16', {
		templateUrl : 'cep/components/career-cluster/page-partials/pathways/information-technology.html',
		controller : 'CareerClusterPathwayController'
	})

	.when('/classroom-activities', {
		templateUrl : 'cep/components/dashboard/page-partials/classroom-activities.html'
	})
	
	.when('/portfolio-directions', {
		templateUrl : 'cep/components/portfolio/page-partials/portfolio-directions.html',
		controller : 'PortfolioDirectionsController'
	})

	.when('/portfolio', {
		templateUrl : 'cep/components/portfolio/page-partials/portfolio.html',
		controller : 'PortfolioController',
		resolve : {
			workExperience : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getWorkExperience(userId);
			}],
			education : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getEducation(userId);
			}],
			achievement : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getAchievements(userId);
			}],
			interest : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getInterests(userId);
			}],
			skill : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getSkills(userId);
			}],
			workValue : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getWorkValues(userId);
			}],
			volunteer : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getVolunteers(userId);
			}],
			actScore : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getActScore(userId);
			}],
			asvabScore : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getAsvabScore(userId);
			}],
			fyiScore : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getFyiScore(userId);
			}],
			satScore : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getSatScore(userId);
			}],
			otherScore : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getOtherScore(userId);
			}],
			favoriteList : ['UserFactory', function(UserFactory) {
				return UserFactory.getFavoritesList();
			}],
			careerClusterFavoriteList : ['UserFactory', function(UserFactory) {
				return UserFactory.getCareerClusterFavoritesList();
			}],
			plan : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getPlan(userId);
			}],
			UserInfo : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.selectNameGrade(userId);
			}],
			citmFavoriteOccupations : ['PortfolioRestFactory', '$rootScope', function(PortfolioRestFactory, $rootScope) {
				var userId = $rootScope.globals.currentUser.userId;
				return PortfolioRestFactory.getCitmFavoriteOccupations(userId);
			}],
			schoolFavoriteList : ['UserFactory', function(UserFactory) {
				return UserFactory.getSchoolFavoritesList();
			}]
		}
	})

	.when('/favorites', {
		templateUrl : 'cep/components/favorites/page-partials/favorites.html',
		controller : 'FavoritesController',
		resolve : {
			favoriteList : ['UserFactory', function(UserFactory) {
				return UserFactory.getFavoritesList();
			}],
			careerClusterFavoriteList : ['UserFactory', function(UserFactory) {
				return UserFactory.getCareerClusterFavoritesList();
			}],
			schoolFavoriteList : ['UserFactory', function(UserFactory) {
				return UserFactory.getSchoolFavoritesList();
			}],
			scoreSummary : ['FYIRestFactory', '$rootScope', 'PortfolioRestFactory', '$q', function(FYIRestFactory, $rootScope, PortfolioRestFactory, $q) {
				
				var userId = $rootScope.globals.currentUser.userId;
				var p = $q.defer();
				var pTwo = $q.defer();

				// If role is teacher or parent
				if ($rootScope.globals.currentUser.role == 'E'
					|| $rootScope.globals.currentUser.role == 'D') {

					// Get access code to pull teacher scores.
					PortfolioRestFactory.getAccessCode(userId).then(function(s) {
						var accessCode = s.data.r;
						var scoreAccessCode;

						// Uses teacher specific data.
						if (accessCode.indexOf('CEP123') !== -1) {
							scoreAccessCode = 'CEP123';
						} else {
							scoreAccessCode = 'CEP456';
						}

						// Get test score by access code.
						FYIRestFactory.getTestScoreWithAccessCode(scoreAccessCode).then(function(sTwo) {
							pTwo.resolve(sTwo);
						}, function(e) {
							pTwo.reject(e);
						})
						p.resolve(s);
					}, function(e) {
						p.reject(e);
					});

					return $q.all([ pTwo.promise, p.promise ]);

				} else {
					p.resolve(true);
					return $q.all([ FYIRestFactory.getTestScoreWithUserId(userId), p.promise ]);
				}
				
			}]
		}
	})

	.when('/media-center', {
		title : 'Media Center | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/media-center/page-partials/media-center.html',
		controller : 'MediaCenterController',
		resolve : {
			mediaCenterList : ['MediaCenterService', function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}],
		}
	})

	.when('/media-center-article-list/:categoryId', {
		title : function() {
			if ($route.current.params.categoryId == 5) {
				return 'Tutorials | ASVAB Career Exploration Program';
			} else if ($route.current.params.categoryId == 2) {
				return 'Resources for Parents | ASVAB Career Exploration Program';
			} else if ($route.current.params.categoryId == 4) {
				return 'Resources for Educators | ASVAB Career Exploration Program';
			} else if ($route.current.params.categoryId == 1) {
				return 'Resources for Students | ASVAB Career Exploration Program';
			} else {
				return undefined
			}
		},
		templateUrl : 'cep/components/media-center/page-partials/media-center-article-list.html',
		controller : 'MediaCenterArticleListController',
		resolve : {
			mediaCenterList : ['MediaCenterService', function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}]
		}
	})

	.when('/media-center-article/:mediaId', {
		templateUrl : 'cep/components/media-center/page-partials/media-center-article.html',
		controller : 'MediaCenterArticleController',
		resolve : {
			mediaCenterList : ['MediaCenterService', function(MediaCenterService) {
				return MediaCenterService.getMediaCenterList();
			}]
		}
	})

	.when('/search-result/:searchString', {
		templateUrl : 'cep/components/search/page-partials/search-result.html',
		controller : 'SearchResultController',
		resolve : {
			searchResult : ['SearchRestFactory', '$route', function(SearchRestFactory, $route) {
				return SearchRestFactory.getSearchResult($route.current.params.searchString);
			}]
		}
	})

	.when('/search-result-public/:searchString', {
		templateUrl : 'cep/components/search/page-partials/search-result-public.html',
		controller : 'SearchResultController',
		resolve : {
			searchResult : ['SearchRestFactory', '$route', function(SearchRestFactory, $route) {
				return SearchRestFactory.getSearchResult($route.current.params.searchString);
			}]
		}
	})

	.when('/compare-occupation/:socIdOne/:socIdTwo/:socIdThree', {
		templateUrl : 'cep/components/compare-occupation/page-partials/compare-occupation.html',
		controller : 'CompareOccupationController',
		resolve : {
			occupationOne : ['OccufindRestFactory', 'careerClusterRestFactory', 'NotesRestFactory', '$route', '$q', '$rootScope', function(OccufindRestFactory, careerClusterRestFactory, NotesRestFactory, $route, $q, $rootScope) {

				var title = OccufindRestFactory.getOccupationTitleDescription($route.current.params.socIdOne);
				var skillImportance = careerClusterRestFactory.getSkillImportance($route.current.params.socIdOne);
				var occupationInterestCodes = OccufindRestFactory.getOccupationInterestCodes($route.current.params.socIdOne);
				var educationLevel = careerClusterRestFactory.getEducationLevel($route.current.params.socIdOne);
				var servicesOffering = OccufindRestFactory.getServiceOfferingCareer($route.current.params.socIdOne);
				var onetSocTrimmed = $route.current.params.socIdOne.slice(0, -3);
				var nationalSalary = OccufindRestFactory.getNationalSalary(onetSocTrimmed);
				var notes = NotesRestFactory.getOccupationNotes({
					socId : $route.current.params.socIdOne,
					userId : $rootScope.globals.currentUser.userId
				});

				return $q.all([ title, skillImportance, occupationInterestCodes, educationLevel, servicesOffering, nationalSalary, notes ]);
			}],
			occupationTwo : ['OccufindRestFactory', 'careerClusterRestFactory', 'NotesRestFactory', '$route', '$q', '$rootScope', function(OccufindRestFactory, careerClusterRestFactory, NotesRestFactory, $route, $q, $rootScope) {

				var title = OccufindRestFactory.getOccupationTitleDescription($route.current.params.socIdTwo);
				var skillImportance = careerClusterRestFactory.getSkillImportance($route.current.params.socIdTwo);
				var occupationInterestCodes = OccufindRestFactory.getOccupationInterestCodes($route.current.params.socIdTwo);
				var educationLevel = careerClusterRestFactory.getEducationLevel($route.current.params.socIdTwo);
				var servicesOffering = OccufindRestFactory.getServiceOfferingCareer($route.current.params.socIdTwo);
				var onetSocTrimmed = $route.current.params.socIdTwo.slice(0, -3);
				var nationalSalary = OccufindRestFactory.getNationalSalary(onetSocTrimmed);
				var notes = NotesRestFactory.getOccupationNotes({
					socId : $route.current.params.socIdTwo,
					userId : $rootScope.globals.currentUser.userId
				});

				return $q.all([ title, skillImportance, occupationInterestCodes, educationLevel, servicesOffering, nationalSalary, notes ]);
			}],
			occupationThree : ['OccufindRestFactory', 'careerClusterRestFactory', 'NotesRestFactory', '$route', '$q', '$rootScope', function(OccufindRestFactory, careerClusterRestFactory, NotesRestFactory, $route, $q, $rootScope) {

				if ($route.current.params.socIdThree != "undefined") {
					var title = OccufindRestFactory.getOccupationTitleDescription($route.current.params.socIdThree);
					var skillImportance = careerClusterRestFactory.getSkillImportance($route.current.params.socIdThree);
					var occupationInterestCodes = OccufindRestFactory.getOccupationInterestCodes($route.current.params.socIdThree);
					var educationLevel = careerClusterRestFactory.getEducationLevel($route.current.params.socIdThree);
					var servicesOffering = OccufindRestFactory.getServiceOfferingCareer($route.current.params.socIdThree);
					var onetSocTrimmed = $route.current.params.socIdThree.slice(0, -3);
					var nationalSalary = OccufindRestFactory.getNationalSalary(onetSocTrimmed);
					var notes = NotesRestFactory.getOccupationNotes({
						socId : $route.current.params.socIdThree,
						userId : $rootScope.globals.currentUser.userId
					});

					return $q.all([ title, skillImportance, occupationInterestCodes, educationLevel, servicesOffering, nationalSalary, notes ]);
				} else {
					return undefined;

				}
			}]
		}
	}).when('/faq', {
		title : 'FAQ ASVAB CEP',
		templateUrl : 'cep/components/faq/page-partials/faq.html',
		controller : 'FaqController'
	})

	/**
	 * Static pages
	 */
	.when('/educators-information-bring', {
		title : 'Bring the ASVAB Career Exploration Program to Your School',
		templateUrl : 'cep/components/static-pages/page-partials/educators-information-bring.html',
	}).when('/educators-post-test', {
		title : 'Post-Test Activities | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/educators-post-test.html',
	}).when('/educators-test', {
		title : 'ASVAB Test | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/educators-test.html',
	}).when('/educators', {
		title : 'Choose Careers | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/educators.html',
	}).when('/teachers', {
		controller : 'TeacherLandingController',
		title : 'Teachers | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/educators-teacher-landing.html',
	}).when('/general-help', {
		templateUrl : 'cep/components/static-pages/page-partials/general-help.html',
	}).when('/general-link', {
		templateUrl : 'cep/components/static-pages/page-partials/general-link.html',
	}).when('/general-occufind-view-all', {
		title : 'OCCU-Find | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/general-occufind-view-all.html',
	}).when('/general-privacy-security', {
		templateUrl : 'cep/components/static-pages/page-partials/general-privacy-security.html',
	}).when('/general-resources', {
		title : 'Resources | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/general-resources.html',
	}).when('/general-site', {
		templateUrl : 'cep/components/static-pages/page-partials/general-site.html',
	}).when('/parents-information', {
		title : 'Help Your Child Choose a Career | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/parents-information.html',
	}).when('/parents-post-test', {
		title : 'Post-Test | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/parents-post-test.html',
	}).when('/parents-test', {
		title : 'ASVAB Test | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/parents-test.html',
	}).when('/parents', {
		title : 'Help Your Child Choose a Career | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/parents.html',
	}).when('/student-explore-action-steps', {
		title : 'Career Planning Tools | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-explore-action-steps.html',
	}).when('/student-explore-cc', {
		templateUrl : 'cep/components/static-pages/page-partials/student-explore-cc.html',
	}).when('/student-explore-occu-find', {
		title : 'Find Your Dream Job | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-explore-occu-find.html',
	}).when('/student-explore', {
		title : 'Explore Careers | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-explore.html',
	}).when('/student-learn-interest-area', {
		templateUrl : 'cep/components/static-pages/page-partials/student-learn-interest-area.html',
	}).when('/student-learn-interests', {
		title : 'Interests | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-learn-interests.html',
	}).when('/student-learn-skills', {
		title : 'Skills | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-learn-skills.html',
	}).when('/student-learn-work-values', {
		title : 'Work Values | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-learn-work-values.html',
	}).when('/student-learn', {
		title : 'Choosing the Right Career | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-learn.html',
	}).when('/student-plan-tools', {
		title : 'Career Planning Tools | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-plan-tools.html',
	}).when('/student-plan-understanding', {
		title : 'Understand Your Options | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-plan-understanding.html',
	}).when('/student-plan', {
		title : 'Plan for Your Future | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-plan.html',
	}).when('/student-post-test', {
		title : 'Post-Test Activities | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-post-test.html',
	}).when('/student-program', {
		title : 'ASVAB Test | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student-program.html',
	}).when('/student', {
		title : 'Students | ASVAB Career Exploration Program',
		templateUrl : 'cep/components/static-pages/page-partials/student.html',
	}).when('/asvab-cep-at-your-school', {
		templateUrl : 'cep/components/static-pages/page-partials/asvab-cep-at-your-school.html',
	}).when('/continuing-education', {
		templateUrl : 'cep/components/static-pages/page-partials/ce-opportunity.html',
	}).when('/countdown', {
		templateUrl : 'cep/components/static-pages/page-partials/countdown.html',
	});
	// .otherwise({ redirectTo: '/' }); TODO: test this

	$locationProvider.html5Mode(true);

}]);

cepApp.run(['$rootScope', '$location', '$cookieStore', '$http', '$timeout', 'UserFactory', 'modalService', '$window', '$route', function($rootScope, $location, $cookieStore, $http, $timeout, UserFactory, modalService, $window, $route) {

	// google analytics
	$window.ga('create', 'UA-83809749-1', 'auto');

	// initialize foundation for sites to work with angular
	$rootScope.$on('$viewContentLoaded', function() {
		$(document).foundation();
	});

	// keep user logged in after page refresh
	$rootScope.globals = $cookieStore.get('globals') || {};
	if ($rootScope.globals.currentUser) {
		$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint
		// ignore:line
	}

	$rootScope.$on("$routeChangeStart", function(event, next, current) {
	});

	$rootScope.$on('$routeChangeSuccess', function() {
		if ($route.current.title) {
			document.title = $route.current.title;
		} else {
			document.title = 'ASVAB Career Exploration Program';
		}
	});

	$rootScope.$on("$routeChangeError", function(event, current, previous, rejection, $window) {

		console.log("Failed to change routes.");
		if (!!previous) {

			// Route to previous path.
			$location.path(previous.$$route.originalPath);

			// Alert user there was an error.
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error processing your request! Please try again. If error persists, please contact the site administrator.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
		} else {
			$location.path('/');
			// Alert user there was an error.
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error processing your request! Please try again. If error persists, please contact the site administrator.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
		}
	});

	$rootScope.$on('$locationChangeStart', function(event, next, current) {

		// google analytics track page
		$window.ga('send', 'pageview', $location.path());

		var firstSlashPos = $location.path().indexOf('/') + 1;
		var appParamPath = $location.path().substr(firstSlashPos, $location.path().lastIndexOf('/') - 1);
		var restrictedParamPage = $.inArray(appParamPath, [ 'search-result-public', 'media-center-article-list', 'media-center-article' ]) === -1;

		var lastSlashPos = $location.path().lastIndexOf('/') + 1;
		var appPath = $location.path().substr(1);
		var numOfSlashes = (appPath.match(new RegExp("/", "g")) || []).length; // logs?
		if (numOfSlashes > 0) {
			var pathArray = appPath.split("/");
			appPath = pathArray[0];
		}
		// redirect to login page if not logged in and trying to access a
		// restricted page
		var restrictedPage = $.inArray(appPath, [ '', 'passwordReset', 'occufind-occupation-details', 'occufind-education-details', 'occufind-school-details', 'occufind-military-details', 'occufind-employment-details', 'faq', 'media-center', 'career-cluster-public', 'career-cluster-pathway-1', 'career-cluster-pathway-2', 'career-cluster-pathway-3', 'career-cluster-pathway-4', 'career-cluster-pathway-5', 'career-cluster-pathway-6', 'career-cluster-pathway-7', 'career-cluster-pathway-8', 'career-cluster-pathway-9', 'career-cluster-pathway-10', 'career-cluster-pathway-11', 'career-cluster-pathway-12', 'career-cluster-pathway-13', 'career-cluster-pathway-14', 'career-cluster-pathway-15', 'career-cluster-pathway-16', 'educators-information-bring', 'educators-post-test', 'educators-test', 'educators', 'teachers', 'general-help', 'general-link', 'general-occufind-view-all', 'general-privacy-security', 'general-resources', 'general-site', 'parents-information', 'parents-post-test', 'parents-test', 'parents', 'student-explore-action-steps', 'student-explore-cc', 'student-explore-occu-find', 'student-explore', 'student-learn-interest-area', 'student-learn-interests', 'student-learn-skills', 'student-learn-work-values', 'student-learn', 'student-plan-tools', 'student-plan-understanding', 'student-plan', 'student-post-test', 'student-program', 'student', 'asvab-cep-at-your-school', 'teacher' ]) === -1;
		var loggedIn = $rootScope.globals.currentUser;

		if (restrictedPage && !loggedIn && restrictedParamPage) {

			UserFactory.clearSessionStorage();
			$location.path('/');
		}

	});
}]);

cepApp.controller('CareerClusterController', [ '$scope', '$location', 'career', 'careerClusterService', 'OccufindSearchService', function($scope, $location, career, careerClusterService, OccufindSearchService) {

	$scope.career = career.data;
	careerClusterService.careerClusters = career.data;

	$scope.selectedCareerCluster = function(ccId) {
		careerClusterService.selectedCareerCluster = ccId;
		$location.url('/career-cluster-occupation-results/' + ccId);
	}
	
	/**
	 * Tracks how many selections are made for skill selection section. Limit user selection to one.
	 */
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.checkChangedLimitOne = function(item) {
		if (item.selected)
			$scope.skillChecked++;
		else
			$scope.skillChecked--;
	}
	
	// for skill selection checkbox 
	$scope.skillSelectionOptions = [ {
		name : 'Verbal',
		value : 'verbalSkill',
		selected : false
	}, {
		name : 'Math',
		value : 'mathSkill',
		selected : false
	}, {
		name : 'Science/Technical',
		value : 'sciTechSkill',
		selected : false
	} ];


	/**
	 * Search feature
	 */
	$scope.stemOccupationFlag;
	$scope.brightOccupationFlag;
	$scope.greenOccupationFlag;
	$scope.hotOccupationFlag;

	$scope.interestCodes = [ {
		name : 'Realistic',
		value : 'R',
		selected : false
	}, {
		name : 'Investigative',
		value : 'I',
		selected : false
	}, {
		name : 'Artistic',
		value : 'A',
		selected : false
	}, {
		name : 'Social',
		value : 'S',
		selected : false
	}, {
		name : 'Enterprising',
		value : 'E',
		selected : false
	}, {
		name : 'Conventional',
		value : 'C',
		selected : false
	}, ];

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 2;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Search button sets up the search service.
	 */
	$scope.occufindSearch = function() {

		OccufindSearchService.userSearch = {
			userSearch : {
				interestCodeOne : undefined,
				interestCodeTwo : undefined,
				brightOccupationFlag : false,
				greenOccupationFlag : false,
				stemOccupationFlag : false,
				hotOccupationFlag : false,
				userSearchString : undefined
			}
		}

		// push skill selection value service object
		var isSkillSelected = false;
		for (var i = 0; i < $scope.skillSelectionOptions.length; i++) {

			if ($scope.skillSelectionOptions[i].selected == true) {
				isSkillSelected = true;
				OccufindSearchService.skillSelected = $scope.skillSelectionOptions[i].value;
			}
		}
		if (!isSkillSelected) {
			OccufindSearchService.skillSelected = undefined;
		}

		// push all interest code selection(s) to array
		var arraySelectedInterestCd = [];
		for (var i = 0; i < $scope.interestCodes.length; i++) {

			if ($scope.interestCodes[i].selected == true) {
				arraySelectedInterestCd.push($scope.interestCodes[i].value);
			}
		}

		// validate selection
		if (arraySelectedInterestCd < 1) {
			/*alert("Please select interest code(s)");
			return false;*/
		} else {

			// assign interest code selection(s) to occufind search service
			OccufindSearchService.userSearch.interestCodeOne = arraySelectedInterestCd[0];
			if (arraySelectedInterestCd.length == 2) {
				OccufindSearchService.userSearch.interestCodeTwo = arraySelectedInterestCd[1];
			}
		}

		// assign user's search selections to occufind search object
		OccufindSearchService.userSearch.brightOccupationFlag = $scope.brightOccupationFlag;
		OccufindSearchService.userSearch.greenOccupationFlag = $scope.greenOccupationFlag;
		OccufindSearchService.userSearch.stemOccupationFlag = $scope.stemOccupationFlag;
		OccufindSearchService.userSearch.userSearchString = $scope.searchString;
		OccufindSearchService.userSearch.hotOccupationFlag = $scope.hotOccupationFlag;

		$location.path('/occufind-occupation-search-results');

	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
} ]);
cepApp.controller('CareerClusterOccupationDetailsController', [ '$scope', '$location', 'careerClusterService', 'occupationTitleDescription', 'stateSalary', 'AuthenticationService', 'skillImportance', 'tasks', 'occupationInterestCodes', 'educationLevel', 'relatedOccupations', 'militaryResources', 'oohResources', '$q', function($scope, $location, careerClusterService, occupationTitleDescription, stateSalary, AuthenticationService, skillImportance, tasks, occupationInterestCodes, educationLevel, relatedOccupations, militaryResources, oohResources, $q) {

	$q.all([ occupationTitleDescription, skillImportance, stateSalary, tasks ]).then(function successCallback(response) {
		$scope.occupationTitle = occupationTitleDescription.data.title;
		$scope.occupationDescription = occupationTitleDescription.data.description;
		$scope.skillImportance = skillImportance.data;
		$scope.stateSalary = stateSalary.data;
		$scope.tasks = tasks.data;
		$scope.occupationInterestCodes = occupationInterestCodes.data;
		$scope.educationLevel = educationLevel.data;
		$scope.relatedOccupations = relatedOccupations.data;
		$scope.militaryResources = militaryResources.data;
		$scope.oohResources = oohResources.data;

		$scope.selectedCareerCluster = careerClusterService.selectedCareerCluster;
		$scope.occupationTitle = careerClusterService.selectedOcupationTitle;

		/**
		 * Skill rating
		 */
		angular.element(document).ready(function() {
			$('.customrating').each(function() {
				var rating = $(this).data('customrating');
				var width = $(this).width() / 5 * rating;
				if (rating.toString().indexOf('.') !== -1) {
					width += 1;
				}
				$(this).width(width);
			});
		});

		/**
		 * Salary map implementation
		 */
		var outerArray = [];
		var innerArray = [ 'State', 'AverageSalary' ];
		outerArray[0] = innerArray;
		for (var i = 0; i < $scope.stateSalary.length; i++) {
			var innerArray = [ 'US-' + $scope.stateSalary[i].state, $scope.stateSalary[i].avgSalary ];
			outerArray[i + 1] = innerArray;
		}

		var chart1 = {};
		chart1.type = "GeoChart";
		chart1.data = outerArray;

		chart1.options = {
			width : 406,
			height : 297,
			region : "US",
			resolution : "provinces",
			colors : [ '#e6eeff', '#0055ff' ]
		};

		/*
		 * chart1.formatters = { number : [{ columnNum: 1, pattern: "$ #,##0.00" }] };
		 */

		$scope.chart = chart1;

		$scope.logout = function() {
			AuthenticationService.ClearCredentials();
		};
		

		/**
		 * Pie chart Implementation
		 */
		$scope.chartObject = {};
		var col = [ {
			id : "e",
			label : "Education",
			type : "string"
		}, {
			id : "v",
			label : "Value",
			type : "number"
		} ];
		var row = [];

		for (var i = 0; i < $scope.educationLevel.length; i++) {
			var c = {
				c : [ {
					v : $scope.educationLevel[i].educationDesc
				}, {
					v : $scope.educationLevel[i].educationLvlVal
				} ]
			};
			row.push(c);
		}

		$scope.chartObject.data = {
			"cols" : col,
			"rows" : row
		};

		$scope.chartObject.type = "PieChart";
		$scope.chartObject.options = {
			'title' : 'EducationLevel',
			colors : [ '#0039b3', '#0051ff', '#4d85ff', '#99b9ff', '#e6eeff', '#001033', '#002880', '#0041cc', '#1a62ff', '#6696ff' ]
		};

		$scope.chartPie = $scope.chartObject;
	}, function errorCallback(response) {
		alert("error, try refreshing");
	});

} ]);

cepApp.controller('CareerClusterOccupationController', [ '$scope', '$rootScope', '$route', '$cookieStore', '$location', 'careerClusterService', 'AuthenticationService', 'occupationResults', 'OccufindSearchService', 'careerClusterById', function($scope, $rootScope, $route, $cookieStore, $location, careerClusterService, AuthenticationService, occupationResults, OccufindSearchService, careerClusterById) {
	//$scope.careerOccupation = careerClusterOccupationSearch.data;
	//$scope.careerClusters = careerClusterService.careerClusters;
	//$scope.selectedCareerCluster = careerClusterService.selectedCareerCluster;

	/*$scope.selectedCareerClusterOccupation = function(onetSoc, title) {
		careerClusterService.selectedOnetSoc = onetSoc;
		careerClusterService.selectedOcupationTitle = title;
	};*/
	
	$scope.ccId = $route.current.params.ccId;
	$scope.occupationResults = occupationResults.data;
	$scope.careerCluster = careerClusterById.data;
	$scope.skillSelected;
	
	$scope.byRange = function(fieldName, minValue, maxValue) {
		/*
		 * if (minValue === undefined) minValue = Number.MIN_VALUE; if (maxValue
		 * === undefined) maxValue = Number.MAX_VALUE;
		 */

		return function predicateFunc(item) {
			return minValue <= item[fieldName] && item[fieldName] <= maxValue;
		};
	};

	/**
	 * Pagination
	 */
	$scope.totalItems = $scope.occupationResults.length;
	//$scope.currentPage = OccufindSearchService.currentPage;
	$scope.currentPage = 1;
	//$scope.currentPage = $rootScope.globals.paginationPage == undefined ? 1 : $rootScope.globals.paginationPage;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 20;
	
	/**
	 * Utilizes $watch on skillSelected. If variable changes, this function will
	 * be executed to help toggle the list view and populate the lists for skill
	 * importance.
	 */
	$scope.$watch('skillSelected', function() {
		//OccufindSearchService.skillSelected = $scope.skillSelected;

		if ($scope.occupationResults != undefined) {

			// filter only if skill selection is made
			if ($scope.skillSelected == 'mathSkill' || $scope.skillSelected == 'verbalSkill' || $scope.skillSelected == 'sciTechSkill') {
				$scope.highFiltered = [];
				$scope.mediumFiltered = [];
				$scope.lowFiltered = [];
				var length = $scope.occupationResults.length;

				// filter high skill
				for (var i = 0; i < length; i++) {

					// determines which skill to filter
					var val;
					switch ($scope.skillSelected) {
					case 'mathSkill':
						val = $scope.occupationResults[i].mathSkill;
						break;
					case "verbalSkill":
						val = $scope.occupationResults[i].verbalSkill;
						break;
					case "sciTechSkill":
						val = $scope.occupationResults[i].sciTechSkill;
						break;
					}

					// filter by range
					if (4 <= val && val <= 5) {
						$scope.highFiltered.push($scope.occupationResults[i]);
					} else if (2.5 <= val && val <= 3.5) {
						$scope.mediumFiltered.push($scope.occupationResults[i]);
					} else if (0 <= val && val <= 2) {
						$scope.lowFiltered.push($scope.occupationResults[i]);
					} else {
						console.log("no data");
						console.log($scope.occupationResults[i]);
					}

				}

				// high filter pagination properties
				$scope.highCurrentPage = 1;
				$scope.highMaxSize = 4;
				$scope.highItemsPerPage = 20;

				$scope.setHighPage = function(pageNo) {
					$scope.highCurrentPage = pageNo;
				};

				// medium filter pagination properties
				$scope.mediumCurrentPage = 1;
				$scope.mediumMaxSize = 4;
				$scope.mediumItemsPerPage = 20;

				$scope.setMediumPage = function(pageNo) {
					$scope.mediumCurrentPage = pageNo;
				};

				// low filter pagination properties
				$scope.lowCurrentPage = 1;
				$scope.lowMaxSize = 4;
				$scope.lowItemsPerPage = 20;

				$scope.setLowPage = function(pageNo) {
					$scope.lowCurrentPage = pageNo;
				};
			}
		}

	});

	$scope.selectedOccupation = function(onetSoc, title, stem, bright, green, hot, interestCdOne, interestCdTwo, interestCdThree) {
		OccufindSearchService.selectedOnetSoc.push(onetSoc);
		OccufindSearchService.selectedOcupationTitle = title;
		OccufindSearchService.occupationGreen = green;
		OccufindSearchService.occupationStem = stem;
		OccufindSearchService.occupationBright = bright;
		OccufindSearchService.occupationHot = hot;
		OccufindSearchService.occupationInterestOne = interestCdOne;
		OccufindSearchService.occupationInterestTwo = interestCdTwo;
		OccufindSearchService.occupationInterestThree = interestCdThree;
		$location.url('/occufind-occupation-details/' + onetSoc);
	};

	/**
	 * Search feature
	 */
	$scope.stemOccupationFlag;
	$scope.brightOccupationFlag;
	$scope.greenOccupationFlag;
	$scope.hotOccupationFlag;

	$scope.interestCodes = [ {
		name : 'Realistic',
		value : 'R',
		selected : false
	}, {
		name : 'Investigative',
		value : 'I',
		selected : false
	}, {
		name : 'Artistic',
		value : 'A',
		selected : false
	}, {
		name : 'Social',
		value : 'S',
		selected : false
	}, {
		name : 'Enterprising',
		value : 'E',
		selected : false
	}, {
		name : 'Conventional',
		value : 'C',
		selected : false
	}, ];

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 2;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Search button sets up the search service.
	 */
	$scope.occufindSearch = function() {

		OccufindSearchService.userSearch = {
			userSearch : {
				interestCodeOne : undefined,
				interestCodeTwo : undefined,
				brightOccupationFlag : false,
				greenOccupationFlag : false,
				stemOccupationFlag : false,
				hotOccupationFlag : false,
				userSearchString : undefined
			}
		}

		OccufindSearchService.skillSelected = $scope.skillSelected;

		// push all interest code selection(s) to array
		var arraySelectedInterestCd = [];
		for (var i = 0; i < $scope.interestCodes.length; i++) {

			if ($scope.interestCodes[i].selected == true) {
				arraySelectedInterestCd.push($scope.interestCodes[i].value);
			}
		}

		// validate selection
		if (arraySelectedInterestCd < 1) {
			/*alert("Please select interest code(s)");
			return false;*/
		} else {

			// assign interest code selection(s) to occufind search service
			OccufindSearchService.userSearch.interestCodeOne = arraySelectedInterestCd[0];
			if (arraySelectedInterestCd.length == 2) {
				OccufindSearchService.userSearch.interestCodeTwo = arraySelectedInterestCd[1];
			}
		}

		// assign user's search selections to occufind search object
		OccufindSearchService.userSearch.brightOccupationFlag = $scope.brightOccupationFlag;
		OccufindSearchService.userSearch.greenOccupationFlag = $scope.greenOccupationFlag;
		OccufindSearchService.userSearch.stemOccupationFlag = $scope.stemOccupationFlag;
		OccufindSearchService.userSearch.userSearchString = $scope.searchString;
		OccufindSearchService.userSearch.hotOccupationFlag = $scope.hotOccupationFlag;

		$rootScope.globals.userSearch = OccufindSearchService.userSearch;
		$cookieStore.put('globals', $rootScope.globals);

		$location.path('/occufind-occupation-search-results');

	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

	$scope.logout = function() {
		AuthenticationService.ClearCredentials();
	};
} ]);
cepApp.controller('CareerClusterPathwayController', [ '$scope', '$location', 'resource', '$rootScope', '$window', function($scope, $location, resource, $rootScope, $window) {

	$scope.urlPrefix = resource + 'static/';
	$scope.urlSuffix = '_IMG.jpg';

	var loggedIn = $rootScope.globals.currentUser;

	$scope.url = 'occufind-occupation-details/';

	$scope.back = function() {
		$window.history.back();
	}
	
	$scope.scrollTop = function(){
		$window.scrollTo(0, 0);
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
cepApp.controller('CareerClusterPublicController', [ '$scope', '$location', 'career', 'careerClusterService', 'AuthenticationService', 'OccufindSearchService', function($scope, $location, career, careerClusterService, AuthenticationService, OccufindSearchService) {

	$scope.career = career.data;
	careerClusterService.careerClusters = career.data;

	$scope.selectedCareerCluster = function(ccId) {
		careerClusterService.selectedCareerCluster = ccId;
		$location.url('/career-cluster-pathway-' + ccId);
	}
	
	$scope.logout = function() {
		AuthenticationService.ClearCredentials();
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
cepApp.factory('careerClusterRestFactory', [ '$http', 'appUrl', function($http, appUrl) {

	var careerClusterRestFactory = {};

	careerClusterRestFactory.getCareerCluster = function() {
		return $http.get(appUrl + 'careerCluster/careerClusterList');
	}

	careerClusterRestFactory.getCareerClusterOccupation = function(ccId) {
		return $http.get(appUrl + 'careerCluster/careerClusterOccupationList/' + ccId);
	}
	
	careerClusterRestFactory.getCareerClusterOccupations = function(ccId) {
		return $http.get(appUrl + 'careerCluster/CareerClusterOccupations/' + ccId + '/');
	}
	
	careerClusterRestFactory.getCareerClusterById = function(ccId) {
		return $http.get(appUrl + 'careerCluster/CareerClusterById/' + ccId + '/');
	}
	
	careerClusterRestFactory.getStateSalary = function(onetSocTrimmed){
		return $http.get(appUrl + 'occufind/StateSalary/' + onetSocTrimmed + '/');
	}
	
	careerClusterRestFactory.getSkillImportance = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/occupationSkillImportance/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getTasks = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/careerClusterOccupationTaskList/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getOccupationInterestCodes = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/occupationInterestCodes/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getEducationLevel = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/occupationEducationLevel/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getRelatedOccupations = function(onetSoc){
		return $http.get(appUrl + 'careerCluster/relatedOccupation/' + onetSoc + '/');
	}
	
	careerClusterRestFactory.getMilitaryResources = function(onetSoc){
		return $http.get(appUrl + 'occufind/militaryCareerResource/' + onetSoc + '/');
	}

	careerClusterRestFactory.getOOHResources = function(onetSoc){
		return $http.get(appUrl + 'occufind/OOHResource/' + onetSoc + '/');
	}


	return careerClusterRestFactory;

} ]);
cepApp.service('careerClusterService', function() {
	var selectedCareerCluster;

	this.getSelectedCareerCluster = function() {
		return selectedCareerCluster;
	}

	var careerClusters;

	this.getCareerClusters = function() {
		return careerClusters;
	}

	var selectedOnetSoc;

	this.getSelectedOnetSoc = function() {
		return selectedOnetSoc;
	}
	
	var selectedOcupationTitle;
	
	this.getSelectedOcupationTitle = function(){
		return selectedOcupationTitle;
	}
});
cepApp.service('ApprenticeshipModalService', [ '$uibModal', '$location', '$http', 'modalService', function($uibModal, $location, $http, modalService) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/apprenticeship-list-modal.html',
		size : 'lg',                                                
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.isApprenticeshipsVisable = true;
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				$scope.loadingCareerOneInfo = false;
				$scope.apprenticeshipPagination = {};
				$scope.apprenticeshipPagination.TotalItems = 100; // occupationCertificates.CertificationInfo.CertificationList.length;
				$scope.apprenticeshipPagination.CurrentPage = 1;
				$scope.apprenticeshipPagination.MaxSize = 4;
				$scope.apprenticeshipPagination.ItemsPerPage = 10;
				$scope.onetOccupationCode = tempModalDefaults.socId
				$scope.occupationTitle = tempModalDefaults.occupationTitle;

				$scope.$watch('statesList.selectedOption', function() {
					console.log('the statesList.selectedOption has changed ....');
					// get licenses information ....
					var socId = $scope.onetOccupationCode;
					var location = $scope.statesList.selectedOption.value;
					$scope.loadingCareerOneInfo = true;
					$q.all([
					/* asyncLicense(socId, location), */
					asynApprenticeship(socId, location) ]).then(function(data) {
						$scope.loadingCareerOneInfo = false;

						if(IsJsonString(data[0].payload)){
							$scope.apprenticeshipList = JSON.parse(data[0].payload);
							// Apprenticeships ...
							var list = $scope.apprenticeshipList.ApprenticeshipSponsorList;
							$scope.apprenticeshipLabel = "";
							if (typeof list.length == 'undefined') {
								console.log('We have one ')
								$scope.apprenticeshipList = new Array();
								$scope.apprenticeshipList.push(list);
							} else {
								$scope.apprenticeshipList = list;
							}
							$scope.apprenticeshipPagination.TotalItems = $scope.apprenticeshipList.length;
						} else {
							$scope.apprenticeshipPagination.TotalItems = 0;
							$scope.apprenticeshipList = {};
							$scope.apprenticeshipLabel = "No apprenticeships for this state and occupation.";
						}
					}, function(reason) { // error
						$scope.loadingCareerOneInfo = false;
						$scope.licensesPagination.TotalItems = 0;
						$scope.apprenticeshipPagination.TotalItems = 0;
					}); // end of $q
				}); // end of watch state

				$scope.apprenticeship = {}
				$scope.findApprenticeship = function() {
					$scope.loadingCareerOneInfo = true;
					// get licenses information ....
					var socId = $scope.onetOccupationCode;
					var location = $scope.statesList.selectedOption.value;
					var promise = asynApprenticeship(socId, location);
					promise.then(function(greeting) {
						$scope.loadingCareerOneInfo = false;
						if(IsJsonString(greeting.payload)){
							conlose.log($scope.apprenticeshipList);
							$scope.apprenticeshipList = JSON.parse(greeting.payload);
							var list = $scope.apprenticeshipList;

							$scope.apprenticeshipLabel = "";

							if (typeof list.RecordCount < 2) {
								console.log('We have one ')
								$scope.apprenticeshipList = new Array();
								$scope.apprenticeshipList.push(list);
							} else {
								$scope.apprenticeshipList = list;
							}

							$scope.apprenticeshipPagination.TotalItems = $scope.apprenticeshipList.RecordCount;

							$("#apprenticeshipModalButton").click();
						} else {
							$scope.apprenticeshipList = greeting.payload;
							$scope.apprenticeshipPagination.TotalItems = 0;
							$scope.apprenticeshipList = {};
							$scope.apprenticeshipLabel = "No apprenticeships for this state and occupation.";

							return;
						}
					}, function(reason) {
						$scope.loadingCareerOneInfo = true;
						alert('Failed: ' + reason);
					}, function(update) {
						alert('Got notification: ' + update);
					});
				}

				function IsJsonString(str) {
				    try {
				        JSON.parse(str);
				    } catch (e) {
				        return false;
				    }
				    return true;
				}
				
				$scope.statesList = {
					availableOptions : [ {
						value : 'AL',
						name : 'ALABAMA'
					}, {
						value : 'AK',
						name : 'ALASKA'
					}, {
						value : 'AZ',
						name : 'ARIZONA'
					}, {
						value : 'AR',
						name : 'ARKANSAS'
					}, {
						value : 'CA',
						name : 'CALIFORNIA'
					}, {
						value : 'CO',
						name : 'COLORADO'
					}, {
						value : 'CT',
						name : 'CONNECTICUT'
					}, {
						value : 'DE',
						name : 'DELAWARE'
					}, {
						value : 'FL',
						name : 'FLORIDA'
					}, {
						value : 'GA',
						name : 'GEORGIA'
					}, {
						value : 'HI',
						name : 'HAWAII'
					}, {
						value : 'ID',
						name : 'IDAHO'
					}, {
						value : 'IL',
						name : 'ILLINOIS'
					}, {
						value : 'IN',
						name : 'INDIANA'
					}, {
						value : 'IA',
						name : 'IOWA'
					}, {
						value : 'KS',
						name : 'KANSAS'
					}, {
						value : 'KY',
						name : 'KENTUCKY'
					}, {
						value : 'LA',
						name : 'LOUISIANA'
					}, {
						value : 'ME',
						name : 'MAINE'
					}, {
						value : 'MD',
						name : 'MARYLAND'
					}, {
						value : 'MA',
						name : 'MASSACHUSETTS'
					}, {
						value : 'MI',
						name : 'MICHIGAN'
					}, {
						value : 'MN',
						name : 'MINNESOTA'
					}, {
						value : 'MS',
						name : 'MISSISSIPPI'
					}, {
						value : 'MO',
						name : 'MISSOURI'
					}, {
						value : 'MT',
						name : 'MONTANA'
					}, {
						value : 'NE',
						name : 'NEBRASKA'
					}, {
						value : 'NV',
						name : 'NEVADA'
					}, {
						value : 'NH',
						name : 'NEW HAMPSHIRE'
					}, {
						value : 'NJ',
						name : 'NEW JERSEY'
					}, {
						value : 'NM',
						name : 'NEW MEXICO'
					}, {
						value : 'NY',
						name : 'NEW YORK'
					}, {
						value : 'NC',
						name : 'NORTH CAROLINA'
					}, {
						value : 'ND',
						name : 'NORTH DAKOTA'
					}, {
						value : 'OH',
						name : 'OHIO'
					}, {
						value : 'OK',
						name : 'OKLAHOMA'
					}, {
						value : 'OR',
						name : 'OREGON'
					}, {
						value : 'PA',
						name : 'PENNSYLVANIA'
					}, {
						value : 'RI',
						name : 'RHODE ISLAND'
					}, {
						value : 'SC',
						name : 'SOUTH CAROLINA'
					}, {
						value : 'SD',
						name : 'SOUTH DAKOTA'
					}, {
						value : 'TN',
						name : 'TENNESSEE'
					}, {
						value : 'TX',
						name : 'TEXAS'
					}, {
						value : 'UT',
						name : 'UTAH'
					}, {
						value : 'VT',
						name : 'VERMONT'
					}, {
						value : 'VA',
						name : 'VIRGINIA'
					}, {
						value : 'WA',
						name : 'WASHINGTON'
					}, {
						value : 'WV',
						name : 'WEST VIRGINIA'
					}, {
						value : 'WI',
						name : 'WISCONSIN'
					}, {
						value : 'WY',
						name : 'WYOMING'
					}, {
						value : 'GU',
						name : 'GUAM'
					}, {
						value : 'PR',
						name : 'PUERTO RICO'
					}, {
						value : 'VI',
						name : 'VIRGIN ISLANDS'
					} ],
					selectedOption : {
						value : 'AL',
						name : 'ALABAMA'
					}
				};

				// /apprenticeship/{onet_soc}/{state}
				function asynApprenticeship(onetSoc, location) {
					var deferred = $q.defer();

					deferred.notify('Query for apprenticeship for ' + onetSoc + ' in ' + location);

					var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
					var careerOneStopUrl = '/CEP/rest/' + 'CareerOneStop/apprenticeship/' + onetOccupationCode + '/' + location;

					try {
						$http.get(careerOneStopUrl).then(function(response) {
							// $scope.Available = response.data; 
							//  window.alert(JSON.stringify(response.data));
							console.log('success');
							deferred.resolve(response.data);
						}, function(error) {

							console.log(error);
							deferred.reject('Greeting ' + name + ' is not allowed.');
						});
					} catch (err) {
						alert(err.message);
					}

					return deferred.promise;
				}

				$scope.findApprenticeship();
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('CertificationDetailsService', [ '$uibModal', '$location', '$http', 'modalService','$sce', function($uibModal, $location, $http, modalService,$sce) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/certification-details-modal.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

  	
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				$scope.occupationTitle = tempModalDefaults.occupationTitle;
	
				$scope.certDetail = {};
				$scope.certDetail.Name;
				$scope.certDetail.OrgName;
				$scope.certDetail.OrgAddress;
				$scope.certDetail.OrgWebPage;
				$scope.certDetail.Description;
				$scope.certDetail.Experience;
				$scope.certDetail.ExamDetails;
				$scope.certDetail.Exam;
				$scope.certDetail.Renewal;
				$scope.certDetail.CEU;
				$scope.certDetail.ReExam;
				$scope.certDetail.CPD;
				$scope.certDetail.xxx;

				$scope.openCertificateDetail = function(certificateDetail) {
				
					// get detailed information ....
						$scope.certDetail.Name = certificateDetail.Name;
						$scope.certDetail.OrgName = certificateDetail.Organization;
						$scope.certDetail.OrgAddress = certificateDetail.OrganizationAddress;
						$scope.certDetail.OrgWebPage = certificateDetail.OrganizationUrl;
						if (jQuery.isEmptyObject(certificateDetail.Description)) {
							$scope.certDetail.Description = '';
						} else {
							$scope.certDetail.Description = certificateDetail.Description.replace('&#xd;','\n');
						}

						$scope.certDetail.CertDetailList = certificateDetail.CertDetailList;


				}

				function IsJsonString(str) {
				    try {
				        JSON.parse(str);
				    } catch (e) {
				        return false;
				    }
				    return true;
				}
				
				// /certDetail/{cert_id}
				function asyncCertificationDetail(certId) {
					var deferred = $q.defer();

					deferred.notify('About to query licenses for ' + certId);

					var careerOneStopUrl = '/CEP/rest/CareerOneStop/certDetail/' + certId;

					try {
						$http.get(careerOneStopUrl).then(function(response) {
							// $scope.Available = response.data;
							// window.alert(JSON.stringify(response.data));
							console.log('success');
							deferred.resolve(response.data);
						}, function(error) {

							console.log(error);
							deferred.reject('Greeting ' + name + ' is not allowed.');
						});
					} catch (err) {
						alert(err.message);
					}

					return deferred.promise;
				}

				// 
				$scope.openCertificateDetail(tempModalDefaults.certId);
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('CertificationListModalService', [ '$uibModal', '$http','CertificationDetailsService', function($uibModal, $http, CertificationDetailsService) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/certification-list-modal.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.occupationCertificates = tempModalDefaults.certs;
				$scope.occupationTitle = tempModalDefaults.occupationTitle;

				// Following line in occufind since we pass scope it should be
				// there.
				// $scope.occupationCertificates =
				// JSON.parse(occupationCertificates.data.payload);
				$scope.isCertificationListVisable = true;
				$scope.loadingCareerOneInfo = false;
				/**
				 * Pagination for Certifications, Licenses and Apprenticeship
				 * Modals
				 */
				$scope.certPagination = {};
				$scope.certPagination.TotalItems = $scope.occupationCertificates.RecordCount;
				$scope.certPagination.CurrentPage = 1;
				$scope.certPagination.MaxSize = 4;
				$scope.certPagination.ItemsPerPage = 10;
				
				$scope.openCertificateDetail = function(certificateDetail) {
					/* alert(certId); */
					CertificationDetailsService.show({
						certId : certificateDetail,
						occupationTitle: $scope.occupationTitle
					}, {});
				}

			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};  // End of this.show 

} ]);
cepApp.service('LicenseDetailsModalService', [ '$uibModal', '$location', '$http','modalService', '$sce', function($uibModal, $location, $http,modalService, $sce) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/license-details-modal.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				var row = tempModalDefaults.row

				$scope.licenseDetails = {};
				$scope.licenseDetails.LICENSENAME= row.Title;
				$scope.licenseDetails.LICENSINGAGENCY= row.LicenseAgency.Name;
				$scope.licenseDetails.LICTITLE= row.Title;
				$scope.licenseDetails.DESCRIPTION= $sce.trustAsHtml(row.Description);
				$scope.licenseDetails.LICENSEAGENCY= row.LicenseAgency.Name;
				$scope.licenseDetails.ADDRESS1= row.LicenseAgency.Address;
				$scope.licenseDetails.CITY= row.LicenseAgency.City;
				$scope.licenseDetails.STATE= row.LicenseAgency.State;
				$scope.licenseDetails.ZIP= row.LicenseAgency.Zip;
				$scope.licenseDetails.URL= row.LicenseAgency.Url
				$scope.licenseDetails.EMAIL= row.LicenseAgency.Email;
				$scope.licenseDetails.TELEPHONE= formatPhoneNumber(row.LicenseAgency.Phone);
				
				function formatPhoneNumber(number) {

			        if (!number) { return ''; }

			        number = String(number);

			        var formattedNumber = number;

			        var c = (number[0] == '1') ? '1 ' : '';
			        number = number[0] == '1' ? number.slice(1) : number;

			        var area = number.substring(0,3);
			        var front = number.substring(3, 6);
			        var end = number.substring(6, 10);

			        if (front) {
			            formattedNumber = (c + "(" + area + ") " + front);  
			        }
			        if (end) {
			            formattedNumber += ("-" + end);
			        }
			        return formattedNumber;
			    }

			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('LicensesListModalService', [ '$uibModal', '$location', '$http', 'LicenseDetailsModalService','modalService', function($uibModal, $location, $http, LicenseDetailsModalService, modalService) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/career-one-stop/page-partials/licenses-list-modal.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$q', function($scope, $uibModalInstance, $q) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				$scope.licensesPagination = {};
				$scope.licensesPagination.TotalItems = 100; // occupationCertificates.CertificationInfo.CertificationList.length;
				$scope.licensesPagination.CurrentPage = 1;
				$scope.licensesPagination.MaxSize = 4;
				$scope.licensesPagination.ItemsPerPage = 10;
				$scope.onetOccupationCode = tempModalDefaults.socId
				$scope.occupationTitle = tempModalDefaults.occupationTitle;
				$scope.loadingCareerOneInfo = false;

				$scope.openLicenseDetail = function(record) {

					LicenseDetailsModalService.show({
						row :record
					}, {});


				}

				$scope.$watch('statesList.selectedOption', function() {
					console.log('the statesList.selectedOption has changed ....');
					// get licenses information ....

					var socId = $scope.onetOccupationCode;
					var location = $scope.statesList.selectedOption.value;
					$scope.loadingCareerOneInfo = true;

					var promise = asyncLicense(socId, location);
					promise.then(function(data) {
						$scope.loadingCareerOneInfo = false;

						if(data){
							if(IsJsonString(data.payload)){
								$scope.licensesList = JSON.parse(data.payload);
								var list = $scope.licensesList;
								$scope.licensesLabel = "";
								if (typeof list.length == 'undefined' && typeof list.LICENSEID != 'undefined') {
									console.log('We have one ')
									$scope.licensesList = new Array();
									$scope.licensesList.push(list);
								} else {
									$scope.licensesList = list.LicenseList;
								}
								$scope.licensesLabel = "";
								$scope.licensesPagination.TotalItems = $scope.licensesList.RecordCount;
							}else{
								$scope.licensesList = data.payload;
								$scope.licensesPagination.TotalItems = 0;
								$scope.licensesLabel = "No Licenses for this state and occupation.";
							}
						}

					}, function(reason) { // error
						$scope.loadingCareerOneInfo = false;
						$scope.licensesPagination.TotalItems = 0;
						$scope.apprenticeshipPagination.TotalItems = 0;
					}); // end of $q
				}); // end of watch

				$scope.openLicenses = function() {
					$scope.loadingCareerOneInfo = true;
					// get licenses information ....
					var socId = $route.current.params.socId;
					var location = $scope.statesList.selectedOption.value;
					var promise = asyncLicense(socId, location);
					promise.then(function(greeting) {
						$scope.loadingCareerOneInfo = false;
						$scope.licensesList = JSON.parse(greeting.payload);
						if ($scope.licensesList.Licenses.ErrorNumber) {
							$scope.licensesPagination.TotalItems = 0;
							var customModalOptions = {
								headerText : 'Licenses Error',
								bodyText : 'No Licenses found for this occupation.'
							};
							var customModalDefaults = {};
							modalService.show(customModalDefaults, customModalOptions);
							$("#licensesModalButton").click();
							return;
						}

						var list = $scope.licensesList.Licenses.LicenseList.License;

						if (typeof list.length == 'undefined' && typeof list.LICENSEID != 'undefined') {
							console.log('We have one ')
							$scope.licensesList = new Array();
							$scope.licensesList.push(list);
						} else {
							$scope.licensesList = list;
						}
						$scope.licensesPagination.TotalItems = $scope.licensesList.length;

						$("#licensesModalButton").click();
					}, function(reason) {
						$scope.loadingCareerOneInfo = false;
						alert('Failed: ' + reason);
					}, function(update) {
						alert('Got notification: ' + update);
					});
				}

				$scope.statesList = {
					availableOptions : [ {
						value : 'AL',
						name : 'ALABAMA'
					}, {
						value : 'AK',
						name : 'ALASKA'
					}, {
						value : 'AZ',
						name : 'ARIZONA'
					}, {
						value : 'AR',
						name : 'ARKANSAS'
					}, {
						value : 'CA',
						name : 'CALIFORNIA'
					}, {
						value : 'CO',
						name : 'COLORADO'
					}, {
						value : 'CT',
						name : 'CONNECTICUT'
					}, {
						value : 'DE',
						name : 'DELAWARE'
					}, {
						value : 'FL',
						name : 'FLORIDA'
					}, {
						value : 'GA',
						name : 'GEORGIA'
					}, {
						value : 'HI',
						name : 'HAWAII'
					}, {
						value : 'ID',
						name : 'IDAHO'
					}, {
						value : 'IL',
						name : 'ILLINOIS'
					}, {
						value : 'IN',
						name : 'INDIANA'
					}, {
						value : 'IA',
						name : 'IOWA'
					}, {
						value : 'KS',
						name : 'KANSAS'
					}, {
						value : 'KY',
						name : 'KENTUCKY'
					}, {
						value : 'LA',
						name : 'LOUISIANA'
					}, {
						value : 'ME',
						name : 'MAINE'
					}, {
						value : 'MD',
						name : 'MARYLAND'
					}, {
						value : 'MA',
						name : 'MASSACHUSETTS'
					}, {
						value : 'MI',
						name : 'MICHIGAN'
					}, {
						value : 'MN',
						name : 'MINNESOTA'
					}, {
						value : 'MS',
						name : 'MISSISSIPPI'
					}, {
						value : 'MO',
						name : 'MISSOURI'
					}, {
						value : 'MT',
						name : 'MONTANA'
					}, {
						value : 'NE',
						name : 'NEBRASKA'
					}, {
						value : 'NV',
						name : 'NEVADA'
					}, {
						value : 'NH',
						name : 'NEW HAMPSHIRE'
					}, {
						value : 'NJ',
						name : 'NEW JERSEY'
					}, {
						value : 'NM',
						name : 'NEW MEXICO'
					}, {
						value : 'NY',
						name : 'NEW YORK'
					}, {
						value : 'NC',
						name : 'NORTH CAROLINA'
					}, {
						value : 'ND',
						name : 'NORTH DAKOTA'
					}, {
						value : 'OH',
						name : 'OHIO'
					}, {
						value : 'OK',
						name : 'OKLAHOMA'
					}, {
						value : 'OR',
						name : 'OREGON'
					}, {
						value : 'PA',
						name : 'PENNSYLVANIA'
					}, {
						value : 'RI',
						name : 'RHODE ISLAND'
					}, {
						value : 'SC',
						name : 'SOUTH CAROLINA'
					}, {
						value : 'SD',
						name : 'SOUTH DAKOTA'
					}, {
						value : 'TN',
						name : 'TENNESSEE'
					}, {
						value : 'TX',
						name : 'TEXAS'
					}, {
						value : 'UT',
						name : 'UTAH'
					}, {
						value : 'VT',
						name : 'VERMONT'
					}, {
						value : 'VA',
						name : 'VIRGINIA'
					}, {
						value : 'WA',
						name : 'WASHINGTON'
					}, {
						value : 'WV',
						name : 'WEST VIRGINIA'
					}, {
						value : 'WI',
						name : 'WISCONSIN'
					}, {
						value : 'WY',
						name : 'WYOMING'
					}, {
						value : 'GU',
						name : 'GUAM'
					}, {
						value : 'PR',
						name : 'PUERTO RICO'
					}, {
						value : 'VI',
						name : 'VIRGIN ISLANDS'
					} ],
					selectedOption : {
						value : 'AL',
						name : 'ALABAMA'
					}
				};

				function asyncLicense(onetSoc, location) {
					var deferred = $q.defer();

					deferred.notify('About to query licenses for ' + onetSoc + ' in ' + location);

					var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
					var careerOneStopUrl = '/CEP/rest/' + 'CareerOneStop/license/' + onetOccupationCode + '/' + location.trim();

					try {
						$http.get(careerOneStopUrl).then(function(response) {
							console.log('Licenses fetch success');
							deferred.resolve(response.data);
						}, function(error) {
							console.log('Licenses fetch error' + error);
							deferred.reject(error);
						});
					} catch (err) {
						alert(err.message);
					}

					return deferred.promise;
				}
				function asyncLicenseDetail(onetSoc, licenseId, location) {
					var deferred = $q.defer();

					deferred.notify('About to query licenses for ' + licenseId);

					var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
					var careerOneStopUrl = '/CEP/rest/' + 'CareerOneStop/licenseDetail/' + onetOccupationCode + '/' + licenseId + '/' + location;

					try {
						$http.get(careerOneStopUrl).then(function(response) {
							// $scope.Available = response.data; 
							//  window.alert(JSON.stringify(response.data));
							console.log('success');
							deferred.resolve(response.data);
						}, function(error) {

							console.log(error);
							deferred.reject('Greeting ' + name + ' is not allowed.');
						});
					} catch (err) {
						alert(err.message);
					}

					return deferred.promise;
				}
				/////////////////////////
				
				function IsJsonString(str) {
				    try {
				        JSON.parse(str);
				    } catch (e) {
				        return false;
				    }
				    return true;
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.controller('CompareOccupationController', [ '$scope', 'occupationOne', 'occupationTwo', 'occupationThree', '$q', '$window', function($scope, occupationOne, occupationTwo, occupationThree, $q, $window) {

	// first occupation
	$scope.occupationOneTitle = occupationOne[0].data.title;
	$scope.occupationOneInterestCdOne = occupationOne[2].data[0].interestCd;
	$scope.occupationOneInterestCdTwo = occupationOne[2].data[1].interestCd;
	$scope.occupationOneInterestCdThree = occupationOne[2].data[2].interestCd;
	$scope.occupationOneSkillRating = occupationOne[1].data;
	$scope.occupationOneEducation = occupationOne[3].data;
	$scope.occupationOneServiceOffering = occupationOne[4].data;
	$scope.occupationOneNationalSalary = occupationOne[5].data;
	$scope.occupationOneNotes = occupationOne[6].data.note;

	// second occupation
	$scope.occupationTwoTitle = occupationTwo[0].data.title;
	$scope.occupationTwoInterestCdOne = occupationTwo[2].data[0].interestCd;
	$scope.occupationTwoInterestCdTwo = occupationTwo[2].data[1].interestCd;
	$scope.occupationTwoInterestCdThree = occupationTwo[2].data[2].interestCd;
	$scope.occupationTwoSkillRating = occupationTwo[1].data;
	$scope.occupationTwoEducation = occupationTwo[3].data;
	$scope.occupationTwoServiceOffering = occupationTwo[4].data;
	$scope.occupationTwoNationalSalary = occupationTwo[5].data;
	$scope.occupationTwoNotes = occupationTwo[6].data.note;

	// third occupation
	if (occupationThree != undefined) {
		$scope.occupationThreeTitle = occupationThree[0].data.title;
		$scope.occupationThreeInterestCdOne = occupationThree[2].data[0].interestCd;
		$scope.occupationThreeInterestCdTwo = occupationThree[2].data[1].interestCd;
		$scope.occupationThreeInterestCdThree = occupationThree[2].data[2].interestCd;
		$scope.occupationThreeSkillRating = occupationThree[1].data;
		$scope.occupationThreeEducation = occupationThree[3].data;
		$scope.occupationThreeServiceOffering = occupationThree[4].data;
		$scope.occupationThreeNationalSalary = occupationThree[5].data;
		$scope.occupationThreeNotes = occupationThree[6].data.note;
	}

	$scope.occupationThree = occupationThree;

	/**
	 * Skill rating
	 */
	angular.element(document).ready(function() {
		$('.customrating').each(function() {
			var rating = $(this).data('customrating');
			var width = $(this).width() / 5 * rating;
			if (rating.toString().indexOf('.') !== -1) {
				width += 1;
			}
			$(this).width(width);
		});
		
		
		/**
		 * New star rating.
		 */
		$(function() {
		    $( '.ratebox' ).raterater({ 

		    // allow the user to change their mind after they have submitted a rating
		    allowChange: false,

		    // width of the stars in pixels
		    starWidth: 12,

		    // spacing between stars in pixels
		    spaceWidth: 0,

		    numStars: 5,
		    isStatic: true,
		    step: false,
		    });

		});
	});

	/**
	 * Print and download PDF.
	 */
	var div = document.getElementById("printPdf");

	function getCanvas() {
		var defer = $q.defer();
		var t = html2canvas(div, {
			imageTimeout : 2000,
			removeContainer : true,
			background : undefined
		}).then(function(canvas) {
			var img = canvas.toDataURL("image/jpeg");
			defer.resolve(img);
		}, function(error) {
			defer.reject(error);
		});

		return defer.promise;
	}

	$scope.printPdf = function() {
		
		var t = getCanvas();
		
		// IE support
		if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {
			printIE(t);
			return false;
		}

		t.then(function(s) {

			var docDefinition = {
				content : [ {
					text : 'Occupation Comparison\n\n',
					alignment : 'center',
					fontSize : 18,
					bold : true
				}, {
					image : s,
					width : 520
				} ]
			};

			pdfMake.createPdf(docDefinition).print();
		}, function(error) {
			console.log(error);
			alert(error);
		});

	};
	

	/**
	 * IE support for printing element.
	 */
	function printIE(canvas) {
		
		canvas.then(function(img){
			var popup = window.open();
			popup.document.write('<img src=' + img + '>');
			popup.document.close();
			popup.focus();
			popup.print();
			popup.close();
			
		}, function(error){
			
			alert('Printing failed.');
		});
		
	}

	$scope.downloadPdf = function() {
		var t = getCanvas();

		t.then(function(s) {
			var docDefinition = {
				content : [ {
					text : 'Occupation Comparison\n\n',
					alignment : 'center',
					fontSize : 18,
					bold : true
				}, {
					image : s,
					width : 520
				} ]
			};

			pdfMake.createPdf(docDefinition).download('compare.pdf');
		}, function(error) {
			console.log(error);
			alert(error);
		});
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

cepApp.controller('ClassroomModalController', [ '$scope', 'ClassroomModalService', '$uibModal', function($scope, ClassroomModalService, $uibModal) {

		 $scope.classroomModal = function(type) {
			 ClassroomModalService.showModal({
					size : 'lg'
				}, {
				 type: type
			 });
		 }

} ]);
cepApp.controller('DashboardController', [ 
	'resource', '$scope', '$rootScope', '$route', '$templateCache', 'PortfolioRestFactory', 'numberTestTaken', '$window', 'UserFactory', 'fyiManualInput', 'cepManualInput', 'profileExist', 'userInterestCodes', 
	'PortfolioService', '$location', 'brightCareers', 'stemCareers', 'mediaCenterList', 'noteList', 'modalService', 'youTubeModalService', 'tiedLink', 'FYIScoreService', 'scoreSummary', 'asvabScore', 'isDemoTaken', 
	function(resource, $scope, $rootScope, $route, $templateCache, PortfolioRestFactory, numberTestTaken, $window, UserFactory, fyiManualInput, cepManualInput, profileExist, userInterestCodes, 
	PortfolioService, $location, brightCareers, stemCareers, mediaCenterList, noteList, modalService, youTubeModalService, tiedLink, FYIScoreService, scoreSummary, asvabScore, isDemoTaken) {

	$scope.currentUser = $rootScope.globals.currentUser;
	$scope.numberTestTaken = numberTestTaken;
	$scope.profileExist = profileExist.data;
	$scope.brightCareers = brightCareers[0];
	$scope.stemCareers = stemCareers[0];
	$scope.mediaCenterList = mediaCenterList;
	$scope.noteList = noteList;
	$scope.domainName = resource;
	$scope.tiedLink = tiedLink;
	$scope.isDemoTaken = isDemoTaken.data.r;
	
	$scope.userRole = $rootScope.globals.currentUser.role;

	$scope.interestCodeOne = userInterestCodes.interestCodeOne;
	$scope.interestCodeTwo = userInterestCodes.interestCodeTwo;
	$scope.interestCodeThree = userInterestCodes.interestCodeThree;
	$scope.scoreChoice = userInterestCodes.scoreChoice;

	$scope.scoreSummary = scoreSummary[0].data; // actual test score data
	$scope.asvabScore = asvabScore.data; // manually entered score data

	$scope.haveAsvabTestScores = (angular.isObject($scope.scoreSummary) && $scope.scoreSummary.hasOwnProperty('verbalAbility')); // do we have actual test scores
	$scope.haveManualAsvabTestScores = ($scope.asvabScore.length > 0 && !$scope.haveAsvabTestScores); // do we have manual test scores

	// Show initial option to enter scores full width (no interest codes, no actual scores, no entered scores)
	$scope.showInitial = ($scope.interestCodeOne == undefined && !$scope.haveAsvabTestScores && !$scope.haveManualAsvabTestScores);
	/**** Start Student Tracking ****/
	var user = $scope.currentUser;
	if (user.zipCode != undefined && user.zipCode != '') {
		ga('send', 'event', 'StudentLogin', 'ZipCode', user.zipCode);
	} 
	if (user.city != undefined && user.city != '') {
		ga('send', 'event', 'StudentLogin', 'City', user.city);
	} 
	if (user.mepsId != undefined && user.mepsId != '') {
		ga('send', 'event', 'StudentLogin', 'MEPs', user.mepsId);
	} 
	if (user.mepsName != undefined && user.mepsName != '') {
		ga('send', 'event', 'StudentLogin', 'MEPsName', user.mepsName);
	}
	if (user.state != undefined && user.state != '') {
		ga('send', 'event', 'StudentLogin', 'State', user.state);
	}
	if (user.userId != undefined && user.userId != '') {
		ga('send', 'event', 'StudentLogin', 'UserID', user.userId);
	}
	/**** End Student Tracking ****/
	/**** For Testing ****/
	//console.log($scope.scoreSummary);
	//console.log($scope.asvabScore);
	//$scope.haveAsvabTestScores = false;
	//$scope.haveManualAsvabTestScores = false;
	/*********************/

	$scope.getGenderCode = function() {
		// data should be f or m, but had m1 at one point hence substring
		var genderCode = ""; // none
		if ($scope.haveAsvabTestScores) {
			genderCode = $scope.scoreSummary.controlInformation.gender.substring(0,1).toLowerCase();
		}
		return genderCode;
	}

	$scope.getGenderFull = function(genderCode) {
		switch (genderCode) {
			case "f":
				genderCode = "Female";
				break;
			case "m":
				genderCode = "Male";
				break;
			default:
				genderCode = "Not Available";
				break;
		}
		return genderCode;
	}

	$scope.tiedLinkClick = function (){
		FYIScoreService.setTiedLinkClicked(true);
		$location.path('find-your-interest-score');
	}
	
	$scope.isShowTest = function() {
		var userRole = $rootScope.globals.currentUser.role;

		// student role can only
		if (userRole != 'A') {
			if ($scope.numberTestTaken < 2) {
				return true;
			}
			return false;
		}
		return true;
	}

	$scope.fyiManualInput = function() {
		fyiManualInput.show({}, {});
	}

	$scope.cepManualInput = function() {
		cepManualInput.show({}, {}).then(function(result) {
			//console.log(result);
			if (result) {
				if (result.hasOwnProperty('aFQTRawSSComposites')) {
					// Retrieved asvab scores
					$scope.scoreSummary = result;
					$scope.haveAsvabTestScores = true;
					$scope.haveManualAsvabTestScores = false;
					$scope.showInitial = false;
				} else {
					// Entered asvab scores
					$scope.haveAsvabTestScores = false;
					$scope.haveManualAsvabTestScores = true;
					$scope.showInitial = false;
					$scope.asvabScore.push({verbalScore : result.verbalScore, mathScore : result.mathScore, scienceScore : result.scienceScore});
				}
				
				// Update session storage
				$window.sessionStorage.setItem('manualScores', $scope.haveManualAsvabTestScores);
				$window.sessionStorage.setItem('sV', $scope.getScore('verbal'));
				$window.sessionStorage.setItem('sM', $scope.getScore('math'));
				$window.sessionStorage.setItem('sS', $scope.getScore('science'));
				$window.sessionStorage.setItem('sA', $scope.getScore('afqt'));

				// Update book stacks
				$scope.vBooks.score = $scope.getScore('verbal');
				$scope.mBooks.score = $scope.getScore('math');
				$scope.sBooks.score = $scope.getScore('science');
				$scope.scoreSorting();
			}
		});
	}

	/**
	 * Route user to portfolio page.
	 */
	$scope.routeToPortfolio = function() {
		var promise = PortfolioService.getIsPortfolioStarted();
		promise.then(function(success) {

			// if portfolio exists then skip instructions
			if (success > 0) {
				$location.path('portfolio');
			} else {
				$location.path('portfolio-directions');
			}
		}, function(error) {
			console.log(error);
		});

	}

	$scope.resultsInfo = function() {
		var customModalOptions = {
				headerText : 'ASVAB Results',
				bodyText : '<p>ASVAB Results More Information Coming Soon!</p>' + 
				'<p>Need content to display here.</p>'
			};
			var customModalDefaults = {size : 'lg'};
			modalService.show(customModalDefaults, customModalOptions);
	}
	
	$scope.interestCodes = function() {
		var customModalOptions = {
				headerText : 'Interest Code Legend',
				bodyText : ' <center><img src="/CEP/images/interest_codes_computer.png"></center> '
			};
			var customModalDefaults = {};
			modalService.show(customModalDefaults, customModalOptions);
	}
	
	
	$scope.selectedOccupation = function(onetSoc) {
		$location.url('/occufind-occupation-details/' + onetSoc);
	};
	
	$scope.interestCodeInfo = function(interestCode) {
		var modalOptions;
		switch (interestCode) {
		case "R":
			modalOptions = {
				headerText : 'Realistic',
				bodyText : '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
				'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
				'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
			};
			break;
		case "I":
			modalOptions = {
				headerText : 'Investigative',
				bodyText : '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
				'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
				'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
			};
			break;
		case "A":
			modalOptions = {
				headerText : 'Artistic',
				bodyText : '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
				'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
				'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
			};
			break;
		case "S":
			modalOptions = {
				headerText : 'Social',
				bodyText : '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
				'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
				'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
			};
			break;
		case "E":
			modalOptions = {
				headerText : 'Enterprising',
				bodyText : '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
				'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
				'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
			};
			break;
		case "C":
			modalOptions = {
				headerText : 'Conventional',
				bodyText : '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
				'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
				'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
			};
			break;
		}
		

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}
	
	$scope.showYouTubeModal = function(videoName) {
		youTubeModalService.show({
			size : 'lg'
		}, {
			videoName : videoName
		});
	}

	/**
	 * Get scores for display
	 */
	$scope.getScore = function(area) {
		var score = 0;

		switch (area) {
			case "verbal":
				if ($scope.haveAsvabTestScores) {
					score = $scope.scoreSummary ? Number($scope.scoreSummary.verbalAbility.va_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore);
				}
				break;
			case "math":
				if ($scope.haveAsvabTestScores) {
					score = $scope.scoreSummary ? Number($scope.scoreSummary.mathematicalAbility.ma_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore);
				}
				break;
			case "science":
				if ($scope.haveAsvabTestScores) {
					score = $scope.scoreSummary ? Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS) : 0;
				} else if ($scope.haveManualAsvabTestScores) {
					score = Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore);
				}
				break;
			case "afqt": // no manual entry for AFQT
				score = $scope.scoreSummary ? Number($scope.scoreSummary.aFQTRawSSComposites.afqt) : 0;
				break;
			default:
				break;
		}
		return score;
	}
	
	// Add scores to session.
	if ($scope.haveAsvabTestScores) {
		$window.sessionStorage.setItem('manualScores', false);
		$window.sessionStorage.setItem('sV', $scope.getScore('verbal'));
		$window.sessionStorage.setItem('sM', $scope.getScore('math'));
		$window.sessionStorage.setItem('sS', $scope.getScore('science'));
		$window.sessionStorage.setItem('sA', $scope.getScore('afqt'));
	} else if ($scope.haveManualAsvabTestScores) {
		$window.sessionStorage.setItem('manualScores', true);
		$window.sessionStorage.setItem('sV', Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore));
		$window.sessionStorage.setItem('sM', Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore));
		$window.sessionStorage.setItem('sS', Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore));
	}

	// Create book objects for sort/ranking - display gray book (score: 0) for manual scores
	$scope.vBooks = { 
		name: "verbal",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sV') : 0
		score: $window.sessionStorage.getItem('sV')
	}

	$scope.mBooks = {
		name: "math",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sM'): 0
		score: $window.sessionStorage.getItem('sM')
	}

	$scope.sBooks = {
		name: "science",
		//score: $scope.haveAsvabTestScores ? $window.sessionStorage.getItem('sS') : 0
		score: $window.sessionStorage.getItem('sS')
	}	

	/**
	 * Calculate book sprite x pos from ranked score
	 * 1 book = lowest score
	 * 2 books = middle score
	 * 3 books = highest score
	 * Ties would use just 2- and 3-book stacks, with the duplicate 
	 * determined by whether the tie was with the highest or the lowest score
	 */
	$scope.scoreSorting = function() {
		$scope.scoresToSort = [$scope.vBooks, $scope.mBooks, $scope.sBooks];
		$scope.sortedScores = [];

		for (var i = 0; i < $scope.scoresToSort.length; i++) {
			$scope.sortedScores.push($scope.scoresToSort[i]);
		}

		$scope.sortedScores.sort(function(a, b) {
			return a.score-b.score;
		});

		for(var i = 0; i < $scope.sortedScores.length; i++) {
			$scope.sortedScores[i].rank = i + 1;
			$scope.sortedScores[i].tie = false;
		}
		$scope.scoreRanking();
	}

	$scope.scoreRanking = function() {
		for (var k = 0; k < $scope.sortedScores.length; k++) {
			for (var h = 1; h < $scope.sortedScores.length + 1; h++) {
				if ($scope.sortedScores[k+h] !== undefined) {
					if ($scope.sortedScores[k+h].tie !== true) {
						if ($scope.sortedScores[k].score === $scope.sortedScores[h + k].score) {
							$scope.sortedScores[k].rank = k + 1;
							$scope.sortedScores[h + k].rank = k + 1;
							$scope.sortedScores[k].tie = true;
							$scope.sortedScores[h + k].tie = true;
						}
					}
				}    
			}
			
			// if tie scores, 2 books for low score tie, 3 books for high score tie
			if ($scope.sortedScores[k].tie) {
				if ($scope.sortedScores[k].rank == 1) {
					$scope.sortedScores[k].rank = 2; // low tie
				} else {
					$scope.sortedScores[k].rank = 3; // high tie
				}
			}
			
			// no books for 0 score
			if ($scope.sortedScores[k].score == 0) {
				$scope.sortedScores[k].rank = 0; // no score
			}

			// set xPos based on rank, -60px incraments
			$scope.sortedScores[k].xPos = $scope.sortedScores[k].rank * -32; // larger book stack (on dash)
			$scope.sortedScores[k].xPosSmall = $scope.sortedScores[k].rank * -32; // smaller book stack (in left nav)
			
		}


		// Add book objects to session
		$window.sessionStorage.setItem('vBooks', angular.toJson($scope.vBooks));
		$window.sessionStorage.setItem('mBooks', angular.toJson($scope.mBooks));
		$window.sessionStorage.setItem('sBooks', angular.toJson($scope.sBooks));
	}

	/**
	 * Get book sprite x pos
	 */
	$scope.getBookPos = function(area) {
		// return bsackground style position object
		switch (area) {
			case "verbal":
				return {"background-position": $scope.vBooks.xPos + "px 0px"};
			case "math":
				return {"background-position": $scope.mBooks.xPos + "px 0px"};
			case "science":
				return {"background-position": $scope.sBooks.xPos + "px 0px"}; 
		}
	}

	/**
	 * Toggle score book/table display
	 */
	$scope.showScoresTable = false;
	$scope.toggleScoreView = function() {
		$scope.showScoresTable = $scope.showScoresTable === false;
	}

	// Get gender from asvab scor data if we have it, store it globally
	$rootScope.globals.currentUser.gender = $scope.getGenderFull($scope.getGenderCode());
	
	// Initial book sorting/ranking
	$scope.scoreSorting();

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
		
		/**
		 * Flyout JQuery. Teacher demo.
		 */
		if (($scope.userRole == 'E' || $scope.userRole == 'D')
			&& $scope.isDemoTaken == 0) {
			
			$(function() {
				
				$("document").ready(function(){
					$("#load-popup").trigger("click");
					$(".swlFlyout ").addClass("hiddan_arrow");
				});		
		        $('#load-popup').flyout({
		            content: '<h2 id="tour-title" class="swlFlyout_title">Get familiar with<br/> MY ASVAB CEP</h2><a class="next1">TAKE THE TOUR</a><i class="click1">&times;</i>',
		            html: true,
		            placement: 'top'
		        });
				$(document).on('click', '.next1', function(e) {
					/*$("#sample_score").trigger('click');*/
					$("#sample_score").flyout('show');
					$("#load-popup").trigger('click');
					$('html, body').animate({scrollTop: $('#sample_score').offset().top -300 }, 600);
				});
				$(document).on('click', '.click1', function(e) {
					$("#load-popup").trigger('click');
				});	
				
				
		        $('#sample_score').flyout({
		            title: 'UNDERSTAND ASVAB SCORES',
		            content: '<p>Career Exploration Scores represent strengths in Verbal, Math, and Science/Technical skills. </p><a class="next2">next</a><i class="click2">&times;</i>',
		            html: true,
		            placement: 'left', 
		            trigger: 'manual'
		        });
				$(document).on('click', '.next2', function(e) {
					$("#skill_strength").trigger('click');
					$(".click2").trigger('click');
					$('html, body').animate({scrollTop: $('#skill_strength').offset().top -300 }, 600);
				});			
				$(document).on('click', '.click2', function(e) {
					/*$("#sample_score").trigger('click');*/
					$("#sample_score").flyout('hide');
				});
	
	
		        $('#skill_strength').flyout({
		            title: 'UNDERSTAND ASVAB SCORES',
		            content: '<p>The most books signify skill strength.</p><a class="next3">next</a><i class="click3">&times;</i>',
		            html: true,
		            placement: 'right'
		        });
				$(document).on('click', '.next3', function(e) {
					/*$("#see_result").trigger('click');*/
					$("#see_result").flyout('show');
					$(".click3").trigger('click');
					$('html, body').animate({scrollTop: $('#see_result').offset().top -300 }, 600);
				});			
				$(document).on('click', '.click3', function(e) {
					$("#skill_strength").trigger('click');
				});	
	
	
		        $('#see_result').flyout({
		            title: 'UNDERSTAND ASVAB SCORES',
		            content: '<p>Access online score report and Understanding Scores tutorial.</p><a class="next4">next</a><i class="click4">&times;</i>',
		            html: true,
		            placement: 'left',
		            trigger: 'manual'
		        });
				$(document).on('click', '.next4', function(e) {
					$("#step1").trigger('click');
					$(".click4").trigger('click');
					$('html, body').animate({scrollTop: $('#step1').offset().top -300 }, 600);
				});			
				$(document).on('click', '.click4', function(e) {
					/*$("#see_result").trigger('click');*/
					$("#see_result").flyout('hide');
				});
				
					
		        $('#step1').flyout({
		            title: 'FIND YOUR INTERESTS ',
		            content: '<p>90 item interest inventory based on John Holland<em>s</em> codes of career choice.</p><a class="next5">next</a><i class="click5">&times;</i>',
		            html: true,
		            placement: 'top'
					
		        });
				$(document).on('click', '.next5', function(e) {
					$("#step2").trigger('click');
					$(".click5").trigger('click');
					$('html, body').animate({scrollTop: $('#step2').offset().top -300 }, 600);
				});			
				$(document).on('click', '.click5', function(e) {
					$("#step1").trigger('click');
				});
				
				
		        $('#step2').flyout({
		            title: 'SEARCH THE OCCU-FIND',
		            content: '<p>Sort 1,000+ careers using your unique skill & interest combination to find careers that match. Learn about the variety of ways to get started in any career.</p><a class="next6">next</a><i class="click6">&times;</i>',
		            html: true,
		            placement: 'top'
		        });
				$(document).on('click', '.next6', function(e) {
					$("#step3").trigger('click');
					$(".click6").trigger('click');
					$('html, body').animate({scrollTop: $('#step3').offset().top -300 }, 600);
				});			
				$(document).on('click', '.click6', function(e) {
					$("#step2").trigger('click');
				});	
				
				
		        $('#step3').flyout({
		            title: 'INTEGRATE CAREER EXPLORATION INTO YOUR CURRICULUM',
		            content: '<p>Create a plan, track accomplishments, and favorite careers. </p><a class="next7">next</a><i class="click7">&times;</i>',
		            html: true,
		            placement: 'top'
		        });
				$(document).on('click', '.next7', function(e) {
					/*$("#classroom_activities").trigger('click');*/
					$("#classroom_activities").flyout('show');
					$(".click7").trigger('click');
					$('html, body').animate({scrollTop: $('#classroom_activities').offset().top -300 }, 600);
				});			
				$(document).on('click', '.click7', function(e) {
					$("#step3").trigger('click');
				});	
	
				
		        $('#classroom_activities').flyout({
		            title: 'INTEGRATE CAREER EXPLORATION INTO YOUR CURRICULUM',
		            content: '<p>Access activities you can use now to connect your classroom to the real world.</p><a class="next8">Finished</a><i class="click8">&times;</i>',
		            html: true,
		            trigger: 'manual'
		        });
		        
		        $scope.demoButtonDisable = false;
				$(document).on('click', '.click8,.next8', function(e) {
					
					// Store teacher demo completion information to database.
					if (!$scope.demoButtonDisable) { // disable if but was clicked
						
						$scope.demoButtonDisable = true; // disable button
						
						var obj = {
							userId : $rootScope.globals.currentUser.userId
						};

						var promise = PortfolioRestFactory.insertTeacherDemoIsTaken(obj);
						promise.then(function(response) {
							$("#classroom_activities").flyout('hide');
						}, function(reason) {
							$scope.demoButtonDisable = false; // enable button if failed
						}, function(update) {
						});
					}
					
				});
				
					
				$(document).on('click', '.swlFlyout_content > a', function(e) {
					return false;
				});	
	
		    });
			
			$(function() {
			    if ($(window).width() < 640) {
					
					$("document").ready(function(){
						$('html, body').animate({scrollTop: $('#load-popup').offset().top -260 });
					});		
					
					
			        $('#sample_score').flyout({
			            title: 'UNDERSTAND ASVAB SCORES',
			            content: '<p>Career Exploration Scores represent strengths in Verbal, Math, and Science/Technical skills. </p><a class="next2">next</a><i class="click2">x</i>',
			            html: true,
			            placement: 'top'
			        });
	
	
			        $('#skill_strength').flyout({
			            title: 'UNDERSTAND ASVAB SCORES',
			            content: '<p>The most books signify skill strength.</p><a class="next3">next</a><i class="click3">x</i>',
			            html: true,
			            placement: 'bottom'
			        });
	
	
			        $('#see_result').flyout({
			            title: 'UNDERSTAND ASVAB SCORES',
			            content: '<p>Access online score report and Understanding Scores tutorial.</p><a class="next4">next</a><i class="click4">x</i>',
			            html: true,
			            placement: 'bottom'
			        });
				}
	
				
			});
		}
	});
	

} ]);

cepApp.service('cepManualInput', [ 
	'$uibModal', 'FYIScoreService', '$location', 'FYIRestFactory', '$rootScope', '$q', 'PortfolioRestFactory', 
	function($uibModal, FYIScoreService, $location, FYIRestFactory, $rootScope, $q, PortfolioRestFactory) {
	
	var modalDefaults = {
		animation : true,
		size : 'lg',
		templateUrl : 'cep/components/dashboard/page-partials/cep-manual-input.html'
	};

	var modalOptions = {};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {
		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$rootScope', '$uibModalInstance', '$route', function($scope, $rootScope, $uibModalInstance, $route) {
				$scope.modalOptions = tempModalOptions;

				$scope.enter = $scope.modalOptions.enter;
				$scope.retrieve = $scope.modalOptions.retrieve;

				$scope.displayOptions = function(viewFor) {
					switch (viewFor) {
						case 'enter':
							$scope.enter = true;
							$scope.retrieve = false;
							break;
						case 'retrieve':
							$scope.retrieve = true;
							$scope.enter = false;
							break;
						case 'initial':
							$scope.enter = false;
							$scope.retrieve = false;
							break;
					}
				}
				

				$scope.modalOptions.submitAsvabScores = function(scoreObj) {
					// add userId - only insert scores not update from dahsboard
					scoreObj.userId = $rootScope.globals.currentUser.userId;
					var promise = PortfolioRestFactory.insertAsvabScore(scoreObj);
					//var promise = PortfolioRestFactory.updateAsvabScore(scoreObj); // returns bad request
					promise.then(function(response) {
						$route.reload();
						$uibModalInstance.close(scoreObj);
					}, function(reason) {
						console.log(reason);
						alert("Failed to save ASVAB score. Try again. If error persist, please contact site administrator.");
					}, function(update) {
						alert('Got notification: ' + update);
					});
					
				};
				$scope.modalOptions.check = function() {
					var accessCode = $scope.accessCode;
					//var userId = $rootScope.globals.currentUser.userId;
					
					var promise = FYIRestFactory.getTestScoreWithAccessCode(accessCode);
					promise.then(function(response) {
						$uibModalInstance.close(response.data);
					}, function(reason) {
						console.log(reason);
						alert("Failed to retrieve your ASVAB scores. Try again. If error persist, please contact site administrator.");
					}, function(update) {
						alert('Got notification: ' + update);
					});
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}
		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('ClassroomModalService', [ '$uibModal', 'resource', function($uibModal, resource) {
	
	var getCitMLink = function () {
		if (location.href.indexOf('localhost') > -1 || location.href.indexOf('asvabcep') > -1) {
			return "http://asvabcitm.humrro.org";
		} else {
			return "http://www.careersinthemilitary.com";
		}
	}
	
	var getCepLink = function () {
		if (location.href.indexOf('localhost') > -1 || location.href.indexOf('asvabcep') > -1) {
			return "http://asvabcep.humrro.org";
		} else {
			return "http://www.asvabprogram.com";
		}
	}
	
	var modalDefaults = {
			animation : true,
			size : 'lg',
			templateUrl : 'cep/components/dashboard/page-partials/classroom-modal.html'
		};

	var modalOptions = {};
	
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				
				$scope.documents = resource + 'pdf/';

				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				
				switch (customModalOptions.type) {
					case "inspire":
						$scope.title = "Share to Inspire";
						$scope.text =  " <p>After participating in the ASVAB CEP, make a short video about how the program helped you.  Inspire other high school students to take the" +
									   " time to learn about careers, ways to get there, and how important self-knowledge is while making these types of decisions.  Identify which" +
									   " pieces of information were most helpful.</p><strong>Don&#39;t forget to #whatsyourdreamjob #ASVABCEP</strong>";
						$scope.link =  "ASVAB_CEP_Share_to_Inspire.pdf"
						break;
					case "listen":
						$scope.title = "Listen Before You Launch";	
						$scope.text =  "<p>For this project, you will conduct an interview with someone who has retired from his or her profession.  Ask them the following questions:</p>"+
								" <p>1.	How did you choose your career?</p>"+
								" <p>2. 	When did you decide this is the career you wanted?</p>"+
								" <p>3. 	How did you prepare for your first job?</p>"+
								" <p>4.	What was the first thing you learned on the job?</p>"+
								" <p>5.	Tell me about how your career field has changed over the years.  Can you tell me about a task you did every day and how it changed?</p>"+
								" <p>6. 	What are the pros and cons of being in this career?</p>"+
								" <p>7.	Who helped you the most in your career?</p>"+
								" <p>8.	What is the best advice you can give to me about choosing this career path?</p>"+
								" <p>9. 	Can you teach me something related to this type of work?</p>"+	
								" <p>Now, using the information you received, create a presentation about the person&#39;s life and profession.  How did this help you prepare for your" +
								" career goals. How will you adapt to the changes that will change your job over time?  What does the OCCU-Find say about the national employment" +
								" outlook for your career?  (to find this information, use the OCCU-Find search tool to locate your occupation, then under employment statistics," +
								" where the map is located, click more details.)</p>";
						$scope.link = 	"ASVAB_CEP_Listen_Before_you_Launch.pdf"							
						break;
					case "cte":
						$scope.title = "CTE Reel Review";				
						$scope.text =  "<p>Identify a CTE career you are interested in, and find a movie or TV show that portrays someone in that occupation.  Using the OCCU-Find, under &#34;What" +
								" they do&#34;, check out the job tasks that are listed.  Which of these job tasks are portrayed in the movie or show?  What tools were used to help them" +
								" in their job?  Do these tools actually exist?  (Find reference) Was this career presented realistically?  Create a YouTube video of your review of" +
								" the movie in terms of how it depicts career information.</p> <strong>Don&#39;t forget to #ASVABCEP</strong>";
						$scope.link = 	"ASVAB_CEP_CTE_Reel_Review.pdf"							
						break;
					case "essay":
						$scope.title = "Career Exploration Essay";				
						$scope.text =  "<p>The OCCU-Find contains many data points that are important to explore while you are thinking about careers.  Choose three jobs you have identified" +
								" as a potential career choice. Explain why they are interesting to you. Your essay should include a plan A and and plan B of how to reach each of" +
								" the career goals.  For instance, if you chose a job that requires an advanced degree, but do not have money for an advanced degree, write about" +
								" how you can still reach you career goals. Remember, there are many pathways, many ways to get experience, and many related jobs.</p>";
						$scope.link = 	"ASVAB_CEP_Career_Exploration_Essay.pdf"
						break;
					case "education":
						$scope.title = "My Education and Career Plans";				
						$scope.text =  "<p>Complete My Education and Career Plans worksheet. Then complete portfolio and print or email to your counselor.</p>";
						$scope.link = 	"ASVAB_CEP_Edu_and_Career_Plans.pdf"
						break;
					case "benefit":
						$scope.title = "Career Decisions: Cost Benefit Analysis";				
						$scope.text =  "<p>You are 24 years old and looking forward to being a financially independent individual. You will need to create a detailed future financial plan and" +
								" monthly budget to live the kind of life you have always dreamed of! You currently have $6500 in a Savings account. Since you are independent, you" +
								" have no support from roommates or parents, but if you are going to be married or have children, you will need to add in all the additional costs." +
								" First, you will need to use the OCCU-Find to choose a career, and determine how you will reach this career goal by age 24. Also, you will need to" +
								" take into account the cost of living differences.  If you live in CA vs. TN, the salaries are higher, but so is the cost of living. "+
							    " Your presentation will need to include the following elements (and references to sites you used for figures):  Career earnings and federal and state" +
							    " taxes deducted from your pay check, how you got there and any costs associated with that path (school loans, etc), geographic location, vehicle," +
							    " housing, food, personal expenses (cell phone, cable, utilities, gas, clothes, makeup, deodorant, etc.), gifts, donations, vacations, etc.</p>";
						$scope.link = 	"ASVAB_CEP_Career_Decisions-Cost_Benefit_Analysis.pdf"
						break;
					case "collage":
						$scope.title = "Career Collage Project";				
						$scope.text = "<p>Think back on all the times someone has asked you about your future career goals:  What do you want to be when you grow up?</p>" +
								" <p>You will be using poster board to make a collage of all the jobs you have ever answered that question with&#46;&#46;&#46;. ballet dancer," +
								" race car driver, doctor, trash collector, bagel shop owner, etc. Find a picture of each one (you will need at least 25 pictures).</p>" +
								" <p>Now, they need to be organized in some way. What was appealing about each of these jobs to you?  Were your answers related to" +
								" your aptitude? or your skills?  Were your answers influenced by others? Were your answers related to your interests? What do all" +
								" these careers have in common?  Look up the careers using the OCCU-Find tool, is the description of the job what you thought it" +
								" would be like?  Now, ask yourself which of these careers would be attainable for you.  Present your Career Collage project to your" +
								" class.  Make sure you can answer questions like:  What is a typical day like for each of the jobs you have considered throughout your" +
								" life?  What classes in high school would you take to prepare for each of these careers?  What is the outlook like for each of these" +
								" careers?  What was the most interesting thing you learned about each of your careers?</p>";
						$scope.link = 	"ASVAB_CEP_Career_Collage_Project.pdf"
						break;
					case "story":
						$scope.title = "Write Your Story";				
						$scope.text =  "<p>Create a picture book (paper and pencil or electronic) about a career you are exploring.  This book will need to contain pictures that demonstrate what you" +
								" like about this career.  The book will be for children 3 and up and should contain important information to help the child understand the career, explain" +
								" the day to day tasks, and share your excitement.  The story should not be complicated, but should contain at least 10 pages.  You can draw, use clip art," +
								" photographs, magazine clippings, or paint in order to create your illustrations for your story.  Don&#39;t forget to include an &#39;about the author&#39; page with" +
								" your short bio and how you identified this career as one worth pursuing.</p>"; 
						$scope.link = 	"ASVAB_CEP_Write_Your_Story.pdf"
						break;
					case "reel":
						$scope.title = "Reel Career";				
						$scope.text =  "<p>Identify an occupation you are interested in, and find a movie or TV show that portrays someone in that occupation.  Using the OCCU-Find, under &#34;What they do&#34;," +
								" check out the job tasks that are listed.  Which of these job tasks are portrayed in the movie or show?  Was this career presented realistically?  Create a" +
								" YouTube video of your review of the movie in terms of how it depicts career information.</p> <strong>Don&#39;t forget to #ASVABCEP</strong>";
						$scope.link = 	"ASVAB_CEP_Reel_Career.pdf"
						break;
					case "history":
						$scope.title = "History and New Found Knowledge";				
						$scope.text =  "<p>For this project, you will conduct an interview with someone who has retired from his or her profession.  Ask them the following questions:</p>"+
								" <p>1.	 How did you choose your career?  When did you decide this is the career you wanted?</p>"+
								" <p>2.	How did you prepare for your first job interview in this career?</p>"+
								" <p>3.	What was the first thing you learned on the job?</p>"+
								" <p>4.	Tell me about how your career field has changed over the years.  Can you tell me about a task you did every day and how it changed?</p>"+
								" <p>5.	What are the pros and cons of being in this career?</p>"+
								" <p>6.	Who helped you the most in your career?</p>"+
								" <p>7.	What is the best advice you can give to me about choosing a career path?</p>"+
								" <p>Now, using the information you received, create a power point presentation about the person&#39;s life and profession.  Be sure to include slides about how" +
								" you will use this information to prepare for your career goals.  Other slides to include:  How will you adapt to the changes that are most likely going" +
								" to change your job over time?  What does the OCCU-Find say about the national employment outlook for your career?  (to find this information, use the" +
								" OCCU-Find search tool to locate your occupation, then under employment statistics, where the map is located, click more details.)</p>";
						$scope.link = 	"ASVAB_CEP_History_and_New_Found_Knowledge.pdf"
						break;
					case "family":
						$scope.title = "Family Career Tree";				
						$scope.text =  "<p>Research your family&#39;s career history and create a family tree tracing back four or more generations.  Include at least 2 stories about your family" +
								" members and why they chose a certain profession.  Include names, birthdays, locations, career information (where they worked, what they did, how long" +
								" they worked, how they got there etc).  If you have family members who served in the military, remember to ask them about their specific jobs." +
								"  Do you think it is easier or more difficult to find a career now?  Use information form the OCCU-Find to back up your answer.</p>";
						$scope.link = 	"ASVAB_CEP_Familiy_Career_Tree.pdf"
						break;
					case "plan":
						$scope.title = "Plan Your Adventure";				
						$scope.text =  "<p>The OCCU-Find contains many data points that are important to explore while you are thinking about careers.  Choose three STEM jobs you have identified" +
								" as a potential career choice. Explain why they are interesting to you. Your essay should include a plan A and and plan B of how to reach each of the" +
								" career goals.  For instance, if you chose a job that requires an advanced degree, but do not have money for an advanced degree, write about how you" +
								" can still reach you career goals. Remember, there are many pathways, many ways to get experience, and many related jobs.</p>";
						$scope.link = 	"ASVAB_CEP_Plan_Your_Adventure.pdf"
						break;
					case "old":
						$scope.title = "Learn from the Old Win with STEM";				
						$scope.text =  "<p>For this project, you will conduct an interview with someone who has retired from a STEM career.  Ask them the following questions:</p>" +
								" <p>1.   How did you choose your career?</p>" +
								" <p>2.   When did you decide this is the career you wanted?</p>" +
								" <p>3.   How did you prepare for your first job?</p>" +
								" <p>4.   What was the first thing you learned on the job?</p>" +
								" <p>5.   Tell me about how your career field has changed over the years.  Can you tell me about a task you did every day and how it has changed?</p>" +
								" <p>6.   What are the pros and cons of being in this career?</p>" +
								" <p>7.   Who helped you the most in your career?</p>" +
								" <p>8.   What is the best advice you can give to me about choosing this career path?</p>" +
								" <p>9.   How do you see this job changing in the future?</p>" +
								" <p>Now, using the information you received, create a presentation about the person&#39;s life and profession.  How did this help you prepare for your" +
								" career goals.  How will you adapt to the changes that will change your job over time?  What does the OCCU-Find say about the national employment" +
								" outlook for your career?  (to find this information, use the OCCU-Find search tool to locate your occupation, then under employment statistics," +
								" where the map is located, click more details.)</p>";
						$scope.link = 	"ASVAB_CEP_Learn_from_the_Old_Win_with_STEM.pdf"
						break;
					case "freshman":
						$scope.title = "Freshman Profile";				
						$scope.text =  "<p>Research schools providing training for your career of choice by selecting College Info under the HOW TO GET THERE section on each OCCU-Find career" +
								" detail page. For your schools of choice, prepare a collage depicting the profile of the ideal freshman: ACT/SAT score, class rank, GPA, etc. Include" +
								" admission requirements and deadlines.</p>";
						$scope.link = 	"ASVAB_CEP_Freshman_Profile.pdf"
						break;
					case "real":
						$scope.title = "What do real jobs look like?";				
						$scope.text =  "<p>After exploring the OCCU-Find, identify 5 careers that interest you.  Look for 2 job postings on <a target=\"_blank\" href=\"http://www.usajobs.gov\">www.usajobs.gov</a> for one of the careers you have chosen" +
									   " to explore.  After reading the job postings, complete the following:</p>"+
									   " <p>1.	Under the Duties is a Summary section that explains a little about the agency and where you would be working.  Of the two job ads, which agency or location would you prefer?  Why? (Hint:  Sometimes there are additional links to learn more about the agency under Agency contact information)</p>" +
									   " <p>2.	Draft 5 interview questions you would need to be prepared to answer before you applied to this job.</p>" +
									   " <p>3.	How would you be evaluated for these jobs?</p>" +
									   " <p>4.	What benefits are included for the jobs?</p>" +
									   " <p>5.	What are 5 questions you would need to ask the employer to help you make your decision to work with them?</p>" ;
						$scope.link = 	"ASVABCEP_What_Do_Real_Jobs_Look_Like.pdf"
						break;
					case "interest":
						$scope.title = "Interests and Needs";				
						$scope.text =  "<p>After taking the FYI, your interests were identified, and you used your interests to look for a career.  Now that you have identified some careers to" +
									   " explore, let&#39;s look at how your career choices will help others.  Abraham Maslow published a theory on human motivation.  His theory outlines certain" +
									   " needs that must be met before someone can fulfill their greatest potential.  After you have read about Maslow&#39;s theory, take a look at 5 of the jobs you" +
									   " have chosen to explore.  Write an essay that explains:</p>"+
									   " <p>o	What needs will you be fulfilling for others in each of those careers?" +
									   " <p>o	How will having those careers help you fulfill your needs?" +
									   " <p>o	How important are your interests when choosing a career?  Are they more, less, or the same importance as your aptitude?  Why? (there is no &#34;right&#34; or &#34;wrong&#34; answer to this question).";
						$scope.link = 	"ASVABCEP_Interests_Needs.pdf"
						break;
					case "giving":
						$scope.title = "Giving Back";				
						$scope.text =  "<p>Social science is the study of human society and social relationships.  After using the OCCU-Find to explore careers, choose two careers and create a volunteer" +
									   " project that would allow you to gain skills in helping you for both careers.  Tale a look at the &#34;What they do&#34; section and see where these careers overlap.  For" +
									   " example, if I was interested in becoming an Oral and Maxillofacial Surgeon and a Forest Fighter, I could create a volunteer project that involved communicating with" +
									   " others since both jobs require communication.  My project could focus on communicating information about fire prevention or the importance of taking care of your teeth" +
									   " and gums.  Your project should include an impact statement (how helpful or important this project is), a goal (what you hope to accomplish), a list of materials needed," +
									   " where this will be held, what audience you are focusing on, and a plan of action.  After the class presents their projects, take a vote and pick the one that the class is" +
									   " most excited about doing.  Keep us posted and tag @ASVABCEP so we can see how you are giving back to your community while gaining skills!</p>";
						$scope.link = 	"ASVABCEP_Giving_Back.pdf"
						break;
					case "video":
						$scope.title = "Elevator Pitch Video";				
						$scope.text =  "<p>In the business world, an &#34;elevator pitch&#34; is a quick, passionately delivered description of something you want to do and why. You never know when you&#39;ll be standing in" +
									   " line or sharing an elevator ride with someone who can help or even make your dream job happen!  To make the most of such an opportunity, you must have an elevator pitch ready to roll" +
									   " at a moment&#39;s notice.</p>"+
									   "</br>"+
									   "<p>Prepare your #whatsyourdreamjob elevator pitch!  And record it!</p>"+
									   "</br>"+
									   "<p>Your video <b>must</b> include: </p>"+
									   "<p>&#8226;	Your name</p>"+
									   "<p>&#8226;	What you want to do in your career</p>"+
									   "<p>&#8226;	Why you chose this career (Tell a little story, why are you passionate about this)</p>"+
									   "<p>&#8226;	What materials/resources you&#39;ll need (college money?  Tools? Access to agents?)</p>"+
									   "<p>&#8226;	What obstacles you anticipate and your plan to navigate those obstacles </p>"+
									   "<p>&#8226;	Why someone should help you reach these goals</p>"+
									   "<p>&#8226;	Graciously thank the person for listening to you</p>"+
									   "<p>Be organized with your thoughts and speak passionately about your career goals.  Don&#39;t forget to tag @ASVABCEP and #youdecide #whatsyourdreamjob so we can highlight" +
									   " your elevator pitch!</p>";
						$scope.link = 	"ASVABCEP_Elevator_Pitch_Video.pdf"
						break;
					case "cost":
						$scope.title = "Cost of a Career";
						$scope.text =  " <p>Choose a career using the OCCU-Find. Identify two paths to outline how you could reach this career goal. Make a list of costs associated" +
									   " with each path. Include cost of living, location, the amount of time that it will take to reach your goal, plus any other factors that will affect" +
									   " the cost. Create a comparison chart to show the total amount of cost associated with each path. *Tip: Use the living wage calculator to develop your" +
									   " chart: <a target=\"_blank\" href=\"http://livingwage.mit.edu\">http://livingwage.mit.edu/</a></p>";
						$scope.link =  "ASVAB_CEP_Cost_of_a_Career.pdf"
						break;
					case "math":
						$scope.title = "Math Skills Analysis";
						$scope.text =  "<p>Using the OCCU-Find, search for jobs that match your interest codes and math skills. Identify five careers that interest you.</p>" +
									   "<p>What type of math is important for each of these jobs? How is math used in these jobs? Make a chart like the one below:</p>"+
									   "<img src=\"images/ASVAB_CEP_Math_Skills_Analysis_table.jpg\" alt=\"Math Skills Analysis Table\" style=\"max-width: 100%\">";
						$scope.link =  "ASVAB_CEP_Math_Skills_Analysis.pdf"
						break;
					case "scientific":
						$scope.title = "Scientific Leaps and Bounds";
						$scope.text =  " <p>Choose a scientific advancement made in the last five years. Do some online research to answer these questions.</p>" +
						   			   " <p>1.	Why do you think this advancement is important?</p>" +
							           " <p>2.	How was this advancement made?</p>" +
								       " <p>3.	What specific jobs does this discovery impact?  How?</p>" +
								       " <p>4.	Make a video explaining why you think this advancement is important.</p>" +
								       " <p>5.	Would you rather be on the discovery side or the application side of this discovery? Why?</p>" ;
						$scope.link =  "ASVAB_CEP_Scientific_Leaps_and_Bounds.pdf"
						break;
					case "stem":
						$scope.title = "Which STEM Field is for Me?";
						$scope.text =  " <p>Choose two subfields in science you are most interested in learning about: Biology, Chemistry, Physics, Health, Psychology, etc. </p>" +
									   " <p>Using the OCCU-Find, identify three careers in each of the subfields that interest you. </p>" +
									   " <p>What classes/training could you complete in high school to help you prepare for these careers?  </p>" +
									   " <p>Make an infograph about one of these subfields to encourage other students to explore careers in this area </p>";
						$scope.link =  "ASVAB_CEP_Which_STEM_Field_is_for_Me.pdf"
						break;
					case "linkedin":
						$scope.title = "Create a LinkedIn Profile Using My Portfolio";
						$scope.text = "<p>Did you know LinkedIn is the most widely used social media platform for professionals?  It&#39;s for everyone to use &#8211; you don&#39;t have to have years of experience to set up a LinkedIn profile.</p>" + 
									  "<p>Set up a LinkedIn profile using your ASVAB CEP portfolio (see Step by Step Creation of the Online Portfolio for guidance to set this up). Take your Work Experience and Education directly from the portfolio and copy into your new LinkedIn Profile. Use your Skills and Achievements to build out your profile. You can share information both ways. For example, LinkedIn gives you suggested skills based on your experience and education. You can also add these to your My Portfolio and share it with your counselor or a college or military recruiter.</p>";
						$scope.link = "ASVAB_CEP_Create_a_Linkedin_Profile.pdf";
						break;
					case "action":
						$scope.title = "Create an Action Plan";
						$scope.text = "<p>Career satisfaction is not one size fits all. There are multiple paths you can take to achieve your career goals. Choose one (or a combination) of the following pathways:</p>" +
									  "<p><strong>College</strong>:  Using your list of favorite jobs, identify five colleges that you may want to apply to in the future.  Add these colleges to your favorites and add notes about why you are considering these colleges.  Reflect on tuition costs, required test scores and returning student numbers.  For these five institutions, write a plan of action to include all materials needed for the application process.  Discuss your plan of action and college/university choices with your parents, career counselor, or teacher. </p>" +
									  "<p><strong>Certification/Licenses/Apprenticeship</strong>:  Using your list of favorite jobs, identify the certifications/licenses/or apprenticeships you are interested in pursuing.  Who are the certifying organizations?  What do you have to complete in high school in order to qualify for these opportunities?  Make a plan of action to include all materials needed for the application process.  Discuss your plan of action and certification/license/or apprenticeship choices with your parents, career counselor, or teacher. </p>" +
									  "<p><strong>Military</strong>:  Using your list of favorite jobs, identify the military services offering these occupations.  (If you are interested in an Officer level career, note that college is required, you will want to chart your college path too.)  Using <a target='_blank' href='" + getCitMLink() + "'>www.careersinthemilitary.com</a>, identify your favorite Services that offer the job opportunities that interest you.  What requirements will you need to meet in order to attain this goal?  (ASVAB scores, physical readiness, etc.)  Make a plan of action to include materials needed for the application process.  Discuss your plan of action and military choices with your parents, career counselor, teacher, or a recruiter.</p>";
						$scope.link = "ASVAB_CEP_Create_an_Action_Plan.pdf";
						break;
					case "portfolio":
						$scope.title = "Populating My Portfolio";
						$scope.text = "<p>ASVAB CEP participants can use My Portfolio as a living resume to keep track of accomplishments, aspirations, and career exploration activities. Here, they can track their experience, skills and interests, as well as document future plans. Users can also save favorite careers, colleges, and career clusters of interest.  Follow this step-by-step guide.</p>" +
									  "<p><strong>Step One</strong>:  Capture Your Future Plan(s).</p>" + 
									  "<p>Select from the list of postsecondary intentions (e.g., college, work, military) that you are considering right now. It&#39;s natural for plans to change. You can update this at any time.</p>" + 
									  "<p><strong>Step Two</strong>:  Document Your Accomplishments.</p>" +
									  "<p>Use each section to outline your work experience, education, achievements, skills, and volunteer activities. You can use this information when it&#39;s time to write a formal resume, and you can add information as you accumulate more experiences.</p>" +
									  "<br />" + 
									  "<p>Not sure what to write? Check out the Tips provided under Work Experience and Volunteer Activities, and also the article, <a href='" + getCepLink() + "/media-center-article/64'>Writing a High School Resume that Stands Out</a>. The <a href='" + getCepLink() + "/media-center-article-list/1'>Media Center</a> is full of useful tips.</p>" +
									  "<p><strong>Step Three</strong>:  Track Your Interests, Values, and Favorites.</p>" +
									  "<p>My Portfolio is a perfect place to keep track of what you like and value. If you have taken the FYI, your work-related RIASEC interests will show up here. You can jot down details about why these interests appeal to you.</p>" +
									  "<p>You can also track your work values. What&#39;s important to you in a job?  Do you want to work in a field where you help others?  Do you value job security?  Benefits? Good income? Use this space to write down what is important to you in a job.</p>" +
									  "<br />" +
									  "<p>As you explore the OCCU-Find, you can save notes on careers and colleges and add the ones you like to your favorites.</p>";
						$scope.link = "ASVAB_CEP_Populating_My_Portfolio.pdf";
						break;
					case "portfolio_citm":
						$scope.title = "Populating the Portfolio at careersinthemilitary.com";
						$scope.text = "<p>ASVAB CEP participants who are exploring military career options can add additional military-specific items to their portfolio at <a target='_blank' href='" + getCitMLink() + "'>careersinthemilitary.com</a>.</p>" + 
									  "<p>When you add information to My Portfolio at <a href='" + getCepLink() + "'>asvabprogram.com</a>, your information is also saved at <a target='_blank' href='" + getCitMLink() + "'>careersinthemilitary.com</a>.</p>" +
									  "<p>The two sites work together. When you explore military-specific careers, you are taken to <a target='_blank' href='" + getCitMLink() + "'>careersinthemilitary.com</a>, where you are automatically logged in.</p>" + 
									  "<p>At <a target='_blank' href='" + getCitMLink() + "'>careersinthemilitary.com</a>, go to My Profile and you will see any information you saved in My Portfolio at <a href='" + getCepLink() + "'>asvabprogram.com</a>. You will notice additional military-specific content fields related to Composite Score Favorites and Physical Fitness. Populate these fields if you want to share this portfolio with a recruiter.</p>" + 
									  "<p>At <a target='_blank' href='" + getCitMLink() + "'>careersinthemilitary.com</a> you can make notes and add military careers to your favorites the same way you did at <a href='" + getCepLink() + "'>asvabprogram.com</a>. You will also see suggested careers tailored to your interests. Don&#39;t forget to review the guided exploration activity for more suitable careers.</p>";
						$scope.link = "ASVAB_CEP_Populating_the_Portfolio_citm.pdf";
						break;
					case "virtual":
						$scope.title = "Virtual Job Shadow";
						$scope.text = "<p>Job shadowing is a popular on-the-job learning experience. It involves observing someone working so you can understand firsthand what they actually do at work. Job shadowing can help you to get a better sense of the options available in the career field you&#39;re considering and the competencies required for careers that interest you.</p>" +
									  "<p>Select your favorite career cluster (it will be added to My Portfolio). Review details from at least two career clusters included below. We included links to details about one career from each career cluster that is available in both the civilian and military world. Consider the videos and descriptions. What is similar? What is different? What have you already done to prepare you for entry into this career field? What paths can you take to gain entry into this career field? What do you need to do next? You can add your favorite careers and institutions to your My Portfolio and make notes about what you are planning. Then, populate your My Portfolio with your personal details relevant to the entry requirements or competencies required for the career you are exploring.</p>" +
									  "<p>(If you&#39;re exploring careers not included on this list, more videos and descriptions are available at <a href='" + getCepLink() + "'>asvabprogram.com</a> and <a href='" + getCitMLink() + "'>careersinthemilitary.com</a>.) </p>";
						$scope.link = "ASVAB_CEP_Virtual_Job_Shadow.pdf";
						break;
					default:
						$scope.title = "Not Available";
						$scope.text =  "Not Available";
						$scope.link = 	"Not Available.pdf"
						break;
					}
				
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};
} ]);
cepApp.service('fyiManualInput', [ '$uibModal', 'FYIScoreService', '$location', 'FYIRestFactory', '$rootScope', '$q', function($uibModal, FYIScoreService, $location, FYIRestFactory, $rootScope, $q) {

	var modalDefaults = {
		animation : true,
		size : 'lg',
		templateUrl : 'cep/components/dashboard/page-partials/fyi-manual-input.html'
	};

	var modalOptions = {};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.interestCodeRawScore = {
					rRawScore : undefined,
					iRawScore : undefined,
					aRawScore : undefined,
					sRawScore : undefined,
					eRawScore : undefined,
					cRawScore : undefined
				};
				$scope.gender = '';

				$scope.modalOptions.submit = function(result) {

					// validate inputs
					if ($scope.interestCodeRawScore.rRawScore == undefined || $scope.interestCodeRawScore.iRawScore == undefined || $scope.interestCodeRawScore.aRawScore == undefined || $scope.interestCodeRawScore.sRawScore == undefined || $scope.interestCodeRawScore.eRawScore == undefined || $scope.interestCodeRawScore.cRawScore == undefined) {
						$scope.rawScoreValidate = "Enter a score for all interest codes with a range from 0 to 30";
					} else {
						$scope.rawScoreValidate = undefined;
					}

					if ($scope.gender == '') {
						$scope.genderValidate = "Select gender";
					} else {
						$scope.genderValidate = undefined;
					}

					if ($scope.genderValidate != undefined || $scope.rawScoreValidate != undefined) {
						return false;
					}

					var rawScore = $scope.interestCodeRawScore;

					// create profile
					var fyiObject = {
						userId : $rootScope.globals.currentUser.userId,
						gender : $scope.gender,
						rawScores : undefined
					};

					fyiObject.rawScores = [ {
						interestCd : 'R',
						rawScore : rawScore.rRawScore
					}, {
						interestCd : 'I',
						rawScore : rawScore.iRawScore
					}, {
						interestCd : 'A',
						rawScore : rawScore.aRawScore
					}, {
						interestCd : 'S',
						rawScore : rawScore.sRawScore
					}, {
						interestCd : 'E',
						rawScore : rawScore.eRawScore
					}, {
						interestCd : 'C',
						rawScore : rawScore.cRawScore
					} ];

					FYIScoreService.setFyiObject(fyiObject);
					
					if($location.path() == '/find-your-interest-score'){
						$location.path('/find-your-interest-score-reload');
					} else {
						$location.path('/find-your-interest-score');
					}
					
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.directive('dir', ['$compile', '$parse', function($compile, $parse) {
	return {
		restrict : 'E',
		link : function(scope, element, attr) {
			scope.$watch(attr.content, function() {
				element.html($parse(attr.content)(scope));
				$compile(element.contents())(scope);
			}, true);
		}
	}
}]).controller('FaqController', [ '$scope', 'FaqRestFactory', 'modalService', 'ScoreRequestModalService', 'vcRecaptchaService', 'recaptchaKey', '$location', '$window', 'BringToSchoolModalService', function($scope, FaqRestFactory, modalService, ScoreRequestModalService, vcRecaptchaService, recaptchaKey, $location, $window, BringToSchoolModalService) {

	$scope.about = [ {
		title : 'What is the ASVAB? ',
		description : 'The ASVAB is the Armed Services Vocational Aptitude Battery. It is an aptitude test that measures developed abilities and helps predict future academic and occupational success. '
	}, {
		title : 'What does ASVAB CEP stand for? ',
		description : 'Armed Services Vocational Aptitude Battery (ASVAB) Career Exploration Program (CEP). Pronounced "as-vab c.e.p."'
	}, {
		title : 'What is the purpose of the ASVAB CEP? ',
		description : 'The Department of Defense sponsors the ASVAB CEP with a two-part mission, to provide: a career exploration service to U.S. youth and qualified leads to military recruiters. The ASVAB CEP offers students a chance to explore all paths to careers - college, certifications, apprenticeships, licensure programs, and the Military - in one place.'
	}, {
		title : 'When did the ASVAB CEP begin? ',
		description : 'The ASVAB has been used in the Student Testing Program (STP) since 1968. The Career Exploration Program (CEP) component was added in 1992.'
	}, {
		title : 'What is the difference between ASVAB CEP and the enlistment ASVAB? ',
		description : 'The ASVAB CEP is a career planning and exploration program offered free to participating schools and students. ' +
		'The enlistment version of the ASVAB is primarily given at a Military Entrance Processing Station (MEPS) and is used for recruiting purposes only. '
	}, {
		title : 'If I take the ASVAB, will recruiters contact me? ',
		description : "Military and college recruiters obtain student directory information from multiple sources to identify high-quality talent. Students may be contacted by college and military recruiters regardless of ASVAB CEP participation or the score release option your school chooses.<br><br>" +
		"Schools determine if ASVAB results are released to the Military. However, you can <a ng-click='optInLink()'>opt in</a> to or <a ng-click='optOutLink()'>opt out</a> of the release option your school chooses. Be sure to coordinate with your school counselor. Regardless, students participating in the ASVAB Career Exploration Program have no obligation to the Military or to talk to a military recruiter."
	}, {
		title : 'Is the ASVAB CEP available outside of the United States? ',
		description : 'The ASVAB CEP is administered to high school and post-secondary students across the United States, Puerto Rico, Guam and the Marianas, and Department of Defense Education Activities (DODEA) overseas. The program is not available in other areas.'
	}, {
		title : 'What is the ASVAB CEP connection to the Military? ',
		description : 'ASVAB CEP test sessions are administered by DoD civilians but are supported by a military personnel proctors (one proctor for every 40 students). Proctors are required to ensure test security and validity. Also, 11th and 12th grade student ASVAB CEP scores are valid for enlistment for two years after test date. Students interested in military service can explore their career options at <a ng-click="CITMLink()">careersinthemilitary.com</a>.'
	} ];
	
	$scope.testing = [ {
		title : 'When is my high school testing? ',
		description : 'Contact your school counselor to find out when the ASVAB CEP is coming to your school. If it\'s not already scheduled, tell your counselor you\'re interested. <a ng-click="asvabModal()"> BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
	}, {
		title : 'Can reasonable accommodations be requested for the ASVAB CEP? ',
		description : 'Accommodations are provided for in the ASVAB CEP, including reading the test aloud, extra time, and enlarged print tests. However, all testing completed using an accommodation is coded Option 7 (Invalid for enlistment purposes). Official ASVAB testing sites (Military Entrance Processing Stations and Military Entrance Test sites) do not provide accommodations during testing.'
	}, {
		title : 'Can students who are homeschooled take the ASVAB CEP? ',
		description : 'The ASVAB CEP is offered in schools. If affiliated with a Homeschool Association or Network that can provide a testing facility, we can provide the ASVAB CEP to the group. If not affiliated with an Association or Network, you can contact a local Education Services Specialist at 1-800-323-0513 to find out if area schools provide the program. If they do, you can make arrangements with the school counselor to participate. There is no individual administration of the program.'
	}, {
		title : 'How do I request the ASVAB CEP for my school? ',
		description : 'To contact the nearest office, please dial 1-800-323-0513. Your call will automatically be connected to the closest ASVAB CEP office. Calls are routed based on the area code and number from which you are dialing. Caution: If you are calling from a cell phone number that you obtained in another state, you will be connected to an Education Services Specialist in that area. As an alternative, <a ng-click="asvabModal()">BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
	}, {
		title : 'How do I sign up for the ASVAB CEP? ',
		description : 'High school and post-secondary students across the United States can participate in the ASVAB CEP. Testing schedules are based on school requests rather than individual requests. Your school counselor or our local representative will be able to provide information on school testing in your area. To contact the nearest office, please dial 1-800-323-0513. You will be connected to the closest ASVAB CEP office. Calls are routed based on the area code and number from which you are dialing. (Caution: If you are calling from a cell phone number that you obtained in another state, you will be connected to an Education Services Specialist in that area.) As an alternative, <a ng-click="asvabModal()"> BRING THE ASVAB CEP TO YOUR SCHOOL</a>.'
	} ];

	$scope.score = [ {
		title : 'I took the test at my high school, when do I get my scores? ',
		description : "High schools tests are scored within two weeks of testing and returned to the school counselor with career exploration materials. You will receive your scores from your school counselor or an ASVAB Career Exploration Program Education Services Specialist. If you haven't received your scores, first check with your counselor for assistance. If your school counselor has not received the scores after 30 days, <a ng-click='openScoreRequestModal()'>click here to request your scores.</a>"
	}, {
		title : 'I took the test in high school three years ago, can you send me my scores? ',
		description : "We maintain ASVAB test scores for two years after the date of testing. If you took the test within the last two years, we can retrieve your scores using your name, school name, school city/state, and estimated test date (month/year). <a ng-click='openScoreRequestModal()'>Click here to request your scores.</a>\n" +
		"<br><br>If you tested over two years ago, we can no longer retrieve your scores."
	}, {
		title : 'Can I view my scores online?',
		description : 'Yes. Create an account using the access code found on your ASVAB Summary Results (ASR) sheet.<br>'+
		'<br><ul><li>Your career exploration scores are available at <a ng-click="asvabprogramLink()">asvabprogram.com</a>.</i>'+
		'<li>Your ASVAB CEP scores are converted to military line scores, <a ng-click="CITMCompositeScoreLink()">here</a>.</i></ul>' +
		'The username and password you create gives you access to both websites.'
	}, {
		title : 'The scores I received in school are different from the scores my recruiter has. Which scores are correct? ',
		description : 'Scores on the ASVAB CEP ASVAB Summary Results (ASR) sheet are provided for career exploration purposes only. The only score on the ASR that is the same for military recruiting use is the AFQT or Military Entrance Score. <br><br>' +
		'ASVAB CEP participants are encouraged to create an account using their access code to view their career exploration scores at <a ng-click="asvabprogramLink()">asvabprogram.com</a>. ASVAB CEP scores are converted to military line scores, <a ng-click="CITMCompositeScoreLink()">here</a>.<br><br>' +
		'You must create an account using the access code found on your ASR to view your scores online. The username and password you create gives you access to both websites.<br><br>' + 
		'Note: MEPCOM can only retrieve scores from tests taken in 11th and 12th grade and post-secondary schools. 11th and 12th grade and post-secondary students can use their ASVAB CEP AFQT scores to enlist for up to two years after testing. All 10th grade scores are invalid for enlistment purposes (regardless of age) and are not available through the USMIRS database.',
	} ];

	$scope.access = [ {
		title : "I lost my access code. Can you send it to me?",
		description : "Yes, ASVAB CEP participants can request your access code, <a ng-click='openScoreRequestModal()'>here</a>."
	},{
		title : "Can I request an access code?",
		description : "Parties interested in the career exploration activities ASVAB CEP offers can request a temporary access code, <a ng-click='openScoreRequestModal()'>here</a>."
	}];

	$scope.military = [ {
		title : "Why was there a recruiter at my testing session? ",
		description : 'ASVAB CEP test sessions are administered by Department of Defense civilians but are supported by a military personnel proctors (one proctor for every 40 students). Proctors are required to ensure test security and validity. Most schools elect to use military proctors rather than tying up school staff during test sessions.'
	}, {
		title : "Where can I find military career information? ",
		description : '<a ng-click="CITMLink()">Careersinthemilitary.com</a> is a comprehensive online resource powered by ASVAB CEP that allows students to discover extensive details about military career opportunities across all Services, their Service-specific ASVAB line scores and which Services offer which jobs.'
	}, {
		title : "I'm interested in the Military, where can I take an ASVAB test? ",
		description : 'High school and post-secondary students may be able to test at their local school. Check with your school counselor to find out if the ASVAB CEP is offered. Talk to a recruiter to find out if your scores qualify you for service.<br><br>'+
		'If you\'re not in school, contact a military recruiter to request testing. The test is administered at 65 Military Entrance Processing Stations (MEPS) and over 300 Military Entrance Test (MET) Sites nationwide. You must be sponsored by a recruiter to test at these facilities. <br><br>'+
		'Find the nearest MEPS, <a ng-click=mepcomLink()>here</a>. After selecting the MEPS, select the link to "Military Entrance Test (MET) Sites" under the dark blue bar to find the nearest MET sites for testing.'
	}, {
		title : "Which occupations do I qualify for in the Military? ",
		description : 'The Military Entrance Score, or AFQT, is what the Military will use to determine enlistment eligibility. Each Service sets its own AFQT score requirement. Then, each military job has its own unique composite score requirement.*<br><br>'+
		'Your ASVAB CEP scores are converted to military lines scores <a ng-click="CITMCompositeScoreLink()">here</a>. For the most up-to-date requirements or to find out if you qualify for military service, <a ng-click="contactRecruiter()">contact a recruiter</a>. <br><br>'+
		'If you are in 11<sup>th</sup> grade or beyond and want to enlist using these scores, log in to <a ng-click="asvabprogramLink()">asvabprogram.com</a> or <a ng-click="CITMLink()">careersinthemilitary.com</a> to show recruiters your scores. Recruiters will then submit a formal request to pull your scores using FORM 680-3A. <br><br>'+
		'* These score requirements are subject to change without notice and vary depending on the recruiting environment and the need for new military applicants.'
	}, {
		title : "I want to join the Air Force/Army/Marine Corps/National Guard/Navy. Can you help me? ",
		description : 'The ASVAB CEP helps students search the world of work for occupations that they may find satisfying. Some of these careers are found in the Military. You can use your ASVAB Career Exploration Scores to find military careers for which you would be best suited at <a ng-click="CITMLink()">careersinthemilitary.com</a>. However, ASVAB CEP personnel are not involved in recruiting activities. For information on how to join the Military, click <a ng-click="CITMOptionsLink()">here</a>. Then, <a ng-click="contactRecruiter()">contact a military recruiter</a> in your area.'
	}, {
		title : "I want to join the Military, how do I improve my score? ",
		description : "We recommend the Army's <a ng-click='march2successLink()'>March2Success</a> program for ASVAB test score improvement. This resource provides general test preparation materials for ACT, SAT, and high school completion. As the AFQT score is a combination of mathematics knowledge, arithmetic reasoning, paragraph comprehension, and vocabulary skills, studying the related material at this site in any of those areas may help you improve your qualifying score."
	}, {
		title : "How do I contact a recruiter? ",
		description : 'Contact a military recruiter in your area <a ng-click="contactRecruiter()">here</a>.'
	}, {
		title : "Where can I find more information about the ASVAB? ",
		description : 'Be sure to review the sample test items and test taking strategies <a ng-click="asvbprogramStudentLink()">here</a>.<br>'+
		'Then, you can find more information and more sample test questions at <a ng-click="officialasvabLink()">officialasvab.com</a>. For even more practice questions, visit <a ng-click="todaymilitaryLink()">todaysmilitary.com</a>.'
	} ];

	$scope.userType;
	$scope.firstName;
	$scope.lastName;
	$scope.email;
	$scope.topics;
	$scope.message;
	$scope.formDisabled = false;
	$scope.recaptchaResponse = '';
	$scope.recaptchaKey = recaptchaKey;

	$scope.emailUs = function() {
		$scope.validation = undefined;
		$scope.formDisabled = true;
		var emailObject = {
			userType : $scope.userType,
			firstName : $scope.firstName,
			lastName : $scope.lastName,
			email : $scope.email,
			topics : $scope.topics,
			message : $scope.message,
			recaptchaResponse : $scope.recaptchaResponse
		}

		if ($scope.userType == undefined ||$scope.firstName == undefined || $scope.lastName == undefined || $scope.email == undefined || $scope.topics == undefined || $scope.message == undefined) {
			$scope.validation = "Fill out all fields.";
			$scope.formDisabled = false;
			return false;
		}

		if ($scope.recaptchaResponse === "") { // if string
			// is empty
			$scope.validation = "Please resolve the captcha and submit!";
			$scope.formDisabled = false;
			return false;
		} else {

			FaqRestFactory.emailUs(emailObject).then(function(success) {
				
				// Track event in Google Analytics.
				var path = $location.path();
				$window.ga('create', 'UA-83809749-1', 'auto');
				$window.ga('send', 'pageview', path + '#'+ emailObject.userType + '#' + emailObject.topics + '#contact-form-submitted' );

				$scope.userType = undefined;
				$scope.firstName = undefined;
				$scope.lastName = undefined;
				$scope.email = undefined;
				$scope.topics = undefined;
				$scope.message = undefined;
				$scope.recaptchaResponse = '';
				vcRecaptchaService.reload();

				var modalOptions = {
					headerText : 'Contact Us',
					bodyText : 'Thank you for contacting us!'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);

				$scope.formDisabled = false;
			}, function(error) {

				var error = error.data.errorMessage;
				if (error == undefined) {
					var modalOptions = {
						headerText : 'Contact Us',
						bodyText : 'There was an error when trying to contact us. Please try again.'
					};

					modalService.showModal({
						size : 'sm'
					}, modalOptions);
					vcRecaptchaService.reload();
				} else {
					var modalOptions = {
						headerText : 'Contact Us',
						bodyText : error
					};

					modalService.showModal({
						size : 'sm'
					}, modalOptions);
				}

				$scope.formDisabled = false;
			});
		}

	}

	$scope.openScoreRequestModal = function() {
		ScoreRequestModalService.showModal({
			size : 'lg'
		}, {});
	}

	$scope.optInLink = function(){
		var url = "https://www.asvabprogram.com/pdf/ASVAB_CEP_Opt_In_Letter.docx"
			window.open(url,'_blank')
	}
	
	$scope.optOutLink = function(){
		var url = "https://www.asvabprogram.com/pdf/ASVAB_CEP_Opt_Out_Letter.docx"
			window.open(url,'_blank')
	}
	
	$scope.CITMLink = function(){
		var url = "https://www.careersinthemilitary.com"
			window.open(url,'_blank')
	}

	$scope.asvabprogramLink = function(){
		var url = "https://www.asvabprogram.com"
			window.open(url,'_blank')
	}
	$scope.asvbprogramStudentLink = function(){
		var url = "https://www.asvabprogram.com/student-program"
			window.open(url,'_blank')
	}
	$scope.CITMCompositeScoreLink = function(){
		var url = "https://www.careersinthemilitary.com/composite-scores"
			window.open(url,'_blank')
	}
	$scope.CITMOptionsLink = function(){
		var url = "https://www.careersinthemilitary.com/options"
			window.open(url,'_blank')
	}

	$scope.contactRecruiter = function(){
		var url = "https://www.careersinthemilitary.com/contact-us"
			window.open(url,'_blank')
	}
	
	$scope.officialasvabLink = function(){
		var url = "http://www.officialasvab.com/faq_app.htm"
			window.open(url,'_blank')
	}

	$scope.mepcomLink = function(){
		var url = "http://www.mepcom.army.mil/Units/"
			window.open(url,'_blank')
	}
	
	$scope.march2successLink = function(){
		var url = "http://www.march2success.com"
			window.open(url,'_blank')
	}
	$scope.todaymilitaryLink = function(){
		var url = "https://todaysmilitary.com/joining/asvab-test-sample-questions"
			window.open(url,'_blank')
	}
	
	$scope.asvabModal = function() {
		BringToSchoolModalService.show({}, {});
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

cepApp.factory('FaqRestFactory', [ '$http', 'appUrl', function($http, appUrl) {

	var faqRestFactory = {};

	faqRestFactory.emailUs = function(emailObject) {
		return $http.post(appUrl + 'contactUs/email', emailObject);
	}

	faqRestFactory.scheduleEmail = function(emailObject) {
		return $http.post(appUrl + 'contactUs/scheduleEmail', emailObject);
	}

	faqRestFactory.shareWithCounselor = function(email, recaptcha) {
		return $http.post(appUrl + 'contactUs/shareWithCounselor/' + email + '/' + recaptcha + '/');
	}

	faqRestFactory.scoreRequest = function(emailObject) {
		return $http.post(appUrl + 'contactUs/scoreRequest', emailObject);
	}

	return faqRestFactory;

} ]);
cepApp.service('ScoreRequestModalService', [ '$uibModal', '$sce', 'modalService', '$templateCache', 'FaqRestFactory', 'vcRecaptchaService', 'recaptchaKey', '$location', '$window', function($uibModal, $sce, modalService, $templateCache, FaqRestFactory, vcRecaptchaService, recaptchaKey, $location, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/faq/page-partials/score-request-modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : '',
		windowClass : 'asvab-modal'
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

				// date picker template override
				$templateCache.put("uib/template/datepickerPopup/popup.html", "<div>\n" + "  <ul class=\"uib-datepicker-popup dropdown-menu uib-position-measure\" dropdown-nested ng-if=\"isOpen\" ng-keydown=\"keydown($event)\" ng-click=\"$event.stopPropagation()\">\n" + "    <li ng-transclude></li>\n" + "    <li ng-show=\"$parent.$parent.showErrorOne\" class=\"uib-button-bar\" style=\"background-color: #f9e3e3; padding: 10px; font-size: smaller; width: 220px;\">Please see your school counselor for more details.</li>\n" + "    <li ng-show=\"$parent.$parent.showErrorTwo\" class=\"uib-button-bar\" style=\"background-color: #f9e3e3; padding: 10px; font-size: smaller; width: 220px;\">Test scores more than two years old are removed from the ASVAB CEP system.</li>\n" + "    <li ng-if=\"showButtonBar\" class=\"uib-button-bar\">\n" + "      <span class=\"btn-group pull-left\">\n test" + "        <button type=\"button\" class=\"btn btn-sm btn-info uib-datepicker-current\" ng-click=\"select('today', $event)\" ng-disabled=\"isDisabled('today')\">{{ getText('current') }}</button>\n" + "        <button type=\"button\" class=\"btn btn-sm btn-danger uib-clear\" ng-click=\"select(null, $event)\">{{ getText('clear') }}</button>\n" + "      </span>\n" + "      <button type=\"button\" class=\"btn btn-sm btn-success pull-right uib-close\" ng-click=\"close($event)\">{{ getText('close') }}</button>\n" + "    </li>\n" + "  </ul>\n" + "</div>\n" + "");

				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.notAllowedPopup = function() {
					$uibModalInstance.dismiss('cancel');
					var modalOptions = {
						headerText : 'Score Request',
						bodyText : 'MEPCOM cannot release test scores to parents of children ages 18 and over.'
					};

					modalService.showModal({
						size : 'sm'
					}, modalOptions);
				}

				$scope.formDisabled = false;
				$scope.userType;
				$scope.firstName;
				$scope.lastName;
				$scope.email;
				$scope.schoolName;
				$scope.city;
				$scope.state;
				$scope.message;
				$scope.recaptchaResponse = '';
				$scope.recaptchaKey = recaptchaKey;
				$scope.widgetId;
				$scope.onWidgetCreate = function(_widgetId) {
					$scope.widgetId = _widgetId;
				};

				$scope.submit = function() {
					$scope.formDisabled = true;
					$scope.working = true;
					$scope.validation = undefined;
					$scope.showDateError = false;

					if (!($scope.dt instanceof Date)) {
						$scope.validation = "Invalid Date.";
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					}
					
					var emailObject = {
						userType : $scope.userType,
						firstName : $scope.firstName,
						lastName : $scope.lastName,
						email : $scope.email,
						schoolName : $scope.schoolName,
						city : $scope.city,
						state : $scope.state,
						message : $scope.message,
						dt : $scope.dt.getMonth() + '/' + $scope.dt.getDate() + '/' + $scope.dt.getFullYear(),
						recaptchaResponse : $scope.recaptchaResponse
					}

					console.log(emailObject);

					var threeMonthTimeStamp = new Date();
					threeMonthTimeStamp.setMonth(threeMonthTimeStamp.getMonth() - 3);

					var twoYearTimeStamp = new Date();
					twoYearTimeStamp.setMonth(twoYearTimeStamp.getMonth() - 24);

					var selectedTimeStamp = $scope.dt;

					if ($scope.userType == undefined || $scope.firstName == undefined || $scope.lastName == undefined || $scope.email == undefined || $scope.schoolName == undefined || $scope.city == undefined || $scope.state == undefined || $scope.dt == undefined) {
						$scope.validation = "Fill out all fields.";
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					} else if (twoYearTimeStamp > selectedTimeStamp || threeMonthTimeStamp < selectedTimeStamp) {
						$scope.validation = "Invalid Date.";
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					}

					if ($scope.recaptchaResponse === "") { // if string
						// is empty
						$scope.validation = "Please resolve the captcha and submit!";
						$scope.formDisabled = false;
						$scope.working = false;
						return false;
					} else {

						FaqRestFactory.scoreRequest(emailObject).then(function(success) {
							
							// Track event in Google Analytics.
							var path = $location.path();
							$window.ga('create', 'UA-83809749-1', 'auto');
							$window.ga('send', 'pageview', path + '#' + emailObject.userType + '#score-request' + '#contact-form-submitted' );

							$scope.userType = undefined;
							$scope.firstName = undefined;
							$scope.lastName = undefined;
							$scope.email = undefined;
							$scope.schoolName = undefined;
							$scope.city = undefined;
							$scope.state = undefined;
							$scope.message = undefined;
							$scope.recaptchaResponse = '';
							vcRecaptchaService.reload();

							$scope.stepTwo = false;
							$scope.stepThree = true;
							$scope.formDisabled = false;
							$scope.working = false;
						}, function(error) {

							var error = error.data.errorMessage;
							if (error == undefined) {
								$scope.validation = "There was an error proccessing your request. Please try submitting again.";
								vcRecaptchaService.reload($scope.widgetId);
							} else {
								$scope.validation = error;
							}
							$scope.formDisabled = false;
							$scope.working = false;
						});
					}
				}

				/**
				 * Date picker popup
				 */
				$scope.$watch('dt', function() {

					var threeMonthTimeStamp = new Date();
					threeMonthTimeStamp.setMonth(threeMonthTimeStamp.getMonth() - 3);

					var twoYearTimeStamp = new Date();
					twoYearTimeStamp.setMonth(twoYearTimeStamp.getMonth() - 24);

					var selectedTimeStamp = $scope.dt;

					if (twoYearTimeStamp > selectedTimeStamp) {
						$scope.showErrorTwo = true;
						$scope.showErrorOne = false;
					} else if (threeMonthTimeStamp < selectedTimeStamp) {
						$scope.showErrorOne = true;
						$scope.showErrorTwo = false;
					} else {
						$scope.showErrorOne = false;
						$scope.showErrorTwo = false;
					}
				});

				$scope.today = function() {
					$scope.dt = undefined;
				};

				$scope.today();

				$scope.clear = function() {
					$scope.dt = null;
				};

				$scope.inlineOptions = {
					customClass : getDayClass,
					minDate : new Date(),
				};

				$scope.dateOptions = {
					// dateDisabled: disabled,
					formatYear : 'yy',
					maxDate : new Date(2020, 5, 22),
					minDate : new Date(),
					showWeeks : false,
				// startingDay: 1
				};

				// Disable weekend selection
				function disabled(data) {
					var date = data.date, mode = data.mode;
					return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
				}

				$scope.toggleMin = function() {
					$scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
					$scope.dateOptions.minDate = $scope.inlineOptions.minDate;
				};

				$scope.toggleMin();

				$scope.open1 = function() {
					$scope.popup1.opened = true;
				};

				$scope.open2 = function() {
					$scope.popup2.opened = true;
				};

				$scope.setDate = function(year, month, day) {
					$scope.dt = new Date(year, month, day);
				};

				$scope.formats = [ 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate' ];
				$scope.format = $scope.formats[0];
				$scope.altInputFormats = [ 'M!/d!/yyyy' ];

				$scope.popup1 = {
					opened : false
				};

				$scope.popup2 = {
					opened : false
				};

				var tomorrow = new Date();
				tomorrow.setDate(tomorrow.getDate() + 1);
				var afterTomorrow = new Date();
				afterTomorrow.setDate(tomorrow.getDate() + 1);
				$scope.events = [ {
					date : tomorrow,
					status : 'full'
				}, {
					date : afterTomorrow,
					status : 'partially'
				} ];

				function getDayClass(data) {
					var date = data.date, mode = data.mode;
					if (mode === 'day') {
						var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

						for (var i = 0; i < $scope.events.length; i++) {
							var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

							if (dayToCheck === currentDay) {
								return $scope.events[i].status;
							}
						}
					}

					return '';
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.controller('FavoritesController', [ '$scope', 'UserFactory', 'modalService', 'favoriteList', 'NotesModalService', '$location', 'careerClusterFavoriteList', 'schoolFavoriteList', 'scoreSummary', function($scope, UserFactory, modalService, favoriteList, NotesModalService, $location, careerClusterFavoriteList, schoolFavoriteList, scoreSummary) {

	$scope.favoriteList = favoriteList;
	$scope.careerClusterFavoriteList = careerClusterFavoriteList;
	$scope.schoolFavoriteList = schoolFavoriteList;
	console.log($scope.schoolFavoriteList);

	var favoritelength = $scope.favoriteList.length

	// reset occupations selected
	for (var i = 0; i < favoritelength; i++) {
		$scope.favoriteList[i].selected = false;
	}

	$scope.scoreSummary = scoreSummary[0].data; 

	$scope.haveAsvabTestScores = (angular.isObject($scope.scoreSummary) && $scope.scoreSummary.hasOwnProperty('verbalAbility'));
	
	// remove favorites using user factory service
	$scope.removeFavorites = function(id) {
		$scope.myStyle = {'cursor': 'wait'};

		var promise = UserFactory.deleteFavorite(id);
		promise.then(function(response) {
			$scope.favoriteList = response;
			$scope.myStyle = {'cursor': 'pointer'};
		}, function(reason) {
			$scope.myStyle = {'cursor': 'pointer'};

			for (var i = 0; i < $scope.favoriteList.length; i++) {
				if ($scope.favoriteList[i].id = id) {
					$scope.favoriteList[i].disabled = false;
				}
			}

			// inform user that there was an error and to try again.
			console.log(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
		})
	}

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 3;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	$scope.compareSelection = function() {
		var occupationSelected = [];

		for (var i = 0; i < favoritelength; i++) {

			if ($scope.favoriteList[i].selected == true) {
				occupationSelected.push($scope.favoriteList[i].onetSocCd);
			}
		}

		if (occupationSelected.length < 2) {
			var modalOptions = {
				headerText : 'Favorites',
				bodyText : 'Select at least 2 occupations.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
			return false;
		}
		
		//$location.path('/compare-occupation');
		$location.url('/compare-occupation/' + occupationSelected[0] + '/' + occupationSelected[1] + '/' + occupationSelected[2] );

	}
	
	// remove career cluster favorites using user factory service
	$scope.removeCareerClusterFavorites = function(id) {
		$scope.myStyle = {'cursor': 'wait'};

		var promise = UserFactory.deleteCareerClusterFavorite(id);
		promise.then(function(response) {
			$scope.careerClusterFavoriteList = response;
			$scope.myStyle = {'cursor': 'pointer'};
		}, function(reason) {
			$scope.myStyle = {'cursor': 'pointer'};

			for (var i = 0; i < $scope.careerClusterFavoriteList.length; i++) {
				if ($scope.careerClusterFavoriteList[i].id = id) {
					$scope.careerClusterFavoriteList[i].disabled = false;
				}
			}

			// inform user that there was an error and to try again.
			console.log(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
		})
	}
	
	// remove school favorites using user factory service
		$scope.removeSchoolFavorites = function(id) {
			$scope.myStyle = {'cursor': 'wait'};

			var promise = UserFactory.deleteSchoolFavorite(id);
			promise.then(function(response) {
				$scope.schoolFavoriteList = response;
				$scope.myStyle = {'cursor': 'pointer'};
			}, function(reason) {
				$scope.myStyle = {'cursor': 'pointer'};

				for (var i = 0; i < $scope.schoolFavoriteList.length; i++) {
					if ($scope.schoolFavoriteList[i].id = id) {
						$scope.schoolFavoriteList[i].disabled = false;
					}
				}

				// inform user that there was an error and to try again.
				console.log(reason);
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'There was an error proccessing your request. Please try again.'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
			})
		}

	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);

cepApp.factory('FavoriteRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var favoritesRestFactory = {};

	favoritesRestFactory.getFavoriteOccupation = function(userId) {
		return $http.get(appUrl + 'favorite/getFavoriteOccupation/' + userId + '/');
	}

	favoritesRestFactory.insertFavoriteOccupation = function(favoriteObject) {
		return $http.post(appUrl + 'favorite/insertFavoriteOccupation', favoriteObject);
	}

	favoritesRestFactory.deleteFavoriteOccupation = function(id) {

		return $http({
			method : 'DELETE',
			url : appUrl + 'favorite/deleteFavoriteOccupation/' + id + '/'
		})
	}
	
	favoritesRestFactory.getFavoriteCareerCluster = function(userId) {
		return $http.get(appUrl + 'favorite/getFavoriteCareerCluster/' + userId + '/');
	}

	favoritesRestFactory.insertFavoriteCareerCluster = function(favoriteObject) {
		return $http.post(appUrl + 'favorite/insertFavoriteCareerCluster', favoriteObject);
	}

	favoritesRestFactory.deleteFavoriteCareerCluster = function(id) {

		return $http({
			method : 'DELETE',
			url : appUrl + 'favorite/deleteFavoriteCareerCluster/' + id + '/'
		})
	}
	
	favoritesRestFactory.getFavoriteSchool = function(userId) {
		return $http.get(appUrl + 'favorite/getFavoriteSchool/' + userId + '/');
	}

	favoritesRestFactory.insertFavoriteSchool = function(favoriteObject) {
		return $http.post(appUrl + 'favorite/insertFavoriteSchool', favoriteObject);
	}

	favoritesRestFactory.deleteFavoriteSchool = function(id) {

		return $http({
			method : 'DELETE',
			url : appUrl + 'favorite/deleteFavoriteSchool/' + id + '/'
		})
	}

	return favoritesRestFactory;

} ]);
cepApp.controller('FindYourInterestController', [ '$scope', 'mediaCenterList',function($scope, mediaCenterList) {

	$scope.mediaCenterList = mediaCenterList;
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);

cepApp.controller('FindYourInterestScoreController', [ '$route', 'resource', '$rootScope', '$scope', '$http', '$location', 'FYIScoreService', 'CombinedAndGenderScores', 'FYIRestFactory', '$window', 'modalService', 'scoreChoice', 'UserFactory', function($route, resource, $rootScope, $scope, $http, $location, FYIScoreService, CombinedAndGenderScores, FYIRestFactory, $window, modalService, scoreChoice, UserFactory) {
	
	$scope.scores = CombinedAndGenderScores[0].data;
	var topCombinedInterestCodes = FYIScoreService.getTopCombinedInterestCodes();
	var topGenderInterestCodes = FYIScoreService.getTopGenderInterestCodes();
	$scope.numberTestTaken = CombinedAndGenderScores[1].data;
	$scope.scoreChoice = scoreChoice.data.scoreChoice;
	$window.sessionStorage.setItem('numberTestTaken', $scope.numberTestTaken);
	$scope.limit;
	$scope.baseUrl = resource;
	$scope.domainUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port();

	// Determines if users see a link to re-take FYI.
	$scope.isShowTest = function() {
		$scope.userRole = $rootScope.globals.currentUser.role;

		// student role can only
		if ($scope.userRole != 'A') {
			$scope.limit = 2;
			if ($scope.numberTestTaken < $scope.limit) {
				return true;
			}
			$scope.isLimitReached = true;
			return false;
		}
		return true;
	}

	// If score choice is undefined then default to combined.
	$scope.scoreSelected = $scope.scoreChoice == undefined ? 'combined' : $scope.scoreChoice;

	// If score selection changes then update FYI scores asynchronously.
	$scope.$watch('scoreSelected', function(newValue, oldValue) {
		if ($scope.scoreSelected != undefined) {
			// Setup score object.
			var scoreObject = {
				userId : $rootScope.globals.currentUser.userId,
				scoreChoice : $scope.scoreSelected,
				interestCodeOne : $scope.scoreSelected == 'gender' ? $scope.genderScoreSort[0].interestCd : $scope.combinedScoreSort[0].interestCd,
				interestCodeTwo : $scope.scoreSelected == 'gender' ? $scope.genderScoreSort[1].interestCd : $scope.combinedScoreSort[1].interestCd,
				interestCodeThree : $scope.scoreSelected == 'gender' ? $scope.genderScoreSort[2].interestCd : $scope.combinedScoreSort[2].interestCd
			}

			// Rest call to post users FYI scores.
			FYIRestFactory.postUserScores(angular.toJson(scoreObject)).then(function successCallback(response) {

				// Remove session storage for interest codes.
				$window.sessionStorage.removeItem("interestCodeOne");
				$window.sessionStorage.removeItem("interestCodeTwo");
				$window.sessionStorage.removeItem("interestCodeThree");
				
				// Reset values
				FYIScoreService.setCombinedTiedScoreObject(undefined);
				FYIScoreService.setGenderTiedScoreObject(undefined);
				$window.sessionStorage.removeItem("combinedTiedScoreObject");
				$window.sessionStorage.removeItem("genderTiedScoreObject");

				// Enables going back to the tied break page if user returns to
				// FYI page.
				/*FYIScoreService.setTopCombinedInterestCodes(undefined);
				FYIScoreService.setTopGenderInterestCodes(undefined);*/
				
				// Determines which section to highlight using CSS.
				$scope.myClassCombined = $scope.scoreSelected == 'combined' ? {
					selected : true
				} : {
					selected : false
				};
				$scope.myClassGender = $scope.scoreSelected == 'gender' ? {
					selected : true
				} : {
					selected : false
				};

				
				UserFactory.getUserInterestCodes().then(function(response) {
					// Update left navigation interest codes.
					$rootScope.$emit("updateInterestCodes", {});
				}, function(error){
				});
				
				
				

			}, function errorCallback(response) {
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'Failed to save test scores. Try submitting again. If Error continues, please inform site administrator.'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
			});
		}
	});
	
	
	
	// Determines which section to highlight using CSS.
	$scope.myClassCombined = $scope.scoreSelected == 'combined' ? {
		selected : true
	} : {
		selected : false
	};
	$scope.myClassGender = $scope.scoreSelected == 'gender' ? {
		selected : true
	} : {
		selected : false
	};


	// sort gender score list in desc order
	var genderScoreSort = [];
	for (var i = 0; i < $scope.scores.length; i++) {
		genderScoreSort.push($scope.scores[i]);
	}
	genderScoreSort.sort(function(a, b) {
		return b.genderScore - a.genderScore
	});

	// When topCombinedInterestCodes is undefined, this means there are no tied
	// scores for combined.
	if (topGenderInterestCodes == undefined) {
		$scope.genderScoreSort = genderScoreSort;
	} else {

		// There are tied combined scores so need to order scores depending on
		// user's selections.
		var tempArray = [];

		// Use the user's tied score selection and move it to the top 3.
		for (var i = 0; i < topGenderInterestCodes.length; i++) {
			for (var f = 0; f < genderScoreSort.length; f++) {
				if (topGenderInterestCodes[i].interestCd == genderScoreSort[f].interestCd) {
					tempArray.push(genderScoreSort[f]);
					break;
				}
			}
		}

		// All other scores will be added to the bottom three.
		for (var f = 0; f < genderScoreSort.length; f++) {
			var isMatch = false;
			for (var i = 0; i < topGenderInterestCodes.length; i++) {
				if (topGenderInterestCodes[i].interestCd == genderScoreSort[f].interestCd) {
					isMatch = true;
					break;
				}
			}

			if (!isMatch) {
				tempArray.push(genderScoreSort[f]);
			}
		}

		$scope.genderScoreSort = tempArray;
	}

	// Setup message for social media sharing.
	$scope.genderShareText = "\n\nGender Score:";
	$scope.genderShareTextEmail = '%0D%0A %0D%0AGender Score:';
	for (var i = 0; i < $scope.genderScoreSort.length; i++) {
		$scope.genderShareText = $scope.genderShareText + '\n' + ($scope.genderScoreSort[i].interestCd == 'C' ? 'Conventional' : $scope.genderScoreSort[i].interestCd == 'S' ? 'Social' : $scope.genderScoreSort[i].interestCd == 'E' ? 'Enterprising' : $scope.genderScoreSort[i].interestCd == 'A' ? 'Artistic' : $scope.genderScoreSort[i].interestCd == 'R' ? 'Realistic' : $scope.genderScoreSort[i].interestCd == 'I' ? 'Investigative' : $scope.genderScoreSort[i].interestCd) + ": " + Math.floor($scope.genderScoreSort[i].genderScore * 100);
		$scope.genderShareTextEmail = $scope.genderShareTextEmail + '%0D%0A' + ($scope.genderScoreSort[i].interestCd == 'C' ? 'Conventional' : $scope.genderScoreSort[i].interestCd == 'S' ? 'Social' : $scope.genderScoreSort[i].interestCd == 'E' ? 'Enterprising' : $scope.genderScoreSort[i].interestCd == 'A' ? 'Artistic' : $scope.genderScoreSort[i].interestCd == 'R' ? 'Realistic' : $scope.genderScoreSort[i].interestCd == 'I' ? 'Investigative' : $scope.genderScoreSort[i].interestCd) + ": " + Math.floor($scope.genderScoreSort[i].genderScore * 100);
	}

	// sort combined score list in desc order
	var combinedScoreSort = [];
	for (var i = 0; i < $scope.scores.length; i++) {
		combinedScoreSort.push($scope.scores[i]);
	}
	combinedScoreSort.sort(function(a, b) {
		return b.combinedScore - a.combinedScore
	});

	// When topCombinedInterestCodes is undefined, this means there are no tied
	// scores for combined.
	if (topCombinedInterestCodes == undefined) {
		$scope.combinedScoreSort = combinedScoreSort;
	} else {

		// There are tied combined scores so need to order scores depending on
		// user's selections.
		var tempArray = [];

		// Use the user's tied score selection and move it to the top 3.
		for (var i = 0; i < topCombinedInterestCodes.length; i++) {
			for (var f = 0; f < combinedScoreSort.length; f++) {
				if (topCombinedInterestCodes[i].interestCd == combinedScoreSort[f].interestCd) {
					tempArray.push(combinedScoreSort[f]);
					break;
				}
			}
		}

		// All other scores will be added to the bottom three.
		for (var f = 0; f < combinedScoreSort.length; f++) {
			var isMatch = false;
			for (var i = 0; i < topCombinedInterestCodes.length; i++) {
				if (topCombinedInterestCodes[i].interestCd == combinedScoreSort[f].interestCd) {
					isMatch = true;
					break;
				}
			}

			if (!isMatch) {
				tempArray.push(combinedScoreSort[f]);
			}
		}

		$scope.combinedScoreSort = tempArray;
	}

	// Setup message for social media sharing.
	$scope.combinedShareText = "\n\nCombined Score:";
	$scope.combinedShareTextEmail = '%0D%0A %0D%0ACombined Score:';
	for (var i = 0; i < $scope.combinedScoreSort.length; i++) {
		$scope.combinedShareText = $scope.combinedShareText + '\n' + ($scope.combinedScoreSort[i].interestCd == 'C' ? 'Conventional' : $scope.combinedScoreSort[i].interestCd == 'S' ? 'Social' : $scope.combinedScoreSort[i].interestCd == 'E' ? 'Enterprising' : $scope.combinedScoreSort[i].interestCd == 'A' ? 'Artistic' : $scope.combinedScoreSort[i].interestCd == 'R' ? 'Realistic' : $scope.combinedScoreSort[i].interestCd == 'I' ? 'Investigative' : $scope.combinedScoreSort[i].interestCd) + ": " + Math.floor($scope.combinedScoreSort[i].combinedScore * 100);
		$scope.combinedShareTextEmail = $scope.combinedShareTextEmail + '%0D%0A' + ($scope.combinedScoreSort[i].interestCd == 'C' ? 'Conventional' : $scope.combinedScoreSort[i].interestCd == 'S' ? 'Social' : $scope.combinedScoreSort[i].interestCd == 'E' ? 'Enterprising' : $scope.combinedScoreSort[i].interestCd == 'A' ? 'Artistic' : $scope.combinedScoreSort[i].interestCd == 'R' ? 'Realistic' : $scope.combinedScoreSort[i].interestCd == 'I' ? 'Investigative' : $scope.combinedScoreSort[i].interestCd) + ": " + Math.floor($scope.combinedScoreSort[i].combinedScore * 100);
	}
	$scope.combinedShareText = $scope.combinedShareText + "\n\n";
	
	/**
	 * Gender information popup.
	 */
	$scope.genderInfoModal = function() {
		var modalOptions = {
			headerText : 'Gender Specific',
			bodyText : 'Growing up, males and females get different messages from their parents, schools, and the media about what careers are appropriate for them, resulting in exposure to different experiences. Three interest areas, Realistic, Artistic, and Social, tend to have significant difference in how males and females respond.  Gender-Specific results show your interests as they compare to those in your gender, and the scores may offer you a different set of careers to explore.'
		};

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}

	$scope.interestCodeInfo = function(interestCode) {
		var modalOptions;
		switch (interestCode) {
		case "R":
			modalOptions = {
				headerText : 'Realistic',
				bodyText : '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
				'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
				'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li><ul>'
			};
			break;
		case "I":
			modalOptions = {
				headerText : 'Investigative',
				bodyText : '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
				'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
				'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li><ul>'
			};
			break;
		case "A":
			modalOptions = {
				headerText : 'Artistic',
				bodyText : '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
				'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
				'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li><ul>'
			};
			break;
		case "S":
			modalOptions = {
				headerText : 'Social',
				bodyText : '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
				'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
				'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li><ul>'
			};
			break;
		case "E":
			modalOptions = {
				headerText : 'Enterprising',
				bodyText : '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
				'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
				'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li><ul>'
			};
			break;
		case "C":
			modalOptions = {
				headerText : 'Conventional',
				bodyText : '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
				'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
				'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li><ul>'
			};
			break;
		}
		

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
} ]);

cepApp.controller('FindYourInterestTestController', [ '$scope', '$http', '$location', 'FYIScoreService', 'FYIRestFactory', 'appUrl', '$q', 'questions', '$rootScope', 'modalService', function($scope, $http, $location, FYIScoreService, FYIRestFactory, appUrl, $q, questions, $rootScope, modalService) {

	$scope.questionList = questions[0].data;

	$scope.submitAnswer = function() {

		// this will be used for post call
		var fyiObject = {
			userId : $rootScope.globals.currentUser.userId,
			gender : $scope.gender,
			answers : undefined,
			rawScores : undefined
		};

		// set complete questionAnswerList in service
		FYIScoreService.questionAnswerList = $scope.questionList;

		// form validate for safari support
		var len = $scope.questionList.length;
		for (var i = 0; i < len; i++) {
			if ($scope.questionList[i].answer == undefined) {
				var modalOptions = {
					headerText : 'Find Your Interest',
					bodyText : 'Answer all questions.'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
				return false;
			}
		}

		// form validate for safari support
		if ($scope.gender == undefined) {
			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : 'Select gender.'
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
			return false;
		}

		// reset values
		FYIScoreService.resetValues();
		
		// calculate score
		FYIScoreService.calculateRawScore();

		var likes = FYIScoreService.getInterestLikes();
		var indifferents = FYIScoreService.getInterestIndifferents();
		var dislikes = FYIScoreService.getInterestDislikes();

		// if 81 or more of the same responses alert user to vary response
		if (likes >= 81 || indifferents >= 81 || dislikes >= 81) {

			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : '<p><b>Your response did not show a sufficient pattern of likes and dislikes for us to score your responses and provide you with your three Top Interest Codes.</b></p>' + '<p><b>Try retaking the inventory and using more varied responses.</b></p>' + '<ul><li>If you used a lot of Likes. try being more selective.</li><li>If you used a lot of Dislike, try being less slective.</li><li>If you used a lot of Indifferents, move off the fence</li></ul>' + '<hr>' + '<b>Tips for retaking the FYI:</b>' + '<ul><li>Read each interest item carefully before responding.</li><li>Make sure that your responses accurately reflect your interests.</li>' + '<li>Think about hobbies that youenjoy and relate them to the items in the FYI.</li><li>Keep in mind that you are not selecting an occupation. You are selecting activities that you either like, dislike, or are indifferent to doing</li></ul>'
			};

			modalService.showModal({}, modalOptions);

			return false;
		}

		var calcScore = FYIScoreService.getInterestCodeRawScore();

		fyiObject.rawScores = [ {
			interestCd : 'R',
			rawScore : calcScore.rRawScore
		}, {
			interestCd : 'I',
			rawScore : calcScore.iRawScore
		}, {
			interestCd : 'A',
			rawScore : calcScore.aRawScore
		}, {
			interestCd : 'S',
			rawScore : calcScore.sRawScore
		}, {
			interestCd : 'E',
			rawScore : calcScore.eRawScore
		}, {
			interestCd : 'C',
			rawScore : calcScore.cRawScore
		} ];

		/*
		 * $scope.interestCodeRawScore =
		 * FYIScoreService.getInterestCodeRawScore();
		 * 
		 * var genderRest =
		 * FYIRestFactory.getGenderScore(angular.toJson($scope.interestCodeRawScore)).then(function
		 * successCallback(response) { FYIScoreService.genderScore =
		 * response.data; }, function errorCallback(response) { alert(response); })
		 * 
		 * var combinedRest =
		 * FYIRestFactory.getCombinedScore(angular.toJson($scope.interestCodeRawScore)).then(function
		 * successCallback(response) { FYIScoreService.combinedScore =
		 * response.data; }, function errorCallback(response) { alert(response); })
		 */

		// delete properties not being used anymore before posting data
		var answers = JSON.parse(JSON.stringify($scope.questionList));
		var len = $scope.questionList.length;
		for (var i = 0; i < len; i++) {
			delete answers[i].qText;
			delete answers[i].interestCd;
		}

		fyiObject.answers = answers;

		FYIScoreService.setFyiObject(fyiObject);

		$location.path('/find-your-interest-score');

	};

	// show or hide buttons depending if question object is populated
	$scope.button = function() {
		if ($scope.questionList !== undefined) {
			return true;
		} else {
			return false;
		}
	};

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

cepApp.controller('FindYourInterestTiedController', [ '$scope', '$q', 'FYIScoreService', 'FYIRestFactory', '$rootScope', '$window', '$location', 'modalService', function($scope, $q, FYIScoreService, FYIRestFactory, $rootScope, $window, $location, modalService) {

	/**
	 * 
	 * Combined Tie Score
	 * 
	 */
	$scope.combinedTiedScoreObject = FYIScoreService.getCombinedTiedScoreObject();
	$scope.showGenderTieSelection = false;

	/**
	 * Keep track and limit number of selections that can be made.
	 */
	if ($scope.combinedTiedScoreObject !== undefined) {
		$scope.combinedLimit = $scope.combinedTiedScoreObject.numSlotsLeft;
		$scope.combinedChecked = 0;
		$scope.combinedCheckChanged = function(item) {
			if (item.selected)
				$scope.combinedChecked++;
			else
				$scope.combinedChecked--;
		}
	} else {
		$scope.showGenderTieSelection = true;
	}

	/**
	 * Submit score selection.
	 */
	$scope.combinedSubmit = function() {
		var topInterestCodes = [];
		var scoreList = $scope.combinedTiedScoreObject.scoreList;

		// determine the top three interest scores
		var numSelection = 0;
		for (var i = 0; i < scoreList.length; i++) {
			if (topInterestCodes.length < 3) {
				if (scoreList[i].checkbox != true) {
					topInterestCodes.push(scoreList[i]);
				} else {
					if (scoreList[i].selected == true) {
						topInterestCodes.push(scoreList[i]);
						numSelection++;
					}
				}
			}
		}

		// validate selections are made
		if (numSelection != $scope.combinedLimit) {

			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : 'Select ' + $scope.combinedTiedScoreObject.numSlotsLeft + ' of the ' + $scope.combinedTiedScoreObject.numTiedScore + ' tied interests.'
			};

			modalService.showModal({}, modalOptions);
			return false;
		}

		FYIScoreService.setTopCombinedInterestCodes(topInterestCodes);

		// Determines if gender scores need a tie breaker.
		if ($scope.genderTiedScoreObject != undefined) {
			var numMatch = 0;
			var combinedNumberCheckboxes = 0;
			var genderNumberCheckboxes = 0;

			for (i in $scope.combinedTiedScoreObject.scoreList) {
				if ($scope.combinedTiedScoreObject.scoreList[i].checkbox == true) {
					combinedNumberCheckboxes++;
				}
			}

			for (i in $scope.genderTiedScoreObject.scoreList) {
				if ($scope.genderTiedScoreObject.scoreList[i].checkbox == true) {
					genderNumberCheckboxes++;
				}
			}
			if (combinedNumberCheckboxes == genderNumberCheckboxes) {
				for (var i = 0; i < scoreList.length; i++) {
					for (var f = 0; f < $scope.genderTiedScoreObject.scoreList.length; f++) {
						if (scoreList[i].checkbox == true && $scope.genderTiedScoreObject.scoreList[f].checkbox == true && scoreList[i].interestCd == $scope.genderTiedScoreObject.scoreList[f].interestCd) {
							numMatch++;
							break;
						}
					}
				}
			}

			// If interest codes are not the same then display gender score tie
			// breaker.
			if (numMatch != combinedNumberCheckboxes) {
				$scope.showGenderTieSelection = true;
				return false;
			} else {

				// Gender tie score breaker are the same for combined, so reuse
				// combined FYI selection for gender scores.
				var genderScoreList = $scope.genderTiedScoreObject.scoreList;
				var tempGenderScoreList = [];
				for (var f = 0; f < topInterestCodes.length; f++) {
					for (var i = 0; i < genderScoreList.length; i++) {
						if (topInterestCodes[f].interestCd == genderScoreList[i].interestCd) {
							tempGenderScoreList.push(genderScoreList[i]);
							break;
						}
					}
				}

				FYIScoreService.setTopGenderInterestCodes(tempGenderScoreList);
			}

		}

		// Save Top interest code to databse
		saveTopInterestCodeToDatabase();

		// Redirect to score page.
		$location.path('/find-your-interest-score');

	}

	/**
	 * 
	 * Gender Tie Score
	 * 
	 */
	$scope.genderTiedScoreObject = FYIScoreService.getGenderTiedScoreObject();

	/**
	 * Keep track and limit number of selections that can be made.
	 */
	if ($scope.genderTiedScoreObject !== undefined) {
		$scope.genderLimit = $scope.genderTiedScoreObject.numSlotsLeft;
		$scope.genderChecked = 0;
		$scope.genderCheckChanged = function(item) {
			if (item.selected)
				$scope.genderChecked++;
			else
				$scope.genderChecked--;
		}
	}

	/**
	 * Submit score selection.
	 */
	$scope.genderSubmit = function() {
		var topInterestCodes = [];
		var scoreList = $scope.genderTiedScoreObject.scoreList;

		// determine the top three interest scores
		var numSelection = 0;
		for (var i = 0; i < scoreList.length; i++) {
			if (topInterestCodes.length < 3) {
				if (scoreList[i].checkbox != true) {
					topInterestCodes.push(scoreList[i]);
				} else {
					if (scoreList[i].selected == true) {
						topInterestCodes.push(scoreList[i]);
						numSelection++;
					}
				}
			}
		}

		// validate selections are made
		if (numSelection != $scope.genderLimit) {

			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : 'Select ' + $scope.genderTiedScoreObject.numSlotsLeft + ' of the ' + $scope.genderTiedScoreObject.numTiedScore + ' tied interests.'
			};

			modalService.showModal({}, modalOptions);
			return false;
		}

		FYIScoreService.setTopGenderInterestCodes(topInterestCodes);

		// Save Top interest code to database
		saveTopInterestCodeToDatabase();

		// Redirect to score page.
		$location.path('/find-your-interest-score');
	}

	/**
	 * Saves the top selected interest codes for gender and combine to the
	 * database.
	 */
	saveTopInterestCodeToDatabase = function() {
		
		// Reset values
		FYIScoreService.setCombinedTiedScoreObject(undefined);
		FYIScoreService.setGenderTiedScoreObject(undefined);
		$window.sessionStorage.removeItem("combinedTiedScoreObject");
		$window.sessionStorage.removeItem("genderTiedScoreObject");
		
		var topGScore = FYIScoreService.getTopGenderInterestCodes();
		var topCScore = FYIScoreService.getTopCombinedInterestCodes();
		var tiedObject = {
			userId : $rootScope.globals.currentUser.userId,
			combineInterestOne : topCScore ? topCScore[0].interestCd : null,
			combineInterestTwo : topCScore ? topCScore[1].interestCd : null,
			combineInterestThree : topCScore ? topCScore[2].interestCd : null,
			genderInterestOne : topGScore ? topGScore[0].interestCd : null,
			genderInterestTwo : topGScore ? topGScore[1].interestCd : null,
			genderInterestThree : topGScore ? topGScore[2].interestCd : null,
		}
		FYIRestFactory.insertTiedSelection(tiedObject);
		
		// Reset value
		FYIScoreService.setTiedLinkClicked(false);
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

cepApp.controller('FindYourInterestTiedScoreController', [ '$scope', 'FYIScoreService', 'FYIRestFactory', '$rootScope', '$window', '$location', 'modalService', function($scope, FYIScoreService, FYIRestFactory, $rootScope, $window, $location, modalService) {

	$scope.tiedScoreObject = FYIScoreService.getTiedScoreObject();

	/**
	 * Keep track and limit number of selections that can be made.
	 */
	$scope.limit = $scope.tiedScoreObject.numSlotsLeft;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Submit score selection.
	 */
	$scope.submit = function() {
		var topInterestCodes = [];
		var scoreList = $scope.tiedScoreObject.scoreList;

		// determine the top three interest scores
		var numSelection = 0;
		for (var i = 0; i < scoreList.length; i++) {
			if (topInterestCodes.length < 3) {
				if (scoreList[i].checkbox != true) {
					topInterestCodes.push(scoreList[i]);
				} else {
					if (scoreList[i].selected == true) {
						topInterestCodes.push(scoreList[i]);
						numSelection++;
					}
				}
			}
		}

		// validate selections are made
		if (numSelection != $scope.limit) {

			var modalOptions = {
				headerText : 'Find Your Interest',
				bodyText : 'Select ' + $scope.tiedScoreObject.numSlotsLeft + ' of the ' + $scope.tiedScoreObject.numTiedScore + ' tied interests.'
			};

			modalService.showModal({}, modalOptions);
			return false;
		}

		// setup score object to post
		var scoreObject = {
			userId : $rootScope.globals.currentUser.userId,
			scoreChoice : $scope.scoreSelected,
			interestCodeOne : topInterestCodes[0].interestCd,
			interestCodeTwo : topInterestCodes[1].interestCd,
			interestCodeThree : topInterestCodes[2].interestCd
		}

		// post user score selection
		FYIRestFactory.postUserScores(angular.toJson(scoreObject)).then(function successCallback(response) {

			// erase session storage for interest codes
			$window.sessionStorage.removeItem("interestCodeOne");
			$window.sessionStorage.removeItem("interestCodeTwo");
			$window.sessionStorage.removeItem("interestCodeThree");

			// reset selected property
			var i = 0;
			while (i < $scope.tiedScoreObject.scoreList.length) {
				$scope.tiedScoreObject.scoreList[i].selected = false;
				i++;
			}

			$location.path('/dashboard');
		}, function errorCallback(response) {
			
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'Failed to save test scores. Try submitting again. If Error continues, please inform site administrator.'
			};

			modalService.showModal({}, modalOptions);
			return false;
			
		});
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);

cepApp.controller('MyAsvabSummaryController', [ 
	'$scope', 'FYIScoreService', 'FYIRestFactory', '$rootScope', '$window', '$location', '$anchorScroll', '$route', '$templateCache', 'MediaCenterService', 'modalService', 'youTubeModalService', 'scoreSummary', 'cepManualInput', 'ssoUrl',
	function($scope, FYIScoreService, FYIRestFactory, $rootScope, $window, $location, $anchorScroll, $route, $templateCache, MediaCenterService, modalService, youTubeModalService, scoreSummary, cepManualInput, ssoUrl) {
	if($rootScope.globals.currentUse)
	$scope.userRole = $rootScope.globals.currentUser.role;
	$scope.scoreSummary = scoreSummary[0].data;
	$scope.haveAsvabTestScores = (angular.isObject($scope.scoreSummary) && $scope.scoreSummary.hasOwnProperty('verbalAbility')); // do we have actual test scores
	$scope.haveManualAsvabTestScores = (!$scope.haveAsvabTestScores && ($window.sessionStorage.getItem('manualScores') == "true")); // do we have manual test scores
	
	$scope.genderFull = "";
	$scope.testDate = {
		"month" : "",
		"day" : "",
		"year" : ""
	}
	$scope.mediaCenterArticle = 'media-center-article/';
	$scope.resultsSelection = "student";

	MediaCenterService.getMediaCenterList().then(function(s) {
		$scope.mediaCenterList = s;
	})

	$scope.showYouTubeModal = function(videoName) {
		youTubeModalService.show({
			size : 'lg'
		}, {
			videoName : videoName
		});
	}

	$scope.cepManualInput = function(cepManualMode) {
		var enterMode = cepManualMode == 'enter' ? true : false;
		var retrieveMode = cepManualMode == 'retrieve' ? true : false;
		cepManualInput.show({}, {enter: enterMode, retrieve: retrieveMode}).then(function(result) {
			//console.log(result);
			if (result) {
				if (result.hasOwnProperty('aFQTRawSSComposites')) {
					// Retrieved asvab scores
					$scope.scoreSummary = result;
					//console.log($scope.scoreSummary);
					$scope.haveAsvabTestScores = true;
					$scope.haveManualAsvabTestScores = false;
				} else {
					// Entered asvab scores
					$scope.haveAsvabTestScores = false;
					$scope.haveManualAsvabTestScores = true;
					$scope.asvabScore.push({verbalScore : result.verbalScore, mathScore : result.mathScore, scienceScore : result.scienceScore});
				}
				
				// Update session storage
				$window.sessionStorage.setItem('manualScores', $scope.haveManualAsvabTestScores);
				//$window.sessionStorage.setItem('sV', $scope.getScore('verbal'));
				//$window.sessionStorage.setItem('sM', $scope.getScore('math'));
				//$window.sessionStorage.setItem('sS', $scope.getScore('science'));
				//$window.sessionStorage.setItem('sA', $scope.getScore('afqt'));

				// Update book stacks
				//$scope.vBooks.score = $scope.getScore('verbal');
				//$scope.mBooks.score = $scope.getScore('math');
				//$scope.sBooks.score = $scope.getScore('science');
				//$scope.scoreSorting();
				
				//update charts on page
				/*
				console.log($scope.myCharts["cesScores1_1"]);
				$scope.myCharts["cesScores1_1"].update();
				*/
				
			}
		});
	}

	$scope.scrollTo = function(id) {
		$location.hash(id);
		$anchorScroll();
	}

	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			$('.goToTop').fadeIn();
		} else {
			$('.goToTop').fadeOut();
		}
	});

	$('.goToTop').click(function () {
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});

	$scope.hiliteItems = function(area) {
		/**
		 * TODO: hilite areas of chart canvas
		 */
		var returnVal;
		if (area == "none") {
			returnVal = "Clear Hilites";
		} 
		else {
			switch(area) {
				case "verbal":
					returnVal = "Hilite Verbal Skills";
					break;
				case "math":
					returnVal = "Hilite Math Skills";
					break;
				case "science":
					returnVal = "Hilite Science Skills";
					break;
				case "other":
					returnVal = "Hilite Other Skills";
					break;
				default:
					break;
			};
		}
	}

	$scope.getLow = function(num1, num2) {
		return Math.min(num1, num2);
	}

	$scope.getHigh = function(num1, num2) {
		return Math.max(num1, num2);
	}

	/**
	 * On chart create event add all charts to object for reference
	 */
	$scope.myCharts = [];
	$scope.$on('chart-create', function (evt, chart) {
			$scope.myCharts[chart.chart.canvas.id] = chart;
			
			//chart.update();
	});

	/**
	 * CES Percentile Scores
	 * display order femmale, male, all
	 */
	$scope.chartRef = $scope.scoreSummary.verbalAbility
	if($scope.scoreSummary.controlInformation.gender != undefined)
		$scope.gender = $scope.scoreSummary.controlInformation.gender.substring(0,1).toLowerCase();
	else
		$scope.gender  == 'm';
	
	$scope.cesData2_1 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.va_GS_Percentage) :  Number($scope.chartRef.va_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.va_GS_Percentage) :  Number($scope.chartRef.va_GOS_Percentage), 
			Number($scope.chartRef.va_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.mathematicalAbility
	$scope.cesData2_2 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.ma_GS_Percentage) :  Number($scope.chartRef.ma_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.ma_GS_Percentage) :  Number($scope.chartRef.ma_GOS_Percentage), 
			Number($scope.chartRef.ma_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.scienceTechnicalAbility
	$scope.cesData2_3 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.tec_GS_Percentage) :  Number($scope.chartRef.tec_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.tec_GS_Percentage) :  Number($scope.chartRef.tec_GOS_Percentage), 
			Number($scope.chartRef.tec_COMP_Percentage)
		]
	];

	/**
	 * ASVAB Percentile Scores
	 * display order femmale, male, all
	 */
	$scope.chartRef = $scope.scoreSummary.generalScience
	$scope.asvabData2_1 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.gs_GS_Percentage) :  Number($scope.chartRef.gs_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.gs_GS_Percentage) :  Number($scope.chartRef.gs_GOS_Percentage), 
			Number($scope.chartRef.gs_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.arithmeticReasoning
	$scope.asvabData2_2 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.ar_GS_Percentage) :  Number($scope.chartRef.ar_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.ar_GS_Percentage) :  Number($scope.chartRef.ar_GOS_Percentage), 
			Number($scope.chartRef.ar_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.wordKnowledge
	$scope.asvabData2_3 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.wk_GS_Percentage) :  Number($scope.chartRef.wk_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.wk_GS_Percentage) :  Number($scope.chartRef.wk_GOS_Percentage), 
			Number($scope.chartRef.wk_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.paragraphComprehension
	$scope.asvabData2_4 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.pc_GS_Percentage) :  Number($scope.chartRef.pc_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.pc_GS_Percentage) :  Number($scope.chartRef.pc_GOS_Percentage), 
			Number($scope.chartRef.pc_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.mathematicsKnowledge
	$scope.asvabData2_5 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.mk_GS_Percentage) :  Number($scope.chartRef.mk_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.mk_GS_Percentage) :  Number($scope.chartRef.mk_GOS_Percentage), 
			Number($scope.chartRef.mk_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.electronicsInformation
	$scope.asvabData2_6 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.ei_GS_Percentage) :  Number($scope.chartRef.ei_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.ei_GS_Percentage) :  Number($scope.chartRef.ei_GOS_Percentage), 
			Number($scope.chartRef.ei_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.autoShopInformation
	$scope.asvabData2_7 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.as_GS_Percentage) :  Number($scope.chartRef.as_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.as_GS_Percentage) :  Number($scope.chartRef.as_GOS_Percentage), 
			Number($scope.chartRef.as_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.mechanicalComprehension
	$scope.asvabData2_8 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.mc_GS_Percentage) :  Number($scope.chartRef.mc_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.mc_GS_Percentage) :  Number($scope.chartRef.mc_GOS_Percentage), 
			Number($scope.chartRef.mc_COMP_Percentage)
		]
	];
	$scope.chartRef = $scope.scoreSummary.assemblingObjects
	$scope.asvabData2_9 = [
		[
			$scope.gender == 'f' ? Number($scope.chartRef.ao_GS_Percentage) :  Number($scope.chartRef.ao_GOS_Percentage),
			$scope.gender == 'm' ? Number($scope.chartRef.ao_GS_Percentage) :  Number($scope.chartRef.ao_GOS_Percentage), 
			Number($scope.chartRef.ao_COMP_Percentage)
		]
	];

	$scope.greenColors = [
		{
			backgroundColor: "#29afb5",
			borderColor: "#29afb5",
			hoverBackgroundColor: "#29afb5",
			hoverBorderColor: "#29afb5",
		}
	];
	
	$scope.redColors = [
		{
			backgroundColor: "#8b2e33",
			borderColor: "#8b2e33",
			hoverBackgroundColor: "#8b2e33",
			hoverBorderColor: "#8b2e33"
		}
	];
	
	$scope.purpleColors = [
		{
			backgroundColor: "#413a92",
			borderColor: "#413a92",
			hoverBackgroundColor: "#413a92",
			hoverBorderColor: "#413a92"
		}
	];
	$scope.blackColors = [
		{
			backgroundColor: "#333333",
			borderColor: "#333333",
			hoverBackgroundColor: "#333333",
			hoverBorderColor: "#333333"
		}
	];
	
	/**
	 * All/Global Percent chart labels/options
	 */
	$scope.percentChartLabels = ["Female", "Male", "All"];
	$scope.percentChartOptions = {
		events: [],
		title: {
			display: false
		},
		legend: {
			display: false
		},
		scales: {
			xAxes: [{
				ticks: {
					beginAtZero: true,
					min: 0,
					max: 100,
					stepSize: 20
				},
				gridLines: {
					zeroLineColor: "transparent",
					zeroLineWidth: 0
				}
			}],
			yAxes: [{
				barThickness: 16,
				gridLines: {
					zeroLineColor: "transparent",
					zeroLineWidth: 0
				}
			}]
		},
		tooltips: {
			enabled: false
		},
		animation: {
			duration: 250,
			onProgress: modifyChartBars,
			onComplete: modifyChartBars
		}
	};

	/**
	 * Standard Scores - Actual
	 */
	$scope.standardScores = {
		cesScores1_1: [ // actual scores
			Number($scope.scoreSummary.verbalAbility.va_SGS), 
			Number($scope.scoreSummary.mathematicalAbility.ma_SGS), 
			Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS)
		],
		asvabScores1_1: [ // actual scores
			Number($scope.scoreSummary.generalScience.gs_SGS) > 0 ? Number($scope.scoreSummary.generalScience.gs_SGS) : 0, 
			Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) : 0, 
			Number($scope.scoreSummary.wordKnowledge.wk_SGS) > 0 ? Number($scope.scoreSummary.wordKnowledge.wk_SGS) : 0, 
			Number($scope.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? Number($scope.scoreSummary.paragraphComprehension.pc_SGS) : 0, 
			Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) : 0, 
			Number($scope.scoreSummary.electronicsInformation.ei_SGS) > 0 ? Number($scope.scoreSummary.electronicsInformation.ei_SGS) : 0, 
			Number($scope.scoreSummary.autoShopInformation.as_SGS) > 0 ? Number($scope.scoreSummary.autoShopInformation.as_SGS) : 0, 
			Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) : 0,
			Number($scope.scoreSummary.assemblingObjects.ao_SGS) > 0 ? Number($scope.scoreSummary.assemblingObjects.ao_SGS) : 0
		]
	};
	
	/**
	 * CES Standard Scores Chart Data
	 * chart.js does not have a range bar chart
	 * Using a stacked horz bar chart to create a range type chart
	 * inital bar is set to transparent
	 * LSL = lower score limit; USL = high score limit
	 * added getLow/getHigh due to USL coming in lower than LSL in some data
	 */
	$scope.lowVals = [];
	$scope.lowVals = [
		$scope.getLow(Number($scope.scoreSummary.verbalAbility.va_LSL), Number($scope.scoreSummary.verbalAbility.va_USL)),
		$scope.getLow(Number($scope.scoreSummary.mathematicalAbility.ma_LSL), Number($scope.scoreSummary.mathematicalAbility.ma_USL)),
		$scope.getLow(Number($scope.scoreSummary.scienceTechnicalAbility.tec_LSL), Number($scope.scoreSummary.scienceTechnicalAbility.tec_USL)),
	];
	$scope.highVals = [];
	$scope.highVals = [
		$scope.getHigh(Number($scope.scoreSummary.verbalAbility.va_LSL), Number($scope.scoreSummary.verbalAbility.va_USL)),
		$scope.getHigh(Number($scope.scoreSummary.mathematicalAbility.ma_LSL), Number($scope.scoreSummary.mathematicalAbility.ma_USL)),
		$scope.getHigh(Number($scope.scoreSummary.scienceTechnicalAbility.tec_LSL), Number($scope.scoreSummary.scienceTechnicalAbility.tec_USL)),
	];

	$scope.cesData1_1 = [
		[ // score low range bar start value < actual score or 0 if no score
			Number($scope.scoreSummary.verbalAbility.va_SGS) > 0 ? $scope.lowVals[0]+2 : 0,
			Number($scope.scoreSummary.mathematicalAbility.ma_SGS) > 0 ? $scope.lowVals[1]+2 : 0,
			Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? $scope.lowVals[2]+2 : 0
		],
		[ // score total bar range (high - low) > actual score or 0 if no score
			Number($scope.scoreSummary.verbalAbility.va_SGS) > 0 ? $scope.highVals[0] - $scope.lowVals[0]+2 : 0,
			Number($scope.scoreSummary.mathematicalAbility.ma_SGS) > 0 ? $scope.highVals[1] - $scope.lowVals[1]+2 : 0,
			Number($scope.scoreSummary.scienceTechnicalAbility.tec_SGS) > 0 ? $scope.highVals[2] - $scope.lowVals[2]+2 : 0
		]
	];

	$scope.cesLabels1_1 = [
		"Verbal Skills", 
		"Math Skills", 
		["Science/", "Technical Skills"]
	];

	$scope.standardScoreColors1 = [
		{
			backgroundColor: ["transparent", "transparent", "transparent"],
			borderColor: ["transparent", "transparent", "transparent"],
			hoverBackgroundColor: ["transparent", "transparent", "transparent"],
			hoverBorderColor: ["transparent", "transparent", "transparent"]
		},
		{
			backgroundColor: [
				$scope.greenColors[0].backgroundColor,
				$scope.redColors[0].backgroundColor,
				$scope.purpleColors[0].backgroundColor
			],
			borderColor: [
				$scope.greenColors[0].borderColor,
				$scope.redColors[0].borderColor,
				$scope.purpleColors[0].borderColor
			]//,
			//hoverBackgroundColor: "#333333",
			//hoverBorderColor: "#333333"
		}
	];

	/**
	 * ASVAB Standard Scores Chart Data
	 * chart.js does not have a range bar chart
	 * Using a stacked horz bar chart to create a range type chart
	 * inital bar is set to transparent 
	 * LSL = lower score limit; USL = high score limit
	 * added getLow/getHihg due to USL coming in lower than LSL in some data
	 */
	$scope.lowVals = [];
	$scope.lowVals = [
		$scope.getLow(Number($scope.scoreSummary.generalScience.gs_LSL), Number($scope.scoreSummary.generalScience.gs_USL)),
		$scope.getLow(Number($scope.scoreSummary.arithmeticReasoning.ar_LSL), Number($scope.scoreSummary.arithmeticReasoning.ar_USL)),
		$scope.getLow(Number($scope.scoreSummary.wordKnowledge.wk_LSL), Number($scope.scoreSummary.wordKnowledge.wk_USL)),
		$scope.getLow(Number($scope.scoreSummary.paragraphComprehension.pc_LSL), Number($scope.scoreSummary.paragraphComprehension.pc_USL)),
		$scope.getLow(Number($scope.scoreSummary.mathematicsKnowledge.mk_LSL), Number($scope.scoreSummary.mathematicsKnowledge.mk_USL)),
		$scope.getLow(Number($scope.scoreSummary.electronicsInformation.ei_LSL), Number($scope.scoreSummary.electronicsInformation.ei_USL)),
		$scope.getLow(Number($scope.scoreSummary.autoShopInformation.as_LSL), Number($scope.scoreSummary.autoShopInformation.as_USL)),
		$scope.getLow(Number($scope.scoreSummary.mechanicalComprehension.mc_LSL), Number($scope.scoreSummary.mechanicalComprehension.mc_USL)),
		$scope.getLow(Number($scope.scoreSummary.assemblingObjects.ao_LSL), Number($scope.scoreSummary.assemblingObjects.ao_USL))
	];
	$scope.highVals = [];
	$scope.highVals = [
		$scope.getHigh(Number($scope.scoreSummary.generalScience.gs_LSL), Number($scope.scoreSummary.generalScience.gs_USL)),
		$scope.getHigh(Number($scope.scoreSummary.arithmeticReasoning.ar_LSL), Number($scope.scoreSummary.arithmeticReasoning.ar_USL)),
		$scope.getHigh(Number($scope.scoreSummary.wordKnowledge.wk_LSL), Number($scope.scoreSummary.wordKnowledge.wk_USL)),
		$scope.getHigh(Number($scope.scoreSummary.paragraphComprehension.pc_LSL), Number($scope.scoreSummary.paragraphComprehension.pc_USL)),
		$scope.getHigh(Number($scope.scoreSummary.mathematicsKnowledge.mk_LSL), Number($scope.scoreSummary.mathematicsKnowledge.mk_USL)),
		$scope.getHigh(Number($scope.scoreSummary.electronicsInformation.ei_LSL), Number($scope.scoreSummary.electronicsInformation.ei_USL)),
		$scope.getHigh(Number($scope.scoreSummary.autoShopInformation.as_LSL), Number($scope.scoreSummary.autoShopInformation.as_USL)),
		$scope.getHigh(Number($scope.scoreSummary.mechanicalComprehension.mc_LSL), Number($scope.scoreSummary.mechanicalComprehension.mc_USL)),
		$scope.getHigh(Number($scope.scoreSummary.assemblingObjects.ao_LSL), Number($scope.scoreSummary.assemblingObjects.ao_USL))
	];

	$scope.asvabData1_1 = [
		[ // // score low range bar start value < actual score
			Number($scope.scoreSummary.generalScience.gs_SGS) > 0 ? $scope.lowVals[0]+2 : 0,
			Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? $scope.lowVals[1]+2 : 0,
			Number($scope.scoreSummary.wordKnowledge.wk_SGS) > 0 ? $scope.lowVals[2]+2 : 0,
			Number($scope.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? $scope.lowVals[3]+2 : 0,
			Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? $scope.lowVals[4]+2 : 0,
			Number($scope.scoreSummary.electronicsInformation.ei_SGS) > 0 ? $scope.lowVals[5]+2 : 0,
			Number($scope.scoreSummary.autoShopInformation.as_SGS) > 0 ? $scope.lowVals[6]+2 : 0,
			Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? $scope.lowVals[7]+2 : 0,
			Number($scope.scoreSummary.assemblingObjects.ao_SGS) > 0 ? $scope.lowVals[8]+2 : 0
		],
		[ // score total bar range (high - low) > actual score
			Number($scope.scoreSummary.generalScience.gs_SGS) > 0 ? $scope.highVals[0] - $scope.lowVals[0]+2 : 0,
			Number($scope.scoreSummary.arithmeticReasoning.ar_SGS) > 0 ? $scope.highVals[1] - $scope.lowVals[1]+2 : 0,
			Number($scope.scoreSummary.wordKnowledge.wk_SGS) > 0 ? $scope.highVals[2] - $scope.lowVals[2]+2 : 0,
			Number($scope.scoreSummary.paragraphComprehension.pc_SGS) > 0 ? $scope.highVals[3] - $scope.lowVals[3]+2 : 0,
			Number($scope.scoreSummary.mathematicsKnowledge.mk_SGS) > 0 ? $scope.highVals[4] - $scope.lowVals[4]+2 : 0,
			Number($scope.scoreSummary.electronicsInformation.ei_SGS) > 0 ? $scope.highVals[5] - $scope.lowVals[5]+2 : 0,
			Number($scope.scoreSummary.autoShopInformation.as_SGS) > 0 ? $scope.highVals[6] - $scope.lowVals[6]+2 : 0,
			Number($scope.scoreSummary.mechanicalComprehension.mc_SGS) > 0 ? $scope.highVals[7] - $scope.lowVals[7]+2 : 0,
			Number($scope.scoreSummary.assemblingObjects.ao_SGS) > 0 ? $scope.highVals[8] - $scope.lowVals[8]+2 : 0
		]
	];
	
	$scope.asvabLabels1_2 = [
		["General", "Science"], 
		["Arithmetic", "Reasoning"], 
		["World", "Knowledge"], 
		["Paragraph", "Comprehension"], 
		["Mathematics", "Knowledge"], 
		["Electronics", "Information"], 
		["Auto and", "Shop Information"], 
		["Mechanical", "Comprehension"],
		["Assembling", "Objects"]
	];

	$scope.standardScoreColors2 = [
		{
			backgroundColor: ["transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent"],
			borderColor: ["transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent"],
			hoverBackgroundColor: ["transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent"],
			hoverBorderColor: ["transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent", "transparent"]
		},
		{
			backgroundColor: [
				$scope.purpleColors[0].backgroundColor,
				$scope.redColors[0].backgroundColor,
				$scope.greenColors[0].backgroundColor,
				$scope.greenColors[0].backgroundColor,
				$scope.redColors[0].backgroundColor,
				$scope.purpleColors[0].backgroundColor,
				$scope.blackColors[0].backgroundColor,
				$scope.purpleColors[0].backgroundColor,
				$scope.blackColors[0].backgroundColor
			],
			borderColor: [
				$scope.purpleColors[0].borderColor,
				$scope.redColors[0].borderColor,
				$scope.greenColors[0].borderColor,
				$scope.greenColors[0].borderColor,
				$scope.redColors[0].borderColor,
				$scope.purpleColors[0].borderColor,
				$scope.blackColors[0].borderColor,
				$scope.purpleColors[0].borderColor,
				$scope.blackColors[0].backgroundColor
			]//,
			//hoverBackgroundColor: "#333333",
			//hoverBorderColor: "#333333"
		}
	];

	$scope.scoreChartOptions = {
		events: [],
		title: {
			display: false
		},
		legend: {
			display: false
		},
		scales: {
			xAxes: [{
				stacked: true,
				ticks: {
					beginAtZero: true,
					min: 0,
					max: 100,
					stepSize: 20
				},
				gridLines: {
					zeroLineColor: "transparent",
					zeroLineWidth: 0
				}
			}],
			yAxes: [{
				stacked: true,
				barThickness: 16,
				gridLines: {
					zeroLineColor: "transparent",
					zeroLineWidth: 0
				}
			}]
		},
		tooltips: {
			enabled: false
		},
		animation: {
			duration: 250,
			onProgress: modifyChartBars,
			onComplete: modifyChartBars
		}
	};

	/**
	 * AFQT doughnut chart
	 */
	$scope.getAfqtScore = function() {
		var afqtScore = Number($scope.scoreSummary.aFQTRawSSComposites.afqt);
		return afqtScore
	}

	$scope.afqtChartData = [$scope.getAfqtScore(), (100-$scope.getAfqtScore())];
	$scope.afqtChartLabels = ["AFQT Score", "Total"];
	$scope.afqtChartColors = ['#4c44b4', '#f2f2f2'];
	$scope.afqtChartOptions = {
		events: [],
		segmentShowStroke : false,
		animation: false,
		tooltips: {
			enabled: false
		},
		elements: {
			arc: {
				borderWidth: 0
			}
		}
	};

	/**
	 * Chart helper function to modify bar chart bar displays
	 */
	function modifyChartBars() {
		var txt = "";
		var txtW = 0;
		var chartId = "";
		var chartInstance = this.chart;
		var ctx = chartInstance.ctx;
		var prevX = 0;
		var sX = 0;
		var sW = 0;
		var rem = [];
		
		ctx.font = Chart.helpers.fontString(10, 'normal', Chart.defaults.global.defaultFontFamily);
		ctx.fillStyle = chartInstance.config.options.defaultFontColor;
		ctx.textAlign = "center";
		ctx.textBaseline = 'middle';

		Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
			var meta = chartInstance.controller.getDatasetMeta(i);
			var stacked = meta.data[i]._yScale.options.stacked ? true : false; // detect if chart is stacked

			Chart.helpers.each(meta.data.forEach(function (bar, index) {
				chartId = chartInstance.canvas.id;
				txt = dataset.data[index].toString();
				txtW = Math.ceil(ctx.measureText(txt).width);
				if (dataset.backgroundColor[i] != "transparent") { // don't label transparent bars
					if (stacked) { // using stacked chart to display score range chart
						if ((dataset.data[index] <= 0) || ($scope.standardScores[chartId][index] <= 0)) { // if no data
							txt = "No Data";
						} else {
							txt = $scope.standardScores[chartId][index]; // replace bar label with actual score
						}
						sX = bar._model.base;
						sW = bar._model.x - sX;
					}

					if (dataset.data[index] <= 0) { // prevent label from displaying on bar if bar length is too narrow
						ctx.fillStyle = 'rgba(21, 21, 21, 1)'; // dark grey
						ctx.fillText("No Data", (bar._model.x + 20), bar._model.y);
					} else if (dataset.data[index] < 5) { // prevent label from displaying on bar if bar length is too narrow
						ctx.fillText(txt, (bar._model.x + 4), bar._model.y);
					} else {
						ctx.fillStyle = 'rgba(242, 242, 242, 1)'; // very light grey
						if (stacked && txt != "No Data") {
							ctx.fillText(txt, (sX + (sW/2)), bar._model.y);// center on score band
						} else {
							ctx.fillText(txt, (bar._model.x - txtW), bar._model.y); // place at end
						}
					}
				}
			}), this);
		}), this);
	}

	$scope.moreInfo = function(area) {
		var modalOptions;
		switch (area) {
		case "ces":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<p>Consider these scores when exploring careers in the OCCU-Find. Use your highest score to review occupations based on the corresponding skill importance rating.</p>' + 
					'<p>ASVAB test scores are combined to yield three Career Exploration Scores: Verbal Skills, Math Skills, and Science/Technical Skills. Each of these scores is made up of a combination of some of the individual ASVAB tests.</p>' + 
					'<p><strong>Verbal Skills</strong> - estimates your potential for verbal activities; derived from the Word Knowledge (WK) and Paragraph Comprehension (PC) tests.</p>' + 
					'<p><strong>Math Skills</strong> - estimates your potential for mathematical activities; derived from the Mathematics Knowledge (MK) and Arithmetic Reasoning (AR) tests.</p>' + 
					'<p><strong>Science/Technical Skills</strong> - estimates your potential for science and technical activities, derived from the General Science (GS), Mechanical Comprehension (MC), and Electronics Information (EI) tests.</p>' + 
					'<p>These three Career Exploration Scores are good indicators of your potential for success in further education, training, and in occupations. They focus on skills (which are learned and modifiable) rather than on abilities (which are often seen as fixed).</p>'
			};
			break;
		case "cesStandard":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<h4>Standard Scores</h4>' +
					'<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
			};
			break;
		case "cesPercent":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<h4>Percentile Scores</h4>' +
					'<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
			};
			break;
		case "scoreBands":
			modalOptions = {
				headerText : 'Score Bands',
				bodyText : '<p>On the ASVAB Summary Results sheet, the standard scores are shown in a graph with corresponding bands illustrating the range within which your scores would likely fall should you take the test again. You might want to focus your attention to any score bands that stand out (i.e., are located to the left or right of the other score bands). Such scores would suggest either a strength (to the right of the others) or a weakness (to the left of the others).</p>'
			};
			break;
		case "asvabStandard":
			modalOptions = {
				headerText : 'ASVAB Test Scores',
				bodyText : '<h4>Standard Scores</h4>' +
				'<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
			};
			break;
		case "asvabPercent":
			modalOptions = {
				headerText : 'ASVAB Test Scores',
				bodyText : '<h4>Percentile Scores</h4>' +
				'<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
			};
			break;
		case "afqt":
			modalOptions = {
				headerText : 'Military Entrance Score (AFQT)',
				bodyText : '<p>Participation in the ASVAB CEP does not obligate you to talk with military recruiters or consider a military career.</p>' + 
				'<p>The AFQT score is what the Military will use to determine enlistment eligibility.</p>' + 
				'<p>If you are interested in joining the Military it is best to discuss this score with a recruiter.</p>'
			};
			break;
		}
		
		// For ASVAB open CITM Composites Score page in new tab/window not modal win
		if (area == "asvab") {
			//$window.open('http://www.citm.com/CITM/composite-scores', '_blank');
            $window.open(ssoUrl + 'composite-scores', '_blank');
		} else {
			modalService.showModal({
				size : 'md'
			}, modalOptions);
		}
	}

	$scope.getAbbrMonth = function(monthStr) {
		var returnMonth;
		switch (monthStr) {
			case "1": case "01": returnMonth = "Jan"; break; case "2": case "02": returnMonth = "Feb"; break;
			case "3": case "03": returnMonth = "Mar"; break; case "4": case "04": returnMonth = "Apr"; break;
			case "5": case "05": returnMonth = "May"; break; case "6": case "06": returnMonth = "Jun"; break;
			case "7": case "07": returnMonth = "Jul"; break; case "8": case "08": returnMonth = "Aug"; break;
			case "9": case "09": returnMonth = "Sep"; break; case "10": returnMonth = "Oct"; break;
			case "11": returnMonth = "Nov"; break; case "12": returnMonth = "Dec"; break;
		}
		return returnMonth;
	}

	if ($scope.haveAsvabTestScores) {
		// Set test full gender title
		$scope.genderFull = $scope.gender == "f" ? "Female" : "Male";
		// Set test date
		$scope.testDate.year = $scope.scoreSummary.controlInformation.administeredYear;
		$scope.testDate.day = $scope.scoreSummary.controlInformation.administeredDay;
		$scope.testDate.month = $scope.getAbbrMonth($scope.scoreSummary.controlInformation.administeredMonth);
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

cepApp.controller('FindYourInterestSummaryManualController', [ 
	'$scope', 'FYIScoreService', 'FYIRestFactory', '$rootScope', '$window', '$location', '$anchorScroll', '$route', '$templateCache', 'MediaCenterService', 'modalService', 'youTubeModalService', 'cepManualInput', 'asvabScore', 'ssoUrl', 
	function($scope, FYIScoreService, FYIRestFactory, $rootScope, $window, $location, $anchorScroll, $route, $templateCache, MediaCenterService, modalService, youTubeModalService, cepManualInput, asvabScore, ssoUrl) {
	
	$scope.scoreSummary;
	$scope.asvabScore = asvabScore.data; // manually entered score data

	$scope.genderFull = "";
	$scope.testDate = {
		"month" : "",
		"day" : "",
		"year" : ""
	}

    // Retrieve scores from session.
	$scope.verbalScore = Number($scope.asvabScore[$scope.asvabScore.length-1].verbalScore);
	$scope.mathScore = Number($scope.asvabScore[$scope.asvabScore.length-1].mathScore);
	$scope.scienceScore = Number($scope.asvabScore[$scope.asvabScore.length-1].scienceScore);
	$scope.haveManualAsvabTestScores = $window.sessionStorage.getItem('manualScores') == "true";
	
	// set hs grade level
	$scope.hsGrade = $scope.asvabScore[$scope.asvabScore.length-1].grade; //$scope.asvabScore.hsGrade;
	// Set test gender (full) title

	$scope.getGenderFull = function() {
		var genderCode = $scope.asvabScore[$scope.asvabScore.length-1].gender;
		if (genderCode != null)
			genderCode = genderCode.toLowerCase();
		switch (genderCode) {
			case "f":
				genderCode = "Female";
				break;
			case "m":
				genderCode = "Male";
				break;
			default:
				genderCode = null;
				break;
		}
		return genderCode;
	}
	
	$scope.gender  = $scope.getGenderFull();

	$scope.isGrade = function(){
		if($scope.hsGrade != null)
			return true;
		return false;
	}
	
	$scope.isGender = function(){
		if($scope.gender != null)
			return true;
		return false;
	}
	
	$scope.isDate = function(){
		if($scope.testDate.month != null && $scope.testDate.day != null && $scope.testDate.year != null)
			return true;
		return false;
	}
	
	$scope.isLocation = function(){
		if($scope.testLocation != null)
			return true;
		return false;
	}
		
	
	// Set test date
	$scope.testDate.year = $scope.asvabScore[$scope.asvabScore.length-1].testYear; //$scope.asvabScore.testYear;
	$scope.testDate.day = $scope.asvabScore[$scope.asvabScore.length-1].testDay; //$scope.asvabScore.testDay;
	$scope.testDate.month = $scope.asvabScore[$scope.asvabScore.length-1].testMonth; //$scope.asvabScore.testMonth;
	$scope.testLocation = $scope.asvabScore[$scope.asvabScore.length-1].testLocation;
	
	$scope.cepManualInput = function(cepManualMode) {
		var enterMode = cepManualMode == 'enter' ? true : false;
		var retrieveMode = cepManualMode == 'retrieve' ? true : false;
		cepManualInput.show({}, {enter: enterMode, retrieve: retrieveMode}).then(function(result) {
			//console.log(result);
			if (result) {
				if (result.hasOwnProperty('aFQTRawSSComposites')) {
					// Retrieved asvab scores
					$scope.scoreSummary = result;
					console.log("TODO: nav to summary with new score data with access code param???");
				} else {
					// Entered asvab scores
					$scope.haveManualAsvabTestScores = true;
					$scope.asvabScore.push({verbalScore : result.verbalScore, mathScore : result.mathScore, scienceScore : result.scienceScore});
				}
				
				// Update session storage
				$window.sessionStorage.setItem('manualScores', $scope.haveManualAsvabTestScores);
				$window.sessionStorage.setItem('sV', $scope.asvabScore.verbalScore);
				$window.sessionStorage.setItem('sM', $scope.asvabScore.mathScore);
				$window.sessionStorage.setItem('sS', $scope.asvabScore.scienceScore);

				// Update book stacks
				//$scope.vBooks.score = $scope.getScore('verbal');
				//$scope.mBooks.score = $scope.getScore('math');
				//$scope.sBooks.score = $scope.getScore('science');
				//$scope.scoreSorting();
			}
		});
	}

	$scope.moreInfo = function(area) {
		var modalOptions;
		switch (area) {
		case "ces":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<p>Consider these scores when exploring careers in the OCCU-Find. Use your highest score to review occupations based on the corresponding skill importance rating.</p>' + 
					'<p>ASVAB test scores are combined to yield three Career Exploration Scores: Verbal Skills, Math Skills, and Science/Technical Skills. Each of these scores is made up of a combination of some of the individual ASVAB tests.</p>' + 
					'<p><strong>Verbal Skills</strong> - estimates your potential for verbal activities; derived from the Word Knowledge (WK) and Paragraph Comprehension (PC) tests.</p>' + 
					'<p><strong>Math Skills</strong> - estimates your potential for mathematical activities; derived from the Mathematics Knowledge (MK) and Arithmetic Reasoning (AR) tests.</p>' + 
					'<p><strong>Science/Technical Skills</strong> - estimates your potential for science and technical activities, derived from the General Science (GS), Mechanical Comprehension (MC), and Electronics Information (EI) tests.</p>' + 
					'<p>These three Career Exploration Scores are good indicators of your potential for success in further education, training, and in occupations. They focus on skills (which are learned and modifiable) rather than on abilities (which are often seen as fixed).</p>'
			};
			break;
		case "cesStandard":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<h4>Standard Scores</h4>' +
					'<p>The standard scores are not like what you\'re used to seeing, where the scores range from 0 to 100 with the majority of students scoring between 70 and 100. With ASVAB standard scores, the majority of students score between 30 and 70. This means that a standard score of 50 is an average score, and a score of 60 would be an above-average score.</p>'
			};
			break;
		case "cesPercent":
			modalOptions = {
				headerText : 'Career Exploration Scores',
				bodyText : '<h4>Percentile Scores</h4>' +
					'<p>Percentile scores indicate how well you did in relation to others in the same grade. For each test and composite score, you receive a same grade/same sex, same grade/opposite sex, and same grade/combined sex percentile score.</p>'
			};
			break;
		case "asvab":
			modalOptions = {
				headerText : 'ASVAB Test Scores',
				bodyText : '<p>Need content or correct link.</p>'
			};
			break;
		case "afqt":
			modalOptions = {
				headerText : 'Military Entrance Score (AFQT)',
				bodyText : '<p>Participation in the ASVAB CEP does not obligate you to talk with military recruiters or consider a military career.</p>' + 
				'<p>The AFQT score is what the Military will use to determine enlistment eligibility.</p>' + 
				'<p>If you are interested in joining the Military it is best to discuss this score with a recruiter.</p>'
			};
			break;
		}
		

		// For ASVAB open CITM Composites Score page in new tab/window not modal win
		if (area == "asvab") {
			//$window.open('http://www.citm.com/CITM/composite-scores', '_blank');
			//alert(ssoUrl + 'composite-scores');
            $window.open(ssoUrl + 'composite-scores', '_blank');
		} else {
			modalService.showModal({
				size : 'md'
			}, modalOptions);
		}
	}

	$scope.scrollTo = function(id) {
		$location.hash(id);
		$anchorScroll();
	}

	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			$('.goToTop').fadeIn();
		} else {
			$('.goToTop').fadeOut();
		}
	})

	$('.goToTop').click(function () {
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	})

} ]);
cepApp.factory('FYIRestFactory', [ '$http', 'appUrl', '$q', 'awsApiFullUrl', function($http, appUrl, $q, awsApiFullUrl) {

	var fyiRestFactory = {};

	fyiRestFactory.getTestQuestions = function() {
		return $http.get(appUrl + 'fyi/test');
	}

	fyiRestFactory.postAnswers = function(answerList) {

		var defer = $q.defer();
		$http({method: 'POST', url: awsApiFullUrl + '/api/fyi/save-responses', data: answerList}).then(function(success) {
			defer.resolve(success);
		}, function(error) {
			defer.reject(error);
		});

		return defer.promise;
	}

	fyiRestFactory.getGenderScore = function(rawScores, gender) {
		return $http.post(appUrl + 'fyi/getGenderScore/' + gender + '/', rawScores);
	}

	fyiRestFactory.getCombinedScore = function(rawScores) {
		return $http.post(appUrl + 'fyi/getCombinedScore', rawScores);
	}

	fyiRestFactory.getNumberTestTaken = function(userId) {
		return $http.get(appUrl + 'fyi/NumberTestTaken/' + userId + '/');
	}

	fyiRestFactory.getCombinedAndGenderScores = function(userId) {
		return $http.get(appUrl + 'fyi/CombinedAndGenderScores/' + userId + '/');
	}

	fyiRestFactory.postUserScores = function(scoreObject) {
		return $http.post(appUrl + 'fyi/PostScore', scoreObject);
	}

	fyiRestFactory.getUserInteresteCodes = function(userId) {
		return $http.get(appUrl + 'fyi/UserInterestCodes/' + userId + '/');
	}

	fyiRestFactory.insertProfile = function(fyiObject) {
		return $http.post(appUrl + 'fyi/InsertProfile', fyiObject);
	}

	fyiRestFactory.getProfileExist = function(fyiObject) {
		return $http.post(appUrl + 'fyi/CheckProfileExists', fyiObject);
	}

	fyiRestFactory.getScoreChoice = function(userId) {
		return $http.get(appUrl + 'fyi/ScoreChoice/' + userId + '/');
	}

	fyiRestFactory.getTiedSelection = function(userId) {
		var defer = $q.defer();
		$http.get(appUrl + 'fyi/get-tied-selection/' + userId + '/').then(function(success){
			defer.resolve(success);
		}, function(error){
			defer.reject(error);
		});
		return defer.promise;
	}

	fyiRestFactory.insertTiedSelection = function(tiedObject) {
		var deferred = $q.defer();
		$http.post(appUrl + 'fyi/insert-tied-selection', tiedObject).then(function(success) {
			deferred.resolve(success);
		}, function(error){
			console.log(error);
			deferred.reject(error);
		});
		return deferred.promise;
	}
	
	fyiRestFactory.getTestScoreWithAccessCode = function(accessCode) {
		return $http.get(appUrl + 'testScore/get-test-score-access-code/' + accessCode + '/');
	}
	
	fyiRestFactory.getTestScoreWithUserId = function(userId) {
		return $http.get(appUrl + 'testScore/get-test-score-user-id/' + userId + '/');
	}

	return fyiRestFactory;

} ]);
cepApp.service('FYIScoreService', [ '$window', function($window) {

	var topCombinedInterestCodes
	this.getTopCombinedInterestCodes = function() {
		if (topCombinedInterestCodes == undefined) {
			if (typeof (Storage) !== "undefined") {
				if ($window.sessionStorage.getItem("topCombinedInterestCodes") === null || $window.sessionStorage.getItem("topCombinedInterestCodes") == 'undefined') {
					// TODO: redirect or get value from database
				} else {
					topCombinedInterestCodes = angular.fromJson($window.sessionStorage.getItem('topCombinedInterestCodes'));
				}
				return topCombinedInterestCodes;
			} else {
				// TODO: redirect or get value
			}
		} else {
			return topCombinedInterestCodes;
		}
	}

	this.setTopCombinedInterestCodes = function(topInterestCodes) {
		topCombinedInterestCodes = topInterestCodes;

		if (topInterestCodes === undefined) {
			$window.sessionStorage.removeItem("topCombinedInterestCodes");
		}

		if (typeof (Storage) !== "undefined" && topInterestCodes !== undefined) {
			$window.sessionStorage.setItem('topCombinedInterestCodes', JSON.stringify(topInterestCodes));
		}
	}

	/**
	 * Setup tied score object for combined tied score.
	 */
	var combinedTiedScoreObject;

	this.setCombinedTiedScoreObject = function(object) {

		combinedTiedScoreObject = object;

		if (object === undefined) {
			$window.sessionStorage.removeItem("combinedTiedScoreObject");
		}
		if (typeof (Storage) !== "undefined" && object !== undefined) {
			$window.sessionStorage.setItem('combinedTiedScoreObject', JSON.stringify(combinedTiedScoreObject));
			combinedTiedScoreObject = angular.fromJson($window.sessionStorage.getItem('combinedTiedScoreObject'));
		}
	}

	this.getCombinedTiedScoreObject = function() {
		if (combinedTiedScoreObject == undefined) {
			if (typeof (Storage) !== "undefined") {
				if ($window.sessionStorage.getItem("combinedTiedScoreObject") === null || $window.sessionStorage.getItem("combinedTiedScoreObject") == 'undefined') {
					// TODO: redirect or get value from database
				} else {
					combinedTiedScoreObject = angular.fromJson($window.sessionStorage.getItem('combinedTiedScoreObject'));
				}
				return combinedTiedScoreObject;
			} else {
				// TODO: redirect or get value
			}
		} else {
			return combinedTiedScoreObject;
		}
	}

	var topGenderInterestCodes
	this.getTopGenderInterestCodes = function() {
		if (topGenderInterestCodes == undefined) {
			if (typeof (Storage) !== "undefined") {
				if ($window.sessionStorage.getItem("topGenderInterestCodes") === null || $window.sessionStorage.getItem("topGenderInterestCodes") == 'undefined') {
					// TODO: redirect or get value from database
				} else {
					topGenderInterestCodes = angular.fromJson($window.sessionStorage.getItem('topGenderInterestCodes'));
				}
				return topGenderInterestCodes;
			} else {
				// TODO: redirect or get value
			}
		} else {
			return topGenderInterestCodes;
		}
	}

	this.setTopGenderInterestCodes = function(topInterestCodes) {
		topGenderInterestCodes = topInterestCodes;

		if (topInterestCodes === undefined) {
			$window.sessionStorage.removeItem("topGenderInterestCodes");
		}
		if (typeof (Storage) !== "undefined" && topInterestCodes !== undefined) {
			$window.sessionStorage.setItem('topGenderInterestCodes', JSON.stringify(topInterestCodes));
		}
	}

	/**
	 * Setup tied score object for gender tied score.
	 */
	var genderTiedScoreObject;

	this.setGenderTiedScoreObject = function(object) {

		genderTiedScoreObject = object;

		if (object === undefined) {
			$window.sessionStorage.removeItem("genderTiedScoreObject");
		}
		if (typeof (Storage) !== "undefined" && object !== undefined) {
			$window.sessionStorage.setItem('genderTiedScoreObject', JSON.stringify(genderTiedScoreObject));
			genderTiedScoreObject = angular.fromJson($window.sessionStorage.getItem('genderTiedScoreObject'));
		}
	}

	this.getGenderTiedScoreObject = function() {
		if (genderTiedScoreObject == undefined) {
			if (typeof (Storage) !== "undefined") {
				if ($window.sessionStorage.getItem("genderTiedScoreObject") === null || $window.sessionStorage.getItem("genderTiedScoreObject") == 'undefined') {
					// TODO: redirect or get value from database
				} else {
					genderTiedScoreObject = angular.fromJson($window.sessionStorage.getItem('genderTiedScoreObject'));
				}
				return genderTiedScoreObject;
			} else {
				// TODO: redirect or get value
			}
		} else {
			return genderTiedScoreObject;
		}
	}

	/**
	 * Used to POST test information to database.
	 */
	var fyiObject;

	this.setFyiObject = function(object) {
		fyiObject = object;
	}
	this.getFyiObject = function() {
		return fyiObject;
	}

	// holds questions and answers
	var questionAnswerList;
	var interestLikes = 0;
	var interestDislikes = 0;
	var interestIndifferents = 0;
	var userGender = 'M';
	var genderScore;
	var combinedScore;

	var interestCodeRawScore = {
		rRawScore : 0,
		iRawScore : 0,
		aRawScore : 0,
		sRawScore : 0,
		eRawScore : 0,
		cRawScore : 0
	};

	// reset values
	this.resetValues = function() {
		interestLikes = 0;
		interestDislikes = 0;
		interestIndifferents = 0;
		interestCodeRawScore = {
			rRawScore : 0,
			iRawScore : 0,
			aRawScore : 0,
			sRawScore : 0,
			eRawScore : 0,
			cRawScore : 0
		};
	}

	this.setRawScores = function(rawScores) {
		interestCodeRawScore = rawScores;
	}

	this.getCombinedScore = function() {
		return combinedScore;
	}

	this.getGenderScore = function() {
		return genderScore;
	}

	this.getInterestCodeRawScore = function() {
		return interestCodeRawScore;
	}

	this.getInterestLikes = function() {
		return interestLikes;
	}

	this.getInterestDislikes = function() {
		return interestDislikes;
	}

	this.getInterestIndifferents = function() {
		return interestIndifferents;
	}

	/*
	 * Calculate raw score for each interest code (R, I, A, S, E, C). Like = 2
	 * Indifferent = 1 Dislike = 0
	 * 
	 * Calculate the total number of likes, indifferents and dislikes for each
	 * interest code. This is not being used.
	 * 
	 * Calculate the overall total number of likes, indifferents and dislikes.
	 * 
	 * 
	 * What is tied score and how is it calculated?
	 * 
	 * How is Combined Score calculated?
	 * 
	 * How is Gender Score calculated?
	 */
	this.calculateRawScore = function() {

		var len = this.questionAnswerList.length;

		// loop through answer object
		for (var i = 0; i < len; i++) {
			var interestCd = this.questionAnswerList[i].interestCd;
			var answer = this.questionAnswerList[i].answer;

			if (answer !== undefined && answer == 2) {
				// track likes
				interestLikes++;

				// add 2 to raw score for likes per interest
				// code
				switch (interestCd) {
				case "R":
					interestCodeRawScore.rRawScore = interestCodeRawScore.rRawScore + 2;
					break;
				case "I":
					interestCodeRawScore.iRawScore = interestCodeRawScore.iRawScore + 2;
					break;
				case "A":
					interestCodeRawScore.aRawScore = interestCodeRawScore.aRawScore + 2;
					break;
				case "S":
					interestCodeRawScore.sRawScore = interestCodeRawScore.sRawScore + 2;
					break;
				case "E":
					interestCodeRawScore.eRawScore = interestCodeRawScore.eRawScore + 2;
					break;
				case "C":
					interestCodeRawScore.cRawScore = interestCodeRawScore.cRawScore + 2;
					break;
				}

			} else if (answer !== undefined && answer == 1) {
				// track indifferents
				interestIndifferents++;

				// add 1 to raw score for dislikes per interest
				// code
				switch (interestCd) {
				case "R":
					interestCodeRawScore.rRawScore++;
					break;
				case "I":
					interestCodeRawScore.iRawScore++;
					break;
				case "A":
					interestCodeRawScore.aRawScore++;
					break;
				case "S":
					interestCodeRawScore.sRawScore++;
					break;
				case "E":
					interestCodeRawScore.eRawScore++;
					break;
				case "C":
					interestCodeRawScore.cRawScore++;
					break;
				}
			} else {
				// track dislikes
				interestDislikes++;
			}
		}
	}

	/**
	 * Returns sorted combined score list in desc order.
	 */
	this.getSortedCombinedScore = function(scores) {
		var combinedScoreSort = [];
		for (var i = 0; i < scores.length; i++) {
			combinedScoreSort.push(scores[i]);
		}
		combinedScoreSort.sort(function(a, b) {
			return b.combinedScore - a.combinedScore
		});
		return combinedScoreSort;
	}

	/**
	 * Returns sorted gender score list in desc order.
	 */
	this.getSortedGenderScore = function(scores) {

		var genderScoreSort = [];
		for (var i = 0; i < scores.length; i++) {
			genderScoreSort.push(scores[i]);
		}
		genderScoreSort.sort(function(a, b) {
			return b.genderScore - a.genderScore
		});
		return genderScoreSort;
	}

	/**
	 * Check to see if there are tied scores that requires the user's input.
	 */
	this.isTiedScore = function(selectedScoreList, scoreType) {
		var selectedScoreListCopy = angular.copy(selectedScoreList);
		var numberOfSlotsLeft = 3;
		var len = selectedScoreListCopy.length;
		var tiedScoreList = [];

		for (var i = 0; i < len; i = i + tiedScoreList.length) {
			tiedScoreList = [];

			for (var k = 0 + i; k < len; k++) {

				if (scoreType == 'gender') {
					if (selectedScoreListCopy[i].genderScore == selectedScoreListCopy[k].genderScore) {
						tiedScoreList.push(selectedScoreListCopy[k]);
					}
				} else {
					if (selectedScoreListCopy[i].combinedScore == selectedScoreListCopy[k].combinedScore) {
						tiedScoreList.push(selectedScoreListCopy[k]);
					}
				}
			}

			// if number of slots left is less than 0 then user needs to choose
			// tied score
			if (numberOfSlotsLeft - tiedScoreList.length < 0) {

				// setup checkbox input
				for (var i = 0; i < len; i++) {
					for (var k = 0; k < tiedScoreList.length; k++) {
						if (selectedScoreListCopy[i].interestCd == tiedScoreList[k].interestCd) {

							selectedScoreListCopy[i].checkbox = true;
						}

					}
				}

				var tiedScoreObject = {
					numSlotsLeft : numberOfSlotsLeft,
					scoreList : selectedScoreListCopy,
					scoreType : scoreType,
					numTiedScore : tiedScoreList.length
				};

				if (scoreType == 'gender') {
					this.setGenderTiedScoreObject(tiedScoreObject);
				} else {
					this.setCombinedTiedScoreObject(tiedScoreObject);
				}

				return true;
			}

			// track number of slots left
			numberOfSlotsLeft = numberOfSlotsLeft - tiedScoreList.length;

			// if number of slots is 0 then dont need input from user
			if (numberOfSlotsLeft == 0) {
				return false;
			}
		}

		// if number of slots left is less than zero then needs input from user
		// return numberOfSlotsLeft
	}

	/**
	 * Track if tied link is clicked.
	 */
	var isTiedLinkClicked = false;
	this.setTiedLinkClicked = function(isTiedLinkClicked) {
		this.isTiedLinkClicked = isTiedLinkClicked;
	}
	this.getTiedLinkClicked = function() {
		return this.isTiedLinkClicked;
	}
} ]);
cepApp.controller('GreenStemBrightOccupationController', [ '$scope', '$rootScope', '$route', '$cookieStore', '$location', 'careerClusterService', 'AuthenticationService', 'occupationResults', 'OccufindSearchService', function($scope, $rootScope, $route, $cookieStore, $location, careerClusterService, AuthenticationService, occupationResults, OccufindSearchService) {

	
	$scope.category = $route.current.params.category;
	$scope.occupationResults = occupationResults.data;
	$scope.skillSelected;
	
	/*$scope.startsWith = function (actual, expected) {
	    var lowerStr = (actual + "").toLowerCase();
	    return lowerStr.indexOf(expected.toLowerCase()) === 0;
	}*/
	if (!String.prototype.startsWith) {
	    String.prototype.startsWith = function(searchString, position){
	      position = position || 0;
	      return this.substr(position, searchString.length) === searchString;
	  };
	}
	
	$scope.hot = function(){
		if ($route.current.params.category =='hot')
			return true;
		else
			return false;
	} 

	$scope.filterAlphabet = function(letter) {
		var masterCopy = angular.copy(occupationResults.data);
		var copy = [];

		for (var i = 0; i < masterCopy.length; i++) {
			if (masterCopy[i].occupationTitle.startsWith(letter)) {
				copy.push(masterCopy[i]);
			}
		}
		$scope.occupationResults = copy;
		$scope.totalItems = $scope.occupationResults.length;
		$scope.currentPage = 1;
		$scope.maxSize = 4;
		$scope.itemsPerPage = 20;
	}


	/**
	 * Pagination
	 */
	$scope.totalItems = $scope.occupationResults.length;
	$scope.currentPage = 1;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 20;
	
	/**
	 * Tracks how many selections are made for skill selection section. Limit user selection to one.
	 */
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.checkChangedLimitOne = function(item) {
		if (item.selected)
			$scope.skillChecked++;
		else
			$scope.skillChecked--;
	}
	
	// for skill selection checkbox 
	$scope.skillSelectionOptions = [ {
		name : 'Verbal',
		value : 'verbalSkill',
		selected : false
	}, {
		name : 'Math',
		value : 'mathSkill',
		selected : false
	}, {
		name : 'Science/Technical',
		value : 'sciTechSkill',
		selected : false
	} ];

	$scope.selectedOccupation = function(onetSoc, title, stem, bright, green, hot, interestCdOne, interestCdTwo, interestCdThree) {
		OccufindSearchService.selectedOnetSoc.push(onetSoc);
		OccufindSearchService.selectedOcupationTitle = title;
		OccufindSearchService.occupationGreen = green;
		OccufindSearchService.occupationStem = stem;
		OccufindSearchService.occupationBright = bright;
		OccufindSearchService.occupationHot = hot;
		OccufindSearchService.occupationInterestOne = interestCdOne;
		OccufindSearchService.occupationInterestTwo = interestCdTwo;
		OccufindSearchService.occupationInterestThree = interestCdThree;
		$location.url('/occufind-occupation-details/' + onetSoc);
	};

	/**
	 * Search feature
	 */
	$scope.stemOccupationFlag;
	$scope.brightOccupationFlag;
	$scope.greenOccupationFlag;
	$scope.hotOccupationFlag;

	$scope.interestCodes = [ {
		name : 'Realistic',
		value : 'R',
		selected : false
	}, {
		name : 'Investigative',
		value : 'I',
		selected : false
	}, {
		name : 'Artistic',
		value : 'A',
		selected : false
	}, {
		name : 'Social',
		value : 'S',
		selected : false
	}, {
		name : 'Enterprising',
		value : 'E',
		selected : false
	}, {
		name : 'Conventional',
		value : 'C',
		selected : false
	}, ];

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 2;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Search button sets up the search service.
	 */
	$scope.occufindSearch = function() {

		OccufindSearchService.userSearch = {
			userSearch : {
				interestCodeOne : undefined,
				interestCodeTwo : undefined,
				brightOccupationFlag : false,
				greenOccupationFlag : false,
				stemOccupationFlag : false,
				hotOccupationFlag : false,
				userSearchString : undefined
			}
		}

		// push skill selection value service object
		var isSkillSelected = false;
		for (var i = 0; i < $scope.skillSelectionOptions.length; i++) {

			if ($scope.skillSelectionOptions[i].selected == true) {
				isSkillSelected = true;
				OccufindSearchService.skillSelected = $scope.skillSelectionOptions[i].value;
			}
		}
		if (!isSkillSelected) {
			OccufindSearchService.skillSelected = undefined;
		}

		// push all interest code selection(s) to array
		var arraySelectedInterestCd = [];
		for (var i = 0; i < $scope.interestCodes.length; i++) {

			if ($scope.interestCodes[i].selected == true) {
				arraySelectedInterestCd.push($scope.interestCodes[i].value);
			}
		}

		// validate selection
		if (arraySelectedInterestCd < 1) {
			/*alert("Please select interest code(s)");
			return false;*/
		} else {

			// assign interest code selection(s) to occufind search service
			OccufindSearchService.userSearch.interestCodeOne = arraySelectedInterestCd[0];
			if (arraySelectedInterestCd.length == 2) {
				OccufindSearchService.userSearch.interestCodeTwo = arraySelectedInterestCd[1];
			}
		}

		// assign user's search selections to occufind search object
		OccufindSearchService.userSearch.brightOccupationFlag = $scope.brightOccupationFlag;
		OccufindSearchService.userSearch.greenOccupationFlag = $scope.greenOccupationFlag;
		OccufindSearchService.userSearch.stemOccupationFlag = $scope.stemOccupationFlag;
		OccufindSearchService.userSearch.userSearchString = $scope.searchString;
		OccufindSearchService.userSearch.hotOccupationFlag = $scope.hotOccupationFlag;

		$rootScope.globals.userSearch = OccufindSearchService.userSearch;
		$cookieStore.put('globals', $rootScope.globals);

		$location.path('/occufind-occupation-search-results');

	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

	$scope.logout = function() {
		AuthenticationService.ClearCredentials();
	};
} ]);


cepApp.factory('GreenStemBrightRestFactory', [ '$http', 'appUrl', function($http, appUrl) {

	var greenStemBrightRestFactory = {};

	greenStemBrightRestFactory.getBrightOutlook = function(interestCodeOne, interestCodeTwo, interestCodeThree) {
		return $http.get(appUrl + 'occufind/DashboardBrightOutlook/' + interestCodeOne + '/' + interestCodeTwo + '/' + interestCodeThree + '/');
	}

	greenStemBrightRestFactory.getStemCareers = function(interestCodeOne, interestCodeTwo, interestCodeThree) {
		return $http.get(appUrl + 'occufind/DashboardStemCareers/' + interestCodeOne + '/' + interestCodeTwo + '/' + interestCodeThree + '/');
	}
	
	greenStemBrightRestFactory.getGreenCareers = function(interestCodeOne, interestCodeTwo, interestCodeThree) {
		return $http.get(appUrl + 'occufind/DashboardGreenCareers/' + interestCodeOne + '/' + interestCodeTwo + '/' + interestCodeThree + '/');
	}
	
	greenStemBrightRestFactory.getAllCareers = function() {
		return $http.get(appUrl + 'occufind/ViewAllOccupations/');
	}
	
	greenStemBrightRestFactory.getMilitaryCareers = function() {
		return $http.get(appUrl + 'occufind/ViewMilitaryOccupations/');
	}

	greenStemBrightRestFactory.getHotCareers = function() {
		return $http.get(appUrl + 'occufind/ViewHotOccupations/');
	}
	
	return greenStemBrightRestFactory;

} ]);
cepApp.controller('HomeController',
	['$route', '$rootScope', '$scope', '$location', '$http', '$q', '$interval', 'AuthenticationService', 'appUrl', 'LoginModalService', 'mediaCenterList', 'learnMoreModalService', 'resource', 'youTubeModalService', function($route, $rootScope, $scope, $location, $http, $q, $interval,
			AuthenticationService, appUrl, LoginModalService, mediaCenterList, learnMoreModalService, resource, youTubeModalService) {
	
	$scope.resources = resource;
	$scope.mediaCenterList = mediaCenterList;
	/**
	 * Login Modal Service
	 */
	$scope.showLoginModal = function(){
		LoginModalService.show({},{});
	}

	/**
	 * Learn more modal service
	 */
	$scope.showLearnMoreModal = function() {
		learnMoreModalService.show({}, {});
	}

	angular.element(document).ready(function() {

		if (typeof slideShowTimer !== 'undefined') {
			$interval.cancel(slideShowTimer);
		}

		$('#slides .career').hide().eq(0).addClass('active').show();

		slideShowTimer = $interval(function() {
			rotateCareers();
		}, 5000);

		function rotateCareers() {
			var $current = $('#slides .career.active');
			var $next = $current.next();

			if (!$next.length)
				$next = $('#slides .career').eq(0);

			$current.removeClass('active').fadeOut();
			$next.addClass('active').fadeIn();
		}
		;

	});

		$scope.userLogin = true;
		$scope.loginAccessCodeLabel = '';
		var LOGIN_BASE_URL = '/CEP/rest/' + 'applicationAccess/';

		/**
		 * 
		 */
		$scope.clearLoginAccessCodeLabel = function() {
			$scope.loginAccessCodeLabel = '';
		};

		$scope.clearLoginEmailLabel = function() {
			$scope.loginEmailLabel = '';
		};

		$scope.clearRegistrationLabel = function() {
			$scope.registrationLabel = '';
		};

		/**
		 * AccessCodeLogin - When Access Code Login button pressed
		 * in Login Modal!
		 * 
		 * 
		 */
		$scope.AccessCodeLogin = function() {

			// Login Modal
			var data = {
				email : "",
				accessCode : $scope.accessCode,
				password : "",
				resetKey : ""
			};

			var url = LOGIN_BASE_URL + 'loginAccessCode';

			$http.post(url, data)
				.then(
					function(response) {
						var userData = response.data;

						// If success then
						if (userData.statusNumber == 0) {
							// Set the cookie and global
							// values
							AuthenticationService
								.SetCredentials(
										'null',
										data.accessCode,
										userData.user_id,
										userData.role,
										userData.schoolCode,
										userData.schoolName,
										userData.city,
										userData.state,
										userData.zipCode,
										userData.mepsId,
										userData.mepsName)
							// Close Dialog
							$("#LoginClose").click();
							// Set the location to dashboard
							$location.path('/dashboard');
						} else { // Otherwise set error
							// flag
							$scope.loginAccessCodeLabel = "No Access Code Found";
						}
					}, function(response) {
				          var repoData  = response.data || "Request failed";
				          console.log(repoData); 
				      });
		};
       /**
        * EmailLogin - Login utilizing email and password.
        * 
        * 
        * 
        */
		$scope.EmailLogin = function() {
			var data = {
				email : $scope.email,
				accessCode : "",
				password : $scope.password,
				resetKey : ""
			};

			$scope.password = '';

			//
			var LOGIN_BASE_URL = 'rest/applicationAccess/';
			var url = LOGIN_BASE_URL + 'loginEmail';
			
			$http.post(url, data)
			.then(
				function(response) {
					var userData = response.data;
					if ( userData.statusNumber == 1002 || userData.statusNumber == 1008 ) {
						$scope.loginEmailLabel ='Username / Password combination is invalid';
						
						return;
					} else if ( userData.statusNumber == 1010 ) {
						$scope.loginEmailLabel ='Username has been locked, try resetting your password.';
						return;
					} else if ( userData.statusNumber != 0  ){
						var serverErrorMessage = userData.status.substring(userData.status.indexOf(":") + 1 , userData.status.length + 1);
						$scope.loginEmailLabel =serverErrorMessage;
						return;
					}
					AuthenticationService
					.SetCredentials(
							data.email,
							'null',
							userData.user_id,
							userData.role,
							userData.schoolCode,
							userData.schoolName,
							userData.city,
							userData.state,
							userData.zipCode,
							userData.mepsId,
							userData.mepsName);
					// Close Dialog
					$("#LoginClose").click();
					// Set the location to dashboard
					$location.path('/dashboard');
					
				});
		};

		// Closes Login and pops up Registration
		$scope.PopUpRegistration = function() {
			$("#LoginClose").click();
			$("#openRegistrationButton").click();
		};

		// @RequestMapping(value = "/applicationAccess/register",
		// method = RequestMethod.POST)
		$scope.RegisterStudent = function() {
			$scope.registerAccessCode

			// Register Modal
			var data = {
				email : $scope.registerEmail, // document.getElementById("registerEmail").value,
				accessCode : $scope.registerAccessCode, // document.getElementById("registerAccessCode").value,
				password : "",
				resetKey : ""
			};

			var pass1 = $scope.registerPassword1;
			var pass2 = $scope.registerPassword2;

			if (pass1.length <= 7) {
				$scope.registrationLabel = "Password too short!";
				return;
			}
			
			if (pass1 != pass2) {
				$scope.registrationLabel = "Passwords don't match!";
				return;
			}

			var url = LOGIN_BASE_URL + 'register';

			var temp = $scope.crypt(
				pass1,
				function(hashRet) {
					data.password = hashRet;
					$http.post(url, data)
						.then(
								function(response) {
									$scope.registrationLoginStatus = response.data;
									// Success ..
									// Open Login
									if ($scope.registrationLoginStatus.statusNumber == 0) {
										// Close
										// Register
										$("#RegistrationClose").click();
										// Open
										// Login
										$(
												"#cepLogin")
												.click();
									} else { // Error
												// ..
												// update
												// registration
												// Label
										$scope.registrationLabel = $scope.registrationLoginStatus.status
									}
								});
				});
		};

		// Forgot your password
		$scope.RequestResetPassword = function() {

			var data = {
				email : document.getElementById("resetEmail").value, // document.getElementById("registerEmail").value,
				accessCode : "", // document.getElementById("registerAccessCode").value,
				password : "",
				resetKey : ""
			};

			var url = LOGIN_BASE_URL + 'passwordResetRequest';

			$http.post(url, data).then(function(response) {
				$scope.registrationLoginStatus = response.data;
			});

		};

		$scope.ResetPassword = function() {
			data.email = '';
			data.accessCode = '';
			data.password = '';
			data.resetKey = 'aaa';

			var url = LOGIN_BASE_URL + 'passwordReset';

			$http.post(url, data).then(function(response) {
				$scope.registrationLoginStatus = response.data;
			});

		};


		// *** Begin ***
		var id;
		var bcrypt = new bCrypt();

		var begin = '';
		function result(hashRet) {
			console.log(hashRet);
			$scope.hash = hashRet;
		}

		$scope.ckpassword = function(password, hash,
				callbackFunction) {
			try {

				bcrypt.checkpw(password, hash, callbackFunction,
						null);
			} catch (err) {
				alert(err);
				return;
			}
		};

		$scope.crypt = function(password, callbackFunction) {
			var salt = '';
			var rounds = 10;

			// Auto generate the salt
			try {
				salt = bcrypt.gensalt(rounds);
			} catch (err) {
				alert(err);
				return;
			}

			try {
				// bcrypt.hashpw( password , salt, result , null );
				bcrypt.hashpw(password, salt, callbackFunction,
						null);
			} catch (err) {
				alert(err);
				return;
			}
		}
		// ***  End  ***
		
		$scope.showYouTubeModal = function(videoName) {
			youTubeModalService.show({
				size : 'lg'
			}, {
				videoName : videoName
			});
		}
	}]);
cepApp.service('LoginModalService', [ '$uibModal', '$location', '$http', '$window','$timeout', 'AuthenticationService','appUrl','modalService', 'ssoUrl', function($uibModal, $location, $http, $window, $timeout, AuthenticationService, appUrl, modalService, ssoUrl) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/login/page-partials/login-modal2.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};
		 
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', 'mergeAccountModalService', 'learnMoreModalService', function($scope, $uibModalInstance, mergeAccountModalService, learnMoreModalService) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.isRegister = false;
				$scope.isForgotPassword = false;
				$scope.MergeAccounts = false;
				$scope.userLogin = true;
				$scope.loginAccessCodeLabel = '';
				var LOGIN_BASE_URL = appUrl + 'applicationAccess/';

				
				
				$scope.goToGeneralHelp = function(){
					window.open('general-help#general', '_self', 'resizable,location,menubar,toolbar,scrollbars,status');
					$uibModalInstance.dismiss('cancel');
				}
				
				/**
				 * 
				 */
				$scope.clearLoginAccessCodeLabel = function() {
					$scope.loginAccessCodeLabel = '';
				};

				$scope.clearLoginEmailLabel = function() {
					$scope.loginEmailLabel = '';
				};

				$scope.clearRegistrationLabel = function() {
					$scope.registrationLabel = '';
				};
				
				
				
				
				
				/**
				 * AccessCodeLogin - When Access Code Login button pressed in
				 * Login Modal!
				 * 
				 * 
				 */
				$scope.AccessCodeLogin = function() {

					// Login Modal
					var data = {
						email : "",
						accessCode : $scope.accessCode,
						password : "",
						resetKey : ""
					};

					var url = LOGIN_BASE_URL + 'loginAccessCode';

					$http.post(url, data).then(function(response) {
						var userData = response.data;

						// If success then
						if (userData.statusNumber == 0) {
							// Set the cookie and global
							// values
							AuthenticationService.SetCredentials('null', data.accessCode, userData.user_id, userData.role,
									userData.schoolCode,
									userData.schoolName,
									userData.city,
									userData.state,
									userData.zipCode,
									userData.mepsId,
									userData.mepsName)
							// Close Dialog
							$uibModalInstance.dismiss('cancel');
							// Set the location to dashboard
							//$location.path('/dashboard');
							
							// TODO: login to CITM if user has accesscode.
							$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
						} else { // Otherwise set error
							// flag
							$scope.loginAccessCodeLabel = "No Access Code Found";
					         $timeout(function () { $scope.loginAccessCodeLabel = ''; }, 5000);   
							
						}
						
						
					}, function(response) {
						var repoData = response.data || "Request failed";
						console.log(repoData);
					});
				};
				/**
				 * EmailLogin - Login utilizing email and password.
				 * 
				 * 
				 * 
				 */
				$scope.EmailLogin = function() {
					var data = {
						email : $scope.email,
						accessCode : "",
						password : $scope.password,
						resetKey : ""
					};

					$scope.password = '';

					var url = LOGIN_BASE_URL + 'loginEmail';

					$http.post(url, data).then(function(response) {
						var userData = response.data;
						if (userData.statusNumber == 1002 || userData.statusNumber == 1008) {
							$scope.loginEmailLabel = 'Username / Password combination is invalid';
					        $timeout(function () { $scope.loginEmailLabel = ''; }, 5000);   
							return;
						} else if (userData.statusNumber == 1010) {
							$scope.loginEmailLabel = 'Username has been locked, try resetting your password.';
					        $timeout(function () { $scope.loginEmailLabel = ''; }, 5000);   
							
							return;
						} else if (userData.statusNumber != 0) {
							var serverErrorMessage = userData.status.substring(userData.status.indexOf(":") + 1, userData.status.length + 1);
							$scope.loginEmailLabel = serverErrorMessage;
					        $timeout(function () { $scope.loginEmailLabel = ''; }, 5000);   
							return;
						}
						AuthenticationService.SetCredentials(data.email, 'null', userData.user_id, userData.role,
								userData.schoolCode,
								userData.schoolName,
								userData.city,
								userData.state,
								userData.zipCode,
								userData.mepsId,
								userData.mepsName);
						// Close Dialog
						$uibModalInstance.dismiss('cancel');
						// Set the location to dashboard
						// $location.path('');
						//window.open('dashboard', '_self', 'resizable,location,menubar,toolbar,scrollbars,status');
						
						// Login to CITM.
						$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';

					});
				};

				// @RequestMapping(value = "/applicationAccess/register",
				// method = RequestMethod.POST)
				$scope.RegisterStudent = function() {
					$scope.registerAccessCode

					// Register Modal
					var data = {
						email : $scope.registerEmail, // document.getElementById("registerEmail").value,
						accessCode : $scope.registerAccessCode, // document.getElementById("registerAccessCode").value,
						password : $scope.registerPassword2,
						resetKey : "",
						studentId : $scope.registerStudentId
					};

					var pass1 = $scope.registerPassword1;
					var pass2 = $scope.registerPassword2;
					
					if (pass1.length <= 7) {
						$scope.registrationLabel = "Password too short!";
						return;
					}
					
					if (pass1 != pass2) {
						$scope.registrationLabel = "Passwords don't match!";
						return;
					}

					var url = LOGIN_BASE_URL + 'register';

						$http.post(url, data).then(function(response) {
							$scope.registrationLoginStatus = response.data;
							// Success ..
							var userData = response.data;
							// Open Login
							if ($scope.registrationLoginStatus.statusNumber == 0) {
								// hide registration view which then shows login
								// view
								$scope.isRegister = false;
								// Clear all the data 
								$scope.registerAccessCode = '';
								$scope.registerEmail =  '';
								$scope.registerPassword1 =  '';
								$scope.registerPassword2 =  '';
								$scope.registerStudentId = '';
								
								/*var customModalOptions = {
										headerText : 'Thank you for registering with ASVAB CEP',
										bodyText : 'Thank you for registering with ASVAB CEP, you will recieve an email from us shortly.'
									};
									var customModalDefaults = {};
									modalService.show(customModalDefaults, customModalOptions);*/
							//		$uibModalInstance.dismiss('cancel');
								
								//  Now log in the use ....
								// xxx
								AuthenticationService.SetCredentials($scope.registerEmail , 'null', userData.user_id, userData.role,
										userData.schoolCode,
										userData.schoolName,
										userData.city,
										userData.state,
										userData.zipCode,
										userData.mepsId,
										userData.mepsName);
								// Close Dialog

								$uibModalInstance.dismiss('cancel');
								// Set the location to dashboard
								// $location.path('');
								//window.open('dashboard', '_self', 'resizable,location,menubar,toolbar,scrollbars,status');
								
								// Login to CITM.
								$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
								
								
								
								// Close Dialog
							//	$uibModalInstance.dismiss('cancel');

							} else { // Error
								// ..
								// update
								// registration
								// Label
								if($scope.registrationLoginStatus.statusNumber == 1003) {
									this.email = $scope.registerEmail;
									$scope.showMergeModal();
								} else if ($scope.registrationLoginStatus.statusNumber == 1008) { 
									$scope.registrationLabel = "Email address already registered with a different password "; 
									$scope.resetPassword = true;
									return;
								}else {
									alert($scope.registrationLoginStatus.statusNumber)
									$timeout(function () { $scope.registrationLabel  = ''; }, 5000);   
								}
								
								
							}
						});
				};

				/** MERGE ACCOUNT MODAL **/
				$scope.showMergeModal = function() {
					var modalInstance = mergeAccountModalService.show({
						email : $scope.registerEmail,
						accessCode : $scope.registerAccessCode
					}, {});
					
					modalInstance.then(function (response) {
						if(response.statusNumber == 0) {
							$scope.modalOptions.close();
						}
					});
				};

				// Forgot your password
				$scope.RequestResetPassword = function() {

					var data = {
						email : document.getElementById("resetEmail").value, // document.getElementById("registerEmail").value,
						accessCode : "", // document.getElementById("registerAccessCode").value,
						password : "",
						resetKey : ""
					};

					var url = LOGIN_BASE_URL + 'passwordResetRequest';

					$http.post(url, data).then(function(response) {
						$scope.registrationLoginStatus = response.data;
						
						if ($scope.registrationLoginStatus.statusNumber == 0) {
							// hide registration view which then shows login
							// view
							$scope.isRegister = false;
							// Close Dialog
							$uibModalInstance.dismiss('cancel');
						} else { // Error
							// ..
							// Pop up Modal with error message.
							$scope.forgetLabel = $scope.registrationLoginStatus.status;
							$timeout(function () { $scope.forgetLabel   = ''; }, 5000); 
						}
					});
				};
				
//
//				$scope.MergeAccounts = function() {
//					alert(data.email)
//					
//					//resetKey = $scope.option
//					var data = {
//						email : $scope.email, 
//						accessCode : $scope.accessCode, 
//						password : "",
//						resetKey : ""
//					};
//					//alert(data.email);
//
//					var url = LOGIN_BASE_URL + 'mergeAccountRequest';
//
//					$http.post(url, data).then(function(response) {
//						$scope.mergeAccountStatus = response.data;
//						
//						if ($scope.mergeAccountStatus.statusNumber == 0) {
//							$scope.isMerge = false;
//							$uibModalInstance.dismiss('cancel');
//						} else { // Error
//							$scope.forgetLabel = $scope.mergeAccountStatus.status;
//							$timeout(function () { $scope.forgetLabel   = ''; }, 5000); 
//						}
//					});
//				};

				$scope.ResetPassword = function() {
					data.email = '';
					data.accessCode = '';
					data.password = '';
					data.resetKey = 'aaa';

					var url = LOGIN_BASE_URL + 'passwordReset';

					$http.post(url, data).then(function(response) {
						$scope.registrationLoginStatus = response.data;
					});

				};

				// *** Begin ***
				var id;
				var bcrypt = new bCrypt();

				var begin = '';
				function result(hashRet) {
					console.log(hashRet);
					$scope.hash = hashRet;
				}

				$scope.ckpassword = function(password, hash, callbackFunction) {
					try {

						bcrypt.checkpw(password, hash, callbackFunction, null);
					} catch (err) {
						alert(err);
						return;
					}
				};

				$scope.crypt = function(password, callbackFunction) {
					var salt = '';
					var rounds = 10;

					// Auto generate the salt
					try {
						salt = bcrypt.gensalt(rounds);
					} catch (err) {
						alert(err);
						return;
					}

					try {
						// bcrypt.hashpw( password , salt, result , null );
						bcrypt.hashpw(password, salt, callbackFunction, null);
					} catch (err) {
						alert(err);
						return;
					}
				}
				// *** End ***
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('mergeAccountModalService', ['$http', '$uibModal', '$sce', '$window', 'AuthenticationService', 'ssoUrl', '$window', function ($http, $uibModal, $sce, $window, AuthenticationService, ssoUrl) {

	var modalDefaults = {
		animation: true,
		templateUrl: '/CEP/cep/components/login/page-partials/merge-account-modal.html'
	};

	var modalOptions = {
		headerText: '',
		bodyText: 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass: 'asvab-modal'
	};

	this.showModal = function (customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function (customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = ['$scope', '$uibModalInstance', 'appUrl', function ($scope, $uibModalInstance, appUrl) {
				$scope.successful = false;
				var LOGIN_BASE_URL = appUrl + 'applicationAccess/';
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function (result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function (result) {
					$uibModalInstance.close('cancel');
				};
				
				$scope.email = tempModalDefaults.email;
				$scope.accessCode = tempModalDefaults.accessCode;
				//$scope.userId = $scope.registrationLoginStatus.user_id;
				//$scope.role = $scope.registrationLoginStatus.role
					
				$scope.mergeAccount = function() {
					var url = LOGIN_BASE_URL + 'merge/'+$scope.email+'/'+$scope.accessCode+'/'+$scope.profileFlag+'/';
					
					$scope.loading = true;
					$scope.statusMessage = false;
					$http.get(url)
						.then(function (response) {
							var userData = response.data;
							if (userData.statusNumber == 0) {
								$scope.successful = true;
								//$scope.modalOptions.ok($scope.successful);
								$scope.modalOptions.ok(userData);
							} else if (userData.statusNumber == 1012) {
								$scope.message = "Merge Failed! Access Code is already registered";
								$scope.statusMessage = true;
							} else {
								$scope.message = "Unknown Status Error";
								$scope.statusMessage = true;
							}

							$scope.loading = false;
							AuthenticationService.SetCredentials($scope.email , $scope.accessCode,
									userData.user_id, 
									userData.role,
									userData.schoolCode,
									userData.schoolName,
									userData.city,
									userData.state,
									userData.zipCode,
									userData.mepsId,
									userData.mepsName);
							
							$uibModalInstance.dismiss('cancel');
							$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
						});
				};
			}]
		}
		return $uibModal.open(tempModalDefaults).result;
	};

}]);
cepApp.service('TeacherModalService', [ '$uibModal', '$location', '$http', '$window','$timeout', 'AuthenticationService','appUrl','modalService', 'ssoUrl', 'LoginModalService', function($uibModal, $location, $http, $window, $timeout, AuthenticationService, appUrl, modalService, ssoUrl, LoginModalService) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/login/page-partials/teacher-registration-modal.html',
		size : 'md',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};
		 
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

				var LOGIN_BASE_URL = appUrl + 'applicationAccess/';
				
				// modal scopes
				
				
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.showLoginModal = function() {
					console.log('here');
					$uibModalInstance.dismiss('cancel');
					LoginModalService.show({}, {});
				}
				
				$scope.validateTeacherPromoCode = function() {
					var data = {
						email : "",
						accessCode : $scope.teacherAccessCode,
						password : "",
						resetKey : ""
					};


					var url = LOGIN_BASE_URL + 'testTeacherAccessCode';

					$http.post(url, data).then(function(response) {
						var userData = response.data;
						if (userData.statusNumber == 805 ) {
							$scope.teacherAccessCodeLabel = 'Access Code is not valid';
					        $timeout(function () { $scope.teacherAccessCodeLabel = ''; }, 5000);   
							return;

						} else if (userData.statusNumber != 0) {
							var serverErrorMessage = userData.status.substring(userData.status.indexOf(":") + 1, userData.status.length + 1);
							$scope.loginEmailLabel = serverErrorMessage;
					        $timeout(function () { $scope.loginEmailLabel = ''; }, 5000);   
							
						}
						$scope.create = true;
						return;
					});

				};
				
				
				// @RequestMapping(value = "/applicationAccess/teacherRegistration",
				// method = RequestMethod.POST)
				$scope.teacherRegistration = function() {
					$scope.registerAccessCode

					// Register Modal
					var data = {
						email : $scope.registerEmail, // document.getElementById("registerEmail").value,
						subject: $scope.registerSubject,
						accessCode : $scope.teacherAccessCode, // document.getElementById("registerAccessCode").value,
						password : "",
						resetKey : ""
					};

					var pass1 = $scope.registerPassword1;
					var pass2 = $scope.registerPassword2;

					if (pass1.length <= 7) {
						$scope.registrationLabel = "Password too short!";
						return;
					}
					
					if (pass1 != pass2) {
						$scope.registrationLabel = "Passwords don't match!";
						return;
					}

					var url = LOGIN_BASE_URL + 'teacherRegistration';

					var temp = $scope.crypt(pass1, function(hashRet) {
						data.password = hashRet;
						$http.post(url, data).then(function(response) {
							$scope.registrationLoginStatus = response.data;
							// Success ..
							var userData = response.data;

							// Open Login
							if ($scope.registrationLoginStatus.statusNumber == 0) {
								// hide registration view which then shows login
								// view
								$scope.isRegister = false;
								// Clear all the data 
								$scope.registerAccessCode = '';
								$scope.registerEmail =  '';
								$scope.registerPassword1 =  '';
								$scope.registerPassword2 =  '';
								$scope.registerStudentId = '';
								
								//  Now log in the use ....
								AuthenticationService.SetCredentials($scope.registerEmail , 'null', userData.user_id, userData.role,
										userData.schoolCode,
										userData.schoolName,
										userData.city,
										userData.state,
										userData.zipCode,
										userData.mepsId,
										userData.mepsName);

								// Close Dialog
								$uibModalInstance.dismiss('cancel');

								// Set the location to dashboard

								// Login to CITM.
								$window.location.href = '/CEP/rest/sso/confirm_login?redirect=' + ssoUrl + 'login_callback/test';
								
								
								
								// Close Dialog
							//	$uibModalInstance.dismiss('cancel');

							} else { // Error
								// ..
								// update
								// registration
								// Label
								$scope.registrationLabel = $scope.registrationLoginStatus.status
						         $timeout(function () { $scope.registrationLabel  = ''; }, 5000);   
								
								
							}
						});
					});
				};

				
				$scope.clearRegistrationLabel = function() {
					$scope.registrationLabel = '';
				};
				
				
				// *** Begin ***
				var id;
				var bcrypt = new bCrypt();

				var begin = '';
				function result(hashRet) {
					console.log(hashRet);
					$scope.hash = hashRet;
				}

				$scope.ckpassword = function(password, hash, callbackFunction) {
					try {

						bcrypt.checkpw(password, hash, callbackFunction, null);
					} catch (err) {
						alert(err);
						return;
					}
				};

				$scope.crypt = function(password, callbackFunction) {
					var salt = '';
					var rounds = 10;

					// Auto generate the salt
					try {
						salt = bcrypt.gensalt(rounds);
					} catch (err) {
						alert(err);
						return;
					}

					try {
						// bcrypt.hashpw( password , salt, result , null );
						bcrypt.hashpw(password, salt, callbackFunction, null);
					} catch (err) {
						alert(err);
						return;
					}
				}
				// *** End ***
				
				
				
				
			} ]
			
	
			
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.controller('MediaCenterArticleController', [ 'resource', '$scope', 'mediaCenterList', '$route', '$sce', '$location', 'BringToSchoolModalService', function(resource, $scope, mediaCenterList, $route, $sce, $location, BringToSchoolModalService) {

	$scope.mediaCenterList = mediaCenterList;
	$scope.mediaId = $route.current.params.mediaId
	$scope.article = [];
	$scope.absUrl = $location.absUrl();
	$scope.domainName = resource;
	var path = $location.path();
	
	for(var i = 0; i < $scope.mediaCenterList.length; i++){
		if($scope.mediaCenterList[i].id == $scope.mediaId){
			$scope.trustedVideoUrl = $sce.trustAsResourceUrl($scope.mediaCenterList[i].videoImgUrl);
			$scope.thumbnailUrl = ($scope.mediaCenterList[i].thumbnailUrl).replace(/(\n|\r)/gm,'');
			$scope.article.push($scope.mediaCenterList[i]);
		}
	}
	
	$scope.ShrinkDescription = function(description, size) {
		if (description.length <= size) {
			return description;
		} else {
			return description.substring(0, size) + "...";
		}
	};
	
	/**
	 * Bring ASVAB to your school modal
	 */
	$scope.asvabModal = function(size) {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_LINKED' );
		BringToSchoolModalService.show({}, {});
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);

cepApp.controller('MediaCenterArticleListController', [ '$scope', 'mediaCenterList', '$route', function($scope, mediaCenterList, $route) {

	$scope.mediaCenterList = mediaCenterList;
	$scope.categoryId = $route.current.params.categoryId
	$scope.articleList = [];
	
	for(var i = 0; i < $scope.mediaCenterList.length; i++){
		if($scope.mediaCenterList[i].categoryId == $scope.categoryId){
			$scope.articleList.push($scope.mediaCenterList[i]);
		}
	}
	
	/**
	 * Pagination
	 */
	$scope.totalItems = $scope.articleList.length;
	$scope.currentPage = 1;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 10;

	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);

cepApp.controller('MediaCenterController', [ '$scope', 'mediaCenterList', 'resource',  function($scope, mediaCenterList, resource) {

	$scope.mediaCenterList = mediaCenterList;
	$scope.documents = resource + 'pdf/';
	
	$timeout = twttr.widgets.load();

    angular.element(document).ready(function() {
		$('.sharables').slick({
		  dots: false,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  responsive: [
			{
			  breakpoint: 640,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		  ]
		});
      });
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);

cepApp.factory('MediaCenterRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var mediaCenterRestFactory = {};

	mediaCenterRestFactory.getMediaCenterList = function() {
		return $http.get(appUrl + 'media-center/getMediaCenterList');
	}
	
	return mediaCenterRestFactory;

} ]);
cepApp.factory('MediaCenterService', [ '$q', '$window', 'MediaCenterRestFactory', function($q, $window, MediaCenterRestFactory) {

	var service = {};

	/**
	 * Media Center Session Storage
	 */
	var mediaCenterList = undefined;
	
	/**
	 * Return favorite list.
	 */
	service.getMediaCenterList = function() {

		var deferred = $q.defer();

		// if mediaCenterList is undefined then recover data from database
		if (mediaCenterList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("mediaCenterList") === null || $window.sessionStorage.getItem("mediaCenterList") == 'undefined') {

					// recover data using database
					MediaCenterRestFactory.getMediaCenterList().success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('mediaCenterList', JSON.stringify(data));

						// sync with in memory property
						mediaCenterList = angular.fromJson($window.sessionStorage.getItem('mediaCenterList'));

						deferred.resolve(mediaCenterList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					mediaCenterList = angular.fromJson($window.sessionStorage.getItem('mediaCenterList'));
					deferred.resolve(mediaCenterList);
				}

			} else {

				// no session storage support so get recover data using database
				MediaCenterRestFactory.getMediaCenterList().success(function(data) {

					// success now store in session object
					mediaCenterList = data;
					deferred.resolve(mediaCenterList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(mediaCenterList);
		}

		return deferred.promise;
	}

	return service;

} ]);
cepApp.controller('CareerClusterNotesController', [ '$scope', 'UserFactory', 'modalService', '$route', 'CareerClusterNotesModalService', function($scope, UserFactory, modalService, $route, CareerClusterNotesModalService) {

	/**
	 * Used on career cluster results page.
	 */
	$scope.showOccufindNotes = function() {
		CareerClusterNotesModalService.show({
			size : 'lg'
		}, {
			title : $scope.careerCluster.ccTitle,
			ccId : $route.current.params.ccId
		});
	}
	
	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotes = function(title, ccId) {
		CareerClusterNotesModalService.show({
			size : 'lg'
		}, {
			title : title,
			ccId : ccId
		});
	}

} ]);

cepApp.controller('NotesController', [ '$scope', 'UserFactory', 'modalService', '$route', 'NotesModalService', 'SchoolNotesModalService', function($scope, UserFactory, modalService, $route, NotesModalService,SchoolNotesModalService) {

	/**
	 * Used on Occu-find details page.
	 */
	$scope.showOccufindNotes = function() {
		NotesModalService.show({
			size : 'lg'
		}, {
			title : $scope.occupationTitle,
			socId : $route.current.params.socId
		});
	}

	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotes = function(title, socId) {
		NotesModalService.show({
			size : 'lg'
		}, {
			title : title,
			socId : socId
		});
	}
	
	/**
	 * Used on school details page.
	 */
	$scope.showSchoolNotes = function() {
		NotesModalService.show({
			size : 'lg'
		}, {
			schoolName : $scope.schoolTitle,
			unitId : $route.current.params.unitId
		});
	}

	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotesSchool = function(title, unitId) {
		NotesModalService.show({
			size : 'lg'
		}, {
			schoolName : title,
			unitId : unitId
		});
	}


} ]);

cepApp.controller('SchoolNotesController', [ '$scope', 'UserFactory', 'modalService', '$route', 'SchoolNotesModalService', function($scope, UserFactory, modalService, $route, SchoolNotesModalService) {

	/**
	 * Used on school details page.
	 */
	$scope.showSchoolNotes = function() {
		SchoolNotesModalService.show({
			size : 'lg'
		}, {
			title : $scope.schoolTitle,
			unitId : $route.current.params.unitId
		});
	}

	/**
	 * Used on My Favorites page.
	 */
	$scope.showNotes = function(title, unitId) {
		SchoolNotesModalService.show({
			size : 'lg'
		}, {
			title : title,
			unitId : unitId
		});
	}

} ]);

cepApp.service('CareerClusterNotesModalService', [ '$uibModal', '$sce', 'UserFactory', 'CareerClusterNotesService', '$rootScope', '$q', function($uibModal, $sce, UserFactory, CareerClusterNotesService, $rootScope, $q) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/notes/page-partials/career-cluster-notes-modal.html'
	};

	var modalOptions = {};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {

			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				var promise = CareerClusterNotesService.getCareerClusterNotes();

				promise.then(function(success) {
					$scope.careerClusterNoteList = success;

					$scope.initialNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						ccId : $scope.modalOptions.ccId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					$scope.selectedNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						ccId : $scope.modalOptions.ccId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.careerClusterNoteList.length; i++) {
						if ($scope.careerClusterNoteList[i].ccId == $scope.selectedNotesObject.ccId) {
							$scope.initialNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								ccId : $scope.careerClusterNoteList[i].ccId,
								title : $scope.careerClusterNoteList[i].title,
								notes : $scope.careerClusterNoteList[i].notes,
								noteId : $scope.careerClusterNoteList[i].noteId
							};

							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								ccId : $scope.careerClusterNoteList[i].ccId,
								title : $scope.careerClusterNoteList[i].title,
								notes : $scope.careerClusterNoteList[i].notes,
								noteId : $scope.careerClusterNoteList[i].noteId
							};
						}
					}

				}, function(error) {
					console.log(error);
				})

				/**
				 * Reset to current career cluster object.
				 */
				$scope.resetObject = function() {
					$scope.initialNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						ccId : $scope.modalOptions.ccId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					$scope.selectedNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						ccId : $scope.modalOptions.ccId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.careerClusterNoteList.length; i++) {
						if ($scope.careerClusterNoteList[i].ccId == $scope.selectedNotesObject.ccId) {
							$scope.initialNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								ccId : $scope.careerClusterNoteList[i].ccId,
								title : $scope.careerClusterNoteList[i].title,
								notes : $scope.careerClusterNoteList[i].notes,
								noteId : $scope.careerClusterNoteList[i].noteId
							};

							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								ccId : $scope.careerClusterNoteList[i].ccId,
								title : $scope.careerClusterNoteList[i].title,
								notes : $scope.careerClusterNoteList[i].notes,
								noteId : $scope.careerClusterNoteList[i].noteId
							};
						}
					}
				}

				/**
				 * Save Notes
				 */
				$scope.modalOptions.save = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					var notesExist = false;

					if ($scope.selectedNotesObject.notes == undefined || $scope.selectedNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.careerClusterNoteList.length; i++) {
						if ($scope.careerClusterNoteList[i].ccId == $scope.selectedNotesObject.ccId) {
							notesExist = true;
						}
					}

					var promise;

					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						promise = CareerClusterNotesService.insertCareerClusterNote($scope.selectedNotesObject);
						promise.then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {

						promise = CareerClusterNotesService.updateCareerClusterNote($scope.selectedNotesObject);
						promise.then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};

				/**
				 * Within the modal, user can toggle between career clusters
				 * that they saved notes for.
				 */
				$scope.selectCareerCluster = function(noteId) {

					// set the information to display to the user depending on
					// their selection
					for (var i = 0; i < $scope.careerClusterNoteList.length; i++) {
						if ($scope.careerClusterNoteList[i].noteId == noteId) {
							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								ccId : $scope.careerClusterNoteList[i].ccId,
								title : $scope.careerClusterNoteList[i].title,
								notes : $scope.careerClusterNoteList[i].notes,
								noteId : $scope.careerClusterNoteList[i].noteId
							};
						}
					}
				}

				$scope.modalOptions.close = function() {
					$uibModalInstance.dismiss('cancel');
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};
} ]);
cepApp.factory('CareerClusterNotesService', [ '$q', '$window', '$rootScope', 'NotesRestFactory', function($q, $window, $rootScope, NotesRestFactory) {

	var notesService = {};

	var careerClusterNoteList = undefined;

	/**
	 * Uses notes rest service to insert notes into database. Utilize session
	 * storage. Returns a promise
	 */
	notesService.insertCareerClusterNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.insertCareerClusterNote(selectedNotesObject).success(function(data) {

			// push newly added career cluster to property
			careerClusterNoteList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(careerClusterNoteList));

				// sync with in memory property
				careerClusterNoteList = angular.fromJson($window.sessionStorage.getItem('careerClusterNoteList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to update user's note information. Utilize
	 * session storage. Returns a promise.
	 */
	notesService.updateCareerClusterNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.updateCareerClusterNote(selectedNotesObject).success(function(data) {

			// sync careerClusterNoteList
			var length = careerClusterNoteList.length;
			for (var i = 0; i < length; i++) {
				if (careerClusterNoteList[i].noteId == selectedNotesObject.noteId) {
					careerClusterNoteList[i] = selectedNotesObject;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(careerClusterNoteList));

				// sync with in memory property
				careerClusterNoteList = angular.fromJson($window.sessionStorage.getItem('careerClusterNoteList'));
			}

			deferred.resolve("Update successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to get all user's notes. Utilizes session
	 * storage if user refreshes page. If browser doesn't support session storage, then data will be
	 * obtained by using rest service for each call.
	 */
	notesService.getCareerClusterNotes = function() {
		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (careerClusterNoteList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("careerClusterNoteList") === null || $window.sessionStorage.getItem("careerClusterNoteList") == 'undefined') {

					// recover data using database
					NotesRestFactory.getCareerClusterNotes(userId).success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('careerClusterNoteList', JSON.stringify(data));

						// sync with in memory property
						careerClusterNoteList = angular.fromJson($window.sessionStorage.getItem('careerClusterNoteList'));

						deferred.resolve(careerClusterNoteList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					careerClusterNoteList = angular.fromJson($window.sessionStorage.getItem('careerClusterNoteList'));
					deferred.resolve(careerClusterNoteList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getCareerClusterNotes(userId).success(function(data) {

					// success now store in session object
					careerClusterNoteList = data;
					deferred.resolve(careerClusterNoteList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(careerClusterNoteList);
		}

		return deferred.promise;
	}

	return notesService;

} ]);
cepApp.service('NotesModalService', [ '$uibModal', '$sce', 'UserFactory', 'NotesService', '$rootScope', '$q', 'SchoolNotesService', function($uibModal, $sce, UserFactory, NotesService, $rootScope, $q, SchoolNotesService) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/notes/page-partials/notes-modal.html'
	};

	var modalOptions = {};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {

			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				var promise = NotesService.getNotes();

				promise.then(function(success) {
					$scope.noteList = success;

					// Occurs when using notes from dashboard
					if ($scope.modalOptions.socId == undefined) {
						$scope.modalOptions.socId = $scope.noteList[0].socId;
						$scope.modalOptions.title = $scope.noteList[0].title;
					}

					$scope.initialNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						socId : $scope.modalOptions.socId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					$scope.selectedNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						socId : $scope.modalOptions.socId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].socId == $scope.selectedNotesObject.socId) {
							$scope.initialNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								socId : $scope.noteList[i].socId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};

							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								socId : $scope.noteList[i].socId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};
						}
					}

				}, function(error) {
					console.log(error);
				})

				/**
				 * Reset to current occupation object.
				 */
				$scope.resetObject = function() {
					$scope.initialNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						socId : $scope.modalOptions.socId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					$scope.selectedNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						socId : $scope.modalOptions.socId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].socId == $scope.selectedNotesObject.socId) {
							$scope.initialNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								socId : $scope.noteList[i].socId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};

							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								socId : $scope.noteList[i].socId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};
						}
					}
				}

				/**
				 * Save Notes
				 */
				$scope.modalOptions.save = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					var notesExist = false;

					if ($scope.selectedNotesObject.notes == undefined || $scope.selectedNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].socId == $scope.selectedNotesObject.socId) {
							notesExist = true;
						}
					}

					var promise;

					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						promise = NotesService.insertNote($scope.selectedNotesObject);
						promise.then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {

						promise = NotesService.updateNote($scope.selectedNotesObject);
						promise.then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};

				/**
				 * Within the modal, user can toggle between occupations that
				 * they saved notes for.
				 */
				$scope.selectOccupation = function(noteId) {

					// set the information to display to the user depending on
					// their selection
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].noteId == noteId) {
							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								socId : $scope.noteList[i].socId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};
						}
					}
				}
				
				
				
				/*********************************************************************************************
				 * 
				 ********************************************************************************************/
				$scope.viewSetting = {};

				if ($scope.modalOptions.unitId == undefined) {
					$scope.viewSetting.showSchoolNotes = false;
					$scope.viewSetting.showOccupationNotes = true;
				} else {
					$scope.viewSetting.showSchoolNotes = true;
					$scope.viewSetting.showOccupationNotes = false;
				}

				$scope.modalOptions = tempModalOptions;
				var promise = SchoolNotesService.getNotes();

				promise.then(function(success) {
					$scope.noteListSchool = success;

					// Occurs when using notes from dashboard
					if ($scope.modalOptions.unitId == undefined) {
						$scope.modalOptions.unitId = $scope.noteListSchool[0].unitId;
						$scope.modalOptions.schoolName = $scope.noteListSchool[0].schoolName;
					}

					$scope.initialSchoolNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						unitId : $scope.modalOptions.unitId,
						schoolName : $scope.modalOptions.schoolName,
						notes : undefined
					};

					$scope.selectedSchoolNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						unitId : $scope.modalOptions.unitId,
						schoolName : $scope.modalOptions.schoolName,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteListSchool.length; i++) {
						if ($scope.noteListSchool[i].unitId == $scope.selectedSchoolNotesObject.unitId) {
							$scope.initialSchoolNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteListSchool[i].unitId,
								schoolName : $scope.noteListSchool[i].schoolName,
								notes : $scope.noteListSchool[i].notes,
								noteId : $scope.noteListSchool[i].noteId
							};

							$scope.selectedSchoolNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteListSchool[i].unitId,
								schoolName : $scope.noteListSchool[i].schoolName,
								notes : $scope.noteListSchool[i].notes,
								noteId : $scope.noteListSchool[i].noteId
							};
						}
					}

				}, function(error) {
					console.log(error);
				})

				/**
				 * Reset to current occupation object.
				 */
				$scope.resetSchoolObject = function() {
					$scope.initialSchoolNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						unitId : $scope.modalOptions.unitId,
						schoolName : $scope.modalOptions.schoolName,
						notes : undefined
					};

					$scope.selectedSchoolNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						unitId : $scope.modalOptions.unitId,
						schoolName : $scope.modalOptions.schoolName,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteListSchool.length; i++) {
						if ($scope.noteListSchool[i].unitId == $scope.selectedSchoolNotesObject.unitId) {
							$scope.initialSchoolNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteListSchool[i].unitId,
								schoolName : $scope.noteListSchool[i].schoolName,
								notes : $scope.noteListSchool[i].notes,
								noteId : $scope.noteListSchool[i].noteId
							};

							$scope.selectedSchoolNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteListSchool[i].unitId,
								schoolName : $scope.noteListSchool[i].schoolName,
								notes : $scope.noteListSchool[i].notes,
								noteId : $scope.noteListSchool[i].noteId
							};
						}
					}
				}

				/**
				 * Save Notes
				 */
				$scope.modalOptions.saveSchool = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					var notesExist = false;

					if ($scope.selectedSchoolNotesObject.notes == undefined || $scope.selectedSchoolNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.noteListSchool.length; i++) {
						if ($scope.noteListSchool[i].unitId == $scope.selectedSchoolNotesObject.unitId) {
							notesExist = true;
						}
					}

					var promise;

					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						promise = SchoolNotesService.insertNote($scope.selectedSchoolNotesObject);
						promise.then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {

						promise = SchoolNotesService.updateNote($scope.selectedSchoolNotesObject);
						promise.then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};

				/**
				 * Within the modal, user can toggle between occupations that
				 * they saved notes for.
				 */
				$scope.selectSchool = function(noteId) {

					// set the information to display to the user depending on
					// their selection
					for (var i = 0; i < $scope.noteListSchool.length; i++) {
						if ($scope.noteListSchool[i].noteId == noteId) {
							$scope.selectedSchoolNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteListSchool[i].unitId,
								schoolName : $scope.noteListSchool[i].schoolName,
								notes : $scope.noteListSchool[i].notes,
								noteId : $scope.noteListSchool[i].noteId
							};
						}
					}
				}

				$scope.modalOptions.close = function() {
					$uibModalInstance.dismiss('cancel');
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};
} ]);
cepApp.factory('NotesRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var notesRestFactory = {};

	notesRestFactory.getNotes = function(userId) {
		return $http.get(appUrl + 'notes/getNotes/' + userId + '/');
	}

	notesRestFactory.insertNote = function(noteObject) {
		return $http.post(appUrl + 'notes/insertNote', noteObject);
	}

	notesRestFactory.updateNote = function(noteObject) {
		return $http.put(appUrl + 'notes/updateNote', noteObject);
	}

	notesRestFactory.getOccupationNotes = function(noteObject) {
		return $http.get(appUrl + 'notes/getOccupationNotes/' + noteObject.userId + '/' + noteObject.socId + '/');
	}
	
	notesRestFactory.getCareerClusterNotes = function(userId) {
		return $http.get(appUrl + 'notes/getCareerClusterNotes/' + userId + '/');
	}

	notesRestFactory.insertCareerClusterNote = function(noteObject) {
		return $http.post(appUrl + 'notes/insertCareerClusterNote', noteObject);
	}

	notesRestFactory.updateCareerClusterNote = function(noteObject) {
		return $http.put(appUrl + 'notes/updateCareerClusterNote', noteObject);
	}

	notesRestFactory.getCareerClusterNote = function(noteObject) {
		return $http.get(appUrl + 'notes/getCareerClusterNote/' + noteObject.userId + '/' + noteObject.ccId + '/');
	}
	
	notesRestFactory.getSchoolNotes = function(userId) {
		return $http.get(appUrl + 'notes/getSchoolNotes/' + userId + '/');
	}

	notesRestFactory.insertSchoolNote = function(noteObject) {
		return $http.post(appUrl + 'notes/insertSchoolNote', noteObject);
	}

	notesRestFactory.updateSchoolNote = function(noteObject) {
		return $http.put(appUrl + 'notes/updateSchoolNote', noteObject);
	}

	notesRestFactory.getSchoolNote = function(noteObject) {
		return $http.get(appUrl + 'notes/getSchoolNotes/' + noteObject.userId + '/' + noteObject.unitId + '/');
	}

	return notesRestFactory;

} ]);

cepApp.factory('NotesService', [ '$q', '$window', '$rootScope', 'NotesRestFactory', function($q, $window, $rootScope, NotesRestFactory) {

	var notesService = {};

	var noteList = undefined;

	/**
	 * Uses notes rest service to insert notes into database. Utilize session
	 * storage. Returns a promise
	 */
	notesService.insertNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.insertNote(selectedNotesObject).success(function(data) {

			// push newly added occupation to property
			noteList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('noteList', JSON.stringify(noteList));

				// sync with in memory property
				noteList = angular.fromJson($window.sessionStorage.getItem('noteList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to update user's note information. Utilize
	 * session storage. Returns a promise.
	 */
	notesService.updateNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.updateNote(selectedNotesObject).success(function(data) {

			// sync noteList
			var length = noteList.length;
			for (var i = 0; i < length; i++) {
				if (noteList[i].noteId == selectedNotesObject.noteId) {
					noteList[i] = selectedNotesObject;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('noteList', JSON.stringify(noteList));

				// sync with in memory property
				noteList = angular.fromJson($window.sessionStorage.getItem('noteList'));
			}

			deferred.resolve("Update successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to get all user's notes. Utilizes session
	 * storage if user refreshes page. If browser doesn't support session storage, then data will be
	 * obtained by using rest service for each call.
	 */
	notesService.getNotes = function() {
		if ( typeof $rootScope.globals.currentUser === 'undefined') return;
		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (noteList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("noteList") === null || $window.sessionStorage.getItem("noteList") == 'undefined') {

					// recover data using database
					NotesRestFactory.getNotes(userId).success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('noteList', JSON.stringify(data));

						// sync with in memory property
						noteList = angular.fromJson($window.sessionStorage.getItem('noteList'));

						deferred.resolve(noteList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					noteList = angular.fromJson($window.sessionStorage.getItem('noteList'));
					deferred.resolve(noteList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getNotes(userId).success(function(data) {

					// success now store in session object
					noteList = data;
					deferred.resolve(noteList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(noteList);
		}

		return deferred.promise;
	}

	return notesService;

} ]);
cepApp.service('SchoolNotesModalService', [ '$uibModal', '$sce', 'UserFactory', 'SchoolNotesService', '$rootScope', '$q', function($uibModal, $sce, UserFactory, SchoolNotesService, $rootScope, $q) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/components/notes/page-partials/school-notes-modal.html'
	};

	var modalOptions = {};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {

			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				var promise = SchoolNotesService.getNotes();

				promise.then(function(success) {
					$scope.noteList = success;

					// Occurs when using notes from dashboard
					if ($scope.modalOptions.unitId == undefined) {
						$scope.modalOptions.unitId = $scope.noteList[0].unitId;
						$scope.modalOptions.title = $scope.noteList[0].title;
					}

					$scope.initialNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						unitId : $scope.modalOptions.unitId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					$scope.selectedNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						unitId : $scope.modalOptions.unitId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].unitId == $scope.selectedNotesObject.unitId) {
							$scope.initialNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteList[i].unitId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};

							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteList[i].unitId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};
						}
					}

				}, function(error) {
					console.log(error);
				})

				/**
				 * Reset to current occupation object.
				 */
				$scope.resetObject = function() {
					$scope.initialNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						unitId : $scope.modalOptions.unitId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					$scope.selectedNotesObject = {
						userId : $rootScope.globals.currentUser.userId,
						unitId : $scope.modalOptions.unitId,
						title : $scope.modalOptions.title,
						notes : undefined
					};

					// set notes if saved previously
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].unitId == $scope.selectedNotesObject.unitId) {
							$scope.initialNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteList[i].unitId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};

							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteList[i].unitId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};
						}
					}
				}

				/**
				 * Save Notes
				 */
				$scope.modalOptions.save = function() {
					$scope.error = '';
					$scope.savingNotes = true;
					var notesExist = false;

					if ($scope.selectedNotesObject.notes == undefined || $scope.selectedNotesObject.notes == '') {
						$scope.error = 'Note is blank';
						$scope.savingNotes = false;
						return false;
					}

					// check to see if notes was already saved
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].unitId == $scope.selectedNotesObject.unitId) {
							notesExist = true;
						}
					}

					var promise;

					// if notes were never saved then save notes to database,
					// otherwise update notes
					if (!notesExist) {
						promise = SchoolNotesService.insertNote($scope.selectedNotesObject);
						promise.then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					} else {

						promise = SchoolNotesService.updateNote($scope.selectedNotesObject);
						promise.then(function(success) {
							$scope.savingNotes = false;
							$uibModalInstance.dismiss('cancel');
						}, function(error) {
							$scope.savingNotes = false;
							$scope.error = "There was an error proccessing your request. Please try again."
							return false
						});
					}
				};

				/**
				 * Within the modal, user can toggle between occupations that
				 * they saved notes for.
				 */
				$scope.selectOccupation = function(noteId) {

					// set the information to display to the user depending on
					// their selection
					for (var i = 0; i < $scope.noteList.length; i++) {
						if ($scope.noteList[i].noteId == noteId) {
							$scope.selectedNotesObject = {
								userId : $rootScope.globals.currentUser.userId,
								unitId : $scope.noteList[i].unitId,
								title : $scope.noteList[i].title,
								notes : $scope.noteList[i].notes,
								noteId : $scope.noteList[i].noteId
							};
						}
					}
				}

				$scope.modalOptions.close = function() {
					$uibModalInstance.dismiss('cancel');
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};
} ]);
cepApp.factory('SchoolNotesService', [ '$q', '$window', '$rootScope', 'NotesRestFactory', function($q, $window, $rootScope, NotesRestFactory) {

	var notesService = {};

	var schoolNoteList = undefined;

	/**
	 * Uses notes rest service to insert notes into database. Utilize session
	 * storage. Returns a promise
	 */
	notesService.insertNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.insertSchoolNote(selectedNotesObject).success(function(data) {

			// push newly added occupation to property
			schoolNoteList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('schoolNoteList', JSON.stringify(schoolNoteList));

				// sync with in memory property
				schoolNoteList = angular.fromJson($window.sessionStorage.getItem('schoolNoteList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to update user's note information. Utilize
	 * session storage. Returns a promise.
	 */
	notesService.updateNote = function(selectedNotesObject) {
		var deferred = $q.defer();

		NotesRestFactory.updateSchoolNote(selectedNotesObject).success(function(data) {

			// sync noteList
			var length = schoolNoteList.length;
			for (var i = 0; i < length; i++) {
				if (schoolNoteList[i].noteId == selectedNotesObject.noteId) {
					schoolNoteList[i] = selectedNotesObject;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('schoolNoteList', JSON.stringify(schoolNoteList));

				// sync with in memory property
				schoolNoteList = angular.fromJson($window.sessionStorage.getItem('schoolNoteList'));
			}

			deferred.resolve("Update successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Uses notes rest service to get all user's notes. Utilizes session
	 * storage if user refreshes page. If browser doesn't support session storage, then data will be
	 * obtained by using rest service for each call.
	 */
	notesService.getNotes = function() {
		if ( typeof $rootScope.globals.currentUser === 'undefined') return;
		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (schoolNoteList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("schoolNoteList") === null || $window.sessionStorage.getItem("schoolNoteList") == 'undefined') {

					// recover data using database
					NotesRestFactory.getSchoolNotes(userId).success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('schoolNoteList', JSON.stringify(data));

						// sync with in memory property
						schoolNoteList = angular.fromJson($window.sessionStorage.getItem('schoolNoteList'));

						deferred.resolve(schoolNoteList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					schoolNoteList = angular.fromJson($window.sessionStorage.getItem('schoolNoteList'));
					deferred.resolve(schoolNoteList);
				}

			} else {

				// no session storage support so get recover data using database
				NotesRestFactory.getSchoolNotes(userId).success(function(data) {

					// success now store in session object
					schoolNoteList = data;
					deferred.resolve(schoolNoteList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(schoolNoteList);
		}

		return deferred.promise;
	}

	return notesService;

} ]);
cepApp.controller('OccufindEducationDetailsController', [ '$scope', '$rootScope', 'schoolDetails', 'schoolMajors', 'occupationTitleDescription', '$route', function($scope, $rootScope, schoolDetails, schoolMajors, occupationTitleDescription, $route) {

	$scope.occupationTitle = occupationTitleDescription.data.title;
	$scope.schoolDetails = schoolDetails.data;
	console.log($scope.schoolDetails);
	$scope.sort;
	$scope.socId = $route.current.params.socId;
	
	
	$scope.schoolMajors = [];
	for(var i = 0; i < schoolMajors.data.length; i++) {
		$scope.schoolMajors.push(schoolMajors.data[i]);
	}


	$scope.showMore = false;
	$scope.showMoreText = function() {
		if(!$scope.showMore) {
			return "More";
		} else {
			return "Less";
		}
	}
	$scope.hasMore= function(){
		if($scope.schoolMajors.length <= 3)
			return false;
		else
			return true;	
	}

	
	//pagination
	$scope.totalItems = $scope.schoolDetails.length;
	$scope.currentPage = 1;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 20;
	
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ; 
	}
	
	//filter school list by state
	$scope.$watch('search', function(term) {
        $scope.filtered = stateFilter($scope.schoolDetails, term);
        $scope.totalItems = $scope.filtered.length;
    });
	
	function stateFilter(list, term) {
		var filterList = [];
		if (term == '' || term == undefined) {
			return list;
		}
		for (var i = 0; i < list.length; i++) {
			if (list[i].state == term) {
				filterList.push(list[i]);
			}
		}
		return filterList;
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
cepApp.controller('OccufindEmploymentDetailsController', [ '$scope', '$rootScope', '$route', 'stateSalaryYear', 'occupationTitleDescription', 'employmentMoreDetails', function($scope, $rootScope, $route, stateSalaryYear, occupationTitleDescription, employmentMoreDetails) {

	$scope.stateSalaryYear = stateSalaryYear.data;
	$scope.occupationTitle = occupationTitleDescription.data.title;
	$scope.employementMoreDetails = employmentMoreDetails.data;
	$scope.socId = $route.current.params.socId;
	
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ; 
	}
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
cepApp.controller('OccufindMilitaryDetailsController', [ '$scope', '$window', '$rootScope', '$route', 'occupationTitleDescription', 'militaryMoreDetails', 'occufindMilitaryDetailModalService', 'militaryHotJobs', 'militaryResources', 'ssoUrl', function($scope, $window, $rootScope, $route, occupationTitleDescription, militaryMoreDetails, occufindMilitaryDetailModalService, militaryHotJobs, militaryResources, ssoUrl) {
	
	$scope.ssoUrl = ssoUrl;
	$scope.socId = $route.current.params.socId;
	$scope.occupationTitle = occupationTitleDescription.data.title;
	$scope.militaryMoreDetails = militaryMoreDetails.data;
	$scope.militaryHotJobs = militaryHotJobs.data;
	$scope.militaryResources = militaryResources.data;
	
	$scope.goToCitmDetailPage = function(mcId) {
		var url = $scope.ssoUrl + 'career-detail/' + mcId;
		$window.open(url, '_blank');
	}
	
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ; 
	}
	
	/**
	 * Modal service that displays more information on occupation.
	 */
	$scope.occufindMilitaryDetailModalService = function(index) {
		occufindMilitaryDetailModalService.show({}, {militaryMoreDetails : $scope.militaryMoreDetails, index : index});
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
cepApp.controller('OccufindOccupationDetailsController', [ '$sce', '$scope', '$rootScope', '$location', 'OccufindSearchService', 'occupationTitleDescription', 'stateSalary', 'AuthenticationService', 'skillImportance', 'tasks', 'occupationInterestCodes', 'educationLevel', 'relatedOccupations', 'hotGreenStemFlags', 'servicesOffering', 'relatedCareerCluster', '$q', '$route', 'UserFactory', 'modalService', 'ApprenticeshipModalService', 'CertificationListModalService', 'CertificationDetailsService', 'LicensesListModalService', 'LicenseDetailsModalService', 'occupationCertificates', 'moreResources', 'schoolDetails', 'nationalSalary', 'LoginModalService', 'blsTitle', 'getAlternateView', 'imageAltText', 'jobZone', 'certificateExplanation', 'alternateTitles', function($sce, $scope, $rootScope, $location, OccufindSearchService, occupationTitleDescription, stateSalary, AuthenticationService, skillImportance, tasks, occupationInterestCodes, educationLevel, relatedOccupations, hotGreenStemFlags, servicesOffering, relatedCareerCluster, $q, $route, UserFactory, modalService, ApprenticeshipModalService, CertificationListModalService, CertificationDetailsService, LicensesListModalService, LicenseDetailsModalService, occupationCertificates, moreResources, schoolDetails, nationalSalary, LoginModalService, blsTitle, getAlternateView, imageAltText, jobZone, certificateExplanation, alternateTitles) {

	$q.all([ occupationTitleDescription, skillImportance, stateSalary, tasks ]).then(function successCallback(response) {
		$scope.occupationTitle = occupationTitleDescription.data.title;
		$scope.occupationDescription = occupationTitleDescription.data.description;
		$scope.skillImportance = skillImportance.data;
		$scope.stateSalary = stateSalary.data;
		$scope.nationalSalary = nationalSalary.data;
		$scope.blsTitle = blsTitle.data;
		$scope.tasks = tasks.data;
		$scope.occupationInterestCodes = occupationInterestCodes.data;
		$scope.educationLevel = educationLevel.data;
		$scope.relatedOccupations = relatedOccupations.data;
		$scope.servicesOffering = servicesOffering.data;
		$scope.relatedCareerCluster = relatedCareerCluster.data;
		$scope.moreResources = moreResources.data;
		$scope.alternateView = getAlternateView.data;
		$scope.imageUrl = '/static/' + $route.current.params.socId + '_IMG.jpg';		
		$scope.imageAltText = imageAltText.data;
		$scope.absUrl = $location.absUrl();
		$scope.domainUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port();
		$scope.jobZone = jobZone.data;
		$scope.alternateTitles = alternateTitles.data;
		$scope.certificateExplanation = certificateExplanation.data.licensing;
		
		$scope.showModifiedMsg = function() {
			var socId = $route.current.params.socId;
			if (socId != undefined) {
				var firstThreePositions = socId.substring(0, 3);
				if (firstThreePositions == '55-') {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}

		$scope.showBLSStatement = function() {
			var socId = $route.current.params.socId;
			if (socId != undefined) {
				var lastTwoPositions = socId.slice(-2);
				if (lastTwoPositions == '00') {
					return false;
				} else {
					return true;
				}
			}
			return false;
		}

		$scope.ShrinkDescription = function(description, size) {
			if (description.length <= size) {
				return description;
			} else {
				return description.substring(0, size) + "...";
			}
		};
		
		$scope.occupationGreen = hotGreenStemFlags.data.isGreenOccupation == "1" ? true : false;
		$scope.occupationStem = hotGreenStemFlags.data.isStemOccupation == "1" ? true : false;
		$scope.occupationBright = hotGreenStemFlags.data.isBrightOccupation == "1" ? true : false;

		if (typeof occupationInterestCodes.data[0] != 'undefined') {
			$scope.occupationInterestOne = occupationInterestCodes.data[0].interestCd;
			$scope.occupationInterestTwo = occupationInterestCodes.data[1].interestCd;
			$scope.occupationInterestThree = occupationInterestCodes.data[2].interestCd;
		}
		if (IsJsonString(occupationCertificates.data.payload))
			$scope.occupationCertificates = JSON.parse(occupationCertificates.data.payload);
		else
			$scope.occupationCertificates = occupationCertificates.data.payload;
		
		$scope.schoolDetails = schoolDetails.data;

		if($scope.occupationCertificates)
			if (IsJsonString($scope.occupationCertificates)) {
				$scope.occupationCertificates.TotalItems = 0;
			} else {
				$scope.occupationCertificates.TotalItems = $scope.occupationCertificates.RecordCount; // occupationCertificates.CertificationInfo.CertificationList.length;
			}
		
		if (typeof $rootScope.globals.currentUser === 'undefined') {
			$scope.occuFindUserLoggedIn = false;
		} else {
			$scope.occuFindUserLoggedIn = true;
		}
		/**
		 * Skill rating
		 */
		angular.element(document).ready(function() {
			$('.customrating').each(function() {
				var rating = $(this).data('customrating');
				var width = $(this).width() / 5 * rating;
				if (rating.toString().indexOf('.') !== -1) {
					width += 1;
				}
				$(this).width(width);
			});
			
			
			/**
			 * New star rating.
			 */
			$(function() {
			    $( '.ratebox' ).raterater({ 

			    // allow the user to change their mind after they have submitted a rating
			    allowChange: false,

			    // width of the stars in pixels
			    starWidth: 12,

			    // spacing between stars in pixels
			    spaceWidth: 0,

			    numStars: 5,
			    isStatic: true,
			    step: false,
			    });

			});
		});

		
		function IsJsonString(str) {
		    try {
		        JSON.parse(str);
		    } catch (e) {
		        return false;
		    }
		    return true;
		}

		/**
		 * Salary map implementation
		 */
		var outerArray = [];
		var innerArray = [ 'State', 'AverageSalary', {
			type : 'string',
			role : 'tooltip'
		} ];
		outerArray[0] = innerArray;
		for (var i = 0; i < $scope.stateSalary.length; i++) {
			if ($scope.stateSalary[i].avgSalary != -1) {
				var innerArray = [ $scope.stateSalary[i].state, $scope.stateSalary[i].avgSalary, 'Average Salary: $' + $scope.stateSalary[i].avgSalary.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') ];
			} else {
				var innerArray = [ $scope.stateSalary[i].state, 187200, 'Average Salary: $' + '187,200 or more' ];
			}

			outerArray[i + 1] = innerArray;
		}

		var chart1 = {};
		chart1.type = "GeoChart";
		chart1.data = outerArray;
		/*
		 * chart1.data = [ ['State', 'AverageSalary', {type: 'string', role:
		 * 'tooltip'}], ['CA', 22, 'test'] ];
		 */

		chart1.options = {
			width : 480,
			/*
			 * width : 406, height : 297,
			 */
			region : "US",
			resolution : "provinces",
			colors : [ '#ffffff', '#02767b' ]
		};

		/*
		 * chart1.formatters = { number : [{ columnNum: 1, pattern: "$ #,##0.00" }] };
		 */

		$scope.chart = chart1;

		// mobile version
		var chart2 = {};
		chart2.type = "GeoChart";
		chart2.data = outerArray;

		chart2.options = {
			width : 300,
			/*
			 * width : 406, height : 297,
			 */
			region : "US",
			resolution : "provinces",
			colors : [ '#ffffff', '#02767b' ]
		};

		$scope.chartMobile = chart2;

		$scope.logout = function() {
			AuthenticationService.ClearCredentials();
		};

		$scope.showLoginModal = function() {
			LoginModalService.show({}, {});
		}

		/**
		 * Bar chart Implementation
		 */
		
		var barColor = ['blue1', 'blue2', 'blue3', 'blue4', 'purple1', 'purple2', 'purple3'];

		function shortenEducationName(education) {
//			Delete “or the equivalent” after High School Diploma. 
			if(education.indexOf("High School Diploma or the equivalent") != -1) {
				return "High School Diploma";
			}
//			Change "Less than a high school diploma" to "Some High School"
			if(education.indexOf("Less than a High School") != -1) {
				return "Some High School";
			}
//			Change “Some College Education” to “Some College"
			if(education.indexOf("Some College Courses") != -1) {
				return "Some College";
			}
//			Change "Post Baccalaureate Certificate" to "Post Baccalaureate"
			if(education.indexOf("Post Baccalaureate Certificate") != -1) {
				return "Post Baccalaureate";
			}
//			Change “Associate's Degree (or other 2-year degree)” to "Associate's Degree"
			if(education.indexOf("Associate's Degree (or other 2-year degree)") != -1) {
				return "Associate's Degree";
			}
			return education;
		}
		
		
		$scope.barList= [];
		for (var i = 0; i < $scope.educationLevel.length; i++) {
			var educationValue = Math.round($scope.educationLevel[i].educationLvlVal);
			if(educationValue === 0) {
				console.log(educationValue);
				continue;
			}
			
			var data = {};
			var education = shortenEducationName($scope.educationLevel[i].educationDesc);
			
			data['school'] = education;
			data['value'] = educationValue;
			data['color'] = barColor.shift();
			

			// Just in case there are more than 7 education, re-use color
			barColor.push(data['color']);

			$scope.barList.push(data);
		}
		


		/**
		 * Pie chart Implementation
		 */

		
//		$scope.chartObject = {};
//		var col = [ {
//			id : "e",
//			label : "Education",
//			type : "string"
//		}, {
//			id : "v",
//			label : "Value",
//			type : "number"
//		} ];
//		var row = [];
//
//		for (var i = 0; i < $scope.educationLevel.length; i++) {
//			var c = {
//				c : [ {
//					v : $scope.educationLevel[i].educationDesc
//				}, {
//					v : $scope.educationLevel[i].educationLvlVal
//				} ]
//			};
//			row.push(c);
//		}
//
//		$scope.chartObject.data = {
//			"cols" : col,
//			"rows" : row
//		};
//
//		$scope.chartObject.type = "PieChart";
//		$scope.chartObject.options = {
//			colors : [ '#02767b', '#29afb5', '#8bd6d9', '#cee3e7', '#9593b8', '#2f2a6a', '#5c5792', '#8c5050', '#885a4c', '#d69785' ]
//		};
//
//		$scope.chartPie = $scope.chartObject;

	}, function errorCallback(response) {
		alert("error, try refreshing");
	});

	$scope.relatedCareers = function(onetSoc) {
		OccufindSearchService.selectedOnetSoc.push(onetSoc);
		/* $route.reload(); */
		$location.url('/occufind-occupation-details/' + onetSoc);

	}

	// direct to military more details page
	$scope.militaryMoreDetails = function() {
		$location.url('/occufind-military-details/' + $route.current.params.socId);
	}

	// direct to school more details page
	$scope.schoolMoreDetails = function() {
		$location.url('/occufind-education-details/' + $route.current.params.socId);
	}

	// direct to school more details page
	$scope.employmentMoreDetails = function() {
		$location.url('/occufind-employment-details/' + $route.current.params.socId);
	}

	$scope.findCertifications = function() {
		CertificationListModalService.show({
			certs : $scope.occupationCertificates,
			occupationTitle : $scope.occupationTitle
		}, {});
	}

	$scope.findLicenses = function() {
		LicensesListModalService.show({
			socId : $route.current.params.socId,
			occupationTitle : $scope.occupationTitle
		}, {});
	}

	$scope.findApprenticeship = function() {
		ApprenticeshipModalService.show({
			socId : $route.current.params.socId,
			occupationTitle : $scope.occupationTitle
		}, {});
	}
	

	$scope.orderByService = function(item) {
		if (item.serviceCode == 'A') {
			return 1;
		} else if (item.serviceCode == 'M') {
			return 2;
		} else if (item.serviceCode == 'N') {
			return 3;
		} else if (item.serviceCode == 'F') {
			return 4;
		} else if (item.serviceCode == 'C') {
			return 5;
		} else if (item.serviceCode == 'G') {
			return 6;
		} else {
			return 7;
		}
	}
	

	/**
	 * Opens modal popup for description for each section.
	 */
	$scope.openDescription = function(section) {

		var headerTitle, bodyText;
		if (section == 'interest-codes') {
			headerTitle = "Interest Codes";
			bodyText = "RIASEC codes represent the interest codes most closely related to each occupation. Evaluate how well the interest codes for this occupation match your interests."
		} else if(section == 'skill-importance'){
			headerTitle = 'Skill Importance Ratings';
			bodyText = 'These ratings show the relative importance of Verbal, Math, and Science/Technical skills for the job.  Evaluate how well the Skill Importance Ratings match your Career Exploration Scores.';
		} else if(section == 'what-they-do'){
			headerTitle = 'What They Do';
			bodyText = 'This is a list of tasks performed on the job.';
		} else if(section == 'how-to-get-there'){
			headerTitle = 'How to Get There';
			bodyText = 'There may be different ways to get started in the career you want.  Learn about the education, credentials, licenses, apprenticeships, and military opportunities below.';
		} else if(section == 'education'){
			headerTitle = 'Education';
			bodyText = ' This section describes the levels of education seen as necessary to get this job as reported by people who work in the job. '+
					'  You can find a list of institutions that offer degrees related to this occupation under College Info.  '+
						' These totals are rounded to the nearest percent. The actual descriptions of levels of education included are as follows:<br/><br/>  '+
						' <ul> '+
							' <li>Less than a High School Diploma </li> '+
							' <li>High School Diploma or the equivalent (for example GED)</li> '+
							' <li>Post-Secondary Certificate</li> '+
							' <li>Some College Courses</li> '+
							' <li>Associate\'s Degree (or other 2-year degree)</li> '+
							' <li>Bachelor\'s Degree</li> '+
							' <li>Post-Baccalaureate Certificate</li> '+
							' <li>Master\'s Degree</li> '+
							' <li>Post-Master\'s Certificate</li> '+
							' <li>First Professional Degree</li> '+
							' <li>Doctoral Degree</li> '+
							' <li>Post-Doctoral Training</li> '+
						' </ul>';
		} else if(section == 'credentials'){
			headerTitle = 'Credentials';
			bodyText = 'Certification, licensure, and/or apprenticeships are possible paths to this career. Select Find Certification, Find Licenses, and Find Apprenticeships to explore these options.';
		} else if(section == 'mil-offering'){
			headerTitle = 'Military Services Offering this Occupation';
			bodyText = 'This job is available in the Military.  Select More Details to learn more about which branches offer related opportunities.';
		} else if(section == 'emp-stats'){
			headerTitle = 'Employment Statistics';
			bodyText = 'This section illustrates the average salary for this occupation by state. Select More Details to learn more about the national employment outlook.';
		} else if(section == 'poverty-level'){
			headerTitle = 'Poverty Level';
			bodyText = 'This number is issued each year by the Department of Health and Human Services (HHS). It represents the poverty guidelines for a single individual living in the 48 contiguous states. The number varies for citizens of Alaska and Hawaii, and increases with each additional member of the household.';
		} else{
			return 0;
		}

		var modalOptions = {
			headerText : headerTitle,
			bodyText : bodyText
		};
		
		var screenSize = 'sm';
		if(section == 'education') {
			screenSize = 'md';
		}

		modalService.showModal({
			size : screenSize
		}, modalOptions);
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]).directive('showMore', function() {
	  return {
	        restrict: 'A',
	        transclude: true,
	        template: [
	            '<div class="show-more-container"><ng-transclude></ng-transclude></div>',
	            '<a href="#" class="show-more-expand">Show More ...</a>',
	            '<a href="#" class="show-more-collapse">... Show Less</a>',
	        ].join(''),
	        link: function(scope, element, attrs, controller) {
	            var maxHeight = 45;
	            var initialized = null;
	            var containerDom = element.children()[0];
	            var $showMore = angular.element(element.children()[1]);
	            var $showLess = angular.element(element.children()[2]);

	            scope.$watch(function () {
	                // Watch for any change in the innerHTML. The container may start off empty or small,
	                // and then grow as data is added.
	                return containerDom.innerHTML;
	            }, function () {
	                if (null !== initialized) {
	                    // This collapse has already been initialized.
	                    return;
	                }

	                if (containerDom.clientHeight <= maxHeight) {
	                    // Don't initialize collapse unless the content container is too tall.
	                    return;
	                }

	                $showMore.on('click', function () {
	                    element.removeClass('show-more-collapsed');
	                    element.addClass('show-more-expanded');
	                    containerDom.style.height = null;
	                });

	                $showLess.on('click', function () {
	                    element.removeClass('show-more-expanded');
	                    element.addClass('show-more-collapsed');
	                    containerDom.style.height = maxHeight + 'px';
	                });

	                initialized = true;
	                $showLess.triggerHandler('click');
	            });
	        },
	  };
});
/**
 * Controller for occufind.html. This is the main search page for Occu-find.
 * Users can select up to two interest codes to search and filter by skill level
 * importance, hot jobs, STEM jobs, green jobs and search by occupation title
 */
cepApp.controller('OccufindOccupationSearchController', [ '$scope', '$http', '$location', 'OccufindSearchService', 'appUrl', '$rootScope', '$cookieStore','mediaCenterList', function($scope, $http, $location, OccufindSearchService, appUrl, $rootScope, $cookieStore, mediaCenterList) {

	$scope.mediaCenterList = mediaCenterList;
	$scope.skillSelected;


	$scope.stemOccupationFlag;
	$scope.brightOccupationFlag;
	$scope.greenOccupationFlag;
	$scope.hotOccupationFlag;

	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ; 
	}
	
	$scope.interestCodes = [ {
		name : 'Realistic',
		value : 'R',
		selected : false
	}, {
		name : 'Investigative',
		value : 'I',
		selected : false
	}, {
		name : 'Artistic',
		value : 'A',
		selected : false
	}, {
		name : 'Social',
		value : 'S',
		selected : false
	}, {
		name : 'Enterprising',
		value : 'E',
		selected : false
	}, {
		name : 'Conventional',
		value : 'C',
		selected : false
	}, ];

	$scope.skillCodes = [ {
		name : 'Verbal',
		value : 'verbalSkill',
		image : 'images/small-book1.png',
		selected : false
	}, {
		name : 'Math',
		value : 'mathSkill',
		image : 'images/small-book2.png',
		selected : false
	}, {
		name : 'Science/Technical',
		value : 'sciTechSkill',
		image : 'images/small-book3.png',
		selected : false
	}, ];

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 2;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Tracks how many skill selections are made to limit user selection to one
	 */
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.skillCheckChanged = function(item) {
		if (item.selected)
			$scope.skillChecked++;
		else
			$scope.skillChecked--;
	}

	/**
	 * Search button sets up the search service.
	 */
	$scope.occufindSearch = function() {

		OccufindSearchService.userSearch = {
			userSearch : {
				interestCodeOne : undefined,
				interestCodeTwo : undefined,
				brightOccupationFlag : false,
				greenOccupationFlag : false,
				stemOccupationFlag : false,
				hotOccupationFlag : false,
				userSearchString : undefined
			}
		}


		
		for (var i = 0; i < $scope.skillCodes.length; i++) {
			if($scope.skillCodes[i].selected == true) {
				$scope.skillSelected = $scope.skillCodes[i].value;
				break;
			}
		}

		OccufindSearchService.skillSelected = $scope.skillSelected;
		
		// push all interest code selection(s) to array
		var arraySelectedInterestCd = [];
		for (var i = 0; i < $scope.interestCodes.length; i++) {

			if ($scope.interestCodes[i].selected == true) {
				arraySelectedInterestCd.push($scope.interestCodes[i].value);
			}
		}

		// validate selection
		if (arraySelectedInterestCd < 1) {
			/*alert("Please select interest code(s)");
			return false;*/
		} else {

			// assign interest code selection(s) to occufind search service
			OccufindSearchService.userSearch.interestCodeOne = arraySelectedInterestCd[0];
			if (arraySelectedInterestCd.length == 2) {
				OccufindSearchService.userSearch.interestCodeTwo = arraySelectedInterestCd[1];
			}
		}

		// assign user's search selections to occufind search object
		OccufindSearchService.userSearch.brightOccupationFlag = $scope.brightOccupationFlag;
		OccufindSearchService.userSearch.greenOccupationFlag = $scope.greenOccupationFlag;
		OccufindSearchService.userSearch.stemOccupationFlag = $scope.stemOccupationFlag;
		OccufindSearchService.userSearch.userSearchString = $scope.searchString;
		OccufindSearchService.userSearch.hotOccupationFlag = $scope.hotOccupationFlag;
		
		$rootScope.globals.userSearch = OccufindSearchService.userSearch;
		$cookieStore.put('globals', $rootScope.globals);

		$location.path('/occufind-occupation-search-results');

	};
	
	$scope.occuKeyWordfindSearch = function() {
		OccufindSearchService.userSearch = {
			userSearch : {
				interestCodeOne : undefined,
				interestCodeTwo : undefined,
				brightOccupationFlag : false,
				greenOccupationFlag : false,
				stemOccupationFlag : false,
				hotOccupationFlag : false,
				userSearchString : undefined
			}
		}

		// assign user's search selections to occufind search object
		OccufindSearchService.userSearch.brightOccupationFlag = $scope.brightOccupationFlag;
		OccufindSearchService.userSearch.greenOccupationFlag = $scope.greenOccupationFlag;
		OccufindSearchService.userSearch.stemOccupationFlag = $scope.stemOccupationFlag;
		OccufindSearchService.userSearch.userSearchString = $scope.searchString;
		OccufindSearchService.userSearch.hotOccupationFlag = $scope.hotOccupationFlag;
		
		console.log(OccufindSearchService.userSearch.userSearchString);
		
		$rootScope.globals.userSearch = OccufindSearchService.userSearch;
		$cookieStore.put('globals', $rootScope.globals);

		$location.path('/occufind-occupation-search-results');
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);

cepApp.controller('OccufindOccupationSearchResultsController', [ '$scope', '$rootScope', 'OccufindSearchService', '$http', '$location', '$q', 'appUrl', 'occupationResults', '$route', '$routeParams', '$cookieStore', function($scope, $rootScope, OccufindSearchService, $http, $location, $q, appUrl, occupationResults, $route, $routeParams, $cookieStore) {

	$scope.selectedInterestCodeOne = OccufindSearchService.userSearch.interestCodeOne;
	$scope.selectedInterestCodeTwo = OccufindSearchService.userSearch.interestCodeTwo;
	$scope.selectedTitleOne = OccufindSearchService.getSelectedTitleOne(OccufindSearchService.userSearch.interestCodeOne);
	$scope.selectedTitleTwo = OccufindSearchService.getSelectedTitleTwo(OccufindSearchService.userSearch.interestCodeTwo);
	$scope.brightOccupationFlagDisplay = OccufindSearchService.userSearch.brightOccupationFlag;
	$scope.greenOccupationFlagDisplay = OccufindSearchService.userSearch.greenOccupationFlag;
	$scope.stemOccupationFlagDisplay = OccufindSearchService.userSearch.stemOccupationFlag;
	$scope.hotOccupationFlagDisplay = OccufindSearchService.userSearch.hotOccupationFlag;
	$scope.userSearchStringDisplay = OccufindSearchService.userSearch.userSearchString;
	
	if ( typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ; 
	} else {
		$scope.occuFindUserLoggedIn = true ; 
	}
	
	$scope.occupationResults = [];
	if (occupationResults != undefined) {
		$scope.occupationResults = occupationResults.data;
	}

	$scope.skillSelected = OccufindSearchService.skillSelected;
	console.log(OccufindSearchService.skillSelected);

	$scope.byRange = function(fieldName, minValue, maxValue) {
		/*
		 * if (minValue === undefined) minValue = Number.MIN_VALUE; if (maxValue
		 * === undefined) maxValue = Number.MAX_VALUE;
		 */

		return function predicateFunc(item) {
			return minValue <= item[fieldName] && item[fieldName] <= maxValue;
		};
	};

	/**
	 * Pagination
	 */
	$scope.totalItems = $scope.occupationResults.length;
	$scope.currentPage = OccufindSearchService.currentPage;
	//$scope.currentPage = $rootScope.globals.paginationPage == undefined ? 1 : $rootScope.globals.paginationPage;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 20;
	
	//TODO: use cookie storage
	$scope.setCurrentPage = function(currentPage) {
		OccufindSearchService.currentPage = currentPage;
		/*$rootScope.globals.paginationPage = currentPage
		$cookieStore.put('globals', $rootScope.globals);
		console.log($rootScope.globals);*/
	}

	/**
	 * Utilizes $watch on skillSelected. If variable changes, this function will
	 * be executed to help toggle the list view and populate the lists for skill
	 * importance.
	 */
	$scope.$watch('skillSelected', function() {
		OccufindSearchService.skillSelected = $scope.skillSelected;

		if ($scope.occupationResults != undefined) {

			// filter only if skill selection is made
			if ($scope.skillSelected == 'mathSkill' || $scope.skillSelected == 'verbalSkill' || $scope.skillSelected == 'sciTechSkill') {
				$scope.highFiltered = [];
				$scope.mediumFiltered = [];
				$scope.lowFiltered = [];
				var length = $scope.occupationResults.length;

				// filter high skill
				for (var i = 0; i < length; i++) {

					// determines which skill to filter
					var val;
					switch ($scope.skillSelected) {
					case 'mathSkill':
						val = $scope.occupationResults[i].mathSkill;
						break;
					case "verbalSkill":
						val = $scope.occupationResults[i].verbalSkill;
						break;
					case "sciTechSkill":
						val = $scope.occupationResults[i].sciTechSkill;
						break;
					}

					// filter by range
					if (4 <= val && val <= 5) {
						$scope.highFiltered.push($scope.occupationResults[i]);
					} else if (2.5 <= val && val <= 3.5) {
						$scope.mediumFiltered.push($scope.occupationResults[i]);
					} else if (0 <= val && val <= 2) {
						$scope.lowFiltered.push($scope.occupationResults[i]);
					} else {
						console.log("no data");
						console.log($scope.occupationResults[i]);
					}

				}

				// high filter pagination properties
				$scope.highCurrentPage = 1;
				$scope.highMaxSize = 4;
				$scope.highItemsPerPage = 20;

				$scope.setHighPage = function(pageNo) {
					$scope.highCurrentPage = pageNo;
				};

				// medium filter pagination properties
				$scope.mediumCurrentPage = 1;
				$scope.mediumMaxSize = 4;
				$scope.mediumItemsPerPage = 20;

				$scope.setMediumPage = function(pageNo) {
					$scope.mediumCurrentPage = pageNo;
				};

				// low filter pagination properties
				$scope.lowCurrentPage = 1;
				$scope.lowMaxSize = 4;
				$scope.lowItemsPerPage = 20;

				$scope.setLowPage = function(pageNo) {
					$scope.lowCurrentPage = pageNo;
				};
			}
		}

	});
	
	
	/**
	 * Tracks how many selections are made for skill selection section. Limit user selection to one.
	 */
	$scope.skillLimit = 1;
	$scope.skillChecked = 0;
	$scope.checkChangedLimitOne = function(item) {
		if (item.selected)
			$scope.skillChecked++;
		else
			$scope.skillChecked--;
	}
	
	// for skill selection checkbox 
	$scope.skillSelectionOptions = [ {
		name : 'Verbal',
		value : 'verbalSkill',
		selected : false
	}, {
		name : 'Math',
		value : 'mathSkill',
		selected : false
	}, {
		name : 'Science/Technical',
		value : 'sciTechSkill',
		selected : false
	} ];
	

	$scope.selectedOccupation = function(onetSoc, title, stem, bright, green, hot, interestCdOne, interestCdTwo, interestCdThree) {
		OccufindSearchService.selectedOnetSoc.push(onetSoc);
		OccufindSearchService.selectedOcupationTitle = title;
		OccufindSearchService.occupationGreen = green;
		OccufindSearchService.occupationStem = stem;
		OccufindSearchService.occupationBright = bright;
		OccufindSearchService.occupationHot = hot;
		OccufindSearchService.occupationInterestOne = interestCdOne;
		OccufindSearchService.occupationInterestTwo = interestCdTwo;
		OccufindSearchService.occupationInterestThree = interestCdThree;
		$location.url('/occufind-occupation-details/' + onetSoc);
	};

	/**
	 * Search feature
	 */
	$scope.stemOccupationFlag;
	$scope.brightOccupationFlag;
	$scope.greenOccupationFlag;
	$scope.hotOccupationFlag;

	$scope.interestCodes = [ {
		name : 'Realistic',
		value : 'R',
		selected : false
	}, {
		name : 'Investigative',
		value : 'I',
		selected : false
	}, {
		name : 'Artistic',
		value : 'A',
		selected : false
	}, {
		name : 'Social',
		value : 'S',
		selected : false
	}, {
		name : 'Enterprising',
		value : 'E',
		selected : false
	}, {
		name : 'Conventional',
		value : 'C',
		selected : false
	}, ];

	/**
	 * Tracks how many selections are made to limit user selection to two
	 */
	$scope.limit = 2;
	$scope.checked = 0;
	$scope.checkChanged = function(item) {
		if (item.selected)
			$scope.checked++;
		else
			$scope.checked--;
	}

	/**
	 * Search button sets up the search service.
	 */
	$scope.occufindSearch = function() {

		OccufindSearchService.userSearch = {
			userSearch : {
				interestCodeOne : undefined,
				interestCodeTwo : undefined,
				brightOccupationFlag : false,
				greenOccupationFlag : false,
				stemOccupationFlag : false,
				hotOccupationFlag : false,
				userSearchString : undefined
			}
		}

		// push skill selection value service object
		var isSkillSelected = false;
		for (var i = 0; i < $scope.skillSelectionOptions.length; i++) {

			if ($scope.skillSelectionOptions[i].selected == true) {
				isSkillSelected = true;
				OccufindSearchService.skillSelected = $scope.skillSelectionOptions[i].value;
			}
		}
		if (!isSkillSelected) {
			OccufindSearchService.skillSelected = undefined;
		}
		

		// push all interest code selection(s) to array
		var arraySelectedInterestCd = [];
		for (var i = 0; i < $scope.interestCodes.length; i++) {

			if ($scope.interestCodes[i].selected == true) {
				arraySelectedInterestCd.push($scope.interestCodes[i].value);
			}
		}

		// validate selection
		if (arraySelectedInterestCd < 1) {
			/*alert("Please select interest code(s)");
			return false;*/
		} else {

			// assign interest code selection(s) to occufind search service
			OccufindSearchService.userSearch.interestCodeOne = arraySelectedInterestCd[0];
			if (arraySelectedInterestCd.length == 2) {
				OccufindSearchService.userSearch.interestCodeTwo = arraySelectedInterestCd[1];
			}
		}

		// assign user's search selections to occufind search object
		OccufindSearchService.userSearch.brightOccupationFlag = $scope.brightOccupationFlag;
		OccufindSearchService.userSearch.greenOccupationFlag = $scope.greenOccupationFlag;
		OccufindSearchService.userSearch.stemOccupationFlag = $scope.stemOccupationFlag;
		OccufindSearchService.userSearch.userSearchString = $scope.searchString;
		OccufindSearchService.userSearch.hotOccupationFlag = $scope.hotOccupationFlag;

		$rootScope.globals.userSearch = OccufindSearchService.userSearch;
		$cookieStore.put('globals', $rootScope.globals);

		$route.reload();

	};
	
	/**
	 * Scroll to search box.
	 */
	$scope.scrollTo = function(id) {
		var elmnt = document.getElementById("search-sidebar");
	    elmnt.scrollIntoView();
	}
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

cepApp.controller('OccufindSchoolDetailsController', [ '$scope', '$rootScope', 'schoolDetails', 'occupationTitleDescription', '$route', 'modalService', function($scope, $rootScope, schoolDetails, occupationTitleDescription, $route, modalService) {

	$scope.occupationTitle = occupationTitleDescription.data.title;
	$scope.schoolDetails = schoolDetails.data;
	$scope.schoolTitle = $scope.schoolDetails.institutionName;
	$scope.socId = $route.current.params.socId;
	
	$scope.percentageAdmitted = $scope.schoolDetails.totalAdmitted / $scope.schoolDetails.totalApplicants * 100;

	if (typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ;
	} else {
		$scope.occuFindUserLoggedIn = true ;
	}

	$scope.openDescription = function(section) {
		var headerTitle, bodyText;
		if(section == 'rotc'){
			headerTitle = 'Reserve Officers Training Corps (ROTC)';
			bodyText = 'Reserve Officers Training Corps (ROTC) is a college scholarship program that prepares students to become military officers. Offered at more than'+
				' 1,000 colleges and universities, it can help cover the costs of tuition and expenses.  '+
				' <a href="https://www.careersinthemilitary.com/options-becoming-an-officer" target="_blank">Learn more about ROTC</a>.';
		} else{
			return 0;
		}
		
		var modalOptions = {
			headerText : headerTitle,
			bodyText : bodyText
		};
		
		var screenSize = 'sm';
		if(section == 'education') {
			screenSize = 'md';
		}

		modalService.showModal({
			size : screenSize
		}, modalOptions);
	}
	
	
	/**
	 * Pie chart
	 */
	$scope.myChartObject = {};

	$scope.myChartObject.type = "PieChart";


	$scope.myChartObject.data = {
		"cols" : [
			{
				id : "t",
				label : "Topping",
				type : "string"
			},
			{
				id : "s",
				label : "Slices",
				type : "number"
			}
		],
		"rows" : [
			{
				c : [
					{
						v : "2015"
					},
					{
						v : $scope.schoolDetails.retentionRate
					},
				]
			},
			{
				c : [
					{
						v : "2016"
					},
					{
						v : 100 - $scope.schoolDetails.retentionRate
					},
				]
			}
		]
	};

	$scope.myChartObject.options = {
		'legend' : 'none',
		tooltip : {
			trigger : 'none'
		},
		slices : {
			0 : {
				color : '00acad'
			},
			1 : {
				color : 'transparent'
			}
		}
	};
	
	/**
	 * Test score graph
	 */
	$scope.satCritica25Percent = 268*$scope.schoolDetails.satCritica25Percent/800;
	$scope.satCritica75Percent = 268*$scope.schoolDetails.satCritica75Percent/800;
	
	$scope.satMath25Percent = 268*$scope.schoolDetails.satMath25Percent/800;
	$scope.satMath75Percent = 268*$scope.schoolDetails.satMath75Percent/800;

	$scope.actCritical25Percent = 268*$scope.schoolDetails.actCritical25Percent/31;
	$scope.actCritical75Percent = 268*$scope.schoolDetails.actCritical75Percent/31;
	
	/**
	 * Degrere and major modal popup.
	 */
	$scope.majorPopup = function(title, description) {
		var customModalOptions = {
				headerText : title,
				bodyText : description
			};
			var customModalDefaults = {size : 'lg'};
			modalService.show(customModalDefaults, customModalOptions);
	}
	

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);
cepApp.factory('CareerOneStopFactory', [ '$http', 'appUrl', function($http, appUrl) {

	var careerOneStopFactory = {};

	careerOneStopFactory.getOccupationCertificates = function(onetSoc) {
		var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
		return $http.get(appUrl + 'CareerOneStop/cert/' + onetOccupationCode);
	}
	
	careerOneStopFactory.getOccupationCertificateDetails = function(certId) {		
		return $http.get(appUrl + 'CareerOneStop/certDetail/' +  certId.trim() );
	}

	careerOneStopFactory.getOccupationLicenses = function(onetSoc, state) {
		var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
		var careerOneStopUrl = appUrl + 'CareerOneStop/license/'  + onetOccupationCode + '/' + state.trim() ; 
		var returnValue;
			
		
		
		
	    $http.get(careerOneStopUrl )
    	.then(	function (response) {
    		// $scope.Available = response.data; 
    	  //  window.alert(JSON.stringify(response.data));
    	console.log('success'); 
    });
		
		
		
		
		
		
		
		
		
		
		
//        $http.get(careerOneStopUrl).then(function(response){
//            // With the data succesfully returned, call our callback
//            returnValue = response.data;
//        });
		
		return returnValue;
	}
	
	careerOneStopFactory.getOccupationLicensesDetail  = function(onetSoc, licenseId , state) {
		var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
		
		return $http.get(appUrl + 'CareerOneStop/licenseDetail/'  + onetOccupationCode + '/' + licenseId.trim()  + '/' + state.trim() );
	}
	
	
	careerOneStopFactory.getOccupationApprenticeship  = function(onetSoc, state) {
		var onetOccupationCode = onetSoc.replace(/-/g, '').replace(/\./g, '').trim();
		return $http.get(appUrl + 'CareerOneStop/apprenticeship/'  + onetOccupationCode + '/' + state.trim() );
	}
	
	return careerOneStopFactory;

} ]);
cepApp.service('occufindMilitaryDetailModalService', [ '$uibModal', 'FYIScoreService', '$location', 'FYIRestFactory', '$rootScope', '$q', function($uibModal, FYIScoreService, $location, FYIRestFactory, $rootScope, $q) {

	var modalDefaults = {
		animation : true,
		size : 'lg',
		templateUrl : 'cep/components/occufind/page-partials/occufind-military-details-modal.html'
	};

	var modalOptions = {};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.militaryMoreDetails = customModalOptions.militaryMoreDetails;
				$scope.index = customModalOptions.index;

				$scope.modalOptions.submit = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('OccufindSearchService', ['$cookieStore', '$rootScope', function($cookieStore, $rootScope) {
	
	var occufindSearchService = {
		userSearch : {
			interestCodeOne : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.interestCodeOne ? $cookieStore.get('globals').userSearch.interestCodeOne : undefined,
			interestCodeTwo : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.interestCodeTwo ? $cookieStore.get('globals').userSearch.interestCodeTwo : undefined,
			brightOccupationFlag : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.brightOccupationFlag ? $cookieStore.get('globals').userSearch.brightOccupationFlag : false,
			greenOccupationFlag : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.greenOccupationFlag ? $cookieStore.get('globals').userSearch.greenOccupationFlag : false,
			stemOccupationFlag : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.stemOccupationFlag ? $cookieStore.get('globals').userSearch.stemOccupationFlag : false,
			hotOccupationFlag : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.hotOccupationFlag ? $cookieStore.get('globals').userSearch.hotOccupationFlag : false,
			userSearchString : $cookieStore.get('globals') && $cookieStore.get('globals').userSearch && $cookieStore.get('globals').userSearch.userSearchString ? $cookieStore.get('globals').userSearch.userSearchString : undefined
		},
		skillSelected : undefined,
		selectedOnetSoc : [],
		selectedOcupationTitle : undefined,
		occupationGreen : false,
		occupationStem : false,
		occupationBright : false,
		occupationHot : false,
		occupationInterestOne : undefined,
		occupationInterestTwo: undefined,
		occupationInterestThree: undefined,
		currentPage : 1,
		relatedCarreers : []
	};
	
	occufindSearchService.getOnetSoc = function(){
		return occufindSearchService.selectedOnetSoc[occufindSearchService.selectedOnetSoc.length - 1];
	};

	occufindSearchService.getSelectedTitleOne = function(interestCd) {
		if (interestCd != null) {
			switch (interestCd) {
			case "R":
				return "Realistic";
			case "I":
				return "Investigative";
			case "A":
				return "Artistic";
			case "S":
				return "Social";
			case "E":
				return "Enterprising";
			case "C":
				return "Conventional";
			}
		}
	};

	occufindSearchService.getSelectedTitleTwo = function(interestCd) {
		if (interestCd != null) {
			switch (interestCd) {
			case "R":
				return "Realistic";
			case "I":
				return "Investigative";
			case "A":
				return "Artistic";
			case "S":
				return "Social";
			case "E":
				return "Enterprising";
			case "C":
				return "Conventional";
			}
		}
	};

	return occufindSearchService;

	/*
	 * var occupationList; var interestSelected; var skillSelected; var
	 * skillFilterView; var selectedOccupationTitle; var selectedOnetSoc; var
	 * occupationDescriptionASK; var militaryResource; var oohResource; var
	 * stateSalary;
	 * 
	 * this.getStateSalary = function() { return stateSalary; }
	 * 
	 * this.getOohResource = function() { return oohResource; }
	 * 
	 * this.getmilitaryResource = function() { return militaryResource; }
	 * 
	 * this.getOccupationDescriptionASK = function() { return
	 * occupationDescriptionASK; }
	 * 
	 * this.getSelectedOnetSoc = function() { return selectedOnetSoc; }
	 * 
	 * this.getSelectedOccupationTitle = function() { return
	 * selectedOccupationTitle; }
	 * 
	 * this.getOccupationList = function() { return occupationList; }
	 * 
	 * this.getInterestSelected = function() { return interesteSelected; }
	 * 
	 * this.getSkillSelected = function() { return skillSelected; }
	 * 
	 * this.getSkillFilterText = function() { switch (this.skillSelected) { case
	 * "V": return "verbalSkill"; case "M": return "mathSkill"; case "S": return
	 * "sciTechSkill"; } }
	 * 
	 * this.getSkillFilterView = function() {
	 * 
	 * if (this.skillSelected === undefined) { skillFilterView = false; } else {
	 * skillFilterView = true; }
	 * 
	 * return skillFilterView; }
	 * 
	 * this.getInterestText = function() { switch (this.interestSelected) { case
	 * "R": return "Realistic"; case "I": return "Investigative"; case "A":
	 * return "Artistic"; case "S": return "Social"; case "E": return
	 * "Enterprising"; case "C": return "Conventional"; } }
	 */
}]);

cepApp.factory('OccufindRestFactory', [ '$http', 'appUrl', function($http, appUrl) {

	var occufindRestFactory = {};

	occufindRestFactory.getOccupationResults = function(occufindSearchJSON) {
		return $http.post(appUrl + 'occufind/getOccufindSearch', occufindSearchJSON);
	}

	occufindRestFactory.getOccupationTitleDescription = function(onetSoc) {
		return $http.get(appUrl + 'occufind/occupationTitleDescription/' + onetSoc + '/');
	}

	occufindRestFactory.getOccupationInterestCodes = function(onetSoc) {
		return $http.get(appUrl + 'occufind/OccupationInterestCodes/' + onetSoc + '/');
	}

	occufindRestFactory.getHotGreenStemFlags = function(onetSoc) {
		return $http.get(appUrl + 'occufind/HotGreenStemFlags/' + onetSoc + '/');
	}

	occufindRestFactory.getServiceOfferingCareer = function(onetSoc) {
		return $http.get(appUrl + 'occufind/ServiceOfferingCareer/' + onetSoc + '/');
	}

	occufindRestFactory.getRelatedCareerCluster = function(onetSoc) {
		return $http.get(appUrl + 'occufind/RelatedCareerCluster/' + onetSoc + '/');
	}

	occufindRestFactory.getSchoolMoreDetails = function(onetSoc) {
		return $http.get(appUrl + 'occufind/SchoolMoreDetails/' + onetSoc + '/');
	}

	occufindRestFactory.getSchoolMajors = function(onetSoc) {
		return $http.get(appUrl + 'occufind/SchoolMajors/' + onetSoc + '/');
	}

	occufindRestFactory.getStateSalaryYear = function() {
		return $http.get(appUrl + 'occufind/StateSalaryYear');
	}

	occufindRestFactory.getEmploymentMoreDetails = function(trimmedOnetSoc) {
		return $http.get(appUrl + 'occufind/EmploymentMoreDetails/' + trimmedOnetSoc + '/');
	}

	occufindRestFactory.getMilitaryMoreDetails = function(onetSoc) {
		return $http.get(appUrl + 'occufind/MilitaryMoreDetails/' + onetSoc + '/');
	}

	occufindRestFactory.getMilitaryHotJobs = function(onetSoc) {
		return $http.get(appUrl + 'occufind/MilitaryHotJobs/' + onetSoc + '/');
	}

	occufindRestFactory.getNationalSalary = function(trimmedOnetSoc) {
		return $http.get(appUrl + 'occufind/NationalSalary/' + trimmedOnetSoc + '/');
	}
	
	occufindRestFactory.getBLSTitle = function(trimmedOnetSoc) {
		return $http.get(appUrl + 'occufind/BLSTitle/' + trimmedOnetSoc + '/');
	}

	occufindRestFactory.getMoreResources = function(onetSoc) {
		return $http.get(appUrl + 'occufind/OccufindMoreResources/' + onetSoc + '/');
	}
	
	occufindRestFactory.getAlternateView = function(onetSoc) {
		return $http.get(appUrl + 'occufind/OccufindAlternativeView/' + onetSoc + '/');
	}
	
	occufindRestFactory.getImageAltText = function(onetSoc) {
		return $http.get(appUrl + 'occufind/ImageAltText/' + onetSoc + '/');
	}
	
	occufindRestFactory.getJobZone = function(onetSoc) {
		return $http.get(appUrl + 'occufind/job-zone/' + onetSoc + '/');
	}
	
	occufindRestFactory.getCertificateExplanation = function(onetSoc) {
		return $http.get(appUrl + 'occufind/certificateExplanation/' + onetSoc + '/');
	}
		
	occufindRestFactory.getAlternateTitles = function(onetSoc) {
		return $http.get(appUrl + 'occufind/alternateTitles/' + onetSoc + '/');
	}
	
	occufindRestFactory.getSchoolProfile = function(unitId) {
		return $http.get(appUrl + 'occufind/SchoolProfile/' + unitId + '/');
	}
	
	return occufindRestFactory;

} ]);
cepApp.controller('PasswordRestController',
	['$route', '$rootScope', '$scope', '$location', '$http', '$q', 'AuthenticationService', function($route, $rootScope, $scope, $location, $http, $q,
			AuthenticationService) {
	
    // Controller Init function .....
    const BASE_URL = "/CEP/";   
	$scope.restCredentials = {};
	$scope.restCredentials.resetId = '';
	$scope.restCredentials.username = '';
	$scope.registrationLabel = ''; 

	var absoluteUrl = $location.absUrl();
	absoluteUrl = absoluteUrl.replace("%20","+"); 
	absoluteUrl = absoluteUrl.replace("%3D","="); 
	var questionMarkPosition = absoluteUrl.indexOf("?");
	var urlParametersString = absoluteUrl.substr(questionMarkPosition + 1 );
	var urlParameters = urlParametersString.split("+");

	var resetParameters = {};
	for ( i = 0 ; i < urlParameters.length; i++ ) {
		var nameValue = urlParameters[i].split("=");
		resetParameters[nameValue[0]] = nameValue[1]; 
		$scope.restCredentials[nameValue[0]] = nameValue[1];
	}
	
		angular.element(document).ready(function() {

	});


	    $scope.ResetPassword2 = function() {
	        $http.post(BASE_URL + 'rest/applicationAccess/passwordReset', 
	        	$scope.restCredentials )
	        .then(	function (response) { 
	        	$scope.accForInsert = response.data; 
	           // window.alert(JSON.stringify(response.data));
	        	} );
	    };

	           
		// @RequestMapping(value = "/applicationAccess/register",
		// method = RequestMethod.POST)
		$scope.ResetPassword = function() {
			

			// Register Modal
			var data = {
				email : $scope.restCredentials.username, // document.getElementById("registerEmail").value,
				accessCode : "", // document.getElementById("registerAccessCode").value,
				password : "",
				resetKey : $scope.restCredentials.resetId
			};

			var pass1 = $scope.restCredentials.Password1;
			var pass2 = $scope.restCredentials.Password2;

			if (pass1.length <= 7) {
				$scope.registrationLabel = "Password too short!";
				return;
			}
			if (pass1 != pass2) {
				$scope.registrationLabel = "Passwords don't match!";
				$scope.restCredentials.Password1 = '';
				$scope.restCredentials.Password2 = '';
				return;
			}

			var url = BASE_URL + 'rest/applicationAccess/passwordReset';

			var temp = $scope.crypt(
				pass1,
				function(hashRet) {
					data.password = hashRet;
					$http.post(url, data)
						.then(
								function(response) {
									$scope.registrationLoginStatus = response.data;
									// Success ..
									// Open Login
									if ($scope.registrationLoginStatus.statusNumber == 0) {
										
										// Set the location to dashboard
										$location.path('/');
										$location.replace();
										$location.url($location.path());
									} else { // Error
												// ..
												// update
												// registration
												// Label
										$scope.registrationLabel = $scope.registrationLoginStatus.status
									}
								});
				});
		};


		        
		// *** Begin ***
		var id;
		var bcrypt = new bCrypt();

		var begin = '';
		function result(hashRet) {
			console.log(hashRet);
			$scope.hash = hashRet;
		}

		$scope.ckpassword = function(password, hash,
				callbackFunction) {
			try {

				bcrypt.checkpw(password, hash, callbackFunction,
						null);
			} catch (err) {
				alert(err);
				return;
			}
		};

		$scope.crypt = function(password, callbackFunction) {
			var salt = '';
			var rounds = 10;

			// Auto generate the salt
			try {
				salt = bcrypt.gensalt(rounds);
			} catch (err) {
				alert(err);
				return;
			}

			try {
				// bcrypt.hashpw( password , salt, result , null );
				bcrypt.hashpw(password, salt, callbackFunction,
						null);
			} catch (err) {
				alert(err);
				return;
			}
		}
		// ***  End  ***
}]);
cepApp.controller('PortfolioController', [ 
	'$scope', '$rootScope', '$window', '$q', 'PortfolioRestFactory', 'workExperience', 'education', 'achievement', 'interest', 'skill', 'workValue', 'volunteer', 
	'actScore', 'asvabScore', 'fyiScore', 'satScore', 'otherScore', 'UserFactory', 'favoriteList', 'portfolioModal', 'careerClusterFavoriteList', 'plan', 
	'resource', 'modalService', 'UserInfo', 'citmFavoriteOccupations', 'schoolFavoriteList', 'ssoUrl',
	function($scope, $rootScope, $window, $q, PortfolioRestFactory, workExperience, education, achievement, interest, skill, workValue, volunteer, 
	actScore, asvabScore, fyiScore, satScore, otherScore, UserFactory, favoriteList, portfolioModal, careerClusterFavoriteList, plan, resource, modalService, UserInfo, citmFavoriteOccupations, schoolFavoriteList, ssoUrl) {

	if(UserInfo.data.length > 0){
		$scope.fullName = UserInfo.data[0].name;
		$scope.grade = UserInfo.data[0].gpa;
	} else {
		$scope.fullName = '';
		$scope.grade = '';
	}
	
	$scope.ssoUrl = ssoUrl;
	
	$scope.citmFavoriteOccupations = citmFavoriteOccupations.data;

	/**
	 * Saves name and grade to database
	 */
	$scope.syncData = function() {
		$scope.userInfo = {
			userId : $rootScope.globals.currentUser.userId,
			name : $scope.fullName,
			gpa : $scope.grade,
		};

		var promise = PortfolioRestFactory.updateInsertUserInfo($scope.userInfo);
		promise.then(function(response) {
		}, function(reason) {
			console.log("userinfo failed to saved");
		}, function(update) {
		});
	}
	
	$scope.domainName = resource;

	$scope.favoriteList = favoriteList;
	$scope.careerClusterFavoriteList = careerClusterFavoriteList;
	$scope.schoolFavoriteList = schoolFavoriteList;
	

	/**
	 * User's selection for sections to include in print.
	 */
	$scope.workExperiencePrint = true;
	$scope.educationPrint = true;
	$scope.testScorePrint = true;
	$scope.achievementPrint = true;
	$scope.skillsPrint = true;
	$scope.volunteerPrint = true;
	$scope.interestPrint = true;
	$scope.workValuesPrint = true;
	$scope.favoriteOccupationPrint = true;
	$scope.favoriteCitmOccupationPrint = true;
	$scope.favoriteCareerClusterPrint = true;
	$scope.favoriteSchoolPrint = true;
	$scope.planPrint = true;

	/**
	 * Check all sections to print.
	 */
	$scope.selectAll = function() {
		$scope.workExperiencePrint = true;
		$scope.educationPrint = true;
		$scope.testScorePrint = true;
		$scope.achievementPrint = true;
		$scope.skillsPrint = true;
		$scope.volunteerPrint = true;
		$scope.interestPrint = true;
		$scope.workValuesPrint = true;
		$scope.favoriteOccupationPrint = true;
		$scope.favoriteCitmOccupationPrint = true;
		$scope.favoriteCareerClusterPrint = true;
		$scope.favoriteSchoolPrint = true;
		$scope.planPrint = true;
	}

	/**
	 * Uncheck all sections.
	 */
	$scope.selectNone = function() {
		$scope.workExperiencePrint = false;
		$scope.educationPrint = false;
		$scope.testScorePrint = false;
		$scope.achievementPrint = false;
		$scope.skillsPrint = false;
		$scope.volunteerPrint = false;
		$scope.interestPrint = false;
		$scope.workValuesPrint = false;
		$scope.favoriteOccupationPrint = false;
		$scope.favoriteCitmOccupationPrint = false;
		$scope.favoriteCareerClusterPrint = false;
		$scope.favoriteSchoolPrint = true;
		$scope.planPrint = false;
	}

	/**
	 * Tips modal popup
	 */
	$scope.tipsModal = function() {
		portfolioModal.show({}, {});
	}

	/*
	 * Work experience
	 */

	$scope.showWorkExperienceInputs = false;
	$scope.showUpdateWorkExperienceInputs = false;
	$scope.workExperience = workExperience.data;
	$scope.savingWorkExperience = false;
	$scope.workExperienceEndDateRequired = true;
	$scope.updateWorkExperienceEndDateDisabled = true;
	$scope.workExperiencObject = {
		userId : $rootScope.globals.currentUser.userId,
		companyName : undefined,
		jobTitle : undefined,
		startDate : undefined,
		endDate : undefined,
		jobDescription : undefined,
		id : undefined
	};
	$scope.updateWorkExperienceObject;

	/**
	 * Setup for updating work experience input.
	 */
	$scope.setupUpdateWorkExperience = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedWorkEditIndex = index;
		$scope.showWorkExperienceInputs = true;
		$scope.showUpdateWorkExperienceInputs = true;
		var length = $scope.workExperience.length;
		for (var i = 0; i < length; i++) {
			if ($scope.workExperience[i].id == id) {
				$scope.updateWorkExperienceObject = angular.copy($scope.workExperience[i]);
			}
		}
		$scope.updateWorkExperienceEndDateDisabled = $scope.updateWorkExperienceObject.endDate == 'Present' ? true : false;
		console.log($scope.updateWorkExperienceObject);
	}

	/**
	 * Update work experience.
	 */
	$scope.updateWorkExperience = function() {
		$scope.savingWorkExperience = true;
		var promise = PortfolioRestFactory.updateWorkExperience($scope.updateWorkExperienceObject);
		promise.then(function(response) {
			$scope.savingWorkExperience = false;
			$scope.workExperienceEndDateRequired = true;
			$scope.updateWorkExperienceEndDateDisabled = true;
			$scope.showWorkExperienceInputs = false;
			$scope.showUpdateWorkExperienceInputs = false;
			$scope.selectedWorkEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.workExperience.length;
			for (var i = 0; i < length; i++) {
				if ($scope.workExperience[i].id == $scope.updateWorkExperienceObject.id) {
					$scope.workExperience[i] = $scope.updateWorkExperienceObject;
				}
			}
		}, function(reason) {
			$scope.savingWorkExperience = false;
			console.log(reason);
			alert("Failed to update work experience. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert work experience.
	 */
	$scope.insertWorkExperience = function(form) {
		
		if(!form.$valid){
			return false;
		}
		form.$setPristine();
		$scope.savingWorkExperience = true;
		var newObject = angular.copy($scope.workExperiencObject);
		var promise = PortfolioRestFactory.insertWorkExperience(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingWorkExperience = false;
			$scope.workExperienceEndDateRequired = true;
			$scope.showWorkExperienceInputs = false;
			$scope.workExperience.push(response.data);

			// clear form data
			$scope.workExperiencObject = {
				userId : $rootScope.globals.currentUser.userId,
				companyName : undefined,
				jobTitle : undefined,
				startDate : undefined,
				endDate : undefined,
				jobDescription : undefined,
				id : undefined
			};
		}, function(reason) {
			$scope.savingWorkExperience = false;
			console.log(reason);
			alert("Failed to save work experience. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete work experience
	 */
	$scope.deleteWorkExperience = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteWorkExperience($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove work experience locally
			var length = $scope.workExperience.length;
			for (var i = 0; i < length; i++) {
				if ($scope.workExperience[i].id == id) {
					$scope.workExperience.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateWorkExperienceInputs = false;
			$scope.showWorkExperienceInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete work experience. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Education
	 */
	$scope.gpaRegEx = '\^((\d+\.?|\.(?=\d))?\d{0,3})$';
	$scope.showEducationInputs = false;
	$scope.education = education.data;
	$scope.savingEducation = false;
	$scope.educationObject = {
		userId : $rootScope.globals.currentUser.userId,
		schoolName : undefined,
		gpa : undefined,
		startDate : undefined,
		endDate : undefined,
		activities : undefined,
		achievements : undefined,
		id : undefined
	};
	$scope.updateWorkExperienceObject;

	/**
	 * Setup for updating education input.
	 */
	$scope.setupUpdateEducation = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedEducationEditIndex = index;
		$scope.showEducationInputs = true;
		$scope.showUpdateEducationInputs = true;
		var length = $scope.education.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.education[i]);
			if ($scope.education[i].id == id) {
				$scope.updateEducationObject = angular.copy($scope.education[i]);
			}
		}
		console.log($scope.updateEducationObject);
	}

	/**
	 * Update education.
	 */
	$scope.updateEducation = function() {
		$scope.savingEducation = true;
		var promise = PortfolioRestFactory.updateEducation($scope.updateEducationObject);
		promise.then(function(response) {
			$scope.savingEducation = false;
			// $scope.workExperienceEndDateRequired = true;
			$scope.showEducationInputs = false;
			$scope.showUpdateEducationInputs = false;
			$scope.selectedEducationEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.education.length;
			for (var i = 0; i < length; i++) {
				if ($scope.education[i].id == $scope.updateEducationObject.id) {
					$scope.education[i] = $scope.updateEducationObject;
				}
			}
		}, function(reason) {
			$scope.savingEducation = false;
			console.log(reason);
			alert("Failed to update education. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert education.
	 */
	$scope.insertEducation = function() {
		$scope.savingEducation = true;
		var newObject = angular.copy($scope.educationObject);
		var promise = PortfolioRestFactory.insertEducation(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingEducation = false;
			$scope.showEducationInputs = false;
			$scope.education.push(response.data);

			// clear form data
			$scope.educationObject = {
				userId : $rootScope.globals.currentUser.userId,
				schoolName : undefined,
				gpa : undefined,
				startDate : undefined,
				endDate : undefined,
				activities : undefined,
				achievements : undefined,
				id : undefined
			};

		}, function(reason) {
			$scope.savingEducation = false;
			console.log(reason);
			alert("Failed to save education data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete education
	 */
	$scope.deleteEducation = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteEducation($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove education locally
			var length = $scope.education.length;
			for (var i = 0; i < length; i++) {
				if ($scope.education[i].id == id) {
					$scope.education.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateEducationInputs = false;
			$scope.showEducationInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete education data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Achievements
	 */
	$scope.showAchievementInputs = false;
	$scope.achievement = achievement.data;
	$scope.savingAchievement = false;
	$scope.achievementObject = {
		userId : $rootScope.globals.currentUser.userId,
		description : undefined
	};
	$scope.updateAchievementObject;

	/**
	 * Setup for updating achievement input.
	 */
	$scope.setupUpdateAchievement = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedAchievementEditIndex = index;
		$scope.showAchievementInputs = true;
		$scope.showUpdateAchievementInputs = true;
		var length = $scope.achievement.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.achievement[i]);
			if ($scope.achievement[i].id == id) {
				$scope.updateAchievementObject = angular.copy($scope.achievement[i]);
			}
		}
		console.log($scope.updateAchievementObject);
	}

	/**
	 * Update education.
	 */
	$scope.updateAchievement = function() {
		$scope.savingAchievement = true;
		var promise = PortfolioRestFactory.updateAchievement($scope.updateAchievementObject);
		promise.then(function(response) {
			$scope.savingAchievement = false;
			$scope.showAchievementInputs = false;
			$scope.showUpdateAchievementInputs = false;
			$scope.selectedAchievementEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.achievement.length;
			for (var i = 0; i < length; i++) {
				if ($scope.achievement[i].id == $scope.updateAchievementObject.id) {
					$scope.achievement[i] = $scope.updateAchievementObject;
				}
			}
		}, function(reason) {
			$scope.savingAchievement = false;
			console.log(reason);
			alert("Failed to update achievement. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert achievement.
	 */
	$scope.insertAchievement = function() {
		$scope.savingAchievement = true;
		var newObject = angular.copy($scope.achievementObject);
		var promise = PortfolioRestFactory.insertAchievement(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingAchievement = false;
			$scope.showAchievementInputs = false;
			$scope.achievement.push(response.data);

			// clear form data
			$scope.achievementObject = {
				userId : $rootScope.globals.currentUser.userId,
				description : undefined
			};

		}, function(reason) {
			$scope.savingAchievement = false;
			console.log(reason);
			alert("Failed to save achievement data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete achievement
	 */
	$scope.deleteAchievement = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteAchievement($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove achievement locally
			var length = $scope.achievement.length;
			for (var i = 0; i < length; i++) {
				if ($scope.achievement[i].id == id) {
					$scope.achievement.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateAchievementInputs = false;
			$scope.showAchievementInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete achievement data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Interests.
	 */
	$scope.interestSelected = 'interest';
	$scope.showInterestInputs = false;
	$scope.interest = interest.data;
	$scope.savingInterest = false;
	$scope.interestObject = {
		userId : $rootScope.globals.currentUser.userId,
		description : undefined
	};
	$scope.updateInterestObject;

	/**
	 * Setup for updating interest input.
	 */
	$scope.setupInterestSkill = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedInterestEditIndex = index;
		$scope.showInterestInputs = true;
		$scope.showUpdateInterestInputs = true;
		var length = $scope.interest.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.interest[i]);
			if ($scope.interest[i].id == id) {
				$scope.updateInterestObject = angular.copy($scope.interest[i]);
			}
		}
		console.log($scope.updateInterestObject);
	}

	/**
	 * Update interest.
	 */
	$scope.updateInterest = function() {
		$scope.savingInterest = true;
		var promise = PortfolioRestFactory.updateInterest($scope.updateInterestObject);
		promise.then(function(response) {
			$scope.savingInterest = false;
			$scope.showInterestInputs = false;
			$scope.showUpdateInterestInputs = false;
			$scope.selectedInterestEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.interest.length;
			for (var i = 0; i < length; i++) {
				if ($scope.interest[i].id == $scope.updateInterestObject.id) {
					$scope.interest[i] = $scope.updateInterestObject;
				}
			}
		}, function(reason) {
			$scope.savingInterest = false;
			console.log(reason);
			alert("Failed to update interest. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert interest.
	 */
	$scope.insertInterest = function() {
		$scope.savingInterest = true;
		var newObject = angular.copy($scope.interestObject);
		var promise = PortfolioRestFactory.insertInterest(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingInterest = false;
			$scope.showInterestInputs = false;
			$scope.interest.push(response.data);

			// clear form data
			$scope.interestObject = {
				userId : $rootScope.globals.currentUser.userId,
				description : undefined
			};

		}, function(reason) {
			$scope.savingInterest = false;
			console.log(reason);
			alert("Failed to save interest data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete interest.
	 */
	$scope.deleteInterest = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteInterest($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove interest locally
			var length = $scope.interest.length;
			for (var i = 0; i < length; i++) {
				if ($scope.interest[i].id == id) {
					$scope.interest.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateInterestInputs = false;
			$scope.showInterestInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete interest data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Skills.
	 */
	$scope.showSkillInputs = false;
	$scope.skill = skill.data;
	$scope.savingSkill = false;
	$scope.skillObject = {
		userId : $rootScope.globals.currentUser.userId,
		description : undefined
	};
	$scope.updateSkillObject;

	/**
	 * Setup for updating skill input.
	 */
	$scope.setupUpdateSkill = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedSkillsEditIndex = index;
		$scope.showSkillInputs = true;
		$scope.showUpdateSkillInputs = true;
		var length = $scope.skill.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.skill[i]);
			if ($scope.skill[i].id == id) {
				$scope.updateSkillObject = angular.copy($scope.skill[i]);
			}
		}
		console.log($scope.updateSkillObject);
	}

	/**
	 * Update skill.
	 */
	$scope.updateSkill = function() {
		$scope.savingSkill = true;
		var promise = PortfolioRestFactory.updateSkill($scope.updateSkillObject);
		promise.then(function(response) {
			$scope.savingSkill = false;
			$scope.showSkillInputs = false;
			$scope.showUpdateSkillInputs = false;
			$scope.selectedSkillsEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.skill.length;
			for (var i = 0; i < length; i++) {
				if ($scope.skill[i].id == $scope.updateSkillObject.id) {
					$scope.skill[i] = $scope.updateSkillObject;
				}
			}
		}, function(reason) {
			$scope.savingSkill = false;
			console.log(reason);
			alert("Failed to update skill. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert skills.
	 */
	$scope.insertSkill = function() {
		$scope.savingSkill = true;
		var newObject = angular.copy($scope.skillObject);
		var promise = PortfolioRestFactory.insertSkill(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingSkill = false;
			$scope.showSkillInputs = false;
			$scope.skill.push(response.data);

			// clear form data
			$scope.skillObject = {
				userId : $rootScope.globals.currentUser.userId,
				description : undefined
			};

		}, function(reason) {
			$scope.savingSkill = false;
			console.log(reason);
			alert("Failed to save skill data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete skill.
	 */
	$scope.deleteSkill = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteSkill($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove skill locally
			var length = $scope.skill.length;
			for (var i = 0; i < length; i++) {
				if ($scope.skill[i].id == id) {
					$scope.skill.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateSkillInputs = false;
			$scope.showSkillInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete skill data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Work Values.
	 */
	$scope.showWorkValueInputs = false;
	$scope.workValue = workValue.data;
	$scope.savingWorkValue = false;
	$scope.workValueObject = {
		userId : $rootScope.globals.currentUser.userId,
		description : undefined
	};
	$scope.updateWorkValueObject;

	/**
	 * Setup for updating work value input.
	 */
	$scope.setupUpdateWorkValue = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedValuesEditIndex = index;
		$scope.showWorkValueInputs = true;
		$scope.showUpdateWorkValueInputs = true;
		var length = $scope.workValue.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.workValue[i]);
			if ($scope.workValue[i].id == id) {
				$scope.updateWorkValueObject = angular.copy($scope.workValue[i]);
			}
		}
		console.log($scope.updateWorkValueObject);
	}

	/**
	 * Update work value.
	 */
	$scope.updateWorkValue = function() {
		$scope.savingWorkValue = true;
		var promise = PortfolioRestFactory.updateWorkValue($scope.updateWorkValueObject);
		promise.then(function(response) {
			$scope.savingWorkValue = false;
			$scope.showWorkValueInputs = false;
			$scope.showUpdateWorkValueInputs = false;
			$scope.selectedEducationEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.workValue.length;
			for (var i = 0; i < length; i++) {
				if ($scope.workValue[i].id == $scope.updateWorkValueObject.id) {
					$scope.workValue[i] = $scope.updateWorkValueObject;
				}
			}
		}, function(reason) {
			$scope.savingWorkValue = false;
			console.log(reason);
			alert("Failed to update work value. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert work value.
	 */
	$scope.insertWorkValue = function() {
		$scope.savingWorkValue = true;
		var newObject = angular.copy($scope.workValueObject);
		var promise = PortfolioRestFactory.insertWorkValue(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingWorkValue = false;
			$scope.showWorkValueInputs = false;
			$scope.workValue.push(response.data);

			// clear form data
			$scope.workValueObject = {
				userId : $rootScope.globals.currentUser.userId,
				description : undefined
			};

		}, function(reason) {
			$scope.savingWorkValue = false;
			console.log(reason);
			alert("Failed to save work value data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete work value.
	 */
	$scope.deleteWorkValue = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteWorkValue($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.workValue.length;
			for (var i = 0; i < length; i++) {
				if ($scope.workValue[i].id == id) {
					$scope.workValue.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateWorkValueInputs = false;
			$scope.showWorkValueInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete work value data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Volunteer
	 */
	$scope.showVolunteerInputs = false;
	$scope.volunteer = volunteer.data;
	$scope.savingVolunteer = false;
	$scope.volunteerEndDateDisabled = true;
	$scope.updateVolunteerEndDateDisabled = true;
	$scope.volunteerObject = {
		userId : $rootScope.globals.currentUser.userId,
		organizationName : undefined,
		startDate : undefined,
		endDate : undefined,
		description : undefined
	};
	$scope.updateVolunteerObject;

	/**
	 * Setup for updating volunteer input.
	 */
	$scope.setupUpdateVolunteer = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedVolunteerEditIndex = index;
		$scope.showVolunteerInputs = true;
		$scope.showUpdateVolunteerInputs = true;
		var length = $scope.volunteer.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.volunteer[i]);
			if ($scope.volunteer[i].id == id) {
				$scope.updateVolunteerObject = angular.copy($scope.volunteer[i]);
			}
		}
		$scope.updateVolunteerEndDateDisabled = $scope.updateVolunteerObject.endDate == 'Present' ? true : false;
		console.log($scope.updateVolunteerObject);
	}

	/**
	 * Update volunteer.
	 */
	$scope.updateVolunteer = function() {
		$scope.savingVolunteer = true;
		var promise = PortfolioRestFactory.updateVolunteer($scope.updateVolunteerObject);
		promise.then(function(response) {
			$scope.savingVolunteer = false;
			$scope.showVolunteerInputs = false;
			$scope.showUpdateVolunteerInputs = false;
			$scope.updateVolunteerEndDateDisabled = true;
			$scope.selectedVolunteerEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.volunteer.length;
			for (var i = 0; i < length; i++) {
				if ($scope.volunteer[i].id == $scope.updateVolunteerObject.id) {
					$scope.volunteer[i] = $scope.updateVolunteerObject;
				}
			}
		}, function(reason) {
			$scope.savingVolunteer = false;
			console.log(reason);
			alert("Failed to update volunteer. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert volunteer.
	 */
	$scope.insertVolunteer = function() {
		$scope.savingVolunteer = true;
		var newObject = angular.copy($scope.volunteerObject);
		var promise = PortfolioRestFactory.insertVolunteer(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingVolunteer = false;
			$scope.showVolunteerInputs = false;
			$scope.volunteerEndDateDisabled = true;
			$scope.volunteer.push(response.data);

			// clear form data
			$scope.volunteerObject = {
				userId : $rootScope.globals.currentUser.userId,
				organizationName : undefined,
				startDate : undefined,
				endDate : undefined,
				description : undefined
			};

		}, function(reason) {
			$scope.savingVolunteer = false;
			console.log(reason);
			alert("Failed to save volunteer work. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete volunteer.
	 */
	$scope.deleteVolunteer = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteVolunteer($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove volunteer locally
			var length = $scope.volunteer.length;
			console.log($scope.volunteer);
			for (var i = 0; i < length; i++) {
				if ($scope.volunteer[i].id == id) {
					$scope.volunteer.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdateVolunteerInputs = false;
			$scope.showVolunteerInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete volunteer data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Test scores.
	 */
	$scope.showScoreInputs = false; // used for all scores
	$scope.testSelected = actScore.length == 0 ? 'ACT' : asvabScore.length == 0 ? 'ASVAB' : satScore.length == 0 ? 'SAT' : 'OTHER'; // ACT
	// default
	// selection
	$scope.testEditDisable = false;

	// reset so only one section is open at a time.
	function resetScoreEditFlags() {
		$scope.showUpdateFyiScoreInputs = false;
		$scope.showUpdateAsvabScoreInputs = false;
		$scope.showUpdateActScoreInputs = false;
		$scope.showUpdateSatScoreInputs = false;
		$scope.showUpdateOtherScoreInputs = false;
	}

	/**
	 * ACT
	 */
	$scope.actScore = actScore.data;
	$scope.savingActScore = false;
	$scope.actObject = {
		userId : $rootScope.globals.currentUser.userId,
		readingScore : undefined,
		mathScore : undefined,
		writingScore : undefined,
		englishScore : undefined,
		scienceScore : undefined
	};
	$scope.updateActObject;

	/**
	 * Setup for updating ACT input.
	 */
	$scope.setupUpdateActScore = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedACTEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateActScoreInputs = true;
		var length = $scope.actScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.actScore[i]);
			if ($scope.actScore[i].id == id) {
				$scope.updateActObject = angular.copy($scope.actScore[i]);
				break;
			}
		}
		console.log($scope.updateActObject);
	}

	/**
	 * Update ACT.
	 */
	$scope.updateActScore = function() {
		$scope.savingActScore = true;
		var promise = PortfolioRestFactory.updateActScore($scope.updateActObject);
		promise.then(function(response) {
			$scope.savingActScore = false;
			$scope.showScoreInputs = false;
			$scope.showUpdateActScoreInputs = false;
			$scope.selectedACTEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.actScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.actScore[i].id == $scope.updateActObject.id) {
					$scope.actScore[i] = $scope.updateActObject;
					break;
				}
			}
		}, function(reason) {
			$scope.savingActScore = false;
			console.log(reason);
			alert("Failed to update ACT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert ACT score.
	 */
	$scope.insertActScore = function() {
		$scope.savingActScore = true;
		var newObject = angular.copy($scope.actObject);
		var promise = PortfolioRestFactory.insertActScore(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingActScore = false;
			$scope.showScoreInputs = false;
			$scope.actScore.push(response.data);

			// clear form data
			$scope.actObject = {
				userId : $rootScope.globals.currentUser.userId,
				readingScore : undefined,
				mathScore : undefined,
				writingScore : undefined,
				englishScore : undefined,
				scienceScore : undefined
			};

		}, function(reason) {
			$scope.savingActScore = false;
			console.log(reason);
			alert("Failed to save ACT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete ACT score.
	 */
	$scope.deleteActScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteActScore($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.actScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.actScore[i].id == id) {
					$scope.actScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete ACT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * ASVAB
	 */
	$scope.asvabScore = asvabScore.data;
	$scope.haveAsvabTestScores = false;
	$scope.asvabTestScores = {
		verbalScore: undefined,
		mathScore: undefined,
		scienceScore: undefined,
		afqtScore: undefined
	}
	
	// Retrieve scores from session
	if ($window.sessionStorage.getItem('sV') && $window.sessionStorage.getItem('manualScores') == "false") {
		$scope.haveAsvabTestScores = true;
		$scope.asvabTestScores = {
			verbalScore: $window.sessionStorage.getItem('sV'),
			mathScore: $window.sessionStorage.getItem('sM'),
			scienceScore: $window.sessionStorage.getItem('sS'),
			afqtScore: $scope.afqtScore = $window.sessionStorage.getItem('sA')
		}
	}
	
	$scope.savingAsvabScore = false;
	$scope.asvabObject = {
		userId : $rootScope.globals.currentUser.userId,
		verbalScore : undefined,
		mathScore : undefined,
		afqtScore : undefined,
		scienceScore : undefined
	};
	$scope.updateAsvabObject;

	/**
	 * Setup for updating ASVAB input.
	 */
	$scope.setupUpdateAsvabScore = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedASVABEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateAsvabScoreInputs = true;
		var length = $scope.asvabScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.asvabScore[i]);
			if ($scope.asvabScore[i].id == id) {
				$scope.updateAsvabObject = angular.copy($scope.asvabScore[i]);
				break;
			}
		}
		//console.log($scope.updateAsvabObject);
	}

	/**
	 * Update ASVAB.
	 */
	$scope.updateAsvabScore = function() {
		$scope.savingAsvabScore = true;
		$window.sessionStorage.setItem('manualScores', true);
		$window.sessionStorage.setItem('sV', $scope.asvabObject.verbalScore);
		$window.sessionStorage.setItem('sM', $scope.asvabObject.mathScore);
		$window.sessionStorage.setItem('sS', $scope.asvabObject.scienceScore);
		$window.sessionStorage.setItem('sA', $scope.asvabObject.afqtScore);

		var promise = PortfolioRestFactory.updateAsvabScore($scope.updateAsvabObject);
		promise.then(function(response) {
			$scope.savingAsvabScore = false;
			$scope.showScoreInputs = false;
			$scope.showUpdateAsvabScoreInputs = false;
			$scope.selectedASVABEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.asvabScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.asvabScore[i].id == $scope.updateAsvabObject.id) {
					$scope.asvabScore[i] = $scope.updateAsvabObject;
					break;
				}
			}

		}, function(reason) {
			$scope.savingAsvabScore = false;
			console.log(reason);
			alert("Failed to update ASVAB score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert ASVAB score.
	 */
	$scope.insertAsvabScore = function() {
		$scope.savingAsvabScore = true;
		$window.sessionStorage.setItem('manualScores', true);
		$window.sessionStorage.setItem('sV', $scope.asvabObject.verbalScore);
		$window.sessionStorage.setItem('sM', $scope.asvabObject.mathScore);
		$window.sessionStorage.setItem('sS', $scope.asvabObject.scienceScore);
		$window.sessionStorage.setItem('sA', $scope.asvabObject.afqtScore);

		var newObject = angular.copy($scope.asvabObject);
		var promise = PortfolioRestFactory.insertAsvabScore(newObject);
		promise.then(function(response) {
			//console.log(response);
			$scope.savingAsvabScore = false;
			$scope.showScoreInputs = false;
			$scope.asvabScore.push(response.data);

			// clear form data
			$scope.asvabObject = {
				userId : $rootScope.globals.currentUser.userId,
				verbalScore : undefined,
				mathScore : undefined,
				afqtScore : undefined,
				scienceScore : undefined
			};
		}, function(reason) {
			$scope.savingAsvabScore = false;
			console.log(reason);
			alert("Failed to save ASVAB score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete ASVAB score.
	 */
	$scope.deleteAsvabScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteAsvabScore($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			$window.sessionStorage.setItem('manualScores', false);
			//console.log(response);
			// remove work value locally
			var length = $scope.asvabScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.asvabScore[i].id == id) {
					$scope.asvabScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete ASVAB score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * SAT
	 */
	$scope.satScore = satScore.data;
	$scope.savingSatScore = false;
	$scope.satObject = {
		userId : $rootScope.globals.currentUser.userId,
		readingScore : undefined,
		mathScore : undefined,
		writingScore : undefined,
		subject : undefined
	};
	$scope.updateSatObject;

	/**
	 * Setup for updating SAT input.
	 */
	$scope.setupUpdateSatScore = function(id, index) {
		console.log('setupUpdateSatScore');
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedSATEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateSatScoreInputs = true;
		var length = $scope.satScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.satScore[i]);
			if ($scope.satScore[i].id == id) {
				$scope.updateSatObject = angular.copy($scope.satScore[i]);
				break;
			}
		}
		console.log($scope.updateSatObject);
	}

	/**
	 * Update SAT.
	 */
	$scope.updateSatScore = function() {
		console.log('updateSatScore');
		$scope.savingSatScore = true;
		var promise = PortfolioRestFactory.updateSatScore($scope.updateSatObject);
		promise.then(function(response) {
			$scope.savingSatScore = false;
			$scope.showSatInputs = false;
			$scope.showUpdateSatScoreInputs = false;
			$scope.selectedSATEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.satScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.satScore[i].id == $scope.updateSatObject.id) {
					$scope.satScore[i] = $scope.updateSatObject;
					break;
				}
			}
		}, function(reason) {
			$scope.savingSatScore = false;
			console.log(reason);
			alert("Failed to update SAT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert SAT score.
	 */
	$scope.insertSatScore = function() {
		console.log('insertSatScore');
		$scope.savingSatScore = true;
		var newObject = angular.copy($scope.satObject);
		var promise = PortfolioRestFactory.insertSatScore(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingSatScore = false;
			$scope.showScoreInputs = false;
			$scope.satScore.push(response.data);

			// clear form data
			$scope.satObject = {
				userId : $rootScope.globals.currentUser.userId,
				readingScore : undefined,
				mathScore : undefined,
				writingScore : undefined,
				subject : undefined
			};

		}, function(reason) {
			$scope.savingSatScore = false;
			console.log(reason);
			alert("Failed to save SAT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete SAT score.
	 */
	$scope.deleteSatScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteSatScore($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.satScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.satScore[i].id == id) {
					$scope.satScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete SAT score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * FYI. If user has FYI scores then use it as default.
	 */

	// Use FYI test results if exists.
	UserFactory.getUserInterestCodes().then(function(response) {
		$scope.scoreChoice = response.scoreChoice;
		
		if (response.length == 0) {
			$scope.hideFyi = false;
			$scope.fyiScore = fyiScore.data;
		} else {
			$scope.hideFyi = true; // hide fyi inputs
			var array = [];
			array.push(response);
			$scope.fyiScore = array;
		}

	}, function(reason) {
		$scope.hideFyi = false;
		if (reason.length == 0) {
			$scope.fyiScore = fyiScore.data;
		} else {
			alert("Error fetching FYI test result." + reason);
		}
	});

	
	$scope.interestCodesInfo = function() {
		console.log('Here I am');
		var score = $scope.scoreChoice == 'gender' ? 'Gender-Specific' : 'Combined';
		var modalOptions = {
			headerText : 'Interest Codes',
			bodyText : "You're currently using your <strong>" + score + "</strong> scores for career exploration. You have two sets of FYI results, Gender-Specific and Combined. To change the set you're using for career exploration and to learn more about FYI results, go to Return to FYI Results under Step 1."
		};

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}
	$scope.savingFyiScore = false;
	$scope.fyiObject = {
		userId : $rootScope.globals.currentUser.userId,
		interest_code_one : undefined,
		interest_code_two : undefined,
		interest_code_three : undefined
	};
	$scope.updateFyiObject;

	/**
	 * Setup for updating FYI input.
	 */
	$scope.setupUpdateFyiScore = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedFYIEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateFyiScoreInputs = true;
		var length = $scope.fyiScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.fyiScore[i]);
			if ($scope.fyiScore[i].id == id) {
				$scope.updateFyiObject = angular.copy($scope.fyiScore[i]);
				break;
			}
		}
		console.log($scope.updateFyiObject);
	}

	/**
	 * Update FYI.
	 */
	$scope.updateFyiScore = function() {
		$scope.savingFyiScore = true;
		var promise = PortfolioRestFactory.updateFyiScore($scope.updateFyiObject);
		promise.then(function(response) {
			$scope.savingFyiScore = false;
			$scope.showFyiInputs = false;
			$scope.showUpdateFyiScoreInputs = false;
			$scope.selectedFYIEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.fyiScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.fyiScore[i].id == $scope.updateFyiObject.id) {
					$scope.fyiScore[i] = $scope.updateFyiObject;
					break;
				}
			}
		}, function(reason) {
			$scope.savingFyiScore = false;
			console.log(reason);
			alert("Failed to update FYI score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert FYI score.
	 */
	$scope.insertFyiScore = function() {
		$scope.savingFyiScore = true;
		var newObject = angular.copy($scope.fyiObject);
		var promise = PortfolioRestFactory.insertFyiScore(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingFyiScore = false;
			$scope.showInterestInputs = false;
			$scope.interestSelected = 'interest';
			$scope.fyiScore.push(response.data);

			// clear form data
			$scope.fyiObject = {
				userId : $rootScope.globals.currentUser.userId,
				interest_code_one : undefined,
				interest_code_two : undefined,
				interest_code_three : undefined
			}

		}, function(reason) {
			$scope.savingFyiScore = false;
			console.log(reason);
			alert("Failed to save FYI score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete FYI score.
	 */
	$scope.deleteFyiScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteFyiScore($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.fyiScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.fyiScore[i].id == id) {
					$scope.fyiScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete FYI score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Other test scores.
	 */
	$scope.otherScore = otherScore.data;
	$scope.savingOtherScore = false;
	$scope.otherObject = {
		userId : $rootScope.globals.currentUser.userId,
		description : undefined
	};
	$scope.updateOtherObject;

	/**
	 * Setup for updating other input.
	 */
	$scope.setupUpdateOtherScore = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedOtherEditIndex = index;
		$scope.testEditDisable = true;
		resetScoreEditFlags();
		$scope.showScoreInputs = false;
		$scope.showUpdateOtherScoreInputs = true;
		var length = $scope.otherScore.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.otherScore[i]);
			if ($scope.otherScore[i].id == id) {
				$scope.updateOtherObject = angular.copy($scope.otherScore[i]);
				break;
			}
		}
		console.log($scope.updateOtherObject);
	}

	/**
	 * Update other scores.
	 */
	$scope.updateOtherScore = function() {
		$scope.savingOtherScore = true;
		var promise = PortfolioRestFactory.updateOtherScore($scope.updateOtherObject);
		promise.then(function(response) {
			$scope.savingOtherScore = false;
			$scope.showOtherInputs = false;
			$scope.showUpdateOtherScoreInputs = false;
			$scope.selectedOtherEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};
			$scope.testEditDisable = false;

			var length = $scope.otherScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.otherScore[i].id == $scope.updateOtherObject.id) {
					$scope.otherScore[i] = $scope.updateOtherObject;
					break;
				}
			}
		}, function(reason) {
			$scope.savingOtherScore = false;
			console.log(reason);
			alert("Failed to update other score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert other scores.
	 */
	$scope.insertOtherScore = function() {
		$scope.savingOtherScore = true;
		var newObject = angular.copy($scope.otherObject);
		var promise = PortfolioRestFactory.insertOtherScore(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingOtherScore = false;
			$scope.showScoreInputs = false;
			$scope.otherScore.push(response.data);

			// clear form data
			$scope.otherObject = {
				userId : $rootScope.globals.currentUser.userId,
				description : undefined
			};

		}, function(reason) {
			$scope.savingOtherScore = false;
			console.log(reason);
			alert("Failed to save score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete other score.
	 */
	$scope.deleteOtherScore = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deleteOtherScore($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove work value locally
			var length = $scope.otherScore.length;
			for (var i = 0; i < length; i++) {
				if ($scope.otherScore[i].id == id) {
					$scope.otherScore.splice(i, 1);
					break;
				}
			}
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete other score. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Plan of action implementation
	 */
	$scope.showPlanInputs = false;
	$scope.plan = plan.data;
	console.log($scope.plan);
	$scope.savingPlan = false;
	$scope.planObject = {
		userId : $rootScope.globals.currentUser.userId,
		plan : undefined,
		description : undefined
	};
	$scope.updatePlanObject;

	/**
	 * Setup for updating plan of interest input.
	 */
	$scope.setupPlanSkill = function(id, index) {
		$scope.myStyle = {
			'cursor' : 'not-allowed'
		};
		$scope.selectedPlanEditIndex = index;
		$scope.showPlanInputs = true;
		$scope.showUpdatePlanInputs = true;
		var length = $scope.plan.length;
		for (var i = 0; i < length; i++) {
			console.log($scope.plan[i]);
			if ($scope.plan[i].id == id) {
				$scope.updatePlanObject = angular.copy($scope.plan[i]);
			}
		}
		console.log($scope.updatePlanObject);
	}

	/**
	 * Update plan of action.
	 */
	$scope.updatePlan = function() {
		$scope.savingPlan = true;
		var promise = PortfolioRestFactory.updatePlan($scope.updatePlanObject);
		promise.then(function(response) {
			$scope.savingPlan = false;
			$scope.showPlanInputs = false;
			$scope.showUpdatePlanInputs = false;
			$scope.selectedPlanEditIndex = undefined;
			$scope.myStyle = {
				'cursor' : 'pointer'
			};

			var length = $scope.plan.length;
			for (var i = 0; i < length; i++) {
				if ($scope.plan[i].id == $scope.updatePlanObject.id) {
					$scope.plan[i] = $scope.updatePlanObject;
				}
			}
		}, function(reason) {
			$scope.savingPlan = false;
			console.log(reason);
			alert("Failed to update plan of action. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Insert plan of action.
	 */
	$scope.insertPlan = function() {
		$scope.savingPlan = true;
		var newObject = angular.copy($scope.planObject);
		var promise = PortfolioRestFactory.insertPlan(newObject);
		promise.then(function(response) {
			console.log(response);
			$scope.savingPlan = false;
			$scope.showPlanInputs = false;
			$scope.plan.push(response.data);

			// clear form data
			$scope.planObject = {
				userId : $rootScope.globals.currentUser.userId,
				plan : undefined,
				description : undefined
			};

		}, function(reason) {
			$scope.savingPlan = false;
			console.log(reason);
			alert("Failed to save plan of action data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/*
	 * Delete plan of action.
	 */
	$scope.deletePlan = function(id) {
		console.log(id);
		var promise = PortfolioRestFactory.deletePlan($rootScope.globals.currentUser.userId, id);
		promise.then(function(response) {
			console.log(response);
			// remove plan locally
			var length = $scope.plan.length;
			for (var i = 0; i < length; i++) {
				if ($scope.plan[i].id == id) {
					$scope.plan.splice(i, 1);
					break;
				}
			}

			// if user has inputs open and tries to delete then close inputs
			// after delete.
			$scope.showUpdatePlanInputs = false;
			$scope.showPlanInputs = false;
		}, function(reason) {
			console.log(reason);
			alert("Failed to delete plan of action data. Try again. If error persist, please contact site administrator.");
		}, function(update) {
			alert('Got notification: ' + update);
		});
	}

	/**
	 * Print page as pdf. Utilizes pdfmake library.
	 */
	var docDefinition;

	/*
	 * function toDataUrl(url, callback, outputFormat){ var img = new Image();
	 * img.crossOrigin = 'Anonymous'; img.onload = function(){ var canvas =
	 * document.createElement('CANVAS'); var ctx = canvas.getContext('2d'); var
	 * dataURL; canvas.height = this.height; canvas.width = this.width;
	 * ctx.drawImage(this, 0, 0); dataURL = canvas.toDataURL(outputFormat);
	 * callback(dataURL); canvas = null; }; img.src = url; }
	 * 
	 * toDataUrl('images/logo.png', function(base64Img){ logoImg = base64Img;
	 * console.log(logoImg); });
	 */

	/**
	 * Setup PDF and print content.
	 */
	function setupContent() {

		var content = [];

		// User input for name and grade.
		var nameAndGrade = [ {
			columns : [ {
				text : 'Name: ' + $scope.fullName,
				alignment : 'left',
				fontSize : 18,
				bold : true
			}, {
				text : 'Grade: ' + $scope.grade,
				alignment : 'right',
				fontSize : 18,
				bold : true
			} ]
		}, {
			canvas : [ {
				type : 'line',
				x1 : 0,
				y1 : 5,
				x2 : 595 - 2 * 40,
				y2 : 5,
				lineWidth : 1
			} ],
			style : 'horizontalLine'
		} ];
		content.push(nameAndGrade);

		// Interests content.
		if ($scope.planPrint) {
			var len = $scope.plan.length;

			if (len > 0) {

				// section title
				content.push({
					text : 'Future Plan:\n\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : $scope.plan[i].plan
					}, {
						text : $scope.plan[i].description == null ? '' : $scope.plan[i].description
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Work experience content.
		if ($scope.workExperiencePrint) {
			var len = $scope.workExperience.length;

			if (len > 0) {

				// section title
				content.push({
					text : 'Work Experience:\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : [ {
							text : '\n' + $scope.workExperience[i].jobTitle + ', ',
							bold : true
						}, $scope.workExperience[i].companyName ]
					}, {
						text : $scope.workExperience[i].startDate + ' - ' + $scope.workExperience[i].endDate
					}, {
						text : $scope.workExperience[i].jobDescription
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Education content.
		if ($scope.educationPrint) {
			var len = $scope.education.length;
			console.log($scope.education);

			if (len > 0) {

				// section title
				content.push({
					text : 'Education:\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : '\n' + $scope.education[i].schoolName + ', ',
						bold : true
					}, {
						text : $scope.education[i].startDate + ' - ' + $scope.education[i].endDate
					}, {
						text : $scope.education[i].activities == null ? '' : $scope.education[i].activities
					}, {
						text : $scope.education[i].achievements == null ? '' : $scope.education[i].achievements
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Test score content.
		if ($scope.testScorePrint) {
			var actLen = $scope.actScore.length;
			var asvabLen = $scope.asvabScore.length;
			var satLen = $scope.satScore.length;
			var fyiLen = $scope.fyiScore.length;
			var otherLen = $scope.otherScore.length;

			if (actLen > 0 || asvabLen > 0 || satLen > 0 || fyiLen > 0 || otherLen > 0) {
				// section title
				content.push({
					text : 'Test Score:\n',
					style : 'sectionTitle'
				});
			}

			// ACT score content
			if (actLen > 0) {

				for (var i = 0; i < actLen; i++) {

					content.push([ {
						style : 'scoreTable',
						table : {
							widths : [ 150, '*' ],
							headerRows : 1,
							keepWithHeaderRows : 1,
							dontBreakRows : true,
							body : [ [ {
								text : 'ACT Score',
								colSpan : 2,
								alignment : 'left',
								bold : true
							}, {} ], [ {
								text : 'Reading Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].readingScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Math Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].mathScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Writing Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].writingScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Science Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].scienceScore,
								style : 'tableStyle'
							} ], [ {
								text : 'English Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.actScore[i].englishScore,
								style : 'tableStyle'
							} ] ]
						},
						layout : 'noBorders'
					} ]);
				}
			}

			// ASVAB score content
			if (asvabLen > 0) {

				for (var i = 0; i < asvabLen; i++) {

					/*
					 * content.push([ { text : 'Verbal Score:' +
					 * $scope.asvabScore[i].verbalScore },{ text : 'Math Score:' +
					 * $scope.asvabScore[i].mathScore },{ text : 'Science
					 * Score:' + $scope.asvabScore[i].scienceScore },{ text :
					 * 'AFQT Score:' + $scope.asvabScore[i].afqtScore } ]);
					 */

					content.push([ {
						style : 'scoreTable',
						table : {
							widths : [ 150, '*' ],
							headerRows : 1,
							keepWithHeaderRows : 1,
							dontBreakRows : true,
							body : [ [ {
								text : 'ASVAB Score',
								colSpan : 2,
								alignment : 'left',
								bold : true
							}, {} ], [ {
								text : 'Verbal Skills Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.asvabScore[i].verbalScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Math Skills Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.asvabScore[i].mathScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Science and Technical Skills Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.asvabScore[i].scienceScore,
								style : 'tableStyle'
							} ], [ {
								text : 'AFQT Score',
								style : 'tableStyle'
							}, {
								text : '' + $scope.asvabScore[i].afqtScore,
								style : 'tableStyle'
							} ] ]
						},
						layout : 'noBorders'
					} ]);
				}
			}

			// SAT score content
			if (satLen > 0) {

				for (var i = 0; i < satLen; i++) {

					/*
					 * content.push([ { text : 'Reading Score:' +
					 * $scope.satScore[i].readingScore },{ text : 'Math Score:' +
					 * $scope.satScore[i].mathScore },{ text : 'Writing Score:' +
					 * $scope.satScore[i].writingScore },{ text :
					 * $scope.satScore[i].subject != null ? 'Subject Score:' +
					 * $scope.satScore[i].subject : '' } ]);
					 */

					content.push([ {
						style : 'scoreTable',
						table : {
							widths : [ 150, '*' ],
							headerRows : 1,
							keepWithHeaderRows : 1,
							dontBreakRows : true,
							body : [ [ {
								text : 'SAT Score',
								colSpan : 2,
								alignment : 'left',
								bold : true
							}, {} ], [ {
								text : 'Critical Reading',
								style : 'tableStyle'
							}, {
								text : '' + $scope.satScore[i].readingScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Mathematics',
								style : 'tableStyle'
							}, {
								text : '' + $scope.satScore[i].mathScore,
								style : 'tableStyle'
							} ], [ {
								text : 'Writing',
								style : 'tableStyle'
							}, {
								text : '' + $scope.satScore[i].writingScore != 0 ? + $scope.satScore[i].writingScore : 'None',
								style : 'tableStyle'
							} ], [ {
								text : 'Other Subject Score',
								style : 'tableStyle'
							}, {
								text : $scope.satScore[i].subject != null ? 'Subject Score:' + $scope.satScore[i].subject : 'None',
								style : 'tableStyle'
							} ] ]
						},
						layout : 'noBorders'
					} ]);
				}
			}

			// FYI score content
			if (fyiLen > 0) {

				content.push({
					text : '\nFYI Score:\n',
					style : 'sectionTitle'
				})

				for (var i = 0; i < fyiLen; i++) {

					/*
					 * content.push([ { text : 'Interest Code One:' +
					 * $scope.fyiScore[i].interestCodeOne },{ text : 'Interest
					 * Code Two:' + $scope.fyiScore[i].interestCodeTwo },{ text :
					 * 'Interest Code Three:' +
					 * $scope.fyiScore[i].interestCodeThree } ]);
					 */

					var scoreTextOne = $scope.fyiScore[i].interestCodeOne == 'E' ? 'Enterprising' : $scope.fyiScore[i].interestCodeOne == 'C' ? 'Conventional' : $scope.fyiScore[i].interestCodeOne == 'A' ? 'Artistic' : $scope.fyiScore[i].interestCodeOne == 'S' ? 'Social' : $scope.fyiScore[i].interestCodeOne == 'R' ? 'Realistic' : $scope.fyiScore[i].interestCodeOne == 'I' ? 'Investigative' : '';
					var scoreTextTwo = $scope.fyiScore[i].interestCodeTwo == 'E' ? 'Enterprising' : $scope.fyiScore[i].interestCodeTwo == 'C' ? 'Conventional' : $scope.fyiScore[i].interestCodeTwo == 'A' ? 'Artistic' : $scope.fyiScore[i].interestCodeTwo == 'S' ? 'Social' : $scope.fyiScore[i].interestCodeTwo == 'R' ? 'Realistic' : $scope.fyiScore[i].interestCodeTwo == 'I' ? 'Investigative' : '';
					var scoreTextThree = $scope.fyiScore[i].interestCodeThree == 'E' ? 'Enterprising' : $scope.fyiScore[i].interestCodeThree == 'C' ? 'Conventional' : $scope.fyiScore[i].interestCodeThree == 'A' ? 'Artistic' : $scope.fyiScore[i].interestCodeThree == 'S' ? 'Social' : $scope.fyiScore[i].interestCodeThree == 'R' ? 'Realistic' : $scope.fyiScore[i].interestCodeThree == 'I' ? 'Investigative' : '';

					content.push([ {
						text : $scope.fyiScore[i].interestCodeOne + ' | ' + $scope.fyiScore[i].interestCodeTwo + ' | ' + $scope.fyiScore[i].interestCodeThree + ' (' + scoreTextOne + ' | ' + scoreTextTwo + ' | ' + scoreTextThree + ')',
						style : 'tableStyle'
					} ]);
				}
			}

			// Other score content
			if (otherLen > 0) {

				for (var i = 0; i < otherLen; i++) {

					content.push([ {
						style : 'scoreTable',
						table : {
							widths : [ 150, '*' ],
							headerRows : 1,
							keepWithHeaderRows : 1,
							dontBreakRows : true,
							body : [ [ {
								text : 'Other Scores',
								colSpan : 2,
								alignment : 'left',
								bold : true
							}, {} ], [ {
								text : 'Other Scores',
								style : 'tableStyle'
							}, {
								text : '' + $scope.otherScore[i].description,
								style : 'tableStyle'
							} ] ]
						},
						layout : 'noBorders'
					} ]);
				}
			}

			content.push({
				canvas : [ {
					type : 'line',
					x1 : 0,
					y1 : 5,
					x2 : 595 - 2 * 40,
					y2 : 5,
					lineWidth : 1
				} ],
				style : 'horizontalLine'
			})

		}

		// Achievement content.
		if ($scope.achievementPrint) {
			var len = $scope.achievement.length;

			if (len > 0) {

				// section title
				content.push({
					text : 'Achievement:\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : '\nDescription:',
						bold : true
					}, {
						text : $scope.achievement[i].description == null ? '' : $scope.achievement[i].description
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Skill content.
		if ($scope.skillsPrint) {
			var len = $scope.skill.length;

			if (len > 0) {

				// section title
				content.push({
					text : 'Skill:\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : '\nDescription:',
						bold : true
					}, {
						text : $scope.skill[i].description == null ? '' : $scope.skill[i].description
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Volunteer content.
		if ($scope.volunteerPrint) {
			var len = $scope.volunteer.length;

			if (len > 0) {

				// section title
				content.push({
					text : 'Volunteer:\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : '\n' + $scope.volunteer[i].organizationName + ', ',
						bold : true
					}, {
						text : $scope.volunteer[i].startDate + ' - ' + $scope.volunteer[i].endDate
					}, {
						text : $scope.volunteer[i].description == null ? '' : $scope.volunteer[i].description
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Interests content.
		if ($scope.interestPrint) {
			var len = $scope.interest.length;

			if (len > 0) {

				// section title
				content.push({
					text : 'Interest:\n\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : $scope.interest[i].description == null ? '' : $scope.interest[i].description
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Work value content.
		if ($scope.workValuesPrint) {
			var len = $scope.workValue.length;

			if (len > 0) {

				// section title
				content.push({
					text : 'Work Value:\n\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : $scope.workValue[i].description == null ? '' : $scope.workValue[i].description
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}
		
		// Favorite occupation content.
		if ($scope.favoriteCitmOccupationPrint) {
			var len = $scope.citmFavoriteOccupations.length;

			if (len > 0) {

				// section title
				content.push({
					text : len == 1 ? 'Favorite CITM Occupation:\n\n' : 'Favorite CITM Occupations\n\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : $scope.citmFavoriteOccupations[i].title == null ? '' : $scope.citmFavoriteOccupations[i].title
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Favorite occupation content.
		if ($scope.favoriteOccupationPrint) {
			var len = $scope.favoriteList.length;

			if (len > 0) {

				// section title
				content.push({
					text : len == 1 ? 'Favorite Occupation:\n\n' : 'Favorite Occupations\n\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : $scope.favoriteList[i].title == null ? '' : $scope.favoriteList[i].title
					} ]);
				}

				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}

		// Favorite career cluster content.
		if ($scope.favoriteCareerClusterPrint) {
			var len = $scope.careerClusterFavoriteList.length;

			if (len > 0) {

				// section title
				content.push({
					text : len == 1 ? 'Favorite Career Cluster:\n\n' : 'Favorite Career Clusters\n\n',
					style : 'sectionTitle'
				})
				for (var i = 0; i < len; i++) {

					content.push([ {
						text : $scope.careerClusterFavoriteList[i].title == null ? '' : $scope.careerClusterFavoriteList[i].title
					} ]);
				}
				
				content.push({
					canvas : [ {
						type : 'line',
						x1 : 0,
						y1 : 5,
						x2 : 595 - 2 * 40,
						y2 : 5,
						lineWidth : 1
					} ],
					style : 'horizontalLine'
				})
			}
		}
		
		// Favorite school content.
				if ($scope.favoriteSchoolPrint) {
					var len = $scope.schoolFavoriteList.length;

					if (len > 0) {

						// section title
						content.push({
							text : len == 1 ? 'Favorite College:\n\n' : 'Favorite Colleges:\n\n',
							style : 'sectionTitle'
						})
						for (var i = 0; i < len; i++) {

							content.push([ {
								text : $scope.schoolFavoriteList[i].schoolName == null ? '' : $scope.schoolFavoriteList[i].schoolName
							} ]);
						}
					}
				}

		/**
		 * Set content.
		 */
		// [{text: 'ASVAB Portfolio', style:
		// 'headerTitle', margin: [72,40]},
		docDefinition = {
			/*
			 * header : [ { image : 'logo', margin : [ 0, 20, 0, 20 ], alignment :
			 * 'center' }, headerContent ],
			 */
			content : content,

			styles : {
				headerTitle : {
					fontSize : 22,
					bold : true,
					alignment : 'center'
				},
				sectionTitle : {
					margin : [ 0, 0, 0, 0 ],
					bold : true,
					alignment : 'left',
					fontSize : 12
				},
				sectionContent : {
				// margin: [0, 0, 0, 20]
				},
				tableStyle : {
					alignment : 'left'
				},
				scoreTable : {
					margin : [ 0, 30, 0, 0 ]
				},
				horizontalLine : {
					margin : [ 0, 20, 0, 0 ]
				}
			}
		};
	}

	$scope.printPdf = function() {
		setupContent();
		
		// IE support
		if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {
			var modalOptions = {
					headerText : 'Alert',
					bodyText : 'Your browser does not support this feature. Alternatively, you can download as PDF and then print.'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
			return false;
		}
		
		pdfMake.createPdf(docDefinition).print();
	};

	$scope.downloadPdf = function() {
		setupContent();
		pdfMake.createPdf(docDefinition).download('ASVAB_Portfolio.pdf');
	};

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

cepApp.controller('PortfolioDirectionsController', [ '$scope', '$window', 'PortfolioService', function($scope, $window, PortfolioService) {

	$scope.deleteSessionPortfolio = function() {
		PortfolioService.setIsPortfolioStarted(undefined);
		$window.sessionStorage.removeItem("isPortfolioStarted");
	}

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

/*******************************************************************************
 * Factory REST calls for Portfolio page. Does all the CRUD work.
 */
cepApp.factory('PortfolioRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var portfolioRestFactory = {};

	portfolioRestFactory.isPortfolioStarted = function(userId) {
		return $http.get(appUrl + 'portfolio/PortfolioStarted/' + userId + '/');
	}

	/**
	 * Work experience rest calls.
	 */
	portfolioRestFactory.getWorkExperience = function(userId) {
		return $http.get(appUrl + 'portfolio/WorkExperience/' + userId + '/');
	}

	portfolioRestFactory.insertWorkExperience = function(workExperienceObject) {
		console.log(workExperienceObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertWorkExperience', workExperienceObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(workExperienceObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteWorkExperience = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteWorkExperience/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateWorkExperience = function(workExperienceObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateWorkExperience', workExperienceObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Education rest calls.
	 */
	portfolioRestFactory.getEducation = function(userId) {
		return $http.get(appUrl + 'portfolio/Education/' + userId + '/');
	}

	portfolioRestFactory.insertEducation = function(educationObject) {
		console.log(educationObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertEducation', educationObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(educationObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteEducation = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteEducation/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateEducation = function(educationObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateEducation', educationObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Achievement rest calls.
	 */
	portfolioRestFactory.getAchievements = function(userId) {
		return $http.get(appUrl + 'portfolio/Achievement/' + userId + '/');
	}

	portfolioRestFactory.insertAchievement = function(achievementObject) {
		console.log(achievementObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertAchievement', achievementObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(achievementObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteAchievement = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteAchievement/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateAchievement = function(achievementObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateAchievement', achievementObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Interests rest calls.
	 */
	portfolioRestFactory.getInterests = function(userId) {
		return $http.get(appUrl + 'portfolio/Interest/' + userId + '/');
	}

	portfolioRestFactory.insertInterest = function(interestObject) {
		console.log(interestObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertInterest', interestObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(interestObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteInterest = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteInterest/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateInterest = function(interestObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateInterest', interestObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Skills rest calls.
	 */
	portfolioRestFactory.getSkills = function(userId) {
		return $http.get(appUrl + 'portfolio/Skill/' + userId + '/');
	}

	portfolioRestFactory.insertSkill = function(skillObject) {
		console.log(skillObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertSkill', skillObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(skillObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteSkill = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteSkill/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateSkill = function(skillObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateSkill', skillObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Work values rest calls.
	 */
	portfolioRestFactory.getWorkValues = function(userId) {
		return $http.get(appUrl + 'portfolio/WorkValue/' + userId + '/');
	}

	portfolioRestFactory.insertWorkValue = function(workValueObject) {
		console.log(workValueObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertWorkValue', workValueObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(workValueObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteWorkValue = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteWorkValue/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateWorkValue = function(workValueObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateWorkValue', workValueObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Volunteer rest calls.
	 */
	portfolioRestFactory.getVolunteers = function(userId) {
		return $http.get(appUrl + 'portfolio/Volunteer/' + userId + '/');
	}

	portfolioRestFactory.insertVolunteer = function(volunteerObject) {
		console.log(volunteerObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertVolunteer', volunteerObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(volunteerObject);
		return deferred.promise;
	}

	portfolioRestFactory.deleteVolunteer = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteVolunteer/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateVolunteer = function(volunteerObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateVolunteer', volunteerObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * ACT score rest calls.
	 */
	portfolioRestFactory.getActScore = function(userId) {
		return $http.get(appUrl + 'portfolio/ACT/' + userId + '/');
	}

	portfolioRestFactory.insertActScore = function(actObject) {
		console.log(actObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertACT', actObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteActScore = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteACT/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateActScore = function(actObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateACT', actObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * ASVAB score rest calls.
	 */
	portfolioRestFactory.getAsvabScore = function(userId) {
		return $http.get(appUrl + 'portfolio/ASVAB/' + userId + '/');
	}

	portfolioRestFactory.insertAsvabScore = function(asvabObject) {
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertASVAB', asvabObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteAsvabScore = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteASVAB/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateAsvabScore = function(asvabObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateASVAB', asvabObject).then(function successCallback(response) {
		//$http.put(appUrl + 'testScore/updateManualTestScore', asvabObject).then(function successCallback(response) { // returns bad request
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * FYI score rest calls.
	 */
	portfolioRestFactory.getFyiScore = function(userId) {
		return $http.get(appUrl + 'portfolio/FYI/' + userId + '/');
	}

	portfolioRestFactory.insertFyiScore = function(fyiObject) {
		console.log(fyiObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertFYI', fyiObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteFyiScore = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteFYI/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateFyiScore = function(fyiObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateFYI', fyiObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * SAT score rest calls.
	 */
	portfolioRestFactory.getSatScore = function(userId) {
		return $http.get(appUrl + 'portfolio/SAT/' + userId + '/');
	}

	portfolioRestFactory.insertSatScore = function(satObject) {
		console.log(satObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertSAT', satObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteSatScore = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteSAT/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateSatScore = function(satObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateSAT', satObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	/**
	 * Other score rest calls.
	 */
	portfolioRestFactory.getOtherScore = function(userId) {
		return $http.get(appUrl + 'portfolio/Other/' + userId + '/');
	}

	portfolioRestFactory.insertOtherScore = function(otherObject) {
		console.log(otherObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertOther', otherObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.deleteOtherScore = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeleteOther/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updateOtherScore = function(otherObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateOther', otherObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}
	
	/**
	 * Plan of action rest calls.
	 */
	portfolioRestFactory.getPlan = function(userId) {
		return $http.get(appUrl + 'portfolio/Plan/' + userId + '/');
	}

	portfolioRestFactory.insertPlan = function(planObject) {
		console.log(planObject);
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/InsertPlan', planObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		console.log(planObject);
		return deferred.promise;
	}

	portfolioRestFactory.deletePlan = function(userId, id) {
		var deferred = $q.defer();
		$http({
			method : 'DELETE',
			url : appUrl + 'portfolio/DeletePlan/' + userId + '/' + id + '/'
		}).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}

	portfolioRestFactory.updatePlan = function(planObject) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdatePlan', planObject).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}
	
	portfolioRestFactory.selectNameGrade = function(userId) {
		return $http.get(appUrl + 'portfolio/SelectNameGrade/' + userId + '/');
	}
	
	portfolioRestFactory.updateInsertUserInfo = function(userInfo) {
		var deferred = $q.defer();
		$http.put(appUrl + 'portfolio/UpdateInsertUserInfo', userInfo).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}
	
	portfolioRestFactory.getCitmFavoriteOccupations = function(userId) {
		return $http.get(appUrl + 'portfolio/get-citm-favorite-occupation/' + userId + '/');
	}
	
	portfolioRestFactory.getAccessCode = function(userId) {
		return $http.get(appUrl + 'portfolio/get-access-code/' + userId + '/');
	}
	
	portfolioRestFactory.insertTeacherDemoIsTaken = function(obj) {
		var deferred = $q.defer();
		$http.post(appUrl + 'portfolio/insert-teacher-demo-taken', obj).then(function successCallback(response) {
			deferred.resolve(response);
		}, function errorCallback(response) {
			deferred.reject(response);
		});
		return deferred.promise;
	}
	
	portfolioRestFactory.getTeacherDemoIsTaken = function(userId) {
		return $http.get(appUrl + 'portfolio/get-teacher-demo-taken/' + userId + '/');
	}

	return portfolioRestFactory;

} ]);
cepApp.factory('PortfolioService', [ '$rootScope', 'PortfolioRestFactory', '$q', '$location', '$window', function($rootScope, PortfolioRestFactory, $q, $location, $window) {

	var portfolioService = {};

	var isPortfolioStarted = undefined;

	portfolioService.setIsPortfolioStarted = function(value) {
		isPortfolioStarted = value;
	}

	portfolioService.getIsPortfolioStarted = function() {

		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();

		// if isPortfolioStarted is undefined then recover data from database
		if (isPortfolioStarted == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("isPortfolioStarted") === null || $window.sessionStorage.getItem("isPortfolioStarted") == 'undefined') {

					// recover data using database
					PortfolioRestFactory.isPortfolioStarted(userId).success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('isPortfolioStarted', JSON.stringify(data));

						// sync with in memory property
						isPortfolioStarted = angular.fromJson($window.sessionStorage.getItem('isPortfolioStarted'));

						deferred.resolve(isPortfolioStarted);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					isPortfolioStarted = angular.fromJson($window.sessionStorage.getItem('isPortfolioStarted'));
					deferred.resolve(isPortfolioStarted);
				}

			} else {

				// no session storage support so get recover data using database
				PortfolioRestFactory.isPortfolioStarted(userId).success(function(data) {

					// success now store in session object
					isPortfolioStarted = data;
					deferred.resolve(isPortfolioStarted);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(isPortfolioStarted);
		}

		return deferred.promise;
	}

	return portfolioService;
} ]);
cepApp.service('portfolioModal', [ '$uibModal', function($uibModal) {

	var modalDefaults = {
		animation : true,
		size : 'lg',
		templateUrl : 'cep/components/portfolio/page-partials/portfolio-tips-modal.html'
	};

	var modalOptions = {};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;

				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.controller('SearchController', [ '$scope', '$rootScope', '$location', function($scope, $rootScope, $location) {

	$scope.searchString = undefined;

	$scope.search = function() {

		var loggedIn = $rootScope.globals.currentUser;
		if (loggedIn) {
			$location.url('/search-result/' + $scope.searchString);
		} else {
			$location.url('/search-result-public/' + $scope.searchString);
		}

	}
	


} ]);
cepApp.controller('SearchResultController', [ '$scope', '$rootScope', '$location', 'searchResult', 'resource', function($scope, $rootScope, $location, searchResult, resource) {

	$scope.searchResult = searchResult.data;
	$scope.pdfBaseURL = resource + 'pdf/';
	
	console.log($scope.searchResult);
	
	//pagination
	$scope.totalItems = $scope.searchResult.length;
	$scope.currentPage = 1;
	$scope.maxSize = 4;
	$scope.itemsPerPage = 12;

	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});
	
} ]);
cepApp.factory('SearchRestFactory', [ '$http', 'appUrl', '$q', function($http, appUrl, $q) {

	var searchRestFactory = {};

	searchRestFactory.getSearchResult = function(searchString) {
		return $http.get(appUrl + 'search/fileName/' + searchString);
	}

	return searchRestFactory;

} ]);
cepApp.controller('StaticPageMasterController', [ '$scope', '$location', 'MediaCenterService', '$http', 'ASVABSampleTestModalService', 'ASVABICATSampleTestModalService', 'resource', 'BringToSchoolModalService', 'videoModalService', 'youTubeModalService', 'vcRecaptchaService', 'recaptchaKey', '$location', 'FaqRestFactory', 'modalService', '$window', 'ASVABImageZoom', function($scope, $location, MediaCenterService, $http, ASVABSampleTestModalService, ASVABICATSampleTestModalService, resource, BringToSchoolModalService, videoModalService, youTubeModalService, vcRecaptchaService, recaptchaKey, $location, FaqRestFactory, modalService, $window, ASVABImageZoom) {

	$scope.resources = resource + 'static/';
	$scope.documents = resource + 'pdf/';
	$scope.media = resource + 'media-center-content/';
	$scope.mediaCenterArticle = 'media-center-article/';

	var url = $location.url();
	$scope.generalHelpGeneral = false ;
	$scope.generalHelpGender  = false ;
	$scope.generalHelpSkill   = false ;
	$scope.generalHelpRelease = false ;
	$scope.generalHelpFyi  = false ;
	
	if (!String.prototype.startsWith) {
	    String.prototype.startsWith = function(searchString, position){
	      position = position || 0;
	      return this.substr(position, searchString.length) === searchString;
	  };
	}
	
	console.log(url);
	if (  url.startsWith("/general-help") && url.indexOf("#") != -1  ) {
		if ( -1 < url.indexOf("#general")) {
			$scope.generalHelpGeneral = true ;
		} else if ( -1 < url.indexOf("#gender") ) {
			$scope.generalHelpGender  = true ;
		} else if ( -1 < url.indexOf("#skill") ) {
			$scope.generalHelpSkill  = true ;
		} else if ( -1 < url.indexOf("#release") ) {
			$scope.generalHelpRelease  = true ;
		} else if ( -1 < url.indexOf("#fyi") ) {
			$scope.generalHelpFyi  = true ;
		}
	}
	
	$scope.absUrl = $location.absUrl();
	

	
	MediaCenterService.getMediaCenterList().then(function(s) {
		$scope.mediaCenterList = s;
	})

	$scope.asvabModal = function(size) {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', url + '#BRING_ASVAB_FORM_LINKED' );
		BringToSchoolModalService.show({}, {});
	};
	
	$scope.occupationPagination = {};
	$scope.occupationPagination.TotalItems = 0;
	$scope.occupationPagination.CurrentPage = 1;
	$scope.occupationPagination.MaxSize = 4;
	$scope.occupationPagination.ItemsPerPage = 10;

	$http.get('/CEP/rest/occufind/ViewAllOccupations/').then(function(occupations) {
		$scope.occupationList = occupations.data;

		$scope.occupationPagination.TotalItems = $scope.occupationList.length;

	});

	/**
	 * Show sample test modal.
	 */
	$scope.showSampleTestModal = function() {
		ASVABSampleTestModalService.show({}, {});
	}
	
	$scope.showSampleIcatTestModal = function() {
		ASVABICATSampleTestModalService.show({}, {});
	}
	

	$scope.showVideoModal = function(videoName) {
		videoModalService.show({
			size : 'lg'
		}, {
			videoName : videoName
		});
	}
	
	$scope.showYouTubeModal = function(videoName) {
		youTubeModalService.show({
			size : 'lg'
		}, {
			videoName : videoName
		});
	}
	
	/**
	 * Bring ASVAB to your school form implementation.
	 */
	$scope.responseOne = '';
	$scope.responseTwo = '';
	$scope.recaptchaKey = recaptchaKey;
	
	$scope.counselorEmail;
	$scope.shareWithCounselor = function() {

		$scope.formDisabled = true;
		$scope.errorSharing = undefined;
		var email = $scope.counselorEmail;

		if ($scope.responseOne === "") { // if string
			// is empty
			$scope.errorSharing = "Please resolve the captcha and submit!";
			$scope.formDisabled = false;
			return false;
		} else {

			FaqRestFactory.shareWithCounselor(email, $scope.responseOne).then(function(success) {
				$window.ga('send', 'pageview', url + '#BRING_ASVAB_FORM_STUDENT_SHARE' );
				
				var modalOptions = {
					headerText : 'Schedule Notification',
					bodyText : 'Your information was submitted successfully.'
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);

				$scope.counselorEmail = undefined;
				$scope.formDisabled = false;
				vcRecaptchaService.reload();

			}, function(error) {
				var error = error.data.errorMessage;
				if (error == undefined) {
					$scope.errorSharing = 'There was an error proccessing your request. Please try again.';
					vcRecaptchaService.reload();
				} else {
					$scope.errorSharing = error;
				}
				$scope.formDisabled = false;
			});
		}

	}
	
	$scope.trackScheduleClick = function() {
		console.log("tracking");
		$window.ga('send', 'pageview', url + '#BRING_ASVAB_FORM_SCHEDULE' );
	}
	
	$scope.imageZoom = function() {
		ASVABImageZoom.show({},{});
	}
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

} ]);

cepApp.controller('TeacherLandingController', ['$scope', '$location', 'MediaCenterService', '$http', 'BringToSchoolModalService', 'youTubeModalService', '$location', 'modalService', '$window', 'TeacherModalService', '$anchorScroll', function($scope, $location, MediaCenterService, $http, BringToSchoolModalService, youTubeModalService, $location, modalService, $window, TeacherModalService, $anchorScroll) {

	$scope.openRegistrtionModal = function() {

		TeacherModalService.show({},{});
	}

	$scope.showYouTubeModal = function(videoURL) {
		youTubeModalService.show({
			size : 'lg'
		}, {
			videoName : videoURL
		});
	}
	/**
	 * Utilizes hash parameter to scroll to video section.
	 */
	if($location.hash() == 'videos'){
		$anchorScroll();
	};
	
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

}]);

cepApp.service('ASVABICATSampleTestModalService', [ '$uibModal', '$sce', '$window', function($uibModal, $sce, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/static-pages/page-partials/asvab-icat-sample-test-modal.html',
		windowClass : 'asvab-modal',
		size : 'lg'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.checkAnswers = function() {
					for (var i = 0; i < $scope.answers.length; i++) {
						if ($scope.answers[i].selection != undefined && $scope.answers[i].answer == $scope.answers[i].selection) {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: green">Correct!</span>');
						} else if ($scope.answers[i].selection != undefined && $scope.answers[i].answer != $scope.answers[i].selection) {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: red">Incorrect: The correct answer is ' + $scope.answers[i].answer + '!</span>');
						} else {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: orange">No answer selected.</span>');
						}
					}

					angular.element(document).ready(function() {

						// $(".modal").scrollTop(0);
						$('.modal').animate({
							scrollTop : $('.modal-content').offset().top - 80
						}, 500);
					});
				}

				$scope.answers = [ {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}];
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('ASVABSampleTestModalService', [ '$uibModal', '$sce', '$window', function($uibModal, $sce, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/static-pages/page-partials/asvab-sample-test-modal.html',
		windowClass : 'asvab-modal',
		size : 'lg'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.checkAnswers = function() {
					for (var i = 0; i < $scope.answers.length; i++) {
						if ($scope.answers[i].selection != undefined && $scope.answers[i].answer == $scope.answers[i].selection) {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: green">Correct!</span>');
						} else if ($scope.answers[i].selection != undefined && $scope.answers[i].answer != $scope.answers[i].selection) {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: red">Incorrect: The correct answer is ' + $scope.answers[i].answer + '!</span>');
						} else {
							$scope.answers[i].message = $sce.trustAsHtml('<span style="color: orange">No answer selected.</span>');
						}
					}

					angular.element(document).ready(function() {

						// $(".modal").scrollTop(0);
						$('.modal').animate({
							scrollTop : $('.modal-content').offset().top - 80
						}, 500);
					});
				}

				$scope.answers = [ {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'b',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'a',
					selection : undefined
				}, {
					answer : 'd',
					selection : undefined
				}, {
					answer : 'c',
					selection : undefined
				} ];
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('ASVABImageZoom', [ '$uibModal', '$sce', '$window', function($uibModal, $sce, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/components/static-pages/page-partials/asvab-zoom-modal.html',
		windowClass : 'asvab-modal',
		size : 'lg'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyImage != undefined) {
				customModalOptions.bodyImage = $sce.trustAsHtml(customModalOptions.bodyImage);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

			}];
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.controller('CareerClusterAddFavorite', [ '$scope', 'UserFactory', 'modalService', function($scope, UserFactory, modalService) {
	
	/**
	 * Add to favorites.
	 */
	$scope.addFavorite = function(ccId, title) {

		// get the current favorites list
		var favoritePromise = UserFactory.getCareerClusterFavoritesList();
		favoritePromise.then(function(response) {
			
			var len = response.length;
			
			// check to see if career cluster was already added because we don't want duplicates
			for (var i = 0; i < len; i++) {
				if (response[i].ccId == ccId) {

					// inform user that the career cluster was already added to the list
					var modalOptions = {
						headerText : 'Career Cluster Favorites',
						bodyText : '<b>' + title + '</b>' + ' was already added to the list. Select another career cluster to add.'
					};

					modalService.showModal({}, modalOptions);
					return false;	
				}
			}
			
			// limit 10 career clusters can be added
			if(len >= 2){
				
				// alert user that their favorites is full
				var modalOptions = {
					headerText : 'Career Cluster Favorites',
					bodyText : 'Your favorite career cluster list is full. This career cluster was not added.'
				};

				modalService.showModal({}, modalOptions);
				
				return false;
			}

			// save the career cluster id to the database if not yet saved
			var promise = UserFactory.insertFavoriteCareerCluster(ccId, title);
			promise.then(function(response) {

				//inform user that the career cluster is added to their favorites
				var modalOptions = {
					headerText : 'Career Cluster Favorites',
					bodyText : '<b>' + title + '</b> is added to your favorites.',
				};

				modalService.showModal({}, modalOptions);

			}, function(reason) {
				
				//inform user that there was an error and to try again.
				console.log(reason);
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'There was an error proccessing your request. Please try again.',
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
				return false;
			});
		}, function(reason) {
			console.log(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.',
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
			return false;
		});

	}
} ]);

cepApp.controller('FooterController', [ '$rootScope', '$scope', '$http', 'UserFactory', 'BringToSchoolModalService', 'appUrl', '$window', '$location',function($rootScope, $scope, $http, UserFactory, BringToSchoolModalService, appUrl, $window, $location) {

	$scope.version = '';
	var path = $location.path();
	
    $http.get(appUrl + "version")
		.then(	function (response) {
			var data = response.data;
			$scope.version = data["application.version"]; 
	});
	
	
	
	/**
	 * Bring ASVAB to your school modal
	 */
	$scope.asvabModal = function() {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_LINKED' );
		BringToSchoolModalService.show({}, {});
	};
	
	$scope.asvabModal = function(showSchedule) {
		$window.ga('create', 'UA-83809749-1', 'auto');
		$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_LINKED' );
		BringToSchoolModalService.show({}, {showSchedule : showSchedule});
	};
	
	$scope.isLoggedIn = function() {
		return $rootScope.globals.currentUser;
	}
} ]);

cepApp.controller('GA', [ '$scope', '$window', '$location', function($scope, $window, $location) {

	// google analytics
	$window.ga('create', 'UA-83809749-1', 'auto');

	/**
	 * Track click using Google Analytics.
	 */
	$scope.trackClick = function(path) {
		$window.ga('send', 'pageview', path);
	}
	
	/**
	 * Tracks with Google Analytics when ICAT sample test is click and submitted.
	 */
	$scope.trackSampleTestClick = function(param) {
		var path = $location.path();
		$window.ga('send', 'pageview', path + param);
	}

	$scope.trackSocialShare = function(socialPlug, value) {
		$window.ga('send', 'event', 'Share', socialPlug, value);
	}

} ]);

cepApp.controller('HeaderController', [ '$scope', '$rootScope','UserFactory', '$location', '$anchorScroll', 'NotesService', function($scope, $rootScope, UserFactory, $location, $anchorScroll, NotesService) {

	
	$scope.goToDashoboard = function(id) {
		var menu = document.getElementById(id); 
	    if (menu.style.display === "none") {
	    	menu.style.display = "block";
	    } else {
	    	menu.style.display =  "none";
	    }
		$location.path('/dashboard');

	}
	
	$scope.isTeacher = function() {
	    if ($location.path()=='/teachers' || $rootScope.globals.currentUser) {
	    	return true;
	    } else {
	    	return false;
	    }
	}

	$scope.goHome = function() {
		$location.path('/');
	}
	
	$scope.goFavorites = function() {
		$location.path('/favorites');
	}

	$scope.noteList = NotesService.getNotes();

	/**
	 * Look for an id named schollLocation via jQuery.
	 * 
	 * Wait 350 milliseconds for frame work to collapse / expand 
	 * the menus.
	 * 
	 * Then scroll to item in page.
	 * 
	 */
	$scope.schroolToItem = function ( scrollLocation){

		var elm = $("#" + scrollLocation);

		  timeOut = setTimeout(function() {
	          $("body").animate({scrollTop: elm.offset().top}, "slow");
		  }, 350);


	}
	/**
	 * Due to Foundation not playing nicely with Angular, this code instance is
	 * used in all controllers using the Foundation menu widget. So if this code
	 * changes, all instances of this code needs to be updated as well.
	 */
	angular.element(document).ready(function() {
		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});
	});

	/**
	 * Allows header template that is using Foundation and Jquery to work with
	 * ng-include.
	 */
	$scope.$on('$includeContentLoaded', function(event) {
		$(document).foundation();

		$('.dropdown.menu > li > a').click(function() {
			var li = $(this).closest('li');
			if (li.hasClass('open')) {
				li.removeClass('open');
			} else {
				$('.dropdown.menu > li').removeClass('open');
				li.addClass('open');
			}
			return false;
		});

	});

} ]);

cepApp.controller('LeftNavigationController', [ 
	'$scope', '$rootScope', 'UserFactory', 'fyiManualInput', 'PortfolioService', '$location', 'modalService', '$window',
	function($scope, $rootScope, UserFactory, fyiManualInput, PortfolioService, $location, modalService, $window) {

	if ( typeof $rootScope.globals.currentUser === 'undefined') return;
	
	UserFactory.getNumberOfTestTaken().then(function(response) {
		$scope.numberTestTaken = response;
	});
	
	$scope.showScoresTable = false;

	$scope.toggleScoreView = function() {
		$scope.showScoresTable = $scope.showScoresTable === false;
	}
	
	$scope.userRole = $rootScope.globals.currentUser.role;
	
	// Retrieve scores from session.
	$scope.verbalScore = $window.sessionStorage.getItem('sV');
	$scope.mathScore = $window.sessionStorage.getItem('sM');
	$scope.scienceScore = $window.sessionStorage.getItem('sS');
	$scope.afqtScore = $window.sessionStorage.getItem('sA');
	$scope.haveManualAsvabTestScores = $window.sessionStorage.getItem('manualScores') == "true";
	// Retrieve book objects from session.
	$scope.vBooks = angular.fromJson($window.sessionStorage.getItem('vBooks'));
	$scope.mBooks = angular.fromJson($window.sessionStorage.getItem('mBooks'));
	$scope.sBooks = angular.fromJson($window.sessionStorage.getItem('sBooks'));

	/**
	 * Get scores for display
	 */
	$scope.getScore = function(area) {
		var score = 0;
		switch (area) {
			case "verbal":
				score = $scope.verbalScore;
				break;
			case "math":
				score = $scope.mathScore;
				break;
			case "science":
				score = $scope.scienceScore;
				break;
			case "afqt":
				score = $scope.afqtScore;
				break;
			default:
				break;
		}
		return score;
	}

	/**
	 * Get book sprite x pos
	 */
	$scope.getBookPos = function(area) {
		// return bsackground style position object
		switch (area) {
			case "verbal":
				return {"background-position": $scope.vBooks.xPosSmall + "px 0px"};
			case "math":
				return {"background-position": $scope.mBooks.xPosSmall + "px 0px"};
			case "science":
				return {"background-position": $scope.sBooks.xPosSmall + "px 0px"}; 
		}
	}
	
	$scope.isShowTest = function() {
		if($rootScope.globals.currentUser != undefined)
			var userRole = $rootScope.globals.currentUser.role;
		else
			var userRole = 'S';

		// student role can only
		if (userRole != 'A') {
			if ($scope.numberTestTaken < 2) {
				return true;
			}
			return false;
		}
		return true;
	}
	
	$scope.fyiManualInput = function() {
		fyiManualInput.show({}, {});
	}
	
	/**
	 * Route user to portfolio page.
	 */
	$scope.routeToPortfolio = function() {
		var promise = PortfolioService.getIsPortfolioStarted();
		promise.then(function(success) {

			// if portfolio exists then skip instructions
			if (success > 0) {
				$location.path('/portfolio');
			} else {
				$location.path('/portfolio-directions');
			}
		}, function(error) {
			console.log(error);
		});

	}
	
	// set user interest codes
	UserFactory.getUserInterestCodes().then(function(response) {
		$scope.interestCodeOne = response.interestCodeOne;
		$scope.interestCodeTwo = response.interestCodeTwo;
		$scope.interestCodeThree = response.interestCodeThree;
		$scope.scoreChoice = response.scoreChoice;
	});
	
	$rootScope.$on("updateInterestCodes", function(){
		$scope.loadingIndicator = true;
		UserFactory.getUserInterestCodes().then(function(response) {
			$scope.interestCodeOne = response.interestCodeOne;
			$scope.interestCodeTwo = response.interestCodeTwo;
			$scope.interestCodeThree = response.interestCodeThree;
			$scope.scoreChoice = response.scoreChoice;
			$scope.loadingIndicator = false;
		}, function(error){
			$scope.loadingIndicator = false;
		});
     });

	$scope.resultsInfo = function() {
		var customModalOptions = {
				headerText : 'ASVAB Results',
				bodyText : '<p>ASVAB Results More Information Coming Soon!</p>' + 
				'<p>Need content to display here.</p>'
			};
			var customModalDefaults = {size : 'lg'};
			modalService.show(customModalDefaults, customModalOptions);
	}
	
	$scope.interestCodeInfo = function(interestCode) {
		var modalOptions;
		switch (interestCode) {
		case "R":
			modalOptions = {
				headerText : 'Realistic',
				bodyText : '<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
				'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
				'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li></ul>'
			};
			break;
		case "I":
			modalOptions = {
				headerText : 'Investigative',
				bodyText : '<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
				'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
				'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li></ul>'
			};
			break;
		case "A":
			modalOptions = {
				headerText : 'Artistic',
				bodyText : '<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
				'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
				'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li></ul>'
			};
			break;
		case "S":
			modalOptions = {
				headerText : 'Social',
				bodyText : '<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
				'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
				'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li></ul>'
			};
			break;
		case "E":
			modalOptions = {
				headerText : 'Enterprising',
				bodyText : '<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
				'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
				'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li></ul>'
			};
			break;
		case "C":
			modalOptions = {
				headerText : 'Conventional',
				bodyText : '<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
				'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
				'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li></ul>'
			};
			break;
		}
		

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}
	
	$scope.interestCodesInfo = function() {
		
		var score = $scope.scoreChoice == 'gender' ? 'Gender-Specific' : 'Combined';
		var modalOptions = {
			headerText : 'Interest Codes',
			bodyText : "You're currently using your <strong>" + score + "</strong> scores for career exploration. You have two sets of FYI results, Gender-Specific and Combined. To change the set you're using for career exploration and to learn more about FYI results, go to Return to FYI Results under Step 1."
			/*bodyText : '<p><strong style="color: #2279BF">Realistic</strong></p>' +
			'<ul><li>Realistic activities often involve practical, hands-on problems and solutions such as designing, building, and repairing machinery.</li>' +
			'<li>Realistic occupations generally require workers to have physical and mechanical abilities.</li>' +
			'<li>Realistic types generally prefer to work with things (machines, plants, and animals) rather than people. In their work, they are practical, straightforward, and persistent.</li></ul>' +
			'<p><strong style="color: #892C32">Investigative</strong></p>' +
			'<ul><li>Investigative activities involve testing ideas, learning new things, and allow you to use your knowledge to solve problems.</li>' +
			'<li>Investigative occupations generally require workers to have math and science abilities.</li>' +
			'<li>Investigative types generally prefer to work with ideas rather than with people or things. In their work, they are curious, observant, and logical.</li></ul>' +
			'<p><strong style="color: #F3913D">Artistic</strong></p>' +
			'<ul><li>Artistic activities allow you to be creative to do original work such as writing, singing, dancing, sculpting, and painting.</li>' +
			'<li>Artistic occupations generally require workers to have artistic abilities and a good imagination.  </li>' +
			'<li>Artistic types generally prefer to work with ideas rather than things, and like to be in a setting where they can work without a clear set of rules. In their work, they are expressive, original, and independent.</li></ul>' +
			'<p><strong style="color: #174377">Social</strong></p>' +
			'<ul><li>Social activities allow you to use your skills and talents to help, teach, and counsel others.</li>' +
			'<li>Social occupations generally require personal interaction and communication skills and abilities.</li>' +
			'<li>Social types prefer to work with people rather than to work with objects, machines, or data. In their work, they are friendly, outgoing, and helpful.</li></ul>' +
			'<p><strong style="color: #ea292d">Enterprising</strong></p>' +
			'<ul><li>Enterprising activities allow you to take a leadership role in areas such as sales, supervision, and project or business management.</li>' +
			'<li>Enterprising occupations generally require workers to have leadership, sales, and speaking abilities.</li>' +
			'<li>Enterprising types prefer to work with people and ideas rather than things. They like work that is fast-paced, requires a lot of responsibility and decision making, and involves taking risks for profit. In their work, they are persuasive, ambitious, and energetic.</li></ul>' +
			'<p><strong style="color: #52843c">Conventional</strong></p>' +
			'<ul><li>Conventional activities require attention to accuracy and detail such as maintaining orderly and accurate records, procedures, and routines.</li>' +
			'<li>Conventional occupations generally require workers to have clerical, organizational, and arithmetic abilities.</li>' +
			'<li>Conventional types prefer working with data more than with ideas. They apply precise standards in a setting where there is a clear line of authority. In their work, they are efficient, methodical, and detail-oriented.</li></ul>'*/
		};

		modalService.showModal({
			size : 'sm'
		}, modalOptions);
	}
	
	/**
	 * 
	 * Popup modal used on Occu-find pages.
	 * 
	 */
	$scope.careerDefinitionsNoCareerCluster = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" +
			"<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
			"<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" +
			"<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" + 
			"<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'lg'
		}, modalOptions);
	}
	
	$scope.careerDefinitions = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>Available in the Military</b> indicates occupations that are offered in one of more of the Military Services, each with its own unique requirements.</p>" +
			"<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" + 
			"<p><b>Career Clusters</b> contain occupations in the same field of work that require similar skills. You can use Career Clusters to help focus education plans towards obtaining the necessary knowledge, competencies, and training for success in a particular career pathway.</p>" +
			"<p><b>Hot Military Jobs</b> are occupations related to this career that are reported as critical fill by one of more of the Military Services. </p>" +
			"<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" + 
			"<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" + 
			"<p><i>*These definitions were provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'lg'
		}, modalOptions);
	}
	
	$scope.stemDefinition = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>STEM</b> occupations require education in science, technology, engineering, and mathematics (STEM) disciplines.</p>" + 
			"<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'md'
		}, modalOptions);
	}
	
	$scope.brightDefinition = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>Bright Outlook</b> occupations are expected to grow rapidly in the next several years, will have large numbers of job openings, or are new and emerging occupations. </p>" + 
			"<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'md'
		}, modalOptions);
	}
	
	$scope.greenDefinition = function() {
		var modalOptions = {
			headerText : '',
			bodyText : "<p><b>Green</b> occupations will likely change as a result of the green economy. Green economy activities and technologies are increasing the demand for occupations, shaping the work and worker requirements needed for occupational performance, or generating new and emerging occupations.</p>" +
			"<p><i>*This definition was provided by the National Center for O*NET Development for the U.S. Department of Labor obtained from O*NET Online.</i></p>"
		};

		modalService.showModal({
			size : 'md'
		}, modalOptions);
	}
	
	$scope.returnResultsDisabled = function() {
	      return $location.path() === '/my-asvab-summary-results';
	};
	
	$scope.returnManualResultsDisabled = function() {
	      return $location.path() === '/my-asvab-summary-results-manual';
	};
		$scope.occufindDisabled = function() {
	      return $location.path() === '/occufind-occupation-search';
	};
	
	$scope.returnFyiDisabled = function() {
	      return $location.path() === '/find-your-interest-score';
	};
	
	$scope.takeFyiDisabled = function() {
	      return $location.path() === '/find-your-interest';
	};
	
	$scope.portfolioDisabled = function() {
	      return $location.path() === '/portfolio-directions';
	};

	$scope.workValuesHint = function() {
		var modalOptions = {
			headerText : 'Work Values',
			bodyText : "<p>Consider your top work values when exploring occupations and creating your action plan for the future. You can take the work values assessment <a href='work-values-test'>here</a>.</p>"
		};

		modalService.showModal({
			size : 'md'
		}, modalOptions);
	}
} ]);

cepApp.controller('LogOutController', [ '$scope', '$window', '$rootScope', 'UserFactory', 'AuthenticationService', '$location', 'LoginModalService', 'modalService','ipCookie', 'ssoUrl', function($scope, $window, $rootScope, UserFactory, AuthenticationService, $location, LoginModalService, modalService, ipCookie, ssoUrl) {

	$scope.logout = function() {
		AuthenticationService.ClearCredentials();
		UserFactory.clearSessionStorage();
		ipCookie('globals', 'expire', {expires: 0, expirationUnit: 'seconds',  path: '/'});
		//ipCookie('JSESSIONID', 'expire', { expires: 0, expirationUnit: 'seconds', path: '/CEP'});
		//$location.path("/");
		
		//$window.location.href = '/CEP/perform_logout';
		$window.location.href = ssoUrl + 'perform_logout';

		/*var modalOptions = {
			headerText : 'Logged Out',
			bodyText : 'You have successfully logged out.'
		};

		modalService.showModal({
			size : 'sm'
		}, modalOptions);*/
	}
	
	$scope.goToDashoboard = function() {
		$location.path('/dashboard');
	}

	$scope.showLoginModal = function() {
		LoginModalService.show({}, {});
	}

	/**
	 * Returns boolean value if user is logged in or not.
	 */
	$scope.isLoggedIn = function() {
		return $rootScope.globals.currentUser;
	}

} ]);

cepApp.controller('MobileInterestCodesController', [ '$scope', '$rootScope', 'UserFactory', '$window',function($scope, $rootScope, UserFactory, $window) {

	
	if ( typeof $rootScope.globals.currentUser === 'undefined') return;
	
	UserFactory.getNumberOfTestTaken().then(function(response) {
		$scope.numberTestTaken = response;
	});
	
	if (typeof $rootScope.globals.currentUser === 'undefined') {
		$scope.occuFindUserLoggedIn = false ;
	} else {
		$scope.occuFindUserLoggedIn = true ;
	}
	
	$scope.showScoresTable = false;

	$scope.toggleScoreView = function() {
		$scope.showScoresTable = $scope.showScoresTable === false;
	}
	
	// Retrieve scores from session.
	$scope.verbalScore = $window.sessionStorage.getItem('sV');
	$scope.mathScore = $window.sessionStorage.getItem('sM');
	$scope.scienceScore = $window.sessionStorage.getItem('sS');
	$scope.afqtScore = $window.sessionStorage.getItem('sA');
	$scope.haveManualAsvabTestScores = $window.sessionStorage.getItem('manualScores') == "true";
	// Retrieve book objects from session.
	$scope.vBooks = angular.fromJson($window.sessionStorage.getItem('vBooks'));
	$scope.mBooks = angular.fromJson($window.sessionStorage.getItem('mBooks'));
	$scope.sBooks = angular.fromJson($window.sessionStorage.getItem('sBooks'));

	/**
	 * Get scores for display
	 */
	$scope.getScore = function(area) {
		var score = 0;
		switch (area) {
			case "verbal":
				score = $scope.verbalScore;
				break;
			case "math":
				score = $scope.mathScore;
				break;
			case "science":
				score = $scope.scienceScore;
				break;
			case "afqt":
				score = $scope.afqtScore;
				break;
			default:
				break;
		}
		return score;
	}

	/**
	 * Get book sprite x pos
	 */
	$scope.getBookPos = function(area) {
		// return bsackground style position object
		switch (area) {
			case "verbal":
				return {"background-position": $scope.vBooks.xPosSmall + "px 0px"};
			case "math":
				return {"background-position": $scope.mBooks.xPosSmall + "px 0px"};
			case "science":
				return {"background-position": $scope.sBooks.xPosSmall + "px 0px"}; 
		}
	}
	// set user interest codes
	var promise = UserFactory.getUserInterestCodes();
	if ( typeof  promise == 'undefined' ) return;
	promise.then(function(response) {
		$scope.interestCodeOne = response.interestCodeOne;
		$scope.interestCodeTwo = response.interestCodeTwo;
		$scope.interestCodeThree = response.interestCodeThree;
	});
} ]);

cepApp.controller('AddFavorites', [ '$scope', 'UserFactory', 'modalService', '$route', function($scope, UserFactory, modalService, $route) {

	/**
	 * Add to favorites.
	 */
	$scope.addFavorite = function() {

		// get the current favorites list
		var favoritePromise = UserFactory.getFavoritesList();
		favoritePromise.then(function(response) {
			
			var len = response.length;
			
			// check to see if occupation was already added because we don't want duplicates
			for (var i = 0; i < len; i++) {
				if (response[i].onetSocCd == $route.current.params.socId) {

					// inform user that the occupation was already added to the list
					var modalOptions = {
						headerText : 'Favorites',
						bodyText : '<b>' + $scope.occupationTitle + '</b>' + ' was already added to the list. Select another occupation to add.'
					};

					modalService.showModal({}, modalOptions);
					return false;
				}
			}
			
			// limit 10 occupations can be added
			if(len >= 10){
				
				// alert user that their favorites is full
				var modalOptions = {
					headerText : 'Favorites',
					bodyText : 'Your favorite occupation list is full. This occupation was not added.'
				};

				modalService.showModal({}, modalOptions);
				
				return false;
			}

			// save the occupation soc id to the database if not yet saved
			var promise = UserFactory.insertFavoriteOccupation($route.current.params.socId, $scope.occupationTitle);
			promise.then(function(response) {

				//inform user that the occupation is added to their favorites
				var modalOptions = {
					headerText : 'Favorites',
					bodyText : '<b>' + $scope.occupationTitle + '</b> is added to your favorites.',
				};

				modalService.showModal({}, modalOptions);

			}, function(reason) {
				
				//inform user that there was an error and to try again.
				console.log(reason);
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'There was an error proccessing your request. Please try again.',
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
				return false;
			});
		}, function(reason) {
			console.log(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.',
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
			return false;
		});

	}
} ]);

cepApp.controller('SchoolAddFavorites', [ '$scope', 'UserFactory', 'modalService', '$route', function($scope, UserFactory, modalService, $route) {

	/**
	 * Add to favorites.
	 */
	$scope.addFavorite = function() {

		// get the current favorites list
		var favoritePromise = UserFactory.getSchoolFavoritesList();
		favoritePromise.then(function(response) {
			
			var len = response.length;
			
			// check to see if school was already added because we don't want duplicates
			for (var i = 0; i < len; i++) {
				if (response[i].unitId == $route.current.params.unitId) {

					// inform user that the school was already added to the list
					var modalOptions = {
						headerText : 'School Favorites',
						bodyText : '<b>' + $scope.schoolTitle + '</b>' + ' was already added to the list. Select another school to add.'
					};

					modalService.showModal({}, modalOptions);
					return false;
				}
			}
			
			// limit 10 school can be added
			if(len >= 10){
				
				// alert user that their favorites is full
				var modalOptions = {
					headerText : 'School Favorites',
					bodyText : 'Your favorite school list is full. This school was not added.'
				};

				modalService.showModal({}, modalOptions);
				
				return false;
			}

			// save the occupation soc id to the database if not yet saved
			var promise = UserFactory.insertFavoriteSchool($route.current.params.unitId, $scope.schoolTitle);
			promise.then(function(response) {

				//inform user that the occupation is added to their favorites
				var modalOptions = {
					headerText : 'School Favorites',
					bodyText : '<b>' + $scope.schoolTitle + '</b> is added to your favorites.',
				};

				modalService.showModal({}, modalOptions);

			}, function(reason) {
				
				//inform user that there was an error and to try again.
				console.log(reason);
				var modalOptions = {
					headerText : 'Error',
					bodyText : 'There was an error proccessing your request. Please try again.',
				};

				modalService.showModal({
					size : 'sm'
				}, modalOptions);
				return false;
			});
		}, function(reason) {
			console.log(reason);
			var modalOptions = {
				headerText : 'Error',
				bodyText : 'There was an error proccessing your request. Please try again.',
			};

			modalService.showModal({
				size : 'sm'
			}, modalOptions);
			return false;
		});

	}
} ]);

cepApp.factory('responseObserver', ['$q', '$location', '$rootScope', '$cookieStore', '$injector', function responseObserver($q, $location, $rootScope, $cookieStore, $injector) {
	return {
		'responseError' : function(errorResponse) {

			var loggedIn = $rootScope.globals.currentUser;
			
			switch (errorResponse.status) {
			case 403:

				if (loggedIn) {
					$rootScope.globals = {};
					$cookieStore.remove('globals');
					var userFactory = $injector.get('UserFactory');
					userFactory.clearSessionStorage();
					$location.path('/');

					// Alert user session was lost.
					var modalOptions = {
						headerText : 'Error',
						bodyText : 'Session Ended! Please log back in to continue what you were doing.'
					};

					var modal = $injector.get('SessionModalService');
					modal.showModal({
						size : 'sm'
					}, modalOptions);
				}
				return false;
			}

			return $q.reject(errorResponse);
		}
	};
}]);
cepApp.factory('AuthenticationService', ['$http', '$cookieStore', '$rootScope', '$timeout', '$window', 'ipCookie', function($http, $cookieStore, $rootScope, $timeout, $window,ipCookie) {
	var service = {};

	service.Login = Login;
	service.SetCredentials = SetCredentials;
	service.ClearCredentials = ClearCredentials;

	return service;

	function Login(username, password, callback) {

		/*
		 * Dummy authentication for testing, uses $timeout to simulate api call
		 * ----------------------------------------------
		 */
		$timeout(function() {
			var response;
			/*
			 * UserService.GetByUsername(username) .then(function (user) {
			 */

			if ('test' === password) {
				response = {
					success : true
				};
			} else {
				response = {
					success : false,
					message : 'Username or password is incorrect'
				};
			}
			callback(response);
			// });
		}, 1000);

		/*
		 * Use this for real authentication
		 * ----------------------------------------------
		 */
		// $http.post('/api/authenticate', { username: username,
		// password: password })
		// .success(function (response) {
		// callback(response);
		// });
	}

	function SetCredentials(username, password, userId, role, schoolCode, schoolName, city, state, zipCode, mepsId, mepsName) {
		var now = new Date();
		var authdata = Base64.encode(userId + ':' + password + ':' + now.toISOString() );

		$rootScope.globals = {
			currentUser : {
				username : username,
				userId: userId,
				role: role,
				authdata : authdata,
				schoolCode : schoolCode,
				schoolName : schoolName,
				city :	city,
				state : state,
				zipCode : zipCode,
				mepsId : mepsId,
				mepsName : mepsName
			}
		};

		$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint
																				// ignore:line
		// $cookieStore.put('globals', $rootScope.globals);
		// ipCookie.put('x', 5, {path: '/CEP'})
		ipCookie('globals',$rootScope.globals, { path: '/'});
	}

	function ClearCredentials() {
		$rootScope.globals = {};
		$cookieStore.remove('globals');
		$http.defaults.headers.common.Authorization = 'Basic';
	}
}]);

// Base64 encoding service used by AuthenticationService
var Base64 = {

	keyStr : 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

	encode : function(input) {
		var output = "";
		var chr1, chr2, chr3 = "";
		var enc1, enc2, enc3, enc4 = "";
		var i = 0;

		do {
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output + this.keyStr.charAt(enc1) + this.keyStr.charAt(enc2) + this.keyStr.charAt(enc3) + this.keyStr.charAt(enc4);
			chr1 = chr2 = chr3 = "";
			enc1 = enc2 = enc3 = enc4 = "";
		} while (i < input.length);

		return output;
	},

	decode : function(input) {
		var output = "";
		var chr1, chr2, chr3 = "";
		var enc1, enc2, enc3, enc4 = "";
		var i = 0;

		// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
		var base64test = /[^A-Za-z0-9\+\/\=]/g;
		if (base64test.exec(input)) {
			window.alert("There were invalid base64 characters in the input text.\n" + "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" + "Expect errors in decoding.");
		}
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		do {
			enc1 = this.keyStr.indexOf(input.charAt(i++));
			enc2 = this.keyStr.indexOf(input.charAt(i++));
			enc3 = this.keyStr.indexOf(input.charAt(i++));
			enc4 = this.keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

			chr1 = chr2 = chr3 = "";
			enc1 = enc2 = enc3 = enc4 = "";

		} while (i < input.length);

		return output;
	}
};

cepApp.service('BringToSchoolModalService', [ '$uibModal', 'FaqRestFactory', 'modalService', 'vcRecaptchaService', 'recaptchaKey', '$location', '$window', function($uibModal, FaqRestFactory, modalService, vcRecaptchaService, recaptchaKey, $location, $window) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/page-partials/bring-asvab-to-your-school.html',
		size : 'lg',
		windowClass : 'new-modal'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				
				// Track event in Google Analytics.
				var path = $location.path();
				$window.ga('create', 'UA-83809749-1', 'auto');
				$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_VIEW' );
				
				$scope.responseOne = '';
				$scope.responseTwo = '';
				$scope.recaptchaKey = recaptchaKey;
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.counselorEmail;
				$scope.shareWithCounselor = function() {

					$scope.formDisabled = true;
					$scope.errorSharing = undefined;
					var email = $scope.counselorEmail;

					if ($scope.responseOne === "") { // if string
						// is empty
						$scope.errorSharing = "Please resolve the captcha and submit!";
						$scope.formDisabled = false;
						return false;
					} else {

						FaqRestFactory.shareWithCounselor(email, $scope.responseOne).then(function(success) {
							$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_STUDENT_SHARE' );
							
							$uibModalInstance.dismiss('cancel');
							var modalOptions = {
								headerText : 'Schedule Notification',
								bodyText : 'Your information was submitted successfully.'
							};

							modalService.showModal({
								size : 'sm'
							}, modalOptions);

							$scope.counselorEmail = undefined;
							$scope.formDisabled = false;

						}, function(error) {
							var error = error.data.errorMessage;
							if (error == undefined) {
								$scope.errorSharing = 'There was an error proccessing your request. Please try again.';
								vcRecaptchaService.reload();
							} else {
								$scope.errorSharing = error;
							}
							$scope.formDisabled = false;
						});
					}

				}

				if (customModalOptions != undefined && customModalOptions.showSchedule != undefined) {
					$scope.showSchedule = customModalOptions.showSchedule;
				} else {
					$scope.showSchedule = false;
				}
				

				$scope.trackScheduleClick = function() {
					$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_SCHEDULE' );
				}

				// form properties
				$scope.title;
				$scope.position;
				$scope.firstName;
				$scope.lastName;
				$scope.phone;
				$scope.email;
				$scope.heardUs;
				$scope.heardUsOther;
				$scope.schoolName;
				$scope.schoolAddress;
				$scope.schoolCity;
				$scope.schoolCounty;
				$scope.schoolState;
				$scope.schoolZip;
				$scope.widgetId;
				$scope.onWidgetCreate = function(_widgetId) {
					$scope.widgetId = _widgetId;
				};

				$scope.scheduleSubmit = function() {
					$scope.captchaMessage = undefined;
					$scope.formDisabled = true;

					if ($scope.responseTwo === "") { // if string
						// is empty
						$scope.captchaMessage = "Please resolve the captcha and submit!";
						$scope.formDisabled = false;
						return false;
					} else {

						$scope.error = undefined;
						var emailObject = {
							title : $scope.title,
							position : $scope.position,
							firstName : $scope.firstName,
							lastName : $scope.lastName,
							phone : $scope.phone,
							email : $scope.email,
							heardUs: $scope.heardUs,
							heardUsOther: $scope.heardUsOther,
							schoolName : $scope.schoolName,
							schoolAddress : $scope.schoolAddress,
							schoolCity : $scope.schoolCity,
							schoolCounty : $scope.schoolCounty,
							schoolState : $scope.schoolState,
							schoolZip : $scope.schoolZip,
							recaptcha : $scope.responseTwo
						}

						FaqRestFactory.scheduleEmail(emailObject).then(function(success) {
							
							$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_SCHEDULE_SUBMIT' );
							
							$uibModalInstance.dismiss('cancel');
							var modalOptions = {
								headerText : 'Schedule Notification',
								bodyText : 'Your information was submitted successfully.'
							};

							modalService.showModal({
								size : 'sm'
							}, modalOptions);

							$scope.title = undefined;
							$scope.position = undefined;
							$scope.firstName = undefined;
							$scope.lastName = undefined;
							$scope.phone = undefined;
							$scope.email = undefined;
							$scope.heardUs = undefined;
							$scope.heardUsOther = undefined;
							$scope.schoolName = undefined;
							$scope.schoolAddress = undefined;
							$scope.schoolCity = undefined;
							$scope.schoolCounty = undefined;
							$scope.schoolState = undefined;
							$scope.schoolZip = undefined;
							$scope.formDisabled = false;

						}, function(error) {

							var error = error.data.errorMessage;
							if (error == undefined) {
								$scope.error = 'There was an error proccessing your request. Please try again.';
								vcRecaptchaService.reload($scope.widgetId);
							} else {
								$scope.error = error;
							}

							$scope.formDisabled = false;

						});
					}
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.factory('errorLogService', [ '$log', '$window', 'appUrl', function($log, $window, appUrl) {

	function log(exception) {
		$log.error.apply($log, arguments);
		try {
			var args = [];
			if (typeof arguments === 'object') {
				for (var i = 0; i < arguments.length; i++) {
					arg = arguments[i];
					var exceptionItem = {};
					exceptionItem.message = arg.message;
					exceptionItem.stack = arg.stack;

					// use jquery to log information server side
					$.ajax({
						type : "POST",
						url : appUrl + "javascript-error-logging/log-error",
						contentType : "application/json",
						data : angular.toJson({
							errorUrl : $window.location.href,
							errorMessage : exceptionItem.message,
							errorStackTrace : exceptionItem.stack
						})
					});

					args.push(JSON.stringify(exceptionItem));
				}
			}
		} catch (loggingError) {
			// For Developers - log the log-failure.
			$log.warn("Error logging failed");
			$log.log(loggingError);
		}
	}
	return (log);
} ]);

cepApp.service('learnMoreModalService', [ '$uibModal', '$sce', function($uibModal, $sce) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/page-partials/learn-more-modal.html'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass : 'asvab-modal'
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('modalService', [ '$uibModal', '$sce', function($uibModal, $sce) {

	var modalDefaults = {
		animation : true,
		/*
		 * templateUrl: 'cep/page-partials/modal.html',
		 */templateUrl : 'cep/page-partials/modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass : 'asvab-modal'
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.service('SessionModalService', [ '$uibModal', '$sce', '$window', 'base', function($uibModal, $sce, $window, base) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/page-partials/session-modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass : 'asvab-modal'
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$window.location.href = '';
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);
cepApp.factory('UserFactory', [ '$q', '$window', '$rootScope', 'FYIRestFactory', 'FavoriteRestFactory', function($q, $window, $rootScope, FYIRestFactory, FavoriteRestFactory) {

	var userFactory = {};

	// user's interest codes
	var interestCode = {
		interestCodeOne : undefined,
		interestCodeTwo : undefined,
		interestCodeThree : undefined,
		scoreChoice : undefined
	};

	var numberTestTaken = undefined;

	userFactory.getNumberOfTestTaken = function() {
		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();

		if (typeof (Storage) !== "undefined") {

			if ($window.sessionStorage.getItem("numberTestTaken") === null || $window.sessionStorage.getItem("numberTestTaken") == 'undefined') {
				FYIRestFactory.getNumberTestTaken(userId).success(function(data) {
					$window.sessionStorage.setItem('numberTestTaken', data);
					numberTestTaken = data;
					deferred.resolve(numberTestTaken);
				}).error(function(data, status, headers, config) {
					deferred.reject("Error: request returned status " + status);
				});
			} else {
				numberTestTaken = $window.sessionStorage.getItem('numberTestTaken');
				deferred.resolve(numberTestTaken);
				return deferred.promise;

			}
		} else {
			// for browser that don't support session storage
			FYIRestFactory.getNumberTestTaken(userId).success(function(data) {
				numberTestTaken = data;
				deferred.resolve(numberTestTaken);
			}).error(function(data, status, headers, config) {
				deferred.reject("Error: request returned status " + status);
			});
		}
		return deferred.promise;
	}

	/*
	 * Gets user's interest codes and stores in session storage if browser
	 * supports.
	 */
	userFactory.getUserInterestCodes = function() {
		// Please don't remove 
		if (typeof $rootScope.globals.currentUser == 'undefined' ) return;		
		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();
		if (typeof (Storage) !== "undefined") {
			// utilize session storage

			if ($window.sessionStorage.getItem("interestCodeOne") === null || $window.sessionStorage.getItem("interestCodeOne") === 'undefined') {

				FYIRestFactory.getUserInteresteCodes(userId).success(function(results) {
					if (results[0] != undefined) {
						$window.sessionStorage.setItem('interestCodeOne', results[0].interestCodeOne);
						$window.sessionStorage.setItem('interestCodeTwo', results[0].interestCodeTwo);
						$window.sessionStorage.setItem('interestCodeThree', results[0].interestCodeThree);
						$window.sessionStorage.setItem('scoreChoice', results[0].scoreChoice);

						interestCode = {
							interestCodeOne : $window.sessionStorage.getItem('interestCodeOne'),
							interestCodeTwo : $window.sessionStorage.getItem('interestCodeTwo'),
							interestCodeThree : $window.sessionStorage.getItem('interestCodeThree'),
							scoreChoice : $window.sessionStorage.getItem('scoreChoice')
						};

						deferred.resolve(interestCode);
					}
					deferred.resolve([]); // return empty array
				}).error(function(data, status, headers, config) {
					deferred.reject("Error: request returned status " + status);
				});

				return deferred.promise;

			} else {
				interestCode = {
					interestCodeOne : $window.sessionStorage.getItem('interestCodeOne'),
					interestCodeTwo : $window.sessionStorage.getItem('interestCodeTwo'),
					interestCodeThree : $window.sessionStorage.getItem('interestCodeThree'),
					scoreChoice : $window.sessionStorage.getItem('scoreChoice')
				};
				deferred.resolve(interestCode);
				return deferred.promise;

			}
		} else {
			// for browser that don't support session storage
			FYIRestFactory.getUserInteresteCodes(userId).success(function(data) {
				interestCode = {
					interestCodeOne : data[0].interestCodeOne,
					interestCodeTwo : data[0].interestCodeTwo,
					interestCodeThree : data[0].interestCodeThree,
					scoreChoice : data[0].scoreChoice
				};
				deferred.resolve(interestCode);
			}).error(function(data, status, headers, config) {
				deferred.reject("Error: request returned status " + status);
			});
			return deferred.promise;
		}

	}

	/**
	 * Favorites Implementation
	 */
	var favoritesList = undefined;

	/**
	 * Add occupation to favorites
	 */
	userFactory.insertFavoriteOccupation = function(socId, title) {
		var favoriteObject = {
			userId : $rootScope.globals.currentUser.userId,
			onetSocCd : socId
		};

		var deferred = $q.defer();

		FavoriteRestFactory.insertFavoriteOccupation(favoriteObject).success(function(data) {

			// add and set title property
			data.title = title;

			// push newly added occupation to property
			favoritesList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('favoriteList', JSON.stringify(favoritesList));

				// sync with in memory property
				favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Return favorite list.
	 */
	userFactory.getFavoritesList = function() {

		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (favoritesList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("favoriteList") === null || $window.sessionStorage.getItem("favoriteList") == 'undefined') {

					// recover data using database
					FavoriteRestFactory.getFavoriteOccupation(userId).success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('favoriteList', JSON.stringify(data));

						// sync with in memory property
						favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));

						deferred.resolve(favoritesList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));
					deferred.resolve(favoritesList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getFavoriteOccupation(userId).success(function(data) {

					// success now store in session object
					favoritesList = data;
					deferred.resolve(favoritesList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(favoritesList);
		}

		return deferred.promise;
	}

	userFactory.deleteFavorite = function(id) {
		var deferred = $q.defer();
		FavoriteRestFactory.deleteFavoriteOccupation(id).success(function(data) {

			// sync with in memory property
			var length = favoritesList.length;
			for (var i = 0; i < length; i++) {
				if (favoritesList[i].id == id) {
					favoritesList.splice(i, 1);
					break;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('favoriteList', JSON.stringify(favoritesList));
				favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));
			}

			deferred.resolve(favoritesList);

		}).error(function(data, status, headers, config) {

			console.log('Error' + status);
			deferred.reject("Error: request returned status" + status);
		});

		return deferred.promise;

	}

	/**
	 * Career Cluster Favorites Implementation
	 */
	var careerClusterFavoritesList = undefined;

	/**
	 * Add occupation to favorites
	 */
	userFactory.insertFavoriteCareerCluster = function(ccId, title) {
		var favoriteObject = {
			userId : $rootScope.globals.currentUser.userId,
			ccId : ccId
		};

		var deferred = $q.defer();

		FavoriteRestFactory.insertFavoriteCareerCluster(favoriteObject).success(function(data) {

			// add and set title property
			data.title = title;

			// push newly added occupation to property
			careerClusterFavoritesList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(careerClusterFavoritesList));

				// sync with in memory property
				careerClusterFavoritesList = angular.fromJson($window.sessionStorage.getItem('careerClusterFavoritesList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Return career cluster favorite list.
	 */
	userFactory.getCareerClusterFavoritesList = function() {

		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (careerClusterFavoritesList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("careerClusterFavoritesList") === null || $window.sessionStorage.getItem("careerClusterFavoritesList") == 'undefined') {

					// recover data using database
					FavoriteRestFactory.getFavoriteCareerCluster(userId).success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(data));
						// sync with in memory property
						careerClusterFavoritesList = angular.fromJson($window.sessionStorage.getItem('careerClusterFavoritesList'));
						deferred.resolve(careerClusterFavoritesList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					careerClusterFavoritesList = angular.fromJson($window.sessionStorage.getItem('careerClusterFavoritesList'));
					deferred.resolve(careerClusterFavoritesList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getFavoriteCareerCluster(userId).success(function(data) {

					// success now store in session object
					careerClusterFavoritesList = data;
					deferred.resolve(careerClusterFavoritesList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(careerClusterFavoritesList);
		}
		return deferred.promise;
	}

	userFactory.deleteCareerClusterFavorite = function(id) {
		var deferred = $q.defer();
		FavoriteRestFactory.deleteFavoriteCareerCluster(id).success(function(data) {

			// sync with in memory property
			var length = careerClusterFavoritesList.length;
			for (var i = 0; i < length; i++) {
				if (careerClusterFavoritesList[i].id == id) {
					careerClusterFavoritesList.splice(i, 1);
					break;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(careerClusterFavoritesList));
				careerClusterFavoritesList = angular.fromJson($window.sessionStorage.getItem('careerClusterFavoritesList'));
			}

			deferred.resolve(careerClusterFavoritesList);

		}).error(function(data, status, headers, config) {

			console.log('Error' + status);
			deferred.reject("Error: request returned status" + status);
		});

		return deferred.promise;

	}

	/**********************************************************
	 * School favorite implementation.
	 **********************************************************/
	var schoolFavoritesList = undefined;



	/**
	 * Add school to favorites
	 */
	userFactory.insertFavoriteSchool = function(unitId, title) {
		var favoriteObject = {
			userId : $rootScope.globals.currentUser.userId,
			unitId : unitId
		};

		var deferred = $q.defer();

		FavoriteRestFactory.insertFavoriteSchool(favoriteObject).success(function(data) {

			// add and set title property
			data.schoolName = title;

			// push newly added occupation to property
			schoolFavoritesList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(schoolFavoritesList));

				// sync with in memory property
				schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	}

	/**
	 * Return school favorite list.
	 */
	userFactory.getSchoolFavoritesList = function() {

		var userId = $rootScope.globals.currentUser.userId;
		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (schoolFavoritesList == undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("schoolFavoritesList") === null || $window.sessionStorage.getItem("schoolFavoritesList") == 'undefined') {

					// recover data using database
					FavoriteRestFactory.getFavoriteSchool(userId).success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(data));

						// sync with in memory property
						schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));

						deferred.resolve(schoolFavoritesList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));
					deferred.resolve(schoolFavoritesList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getFavoriteSchool(userId).success(function(data) {

					// success now store in session object
					schoolFavoritesList = data;
					deferred.resolve(schoolFavoritesList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(schoolFavoritesList);
		}

		return deferred.promise;
	}

	userFactory.deleteSchoolFavorite = function(id) {
		var deferred = $q.defer();
		FavoriteRestFactory.deleteFavoriteSchool(id).success(function(data) {

			// sync with in memory property
			var length = schoolFavoritesList.length;
			for (var i = 0; i < length; i++) {
				if (schoolFavoritesList[i].id == id) {
					schoolFavoritesList.splice(i, 1);
					break;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(schoolFavoritesList));
				schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));
			}

			deferred.resolve(schoolFavoritesList);

		}).error(function(data, status, headers, config) {

			console.log('Error' + status);
			deferred.reject("Error: request returned status" + status);
		});

		return deferred.promise;

	}


	userFactory.clearSessionStorage = function() {
		$window.sessionStorage.removeItem("interestCodeOne");
		$window.sessionStorage.removeItem("interestCodeTwo");
		$window.sessionStorage.removeItem("interestCodeThree");
		//$window.sessionStorage.removeItem("mediaCenterList");
		$window.sessionStorage.removeItem("noteList");
		$window.sessionStorage.removeItem("schoolNoteList");
		$window.sessionStorage.removeItem("numberTestTaken");
		$window.sessionStorage.removeItem("favoriteList");
		$window.sessionStorage.removeItem("careerClusterFavoritesList");
		$window.sessionStorage.removeItem("schoolFavoritesList");
		$window.sessionStorage.removeItem("isPortfolioStarted");
		$window.sessionStorage.removeItem("combinedTiedScoreObject");
		$window.sessionStorage.removeItem("genderTiedScoreObject");
		$window.sessionStorage.removeItem("topCombinedInterestCodes");
		$window.sessionStorage.removeItem("topGenderInterestCodes");
		$window.sessionStorage.removeItem("sV");
		$window.sessionStorage.removeItem("sM");
		$window.sessionStorage.removeItem("sS");
		$window.sessionStorage.removeItem("sA");
	}

	return userFactory;

} ]);
cepApp.service('videoModalService', [ '$uibModal', '$sce', '$location', function($uibModal, $sce, $location) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/page-partials/video-modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass : 'asvab-modal'
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				$scope.videoURL = $sce.trustAsResourceUrl($location.protocol() + "://" + $location.host() + ":" + $location.port() + "/videos/" + customModalOptions.videoName);
				//$scope.videoURL = $sce.trustAsResourceUrl("http://cep-compta.humrro.org:8080/videos/" + customModalOptions.videoName);
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}
		var modal = $uibModal.open(tempModalDefaults);

		modal.rendered.then(function() {
			// process your logic for DOM element
			var video = document.getElementById('video');
			video.play();
			video.onplaying=function(){$('#show').hide();};
			video.onwaiting=function(){$('#show').show();};
			
			
			/*$('#video').on('loadstart', function(event) {
				$('#show').show();
			});
			$('#video').on('waiting', function(event) {
				$('#show').show();
			});
			$('#video').on('stalled', function(event) {
				$('#show').show();
			});
			$('#video').on('seeking', function(event) {
				$('#show').hide();
			});

			$('#video').on('playing', function(event) {
				$('#show').hide();
			});
			$('#video').on('canplaythrough', function(event) {
				document.getElementById('video').play();
			});*/
			return modal.result;
		});

	};

} ]);

cepApp.directive('myAudio', function() {
	return {
		restrict : 'E',
		link : function(scope, element, attr) {
			var player = element.children('.video')[0];
			element.children('.play').on('click', function() {
				console.log("here");
				player.play();
			});
			element.children('.pause').on('click', function() {
				player.pause();
			});
		}

	};
});
cepApp.service('youTubeModalService', [ '$uibModal', '$sce', '$location', function($uibModal, $sce, $location) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/page-partials/youtube-modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass : 'asvab-modal'
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				//$scope.videoURL = $sce.trustAsResourceUrl($location.protocol() + "://" + $location.host() + ":" + $location.port() + "/videos/" + customModalOptions.videoName);
				//$scope.videoURL = $sce.trustAsResourceUrl("http://cep-compta.humrro.org:8080/videos/" + customModalOptions.videoName);
				$scope.videoURL = $sce.trustAsResourceUrl(customModalOptions.videoName + '&autoplay=1');
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}
		var modal = $uibModal.open(tempModalDefaults);

		modal.rendered.then(function() {
			// process your logic for DOM element
//			var video = document.getElementById('video');
//			video.play();
//			video.onplaying=function(){$('#show').hide();};
//			video.onwaiting=function(){$('#show').show();};
			
			return modal.result;
		});

	};

} ]);

cepApp.directive('myAudio', function() {
	return {
		restrict : 'E',
		link : function(scope, element, attr) {
			var player = element.children('.video')[0];
			element.children('.play').on('click', function() {
				console.log("here");
				player.play();
			});
			element.children('.pause').on('click', function() {
				player.pause();
			});
		}

	};
});