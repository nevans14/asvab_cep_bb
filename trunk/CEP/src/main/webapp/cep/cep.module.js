// create the module and name it cepApp
var cepApp = angular.module('cepApp', [ 'ngRoute', 'ngCookies', 'googlechart', 'ui.bootstrap', 'mm.foundation', 'ngSanitize', '720kb.socialshare','ipCookie', 'vcRecaptcha', 'chart.js', 'updateMeta', 'slick', 'textAngular'])
.filter('isAccPresent', function($sce) {
	  return function(input) {
		  var returnValue = '';
		  var accList;
		  if (typeof input === "undefined" || !input || input.length == 0 ) {
			   return '';
		  }
			
		  if ( typeof input === "string" )  {
			  accList = input.split(",");
		  } else {
			  accList = new Array();
			  accList.push(input);
		  }

			for (var i = 0; i < accList[0].length; i++) {
				if (accList[0][i].Name.toLowerCase() == 'ncca') {  returnValue = returnValue + '<img src="images/icons/ncca-accredited.png" height="16" width="16" >' }
				if (accList[0][i].Name.toLowerCase() == 'ansi') {  returnValue = returnValue + '<img src="images/icons/ansi-accredited.png" height="16" width="16" >' }
				if (accList[0][i].Name.toLowerCase() == 'job corps') {  returnValue = returnValue + '<img src="images/icons/job-corps.png" height="16" width="16"  >' }
				if (accList[0][i].Name.toLowerCase() == 'nam') {  returnValue = returnValue + '<img src="images/icons/third-party-industry-endorsed-cert.png" height="16" width="16" >' }
				if (accList[0][i].Name.toLowerCase() == 'military') {  returnValue = returnValue + '<img src="images/icons/military-occ-specialties.png" height="16" width="16" >' }
				if ( accList[0][i].Name.toLowerCase() == 'in-demand') {  returnValue = returnValue + '<img src="images/icons/in-demand-3.png" height="16" width="16" >' }
			}

	    return  $sce.trustAsHtml(returnValue);
	  };
	});
var url = location.href.toLowerCase();
var ssoUrlValue, ctmUrlValue, baseUrlValue;
var ssoUrl = {
	'dev'	: 'https://legacy-dev.careersinthemilitary.com/',
	'stg'	: 'https://legacy-stg.careersinthemilitary.com/',
	'prod'	: 'https://api.careersinthemilitary.com/',
	'local'	: 'http://localhost:8080/CITM/'
};
var ctmUrl = {
	'dev'	: 'https://dev-www.careersinthemilitary.com/',
	'stg'	: 'https://stg-www.careersinthemilitary.com/',
	'prod'	: 'https://www.careersinthemilitary.com/',
	'local'	: 'http://localhost:8080/CITM/'
};
var awsApiUrl = {
	'dev': 'https://88e82niqyf.execute-api.us-east-1.amazonaws.com',
	'stg': 'https://stg-aws-api.asvabprogram.com:8443',
	'prod': 'https://89sifzhed3.execute-api.us-east-1.amazonaws.com'
}
var awsMediaUrl = {
	'dev': 'https://dev-media.asvabprogram.com/',
	'stg': 'https://stg-media.asvabprogram.com/',
	'prod': 'https://prod-media.asvabprogram.com/'
}
var baseUrl = {
	'dev'	:	'https://legacy-dev.asvabprogram.com/',
	'stg'	: 'https://legacy-stg.asvabprogram.com/',
	'prod'	: 'https://www.asvabprogram.com/',
	'local'	: 'http://localhost:8080/CEP/'
}
if (url.indexOf('legacy-dev') > -1) {
	ssoUrlValue = ssoUrl.dev;
	ctmUrlValue = ctmUrl.dev;
	baseUrlValue = baseUrl.dev;
} else if (url.indexOf('legacy-stg') > -1) {
	ssoUrlValue = ssoUrl.stg;
	ctmUrlValue = ctmUrl.stg;
	baseUrlValue = baseUrl.stg;
} else if (url.indexOf('asvabprogram.com') > -1) {
	ssoUrlValue = ssoUrl.prod;
	ctmUrlValue = ctmUrl.prod;
	baseUrlValue = baseUrl.prod;
} else {
	ssoUrlValue = ssoUrl.local;
	ctmUrlValue = ctmUrl.local;
	baseUrlValue = baseUrl.local;
}
cepApp.constant('appUrl', '/CEP/rest/');
cepApp.constant('resource', '/');
cepApp.constant('base', '/CEP');
cepApp.constant('recaptchaKey', '6LcGnigTAAAAAJUUd9llQh4I5r_-j6mlJoAryekN');
cepApp.constant('ssoUrl', ssoUrlValue);
cepApp.constant('ctmUrl', ctmUrlValue);
cepApp.constant('baseUrl', baseUrlValue);
cepApp.constant('durationToTimeoutReminder', 15*60*1000); // 15 minutes
cepApp.constant('isIE', navigator.userAgent.indexOf("MSIE ") > -1 || navigator.userAgent.indexOf("Trident/") > -1);

if (ssoUrlValue.indexOf('legacy-dev.careersinthemilitary.com') !== -1
	|| ssoUrlValue.indexOf('localhost') !== -1) {
	cepApp.constant('awsApiFullUrl', awsApiUrl.dev + '/Dev');
} else if (ssoUrlValue.indexOf('legacy-stg.careersinthemilitary.com') !== -1) {
	cepApp.constant('awsApiFullUrl', awsApiUrl.stg);
} else {
	cepApp.constant('awsApiFullUrl', awsApiUrl.prod + '/Prod');
}

if (ssoUrlValue.indexOf('api.careersinthemilitary.com') >= 0) {
	cepApp.constant('awsMediaUrl', awsMediaUrl.prod);
} else if (ssoUrlValue.indexOf('legacy-stg.careersinthemilitary.com') >= 0) {
	cepApp.constant('awsMediaUrl', awsMediaUrl.stg);
} else {
	cepApp.constant('awsMediaUrl', awsMediaUrl.dev);
}

cepApp.provider('$exceptionHandler', {
    $get: function( errorLogService ) {
        return( errorLogService );
    }
});

(function(app) {
	app.run(function ($window, $rootScope, $q, $http, ssoUrl) {
		var justDomain = ssoUrl.substring(ssoUrl.indexOf('//') + 2);
		var baseUrl = $("base").attr("href");
		$(document).on("click", "a", function (e) {
			var url = $(this).attr('href');
			if (url != undefined &&	(url.indexOf(justDomain) > -1)) {				
				e.stopPropagation();
				e.preventDefault();
				// if the user is on mobile, then redirect without a new tab
				if (window.outerWidth < 500) {
					location.href = url;
					return;
				}
				// set current link and citm web link to storage
				$window.sessionStorage.setItem('previousPage', location.href);
				$window.sessionStorage.setItem('citmWebLink', url);
				// temporarily send user to the CITM website in the new tab until we get the iframe deny working
				window.open(url, "_blank");
				// redirect to viewer
				//location.href = baseUrl + "citm";
			}
		});
	});

	var routeLoadingIndicator = function($rootScope) {
		return {
			restrict : 'E',
			template : "<div class='loading-indicator' ng-if='isRouteLoading'><h1><i class='fa fa-cog fa-spin fa-2x' style='color: #2f2a6a;;'></i></h1></div>",
			link : function(scope, elem, attrs) {
				scope.isRouteLoading = false;

				$rootScope.$on('$routeChangeStart', function() {
					scope.isRouteLoading = true;
				});

				$rootScope.$on('$routeChangeSuccess', function() {
					scope.isRouteLoading = false;
				});
			}
		};
	};
	routeLoadingIndicator.$inject = [ '$rootScope' ];

	app.directive('routeLoadingIndicator', routeLoadingIndicator);

}(angular.module('cepApp')));
