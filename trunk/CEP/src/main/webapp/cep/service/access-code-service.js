cepApp.factory('AccessCodeService', function ($q, $http, appUrl) {
	var service = {};
	
	service.getAccessCode = function () {
		var defer = $q.defer();
		$http.get(appUrl + 'user/getAccessCode').success(function (res) {
			var data = res;
			defer.resolve(data);
		}).error(function (data, status, headers, config) {
			defer.reject(data);
		});
		return defer.promise;
	};
	
	return service;
});