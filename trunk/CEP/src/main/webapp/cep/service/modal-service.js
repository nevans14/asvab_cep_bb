cepApp.service('modalService', [ '$uibModal', '$sce', '$timeout', '$interval', function($uibModal, $sce, $timeout, $interval) {

	var modalDefaults = {
		animation : true,
		/*
		 * templateUrl: 'cep/page-partials/modal.html',
		 */templateUrl : 'cep/page-partials/modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass : 'asvab-modal'
	};
	
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};		
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};	
	
	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$uibModalStack', function($scope, $uibModalInstance, $uibModalStack) {
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.isDisabled = false;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(results) {
					if (customModalOptions.close != undefined) {
						customModalOptions.close();
					}
					$uibModalInstance.close('cancel');
				}
				if ($scope.modalOptions.disabledSeconds) {
					var intervalPromise;

					$scope.modalOptions.isDisabled = true;
					$timeout(function() {
						$scope.modalOptions.isDisabled = false;
						if ($scope.modalOptions.selfClose) {
							$uibModalStack.dismissAll();
						}
					}, $scope.modalOptions.disabledSeconds * 1000)

					intervalPromise = $interval(function() {
						$scope.modalOptions.disabledSeconds--;
						if ($scope.modalOptions.disabledSeconds <= 0) {
							$interval.cancel(intervalPromise);
						}
					}, 1000)
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);