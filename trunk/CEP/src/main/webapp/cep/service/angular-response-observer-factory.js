cepApp.factory('responseObserver', function responseObserver($q, $location, $rootScope, $cookieStore, $injector, $timeout, ipCookie) {
	return {
		'responseError' : function(errorResponse) {

			var loggedIn = $rootScope.globals.currentUser;

			switch (errorResponse.status) {
			case 403:

				if (loggedIn) {
					var authenticationService = $injector.get('AuthenticationService');
					var modal = $injector.get('SessionModalService');
					var userFactory = $injector.get('UserFactory');
					authenticationService.ClearCredentials();
					userFactory.clearSessionStorage();
					$location.path('/');
					// Alert user session was lost.
					// var modalOptions = {
					// 	headerText : 'Error',
					// 	bodyText : 'Session Ended! Please log back in to continue what you were doing.'
					// };
					// modal.showModal({
					// 	size : 'sm'
					// }, modalOptions);
				}
				return false;
			}

			return $q.reject(errorResponse);
		},
		'response': function(response) {

			var loggedIn = $rootScope.globals.currentUser;

			if (!$location.path().includes('faq') &&
				!$location.path().includes('countdown')
				&& loggedIn
				&& !response.config.url.includes('ping')
				&& (response.config.url.includes('amazonaws')
				|| response.config.url.includes('CEP/rest'))) {
				var userService = $injector.get('UserService');
				userService.getKeepAlivePing().then(function(results) {
					if (results.status == 200) {
						if (!$rootScope.existingTimer) {
							$rootScope.sessionKeepAlive();
						} else {
							$timeout.cancel($rootScope.existingTimer);
							$rootScope.existingTimer = undefined;
							$rootScope.sessionKeepAlive();
						}	
					}
				})
			}
			return response;
		}
	}
});