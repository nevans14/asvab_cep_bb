cepApp.factory('UserFactory', [ '$q', '$window', '$rootScope', 'FYIRestFactory', 'FavoriteRestFactory', 'UserService', function($q, $window, $rootScope, FYIRestFactory, FavoriteRestFactory, UserService) {

	var userFactory = {};

	// user's interest codes
	var interestCode = {
		interestCodeOne : undefined,
		interestCodeTwo : undefined,
		interestCodeThree : undefined,
		scoreChoice : undefined
	};

	var numberTestTaken = undefined;

	userFactory.getNumberOfTestTaken = function() {
		var deferred = $q.defer();

		if (typeof (Storage) !== "undefined") {

			if ($window.sessionStorage.getItem("numberTestTaken") === null || $window.sessionStorage.getItem("numberTestTaken") === 'undefined') {
				FYIRestFactory.getNumberTestTaken().success(function(data) {
					$window.sessionStorage.setItem('numberTestTaken', data);
					numberTestTaken = data;
					deferred.resolve(numberTestTaken);
				}).error(function(data, status, headers, config) {
					deferred.reject("Error: request returned status " + status);
				});
			} else {
				numberTestTaken = $window.sessionStorage.getItem('numberTestTaken');
				deferred.resolve(numberTestTaken);
				return deferred.promise;

			}
		} else {
			// for browser that don't support session storage
			FYIRestFactory.getNumberTestTaken().success(function(data) {
				numberTestTaken = data;
				deferred.resolve(numberTestTaken);
			}).error(function(data, status, headers, config) {
				deferred.reject("Error: request returned status " + status);
			});
		}
		return deferred.promise;
	};

	/*
	 * Gets user's interest codes and stores in session storage if browser
	 * supports.
	 */
	userFactory.getUserInterestCodes = function() {
		// Please don't remove
		var deferred = $q.defer();
		if (typeof (Storage) !== "undefined") {
			// utilize session storage
			if ($window.sessionStorage.getItem("interestCodeOne") === null || $window.sessionStorage.getItem("interestCodeOne") === 'undefined') {
				FYIRestFactory.getUserInteresteCodes().success(function(results) {
					if (results !== undefined && results[0] !== undefined) {
						$window.sessionStorage.setItem('interestCodeOne', results[0].interestCodeOne);
						$window.sessionStorage.setItem('interestCodeTwo', results[0].interestCodeTwo);
						$window.sessionStorage.setItem('interestCodeThree', results[0].interestCodeThree);
						$window.sessionStorage.setItem('scoreChoice', results[0].scoreChoice);
						interestCode = {
							interestCodeOne : $window.sessionStorage.getItem('interestCodeOne'),
							interestCodeTwo : $window.sessionStorage.getItem('interestCodeTwo'),
							interestCodeThree : $window.sessionStorage.getItem('interestCodeThree'),
							scoreChoice : $window.sessionStorage.getItem('scoreChoice')
						};
					}
					deferred.resolve(interestCode);
				}).error(function(data, status, headers, config) {
					deferred.reject("Error: request returned status " + status);
				});
			} else {
				interestCode = {
					interestCodeOne : $window.sessionStorage.getItem('interestCodeOne'),
					interestCodeTwo : $window.sessionStorage.getItem('interestCodeTwo'),
					interestCodeThree : $window.sessionStorage.getItem('interestCodeThree'),
					scoreChoice : $window.sessionStorage.getItem('scoreChoice')
				};
				deferred.resolve(interestCode);
			}
		} else {
			// for browser that don't support session storage
			FYIRestFactory.getUserInteresteCodes().success(function(data) {
				if (data !== undefined && data[0] !== undefined) {
					interestCode = {
						interestCodeOne : data[0].interestCodeOne,
						interestCodeTwo : data[0].interestCodeTwo,
						interestCodeThree : data[0].interestCodeThree,
						scoreChoice : data[0].scoreChoice
					};
				}
				deferred.resolve(interestCode);
			}).error(function(data, status, headers, config) {
				deferred.reject("Error: request returned status " + status);
			});
		}
		return deferred.promise;
	};

	/**
	 * Favorites Implementation
	 */
	var favoritesList = undefined;

	/**
	 * Add occupation to favorites
	 */
	userFactory.insertFavoriteOccupation = function(socId, title) {
		var favoriteObject = {
			onetSocCd : socId
		};

		var deferred = $q.defer();

		FavoriteRestFactory.insertFavoriteOccupation(favoriteObject).success(function(data) {

			// add and set title property
			data.title = title;

			// push newly added occupation to property
			favoritesList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('favoriteList', JSON.stringify(favoritesList));

				// sync with in memory property
				favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	};

	/**
	 * Return favorite list.
	 */
	userFactory.getFavoritesList = function() {

		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (favoritesList === undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("favoriteList") === null || $window.sessionStorage.getItem("favoriteList") === 'undefined') {

					// recover data using database
					FavoriteRestFactory.getFavoriteOccupation().success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('favoriteList', JSON.stringify(data));

						// sync with in memory property
						favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));

						deferred.resolve(favoritesList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));
					deferred.resolve(favoritesList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getFavoriteOccupation().success(function(data) {

					// success now store in session object
					favoritesList = data;
					deferred.resolve(favoritesList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(favoritesList);
		}

		return deferred.promise;
	};

	userFactory.deleteFavorite = function(id) {
		var deferred = $q.defer();
		FavoriteRestFactory.deleteFavoriteOccupation(id).success(function(data) {

			// sync with in memory property
			var length = favoritesList.length;
			for (var i = 0; i < length; i++) {
				if (favoritesList[i].id === id) {
					favoritesList.splice(i, 1);
					break;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('favoriteList', JSON.stringify(favoritesList));
				favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));
			}

			deferred.resolve(favoritesList);

		}).error(function(data, status, headers, config) {

			console.log('Error' + status);
			deferred.reject("Error: request returned status" + status);
		});

		return deferred.promise;

	};
	
	userFactory.setFavorites = function (favorites) {
		if (typeof(Storage) !== "undefined") {
			$window.sessionStorage.setItem('favoriteList', JSON.stringify(favorites));
			favoritesList = angular.fromJson($window.sessionStorage.getItem('favoriteList'));
		}
	};

	/**
	 * Career Cluster Favorites Implementation
	 */
	var careerClusterFavoritesList = undefined;

	/**
	 * Add occupation to favorites
	 */
	userFactory.insertFavoriteCareerCluster = function(ccId, title) {
		var favoriteObject = {
			ccId : ccId
		};

		var deferred = $q.defer();

		FavoriteRestFactory.insertFavoriteCareerCluster(favoriteObject).success(function(data) {

			// add and set title property
			data.title = title;

			// push newly added occupation to property
			careerClusterFavoritesList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(careerClusterFavoritesList));

				// sync with in memory property
				careerClusterFavoritesList = angular.fromJson($window.sessionStorage.getItem('careerClusterFavoritesList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	};

	/**
	 * Return career cluster favorite list.
	 */
	userFactory.getCareerClusterFavoritesList = function() {

		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (careerClusterFavoritesList === undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("careerClusterFavoritesList") === null || $window.sessionStorage.getItem("careerClusterFavoritesList") === 'undefined') {

					// recover data using database
					FavoriteRestFactory.getFavoriteCareerCluster().success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(data));
						// sync with in memory property
						careerClusterFavoritesList = angular.fromJson($window.sessionStorage.getItem('careerClusterFavoritesList'));
						deferred.resolve(careerClusterFavoritesList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					careerClusterFavoritesList = angular.fromJson($window.sessionStorage.getItem('careerClusterFavoritesList'));
					deferred.resolve(careerClusterFavoritesList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getFavoriteCareerCluster().success(function(data) {

					// success now store in session object
					careerClusterFavoritesList = data;
					deferred.resolve(careerClusterFavoritesList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(careerClusterFavoritesList);
		}
		return deferred.promise;
	};

	userFactory.deleteCareerClusterFavorite = function(id) {
		var deferred = $q.defer();
		FavoriteRestFactory.deleteFavoriteCareerCluster(id).success(function(data) {

			// sync with in memory property
			var length = careerClusterFavoritesList.length;
			for (var i = 0; i < length; i++) {
				if (careerClusterFavoritesList[i].id === id) {
					careerClusterFavoritesList.splice(i, 1);
					break;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('careerClusterFavoritesList', JSON.stringify(careerClusterFavoritesList));
				careerClusterFavoritesList = angular.fromJson($window.sessionStorage.getItem('careerClusterFavoritesList'));
			}

			deferred.resolve(careerClusterFavoritesList);

		}).error(function(data, status, headers, config) {

			console.log('Error' + status);
			deferred.reject("Error: request returned status" + status);
		});

		return deferred.promise;

	};

	/**********************************************************
	 * School favorite implementation.
	 **********************************************************/
	var schoolFavoritesList = undefined;



	/**
	 * Add school to favorites
	 */
	userFactory.insertFavoriteSchool = function(unitId, title) {
		var favoriteObject = {
			unitId : unitId
		};

		var deferred = $q.defer();

		FavoriteRestFactory.insertFavoriteSchool(favoriteObject).success(function(data) {

			// add and set title property
			data.schoolName = title;

			// push newly added occupation to property
			schoolFavoritesList.push(data);

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(schoolFavoritesList));

				// sync with in memory property
				schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));
			}

			deferred.resolve("Insert successful.");
		}).error(function(data, status, headers, config) {
			console.log(status);
			deferred.reject("Error: request returned status " + status);
		});

		return deferred.promise;
	};

	/**
	 * Return school favorite list.
	 */
	userFactory.getSchoolFavoritesList = function() {

		var deferred = $q.defer();

		// if favoriteList is undefined then recover data from database
		if (schoolFavoritesList === undefined) {

			if (typeof (Storage) !== "undefined") {

				if ($window.sessionStorage.getItem("schoolFavoritesList") === null || $window.sessionStorage.getItem("schoolFavoritesList") === 'undefined') {

					// recover data using database
					FavoriteRestFactory.getFavoriteSchool().success(function(data) {

						// success now store in session object
						$window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(data));

						// sync with in memory property
						schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));

						deferred.resolve(schoolFavoritesList);

					}).error(function(data, status, headers, config) {

						console.log('Error' + status);
						deferred.reject("Error: request returned status" + status);
					});
				} else {

					// recover data using session storage
					schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));
					deferred.resolve(schoolFavoritesList);
				}

			} else {

				// no session storage support so get recover data using database
				FavoriteRestFactory.getFavoriteSchool().success(function(data) {

					// success now store in session object
					schoolFavoritesList = data;
					deferred.resolve(schoolFavoritesList);

				}).error(function(data, status, headers, config) {

					console.log('Error' + status);
					deferred.reject("Error: request returned status" + status);
				});
			}

		} else {

			// data is available so dont need to use session storage or database
			// to recover
			deferred.resolve(schoolFavoritesList);
		}

		return deferred.promise;
	};

	userFactory.deleteSchoolFavorite = function(id) {
		var deferred = $q.defer();
		FavoriteRestFactory.deleteFavoriteSchool(id).success(function(data) {

			// sync with in memory property
			var length = schoolFavoritesList.length;
			for (var i = 0; i < length; i++) {
				if (schoolFavoritesList[i].id === id) {
					schoolFavoritesList.splice(i, 1);
					break;
				}
			}

			// sync with session storage
			if (typeof (Storage) !== "undefined") {
				$window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(schoolFavoritesList));
				schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));
			}

			deferred.resolve(schoolFavoritesList);

		}).error(function(data, status, headers, config) {

			console.log('Error' + status);
			deferred.reject("Error: request returned status" + status);
		});

		return deferred.promise;

	};
	
	userFactory.setSchoolFavorites = function (favorites) {
		if (typeof(Storage) !== "undefined") {
			$window.sessionStorage.setItem('schoolFavoritesList', JSON.stringify(favorites));
			schoolFavoritesList = angular.fromJson($window.sessionStorage.getItem('schoolFavoritesList'));
		}
	};

	userFactory.getUsername = function () {
		var deferred = $q.defer();
		UserService.getUsername().success(function (results) {
			deferred.resolve(results.username);
		}).error(function (error) {
			deferred.reject(error);
		});
		return deferred.promise;
	};

	userFactory.getIsUsingUnregisterableAccesscode = function() {
		var deferred = $q.defer();
		UserService.getIsUsingUnregisterableAccesscode().success(function (results) {
			deferred.resolve(results);
		}).error(function (error) {
			deferred.reject(error);
		});
		return deferred.promise;
	}

	userFactory.clearSessionStorage = function() {
		$window.sessionStorage.removeItem("interestCodeOne");
		$window.sessionStorage.removeItem("interestCodeTwo");
		$window.sessionStorage.removeItem("interestCodeThree");
		$window.sessionStorage.removeItem("mediaCenterList");
		$window.sessionStorage.removeItem("noteList");
		$window.sessionStorage.removeItem("schoolNoteList");
		$window.sessionStorage.removeItem("careerClusterNoteList");
		$window.sessionStorage.removeItem("numberTestTaken");
		$window.sessionStorage.removeItem("favoriteList");
		$window.sessionStorage.removeItem("careerClusterFavoritesList");
		$window.sessionStorage.removeItem("schoolFavoritesList");
		$window.sessionStorage.removeItem("isPortfolioStarted");
		$window.sessionStorage.removeItem("combinedTiedScoreObject");
		$window.sessionStorage.removeItem("genderTiedScoreObject");
		$window.sessionStorage.removeItem("topCombinedInterestCodes");
		$window.sessionStorage.removeItem("topGenderInterestCodes");
		$window.sessionStorage.removeItem("option-ready-photos");
		$window.sessionStorage.removeItem("sV");
		$window.sessionStorage.removeItem("sM");
		$window.sessionStorage.removeItem("sS");
		$window.sessionStorage.removeItem("sA");
		$window.sessionStorage.removeItem("FYIQuestions");
		$window.sessionStorage.removeItem("gender");
		$window.sessionStorage.removeItem("scoreSummary");
		$window.sessionStorage.removeItem("asvabScore");
		$window.sessionStorage.removeItem("mBooks");
		$window.sessionStorage.removeItem("sBooks");
		$window.sessionStorage.removeItem("manualScores");
		$window.sessionStorage.removeItem("vBooks");
		$window.sessionStorage.removeItem("scoreChoice");
		$window.sessionStorage.removeItem("workValueFinalResults");
		$window.sessionStorage.removeItem("workValueActiveQuestion");
		$window.sessionStorage.removeItem("workValueTopResults");
		$window.sessionStorage.removeItem("workValueUserSelections");
	};

	return userFactory;

} ]);