cepApp.service('BringToSchoolModalService', [ '$uibModal', 'FaqRestFactory', 'modalService', 'vcRecaptchaService', 'recaptchaKey', '$location', '$window', '$sce', 'isIE', '$timeout', '$anchorScroll', function($uibModal, FaqRestFactory, modalService, vcRecaptchaService, recaptchaKey, $location, $window, $sce, isIE, $timeout, $anchorScroll) {

	var modalDefaults = {
		animation : true,
		templateUrl : '/CEP/cep/page-partials/bring-asvab-to-your-school.html',
		size : 'lg',
		windowClass : 'new-modal top-margin-lg'
	};

	var modalOptions = {
		headerText : ''
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', 'DbInfoService', function($scope, $uibModalInstance, DbInfoService) {
				
				// Track event in Google Analytics.
				var path = $location.path();
				$window.ga('create', 'UA-83809749-1', 'auto');
				$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_VIEW' );
				
				$scope.responseOne = '';
				$scope.responseTwo = '';
				$scope.recaptchaKey = recaptchaKey;
				$scope.modalOptions = tempModalOptions;
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
				$scope.isIE = isIE;

				$scope.counselorEmail;
				$scope.studentHeardUs;
				$scope.studentHeardUsOther;
				$scope.shareWithCounselor = function() {

					$scope.formDisabled = true;
					$scope.errorSharing = undefined;
					var email = $scope.counselorEmail;
					var studentHeardUs = $scope.studentHeardUs;
					var studentHeardUsOther = $scope.studentHeardUsOther;

					if ($scope.responseOne === "") { // if string
						// is empty
						$scope.errorSharing = "Please resolve the captcha and submit!";
						$scope.formDisabled = false;
						return false;
					} else {
						var emailObject = {
							email: email,
							studentHeardUs: studentHeardUs,
							studentHeardUsOther : studentHeardUsOther,
							recaptcha: $scope.responseOne
						}
						FaqRestFactory.shareWithCounselor(emailObject).then(function(success) {
							$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_STUDENT_SHARE' );
							
							$uibModalInstance.dismiss('cancel');
							var modalOptions = {
								headerText : 'Schedule Notification',
								bodyText : 'Your information was submitted successfully.'
							};

							modalService.showModal({
								size : 'sm'
							}, modalOptions);

							$scope.counselorEmail = undefined;
							$scope.studentHeardUs = undefined;
							$scope.studentHeardUsOther = undefined;
							$scope.formDisabled = false;

						}, function(error) {
							var error = error.data.errorMessage;
							if (error == undefined) {
								$scope.errorSharing = 'There was an error proccessing your request. Please try again.';
								vcRecaptchaService.reload();
							} else {
								$scope.errorSharing = error;
							}
							$scope.formDisabled = false;
						});
					}

				}

				if (customModalOptions !== undefined && customModalOptions.showSchedule !== undefined) {
					$scope.showSchedule = customModalOptions.showSchedule;
				} else {
					$scope.showSchedule = false;
				}
				
				$scope.isDisplayBanner = false;
				$scope.maintenanceBannerText = '';
				DbInfoService.getByName('cep_bring_modal_banner').then(function (res) {
					$scope.isDisplayBanner = res.data.value2 ? "true" : "false";
					$scope.maintenanceBannerText = $scope.isDisplayBanner ? $sce.trustAsHtml(res.data.value2) : '';
				});	

				$scope.trackScheduleClick = function() {
					$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_SCHEDULE' );
				}

				// form properties
				$scope.title;
				$scope.position;
				$scope.firstName;
				$scope.lastName;
				$scope.phone;
				$scope.phoneExt;
				$scope.email;
				$scope.heardUs;
				$scope.heardUsOther;
				$scope.schoolName;
				$scope.schoolAddress;
				$scope.schoolAddress2;
				$scope.schoolCity;
				$scope.schoolCounty;
				$scope.schoolState;
				$scope.schoolZip;
				$scope.widgetId;
				$scope.studentNum10thGrade = 0;
				$scope.studentNum11thGrade = 0;
				$scope.studentNum12thGrade = 0;
				$scope.studentNumOther = 0;

				$scope.dateOpen = [false, false, false];
				$scope.testFirstChoiceDate;
				$scope.testSecondChoiceDate;
				$scope.testThirdChoiceDate;
				$scope.testFirstChoiceStartTime;
				$scope.testSecondChoiceStartTime;
				$scope.testThirdChoiceStartTime;
				$scope.testStudentNum10thGrade = 0;
				$scope.testStudentNum11thGrade = 0;
				$scope.testStudentNum12thGrade = 0;
				$scope.testStudentNumOther = 0;
				$scope.testInterestInIcat;
				$scope.testNumOfAccommodatedStudents = 0;
				$scope.testScoreReleaseOption;
				$scope.testLocation;
				$scope.testRemarks;
				$scope.testNeedLapboards;
				$scope.testNeedLapboardsNumber;

				$scope.testIndex = 0;
				$scope.testReservations = [];
				$scope.ptiIndex = 0;
				$scope.ptiReservations = [];
				$scope.submitType = 0;  //0: prev, 1: add test 2: remove test

				$scope.progressBarsIndex = 0;
				$scope.progressBarsTotal = 2;

				$scope.ptiFirstChoiceDate;
				$scope.ptiSecondChoiceDate;
				$scope.ptiThirdChoiceDate;
				$scope.ptiFirstChoiceStartTime;
				$scope.ptiSecondChoiceStartTime;
				$scope.ptiThirdChoiceStartTime;
				$scope.ptiStudentNum10thGrade = 0;
				$scope.ptiStudentNum11thGrade = 0;
				$scope.ptiStudentNum12thGrade = 0;
				$scope.ptiStudentNumOther = 0;
				$scope.ptiLocation;
				$scope.ptiRemarks;
				$scope.ptiProjectorAvailable;
				$scope.ptiInternetAvailable;
				$scope.ptiComputerOrPhone;
				$scope.ptiNeedLapboards;
				$scope.ptiNeedLapboardsNumber;

				$scope.activityFirstChoiceDate;
				$scope.activitySecondChoiceDate;
				$scope.activityThirdChoiceDate;
				$scope.activityFirstChoiceStartTime;
				$scope.activitySecondChoiceStartTime;
				$scope.activityThirdChoiceStartTime;
				$scope.activityAttendees = 0;
				$scope.activityLocation;
				$scope.activityRemarks;

				$scope.parentFirstChoiceDate;
				$scope.parentSecondChoiceDate;
				$scope.parentThirdChoiceDate;
				$scope.parentFirstChoiceStartTime;
				$scope.parentSecondChoiceStartTime;
				$scope.parentThirdChoiceStartTime;
				$scope.parentAttendees = 0;
				$scope.parentLocation;
				$scope.parentRemarks;

				$scope.notifyEmailAddresses;
				$scope.isProcessing;

				$scope.onWidgetCreate = function(_widgetId) {
					$scope.widgetId = _widgetId;
				};
				$scope.asvabActivities = {
					asvabTest : false,
					pti : false,
					classroomActivities: false,
					presentation : false
				}
				$scope.asvabActivityFormats = {
					pti : '',
					classroomActivities : '',
					presentation : ''
				}

				$scope.asvabActivityFormatSelection = {
					ptiVirtual : false,
					ptiInPerson : false,
					classroomActivitiesVirtual : false,
					classroomActivitiesInPerson : false,
					presentationVirtual : false,
					presentationInPerson : false,
				}

				$scope.openDatePicker = function(index) {
					$scope.dateOpen[index] = true;
				};

				$scope.setFormDisabled = function() {
					$scope.formDisabled =
						$scope.showSchedule && (
							!$scope.asvabActivities.asvabTest && !$scope.asvabActivities.pti && !$scope.asvabActivities.classroomActivities && !$scope.asvabActivities.presentation
							|| ($scope.asvabActivities.pti && !$scope.asvabActivityFormatSelection.ptiVirtual && !$scope.asvabActivityFormatSelection.ptiInPerson)
							|| ($scope.asvabActivities.classroomActivities && !$scope.asvabActivityFormatSelection.classroomActivitiesVirtual && !$scope.asvabActivityFormatSelection.classroomActivitiesInPerson)
							|| ($scope.asvabActivities.presentation && !$scope.asvabActivityFormatSelection.presentationVirtual && !$scope.asvabActivityFormatSelection.presentationInPerson)
						)
				}

				$scope.showPTISummary = function() {
					return $scope.showSummary &&
						$scope.ptiReservations.some(function(i) {
							return Object.values(i).some(function(j) {
								return j !== undefined && j !== null && j !== 0
							})
						});
				}

				$scope.booleanToText = function(val) {
					return val ? 'Yes' : (val === undefined || val === null) ? null : 'No';
				}

				// Test methods
				$scope.addTest = function(index) {

					index = typeof index !== 'undefined' ? index : undefined;

					var testObj = {
						testFirstChoiceDate: $scope.testFirstChoiceDate,
						testFirstChoiceStartTime: $scope.testFirstChoiceStartTime,
						testSecondChoiceDate: $scope.testSecondChoiceDate,
						testSecondChoiceStartTime: $scope.testSecondChoiceStartTime,
						testThirdChoiceDate: $scope.testThirdChoiceDate,
						testThirdChoiceStartTime: $scope.testThirdChoiceStartTime,
						testStudentNum10thGrade: $scope.testStudentNum10thGrade,
						testStudentNum11thGrade: $scope.testStudentNum11thGrade,
						testStudentNum12thGrade: $scope.testStudentNum12thGrade,
						testStudentNumOther: $scope.testStudentNumOther,
						testInterestInIcat: $scope.testInterestInIcat,
						testNumOfAccommodatedStudents: $scope.testNumOfAccommodatedStudents,
						testScoreReleaseOption: $scope.testScoreReleaseOption,
						testLocation: $scope.testLocation,
						testRemarks: $scope.testRemarks,
						testNeedLapboards: $scope.testNeedLapboards,
						testNeedLapboardsNumber: $scope.testNeedLapboardsNumber,
					}

					if (!isNaN(index)) {
						$scope.testReservations.splice(index, 0, testObj)
						return;
					}

					$scope.testReservations.push(testObj)
				}

				$scope.setTest = function() {
					$scope.testFirstChoiceDate = $scope.testReservations[$scope.testIndex].testFirstChoiceDate;
					$scope.testFirstChoiceStartTime = $scope.testReservations[$scope.testIndex].testFirstChoiceStartTime;
					$scope.testSecondChoiceDate = $scope.testReservations[$scope.testIndex].testSecondChoiceDate;
					$scope.testSecondChoiceStartTime = $scope.testReservations[$scope.testIndex].testSecondChoiceStartTime;
					$scope.testThirdChoiceDate = $scope.testReservations[$scope.testIndex].testThirdChoiceDate;
					$scope.testThirdChoiceStartTime = $scope.testReservations[$scope.testIndex].testThirdChoiceStartTime;
					$scope.testStudentNum10thGrade = $scope.testReservations[$scope.testIndex].testStudentNum10thGrade;
					$scope.testStudentNum11thGrade = $scope.testReservations[$scope.testIndex].testStudentNum11thGrade;
					$scope.testStudentNum12thGrade = $scope.testReservations[$scope.testIndex].testStudentNum12thGrade;
					$scope.testStudentNumOther = $scope.testReservations[$scope.testIndex].testStudentNumOther;
					$scope.testInterestInIcat = $scope.testReservations[$scope.testIndex].testInterestInIcat;
					$scope.testNumOfAccommodatedStudents = $scope.testReservations[$scope.testIndex].testNumOfAccommodatedStudents;
					$scope.testScoreReleaseOption = $scope.testReservations[$scope.testIndex].testScoreReleaseOption;
					$scope.testLocation = $scope.testReservations[$scope.testIndex].testLocation;
					$scope.testRemarks = $scope.testReservations[$scope.testIndex].testRemarks;
					$scope.testNeedLapboards = $scope.testReservations[$scope.testIndex].testNeedLapboards;
					$scope.testNeedLapboardsNumber = $scope.testReservations[$scope.testIndex].testNeedLapboardsNumber;
				}

				$scope.updateSavedTest = function() {
					$scope.testReservations[$scope.testIndex].testFirstChoiceDate = $scope.testFirstChoiceDate;
					$scope.testReservations[$scope.testIndex].testFirstChoiceStartTime = $scope.testFirstChoiceStartTime;
					$scope.testReservations[$scope.testIndex].testSecondChoiceDate = $scope.testSecondChoiceDate;
					$scope.testReservations[$scope.testIndex].testSecondChoiceStartTime = $scope.testSecondChoiceStartTime;
					$scope.testReservations[$scope.testIndex].testThirdChoiceDate = $scope.testThirdChoiceDate;
					$scope.testReservations[$scope.testIndex].testThirdChoiceStartTime = $scope.testThirdChoiceStartTime;
					$scope.testReservations[$scope.testIndex].testStudentNum10thGrade = $scope.testStudentNum10thGrade;
					$scope.testReservations[$scope.testIndex].testStudentNum11thGrade = $scope.testStudentNum11thGrade;
					$scope.testReservations[$scope.testIndex].testStudentNum12thGrade = $scope.testStudentNum12thGrade;
					$scope.testReservations[$scope.testIndex].testStudentNumOther = $scope.testStudentNumOther;
					$scope.testReservations[$scope.testIndex].testInterestInIcat = $scope.testInterestInIcat;
					$scope.testReservations[$scope.testIndex].testNumOfAccommodatedStudents = $scope.testNumOfAccommodatedStudents;
					$scope.testReservations[$scope.testIndex].testScoreReleaseOption = $scope.testScoreReleaseOption;
					$scope.testReservations[$scope.testIndex].testLocation = $scope.testLocation;
					$scope.testReservations[$scope.testIndex].testRemarks = $scope.testRemarks;
					$scope.testReservations[$scope.testIndex].testNeedLapboards = $scope.testNeedLapboards;
					$scope.testReservations[$scope.testIndex].testNeedLapboardsNumber = $scope.testNeedLapboardsNumber;
				}

				$scope.clearTest = function() {
						$scope.testFirstChoiceDate = undefined;
						$scope.testFirstChoiceStartTime = undefined;
						$scope.testSecondChoiceDate = undefined;
						$scope.testSecondChoiceStartTime = undefined;
						$scope.testThirdChoiceDate = undefined;
						$scope.testThirdChoiceStartTime = undefined;
						$scope.testStudentNum10thGrade = 0;
						$scope.testStudentNum11thGrade = 0;
						$scope.testStudentNum12thGrade = 0;
						$scope.testStudentNumOther = 0;
						$scope.testInterestInIcat = undefined;
						$scope.testNumOfAccommodatedStudents = 0;
						$scope.testScoreReleaseOption = undefined;
						$scope.testLocation = undefined;
						$scope.testRemarks = undefined;
						$scope.testNeedLapboards = undefined;
						$scope.testNeedLapboardsNumber = undefined;
				}

				$scope.removeTest = function(index) {

					index = typeof index !== 'undefined' ? index : undefined;

					if (!isNaN(index)) {
						$scope.testReservations.splice(index, 1);
					} else {
						$scope.testReservations.pop();
					}
				}

				// PTI methods
				$scope.addPti = function(index) {

					index = typeof index !== 'undefined' ? index : undefined;

					var testObj = {
						ptiFirstChoiceDate: $scope.ptiFirstChoiceDate,
						ptiSecondChoiceDate: $scope.ptiSecondChoiceDate,
						ptiThirdChoiceDate: $scope.ptiThirdChoiceDate,
						ptiFirstChoiceStartTime: $scope.ptiFirstChoiceStartTime,
						ptiSecondChoiceStartTime: $scope.ptiSecondChoiceStartTime,
						ptiThirdChoiceStartTime: $scope.ptiThirdChoiceStartTime,
						ptiNeedLapboards: $scope.ptiNeedLapboards,
						ptiNeedLapboardsNumber: $scope.ptiNeedLapboardsNumber,
						ptiStudentNum10thGrade: $scope.ptiStudentNum10thGrade,
						ptiStudentNum11thGrade: $scope.ptiStudentNum11thGrade,
						ptiStudentNum12thGrade: $scope.ptiStudentNum12thGrade,
						ptiStudentNumOther: $scope.ptiStudentNumOther,
						ptiLocation: $scope.ptiLocation,
						ptiRemarks: $scope.ptiRemarks,
						ptiProjectorAvailable: $scope.ptiProjectorAvailable,
						ptiInternetAvailable: $scope.ptiInternetAvailable,
						ptiComputerOrPhone: $scope.ptiComputerOrPhone,
					}

					if (!isNaN(index)) {
						$scope.ptiReservations.splice(index, 0, testObj)
						return;
					}

					$scope.ptiReservations.push(testObj)
				}

				$scope.setPti = function() {
					$scope.ptiFirstChoiceDate = $scope.ptiReservations[$scope.ptiIndex].ptiFirstChoiceDate;
					$scope.ptiFirstChoiceStartTime = $scope.ptiReservations[$scope.ptiIndex].ptiFirstChoiceStartTime;
					$scope.ptiSecondChoiceDate = $scope.ptiReservations[$scope.ptiIndex].ptiSecondChoiceDate;
					$scope.ptiSecondChoiceStartTime = $scope.ptiReservations[$scope.ptiIndex].ptiSecondChoiceStartTime;
					$scope.ptiThirdChoiceDate = $scope.ptiReservations[$scope.ptiIndex].ptiThirdChoiceDate;
					$scope.ptiThirdChoiceStartTime = $scope.ptiReservations[$scope.ptiIndex].ptiThirdChoiceStartTime;
					$scope.ptiStudentNum10thGrade = $scope.ptiReservations[$scope.ptiIndex].ptiStudentNum10thGrade;
					$scope.ptiStudentNum11thGrade = $scope.ptiReservations[$scope.ptiIndex].ptiStudentNum11thGrade;
					$scope.ptiStudentNum12thGrade = $scope.ptiReservations[$scope.ptiIndex].ptiStudentNum12thGrade;
					$scope.ptiStudentNumOther = $scope.ptiReservations[$scope.ptiIndex].ptiStudentNumOther;
					$scope.ptiLocation = $scope.ptiReservations[$scope.ptiIndex].ptiLocation;
					$scope.ptiRemarks = $scope.ptiReservations[$scope.ptiIndex].ptiRemarks;
					$scope.ptiNeedLapboards = $scope.ptiReservations[$scope.ptiIndex].ptiNeedLapboards;
					$scope.ptiNeedLapboardsNumber = $scope.ptiReservations[$scope.ptiIndex].ptiNeedLapboardsNumber;
					$scope.ptiProjectorAvailable = $scope.ptiReservations[$scope.ptiIndex].ptiProjectorAvailable;
					$scope.ptiInternetAvailable = $scope.ptiReservations[$scope.ptiIndex].ptiInternetAvailable;
					$scope.ptiComputerOrPhone = $scope.ptiReservations[$scope.ptiIndex].ptiComputerOrPhone;
				}

				$scope.updateSavedPti = function() {
					$scope.ptiReservations[$scope.ptiIndex].ptiFirstChoiceDate = $scope.ptiFirstChoiceDate;
					$scope.ptiReservations[$scope.ptiIndex].ptiFirstChoiceStartTime = $scope.ptiFirstChoiceStartTime;
					$scope.ptiReservations[$scope.ptiIndex].ptiSecondChoiceDate = $scope.ptiSecondChoiceDate;
					$scope.ptiReservations[$scope.ptiIndex].ptiSecondChoiceStartTime = $scope.ptiSecondChoiceStartTime;
					$scope.ptiReservations[$scope.ptiIndex].ptiThirdChoiceDate = $scope.ptiThirdChoiceDate;
					$scope.ptiReservations[$scope.ptiIndex].ptiThirdChoiceStartTime = $scope.ptiThirdChoiceStartTime;
					$scope.ptiReservations[$scope.ptiIndex].ptiStudentNum10thGrade = $scope.ptiStudentNum10thGrade;
					$scope.ptiReservations[$scope.ptiIndex].ptiStudentNum11thGrade = $scope.ptiStudentNum11thGrade;
					$scope.ptiReservations[$scope.ptiIndex].ptiStudentNum12thGrade = $scope.ptiStudentNum12thGrade;
					$scope.ptiReservations[$scope.ptiIndex].ptiStudentNumOther = $scope.ptiStudentNumOther;
					$scope.ptiReservations[$scope.ptiIndex].ptiLocation = $scope.ptiLocation;
					$scope.ptiReservations[$scope.ptiIndex].ptiRemarks = $scope.ptiRemarks;
					$scope.ptiReservations[$scope.ptiIndex].ptiNeedLapboards = $scope.ptiNeedLapboards;
					$scope.ptiReservations[$scope.ptiIndex].ptiNeedLapboardsNumber = $scope.ptiNeedLapboardsNumber;
					$scope.ptiReservations[$scope.ptiIndex].ptiProjectorAvailable = $scope.ptiProjectorAvailable;
					$scope.ptiReservations[$scope.ptiIndex].ptiInternetAvailable = $scope.ptiInternetAvailable;
					$scope.ptiReservations[$scope.ptiIndex].ptiComputerOrPhone = $scope.ptiComputerOrPhone;
				}

				$scope.clearPti = function() {
					$scope.ptiFirstChoiceDate = undefined;
					$scope.ptiSecondChoiceDate = undefined;
					$scope.ptiThirdChoiceDate = undefined;
					$scope.ptiFirstChoiceStartTime = undefined;
					$scope.ptiSecondChoiceStartTime = undefined;
					$scope.ptiThirdChoiceStartTime = undefined;
					$scope.ptiNeedLapboards = undefined;
					$scope.ptiNeedLapboardsNumber = undefined;
					$scope.ptiStudentNum10thGrade = 0;
					$scope.ptiStudentNum11thGrade = 0;
					$scope.ptiStudentNum12thGrade = 0;
					$scope.ptiStudentNumOther = 0;
					$scope.ptiLocation = undefined;
					$scope.ptiRemarks = undefined;
					$scope.ptiProjectorAvailable = undefined;
					$scope.ptiInternetAvailable = undefined;
					$scope.ptiComputerOrPhone = undefined;
				}

				$scope.removePti = function(index) {

					index = typeof index !== 'undefined' ? index : undefined;

					if (!isNaN(index)) {
						$scope.ptiReservations.splice(index, 1);
					} else {
						$scope.ptiReservations.pop();
					}
				}

				$scope.pageSelector = function() {
					if ($scope.submitType === 0) {
						$scope.pageToShow(false);
					} else if ($scope.submitType === 2) {
						$scope.pageToShow(false, {remove: true})
					} else if ($scope.submitType === 1) {
						$scope.pageToShow(false, {add: true})
					} else if ($scope.submitType === 3) {
						$scope.pageToShow();
					} else {
						return;
					}
				}

				$scope.setDeliveryFormat = function() {
					$scope.asvabActivityFormats.pti = $scope.asvabActivityFormatSelection.ptiVirtual ? 'Virtual' : '';
					if (!$scope.asvabActivityFormats.pti) {
						$scope.asvabActivityFormats.pti = $scope.asvabActivityFormatSelection.ptiInPerson ? 'In-person' : '';
					} else if ($scope.asvabActivityFormatSelection.ptiInPerson) {
						$scope.asvabActivityFormats.pti = $scope.asvabActivityFormats.pti.concat(', ' + ($scope.asvabActivityFormatSelection.ptiInPerson ? 'In-person' : ''));
					}

					$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormatSelection.classroomActivitiesVirtual ? 'Virtual' : '';
					if (!$scope.asvabActivityFormats.classroomActivities) {
						$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormatSelection.classroomActivitiesInPerson ? 'In-person' : '';
					} else if ($scope.asvabActivityFormatSelection.classroomActivitiesInPerson) {
						$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormats.classroomActivities.concat(', ' + ($scope.asvabActivityFormatSelection.classroomActivitiesInPerson ? 'In-person' : ''));
					}

					$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormatSelection.presentationVirtual ? 'Virtual' : '';
					if (!$scope.asvabActivityFormats.presentation) {
						$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormatSelection.presentationInPerson ? 'In-person' : '';
					} else if ($scope.asvabActivityFormatSelection.presentationInPerson) {
						$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormats.presentation.concat(', ' + ($scope.asvabActivityFormatSelection.presentationInPerson ? 'In-person' : ''));
					}
				}

				$scope.pageToShow = function(next, reservations) {

					next = typeof next !== 'undefined' ? next : true;
					reservations = typeof reservations !== 'undefined' ? reservations : {};

					$location.hash('top');
					$anchorScroll();

					if (reservations.add && $scope.showTest) {
						if ($scope.testIndex === $scope.testReservations.length) {
							$scope.addTest();
							$scope.testIndex++;
							$scope.clearTest();	
						} else {
							$scope.updateSavedTest();
							$scope.addTest($scope.testIndex);
							$scope.testIndex++;
							$scope.clearTest();
						}
						
						$scope.progressBarsIndex++;
						$scope.progressBarsTotal++;
						return;
					} else if (reservations.remove && $scope.showTest) {
						$scope.removeTest($scope.testIndex);
						$scope.testIndex--;
						$scope.setTest();

						$scope.progressBarsIndex--;
						$scope.progressBarsTotal--;
						return;
					}

					if (reservations.add && ($scope.showPTI1 || $scope.showPTI2)) {
						if ($scope.ptiIndex === $scope.ptiReservations.length) {
							$scope.addPti();
							$scope.ptiIndex++;
							$scope.clearPti();	
						} else {
							$scope.updateSavedPti();
							$scope.addPti($scope.ptiIndex);
							$scope.ptiIndex++;
							$scope.clearPti();
						}
						
						$scope.progressBarsIndex++;
						$scope.progressBarsTotal++;
						return;
					} else if (reservations.remove && ($scope.showPTI1 || $scope.showPTI2)) {
						$scope.removePti($scope.ptiIndex);
						$scope.ptiIndex--;
						$scope.setPti();

						$scope.progressBarsIndex--;
						$scope.progressBarsTotal--;
						return;
					}


					if ($scope.showSchool
						&& next
					) {
						$scope.showSchool = false;
						if ($scope.asvabActivities.asvabTest) {
							$scope.showTest = true;
						} else if ($scope.asvabActivities.pti) {
							$scope.showPTI2 = true;
						} else if ($scope.asvabActivities.classroomActivities) {
							$scope.showActivities = true;
						} else if ($scope.asvabActivities.presentation) {
							$scope.showParents = true;
						}
						$scope.progressBarsIndex++;
						return;
					}

					if ($scope.showTest
						&& !next
					) {
						if ($scope.testIndex === 0) {
							$scope.showTest = false;
							$scope.showSchool = true;
						} else if ($scope.testIndex === $scope.testReservations.length) {
							$scope.addTest();
							$scope.testIndex--;
							$scope.setTest();
						} else {
							$scope.updateSavedTest();
							$scope.testIndex--;
							$scope.setTest();
						}
						$scope.progressBarsIndex--;
						return;
					}

					if ($scope.showTest
						&& next
					) {
						if ($scope.testIndex > $scope.testReservations.length - 1) {
							$scope.addTest();
							$scope.showTest = false;
							$scope.showPTI1 = true;
						} else if ($scope.testIndex + 1 <= $scope.testReservations.length - 1) {
							$scope.updateSavedTest();
							$scope.testIndex++;
							$scope.setTest();
						} else {
							$scope.updateSavedTest();
							$scope.showTest = false;
							$scope.showPTI1 = true;
						}
						$scope.progressBarsIndex++;
						return;
					}

					if ($scope.showPTI1
						&& next
					) {

						if ($scope.ptiIndex > $scope.ptiReservations.length - 1) {
								$scope.addPti();
								$scope.showPTI1 = false;
								if ($scope.asvabActivities.classroomActivities) {
									$scope.showActivities = true;
								} else if ($scope.asvabActivities.presentation) {
									$scope.showParents = true;
								} else {
									$scope.showSummary = true;
								}
							} else if ($scope.ptiIndex + 1 <= $scope.ptiReservations.length - 1) {
								$scope.updateSavedPti();
								$scope.ptiIndex++;
								$scope.setPti();
							} else {
								$scope.updateSavedPti();
								$scope.showPTI1 = false;
								if ($scope.asvabActivities.classroomActivities) {
									$scope.showActivities = true;
								} else if ($scope.asvabActivities.presentation) {
									$scope.showParents = true;
								} else {
									$scope.showSummary = true;
								}
							}

							$scope.progressBarsIndex++;

							return;
					}

					if ($scope.showPTI1
						&& !next
					) {
						if ($scope.ptiIndex === 0) {
							$scope.showPTI1 = false;
							$scope.showTest = true;
							$scope.setTest();
						} else if ($scope.ptiIndex === $scope.ptiReservations.length) {
							$scope.addPti();
							$scope.ptiIndex--;
							$scope.setPti();
						} else {
							$scope.updateSavedPti();
							$scope.ptiIndex--;
							$scope.setPti();
						}

						$scope.progressBarsIndex--;
						return;
					}


					if ($scope.showPTI2
						&& next
					) {

						if ($scope.ptiIndex > $scope.ptiReservations.length - 1) {
								$scope.addPti();
								$scope.showPTI2 = false;
								if ($scope.asvabActivities.classroomActivities) {
									$scope.showActivities = true;
								} else if ($scope.asvabActivities.presentation) {
									$scope.showParents = true;
								} else {
									$scope.showSummary = true;
								}
							} else if ($scope.ptiIndex + 1 <= $scope.ptiReservations.length - 1) {
								$scope.updateSavedPti();
								$scope.ptiIndex++;
								$scope.setPti();
							} else {
								$scope.updateSavedPti();
								$scope.showPTI2 = false;
								if ($scope.asvabActivities.classroomActivities) {
									$scope.showActivities = true;
								} else if ($scope.asvabActivities.presentation) {
									$scope.showParents = true;
								} else {
									$scope.showSummary = true;
								}
							}

							$scope.progressBarsIndex++;
							return;
					}

					if ($scope.showPTI2
						&& !next
					) {
						if ($scope.ptiIndex === 0) {
							$scope.showPTI2 = false;
							$scope.showSchool = true;
						} else if ($scope.ptiIndex === $scope.ptiReservations.length) {
							$scope.addPti();
							$scope.ptiIndex--;
							$scope.setPti();
						} else {
							$scope.updateSavedPti();
							$scope.ptiIndex--;
							$scope.setPti();
						}
						$scope.progressBarsIndex--;
						return;
					}

					if ($scope.showActivities
						&& !next
					) {
						$scope.showActivities = false;
						if ($scope.asvabActivities.asvabTest) {
							$scope.showPTI1 = true;
						} else if ($scope.asvabActivities.pti) {
							$scope.showPTI2 = true;
						} else {
							$scope.showSchool = true;
						}
						$scope.progressBarsIndex--;
						return;
					}

					if ($scope.showActivities
						&& next
					) {
						$scope.showActivities = false;
						if ($scope.asvabActivities.presentation) {
							$scope.showParents = true;
						} else {
							$scope.showSummary = true;
						}
						$scope.progressBarsIndex++;
						return;
					}

					if ($scope.showParents
						&& !next
					) {
						$scope.showParents = false;
						if ($scope.asvabActivities.classroomActivities) {
							$scope.showActivities = true;
						} else if ($scope.asvabActivities.asvabTest) {
							$scope.showPTI1 = true;
						} else if ($scope.asvabActivities.pti) {
							$scope.showPTI2 = true;
						} else {
							$scope.showSchool = true;
						}
						$scope.progressBarsIndex--;
						return
					}

					if ($scope.showParents
						&& next
					) {
						$scope.showParents = false;
						$scope.showSummary = true;
						$scope.progressBarsIndex++;
						return;
					}

					if ($scope.showSummary
						&& !next
					) {
						$scope.showSummary = false;
						if ($scope.asvabActivities.presentation) {
							$scope.showParents = true;
						} else if ($scope.asvabActivities.classroomActivities) {
							$scope.showActivities = true;
						} else if ($scope.asvabActivities.asvabTest) {
							$scope.showPTI1 = true;
						} else if ($scope.asvabActivities.pti) {
							$scope.showPTI2 = true;
						} else {
							$scope.showSchool = true;
						}
						$scope.progressBarsIndex--;
						return
					}
				}

				$scope.progressBarsToShowInital = function() {
					if ($scope.asvabActivities.asvabTest) {
						$scope.progressBarsTotal = $scope.progressBarsTotal + 2;
					}

					if (!$scope.asvabActivities.asvabTest && $scope.asvabActivities.pti) {
						$scope.progressBarsTotal = $scope.progressBarsTotal + 1;
					}

					if ($scope.asvabActivities.classroomActivities) {
						$scope.progressBarsTotal = $scope.progressBarsTotal + 1;
					}

					if ($scope.asvabActivities.presentation) {
						$scope.progressBarsTotal = $scope.progressBarsTotal + 1;
					}
				}

				$scope.focusMissingField = function() {
					if($scope.isIE) {
						if($scope.showTest && !$scope.testFirstChoiceStartTime) {
							$timeout(function() {
								document.getElementById('testFirstChoiceStartTime').focus();
							}, 500);
						}

						if ($scope.showPTI1 && $scope.asvabActivities.pti && !$scope.ptiFirstChoiceStartTime) {
							$timeout(function() {
								document.getElementById('pti1FirstChoiceStartTime').focus();
							}, 500);
						}

						if ($scope.showPTI2 && $scope.asvabActivities.pti && !$scope.ptiFirstChoiceStartTime) {
							$timeout(function() {
								document.getElementById('pti2FirstChoiceStartTime').focus();
							}, 500);
						}

						if ($scope.showActivities && !$scope.activityFirstChoiceStartTime) {
							$timeout(function() {
								document.getElementById('activityFirstChoiceStartTime').focus();
							}, 500);
						}

						if ($scope.showParents && !$scope.parentFirstChoiceStartTime){
								$timeout(function() {
									document.getElementById('parentFirstChoiceStartTime').focus();
								}, 500);
						}
					}
				}

				$scope.notShow = false;
				$scope.showSchedule = false;  //NOTE: toggle to true to test schedule directly
				$scope.showSchool = false;
				// $scope.showTest = true;
				$scope.showTest = false;
				$scope.showPTI1 = false;
				// $scope.showPTI2 = true;
				$scope.showPTI2 = false;
				$scope.showActivities = false;
				$scope.showParents = false;
				$scope.showSummary = false;

				$scope.setFormDisabled();

				$scope.scheduleSubmit = function() {
					$scope.captchaMessage = undefined;
					$scope.formDisabled = true;

						$scope.asvabActivityFormats.pti = $scope.asvabActivityFormatSelection.ptiVirtual ? 'virtual' : '';
						if (!$scope.asvabActivityFormats.pti) {
							$scope.asvabActivityFormats.pti = $scope.asvabActivityFormatSelection.ptiInPerson ? 'in-person' : '';
						} else if ($scope.asvabActivityFormatSelection.ptiInPerson) {
							$scope.asvabActivityFormats.pti = $scope.asvabActivityFormats.pti.concat(',' + ($scope.asvabActivityFormatSelection.ptiInPerson ? 'in-person' : ''));
						}

						$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormatSelection.classroomActivitiesVirtual ? 'virtual' : '';
						if (!$scope.asvabActivityFormats.classroomActivities) {
							$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormatSelection.classroomActivitiesInPerson ? 'in-person' : '';
						} else if ($scope.asvabActivityFormatSelection.classroomActivitiesInPerson) {
							$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormats.classroomActivities.concat(',' + ($scope.asvabActivityFormatSelection.classroomActivitiesInPerson ? 'in-person' : ''));
						}

						$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormatSelection.presentationVirtual ? 'virtual' : '';
						if (!$scope.asvabActivityFormats.presentation) {
							$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormatSelection.presentationInPerson ? 'in-person' : '';
						} else if ($scope.asvabActivityFormatSelection.presentationInPerson) {
							$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormats.presentation.concat(',' + ($scope.asvabActivityFormatSelection.presentationInPerson ? 'in-person' : ''));
						}

						if (
							($scope.asvabActivities.pti && $scope.asvabActivityFormats.pti === '')
							|| ($scope.asvabActivities.classroomActivities && $scope.asvabActivityFormats.classroomActivities === '')
							|| ($scope.asvabActivities.presentation && $scope.asvabActivityFormats.presentation === '')) { 
							$scope.captchaMessage = "Please select activity format!";
							$scope.formDisabled = false;
							return false;
						} else if ($scope.responseTwo === "") {
							$scope.captchaMessage = "Please resolve the captcha and submit!";
							$scope.formDisabled = false;
							return false;
						} else {
							$scope.error = undefined;

						var emailObject = {
							title : $scope.title,
							position : $scope.position,
							firstName : $scope.firstName,
							lastName : $scope.lastName,
							phone : $scope.phone,
							email : $scope.email,
							heardUs: $scope.heardUs,
							heardUsOther: $scope.heardUsOther,
							schoolName : $scope.schoolName,
							schoolAddress : $scope.schoolAddress,
							schoolCity : $scope.schoolCity,
							schoolCounty : $scope.schoolCounty,
							schoolState : $scope.schoolState,
							schoolZip : $scope.schoolZip,
							recaptcha : $scope.responseTwo,
							asvabActivities : $scope.asvabActivities,
							asvabActivityFormats : $scope.asvabActivityFormats
						}

						FaqRestFactory.scheduleEmail(emailObject).then(function(success) {
							
							$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_SCHEDULE_SUBMIT' );
							
							$uibModalInstance.dismiss('cancel');
							var modalOptions = {
								headerText : 'Schedule Notification',
								bodyText : 'Your information was submitted successfully.'
							};

							modalService.showModal({
								size : 'sm'
							}, modalOptions);

							$scope.title = undefined;
							$scope.position = undefined;
							$scope.firstName = undefined;
							$scope.lastName = undefined;
							$scope.phone = undefined;
							$scope.email = undefined;
							$scope.heardUs = undefined;
							$scope.heardUsOther = undefined;
							$scope.schoolName = undefined;
							$scope.schoolAddress = undefined;
							$scope.schoolCity = undefined;
							$scope.schoolCounty = undefined;
							$scope.schoolState = undefined;
							$scope.schoolZip = undefined;
							$scope.formDisabled = false;

						}, function(data) {
							var message = '';
							for (var key in emailObject) {
								if (data.data[key]) {
									message += '\n' + data.data[key][0];
								}
							}
							// if status is 400 (bad request), show validation message, else show generic error message
							if (data.status === 400 && message !== '') {
								$scope.error = message;
							} else {
								$scope.error = 'There was an error processing your request. Please try again.';
								vcRecaptchaService.reload($scope.widgetId);
							}
							$scope.formDisabled = false;
						});
					}
				}

				$scope.scheduleSubmitSummary = function() {
					$scope.captchaMessage = undefined;
					$scope.formDisabled = true;

						$scope.asvabActivityFormats.pti = $scope.asvabActivityFormatSelection.ptiVirtual ? 'virtual' : '';
						if (!$scope.asvabActivityFormats.pti) {
							$scope.asvabActivityFormats.pti = $scope.asvabActivityFormatSelection.ptiInPerson ? 'in-person' : '';
						} else if ($scope.asvabActivityFormatSelection.ptiInPerson) {
							$scope.asvabActivityFormats.pti = $scope.asvabActivityFormats.pti.concat(',' + ($scope.asvabActivityFormatSelection.ptiInPerson ? 'in-person' : ''));
						}

						$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormatSelection.classroomActivitiesVirtual ? 'virtual' : '';
						if (!$scope.asvabActivityFormats.classroomActivities) {
							$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormatSelection.classroomActivitiesInPerson ? 'in-person' : '';
						} else if ($scope.asvabActivityFormatSelection.classroomActivitiesInPerson) {
							$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormats.classroomActivities.concat(',' + ($scope.asvabActivityFormatSelection.classroomActivitiesInPerson ? 'in-person' : ''));
						}

						$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormatSelection.presentationVirtual ? 'virtual' : '';
						if (!$scope.asvabActivityFormats.presentation) {
							$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormatSelection.presentationInPerson ? 'in-person' : '';
						} else if ($scope.asvabActivityFormatSelection.presentationInPerson) {
							$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormats.presentation.concat(',' + ($scope.asvabActivityFormatSelection.presentationInPerson ? 'in-person' : ''));
						}

						if (
							($scope.asvabActivities.pti && $scope.asvabActivityFormats.pti === '')
							|| ($scope.asvabActivities.classroomActivities && $scope.asvabActivityFormats.classroomActivities === '')
							|| ($scope.asvabActivities.presentation && $scope.asvabActivityFormats.presentation === '')) { 
							$scope.captchaMessage = "Please select activity format!";
							$scope.formDisabled = false;
							return false;
						} else if ($scope.responseTwo === "") {
							$scope.captchaMessage = "Please resolve the captcha and submit!";
							$scope.formDisabled = false;
							return false;
						} else {
							$scope.error = undefined;

						var emailObject = {
							title : $scope.title,
							position : $scope.position,
							firstName : $scope.firstName,
							lastName : $scope.lastName,
							phone : $scope.phone,
							phone : $scope.phoneExt,
							email : $scope.email,
							heardUs: $scope.heardUs,
							heardUsOther: $scope.heardUsOther,
							schoolName : $scope.schoolName,
							schoolAddress : $scope.schoolAddress,
							schoolAddress2 : $scope.schoolAddress2,
							schoolCity : $scope.schoolCity,
							schoolCounty : $scope.schoolCounty,
							schoolState : $scope.schoolState,
							schoolZip : $scope.schoolZip,
							recaptcha : $scope.responseTwo,
							asvabActivities : $scope.asvabActivities,
							asvabActivityFormats : $scope.asvabActivityFormats,

							testFirstChoiceDate: $scope.testFirstChoiceDate,
							testFirstChoiceStartTime: $scope.testFirstChoiceStartTime,
							testInterestInIcat: $scope.testInterestInIcat,
							testNumOfAccommodatedStudents: $scope.testNumOfAccommodatedStudents,
							testScoreReleaseOption: $scope.testScoreReleaseOption,
							testLocation: $scope.testLocation,
							testRemarks: $scope.testRemarks,
							testNeedLapboards: $scope.testNeedLapboards,
							testNeedLapboardsNumber: $scope.testNeedLapboardsNumber,

							ptiFirstChoiceDate: $scope.ptiFirstChoiceDate,
							ptiSecondChoiceDate: $scope.ptiSecondChoiceDate,
							ptiThirdChoiceDate: $scope.ptiThirdChoiceDate,
							ptiFirstChoiceStartTime: $scope.ptiFirstChoiceStartTime,
							ptiSecondChoiceStartTime: $scope.ptiSecondChoiceStartTime,
							ptiThirdChoiceStartTime: $scope.ptiThirdChoiceStartTime,
							ptiNeedLapboards: $scope.ptiNeedLapboards,
							ptiNeedLapboardsNumber: $scope.ptiNeedLapboardsNumber,

							ptiProjectorAvailable: $scope.ptiProjectorAvailable,
							ptiInternetAvailable: $scope.ptiInternetAvailable,
							ptiComputerOrPhone: $scope.ptiComputerOrPhone,
							ptiNeedLapboards: $scope.ptiNeedLapboards,
							ptiNeedLapboardsNumber: $scope.ptiNeedLapboardsNumber,

							parentFirstChoiceDate: $scope.parentFirstChoiceDate,
							parentSecondChoiceDate: $scope.parentSecondChoiceDate,
							parentThirdChoiceDate: $scope.parentThirdChoiceDate,
							parentFirstChoiceStartTime: $scope.parentFirstChoiceStartTime,
							parentSecondChoiceStartTime: $scope.parentSecondChoiceStartTime,
							parentThirdChoiceStartTime: $scope.parentThirdChoiceStartTime,
							parentAttendees: $scope.parentAttendees,
							parentLocation: $scope.parentLocation,
							parentRemarks: $scope.parentRemarks,
						}

						FaqRestFactory.scheduleEmail(emailObject).then(function(success) {
							
							$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_SCHEDULE_SUBMIT' );
							
							$uibModalInstance.dismiss('cancel');
							var modalOptions = {
								headerText : 'Schedule Notification',
								bodyText : 'Your information was submitted successfully.'
							};

							modalService.showModal({
								size : 'sm'
							}, modalOptions);

							$scope.title = undefined;
							$scope.position = undefined;
							$scope.firstName = undefined;
							$scope.lastName = undefined;
							$scope.phone = undefined;
							$scope.phoneExt = undefined;
							$scope.email = undefined;
							$scope.heardUs = undefined;
							$scope.heardUsOther = undefined;
							$scope.schoolName = undefined;
							$scope.schoolAddress = undefined;
							$scope.schoolAddress2 = undefined;
							$scope.schoolCity = undefined;
							$scope.schoolCounty = undefined;
							$scope.schoolState = undefined;
							$scope.schoolZip = undefined;
							$scope.formDisabled = false;

						}, function(data) {
							var message = '';
							for (var key in emailObject) {
								if (data.data[key]) {
									message += '\n' + data.data[key][0];
								}
							}
							// if status is 400 (bad request), show validation message, else show generic error message
							if (data.status === 400 && message !== '') {
								$scope.error = message;
							} else {
								$scope.error = 'There was an error processing your request. Please try again.';
								vcRecaptchaService.reload($scope.widgetId);
							}
							$scope.formDisabled = false;
						});
					}
				}

				$scope.scheduleFormSubmitSummary = function() {
					$scope.captchaMessage = undefined;
					$scope.formDisabled = true;

						$scope.asvabActivityFormats.pti = $scope.asvabActivityFormatSelection.ptiVirtual ? 'Virtual' : '';
						if (!$scope.asvabActivityFormats.pti) {
							$scope.asvabActivityFormats.pti = $scope.asvabActivityFormatSelection.ptiInPerson ? 'In-person' : '';
						} else if ($scope.asvabActivityFormatSelection.ptiInPerson) {
							$scope.asvabActivityFormats.pti = $scope.asvabActivityFormats.pti.concat(', ' + ($scope.asvabActivityFormatSelection.ptiInPerson ? 'In-person' : ''));
						}

						$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormatSelection.classroomActivitiesVirtual ? 'Virtual' : '';
						if (!$scope.asvabActivityFormats.classroomActivities) {
							$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormatSelection.classroomActivitiesInPerson ? 'In-person' : '';
						} else if ($scope.asvabActivityFormatSelection.classroomActivitiesInPerson) {
							$scope.asvabActivityFormats.classroomActivities = $scope.asvabActivityFormats.classroomActivities.concat(', ' + ($scope.asvabActivityFormatSelection.classroomActivitiesInPerson ? 'In-person' : ''));
						}

						$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormatSelection.presentationVirtual ? 'Virtual' : '';
						if (!$scope.asvabActivityFormats.presentation) {
							$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormatSelection.presentationInPerson ? 'In-person' : '';
						} else if ($scope.asvabActivityFormatSelection.presentationInPerson) {
							$scope.asvabActivityFormats.presentation = $scope.asvabActivityFormats.presentation.concat(', ' + ($scope.asvabActivityFormatSelection.presentationInPerson ? 'In-person' : ''));
						}

						if (
							($scope.asvabActivities.pti && $scope.asvabActivityFormats.pti === '')
							|| ($scope.asvabActivities.classroomActivities && $scope.asvabActivityFormats.classroomActivities === '')
							|| ($scope.asvabActivities.presentation && $scope.asvabActivityFormats.presentation === '')) { 
							$scope.captchaMessage = "Please select activity format!";
							$scope.formDisabled = false;
							return false;
						} else if ($scope.responseTwo === "") {
							$scope.captchaMessage = "Please resolve the captcha and submit!";
							$scope.formDisabled = false;
							return false;
						} else {
							$scope.error = undefined;


						// Test data
						var alternativeTimes = [];
						var asvabTestReservations = $scope.testReservations.map(function(t) {
							alternativeTimes = [];

							if (!t.testFirstChoiceDate || !t.testFirstChoiceStartTime) {
								return {}
							}

							if (t.testSecondChoiceDate && t.testSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.testSecondChoiceDate.getFullYear(), t.testSecondChoiceDate.getMonth(), t.testSecondChoiceDate.getDate(), t.testSecondChoiceStartTime.getHours(), t.testSecondChoiceStartTime.getMinutes(), t.testSecondChoiceStartTime.getSeconds())})
							} else if (t.testSecondChoiceDate && !t.testSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.testSecondChoiceDate.getFullYear(), t.testSecondChoiceDate.getMonth(), t.testSecondChoiceDate.getDate(), t.testFirstChoiceStartTime.getHours(), t.testFirstChoiceStartTime.getMinutes(), t.testFirstChoiceStartTime.getSeconds())})
							} else if (!t.testSecondChoiceDate && t.testSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.testFirstChoiceDate.getFullYear(), t.testFirstChoiceDate.getMonth(), t.testFirstChoiceDate.getDate(), t.testSecondChoiceStartTime.getHours(), t.testSecondChoiceStartTime.getMinutes(), t.testSecondChoiceStartTime.getSeconds())})
							}

							if (t.testThirdChoiceDate && t.testThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.testThirdChoiceDate.getFullYear(), t.testThirdChoiceDate.getMonth(), t.testThirdChoiceDate.getDate(), t.testThirdChoiceStartTime.getHours(), t.testThirdChoiceStartTime.getMinutes(), t.testThirdChoiceStartTime.getSeconds())})
							} else if (t.testThirdChoiceDate && !t.testThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.testThirdChoiceDate.getFullYear(), t.testThirdChoiceDate.getMonth(), t.testThirdChoiceDate.getDate(), t.testFirstChoiceStartTime.getHours(), t.testFirstChoiceStartTime.getMinutes(), t.testFirstChoiceStartTime.getSeconds())})
							} else if (!t.testThirdChoiceDate && t.testThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.testFirstChoiceDate.getFullYear(), t.testFirstChoiceDate.getMonth(), t.testFirstChoiceDate.getDate(), t.testThirdChoiceStartTime.getHours(), t.testThirdChoiceStartTime.getMinutes(), t.testThirdChoiceStartTime.getSeconds())})
							}

							return {
								testDate: new Date(t.testFirstChoiceDate.getFullYear(), t.testFirstChoiceDate.getMonth(), t.testFirstChoiceDate.getDate(), t.testFirstChoiceStartTime.getHours(), t.testFirstChoiceStartTime.getMinutes(), t.testFirstChoiceStartTime.getSeconds()),
								alternativeTimes: alternativeTimes,
								estimated10thGradeTesters: t.testStudentNum10thGrade,
								estimated11thGradeTesters: t.testStudentNum11thGrade,
								estimated12thGradeTesters: t.testStudentNum12thGrade,
								estimatedPostSecondaryTesters: t.testStudentNumOther,
								isInterestedInICat: t.testInterestInIcat,
								numberOfAccommodatedStudents: t.testNumOfAccommodatedStudents,
								location: t.testLocation,
								remarks: t.testRemarks,
								inNeedOfLapboards: t.testNeedLapboards,
								numberOfLapboads: t.testNeedLapboardsNumber,
								scoreReleaseOptionId: parseInt(t.testScoreReleaseOption),
							}
						})

						// PTI data
						var ptiReservations = $scope.ptiReservations.map(function(t) {
							alternativeTimes = [];

							var today = new Date();
							var ptiFirstDate = (t.ptiFirstChoiceDate || t.ptiFirstChoiceStartTime || t.ptiSecondChoiceDate || t.ptiSecondChoiceStartTime || t.ptiThirdChoiceDate || t.ptiThirdChoiceStartTime)
								? today
								: null;

							if (t.ptiFirstChoiceDate) {
								ptiFirstDate = new Date(t.ptiFirstChoiceDate.getFullYear(), t.ptiFirstChoiceDate.getMonth(), t.ptiFirstChoiceDate.getDate(), ptiFirstDate.getHours(), ptiFirstDate.getMinutes(), ptiFirstDate.getSeconds())
							}

							if (t.ptiFirstChoiceStartTime) {
								ptiFirstDate = new Date(ptiFirstDate.getFullYear(), ptiFirstDate.getMonth(), ptiFirstDate.getDate(), t.ptiFirstChoiceStartTime.getHours(), t.ptiFirstChoiceStartTime.getMinutes(), t.ptiFirstChoiceStartTime.getSeconds());
							}

							if (t.ptiSecondChoiceDate && t.ptiSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.ptiSecondChoiceDate.getFullYear(), t.ptiSecondChoiceDate.getMonth(), t.ptiSecondChoiceDate.getDate(), t.ptiSecondChoiceStartTime.getHours(), t.ptiSecondChoiceStartTime.getMinutes(), t.ptiSecondChoiceStartTime.getSeconds())})
							} else if (t.ptiSecondChoiceDate && !t.ptiSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.ptiSecondChoiceDate.getFullYear(), t.ptiSecondChoiceDate.getMonth(), t.ptiSecondChoiceDate.getDate(), ptiFirstDate.getHours(), ptiFirstDate.getMinutes(), ptiFirstDate.getSeconds())})
							} else if (!t.ptiSecondChoiceDate && t.ptiSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(ptiFirstDate.getFullYear(), ptiFirstDate.getMonth(), ptiFirstDate.getDate(), t.ptiSecondChoiceStartTime.getHours(), t.ptiSecondChoiceStartTime.getMinutes(), t.ptiSecondChoiceStartTime.getSeconds())})
							}

							if (t.ptiThirdChoiceDate && t.ptiThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.ptiThirdChoiceDate.getFullYear(), t.ptiThirdChoiceDate.getMonth(), t.ptiThirdChoiceDate.getDate(), t.ptiThirdChoiceStartTime.getHours(), t.ptiThirdChoiceStartTime.getMinutes(), t.ptiThirdChoiceStartTime.getSeconds())})
							} else if (t.ptiThirdChoiceDate && !t.ptiThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(t.ptiThirdChoiceDate.getFullYear(), t.ptiThirdChoiceDate.getMonth(), t.ptiThirdChoiceDate.getDate(), ptiFirstDate.getHours(), ptiFirstDate.getMinutes(), ptiFirstDate.getSeconds())})
							} else if (!t.ptiThirdChoiceDate && t.ptiThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date(ptiFirstDate.getFullYear(), ptiFirstDate.getMonth(), ptiFirstDate.getDate(), t.ptiThirdChoiceStartTime.getHours(), t.ptiThirdChoiceStartTime.getMinutes(), t.ptiThirdChoiceStartTime.getSeconds())})
							}

							return {
								testDate: ptiFirstDate,
								alternativeTimes: alternativeTimes,
								estimated10thGradeTesters: t.ptiStudentNum10thGrade,
								estimated11thGradeTesters: t.ptiStudentNum11thGrade,
								estimated12thGradeTesters: t.ptiStudentNum12thGrade,
								estimatedPostSecondaryTesters: t.ptiStudentNumOther,
								location: t.ptiLocation,
								remarks: t.ptiRemarks,
								isProjectorAvailable: t.ptiProjectorAvailable,
								isInternetAvailable: t.ptiInternetAvailable,
								willStudentHaveComputerOrPhone: t.ptiComputerOrPhone,
								inNeedOfLapboards: t.ptiNeedLapboards,
								numberOfLapboads: t.ptiNeedLapboardsNumber,
							}
						})

						// Activity data
						var classroomActivityReservation = null;
						if ($scope.asvabActivities.classroomActivities) {
							alternativeTimes = [];

							if (!$scope.activityFirstChoiceDate || !$scope.activityFirstChoiceStartTime) {
								return {}
							}

							if ($scope.activitySecondChoiceDate && $scope.activitySecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.activitySecondChoiceDate.getFullYear(), $scope.activitySecondChoiceDate.getMonth(), $scope.activitySecondChoiceDate.getDate(), $scope.activitySecondChoiceStartTime.getHours(), $scope.activitySecondChoiceStartTime.getMinutes(), $scope.activitySecondChoiceStartTime.getSeconds())})
							} else if ($scope.activitySecondChoiceDate && !$scope.activitySecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.activitySecondChoiceDate.getFullYear(), $scope.activitySecondChoiceDate.getMonth(), $scope.activitySecondChoiceDate.getDate(), $scope.activityFirstChoiceStartTime.getHours(), $scope.activityFirstChoiceStartTime.getMinutes(), $scope.activityFirstChoiceStartTime.getSeconds())})
							} else if (!$scope.activitySecondChoiceDate && $scope.activitySecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.activityFirstChoiceDate.getFullYear(), $scope.activityFirstChoiceDate.getMonth(), $scope.activityFirstChoiceDate.getDate(), $scope.activitySecondChoiceStartTime.getHours(), $scope.activitySecondChoiceStartTime.getMinutes(), $scope.activitySecondChoiceStartTime.getSeconds())})
							}

							if ($scope.activityThirdChoiceDate && $scope.activityThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.activityThirdChoiceDate.getFullYear(), $scope.activityThirdChoiceDate.getMonth(), $scope.activityThirdChoiceDate.getDate(), $scope.activityThirdChoiceStartTime.getHours(), $scope.activityThirdChoiceStartTime.getMinutes(), $scope.activityThirdChoiceStartTime.getSeconds())})
							} else if ($scope.activityThirdChoiceDate && !$scope.activityThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.activityThirdChoiceDate.getFullYear(), $scope.activityThirdChoiceDate.getMonth(), $scope.activityThirdChoiceDate.getDate(), $scope.activityFirstChoiceStartTime.getHours(), $scope.activityFirstChoiceStartTime.getMinutes(), $scope.activityFirstChoiceStartTime.getSeconds())})
							} else if (!$scope.activityThirdChoiceDate && $scope.activityThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.activityFirstChoiceDate.getFullYear(), $scope.activityFirstChoiceDate.getMonth(), $scope.activityFirstChoiceDate.getDate(), $scope.activityThirdChoiceStartTime.getHours(), $scope.activityThirdChoiceStartTime.getMinutes(), $scope.activityThirdChoiceStartTime.getSeconds())})
							}

							classroomActivityReservation = {
								testDate: new Date($scope.activityFirstChoiceDate.getFullYear(), $scope.activityFirstChoiceDate.getMonth(), $scope.activityFirstChoiceDate.getDate(), $scope.activityFirstChoiceStartTime.getHours(), $scope.activityFirstChoiceStartTime.getMinutes(), $scope.activityFirstChoiceStartTime.getSeconds()),
								alternativeTimes: alternativeTimes,
								estimatedPresentationAttendees: $scope.activityAttendees,
								location: $scope.activityLocation,
								remarks: $scope.activityRemarks,
							}
						}

						// Parents data
						var parentReservation = null;
						if ($scope.asvabActivities.presentation) {
							alternativeTimes = [];

							if (!$scope.parentFirstChoiceDate || !$scope.parentFirstChoiceStartTime) {
								return {}
							}

							if ($scope.parentSecondChoiceDate && $scope.parentSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.parentSecondChoiceDate.getFullYear(), $scope.parentSecondChoiceDate.getMonth(), $scope.parentSecondChoiceDate.getDate(), $scope.parentSecondChoiceStartTime.getHours(), $scope.parentSecondChoiceStartTime.getMinutes(), $scope.parentSecondChoiceStartTime.getSeconds())})
							} else if ($scope.parentSecondChoiceDate && !$scope.parentSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.parentSecondChoiceDate.getFullYear(), $scope.parentSecondChoiceDate.getMonth(), $scope.parentSecondChoiceDate.getDate(), $scope.parentFirstChoiceStartTime.getHours(), $scope.parentFirstChoiceStartTime.getMinutes(), $scope.parentFirstChoiceStartTime.getSeconds())})
							} else if (!$scope.parentSecondChoiceDate && $scope.parentSecondChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.parentFirstChoiceDate.getFullYear(), $scope.parentFirstChoiceDate.getMonth(), $scope.parentFirstChoiceDate.getDate(), $scope.parentSecondChoiceStartTime.getHours(), $scope.parentSecondChoiceStartTime.getMinutes(), $scope.parentSecondChoiceStartTime.getSeconds())})
							}

							if ($scope.parentThirdChoiceDate && $scope.parentThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.parentThirdChoiceDate.getFullYear(), $scope.parentThirdChoiceDate.getMonth(), $scope.parentThirdChoiceDate.getDate(), $scope.parentThirdChoiceStartTime.getHours(), $scope.parentThirdChoiceStartTime.getMinutes(), $scope.parentThirdChoiceStartTime.getSeconds())})
							} else if ($scope.parentThirdChoiceDate && !$scope.parentThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.parentThirdChoiceDate.getFullYear(), $scope.parentThirdChoiceDate.getMonth(), $scope.parentThirdChoiceDate.getDate(), $scope.parentFirstChoiceStartTime.getHours(), $scope.parentFirstChoiceStartTime.getMinutes(), $scope.parentFirstChoiceStartTime.getSeconds())})
							} else if (!$scope.parentThirdChoiceDate && $scope.parentThirdChoiceStartTime) {
								alternativeTimes.push({testDate: new Date($scope.parentFirstChoiceDate.getFullYear(), $scope.parentFirstChoiceDate.getMonth(), $scope.parentFirstChoiceDate.getDate(), $scope.parentThirdChoiceStartTime.getHours(), $scope.parentThirdChoiceStartTime.getMinutes(), $scope.parentThirdChoiceStartTime.getSeconds())})
							}

							parentReservation = {
								testDate: new Date($scope.parentFirstChoiceDate.getFullYear(), $scope.parentFirstChoiceDate.getMonth(), $scope.parentFirstChoiceDate.getDate(), $scope.parentFirstChoiceStartTime.getHours(), $scope.parentFirstChoiceStartTime.getMinutes(), $scope.parentFirstChoiceStartTime.getSeconds()),
								alternativeTimes: alternativeTimes,
								estimatedPresentationAttendees: $scope.parentAttendees,
								location: $scope.parentLocation,
								remarks: $scope.parentRemarks,
							}
						}

						var emailObject = {
							title : $scope.title,
							position : $scope.position,
							firstName : $scope.firstName,
							lastName : $scope.lastName,
							phone : $scope.phone,
							phoneExt : $scope.phoneExt,
							email : $scope.email,
							heardUs: $scope.heardUs,
							heardUsOther: $scope.heardUsOther,
							schoolName : $scope.schoolName,
							schoolAddress : $scope.schoolAddress,
							schoolAddress2 : $scope.schoolAddress2,
							schoolCity : $scope.schoolCity,
							// schoolCounty : $scope.schoolCounty,
							schoolState : $scope.schoolState,
							schoolZip : $scope.schoolZip,
							recaptcha : $scope.responseTwo,
							asvabActivities : $scope.asvabActivities,
							asvabActivityFormats : $scope.asvabActivityFormats,
							pop10thGrade: $scope.studentNum10thGrade,
							pop11thGrade: $scope.studentNum11thGrade,
							pop12thGrade: $scope.studentNum12thGrade,
							popPostSecondary: $scope.studentNumOther,
							notifyEmailAddresses: $scope.notifyEmailAddresses,

							asvabTestReservations: asvabTestReservations,
							ptiReservations: ptiReservations,
							classroomActivityReservation: classroomActivityReservation,
							parentReservation: parentReservation,
						}

						$scope.isProcessing = true;

						FaqRestFactory.scheduleEmail(emailObject).then(function(success) {
							
							$scope.isProcessing = false;

							$window.ga('send', 'pageview', path + '#BRING_ASVAB_FORM_SCHEDULE_SUBMIT' );
							
							$uibModalInstance.dismiss('cancel');
							var modalOptions = {
								headerText : 'Schedule Notification',
								bodyText : 'Your information was submitted successfully.'
							};

							modalService.showModal({
								size : 'sm'
							}, modalOptions);

							$scope.title = undefined;
							$scope.position = undefined;
							$scope.firstName = undefined;
							$scope.lastName = undefined;
							$scope.phone = undefined;
							$scope.phoneExt = undefined;
							$scope.email = undefined;
							$scope.heardUs = undefined;
							$scope.heardUsOther = undefined;
							$scope.schoolName = undefined;
							$scope.schoolAddress = undefined;
							$scope.schoolAddress2 = undefined;
							$scope.schoolCity = undefined;
							$scope.schoolCounty = undefined;
							$scope.schoolState = undefined;
							$scope.schoolZip = undefined;
							$scope.formDisabled = false;
							$scope.studentNum10thGrade = undefined,
							$scope.studentNum11thGrade = undefined,
							$scope.studentNum12thGrade = undefined,
							$scope.studentNumOther = undefined,
							$scope.notifyEmailAddresses = undefined,
							$scope.asvabTestReservations = [],
							$scope.ptiReservations = [],
							$scope.classroomActivityReservation = null,
							$scope.parentReservation = null
						}, function(data) {
							$scope.isProcessing = false;

							var message = '';
							for (var key in emailObject) {
								if (data.data[key]) {
									message += '\n' + data.data[key][0];
								}
							}
							// if status is 400 (bad request), show validation message, else show generic error message
							if (data.status === 400 && message !== '') {
								$scope.error = message;
							} else {
								$scope.error = 'There was an error processing your request. Please try again.';
								vcRecaptchaService.reload($scope.widgetId);
							}
							$scope.formDisabled = false;
						});
					}
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);