cepApp.service('youTubeModalService', [ '$uibModal', '$sce', '$location', function($uibModal, $sce, $location) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/page-partials/youtube-modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass : 'asvab-modal'
	};

	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};

	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
				$scope.modalOptions = tempModalOptions;
				//$scope.videoURL = $sce.trustAsResourceUrl($location.protocol() + "://" + $location.host() + ":" + $location.port() + "/videos/" + customModalOptions.videoName);
				//$scope.videoURL = $sce.trustAsResourceUrl("http://cep-compta.humrro.org:8080/videos/" + customModalOptions.videoName);
				$scope.videoURL = $sce.trustAsResourceUrl(customModalOptions.videoName);
				$scope.modalOptions.ok = function(result) {
					$uibModalInstance.close(result);
				};
				$scope.modalOptions.close = function(result) {
					$uibModalInstance.dismiss('cancel');
				};
			} ]
		}
		var modal = $uibModal.open(tempModalDefaults);

		modal.rendered.then(function() {
			// process your logic for DOM element
//			var video = document.getElementById('video');
//			video.play();
//			video.onplaying=function(){$('#show').hide();};
//			video.onwaiting=function(){$('#show').show();};
			
			return modal.result;
		});

	};

} ]);

cepApp.directive('myAudio', function() {
	return {
		restrict : 'E',
		link : function(scope, element, attr) {
			var player = element.children('.video')[0];
			element.children('.play').on('click', function() {
				console.log("here");
				player.play();
			});
			element.children('.pause').on('click', function() {
				player.pause();
			});
		}

	};
});