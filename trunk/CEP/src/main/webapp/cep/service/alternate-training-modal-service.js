cepApp.service('alternateTrainingModalService', [ '$uibModal', '$sce', function($uibModal, $sce) {

	var modalDefaults = {
		animation : true,
		templateUrl : 'cep/page-partials/alternate-training-modal.html',
		windowClass : 'asvab-modal'
	};

	var modalOptions = {
		headerText : '',
		bodyText : 'There is an error processing your request. If the error persists, please contact website administrator.',
		windowClass : 'asvab-modal'
	};
	
	this.showModal = function(customModalDefaults, customModalOptions) {
		if (!customModalDefaults)
			customModalDefaults = {};		
		customModalDefaults.backdrop = 'static';
		return this.show(customModalDefaults, customModalOptions);
	};	
	
	this.show = function(customModalDefaults, customModalOptions) {

		if (customModalOptions != undefined) {
			if (customModalOptions.bodyText != undefined) {
				customModalOptions.bodyText = $sce.trustAsHtml(customModalOptions.bodyText);
			}
		}

		// Create temp objects to work with since we're in a singleton service
		var tempModalDefaults = {};
		var tempModalOptions = {};

		// Map angular-ui modal custom defaults to modal defaults defined in
		// service
		angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

		// Map modal.html $scope custom properties to defaults defined in
		// service
		angular.extend(tempModalOptions, modalOptions, customModalOptions);

		if (!tempModalDefaults.controller) {
			tempModalDefaults.controller = [ '$scope', '$uibModalInstance', '$rootScope', 'UserService', function($scope, $uibModalInstance, $rootScope, UserService) {
        $scope.modalOptions = tempModalOptions;
				$scope.modalOptions.reregister = function(result) {
					$uibModalInstance.close($scope.modalOptions.alternateTrainingSelected);
				};
				$scope.modalOptions.close = function(results) {
					$uibModalInstance.close('cancel');
				}
			} ]
		}

		return $uibModal.open(tempModalDefaults).result;
	};

} ]);