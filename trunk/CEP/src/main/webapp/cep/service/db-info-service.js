cepApp.factory("DbInfoService", function (appUrl, $http, $q) {
	var factory = {};
	
	factory.getByName = function(name) {
		var deferred = $q.defer();
		$http.get(appUrl + 'db-info/get-by-name/' + name + '/').then(function (data) {
			deferred.resolve(data);
		}, function (error) {
			deferred.reject(error);
		});
		return deferred.promise;
	} 
	
	return factory;
});