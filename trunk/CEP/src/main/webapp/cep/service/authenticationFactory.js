cepApp.factory('AuthenticationService', function($http, $cookieStore, $rootScope, $timeout, $window, $location, ipCookie, ssoUrl) {
	var service = {};
	var domain =
		($location.host().indexOf('localhost') > -1) ?
			$location.host() :
			"." + $location.host();
	service.Login = Login;
	service.SetCredentials = SetCredentials;
	service.ClearCredentials = ClearCredentials;
	service.getUserName = getUserName;
	service.resetUserName = resetUserName;
	service.resetPassword = resetPassword;
	service.getAwsXsrfCookie = getAwsXsrfCookie;
	service.setAwsXsrfCookie = setAwsXsrfCookie;

	return service;

	function Login(username, password, callback) {

		/*
		 * Dummy authentication for testing, uses $timeout to simulate api call
		 * ----------------------------------------------
		 */
		$timeout(function() {
			var response;
			/*
			 * UserService.GetByUsername(username) .then(function (user) {
			 */

			if ('test' === password) {
				response = {
					success : true
				};
			} else {
				response = {
					success : false,
					message : 'Username or password is incorrect'
				};
			}
			callback(response);
			// });
		}, 1000);

		/*
		 * Use this for real authentication
		 * ----------------------------------------------
		 */
		// $http.post('/api/authenticate', { username: username,
		// password: password })
		// .success(function (response) {
		// callback(response);
		// });
	}

	function getUserName() {
		var authDataDecoded = Base64.decode($rootScope.globals.currentUser.authdata);
		return authDataDecoded.split(':')[5];
	}
	function getAwsXsrfCookie(){
		return ipCookie('X-XSRF-TOKEN');
	}

	function setAwsXsrfCookie(awsXsrfCookie) {
		var options = {
			path: '/',
			domain: domain
		};
		ipCookie('X-XSRF-TOKEN', awsXsrfCookie, options);
	}

	function resetUserName(userName) {
		var authDataDecoded = Base64.decode($rootScope.globals.currentUser.authdata);
		var authDataArray = authDataDecoded.split(':');
		authDataArray.splice(5, 1, userName);
		var authdata = Base64.encode(authDataArray.join(':'));

		$rootScope.globals.currentUser.authdata = authdata;
		$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;

		ipCookie.remove('globals', options);
		var options = {
			path: '/',
			domain: domain
		};
		ipCookie('globals', $rootScope.globals, options);
	}

	function resetPassword(pw) {
		var authDataDecoded = Base64.decode($rootScope.globals.currentUser.authdata);
		var authDataArray = authDataDecoded.split(':');
		authDataArray.splice(1, 1, pw);
		var authdata = Base64.encode(authDataArray.join(':'));

		$rootScope.globals.currentUser.authdata = authdata;
		$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;

		ipCookie.remove('globals', options);
		var options = {
			path: '/',
			domain: domain
		};
		ipCookie('globals', $rootScope.globals, options);
	}

	function SetCredentials(userId, role, accessCode, schoolCode, schoolName, city, state, zipCode, mepsId, mepsName) {
		$rootScope.globals = ipCookie('globals');

		if (!$rootScope.globals) {
			return;
		}

		$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint

		/**** Start Student Tracking ****/
		var mepsInfo = {
			schoolCode	: schoolCode,
			schoolName	: schoolName,
			city		: city,
			state		: state,
			zipCode		: zipCode,
			mepsId		: mepsId,
			mepsName	: mepsName
		};
		if (mepsInfo.zipCode) {
			ga('send', 'event', 'StudentLogin', 'ZipCode', mepsInfo.zipCode);
		} 
		if (mepsInfo.city) {
			ga('send', 'event', 'StudentLogin', 'City', mepsInfo.city);
		} 
		if (mepsInfo.mepsId) {
			ga('send', 'event', 'StudentLogin', 'MEPs', mepsInfo.mepsId);
		} 
		if (mepsInfo.mepsName) {
			ga('send', 'event', 'StudentLogin', 'MEPsName', mepsInfo.mepsName);
		}
		if (mepsInfo.state) {
			ga('send', 'event', 'StudentLogin', 'State', mepsInfo.state);
		}
		if (userId) {
			ga('send', 'event', 'StudentLogin', 'UserID', userId);
		}
		if (role) {
			ga('send', 'event', 'StudentLogin', 'Role', role);
		}
		if (accessCode) {
			ga('send', 'event', 'StudentLogin', 'AccessCode', accessCode);
		}
		/**** End Student Tracking ****/
		
	}

	function ClearCredentials() {
		$rootScope.globals = {};
		var options = {
			path: '/',
			domain: domain
		};
		ipCookie.remove('globals', options);
		$http.defaults.headers.common.Authorization = 'Basic';
	}
});

// Base64 encoding service used by AuthenticationService
var Base64 = {

	keyStr : 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

	encode : function(input) {
		var output = "";
		var chr1, chr2, chr3 = "";
		var enc1, enc2, enc3, enc4 = "";
		var i = 0;

		do {
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output + this.keyStr.charAt(enc1) + this.keyStr.charAt(enc2) + this.keyStr.charAt(enc3) + this.keyStr.charAt(enc4);
			chr1 = chr2 = chr3 = "";
			enc1 = enc2 = enc3 = enc4 = "";
		} while (i < input.length);

		return output;
	},

	decode : function(input) {
		var output = "";
		var chr1, chr2, chr3 = "";
		var enc1, enc2, enc3, enc4 = "";
		var i = 0;

		// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
		var base64test = /[^A-Za-z0-9\+\/\=]/g;
		if (base64test.exec(input)) {
			window.alert("There were invalid base64 characters in the input text.\n" + "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" + "Expect errors in decoding.");
		}
		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		do {
			enc1 = this.keyStr.indexOf(input.charAt(i++));
			enc2 = this.keyStr.indexOf(input.charAt(i++));
			enc3 = this.keyStr.indexOf(input.charAt(i++));
			enc4 = this.keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

			chr1 = chr2 = chr3 = "";
			enc1 = enc2 = enc3 = enc4 = "";

		} while (i < input.length);

		return output;
	}
};
