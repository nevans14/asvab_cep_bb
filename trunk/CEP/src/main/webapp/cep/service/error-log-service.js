cepApp.factory('errorLogService', [ '$log', '$window', 'appUrl', function($log, $window, appUrl) {

	function log(exception) {
		$log.error.apply($log, arguments);
		try {
			var args = [];
			if (typeof arguments === 'object') {
				for (var i = 0; i < arguments.length; i++) {
					arg = arguments[i];
					var exceptionItem = {};
					exceptionItem.message = arg.message;
					exceptionItem.stack = arg.stack;

					// use jquery to log information server side
					$.ajax({
						type : "POST",
						url : appUrl + "javascript-error-logging/log-error",
						contentType : "application/json",
						data : angular.toJson({
							errorUrl : $window.location.href,
							errorMessage : exceptionItem.message,
							errorStackTrace : exceptionItem.stack
						})
					});

					args.push(JSON.stringify(exceptionItem));
				}
			}
		} catch (loggingError) {
			// For Developers - log the log-failure.
			$log.warn("Error logging failed");
			$log.log(loggingError);
		}
	}
	return (log);
} ]);
