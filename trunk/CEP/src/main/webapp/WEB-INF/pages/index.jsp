<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title>Spring MVC Mybatis Angular https example</title>
<script src="<c:url value="/resources/js/angularjs/angular.js" />"></script>
<script>
angular.module("SimpleApp", [])
  .controller("AssetsController", function ($scope, $http) {
      $scope.connect = function() {
         $http({
            method : 'GET',
            url : '<c:url value="/resources/data/assets.dat" />',
            timeout: 10000,
			cache : false
         }).success(function(data, status, headers, config) {
            $scope.assetsList = data;
         }).error(function(data, status, headers, config) {
            alert("HTTP or data processing error with status (if any):" + status);
         });
      }
   });
</script>
</head>
<body ng-app="SimpleApp" ng-controller='AssetsController' style="width: 400px">
   <h1>https angular js example</h1>
   <h3>Assets list:</h3>
   <ol style="font-size: 0.7em;">
      <li ng-repeat="asset in assetsList">{{asset.assetDescription}} = {{asset.assetValue | currency}}
   </ol>
   <button ng-click="connect()">Fetch Data from Server</button>
   
   <br><hr>
   
   <h1>Maven + Spring MVC + Mybatis example</h1>

<h2>Message : ${message}</h2>
<h2>Counter : ${counter}</h2>	
</body>
</html>