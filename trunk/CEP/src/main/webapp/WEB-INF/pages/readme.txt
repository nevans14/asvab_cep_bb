	
	hsqldb datasource
	<!-- in-memory database and a datasource -->
	<jdbc:embedded-database id="dataSource">
		<jdbc:script location="classpath:database/cep-hsqldb-schema.sql" />
		<jdbc:script location="classpath:database/cep-hsqldb-dataload.sql" />
	</jdbc:embedded-database>

	<!-- database manager -->
	<bean depends-on="dataSource"
		class="org.springframework.beans.factory.config.MethodInvokingBean">
		<property name="targetClass" value="org.hsqldb.util.DatabaseManagerSwing" />
		<property name="targetMethod" value="main" />
		<property name="arguments">
			<list>
				<value>--url</value>
				<value>jdbc:hsqldb:mem:dataSource</value>
				<value>--user</value>
				<value>sa</value>
				<value>--password</value>
				<value></value>
			</list>
		</property>
	</bean>
