package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.testscore.ManualTestScore;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.StudentTestingProgamSelects;


public interface TestScoreMapper {

	public int insertTestScore(@Param("testScoreObjects") List<StudentTestingProgam> testScoreObjects);
	
	public StudentTestingProgamSelects getTestScoreWithAccessCode(@Param("accessCode") String accessCode);
	
	public StudentTestingProgamSelects getTestScoreWithUserId(@Param("userId") Integer userId);
	
	public StudentTestingProgamSelects getMockTestScoreWithAccessCode(@Param("accessCode") String accessCode);
	
	public StudentTestingProgamSelects getMockTestScoreWithUserId(@Param("userId") Integer userId);
	
	public int insertManualTestScore(@Param("manualTestScoreObj") ManualTestScore manualTestScoreObj);
	
	public ManualTestScore getManualTestScore(@Param("userId") int userId);
	
	public int updateManualTestScore(@Param("manualTestScoreObj") ManualTestScore manualTestScoreObj);

	public int deleteManualTestScore(@Param("userId") int userId);
	
	public ArrayList<StudentTestingProgamSelects> filterTestScore(@Param("administeredYear")  String administeredYear,
										 @Param("administeredMonth") String administeredMonth,
										 @Param("lastName")          String lastName,
										 @Param("gender")          	 String gender,
										 @Param("educationLevel")    String educationLevel,
										 @Param("schoolCode")        String schoolCode,
										 @Param("schoolAddress")     String schoolAddress,
										 @Param("schoolCity")        String schoolCity,
										 @Param("schoolState")       String schoolState,
										 @Param("schoolZipCode")     String schoolZipCode,
										 @Param("studentCity")       String studentCity,
										 @Param("studentState")      String studentState,
										 @Param("studentZipCode")    String studentZipCode);
}
