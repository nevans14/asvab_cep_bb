package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.notes.CareerClusterNotes;
import com.cep.spring.model.notes.Notes;

public interface NotesMapper {

	ArrayList<Notes> getNotes(@Param("userId") Integer userId);

	int insertNote(@Param("noteObject") Notes noteObject);

	int updateNote(@Param("noteObject") Notes noteObject);

	String getOccupationNotes(@Param("userId") Integer userId, @Param("socId") String socId);
	
	ArrayList<CareerClusterNotes> getCareerClusterNotes(@Param("userId") Integer userId);

	int insertCareerClusterNote(@Param("noteObject") CareerClusterNotes noteObject);

	int updateCareerClusterNote(@Param("noteObject") CareerClusterNotes noteObject);

	String getCareerClusterNote(@Param("userId") Integer userId, @Param("ccId") Integer ccId);

}
