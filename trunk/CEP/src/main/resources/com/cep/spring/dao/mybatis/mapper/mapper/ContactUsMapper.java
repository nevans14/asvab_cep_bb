package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.faq.EmailContactUs;
import com.cep.spring.model.faq.ScoreRequest;

public interface ContactUsMapper {

	ArrayList<String> getEmail(@Param("roleId") int roleId);

	int insertGeneralContactUs(@Param("emailObject") EmailContactUs emailObject);

	int insertScoreRquest(@Param("emailObject") ScoreRequest emailObject);

}
