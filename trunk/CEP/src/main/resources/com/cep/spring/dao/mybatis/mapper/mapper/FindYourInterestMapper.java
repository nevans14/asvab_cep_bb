package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIQuestion;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FindYourInterestProfile;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

public interface FindYourInterestMapper {

	int getProfileExist(@Param("fyiObject") FindYourInterestProfile fyiObject);

	int insertProfile(@Param("fyiObject") FindYourInterestProfile fyiObject);

	int insertTest(@Param("fyiObject") FindYourInterestProfile fyiObject);

	int insertTestScores(@Param("fyiObject") FindYourInterestProfile fyiObject);

	int insertAnswers(@Param("fyiObject") FindYourInterestProfile fyiObject);

	ArrayList<Scores> getCombinedAndGenderScore(@Param("userId") Integer userId);

	String getTestId();

	ArrayList<FYIQuestion> getFYIQuestions();

	ArrayList<GenderScore> getGenderScore(@Param("gender") char gender, @Param("rRawScore") int rRawScore,
			@Param("iRawScore") int iRawScore, @Param("aRawScore") int aRawScore, @Param("sRawScore") int sRawScore,
			@Param("eRawScore") int eRawScore, @Param("cRawScore") int cRawScore);

	ArrayList<CombinedScore> getCombinedScore(@Param("rRawScore") int rRawScore, @Param("iRawScore") int iRawScore,
			@Param("aRawScore") int aRawScore, @Param("sRawScore") int sRawScore, @Param("eRawScore") int eRawScore,
			@Param("cRawScore") int cRawScore);

	Integer getNumberTestTaken(@Param("oldUserId") Integer oldUserId, @Param("newUserId") Integer newUserId);
	
	Integer getOldUserId(@Param("newUserId") Integer newUserId);

	int updateProfile(@Param("scoreObject") ScoreChoice scoreObject);

	ArrayList<ScoreChoice> getProfile(@Param("userId") Integer userId);

	String getScoreChoice(@Param("userId") Integer userId);

	TiedSelection getTiedSelection(@Param("userId") Integer userId);

	int insertTiedSelection(@Param("tiedObject") TiedSelection tiedObject);
	
	int mergeProfile(@Param("oldUserId") Integer oldUserId, @Param("newUserId") Integer newUserId);
	
	int mergeTests(@Param("oldUserId") Integer oldUserId, @Param("newUserId") Integer newUserId);
	
	int mergeResults(@Param("oldUserId") Integer oldUserId, @Param("newUserId") Integer newUserId);
	
	int mergeAnswers(@Param("oldUserId") Integer oldUserId, @Param("newUserId") Integer newUserId);

	int getLastTestObject(Integer oldUserId, Integer newUserId);
	
	int deleteProfile(@Param("userId") Integer userId);

}
