package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteOccupation;

public interface FavoriteOccupationMapper {

	ArrayList<FavoriteOccupation> getFavoriteOccupation(@Param("userId") Integer userId);

	int insertFavoriteOccupation(@Param("favoriteObject") FavoriteOccupation favoriteObject);

	int deleteFavoriteOccupation(@Param("id") Integer id);
	
	ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(@Param("userId") Integer userId);

	int insertFavoriteCareerCluster(@Param("favoriteObject") FavoriteCareerCluster favoriteObject);

	int deleteFavoriteCareerCluster(@Param("id") Integer id);

	ArrayList<FavoriteCitmOccupation> getFavoriteCitmOccupation(@Param("userId") Integer userId);

	int insertFavoriteCitmOccupation(@Param("favoriteCitmObject") FavoriteCitmOccupation favoriteCitmObject);

	int deleteFavoriteCitmOccupation(@Param("userId") Integer userId)
}
