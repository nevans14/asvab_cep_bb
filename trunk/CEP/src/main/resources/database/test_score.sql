--CREATE TABLE [ASVAB_CITM_2018].[dbo].[test_scores](
--
--    SOURCE_FILE_NAME varchar(255) NOT NULL,
--	--CONTROL INFORMATION
--/*	APT_CD varchar(255)  DEFAULT NULL,
--	STATUS_CD varchar(255)  DEFAULT NULL,
--*/	ACCESS_CODE varchar(255)  NOT NULL,
--	PLATFORM varchar(255) DEFAULT NULL,
--	ANS_SEQ_NUM varchar(255)  DEFAULT NULL,
--/*	SSN_NUM varchar(255)  DEFAULT NULL,*/
--	MEPS_ID varchar(255)  DEFAULT NULL,
--	MET_SITE_ID varchar(255)  DEFAULT NULL,
--	LAST_NAME varchar(255)  DEFAULT NULL,
--/*	SERVICE varchar(255)  DEFAULT NULL,
--	TEST_TYPE varchar(255)  DEFAULT NULL,
--*/	GENDER varchar(255)  DEFAULT NULL,
--	EDUCATION_LEVEL varchar(255)  DEFAULT NULL,
--	CERTIFICATION varchar(255)  DEFAULT NULL,
--/*	ADMINISTRATOR_SSN_NUM varchar(255)  DEFAULT NULL,*/
--	TEST_FORM varchar(255)  DEFAULT NULL,
--	SCORING_METHOD varchar(255)  DEFAULT NULL,
--/*	RP_GRP varchar(255)  DEFAULT NULL,
--	ETHNIC_GRP varchar(255)  DEFAULT NULL,
--*/	ADMINISTERED_YEAR varchar(255)  DEFAULT NULL,
--	ADMINISTERED_MONTH varchar(255)  DEFAULT NULL,
--	ADMINISTERED_DAY varchar(255)  DEFAULT NULL,
--/*	SCORED_YEAR varchar(255)  DEFAULT NULL,
--	SCORED_MONTH varchar(255)  DEFAULT NULL,
--	SOCRED_DAY varchar(255)  DEFAULT NULL,
--	SCORED_HOUR varchar(255)  DEFAULT NULL,
--	SCORED_MINUTE varchar(255)  DEFAULT NULL,
--	SCORED_SECOND varchar(255)  DEFAULT NULL,
--	DOB varchar(255)  DEFAULT NULL,
--*/	
--	--SS ASVAB COMBINE CONTROL
--	SEQ_NUM varchar(255)  DEFAULT NULL,
--	
--	--SCORING SOFTWARE CONTROLS
--/*	TRANSFORMATION_TBL_VERSION varchar(255)  DEFAULT NULL,
--	ITEM_PARAMETER_TBL_VERSION varchar(255)  DEFAULT NULL,
--	PRIOR_DIST_TBL_VER varchar(255)  DEFAULT NULL,
--*/	
--	--AFQT, RAW, SS, COMPOSITES
--	TEST_COMPLETION_CD varchar(255)  DEFAULT NULL,
--/*	RAW_SCORE varchar(255)  DEFAULT NULL,*/
--	GS varchar(255)  DEFAULT NULL,
--	AR varchar(255)  DEFAULT NULL,
--	WK varchar(255)  DEFAULT NULL,
--	PC varchar(255)  DEFAULT NULL,
--/*	NO varchar(255)  DEFAULT NULL,
--	CS varchar(255)  DEFAULT NULL,*/
--	"AS" varchar(255)  DEFAULT NULL,
--	MK varchar(255)  DEFAULT NULL,
--	MC varchar(255)  DEFAULT NULL,
--	EL varchar(255)  DEFAULT NULL,
--	VE varchar(255)  DEFAULT NULL,
--	AO varchar(255)  DEFAULT NULL,
--	AFQT varchar(255)  DEFAULT NULL,
--	GT_ARMY varchar(255)  DEFAULT NULL,
--	CL_ARMY varchar(255)  DEFAULT NULL,
--	CO_ARMY varchar(255)  DEFAULT NULL,
--	EL_ARMY varchar(255)  DEFAULT NULL,
--	FA_ARMY varchar(255)  DEFAULT NULL,
--	GM_ARMY varchar(255)  DEFAULT NULL,
--	MM_ARMY varchar(255)  DEFAULT NULL,
--	OF_ARMY varchar(255)  DEFAULT NULL,
--	SC_ARMY varchar(255)  DEFAULT NULL,
--	ST_ARMY varchar(255)  DEFAULT NULL,
--	GT_NAVY varchar(255)  DEFAULT NULL,
--	EL_NAVY varchar(255)  DEFAULT NULL,
--	BEE_NAVY varchar(255)  DEFAULT NULL,
--	ENG_NAVY varchar(255)  DEFAULT NULL,
--	MEC_NAVY varchar(255)  DEFAULT NULL,
--	MEC2_NAVY varchar(255)  DEFAULT NULL,
--	NUC_NAVY varchar(255)  DEFAULT NULL,
--	OPS_NAVY varchar(255)  DEFAULT NULL,
--	HM_NAVY varchar(255)  DEFAULT NULL,
--	ADM_NAVY varchar(255)  DEFAULT NULL,
--	M_AF varchar(255)  DEFAULT NULL,
--	A_AF varchar(255)  DEFAULT NULL,
--	G_AF varchar(255)  DEFAULT NULL,
--	E_AF varchar(255)  DEFAULT NULL,
--	MM_MC varchar(255)  DEFAULT NULL,
--	GT_MC varchar(255)  DEFAULT NULL, 
--	EL_MC varchar(255)  DEFAULT NULL,
--	CL_MC varchar(255)  DEFAULT NULL,
--	BOOKLET_NUM varchar(255)  DEFAULT NULL,
--	
--	--THETA, ITEM DATA, SCORING
--/*	GS_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	GS_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	GS_ITERATIONS varchar(255)  DEFAULT NULL,
--	GS_ACTUAL_ITEM_RESPONSE  varchar(255)  DEFAULT NULL,
--	GS_DICHOTOMOUS_ITEM_SCORING  varchar(255)  DEFAULT NULL,
--	AR_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	AR_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	AR_ITERATIONS varchar(255)  DEFAULT NULL,
--	AR_ACTUALITEM_RESPONSE  varchar(255)  DEFAULT NULL,
--	AR_DICHOTOMOUS_ITEM_SCORING  varchar(255)  DEFAULT NULL,
--	WK_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	WK_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	WK_ITERATIONS varchar(255)  DEFAULT NULL,
--	WK_ACTUAL_ITEM_RESPONSE  varchar(255)  DEFAULT NULL,
--	WK_DICHOTOMOUS_ITEM_SCORING  varchar(255)  DEFAULT NULL,
--	PC_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	PC_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	PC_ITERATIONS varchar(255)  DEFAULT NULL,
--	PC_ACTUALITEM_RESPONSE  varchar(255)  DEFAULT NULL,
--	PC_DICHOTOMOUS_ITEM_SCORING  varchar(255)  DEFAULT NULL,
--	MK_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	MK_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	MK_ITERATIONS varchar(255)  DEFAULT NULL,
--	MK_ACTUALITEM_RESPONSE  varchar(255)  DEFAULT NULL,
--	MK_DICHOTOMOUS_ITEM_SCORING varchar(255)  DEFAULT NULL,
--	EI_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	EI_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	EI_ITERATIONS varchar(255)  DEFAULT NULL,
--	EI_ACTUAL_ITEM_RESPONSE varchar(255)  DEFAULT NULL,
--	EI_DICHOTOMOUS_ITEM_SCORING varchar(255)  DEFAULT NULL,
--	AS_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	AS_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	AS_ITERATIONS varchar(255)  DEFAULT NULL,
--	AS_ACTUAL_ITEM_RESPONSE varchar(255)  DEFAULT NULL,
--	AS_DICHOTOMOUS_ITEM_SCORING varchar(255)  DEFAULT NULL,
--	MC_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	MC_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	MC_ITERATIONS varchar(255)  DEFAULT NULL,
--	MC_ACTUAL_ITEM_RESPONSE varchar(255)  DEFAULT NULL,
--	MC_DICHOTOMOUS_ITEM_SCORING varchar(255)  DEFAULT NULL,
--	AO_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	AO_UNROUNDED_THETA varchar(255)  DEFAULT NULL,
--	AO_ITERATIONS varchar(255)  DEFAULT NULL,
--	AO_ACTUAL_ITEM_RESPONSE varchar(255)  DEFAULT NULL,
--	AO_DICHOTOMOUS_ITEM_SCORING varchar(255)  DEFAULT NULL,
--	VE_COMPLETION_CD varchar(255)  DEFAULT NULL,
--	DATAENTRY_TYPE varchar(255)  DEFAULT NULL,
--*/	
--	--HIGH SCHOOL INFORMATION
--	SCHOOL_CODE varchar(255)  DEFAULT NULL,
--	SCHOOL_SESSION_NBR varchar(255)  DEFAULT NULL,
--	SCHOOL_SPECIAL_INSTRUCTIONS varchar(255)  DEFAULT NULL,
--	RELEASE_TO_SERVICE_DATE varchar(255)  DEFAULT NULL,
--	SCHOOL_COUNCELLOR_CD varchar(255)  DEFAULT NULL,
--/*	SERVICE_RESPONSIBLE_FOR_SCHEDULING varchar(255)  DEFAULT NULL,*/
--	SCHOOL_SITE_NAME varchar(255)  DEFAULT NULL,
--	SCHOOL_CITY_NAME  varchar(255)  DEFAULT NULL,
--	SCHOOL_STATE_NAME  varchar(255)  DEFAULT NULL,
--	SCHOOL_ZIPCODE  varchar(255)  DEFAULT NULL,
--	
--	--TEST SCORING INDICATORS AND CONTROLS
--/*	SHOT_INDICATOR varchar(255)  DEFAULT NULL,
--	SPECIAL_STUDIES varchar(255)  DEFAULT NULL,
--	PAGE1STATUS varchar(255)  DEFAULT NULL,
--	PAGE2STATUS varchar(255)  DEFAULT NULL,
--	PAGE3STATUS varchar(255)  DEFAULT NULL,
--*/	
--	--STUDENT INFORMATION
--/*	STUDENT_NAME varchar(255)  DEFAULT NULL,
--	STUDENT_ADDRESS varchar(255)  DEFAULT NULL,
--*/	STUDENT_CITY varchar(255)  DEFAULT NULL,
--	STUDENT_STATE varchar(255)  DEFAULT NULL,
--	STUDENT_ZIPCODE varchar(255)  DEFAULT NULL,
--/*	STUDENT_TELEPHONE_NUMBER varchar(255)  DEFAULT NULL,*/
--	STUDENT_INTENTIONS varchar(255)  DEFAULT NULL,
--	
--	--STUDENT RESULT SHEETSCORES
--	MIL_CAREER_SCORE varchar(255)  DEFAULT NULL,
--	MIL_CAREER_SCORE_CATEGORY varchar(255)  DEFAULT NULL,
--	
--	--VERBAL ABILITY
--	VA_YP_SCORE varchar(255)  DEFAULT NULL,
--	VA_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	VA_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	VA_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	VA_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	VA_SGS varchar(255)  DEFAULT NULL,
--	VA_USL varchar(255)  DEFAULT NULL,
--	VA_LSL varchar(255)  DEFAULT NULL,
--	
--	--MATHEMATICAL ABILITY
--	MA_YP_SCORE varchar(255)  DEFAULT NULL,
--	MA_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MA_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MA_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MA_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MA_SGS varchar(255)  DEFAULT NULL,
--	MA_USL varchar(255)  DEFAULT NULL,
--	MA_LSL varchar(255)  DEFAULT NULL,
--	
--	--SCIENCE & TECHNICAL ABILITY
--	TEC_YP_SCORE varchar(255)  DEFAULT NULL,
--	TEC_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	TEC_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	TEC_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	TEC_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	TEC_SGS varchar(255)  DEFAULT NULL,
--	TEC_USL varchar(255)  DEFAULT NULL,
--	TEC_LSL varchar(255)  DEFAULT NULL,
--	
--	--GENERAL SCIENCE
--	GS_YP_SCORE varchar(255)  DEFAULT NULL,
--	GS_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	GS_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	GS_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	GS_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	GS_SGS varchar(255)  DEFAULT NULL,
--	GS_USL varchar(255)  DEFAULT NULL,
--	GS_LSL varchar(255)  DEFAULT NULL,
--	
--	--ARITHMETIC REASONING
--	AR_YP_SCORE varchar(255)  DEFAULT NULL,
--	AR_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AR_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AR_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AR_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AR_SGS varchar(255)  DEFAULT NULL,
--	AR_USL varchar(255)  DEFAULT NULL,
--	AR_LSL varchar(255)  DEFAULT NULL,
--	
--	--WORD KNOWLEDGE
--	WK_YP_SCORE varchar(255)  DEFAULT NULL,
--	WK_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	WK_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	WK_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	WK_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	WK_SGS varchar(255)  DEFAULT NULL,
--	WK_USL varchar(255)  DEFAULT NULL,
--	WK_LSL varchar(255)  DEFAULT NULL,
--	
--	--PARAGRAPH COMPREHENSION
--	PC_YP_SCORE varchar(255)  DEFAULT NULL,
--	PC_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	PC_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	PC_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	PC_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	PC_SGS varchar(255)  DEFAULT NULL,
--	PC_USL varchar(255)  DEFAULT NULL,
--	PC_LSL varchar(255)  DEFAULT NULL,
--	
--	--MATHEMATICS KNOWLEDGE
--	MK_YP_SCORE varchar(255)  DEFAULT NULL,
--	MK_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MK_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MK_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MK_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MK_SGS varchar(255)  DEFAULT NULL,
--	MK_USL varchar(255)  DEFAULT NULL,
--	MK_LSL varchar(255)  DEFAULT NULL,
--	
--	--ELECTRONICS INFORMATION
--	EI_YP_SCORE varchar(255)  DEFAULT NULL,
--	EI_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	EI_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	EI_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	EI_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	EI_SGS varchar(255)  DEFAULT NULL,
--	EI_USL varchar(255)  DEFAULT NULL,
--	EI_LSL varchar(255)  DEFAULT NULL,
--	
--	--AUTO SHOP INFORMATION
--	AS_YP_SCORE varchar(255)  DEFAULT NULL,
--	AS_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AS_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AS_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AS_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AS_SGS varchar(255)  DEFAULT NULL,
--	AS_USL varchar(255)  DEFAULT NULL,
--	AS_LSL varchar(255)  DEFAULT NULL,
--	
--	--MECHANICAL COMPREHENSION
--	MC_YP_SCORE varchar(255)  DEFAULT NULL,
--	MC_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MC_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MC_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MC_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	MC_SGS varchar(255)  DEFAULT NULL,
--	MC_USL varchar(255)  DEFAULT NULL,
--	MC_LSL varchar(255)  DEFAULT NULL,
--	
--	--ASSEMBLINGOBJECTS
--	AO_YP_SCORE varchar(255)  DEFAULT NULL,
--	AO_GS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AO_GOS_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AO_COMP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AO_TP_PERCENTAGE varchar(255)  DEFAULT NULL,
--	AO_SGS varchar(255)  DEFAULT NULL,
--	AO_USL varchar(255)  DEFAULT NULL,
--	AO_LSL varchar(255)  DEFAULT NULL,
--	
--	--RACE FIELDS
--	AMERICAN_INDIAN varchar(255)  DEFAULT NULL,
--	ASIAN varchar(255)  DEFAULT NULL,
--	AFRICAN_AMERICAN varchar(255)  DEFAULT NULL,
--	NATIVE_HAWIIAN varchar(255)  DEFAULT NULL,
--	WHITE varchar(255)  DEFAULT NULL,
--	RESERVED varchar(255)  DEFAULT NULL,
--	HISPANIC varchar(255)  DEFAULT NULL,
--	NOT_HISPANIC varchar(255)  DEFAULT NULL
-- );

CREATE TABLE [dbo].[test_scores](
	[SOURCE_FILE_NAME] [varchar](50) NULL,
	[ACCESS_CODE ] [varchar](10) NULL,
	[PLATFORM] [varchar](10) NULL,
	[ANS_SEQ_NUM] [int] NULL,
	[MEPS_ID] [int] NULL,
	[MET_SITE_ID] [int] NULL,
	[GENDER] [varchar](50) NULL,
	[EDUCATION_LEVEL] [int] NULL,
	[CERTIFICATION] [int] NULL,
	[TEST_FORM] [varchar](10) NULL,
	[SCORING_METHOD] [varchar](10) NULL,
	[ADMINISTERED_YEAR] [int] NULL,
	[ADMINISTERED_MONTH] [int] NULL,
	[ADMINISTERED_DAY] [int] NULL,
	[SEQ_NUM] [int] NULL,
	[TEST_COMPLETION_CD] [int] NULL,
	[GS] [int] NULL,
	[AR] [int] NULL,
	[WK] [int] NULL,
	[PC] [int] NULL,
	[AS] [int] NULL,
	[MK] [int] NULL,
	[MC] [int] NULL,
	[EL] [int] NULL,
	[VE] [int] NULL,
	[AO] [int] NULL,
	[AFQT] [int] NULL,
	[GT_ARMY] [int] NULL,
	[CL_ARMY] [int] NULL,
	[CO_ARMY] [int] NULL,
	[EL_ARMY] [int] NULL,
	[FA_ARMY] [int] NULL,
	[GM_ARMY] [int] NULL,
	[MM_ARMY] [int] NULL,
	[OF_ARMY] [int] NULL,
	[SC_ARMY] [int] NULL,
	[ST_ARMY] [int] NULL,
	[GT_NAVY] [int] NULL,
	[EL_NAVY] [int] NULL,
	[BEE_NAVY] [int] NULL,
	[ENG_NAVY] [int] NULL,
	[MEC_NAVY] [int] NULL,
	[MEC2_NAVY] [int] NULL,
	[NUC_NAVY] [int] NULL,
	[OPS_NAVY] [int] NULL,
	[HM_NAVY] [int] NULL,
	[ADM_NAVY] [int] NULL,
	[M_AF] [int] NULL,
	[A_AF] [int] NULL,
	[G_AF] [int] NULL,
	[E_AF] [int] NULL,
	[MM_MC] [int] NULL,
	[GT_MC] [int] NULL,
	[EL_MC] [int] NULL,
	[CL_MC] [int] NULL,
	[BOOKLET_NUM] [varchar](255) NULL,
	[SCHOOL_CODE] [int] NULL,
	[SCHOOL_SESSION_NBR] [int] NULL,
	[SCHOOL_SPECIAL_INSTRUCTIONS] [int] NULL,
	[RELEASE_TO_SERVICE_DATE] [int] NULL,
	[SCHOOL_COUNCELLOR_CD] [varchar](10) NULL,
	[SCHOOL_SITE_NAME] [varchar](100) NULL,
	[SCHOOL_CITY_NAME] [varchar](100) NULL,
	[SCHOOL_STATE_NAME] [varchar](100) NULL,
	[SCHOOL_ZIPCODE] [varchar](50) NULL,
	[STUDENT_NAME] [varchar](250) NULL,
	[LAST_NAME] [varchar](100) NULL,
	[FIRST_NAME] [varchar](100) NULL,
	[MIDDLE_INITIAL] [varchar](50) NULL,
	[STUDENT_CITY] [varchar](100) NULL,
	[STUDENT_STATE] [varchar](100) NULL,
	[STUDENT_ZIPCODE] [varchar](50) NULL,
	[STUDENT_INTENTIONS] [varchar](10) NULL,
	[MIL_CAREER_SCORE] [int] NULL,
	[MIL_CAREER_SCORE_CATEGORY] [int] NULL,
	[VA_YP_SCORE] [int] NULL,
	[VA_GS_PERCENTAGE] [int] NULL,
	[VA_GOS_PERCENTAGE] [int] NULL,
	[VA_COMP_PERCENTAGE] [int] NULL,
	[VA_TP_PERCENTAGE] [int] NULL,
	[VA_SGS] [int] NULL,
	[VA_USL] [int] NULL,
	[VA_LSL] [int] NULL,
	[MA_YP_SCORE] [int] NULL,
	[MA_GS_PERCENTAGE] [int] NULL,
	[MA_GOS_PERCENTAGE] [int] NULL,
	[MA_COMP_PERCENTAGE] [int] NULL,
	[MA_TP_PERCENTAGE] [int] NULL,
	[MA_SGS] [int] NULL,
	[MA_USL] [int] NULL,
	[MA_LSL] [int] NULL,
	[TEC_YP_SCORE] [int] NULL,
	[TEC_GS_PERCENTAGE] [int] NULL,
	[TEC_GOS_PERCENTAGE] [int] NULL,
	[TEC_COMP_PERCENTAGE] [int] NULL,
	[TEC_TP_PERCENTAGE] [int] NULL,
	[TEC_SGS] [int] NULL,
	[TEC_USL] [int] NULL,
	[TEC_LSL] [int] NULL,
	[GS_YP_SCORE] [int] NULL,
	[GS_GS_PERCENTAGE] [int] NULL,
	[GS_GOS_PERCENTAGE] [int] NULL,
	[GS_COMP_PERCENTAGE] [int] NULL,
	[GS_TP_PERCENTAGE] [int] NULL,
	[GS_SGS] [int] NULL,
	[GS_USL] [int] NULL,
	[GS_LSL] [int] NULL,
	[AR_YP_SCORE] [int] NULL,
	[AR_GS_PERCENTAGE] [int] NULL,
	[AR_GOS_PERCENTAGE] [int] NULL,
	[AR_COMP_PERCENTAGE] [int] NULL,
	[AR_TP_PERCENTAGE] [int] NULL,
	[AR_SGS] [int] NULL,
	[AR_USL] [int] NULL,
	[AR_LSL] [int] NULL,
	[WK_YP_SCORE] [int] NULL,
	[WK_GS_PERCENTAGE] [int] NULL,
	[WK_GOS_PERCENTAGE] [int] NULL,
	[WK_COMP_PERCENTAGE] [int] NULL,
	[WK_TP_PERCENTAGE] [int] NULL,
	[WK_SGS] [int] NULL,
	[WK_USL] [int] NULL,
	[WK_LSL] [int] NULL,
	[PC_YP_SCORE] [int] NULL,
	[PC_GS_PERCENTAGE] [int] NULL,
	[PC_GOS_PERCENTAGE] [int] NULL,
	[PC_COMP_PERCENTAGE] [int] NULL,
	[PC_TP_PERCENTAGE] [int] NULL,
	[PC_SGS] [int] NULL,
	[PC_USL] [int] NULL,
	[PC_LSL] [int] NULL,
	[MK_YP_SCORE] [int] NULL,
	[MK_GS_PERCENTAGE] [int] NULL,
	[MK_GOS_PERCENTAGE] [int] NULL,
	[MK_COMP_PERCENTAGE] [int] NULL,
	[MK_TP_PERCENTAGE] [int] NULL,
	[MK_SGS] [int] NULL,
	[MK_USL] [int] NULL,
	[MK_LSL] [int] NULL,
	[EI_YP_SCORE] [int] NULL,
	[EI_GS_PERCENTAGE] [int] NULL,
	[EI_GOS_PERCENTAGE] [int] NULL,
	[EI_COMP_PERCENTAGE] [int] NULL,
	[EI_TP_PERCENTAGE] [int] NULL,
	[EI_SGS] [int] NULL,
	[EI_USL] [int] NULL,
	[EI_LSL] [int] NULL,
	[AS_YP_SCORE] [int] NULL,
	[AS_GS_PERCENTAGE] [int] NULL,
	[AS_GOS_PERCENTAGE] [int] NULL,
	[AS_COMP_PERCENTAGE] [int] NULL,
	[AS_TP_PERCENTAGE] [int] NULL,
	[AS_SGS] [int] NULL,
	[AS_USL] [int] NULL,
	[AS_LSL] [int] NULL,
	[MC_YP_SCORE] [int] NULL,
	[MC_GS_PERCENTAGE] [int] NULL,
	[MC_GOS_PERCENTAGE] [int] NULL,
	[MC_COMP_PERCENTAGE] [int] NULL,
	[MC_TP_PERCENTAGE] [int] NULL,
	[MC_SGS] [int] NULL,
	[MC_USL] [int] NULL,
	[MC_LSL] [int] NULL,
	[AO_YP_SCORE] [int] NULL,
	[AO_GS_PERCENTAGE] [int] NULL,
	[AO_GOS_PERCENTAGE] [int] NULL,
	[AO_COMP_PERCENTAGE] [int] NULL,
	[AO_TP_PERCENTAGE] [int] NULL,
	[AO_SGS] [int] NULL,
	[AO_USL] [int] NULL,
	[AO_LSL] [int] NULL,
	[AMERICAN_INDIAN] [int] NULL,
	[ASIAN] [int] NULL,
	[AFRICAN_AMERICAN] [int] NULL,
	[NATIVE_HAWIIAN] [int] NULL,
	[WHITE] [int] NULL,
	[RESERVED] [int] NULL,
	[HISPANIC] [int] NULL,
	[NOT_HISPANIC] [int] NULL
) ON [PRIMARY]
GO


 CREATE TABLE [ASVAB_CITM_2018].[dbo].[manual_test_scores](
 	USER_ID  INTEGER  NOT NULL,
 	VE varchar(255)  DEFAULT NULL,
 	MA varchar(255)  DEFAULT NULL,
 	TEC varchar(255)  DEFAULT NULL,
 	AFQT varchar(255)  DEFAULT NULL
 );