package com.cep.spring.model; 

import java.util.Date;

public class AccessCodes {
    private Long userId;
    private String accessCode, role;
    private Date expireDate;
    private Short unlimitedFyi;
    // default constructor
    public AccessCodes() {}
    // parameterized constructor
    public AccessCodes(String accessCode, Date expireDate, String role) {
    	this.accessCode = accessCode;
    	this.expireDate = expireDate;
    	this.role = role;
    }
    
    public AccessCodes(String accessCode, Date expireDate, String role, Short unlimitedFyi) {
    	this.accessCode = accessCode;
    	this.expireDate = expireDate;
    	this.role = role;
    	this.unlimitedFyi = unlimitedFyi;
    }
    // accessors
    public Long getUserId() { return this.userId; }
    public String getAccessCode() {return this.accessCode; }
    public Date getExpireDate() { return this.expireDate; }
    public String getRole() { return this.role; }
    public Short getUnlimitedFyi() { return this.unlimitedFyi; }
    // mutators
    public void setUserId(Long userId) { this.userId = userId; }
    public void setAccessCode(String accessCode) { this.accessCode = accessCode; }
    public void setExpireDate(Date expireDate) { this.expireDate = expireDate; }
    public void setRole(String role) { this.role = role; }
    public void setUnlimitedFyi(Short unlimitedFyi) { this.unlimitedFyi = unlimitedFyi; }    
    // helpers
    @Override
    public String toString() {
    	return
			this.accessCode + "," +
    		this.expireDate + "," +
			this.role + "," +
    		this.unlimitedFyi;
    }
}