package com.cep.spring.dao.mybatis.impl;

import com.cep.spring.dao.mybatis.PlanYourFutureDAO;
import com.cep.spring.dao.mybatis.service.PlanYourFutureService;
import com.cep.spring.model.PlanYourFuture.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class PlanYourFutureDAOImpl implements PlanYourFutureDAO {
    
    private final PlanYourFutureService service;
    
    @Autowired
    public PlanYourFutureDAOImpl(PlanYourFutureService service) {
        this.service = service;
    }

    public ArrayList<UserOccupationPlan> selectOccupationPlans(int userId) {
        return service.selectOccupationPlans(userId);
    }

    public UserOccupationPlan selectOccupationPlan(int userId, int id) {
        return service.selectOccupationPlan(userId, id);
    }

    public int insertOccupationPlan(int userId, UserOccupationPlan model) {
        return service.insertOccupationPlan(userId, model);
    }

    public int updateOccupationPlan(int userId, int id, UserOccupationPlan model) {
        return service.updateOccupationPlan(userId, id, model);
    }

    public int deleteOccupationPlan(int userId, int id) {
        return service.deleteOccupationPlan(userId, id);
    }

    public ArrayList<UserOccupationPlanCalendar> selectPlanCalendars(int userId, int planId) {
        return service.selectPlanCalendars(userId, planId);
    }

    public ArrayList<UserOccupationPlanCalendar> selectCalendars(int userId) {
        return service.selectCalendars(userId);
    }

    public UserOccupationPlanCalendar selectGraduationDate(int userId) {return service.selectGraduationDate(userId);}

    public int insertCalendar(int userId, Integer planId, UserOccupationPlanCalendar model) {
        return service.insertCalendar(userId, planId, model);
    }

    public int updateCalendar(int userId, Integer planId, int id, UserOccupationPlanCalendar model) {
        return service.updateCalendar(userId, planId, id, model);
    }

    public int deleteCalendar(int userId, Integer planId, int id) {
        return service.deleteCalendar(userId, planId, id);
    }

    public ArrayList<UserOccupationPlanCalendarTask> selectPlanTasks(int userId, int planId) {
        return service.selectPlanTasks(userId, planId);
    }

    public ArrayList<UserOccupationPlanCalendarTask> selectTasks(int userId) {
        return service.selectTasks(userId);
    }

    public int insertTask(int userId, Integer planId, UserOccupationPlanCalendarTask model) {
        return service.insertTask(userId, planId, model);
    }

    public int updateTask(int userId, Integer planId, int id, UserOccupationPlanCalendarTask model) {
        return service.updateTask(userId, planId, id, model);
    }

    public int deleteTask(int userId, Integer planId, int id) {
        return service.deleteTask(userId, planId, id);
    }

    public UserSchoolPlan selectSchoolPlan(int userId, int planId) {
        return service.selectSchoolPlan(userId, planId);
    }

    public int insertSchoolPlan(int userId, int planId, UserSchoolPlan model) {
        return service.insertSchoolPlan(userId, planId, model);
    }

    public int updateSchoolPlan(int userId, int planId, UserSchoolPlan model) {
        return service.updateSchoolPlan(userId, planId, model);
    }

    public ArrayList<UserSchoolPlanCourse> selectSchoolCourses(int userId, int planId) {
        return service.selectSchoolCourses(userId, planId);
    }

    public int insertSchoolCourse(int userId, int planId, UserSchoolPlanCourse model) {
        return service.insertSchoolCourse(userId, planId, model);
    }

    public int updateSchoolCourse(int userId, int planId, int id, UserSchoolPlanCourse model) {
        return service.updateSchoolCourse(userId, planId, id, model);
    }

    public int deleteSchoolCourse(int userId, int planId, int id) {
        return service.deleteSchoolCourse(userId, planId, id);
    }

    public ArrayList<UserSchoolPlanActivity> selectSchoolActivities(int userId, int planId) {
        return service.selectSchoolActivities(userId, planId);
    }

    public int insertSchoolActivity(int userId, int planId, UserSchoolPlanActivity model) {
        return service.insertSchoolActivity(userId, planId, model);
    }

    public int updateSchoolActivity(int userId, int planId, int id, UserSchoolPlanActivity model) {
        return service.updateSchoolActivity(userId, planId, id, model);
    }

    public int deleteSchoolActivity(int userId, int planId, int id) {
        return service.deleteSchoolActivity(userId, planId, id);
    }

    public ArrayList<UserCollegePlan> selectCollegePlans(int userId, int planId) {
        return service.selectCollegePlans(userId, planId);
    }

    public int insertCollegePlan(int userId, int planId, UserCollegePlan model) {
        return service.insertCollegePlan(userId, planId, model);
    }

    public int updateCollegePlan(int userId, int planId, int id, UserCollegePlan model) {
        return service.updateCollegePlan(userId, planId, id, model);
    }

    public int deleteCollegePlan(int userId, int planId, int id) {
        return service.deleteCollegePlan(userId, planId, id);
    }

    public ArrayList<UserMilitaryPlan> selectMilitaryPlans(int userId, int planId) {
        return service.selectMilitaryPlans(userId, planId);
    }

    public int insertMilitaryPlan(int userId, int planId, UserMilitaryPlan model) {
        return service.insertMilitaryPlan(userId, planId, model);
    }

    public int updateMilitaryPlan(int userId, int planId, int id, UserMilitaryPlan model) {
        return service.updateMilitaryPlan(userId, planId, id, model);
    }

    public int deleteMilitaryPlan(int userId, int planId, int id) {
        return service.deleteMilitaryPlan(userId, planId, id);
    }

    public int insertMilitaryPlanCareer(int userId, int militaryPlanId, UserMilitaryPlanCareer model) {
        return service.insertMilitaryPlanCareer(userId, militaryPlanId, model);
    }

    public int updateMilitaryPlanCareer(int userId, int militaryPlanId, int id, UserMilitaryPlanCareer model) {
        return service.updateMilitaryPlanCareer(userId, militaryPlanId, id, model);
    }

    public int deleteMilitaryPlanCareer(int userId, int militaryPlanId, int id) {
        return service.deleteMilitaryPlanCareer(userId, militaryPlanId, id);
    }

    public int insertMilitaryPlanRotc(int userId, int militaryPlanId, UserMilitaryPlanRotc model) {
        return service.insertMilitaryPlanRotc(userId, militaryPlanId, model);
    }

    public int updateMilitaryPlanRotc(int userId, int militaryPlanId, int id, UserMilitaryPlanRotc model) {
        return service.updateMilitaryPlanRotc(userId, militaryPlanId, id, model);
    }

    public int insertMilitaryPlanServiceCollege(int userId, int militaryPlanId, UserMilitaryPlanServiceCollege model) {
        return service.insertMilitaryPlanServiceCollege(userId, militaryPlanId, model);
    }

    public int deleteMilitaryPlanServiceCollege(int userId, int militaryPlanId, int id) {
        return service.deleteMilitaryPlanServiceCollege(userId, militaryPlanId, id);
    }

    public UserWorkPlan selectWorkPlan(int userId, int planId) {
        return service.selectWorkPlan(userId, planId);
    }

    public int insertWorkPlan(int userId, int planId, UserWorkPlan model) {
        return service.insertWorkPlan(userId, planId, model);
    }

    public int updateWorkPlan(int userId, int planId, UserWorkPlan model) {
        return service.updateWorkPlan(userId, planId, model);
    }

    public ArrayList<RefGapYearTypes> selectGapYearTypes() { return service.selectGapYearTypes(); }

    public ArrayList<RefGapYearPlans> selectGapYearOptions() {
        return service.selectGapYearOptions();
    }

    public ArrayList<UserGapYearPlan> selectGapYearPlans(int userId, int planId) {
        return service.selectGapYearPlans(userId, planId);
    }

    public int insertGapYearPlan(int userId, int planId, UserGapYearPlan model) {
        return service.insertGapYearPlan(userId, planId, model);
    }

    public int insertGapYearPlanTypes(UserGapYearPlan model) {
        return service.insertGapYearPlanTypes(model);
    }

    public int updateGapYearPlan(int userId, int planId, int id, UserGapYearPlan model) {
        return service.updateGapYearPlan(userId, planId, id, model);
    }

    public int updateGapYearPlanTypes(int id, UserGapYearPlan model) {
        return service.updateGapYearPlanTypes(id, model);
    }

    public int deleteGapYearPlan(int userId, int planId, int id) {
        return service.deleteGapYearPlan(userId, planId, id);
    }
    
}
