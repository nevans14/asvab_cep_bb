package com.cep.spring.model.fyi;

import java.io.Serializable;

public class FYIRawScores implements Serializable {

	private static final long serialVersionUID = 4546683941366610216L;
	private char interestCd;
	private int rawScore;

	public char getInterestCd() {
		return interestCd;
	}

	public void setInterestCd(char interestCd) {
		this.interestCd = interestCd;
	}

	public int getRawScore() {
		return rawScore;
	}

	public void setRawScore(int rawScore) {
		this.rawScore = rawScore;
	}
}
