package com.cep.spring.model.occufind;

import java.io.Serializable;

public class OccufindMoreResources implements Serializable {

	private static final long serialVersionUID = 2704554092796058045L;

	private String id, title, url;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
