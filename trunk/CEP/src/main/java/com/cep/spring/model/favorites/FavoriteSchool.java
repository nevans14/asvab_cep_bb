package com.cep.spring.model.favorites;

import java.io.Serializable;

public class FavoriteSchool implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer unitId;
	private String schoolName, city, state;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getCity() { return city; }

	public void setCity(String city) { this.city = city; }

	public String getState() { return state; }

	public void setState(String state) { this.state = state; }
}
