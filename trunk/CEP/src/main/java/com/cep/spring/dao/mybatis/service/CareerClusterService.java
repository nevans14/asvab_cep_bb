package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.CareerClusterMapper;
import com.cep.spring.model.CareerCluster;
import com.cep.spring.model.CareerClusterOccupation;
import com.cep.spring.model.OccupationEducationLevel;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.RelatedOccupation;
import com.cep.spring.model.SkillImportance;
import com.cep.spring.model.Task;
import com.cep.spring.model.occufind.OccufindSearch;

@Service
public class CareerClusterService {

	private final CareerClusterMapper mapper;

	@Autowired
	public CareerClusterService(CareerClusterMapper mapper) {
		this.mapper = mapper;
	}

	public ArrayList<CareerCluster> getCareerCluster() {
		return mapper.getCareerCluster();
	}

	public ArrayList<CareerClusterOccupation> getCareerClusterOccupation(int ccId) { return mapper.getCareerClusterOccupation(ccId); }

	public SkillImportance getSkillImportance(String onetSoc) {
		return mapper.getSkillImportance(onetSoc);
	}

	public ArrayList<Task> getTasks(String onetSoc) {
		return mapper.getTasks(onetSoc);
	}

	public OccupationInterestCode getOccupationInterestCodes(String onetSoc) { return mapper.getOccupationInterestCodes(onetSoc); }

	public ArrayList<OccupationEducationLevel> getEducationLevel(String onetSoc) { return mapper.getEducationLevel(onetSoc); }

	public ArrayList<RelatedOccupation> getRelatedOccupations(String onetSoc) { return mapper.getRelatedOccupations(onetSoc); }

	public ArrayList<OccufindSearch> getCareerClusterOccupations(int ccId) { return mapper.getCareerClusterOccupations(ccId); }

	public CareerCluster getCareerClusterById(int ccId) {
		return mapper.getCareerClusterById(ccId);
	}
}
