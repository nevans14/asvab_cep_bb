package com.cep.spring.dao.mybatis.impl;

import com.cep.spring.dao.mybatis.WorkValueDAO;
import com.cep.spring.dao.mybatis.service.WorkValueService;
import com.cep.spring.model.WorkValue.WorkValueUserProfile;
import com.cep.spring.model.WorkValue.WorkValueUserTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WorkValueDAOImpl implements WorkValueDAO {

    private final WorkValueService service;

    @Autowired
    public WorkValueDAOImpl(WorkValueService service) {
        this.service = service;
    }

    public int insertTest(WorkValueUserTest model) {
        return service.insertTest(model);
    }

    public WorkValueUserProfile getProfile(Integer userId) {
        return service.getProfile(userId);
    }

}
