package com.cep.spring.dao.mybatis.service;

import com.cep.spring.dao.mybatis.mapper.ClassroomActivityMapper;
import com.cep.spring.model.ClassroomActivity.ClassroomActivity;
import com.cep.spring.model.ClassroomActivity.ClassroomActivityCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassroomActivityService {
    private final ClassroomActivityMapper mapper;

    @Autowired
    public ClassroomActivityService(ClassroomActivityMapper mapper) {
        this.mapper = mapper;
    }

    public List<ClassroomActivityCategory> getClassroomActivityCategories () {
        return mapper.getClassroomActivityCategories();
    }

    public List<ClassroomActivity> getClassroomActivities(String role) {
        return mapper.getClassroomActivities(role);
    }
}
