package com.cep.spring.model;

import java.io.Serializable;

public class HomepageImage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3174084274433517075L;
	private Integer imgId;
	private String imgName;
	private boolean isActive;
	
	public void setImgId(Integer imgId) { this.imgId = imgId; }
	public int getImgId() { return imgId; }
	
	public void setImgName(String imgName) { this.imgName = imgName; }
	public String getImgName() { return imgName; }
	
	public void setIsActive(boolean isActive) { this.isActive = isActive; }
	public boolean getIsActive() { return isActive; }
}