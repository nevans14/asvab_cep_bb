package com.cep.spring.model.resources;

import java.io.Serializable;

public class ResourceType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8155051701878910413L;
	private Short resourceTypeId;
	private String resourceTypeTitle;	
	// accessors
	public Short getResourceTypeId() { return resourceTypeId; }
	public String getResourceTypeTitle() { return resourceTypeTitle; }
	// mutators
	public void setResourceTypeId(Short resourceTypeId) { this.resourceTypeId = resourceTypeId; }
	public void setResourceTypeTitle(String resourceTypeTitle) { this.resourceTypeTitle = resourceTypeTitle; }
}