//package com.cep.spring.utils;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.util.StringUtils;
//import org.springframework.web.context.support.SpringBeanAutowiringSupport;
//import org.springframework.web.context.support.WebApplicationContextUtils;
//
//import com.cep.spring.dao.mybatis.impl.MediaCenterDAOImpl;
//import com.cep.spring.dao.mybatis.impl.OccufindDAOImpl;
//import com.cep.spring.model.media.MediaCenter;
//import com.cep.spring.model.occufind.TitleDescription;
//
//public class CrawlersRedirect extends HttpServlet {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1328977028542486L;
//
//	private Logger logger = LogManager.getLogger(this.getClass().getName());
//
//	@Autowired
//	private MediaCenterDAOImpl mediaDao;
//	
//	@Autowired
//	private OccufindDAOImpl occufindDao;
//
//	public void init(ServletConfig config) throws ServletException {
//		super.init(config);
//		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
//	}
//
//	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//		
//		String userAgent = request.getHeader("user-agent");
//
//		// Full URL.
//		String url = request.getRequestURL().toString();
//
//		if (!StringUtils.isEmpty(userAgent)) {
//			
//			// Google adwords.
//			if (userAgent.toLowerCase().contains("adsbot") || userAgent.toLowerCase().contains("mediapartners")) {
//				response.sendError(307);
//			}
//
//			// Checks to see if the user is a crawler.
//			if (userAgent.toLowerCase().contains("facebookexternalhit")
//					|| userAgent.toLowerCase().contains("twitterbot")
//					|| userAgent.toLowerCase().contains("linkedinbot")) {
//
//				logger.debug(userAgent + ": This is a bot");
//
//				// Crawler is trying to access media center article pages.
//				if (request.getRequestURI().toLowerCase().contains("media-center-article")) {
//
//					// Get the media ID from the URL.
//					String mediaId = url.replaceAll(".*/(\\d{1,5}).*", "$1");
//					MediaCenter article = new MediaCenter();
//
//					try {
//						article = mediaDao.getArticleById(Integer.valueOf(mediaId));
//					} catch (Exception e) {
//						logger.error(e);
//					}
//
//					// Create and serve HTML page to crawler with all meta tags.
//					makeMediaCenterBotPage(response, article, url);
//				}
//
//				// Crawler is trying to access Occufind details page.
//				if (request.getRequestURI().toLowerCase().contains("occufind-occupation-details")) {
//
//					// Get the SOC ID from the URL.
//					String socId = url.replaceAll(".*/(\\d{1,2}-\\d+.\\d+).*", "$1");
//					TitleDescription occupation = new TitleDescription();
//
//					try {
//						occupation = occufindDao.getOccupationTitleDescription(socId);
//					} catch (Exception e) {
//						logger.error(e);
//					}
//
//					// Create and serve HTML page to crawler with all meta tags.
//					makeOccufindBotPage(request, response, occupation, url, socId);
//				}
//
//				// Crawler is trying to access Find Your Interest score page.
//				if (request.getRequestURI().toLowerCase().contains("find-your-interest-score")) {
//
//					String interestCodes = url.replaceAll(".*=(.*)?.*", "$1");
//					// TODO: Implement dynamic social media image share.
//
//					// Create and serve HTML page to crawler with all meta tags.
//					if (!userAgent.toLowerCase().contains("facebookexternalhit")){
//						makeFYIBotPage(request, response, interestCodes);
//					}
//					
//				}
//
//			} else {
//				response.sendError(308);
//				//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//				//throw new ServletException("Not Crawler.");
//			}
//
//		} else {
//			response.sendError(308);
//			//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//			//throw new ServletException("Not Crawler.");
//		}
//
//	}
//
//
//	/**
//	 * Creates and returns crawler friendly HTML page for media center articles
//	 * for social sharing.
//	 * 
//	 * @param response
//	 * @param article
//	 * @param url
//	 * @throws IOException
//	 */
//	private void makeMediaCenterBotPage(HttpServletResponse response, MediaCenter article, String url)
//			throws IOException {
//		PrintWriter out = response.getWriter();
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"UTF-8\" />");
//		out.println("<meta property=\"fb:app_id\" content=\"2055205968038605\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:title\" content=\"" + article.getTitle() + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:description\" content=\"" + article.getArticleDescription() + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:url\" content=\"" + url + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:image\" content=\"" + article.getShareImgUrl() + "\" />");
//		out.println("<meta property=\"og:image:width\" content=\"1200\" />");
//		out.println("<meta property=\"og:image:height\" content=\"630\" />");
//		out.println("<meta property=\"og:title\" content=\"" + article.getTitle() + "\" />");
//		out.println("<meta property=\"og:image\" content=\"" + article.getShareImgUrl() + "\" />");
//		out.println("<meta property=\"og:description\" content=\"" + article.getArticleDescription() + "\" />");
//		out.println("<meta property=\"og:url\" content=\"" + url + "\" />");
//		out.println("<meta name=\"twitter:card\" content=\"summary_large_image\" />");
//		out.println("<meta name=\"twitter:site\" content=\"@asvabcep\" />");
//		out.println("<meta name=\"twitter:title\" content=\"" + article.getTitle() + "\" />");
//		out.println("<meta name=\"twitter:description\" content=\"" + article.getArticleDescription() + "\" />");
//		out.println("<meta name=\"twitter:image\" content=\"" + article.getShareImgUrl() + "\" />");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("<h1>Crawler Page</h1>");
//		out.println("</body>");
//		out.println("</html>");
//	}
//
//	/**
//	 * Creates and returns crawler friendly HTML page for occufind sharing.
//	 * 
//	 * @param request
//	 * @param response
//	 * @param occupation
//	 * @param url
//	 * @param socId
//	 * @throws IOException
//	 */
//	private void makeOccufindBotPage(HttpServletRequest request, HttpServletResponse response,
//			TitleDescription occupation, String url, String socId) throws IOException {
//
//		String scheme = request.getScheme();
//		// Host URL will be production since Twitter and Linkedin don't work
//		// well with Dev environment.
//		String hostURL = scheme + "://www.asvabprogram.com";
//
//		PrintWriter out = response.getWriter();
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"UTF-8\" />");
//		out.println("<meta property=\"fb:app_id\" content=\"2055205968038605\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:title\" content=\"" + occupation.getTitle() + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:description\" content=\"" + occupation.getDescription() + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:url\" content=\"" + hostURL + "/occufind-occupation-details/" + socId + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:image\" content=\"" + hostURL + "/static/" + socId + "_IMG.jpg" + "\" />");
//		out.println("<meta property=\"og:image:width\" content=\"276\" />");
//		out.println("<meta property=\"og:image:height\" content=\"182\" />");
//		out.println("<meta property=\"og:title\" content=\"" + occupation.getTitle()  + "\" />");
//		out.println("<meta property=\"og:image\" content=\"" + hostURL + "/static/" + socId + "_IMG.jpg" + "\" />");
//		out.println("<meta property=\"og:description\" content=\"" + occupation.getDescription() + "\" />");
//		out.println("<meta property=\"og:url\" content=\"" + hostURL + "/occufind-occupation-details/" + socId + "\" />");
//		out.println("<meta name=\"twitter:card\" content=\"summary_large_image\" />");
//		out.println("<meta name=\"twitter:site\" content=\"@asvabcep\" />");
//		out.println("<meta name=\"twitter:title\" content=\"" + occupation.getTitle() + "\" />");
//		out.println("<meta name=\"twitter:description\" content=\"" + occupation.getDescription() + "\" />");
//		out.println("<meta name=\"twitter:image\" content=\"" + hostURL + "/static/" + socId + "_IMG.jpg" + "\" />");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("<h1>Crawler Page</h1>");
//		out.println("</body>");
//		out.println("</html>");
//	}
//
//	/**
//	 * Creates and returns crawler friendly HTML page for occufind sharing.
//	 * 
//	 * @param request
//	 * @param response
//	 * @param occupation
//	 * @param url
//	 * @param socId
//	 * @throws IOException
//	 */
//	private void makeFYIBotPage(HttpServletRequest request, HttpServletResponse response, String interestCodes) throws IOException {
//
//		String scheme = request.getScheme();
//		// Host URL will be production since Twitter and Linkedin don't work
//		// well with Dev environment.
//		String hostURL = scheme + "://www.asvabprogram.com";
//		//String hostURL = scheme + "://cep-compta.humrro.org:8080";
//		
//		String image = hostURL + "/media-center-content/thumbnails/social-share/fyi-score/FYI_facebook_" + interestCodes + ".jpg";
//		PrintWriter out = response.getWriter();
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"UTF-8\"/>");
//		out.println("<meta property=\"fb:app_id\" content=\"2055205968038605\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:title\" content=\"My FYI Results" + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:image\" content=\"" + image + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:image:width\" content=\"1200\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:image:height\" content=\"627\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:url\" content=\"" + hostURL + "/media-center-article/50\" />");
//		out.println("<meta property=\"og:title\" content=\"My FYI Results" + "\" />");
//		out.println("<meta property=\"og:image\" content=\"" + image + "\" />");
//		out.println("<meta property=\"og:description\" content=\"" + "\" />");
//		out.println("<meta property=\"og:url\" content=\"" + hostURL + "\" />");
//		out.println("<meta name=\"twitter:card\" content=\"summary_large_image\" />");
//		out.println("<meta name=\"twitter:site\" content=\"@asvabcep\" />");
//		out.println("<meta name=\"twitter:title\" content=\"My FYI Results" + "\" />");
//		out.println("<meta name=\"twitter:description\" content=\"" + "\" />");
//		out.println("<meta name=\"twitter:image\" content=\"" + image + "\" />");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("<h1>Crawler Page</h1>");
//		out.println("</body>");
//		out.println("</html>");
//	}
//	
//	/**
//	 * Creates and returns crawler friendly HTML page for bring asvab cep to your school.
//	 * 
//	 * @param request
//	 * @param response
//	 * @param occupation
//	 * @param url
//	 * @param socId
//	 * @throws IOException
//	 */
//	private void makeAsvabCEPBotPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
//
//		String scheme = request.getScheme();
//		// Host URL will be production since Twitter and Linkedin don't work
//		// well with Dev environment.
//		String hostURL = scheme + "://www.asvabprogram.com";
//		//String hostURL = scheme + "://cep-compta.humrro.org:8080";
//		
//		String image = hostURL + "/media-center-content/thumbnails/social-share/bring-asvab-to-your-school.jpg";
//		PrintWriter out = response.getWriter();
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"UTF-8\">");
//		out.println("<meta property=\"fb:app_id\" content=\"2055205968038605\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:title\" content=\"Bring the ASVAB CEP to Your School" + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:description\" content=\"If your school needs a career exploration program that allows students to explore all of their post-secondary options, it's easy to sign up and free to participate." + "\" />");
//		out.println("<meta prefix=\"og: http://ogp.me/ns#\" property=\"og:image\" content=\"" + image + "\" />");
//		out.println("<meta property=\"og:image:width\" content=\"1200\" />");
//		out.println("<meta property=\"og:image:height\" content=\"627\" />");
//		out.println("<meta property=\"og:title\" content=\"Bring the ASVAB CEP to Your School" + "\" />");
//		out.println("<meta property=\"og:image\" content=\"" + image + "\" />");
//		out.println("<meta property=\"og:description\" content=\"If your school needs a career exploration program that allows students to explore all of their post-secondary options, it's easy to sign up and free to participate.\" />");
//		out.println("<meta property=\"og:url\" content=\"" + hostURL + "\" />");
//		out.println("<meta name=\"twitter:card\" content=\"summary_large_image\" />");
//		out.println("<meta name=\"twitter:site\" content=\"@asvabcep\" />");
//		out.println("<meta name=\"twitter:title\" content=\"Bring the ASVAB CEP to Your School" + "\" />");
//		out.println("<meta name=\"twitter:description\" content=\"If your school needs a career exploration program that allows students to explore all of their post-secondary options, it's easy to sign up and free to participate.\" />");
//		out.println("<meta name=\"twitter:image\" content=\"" + image + "\" />");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("<h1>Crawler Page</h1>");
//		out.println("</body>");
//		out.println("</html>");
//	}
//
//}
