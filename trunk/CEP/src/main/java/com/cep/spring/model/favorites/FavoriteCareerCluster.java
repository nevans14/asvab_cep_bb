package com.cep.spring.model.favorites;

import java.io.Serializable;

public class FavoriteCareerCluster implements Serializable {

	private static final long serialVersionUID = -3247331560784769158L;
	private Integer id;
	private Integer ccId;
	private String title;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCcId() {
		return ccId;
	}

	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
