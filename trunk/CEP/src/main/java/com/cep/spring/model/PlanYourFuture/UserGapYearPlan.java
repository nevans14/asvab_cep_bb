package com.cep.spring.model.PlanYourFuture;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;
import java.util.ArrayList;

public class UserGapYearPlan implements Serializable {
    private static final long serialVersionUID = -8674729247335383433L;

    private int id, planId;
    private Integer gapYearId; // can be nullable
    private String planName, fundingSource, goals;
    private Double programCost, costOfLiving, estimatedAmount, totalAnticipatedWages;
    private Boolean isVisaRequired, isVaccinationsRequired;
    private ArrayList<UserGapYearPlanType> types;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public Integer getGapYearId() {
        return gapYearId;
    }

    public void setGapYearId(Integer gapYearId) {
        this.gapYearId = gapYearId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        if (planName != null && !planName.isEmpty()) {
            this.planName = Jsoup.clean(planName, Whitelist.basic());
        }
    }

    public String getFundingSource() {
        return fundingSource;
    }

    public void setFundingSource(String fundingSource) {
        if (fundingSource != null && !fundingSource.isEmpty()) {
            this.fundingSource = Jsoup.clean(fundingSource, Whitelist.basic());
        }
    }

    public String getGoals() { return goals; }

    public void setGoals(String goals) {
        if (goals != null && !goals.isEmpty()) {
            this.goals = Jsoup.clean(goals, Whitelist.basic());
        }
    }

    public Double getProgramCost() {
        return programCost;
    }

    public void setProgramCost(Double programCost) {
        this.programCost = programCost;
    }

    public Double getCostOfLiving() {
        return costOfLiving;
    }

    public void setCostOfLiving(Double costOfLiving) {
        this.costOfLiving = costOfLiving;
    }

    public Double getEstimatedAmount() { return estimatedAmount; }

    public void setEstimatedAmount(Double estimatedAmount) { this.estimatedAmount = estimatedAmount; }

    public Double getTotalAnticipatedWages() { return totalAnticipatedWages; }

    public void setTotalAnticipatedWages(Double totalAnticipatedWages) { this.totalAnticipatedWages = totalAnticipatedWages; }

    public Boolean getIsVisaRequired() { return isVisaRequired; }

    public void setIsVisaRequired(Boolean isVisaRequired) { this.isVisaRequired = isVisaRequired; }

    public Boolean getIsVaccinationsRequired() { return isVaccinationsRequired; }

    public void setIsVaccinationsRequired(Boolean isVaccinationsRequired) { this.isVaccinationsRequired = isVaccinationsRequired; }

    public ArrayList<UserGapYearPlanType> getTypes() { return types; }

    public void setTypes(ArrayList<UserGapYearPlanType> types) { this.types = types; }
}
