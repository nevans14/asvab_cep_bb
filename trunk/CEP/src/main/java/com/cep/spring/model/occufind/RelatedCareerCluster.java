package com.cep.spring.model.occufind;

import java.io.Serializable;

public class RelatedCareerCluster implements Serializable {

	private static final long serialVersionUID = 541622428792111862L;

	private String careerClusterName;
	private Integer ccId;

	public Integer getCcId() {
		return ccId;
	}

	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}

	public String getCareerClusterName() {
		return careerClusterName;
	}

	public void setCareerClusterName(String careerClusterName) {
		this.careerClusterName = careerClusterName;
	}
}
