package com.cep.spring.model;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class Registration {
	
	private String email, subject, accessCode, password, resetKey, studentId, firstName, lastName, phoneNumber, functionOther, schoolName, schoolZipCode, schoolState, essName, eventName;
	private Integer functionId, conferenceId;
	private String recaptchaResponse;
	private boolean optedInCommunications = false;
	private boolean updateFYI = false;
	private boolean updateFavorates = false;
	private boolean updateProfile = false;
	
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = Jsoup.clean(studentId, Whitelist.basic());
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = Jsoup.clean(email, Whitelist.basic());
	}

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = Jsoup.clean(subject, Whitelist.basic());
	}

	public String getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(String accessCode) {
		this.accessCode = Jsoup.clean(accessCode, Whitelist.basic());
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = Jsoup.clean(password, Whitelist.basic());
	}

	public String getResetKey() {
		return resetKey;
	}
	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}

	public boolean isOptedInCommunications() {
		return optedInCommunications;
	}	
	public void setOptedInCommunications(boolean optedInCommunications) { this.optedInCommunications = optedInCommunications; }

	public boolean isUpdateFYI() {
		return updateFYI;
	}
	public void setUpdateFYI(boolean updateFYI) {
		this.updateFYI = updateFYI;
	}

	public boolean isUpdateFavorates() {
		return updateFavorates;
	}
	public void setUpdateFavorates(boolean updateFavorates) {
		this.updateFavorates = updateFavorates;
	}

	public boolean isUpdateProfile() {
		return updateProfile;
	}
	public void setUpdateProfile(boolean updateProfile) {
		this.updateProfile = updateProfile;
	}

	public String getRecaptchaResponse() {
		return recaptchaResponse;
	}
	public void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse;
	}

	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) {
		this.firstName = Jsoup.clean(firstName, Whitelist.basic());
	}

	public String getLastName() { return lastName; }
	public void setLastName(String lastName) {
		this.lastName = Jsoup.clean(lastName, Whitelist.basic());
	}

	public String getPhoneNumber() { return phoneNumber; }
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = Jsoup.clean(phoneNumber, Whitelist.basic());
	}

	public String getFunctionOther() { return functionOther; }
	public void setFunctionOther(String functionOther) {
		this.functionOther = Jsoup.clean(functionOther, Whitelist.basic());
	}

	public String getSchoolName() { return schoolName; }
	public void setSchoolName(String schoolName) {
		this.schoolName = Jsoup.clean(schoolName, Whitelist.basic());
	}

	public String getSchoolZipCode() { return schoolZipCode; }
	public void setSchoolZipCode(String schoolZipCode) {
		this.schoolZipCode = Jsoup.clean(schoolZipCode, Whitelist.basic());
	}

	public String getSchoolState() { return schoolState; }
	public void setSchoolState(String schoolState) {
		this.schoolState = Jsoup.clean(schoolState, Whitelist.basic());
	}

	public String getEssName() { return essName; }
	public void setEssName(String essName) {
		this.essName = Jsoup.clean(essName, Whitelist.basic());
	}

	public String getEventName() { return eventName; }
	public void setEventName(String eventName) {
		this.eventName = Jsoup.clean(eventName, Whitelist.basic());
	}

	public Integer getFunctionId() { return functionId; }
	public void setFunctionId(Integer functionId) { this.functionId = functionId; }

	public Integer getConferenceId() { return conferenceId; }
	public void setConferenceId(Integer conferenceId) { this.conferenceId = conferenceId; }
}
