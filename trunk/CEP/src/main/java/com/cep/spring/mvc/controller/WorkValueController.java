package com.cep.spring.mvc.controller;

import com.cep.spring.dao.mybatis.WorkValueDAO;
import com.cep.spring.model.WorkValue.WorkValueUserProfile;
import com.cep.spring.model.WorkValue.WorkValueUserTest;
import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/work-values")
public class WorkValueController {

    private final Logger logger = LogManager.getLogger();
    private final WorkValueDAO dao;
    private final UserManager userManager;

    @Autowired
    public WorkValueController(WorkValueDAO dao, UserManager userManager) {
        this.dao = dao;
        this.userManager = userManager;
    }

    @RequestMapping(value = "/save-test", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Integer> saveTest() {
        int result;
        WorkValueUserTest model = new WorkValueUserTest();
        model.setUserId(userManager.getUserId());
        try {
            result = dao.insertTest(model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model.getTestId(), HttpStatus.OK);
    }

    @RequestMapping(value = "/get-profile", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<WorkValueUserProfile> getProfile() {
        WorkValueUserProfile result;
        try {
            result = dao.getProfile(userManager.getUserId());
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
