package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.service.NotesService;
import com.cep.spring.model.notes.CareerClusterNotes;
import com.cep.spring.model.notes.Notes;
import com.cep.spring.model.notes.SchoolNotes;

@Repository
public class NotesDAOImpl {

	private final NotesService service;

	@Autowired
	public NotesDAOImpl(NotesService service) {
		this.service = service;
	}

	public ArrayList<Notes> getNotes(Integer userId) {
		return service.getNotes(userId);
	}

	public int insertNote(Notes noteObject, Integer userId) {
		return service.insertNote(noteObject, userId);
	}

	public int updateNote(Notes noteObject, Integer userId) {
		return service.updateNote(noteObject, userId);
	}
	
	public int deleteNote(Integer userId, Integer noteId) {
		return service.deleteNote(userId, noteId);
	}

	public String getOccupationNotes(Integer userId, String socId) {
		return service.getOccupationNotes(userId, socId);
	}

	public ArrayList<CareerClusterNotes> getCareerClusterNotes(Integer userId) {
		return service.getCareerClusterNotes(userId);
	}

	public int insertCareerClusterNote(CareerClusterNotes noteObject, Integer userId) {
		return service.insertCareerClusterNote(noteObject, userId);
	}

	public int updateCareerClusterNote(CareerClusterNotes noteObject, Integer userId) {
		return service.updateCareerClusterNote(noteObject, userId);
	}
	
	public int deleteCareerClusterNote(Integer userId, Integer ccNoteId) {
		return service.deleteCareerClusterNote(userId, ccNoteId);
	}

	public String getCareerClusterNote(Integer userId, Integer ccId) {
		return service.getCareerClusterNote(userId, ccId);
	}
	
	public ArrayList<SchoolNotes> getSchoolNotes(Integer userId) {
		return service.getSchoolNotes(userId);
	}

	public int insertSchoolNote(SchoolNotes noteObject, Integer userId) {
		return service.insertSchoolNote(noteObject, userId);
	}

	public int updateSchoolNote(SchoolNotes noteObject, Integer userId) {
		return service.updateSchoolNote(noteObject, userId);
	}
	
	public int deleteSchoolNote(Integer userId, Integer schoolNoteId) {
		return service.deleteSchoolNote(userId, schoolNoteId);
	}

	public String getSchoolNote(Integer userId, Integer unitId) {
		return service.getSchoolNote(userId, unitId);
	}

}
