package com.cep.spring.dao.mybatis;

import com.cep.spring.model.WorkValue.WorkValueUserProfile;
import com.cep.spring.model.WorkValue.WorkValueUserTest;

public interface WorkValueDAO {

    int insertTest(WorkValueUserTest model);

    WorkValueUserProfile getProfile(Integer userId);

}
