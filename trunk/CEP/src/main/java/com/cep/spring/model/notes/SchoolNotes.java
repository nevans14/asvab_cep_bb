package com.cep.spring.model.notes;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class SchoolNotes implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer noteId, unitId;
	private String notes, schoolName;

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = Jsoup.clean(schoolName, Whitelist.basic());
	}

	public Integer getNoteId() {
		return noteId;
	}

	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = Jsoup.clean(notes, Whitelist.basic());
	}
}
