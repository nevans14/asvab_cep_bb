package com.cep.spring.model.PlanYourFuture;

import java.io.Serializable;

public class UserMilitaryPlanRotc implements Serializable {
    private static final long serialVersionUID = 1481118214537392394L;
    private int id, militaryPlanId, unitId, collegePlanId;
    private boolean wantsToApply;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMilitaryPlanId() {
        return militaryPlanId;
    }

    public void setMilitaryPlanId(int militaryPlanId) {
        this.militaryPlanId = militaryPlanId;
    }

    public int getCollegePlanId() {
        return collegePlanId;
    }

    public void setCollegePlanId(int collegePlanId) {
        this.collegePlanId = collegePlanId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public boolean getWantsToApply() {
        return wantsToApply;
    }

    public void setWantsToApply(boolean wantsToApply) {
        this.wantsToApply = wantsToApply;
    }
}
