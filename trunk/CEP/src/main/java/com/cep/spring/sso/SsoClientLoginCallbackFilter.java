package com.cep.spring.sso;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.IllegalFormatException;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cep.spring.utils.Security;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.session.ExpiringSession;
import org.springframework.session.SessionRepository;
import org.springframework.web.filter.OncePerRequestFilter;

import com.cep.spring.utils.CookieHelper;

/**
 * This class utilizes Redis and Spring Session to share httpsession between
 * CEP and CITM. When user login CEP, they are redirected here to set the
 * session and create a cookie for Angular. This addresses cross domain
 * SSO.
 *
 */
@PropertySource(value = {"classpath:SsoProperties.properties", "classpath:application.properties"})
public class SsoClientLoginCallbackFilter extends OncePerRequestFilter {

	private final Logger logger = LogManager.getLogger();
	/*Redirection Urls*/
	@Value("${citm.url}")
	private String ssoUrl;
	@Value("${citm.frontEndUrl}")
	private String citmFrontEndUrl;
	/*Domains for Cookies*/
	@Value("${cep.domain}")
	private String cepDomain;
	@Value("${security.cookie.secure}")
	private boolean isSecure;

	private final SessionRepository<ExpiringSession> sessionRepository;
	private final RequestCache requestCache;
	private final Security security;


	@SuppressWarnings("unchecked")
	public <S extends ExpiringSession> SsoClientLoginCallbackFilter(SessionRepository<S> sessionRepository,
			RequestCache requestCache,
			Security security) {
		this.sessionRepository = (SessionRepository<ExpiringSession>) sessionRepository;
		this.requestCache = requestCache;
		this.security = security;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
		final String TOKEN_PARAM = "token";
		final String SALT_PARAM = "salt";
		final int MAX_AGE = -1;
		final String PATH = "/";
		final String SERVER_PATH = "/CEP/";
		final String GLOBALS = "globals";
		final String SESSION = "SESSION";
		RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
		RequestMatcher requestMatcher = new AntPathRequestMatcher("/login_callback/test");
		try {
			if (requestMatcher.matches(request)) {
				String token = request.getParameter(TOKEN_PARAM);
				String salt = request.getParameter(SALT_PARAM);
				if (token != null && salt != null) {
					byte[] iv = security.getBytes(salt);
					String sessionId = security.decrypt(token, iv);
					ExpiringSession session = sessionRepository.getSession(sessionId);
					SecurityContext securityContext = session.getAttribute("SPRING_SECURITY_CONTEXT");
					String role = session.getAttribute("USER_ROLE");
					if (securityContext != null && securityContext.getAuthentication().isAuthenticated()) {
						// create encoded string for the angular apps
						String jsonString = String.format(Locale.ENGLISH, "{\"currentUser\":{\"username\":\"null\",\"role\":\"%s\",\"authdata\":\"null==\",\"isRegistered\":\"true\"}}", role);
						String authData = URLEncoder.encode(jsonString, "UTF-8");
						// Creates a cookie for CEP
						response.addCookie(CookieHelper.addCookie(
								SESSION,
								sessionId,
								ssoUrl.contains("localhost") ? null : cepDomain,
								SERVER_PATH,
								MAX_AGE,
								isSecure,
								true));
						response.addCookie(CookieHelper.addCookie(
								GLOBALS,
								authData,
								ssoUrl.contains("localhost") ? null : cepDomain,
								PATH,
								MAX_AGE,
								isSecure,
								false));
						SavedRequest savedRequest = requestCache.getRequest(request, response);
						if (savedRequest != null) {
							logger.debug("re-enforcing cached request at {} {}", savedRequest.getRedirectUrl(),
									savedRequest.getMethod());
							session.setAttribute("SPRING_SECURITY_SAVED_REQUEST", savedRequest);
						}
						sessionRepository.save(session);
					}
				}
			}
		} catch (IllegalFormatException e) {
			logger.error("IllegalFormat Error (SSO Failed)", e);
		} catch (IOException e) {
			logger.error("IO Error (SSO Failed)", e);
		} finally {
			try {
				// if on dev, redirect to the front end, instead of the backend
				if (!ssoUrl.contains("localhost")) {
					redirectStrategy.sendRedirect(request, response, citmFrontEndUrl + "profile");
				} else {
					redirectStrategy.sendRedirect(request, response, ssoUrl + "profile");
				}
			} catch (IOException e) {
				logger.error("IO Error - Failed to Redirect Back to CTM");
			}
		}
	}
}
