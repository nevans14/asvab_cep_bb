package com.cep.spring.model.occufind;

import java.io.Serializable;

public class CertificateExplanation implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7671939947515650542L;

	private String licensing;
	
	private String training;
	
	public String getLicensing() {
		return licensing;
	}

	public void setLicensing(String licensing) {
		this.licensing = licensing;
	}

	public String getTraining() {
		return training;
	}

	public void setTraining(String training) {
		this.training = training;
	}
}
