package com.cep.spring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


/**
 * Used by spring filters to determine if user has been authenticated.  If this
 *  is not implemented the browser will pop up a user dialog for credentials.
 *  
 *  Note:  The actual decision to validate the user is done in the 
 *  LoginRegistrationController, if user has supplied sufficient credentials then
 *  the Spring Security Context is supplied with the appropriate 
 *  authenticationToken.  This method is utilized by the security filter to 
 *  determine if the request is from an authenticated user, and does this user 
 *  have the appropriate role(s).
 *  
 *  
 *  
 * @author Dave Springer
 *
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{

    private final Logger logger = LogManager.getLogger();

	/**
	 * 
	 */
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	      logger.trace("spring custom authentication ");
		  String username = authentication.getName();
	      String password = (String) authentication.getCredentials();
	      if ( null == SecurityContextHolder.getContext().getAuthentication() ) return null;
		  return new UsernamePasswordAuthenticationToken(
		  		username,
				password,
				SecurityContextHolder.getContext().getAuthentication().getAuthorities());
	}

	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
