package com.cep.spring.dao.mybatis.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.ResourceDAO;
import com.cep.spring.dao.mybatis.service.ResourceService;
import com.cep.spring.model.resources.ResourceQuickLinkTopic;
import com.cep.spring.model.resources.ResourceTopic;

@Repository
public class ResourceDAOImpl implements ResourceDAO {

	private final ResourceService service;

	@Autowired
	public ResourceDAOImpl(ResourceService service) {
		this.service = service;
	}
	
	/***
	 * Returns all resources
	 * @return all resources
	 */
	public List<ResourceTopic> getResources() { return service.getResources(); }
	
	/***
	 * Returns all quick links
	 * @return all quick links
	 */
	public List<ResourceQuickLinkTopic> getQuickLinks() { return service.getQuickLinks(); }
	
}