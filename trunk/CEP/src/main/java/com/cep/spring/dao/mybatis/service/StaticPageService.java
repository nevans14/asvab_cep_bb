package com.cep.spring.dao.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.StaticPageMapper;
import com.cep.spring.model.StaticPage;

@Service
public class StaticPageService {

	private final StaticPageMapper mapper;

	@Autowired
	public StaticPageService(StaticPageMapper mapper) {
		this.mapper = mapper;
	}
	
	public StaticPage getPageById(int pageId) {
		return mapper.getPageById(pageId);
	}
	
	public StaticPage getPageByPageNameOld(String pageName) {
		return mapper.getPageByPageNameOld(pageName);
	}
	
    public StaticPage getPageByPageName(String pageName, String website) {
        return mapper.getPageByPageName(pageName, website);
    }
}