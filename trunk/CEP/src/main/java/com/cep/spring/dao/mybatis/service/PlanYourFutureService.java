package com.cep.spring.dao.mybatis.service;

import com.cep.spring.dao.mybatis.mapper.PlanYourFutureMapper;
import com.cep.spring.model.PlanYourFuture.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PlanYourFutureService {

    private final PlanYourFutureMapper mapper;

    @Autowired
    public PlanYourFutureService(PlanYourFutureMapper mapper) {
        this.mapper = mapper;
    }

    public ArrayList<UserOccupationPlan> selectOccupationPlans(int userId) {
        return mapper.selectOccupationPlans(userId);
    }

    public UserOccupationPlan selectOccupationPlan(int userId, int id) {
        return mapper.selectOccupationPlan(userId, id);
    }

    public int insertOccupationPlan(int userId, UserOccupationPlan model) {
        return mapper.insertOccupationPlan(userId, model);
    }

    public int updateOccupationPlan(int userId, int id, UserOccupationPlan model) {
        return mapper.updateOccupationPlan(userId, id, model);
    }

    public int deleteOccupationPlan(int userId, int id) {
        return mapper.deleteOccupationPlan(userId, id);
    }

    public ArrayList<UserOccupationPlanCalendar> selectPlanCalendars(int userId, int planId) {
        return mapper.selectPlanCalendars(userId, planId);
    }

    public ArrayList<UserOccupationPlanCalendar> selectCalendars(int userId) {
        return mapper.selectCalendars(userId);
    }

    public UserOccupationPlanCalendar selectGraduationDate(int userId) { return mapper.selectGraduationDate(userId); }

    public int insertCalendar(int userId, Integer planId, UserOccupationPlanCalendar model) {
        return mapper.insertCalendar(userId, planId, model);
    }

    public int updateCalendar(int userId, Integer planId, int id, UserOccupationPlanCalendar model) {
        return mapper.updateCalendar(userId, planId, id, model);
    }

    public int deleteCalendar(int userId, Integer planId, int id) {
        return mapper.deleteCalendar(userId, planId, id);
    }

    public ArrayList<UserOccupationPlanCalendarTask> selectPlanTasks(int userId, int planId) {
        return mapper.selectPlanTasks(userId, planId);
    }

    public ArrayList<UserOccupationPlanCalendarTask> selectTasks(int userId) {
        return mapper.selectTasks(userId);
    }

    public int insertTask(int userId, Integer planId, UserOccupationPlanCalendarTask model) {
        return mapper.insertTask(userId, planId, model);
    }

    public int updateTask(int userId, Integer planId, int id, UserOccupationPlanCalendarTask model) {
        return mapper.updateTask(userId, planId, id, model);
    }

    public int deleteTask(int userId, Integer planId, int id) {
        return mapper.deleteTask(userId, planId, id);
    }

    public UserSchoolPlan selectSchoolPlan(int userId, int planId) {
        return mapper.selectSchoolPlan(userId, planId);
    }

    public int insertSchoolPlan(int userId, int planId, UserSchoolPlan model) {
        return mapper.insertSchoolPlan(userId, planId, model);
    }

    public int updateSchoolPlan(int userId, int planId, UserSchoolPlan model) {
        return mapper.updateSchoolPlan(userId, planId, model);
    }

    public ArrayList<UserSchoolPlanCourse> selectSchoolCourses(int userId, int planId) {
        return mapper.selectSchoolCourses(userId, planId);
    }

    public int insertSchoolCourse(int userId, int planId, UserSchoolPlanCourse model) {
        return mapper.insertSchoolCourse(userId, planId, model);
    }

    public int updateSchoolCourse(int userId, int planId, int id, UserSchoolPlanCourse model) {
        return mapper.updateSchoolCourse(userId, planId, id, model);
    }

    public int deleteSchoolCourse(int userId, int planId, int id) {
        return mapper.deleteSchoolCourse(userId, planId, id);
    }

    public ArrayList<UserSchoolPlanActivity> selectSchoolActivities(int userId, int planId) {
        return mapper.selectSchoolActivities(userId, planId);
    }

    public int insertSchoolActivity(int userId, int planId, UserSchoolPlanActivity model) {
        return mapper.insertSchoolActivity(userId, planId, model);
    }

    public int updateSchoolActivity(int userId, int planId, int id, UserSchoolPlanActivity model) {
        return mapper.updateSchoolActivity(userId, planId, id, model);
    }

    public int deleteSchoolActivity(int userId, int planId, int id) {
        return mapper.deleteSchoolActivity(userId, planId, id);
    }

    public ArrayList<UserCollegePlan> selectCollegePlans(int userId, int planId) {
        return mapper.selectCollegePlans(userId, planId);
    }

    public int insertCollegePlan(int userId, int planId, UserCollegePlan model) {
        return mapper.insertCollegePlan(userId, planId, model);
    }

    public int updateCollegePlan(int userId, int planId, int id, UserCollegePlan model) {
        return mapper.updateCollegePlan(userId, planId, id, model);
    }

    public int deleteCollegePlan(int userId, int planId, int id) {
        return mapper.deleteCollegePlan(userId, planId, id);
    }

    public ArrayList<UserMilitaryPlan> selectMilitaryPlans(int userId, int planId) {
        return mapper.selectMilitaryPlans(userId, planId);
    }

    public int insertMilitaryPlan(int userId, int planId, UserMilitaryPlan model) {
        return mapper.insertMilitaryPlan(userId, planId, model);
    }

    public int updateMilitaryPlan(int userId, int planId, int id, UserMilitaryPlan model) {
        return mapper.updateMilitaryPlan(userId, planId, id, model);
    }

    public int deleteMilitaryPlan(int userId, int planId, int id) {
        return mapper.deleteMilitaryPlan(userId, planId, id);
    }

    public int insertMilitaryPlanCareer(int userId, int militaryPlanId, UserMilitaryPlanCareer model) {
        return mapper.insertMilitaryPlanCareer(userId, militaryPlanId, model);
    }

    public int updateMilitaryPlanCareer(int userId, int militaryPlanId, int id, UserMilitaryPlanCareer model) {
        return mapper.updateMilitaryPlanCareer(userId, militaryPlanId, id, model);
    }

    public int deleteMilitaryPlanCareer(int userId, int militaryPlanId, int id) {
        return mapper.deleteMilitaryPlanCareer(userId, militaryPlanId, id);
    }

    public int insertMilitaryPlanRotc(int userId, int militaryPlanId, UserMilitaryPlanRotc model) {
        return mapper.insertMilitaryPlanRotc(userId, militaryPlanId, model);
    }

    public int updateMilitaryPlanRotc(int userId, int militaryPlanId, int id, UserMilitaryPlanRotc model) {
        return mapper.updateMilitaryPlanRotc(userId, militaryPlanId, id, model);
    }

    public int insertMilitaryPlanServiceCollege(int userId, int militaryPlanId, UserMilitaryPlanServiceCollege model) {
        return mapper.insertMilitaryPlanServiceCollege(userId, militaryPlanId, model);
    }

    public int deleteMilitaryPlanServiceCollege(int userId, int militaryPlanId, int id) {
        return mapper.deleteMilitaryPlanServiceCollege(userId, militaryPlanId, id);
    }

    public UserWorkPlan selectWorkPlan(int userId, int planId) {
        return mapper.selectWorkPlan(userId, planId);
    }

    public int insertWorkPlan(int userId, int planId, UserWorkPlan model) {
        return mapper.insertWorkPlan(userId, planId, model);
    }

    public int updateWorkPlan(int userId, int planId, UserWorkPlan model) {
        return mapper.updateWorkPlan(userId, planId, model);
    }

    public ArrayList<RefGapYearTypes> selectGapYearTypes() { return mapper.selectGapYearTypes(); }

    public ArrayList<RefGapYearPlans> selectGapYearOptions() {
        return mapper.selectGapYearOptions();
    }

    public ArrayList<UserGapYearPlan> selectGapYearPlans(int userId, int planId) {
        return mapper.selectGapYearPlans(userId, planId);
    }

    public int insertGapYearPlan(int userId, int planId, UserGapYearPlan model) {
        return mapper.insertGapYearPlan(userId, planId, model);
    }

    public int insertGapYearPlanTypes(UserGapYearPlan model) {
        return  mapper.insertGapYearPlanTypes(model);
    }

    public int updateGapYearPlan(int userId, int planId, int id, UserGapYearPlan model) {
        return mapper.updateGapYearPlan(userId, planId, id, model);
    }

    public int updateGapYearPlanTypes(int id, UserGapYearPlan model) {
        return mapper.updateGapYearPlanTypes(id, model);
    }

    public int deleteGapYearPlan(int userId, int planId, int id) {
        return mapper.deleteGapYearPlan(userId, planId, id);
    }
}
