package com.cep.spring.mvc.controller;

import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.model.DbInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.cep.spring.dao.mybatis.FlickrApiDAO;
import com.cep.spring.model.optionready.FlickrPhotoInfo;

@RestController
@RequestMapping("/flickr")
public class FlickrServiceController {
	
	private final Logger logger = LogManager.getLogger();
	private final FlickrApiDAO dao;
	private final DbInfoDAO dbInfoDao;

	@Autowired
	public FlickrServiceController(FlickrApiDAO dao, DbInfoDAO dbInfoDao) {
		this.dao = dao;
		this.dbInfoDao = dbInfoDao;
	}
	
	@RequestMapping(value = "/get-public-photos", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<FlickrPhotoInfo>> getFlickrGalleries() {
		// if debugging turned on?
		boolean isDebugging = false;
		DbInfo dbInfoIsDebugging = null;
		try {
			dbInfoIsDebugging = dbInfoDao.getDbInfo("is_debugging_on");
		} catch (Exception e) {
			// if db is offline, that's OK because it's not required to get Flickr Photos
			logger.error("Error", e);
		}
		// check if the db has debugging turned on
		if (dbInfoIsDebugging != null) {
			isDebugging = Boolean.parseBoolean(dbInfoIsDebugging.getValue());
		}
		List<FlickrPhotoInfo> results;
		try {
			results = dao.getPhotoCollection(isDebugging);
		} catch (Exception e) {
			// log error
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}	
}