package com.cep.spring.model.CareerPlan;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class CareerPlanAnswer {
    private Integer userId;
    private String responses;

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public void setResponses(String responses) {
        this.responses = Jsoup.clean(responses, Whitelist.basic());
    }

    public Integer getUserId() {
        return userId;
    }
    public String getResponses() {
        return responses;
    }
}
