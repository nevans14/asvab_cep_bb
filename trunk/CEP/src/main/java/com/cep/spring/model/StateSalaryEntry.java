package com.cep.spring.model;

import java.io.Serializable;

public class StateSalaryEntry implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1521570969855471979L;
	private String occupationCd, state;
	private Integer entrySalary;
	
	public String getOccupationCd() { return occupationCd; }
	public void setOccupationCd(String occupationCd) { this.occupationCd = occupationCd; }
	
	public String getState() { return state; }
	public void setState(String state) { this.state = state; }
	
	public Integer getEntrySalary() { return entrySalary; }
	public void setEntrySalary(Integer entrySalary) { this.entrySalary = entrySalary; }
}