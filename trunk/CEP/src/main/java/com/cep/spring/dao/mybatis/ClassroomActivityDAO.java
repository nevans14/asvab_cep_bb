package com.cep.spring.dao.mybatis;

import com.cep.spring.model.ClassroomActivity.ClassroomActivity;
import com.cep.spring.model.ClassroomActivity.ClassroomActivityCategory;

import java.util.List;

public interface ClassroomActivityDAO {

    List<ClassroomActivityCategory> getClassroomActivityCategories();

    List<ClassroomActivity> getClassroomActivities(String role);

}
