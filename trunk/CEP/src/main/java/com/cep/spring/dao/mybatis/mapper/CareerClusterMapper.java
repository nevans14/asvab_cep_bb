package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.CareerCluster;
import com.cep.spring.model.CareerClusterOccupation;
import com.cep.spring.model.OccupationEducationLevel;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.RelatedOccupation;
import com.cep.spring.model.SkillImportance;
import com.cep.spring.model.Task;
import com.cep.spring.model.occufind.OccufindSearch;

public interface CareerClusterMapper {

	ArrayList<CareerCluster> getCareerCluster();

	ArrayList<CareerClusterOccupation> getCareerClusterOccupation(@Param("ccId") int ccId);

	SkillImportance getSkillImportance(@Param("onetSoc") String onetSoc);

	ArrayList<Task> getTasks(@Param("onetSoc") String onetSoc);

	OccupationInterestCode getOccupationInterestCodes(@Param("onetSoc") String onetSoc);

	ArrayList<OccupationEducationLevel> getEducationLevel(@Param("onetSoc") String onetSoc);

	ArrayList<RelatedOccupation> getRelatedOccupations(@Param("onetSoc") String onetSoc);

	ArrayList<OccufindSearch> getCareerClusterOccupations(@Param("ccId") int ccId);

	CareerCluster getCareerClusterById(@Param("ccId") int ccId);

}
