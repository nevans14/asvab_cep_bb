package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.PortfolioDAO;
import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

@Controller
@RequestMapping("/portfolio")
public class PortfolioController {

    private final Logger logger = LogManager.getLogger();
	private final UserManager userManager;
	private final PortfolioDAO dao;

	@Autowired
	public PortfolioController(PortfolioDAO dao, UserManager userManager) {
		this.dao = dao;
		this.userManager = userManager;
	}

	@RequestMapping(value = "/PortfolioStarted", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> isPortfolioStarted() {
		int results;
		try {
			results = dao.isPortfolioStarted(userManager.getUserId()).size();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/IsRegistered", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Boolean> isRegistered() {
		Boolean results = userManager.isRegistered();

		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/WorkExperience", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<WorkExperience>> getWorkExperience() {
	    ArrayList<WorkExperience> results;
	    try {
	    	results = dao.getWorkExperience(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertWorkExperience", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<WorkExperience> insertWorkExperience(
			@RequestBody WorkExperience workExperienceObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertWorkExperience(workExperienceObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
		    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(workExperienceObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateWorkExperience", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<WorkExperience> updateWorkExperience(
			@RequestBody WorkExperience workExperienceObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateWorkExperience(workExperienceObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(workExperienceObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteWorkExperience/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteWorkExperience(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteWorkExperience(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Education", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Education>> getEducation() {
	    ArrayList<Education> results;
	    try {
	    	results = dao.getEducation(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertEducation", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Education> insertEducation(@RequestBody Education educationObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertEducation(educationObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(educationObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateEducation", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Education> updateEducation(@RequestBody Education educationObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateEducation(educationObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(educationObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteEducation/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteEducation(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteEducation(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Achievement", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Achievement>> getAchievements() {
		ArrayList<Achievement> results;
		try {
			results = dao.getAchievements(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertAchievement", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Achievement> insertAchievement(@RequestBody Achievement achievementObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertAchievement(achievementObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(achievementObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateAchievement", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Achievement> updateAchievement(@RequestBody Achievement achievementObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateAchievement(achievementObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(achievementObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteAchievement/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteAchievement(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteAchievement(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Interest", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Interest>> getInterests() {
	    ArrayList<Interest> results;
	    try {
	    	results = dao.getInterests(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertInterest", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Interest> insertInterest(@RequestBody Interest interestObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertInterest(interestObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(interestObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateInterest", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Interest> updateInterest(@RequestBody Interest interestObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateInterest(interestObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(interestObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteInterest/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteInterest(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteInterest(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Skill", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Skill>> getSkills() {
	    ArrayList<Skill> results;
	    try {
	    	results = dao.getSkills(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertSkill", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Skill> insertSkill(@RequestBody Skill skillObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertSkill(skillObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(skillObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateSkill", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Skill> updateSkill(@RequestBody Skill skillObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateSkill(skillObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(skillObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteSkill/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteSkill(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteSkill(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/WorkValue", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<WorkValue>> getWorkValues() {
	    ArrayList<WorkValue> results;
	    try {
	    	results = dao.getWorkValues(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertWorkValue", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<WorkValue> insertWorkValue(@RequestBody WorkValue workValueObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertWorkValue(workValueObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(workValueObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateWorkValue", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<WorkValue> updateWorkValue(@RequestBody WorkValue workValueObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateWorkValue(workValueObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(workValueObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteWorkValue/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteWorkValue(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteWorkValue(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Volunteer", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Volunteer>> getVolunteers() {
		ArrayList<Volunteer> results;
		try {
			results = dao.getVolunteers(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertVolunteer", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Volunteer> insertVolunteer(@RequestBody Volunteer volunteerObject) {
		int rowsInserted;
		// set user ID and clean inputs
		try {
			rowsInserted = dao.insertVolunteer(volunteerObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(volunteerObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateVolunteer", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Volunteer> updateVolunteer(@RequestBody Volunteer volunteerObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateVolunteer(volunteerObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(volunteerObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteVolunteer/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteVolunteer(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteVolunteer(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/ASVAB", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<AsvabScore>> getAsvabScore() {
	    ArrayList<AsvabScore> results;
	    try {
	    	results = dao.getAsvabScore(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertASVAB", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<AsvabScore> insertAsvabScore(@RequestBody AsvabScore asvabObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertAsvabScore(asvabObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(asvabObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateASVAB", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<AsvabScore> updateAsvabScore(@RequestBody AsvabScore asvabObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateAsvabScore(asvabObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(asvabObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteASVAB/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteAsvabScore(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteAsvabScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/ACT", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<ActScore>> getActScore() {
	    ArrayList<ActScore> results;
	    try {
	    	results = dao.getActScore(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertACT", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<ActScore> insertActScore(@RequestBody ActScore actObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertActScore(actObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(actObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateACT", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<ActScore> updateActScore(@RequestBody ActScore actObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateActScore(actObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(actObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteACT/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteActScore(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteActScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/SAT", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<SatScore>> getSatScore() {
	    ArrayList<SatScore> results;
	    try {
	    	results = dao.getSatScore(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertSAT", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<SatScore> insertSatScore(@RequestBody SatScore satObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertSatScore(satObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(satObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateSAT", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<SatScore> updateSatScore(@RequestBody SatScore satObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateSatScore(satObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(satObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteSAT/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteSatScore(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteSatScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/FYI", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FyiScore>> getFyiScore() {
	    ArrayList<FyiScore> results;
	    try {
	    	results = dao.getFyiScore(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertFYI", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FyiScore> insertFyiScore(@RequestBody FyiScore fyiObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertFyiScore(fyiObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(fyiObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateFYI", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<FyiScore> updateFyiScore(@RequestBody FyiScore fyiObject) {
		int rowsUpdate;
		try {
			rowsUpdate = dao.updateFyiScore(fyiObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(fyiObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteFYI/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFyiScore(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteFyiScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Other", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<OtherScore>> getOtherScore() {
	    ArrayList<OtherScore> results;
	    try {
	    	results = dao.getOtherScore(userManager.getUserId());
		} catch (Exception e) {
	    	logger.error("Error", e);
	    	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertOther", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<OtherScore> insertOtherScore(@RequestBody OtherScore otherObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertOtherScore(otherObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(otherObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdateOther", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<OtherScore> updateOtherScore(@RequestBody OtherScore otherObject) {
		int rowsUpdate;
		try {
			rowsUpdate = dao.updateOtherScore(otherObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdate != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(otherObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeleteOther/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteOtherScore(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteOtherScore(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/Plan", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Plan>> getPlan() {
	    ArrayList<Plan> results;
	    try {
	    	results = dao.getPlan(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/InsertPlan", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Plan> insertPlan(@RequestBody Plan planObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertPlan(planObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(planObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdatePlan", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<Plan> updatePlan(@RequestBody Plan planObject) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updatePlan(planObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(planObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/DeletePlan/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deletePlan(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deletePlan(userManager.getUserId(), id);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/SelectNameGrade", method = RequestMethod.GET)
	public @ResponseBody ArrayList<UserInfo> selectNameGrade() {
		ArrayList<UserInfo> results = new ArrayList<>();
		try {
			results = dao.selectNameGrade(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/UpdateInsertUserInfo", method = RequestMethod.PUT)
	public @ResponseBody ResponseEntity<UserInfo> updateInsertNameGrade(@RequestBody UserInfo userInfo) {
		int rowsUpdated;
		try {
			rowsUpdated = dao.updateInsertNameGrade(userInfo, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(userInfo, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get-citm-favorite-occupation/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<com.cep.spring.model.portfolio.FavoriteOccupation>> getFavoriteOccupation() {
		ArrayList<com.cep.spring.model.portfolio.FavoriteOccupation> results;
		try {
			results = dao.getFavoriteOccupation(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-access-code/", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getAccessCode() {
		String r = null;
		try {
			r = userManager.getUserAccessCode();
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return Collections.singletonMap("r", r);
	}
	
	/**
	 * Stores information on whether user has completed teacher demo.
	 */
	@RequestMapping(value = "/insert-teacher-demo-taken", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> insertTeacherDemoTaken() {
		int rowsInserted;
		try {
			rowsInserted = dao.insertTeacherDemoTaken(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(rowsInserted, HttpStatus.OK);
		}
	}
	
	/**
	 * Returns information on whether user has completed teacher demo.
	 * @return returns if the user has taken the demo
	 */
	@RequestMapping(value = "/get-teacher-demo-taken", method = RequestMethod.GET)
	public @ResponseBody Map<String, Integer> getTeacherDemoTaken() {
		Integer r = null;
		try {
			r = dao.getTeacherDemoTaken(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return Collections.singletonMap("r", r);
	}
	
	@RequestMapping(value = "/merge-portfolio/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public void mergePortfolio(@PathVariable Integer oldUserId, @PathVariable Integer newUserId) {
		try {
			List<String> oldPortfolioList = dao.isPortfolioStarted(oldUserId);
			for (String portfolio: oldPortfolioList)
 			switch (portfolio) {
			case "PORTFOLIO_ACHIEVEMENT":
				List<Achievement> oldAchievements = dao.getAchievements(oldUserId);
				List<Achievement> newAchievements = dao.getAchievements(newUserId);
				for(Achievement achievementObject: oldAchievements) {
					if(!newAchievements.contains(achievementObject))
						dao.insertAchievement(achievementObject, newUserId);
				}
				break;
			case "PORTFOLIO_EDUCATION":
				List<Education> oldEducations = dao.getEducation(oldUserId);
				List<Education> newEducations = dao.getEducation(newUserId);
				for(Education educationObject: oldEducations) {
					if(!newEducations.contains(educationObject))
						dao.insertEducation(educationObject, newUserId);
				}
				break;
			case "PORTFOLIO_INTEREST":
				List<Interest> oldInterests = dao.getInterests(oldUserId);
				List<Interest> newInterests = dao.getInterests(newUserId);
				for(Interest interestObject: oldInterests) {
					if(!newInterests.contains(interestObject))
						dao.insertInterest(interestObject, newUserId);
				}
				break;
			case "PORTFOLIO_SKILL":
				List<Skill> oldSkills = dao.getSkills(oldUserId);
				List<Skill> newSkills = dao.getSkills(newUserId);
				for(Skill skillObject: oldSkills) {
					if(!newSkills.contains(skillObject))
					dao.insertSkill(skillObject, newUserId);
				}
				break;
			case "PORTFOLIO_TEST_ACT":
				List<ActScore> oldActScores = dao.getActScore(oldUserId);
				List<ActScore> newActScores = dao.getActScore(newUserId);
				for(ActScore actScoreObject: oldActScores) {
					if(!newActScores.contains(actScoreObject))
						dao.insertActScore(actScoreObject, newUserId);
				}
				break;
			case "PORTFOLIO_TEST_ASVAB":
				List<AsvabScore> oldAsvabScores = dao.getAsvabScore(oldUserId);
				List<AsvabScore> newAsvabScores = dao.getAsvabScore(newUserId);
				for(AsvabScore asvabScoreObject: oldAsvabScores) {
					if(!newAsvabScores.contains(asvabScoreObject))
						dao.insertAsvabScore(asvabScoreObject, newUserId);
				}
				break;
			case "PORTFOLIO_TEST_FYI":
				List<FyiScore> oldFyiScores = dao.getFyiScore(oldUserId);
				List<FyiScore> newFyiScores = dao.getFyiScore(newUserId);
				for(FyiScore fyiScoreObject: oldFyiScores) {
					if(!newFyiScores.contains(fyiScoreObject))
						dao.insertFyiScore(fyiScoreObject, newUserId);
				}
				break;
			case "PORTFOLIO_TEST_OTHER":
				List<OtherScore> oldOtherScores = dao.getOtherScore(oldUserId);
				List<OtherScore> newOtherScores = dao.getOtherScore(newUserId);
				for(OtherScore otherScoreObject: oldOtherScores) {
					if(!newOtherScores.contains(otherScoreObject))
						dao.insertOtherScore(otherScoreObject, newUserId);
				}
				break;
			case "PORTFOLIO_TEST_SAT":
				List<SatScore> oldSatScores = dao.getSatScore(oldUserId);
				List<SatScore> newSatScores = dao.getSatScore(newUserId);
				for(SatScore satScoreObject: oldSatScores) {
					if(!newSatScores.contains(satScoreObject))
						dao.insertSatScore(satScoreObject, newUserId);
				}
				break;
			case "PORTFOLIO_VOLUNTEER":
				List<Volunteer> oldVolunteers = dao.getVolunteers(oldUserId);
				List<Volunteer> newVolunteers = dao.getVolunteers(newUserId);
				for(Volunteer volunteerObject: oldVolunteers) {
					if(!newVolunteers.contains(volunteerObject))
						dao.insertVolunteer(volunteerObject, newUserId);
				}
				break;		
			case "PORTFOLIO_WORK_EXPERIENCE":
				List<WorkExperience> oldWorkExperiences = dao.getWorkExperience(oldUserId);
				List<WorkExperience> newWorkExperiences = dao.getWorkExperience(newUserId);
				for(WorkExperience workExperienceObject: oldWorkExperiences) {
					if(!newWorkExperiences.contains(workExperienceObject))
						dao.insertWorkExperience(workExperienceObject, newUserId);
				}
				break;
			case "PORTFOLIO_WORK_VALUE":
				List<WorkValue> oldWorkValues = dao.getWorkValues(oldUserId);
				List<WorkValue> newWorkValues = dao.getWorkValues(newUserId);
				for(WorkValue workValueObject: oldWorkValues) {
					if(!newWorkValues.contains(workValueObject))
						dao.insertWorkValue(workValueObject, newUserId);
				}
				break;					
			case "PORTFOLIO_PLANS":
				Set<String> uniquePlans = new HashSet<>();
				List<Plan> oldPlans = dao.getPlan(oldUserId);
				List<Plan> newPlans = dao.getPlan(newUserId);
				newPlans.forEach(a->uniquePlans.add(a.getPlan()));
				for(int i = 0; i < oldPlans.size(); i++) {
					if(uniquePlans.contains(oldPlans.get(i).getPlan())) {
						for(Plan PlanObject: newPlans) {
							if(oldPlans.get(i).getPlan().contains(PlanObject.getPlan())){
								PlanObject.setDescription(PlanObject.getDescription()+", " + oldPlans.get(i).getDescription());
								dao.updatePlan(PlanObject, newUserId);
							}
						}
					} else {
						dao.insertPlan(oldPlans.get(i), newUserId);
					}
				}
				break;	

			}
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
	
	@RequestMapping(value = "/copy-portfolio/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public void copyPortfolio(@PathVariable Integer oldUserId, @PathVariable Integer newUserId) {
		try {
			List<String> oldPortfolioList = dao.isPortfolioStarted(oldUserId);
			for (String portfolio: oldPortfolioList)
			switch (portfolio) {
			case "PORTFOLIO_ACHIEVEMENT":
				List<Achievement> oldAchievements = dao.getAchievements(oldUserId);
				for(Achievement achievementObject: oldAchievements)
					dao.updateAchievement(achievementObject, newUserId);
				break;
			case "PORTFOLIO_EDUCATION":
				List<Education> oldEducations = dao.getEducation(oldUserId);
				for(Education educationObject: oldEducations) 
					dao.updateEducation(educationObject, newUserId);
				break;
			case "PORTFOLIO_INTEREST":
				List<Interest> oldInterests = dao.getInterests(oldUserId);
				for(Interest interestObject: oldInterests) 
					dao.updateInterest(interestObject, newUserId);
				break;
			case "PORTFOLIO_SKILL":
				List<Skill> oldSkills = dao.getSkills(oldUserId);
				for(Skill skillObject: oldSkills) 
					dao.updateSkill(skillObject, newUserId);
				break;
			case "PORTFOLIO_TEST_ACT":
				List<ActScore> oldActScores = dao.getActScore(oldUserId);
				for(ActScore actScoreObject: oldActScores) 
					dao.updateActScore(actScoreObject, newUserId);
				break;
			case "PORTFOLIO_TEST_ASVAB":
				List<AsvabScore> oldAsvabScores = dao.getAsvabScore(oldUserId);
				for(AsvabScore asvabScoreObject: oldAsvabScores) 
					dao.updateAsvabScore(asvabScoreObject, newUserId);
				break;
			case "PORTFOLIO_TEST_FYI":
				List<FyiScore> oldFyiScores = dao.getFyiScore(oldUserId);
				for(FyiScore fyiScoreObject: oldFyiScores)
					dao.updateFyiScore(fyiScoreObject, newUserId);
				break;
			case "PORTFOLIO_TEST_OTHER":
				List<OtherScore> oldOtherScores = dao.getOtherScore(oldUserId);
				for(OtherScore otherScoreObject: oldOtherScores)
					dao.updateOtherScore(otherScoreObject, newUserId);
				break;
			case "PORTFOLIO_TEST_SAT":
				List<SatScore> oldSatScores = dao.getSatScore(oldUserId);
				for(SatScore satScoreObject: oldSatScores) 
					dao.updateSatScore(satScoreObject, newUserId);
				break;
			case "PORTFOLIO_VOLUNTEER":
				List<Volunteer> oldVolunteers = dao.getVolunteers(oldUserId);
				for(Volunteer volunteerObject: oldVolunteers) 
					dao.updateVolunteer(volunteerObject, newUserId);
				break;		
			case "PORTFOLIO_WORK_EXPERIENCE":
				List<WorkExperience> oldWorkExperiences = dao.getWorkExperience(oldUserId);
				for(WorkExperience workExperienceObject: oldWorkExperiences)
					dao.updateWorkExperience(workExperienceObject, newUserId);
				break;
			case "PORTFOLIO_WORK_VALUE":
				List<WorkValue> oldWorkValues = dao.getWorkValues(oldUserId);
				for(WorkValue workValueObject: oldWorkValues) 
					dao.updateWorkValue(workValueObject, newUserId);
				break;					
			case "PORTFOLIO_PLANS":
				List<Plan> oldPlans = dao.getPlan(oldUserId);
				for(Plan planObject: oldPlans)
					dao.updatePlan(planObject, newUserId);
				break;			
			}
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
}
