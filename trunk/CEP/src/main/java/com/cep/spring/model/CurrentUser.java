package com.cep.spring.model;

public class CurrentUser {
    private String role;
    private String authdata;
    private Boolean isRegistered;

    public CurrentUser(String role, String authData, Boolean isRegistered) {
        this.role = role;
        this.authdata = authData;
        this.isRegistered = isRegistered;
    }
}
