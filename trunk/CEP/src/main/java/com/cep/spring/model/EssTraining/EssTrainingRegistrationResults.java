package com.cep.spring.model.EssTraining;

import java.io.Serializable;

public class EssTrainingRegistrationResults implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2354085594890405567L;
	private Integer resultCode;
	private String resultMessage;
	
	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}
	
	public Integer getResultCode() {
		return this.resultCode;
	}
	
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	
	public String getResultMessage() {
		return this.resultMessage;
	}
}