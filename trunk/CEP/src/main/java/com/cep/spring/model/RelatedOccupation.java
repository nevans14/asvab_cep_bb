package com.cep.spring.model;

import java.io.Serializable;

public class RelatedOccupation implements Serializable {

	private static final long serialVersionUID = 7330714699192304191L;
	private String relatedOnetSoc;
	private String title;
	private int isBrightOccupation;
	private int isGreenOccupation;
	private int isStemOccupation;

	public int getIsBrightOccupation() {
		return isBrightOccupation;
	}

	public void setIsBrightOccupation(int isBrightOccupation) {
		this.isBrightOccupation = isBrightOccupation;
	}

	public int getIsGreenOccupation() {
		return isGreenOccupation;
	}

	public void setIsGreenOccupation(int isGreenOccupation) {
		this.isGreenOccupation = isGreenOccupation;
	}

	public int getIsStemOccupation() {
		return isStemOccupation;
	}

	public void setIsStemOccupation(int isStemOccupation) {
		this.isStemOccupation = isStemOccupation;
	}

	public String getRelatedOnetSoc() {
		return relatedOnetSoc;
	}

	public void setRelatedOnetSoc(String relatedOnetSoc) {
		this.relatedOnetSoc = relatedOnetSoc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
