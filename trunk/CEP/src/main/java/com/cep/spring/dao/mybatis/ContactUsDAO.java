package com.cep.spring.dao.mybatis;

import java.util.ArrayList;

import com.cep.spring.model.faq.EmailContactUs;
import com.cep.spring.model.faq.ScoreRequest;

public interface ContactUsDAO {

	ArrayList<String> getEmail(int roleId) throws Exception;

	int insertGeneralContactUs(EmailContactUs emailObject) throws Exception;

	int insertScoreRquest(ScoreRequest emailObject) throws Exception;

	boolean isSubscribed(String email);

}