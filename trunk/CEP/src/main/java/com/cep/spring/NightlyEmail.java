package com.cep.spring;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cep.spring.dao.mybatis.ContactUsDAO;
import com.cep.spring.dao.mybatis.TestScoreDao;
import com.cep.spring.model.testscore.RecordCount;
import com.cep.spring.utils.ConductSSLHandShake;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.qos.logback.classic.Logger;

@RestController
public class NightlyEmail {
	
	private final Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("NightlyEmail");
	private static final String PROD_URL = "https://cepts.dpac.mil/cepts";
	private static final String CEP_PROD_URL = "http://www.asvabprogram.com/";
	private static final String EMAIL_SUBJECT = "Nightly test score report";
	
	@Value("${application.url}")
	private String appUrl;

	private final JdbcTemplate jdbcTemplate;
	private final ContactUsDAO contactUsDao;
	private final CEPMailer mailer;
	private final TestScoreDao dao;
	private final ConductSSLHandShake conductSSLHandShake;

	@Autowired
	public NightlyEmail(JdbcTemplate jdbcTemplate, ContactUsDAO contactUsDao, CEPMailer mailer, TestScoreDao dao, ConductSSLHandShake conductSSLHandShake) {
		this.jdbcTemplate = jdbcTemplate;
		this.contactUsDao = contactUsDao;
		this.mailer = mailer;
		this.dao = dao;
		this.conductSSLHandShake = conductSSLHandShake;
	}
	
	@Scheduled(cron = "0 0 5 * * TUE,WED,THU,FRI,SAT")
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, value="/getNightlyEmail")
	public @ResponseBody void sendNightlyEmail() throws Exception{
		if(prodCheck()) {
			boolean isWebServiceOn = Boolean.parseBoolean(jdbcTemplate.queryForObject("SELECT value FROM dbInfo WHERE name='turn_on_web_service_accesscode'", String.class));
	
			ArrayList<String> emails = contactUsDao.getEmail(12);
			
			if (!isWebServiceOn) {
				logger.debug("Currently on webservice is turned off. Terminating daily email.");
				sendEmails(emails, "The web service is turned off today.");
				return;
			}
	
			RestTemplate restTemplate = conductSSLHandShake.doHandShake();
			
			LocalDateTime date = LocalDateTime.now().minusDays(1);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
			String sourceFileName = date.format(formatter);
			logger.debug("STARTING: " + sourceFileName);
			logger.debug("Sending Nightly Email for "+ sourceFileName);
			int icatCount = dao.getRecordCount(sourceFileName, "ICAT");
			String dmdcSTPCount;
			String dmdcIcatCount;
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				String icatTypeCount = getICATCountFromDMDC(restTemplate, sourceFileName);
				dmdcIcatCount = parse(icatTypeCount, sourceFileName, objectMapper).get(sourceFileName).toString();
			}catch (Exception e) {
				logger.error("Error", e);
				dmdcIcatCount = "No test score counts returned for iCAT CEP.";
			}
			 
			int stpCount = dao.getRecordCount(sourceFileName, "STP");

			try {
				String stpTypeCount = getSTPCountFromDMDC(restTemplate, sourceFileName);
				dmdcSTPCount = parse(stpTypeCount, sourceFileName, objectMapper).get(sourceFileName).toString();
			} catch(Exception e) {
				logger.error("Error", e);
				dmdcSTPCount ="No test score counts returned for MEPCOM PNP.";
			}
			String msg = "For " + sourceFileName + ":\n iCat CEP scores transferred to CEP: " + icatCount + "\n iCat CEP scores at DMDC: " + dmdcIcatCount + "\n MEPCOM PNP scores transferred to CEP: " + stpCount + "\n MEPCOM PNP scores at DMDC: " + dmdcSTPCount;
			sendEmails(emails, msg);
		}
	}
	
	private HashMap<String, Integer> parse(String jsonString, String sourceFileName, ObjectMapper objectMapper) {
		HashMap<String, Integer> map = new HashMap<>();
		try {
			List<RecordCount> pnpRecordCount = objectMapper.readValue("["+jsonString+"]", new TypeReference<List<RecordCount>>(){});
			for(RecordCount fileName: pnpRecordCount) {
				if(map.containsKey(fileName.getRecordDate()))
					map.put(fileName.getRecordDate(), map.get(fileName.getRecordDate()) + Integer.parseInt(fileName.getCount()));
				else
					map.put(fileName.getRecordDate(), Integer.valueOf(fileName.getCount()));
			}
		} catch (JsonParseException e) {
			logger.error("JsonParse Error", e);
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			logger.error("JsonMapping Error", e);
			throw new RuntimeException(e);
		} catch (IOException e) {
			logger.error("IO Error", e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			logger.error("Error", e);
			throw new RuntimeException(e);
		}
		return map;
	}
	
	private boolean prodCheck() {
		return CEP_PROD_URL.contains(appUrl);
	}
	
	private String getICATCountFromDMDC(RestTemplate restTemplate, String recordDate) {
		return restTemplate.getForObject(PROD_URL+"/getIcatByDate?from="+recordDate+"&to="+recordDate, String.class);
	}
	
	private String getSTPCountFromDMDC(RestTemplate restTemplate, String recordDate) {
		return restTemplate.getForObject(PROD_URL+"/getPnpByDate?from="+recordDate+"&to="+recordDate, String.class);
	}

	private void sendEmails(List<String> emails, String msg) {
		try {
			for (String email : emails) {
				mailer.sendEmail(email, msg , EMAIL_SUBJECT, null);
			}
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
}
