package com.cep.spring.utils;

import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.model.DbInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class DbInfoUtils {

    private final Logger logger = LogManager.getLogger();
    private final DbInfoDAO dao;

    public DbInfoUtils(DbInfoDAO dao) {
        this.dao = dao;
    }

    /**
     * Checks to see if the access code is a teacher promo code
     * @param accessCode the provided access code
     * @return boolean: is a promo code or not
     */
    public boolean isPromoCode(String accessCode) {
        final String teacher_access_codes = "teacher_access_codes";
        DbInfo dbInfo = callDb(teacher_access_codes);
        String[] codes = splitCodes(dbInfo.getValue());
        return checkCode(codes, accessCode);
    }

    /**
     * Checks to see if the access code is a direct mailer (parent) promo code
     * @param accessCode the provided access code
     * @return boolean: is a direct mailer code or not
     */
    public boolean isParentCode(String accessCode) {
        final String parent_access_codes = "parent_access_codes";
        DbInfo dbInfo = callDb(parent_access_codes);
        String[] codes = splitCodes(dbInfo.getValue());
        return checkCode(codes, accessCode);
    }

    /**
     * Checks to see if the access code will use the "Score 50" scores
     * @param accessCode the provided access code
     * @return boolean: is a "score 50" access code or not
     */
    public boolean isScore50(String accessCode) {
        final String score50_access_codes = "score50_access_codes";
        DbInfo dbInfo = callDb(score50_access_codes);
        String[] codes = splitCodes(dbInfo.getValue());
        // determine if the access code is registered
        boolean isUnregistrable = isAccessCodeUnregistrable(accessCode);
        if (isUnregistrable) {
            return checkCode(codes, accessCode);
        }
        return checkCode(codes, accessCode, true);
    }

    /**
     * Checks to see if the access code is able to be registered
     * @param accessCode the provided access code
     * @return boolean: is registrable or not
     */
    public boolean isAccessCodeUnregistrable(String accessCode) {
        final String unregistrable_access_codes = "unregistrable_access_codes";
        DbInfo dbInfo = callDb(unregistrable_access_codes);
        String[] codes = splitCodes(dbInfo.getValue());
        return checkCode(codes, accessCode);
    }

    private String[] splitCodes(String value) {
        return value.split(",");
    }

    private boolean checkCode(String[] codes, String accessCode) {
        for (String code : codes) {
            if (code == null) continue;
            code = code.trim();
            if (code.isEmpty()) continue;
            if (code.equalsIgnoreCase(accessCode)) return true;
        }
        return false;
    }

    private boolean checkCode(String[] codes, String accessCode, boolean trimPrefix) {
        if (!trimPrefix) {
            return checkCode(codes, accessCode);
        }
        String prefix = accessCode.substring(0, 6);
        accessCode = accessCode.replace(prefix, "");
        return checkCode(codes, accessCode);
    }

    private DbInfo callDb(String name) {
        DbInfo dbInfo = null;
        try {
            dbInfo = dao.getDbInfo(name);
        } catch (Exception e) {
            logger.error("Error", e);
        }
        if (dbInfo == null) {
            throw new RuntimeException("dbInfo is null");
        }
        return dbInfo;
    }
}
