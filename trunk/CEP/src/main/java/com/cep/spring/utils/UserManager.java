package com.cep.spring.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserManager {

    private final Logger logger = LogManager.getLogger();
    private final HttpServletRequest request;

    @Autowired
    public UserManager(HttpServletRequest request) {
        this.request = request;
    }

    public void setUserId(Integer userId) {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                session.setAttribute("ASVAB_USER_ID", String.valueOf(userId));
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
            }
        }
    }

    /***
     * Returns the User ID of the logged in user.
     * @return returns user id in session
     */
    public Integer getUserId() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("ASVAB_USER_ID");
                if (attribute != null) {
                    return Integer.valueOf(attribute.toString());
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
                return null;
            }
        }
        return null;
    }

    /***
     * Returns the Access Code of the logged in user.
     * @return returns user access code in session
     */
    public String getUserAccessCode() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("USER_ACCESS_CODE");
                if (attribute != null) {
                    return attribute.toString();
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
                return null;
            }
        }
        return null;
    }

    public String getUserName() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("USERNAME");
                if (attribute != null) {
                    return attribute.toString();
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
                return null;
            }
        }
        return null;
    }

    public void setUserName(String userName) {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                session.setAttribute("USERNAME", userName);
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
            }
        }
    }

    /***
     * Returns the User Role of the logged in user.
     * @return returns the user role that's in session
     */
    public String getUserRole() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("USER_ROLE");
                if (attribute != null) {
                    return attribute.toString();
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
                return null;
            }
        }
        return null;
    }

    public boolean isRegistered() {
        if (request != null) {
            HttpSession session = request.getSession();
            try {
                Object attribute = session.getAttribute("IS_REGISTERED");
                if (attribute != null) {
                    return Boolean.parseBoolean(attribute.toString());
                }
            } catch (Exception ex) {
                logger.error("error: " + ex.getMessage());
            }
        }
        return false;
    }

    public void createMock() {
        // create authorities
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>() {{
            add(new SimpleGrantedAuthority("ROLE_USER"));
        }};
        // create dummy authentication
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken("MyUserName", "MyPassword", authorities);
        // set authentication
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        // create session
        HttpSession session = request.getSession(true);
        // set session information
        session.setAttribute("ASVAB_USER_ID", 1847711);
        session.setAttribute("ASVAB_USER_ROLE", "USER_ROLE");
        session.setAttribute("USER_ROLE", "P");
        session.setAttribute("USER_ACCESS_CODE", "Mugur-A");
    }
}
