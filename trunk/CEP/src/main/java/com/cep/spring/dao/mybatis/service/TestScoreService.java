package com.cep.spring.dao.mybatis.service;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import com.cep.spring.model.testscore.*;
import com.cep.spring.utils.SimpleFileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.TestScoreMapper;

@Service
public class TestScoreService {
	
	private static final String TXT_FILE_FORMAT = ".txt";
	private static final String ZIP_FILE_FORMAT = ".zip";

	private final Logger logger = LogManager.getLogger();
	private final TestScoreMapper mapper;

	@Autowired
	public TestScoreService(TestScoreMapper mapper) {
		this.mapper = mapper;
	}
		
	public int insertTestScore(List<StudentTestingProgam> testScoreObjects) {
		return mapper.insertTestScore(testScoreObjects);
	}
	
	public StudentTestingProgamSelects getTestScoreWithAccessCode(String accessCode) {
		StudentTestingProgamSelects testScore = mapper.getTestScoreWithAccessCode(accessCode);
		if (testScore == null)
			testScore = mapper.getMockTestScoreWithAccessCode(accessCode);
		return testScore;
	}
	
	public StudentTestingProgamSelects getTestScoreWithUserId(Integer userId) {
		StudentTestingProgamSelects testScore = mapper.getTestScoreWithUserId(userId);
		if (testScore == null)
			testScore = mapper.getMockTestScoreWithUserId(userId);
		return testScore;
	}

	public StudentAsvabScoreResults getStudentASRByAccessCode(String accessCode) {
		return mapper.getStudentASRByAccessCode(accessCode);
	}

	public int insertManualTestScore(ManualTestScore manualTestScoreObj) {
		return mapper.insertManualTestScore(manualTestScoreObj);
	}

	public ManualTestScore getManualTestScore(int userId) {
		return mapper.getManualTestScore(userId);
	}

	public int updateManualTestScore(ManualTestScore manualTestScoreObj) {
		return mapper.updateManualTestScore(manualTestScoreObj);
	}

	public int deleteManualTestScore(int userId) {
		return mapper.deleteManualTestScore(userId);
	}

	public List<StudentTestingProgamSelects> filterTestScore(FilterScoreObj filterScoreObj) {
		return mapper.filterTestScore(filterScoreObj.getAdministeredYear(), 
									  filterScoreObj.getAdministeredMonth(), 
									  filterScoreObj.getLastName(),
									  filterScoreObj.getGender(),
									  filterScoreObj.getEducationLevel(), 
									  filterScoreObj.getSchoolCode(), 
									  filterScoreObj.getSchoolAddress(), 
									  filterScoreObj.getSchoolCity(),
									  filterScoreObj.getSchoolState(), 
									  filterScoreObj.getSchoolZipCode(), 
									  filterScoreObj.getStudentCity(), 
									  filterScoreObj.getStudentState(), 
									  filterScoreObj.getStudentZipCode());
	}
	
	/**
	* This method allow the user to download a file
	* @author Avishek Roy
	*/

	@SuppressWarnings("unchecked")
	public void downloadFile(HttpServletResponse response, Object source, String name) {
		String disposition;
		InputStream inputStream = null;
		FileInputStream fis = null;
		ZipOutputStream zos = null;
		try {
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
			if (source instanceof String) {
				if (name == null)
					name = "testscores_" + formatter.format(now) + TXT_FILE_FORMAT;
				disposition = "attachment; fileName=" + name;
				inputStream = new ByteArrayInputStream(((String) source).getBytes());
				response.setContentType("text/csv");
				response.setHeader("Content-Disposition", disposition);
				response.setHeader("content-Length", String.valueOf(stream(inputStream, response.getOutputStream())));
			} else {
				if (source != null) {
					if (((List<File>) source).size() == 1) {
						File file = ((List<File>) source).get(0);
						fis = new FileInputStream(file);
						inputStream = new BufferedInputStream(fis);
						disposition = "attachment; fileName=" + file.getName();
						response.setContentType("text/csv");
						response.setHeader("Content-Disposition", disposition);
						response.setHeader("content-Length", String.valueOf(stream(inputStream, response.getOutputStream())));
					} else {
						byte[] buffer = new byte[1024];
						response.setContentType("application/zip");
						response.setHeader("Content-Disposition", "attachment; filename=" + formatter.format(now) + ZIP_FILE_FORMAT);
						zos = new ZipOutputStream(response.getOutputStream());
						for (File file : ((List<File>) source)) {
							fis = new FileInputStream(file);
							zos.putNextEntry(new ZipEntry(file.getName()));

							int length;
							while ((length = fis.read(buffer)) > 0) {
								zos.write(buffer, 0, length);
							}
							zos.closeEntry();
						}
						zos.finish();
					}
				}
			}
		} catch (FileNotFoundException e) {
			logger.error("File Error", e);
		} catch (IOException e) {
			logger.error("IO Error", e);
		} finally {
			SimpleFileUtils.safeClose(inputStream);
			SimpleFileUtils.safeClose(fis);
			SimpleFileUtils.safeClose(zos);
		}
	}

	private long stream(InputStream input, OutputStream output) {
		ByteBuffer buffer = ByteBuffer.allocate(10240);
		ReadableByteChannel inputChannel = null;
		WritableByteChannel outputChannel = null;
		long size = 0;
		try {
			inputChannel = Channels.newChannel(input);
			outputChannel = Channels.newChannel(output);
			while (inputChannel.read(buffer) != -1) {
				buffer.flip();
				size += outputChannel.write(buffer);
				buffer.clear();
			}
		} catch (IOException e) {
			logger.error("IO Error", e);
		} finally {
			SimpleFileUtils.safeClose(inputChannel);
			SimpleFileUtils.safeClose(outputChannel);
		}
		return size;
	}

	public void downloadFile(HttpServletResponse response, Object source) {
		downloadFile(response, source, null);
	}

	public int deleteIcatDuplicates(String sourceFileName, String type) {
		return mapper.deleteIcatDuplicates(sourceFileName, type);
	}
	
	public int getRecordCount(String sourceFileName, String platform) {
		return mapper.getRecordCount(sourceFileName, platform);
	}
}
