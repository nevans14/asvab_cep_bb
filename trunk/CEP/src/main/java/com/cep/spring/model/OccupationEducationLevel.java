package com.cep.spring.model;

import java.io.Serializable;

public class OccupationEducationLevel implements Serializable {

	private static final long serialVersionUID = 3713051733874423481L;
	private String onetSoc;
	private String educationDesc;
	private double educationLvlVal;

	public String getOnetSoc() {
		return onetSoc;
	}

	public void setOnetSoc(String onetSoc) {
		this.onetSoc = onetSoc;
	}

	public String getEducationDesc() {
		return educationDesc;
	}

	public void setEducationDesc(String educationDesc) {
		this.educationDesc = educationDesc;
	}

	public double getEducationLvlVal() {
		return educationLvlVal;
	}

	public void setEducationLvlVal(double educationLvlVal) {
		this.educationLvlVal = educationLvlVal;
	}
}
