package com.cep.spring.dao.mybatis;

import java.util.ArrayList;
import com.cep.spring.model.optionready.FlickrPhotoInfo;

public interface FlickrApiDAO {
	
	ArrayList<FlickrPhotoInfo> getPhotoCollection(boolean isDebugging) throws Exception;
	
}