package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.DbInfo;

public interface DbInfoMapper {
	
	DbInfo getDbInfo(String name);
	
}