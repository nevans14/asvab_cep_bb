package com.cep.spring.model.occufind;

import java.io.Serializable;
import java.util.List;

public class OccufindSearch implements Serializable {

	private static final long serialVersionUID = 3233993850045825627L;
	private String onetSoc;
	private String occupationTitle;
	private double verbalSkill;
	private double mathSkill;
	private double sciTechSkill;
	private String interestCdOne;
	private String interestCdTwo;
	private String interestCdThree;
	private Boolean isBrightOccupation;
	private Boolean isGreenOccupation;
	private Boolean isStemOccupation;
	private Boolean isHotOccupation;
	private Boolean isAvailableInTheMilitary;
	private List<String> alternateTitles;

	public String getOnetSoc() {
		return onetSoc;
	}

	public void setOnetSoc(String onetSoc) {
		this.onetSoc = onetSoc;
	}

	public String getOccupationTitle() {
		return occupationTitle;
	}

	public void setOccupationTitle(String occupationTitle) {
		this.occupationTitle = occupationTitle;
	}

	public double getVerbalSkill() {
		return verbalSkill;
	}

	public void setVerbalSkill(double verbalSkill) {
		this.verbalSkill = verbalSkill;
	}

	public double getMathSkill() {
		return mathSkill;
	}

	public void setMathSkill(double mathSkill) {
		this.mathSkill = mathSkill;
	}

	public double getSciTechSkill() {
		return sciTechSkill;
	}

	public void setSciTechSkill(double sciTechSkill) {
		this.sciTechSkill = sciTechSkill;
	}

	public String getInterestCdOne() {
		return interestCdOne;
	}

	public void setInterestCdOne(String interestCdOne) {
		this.interestCdOne = interestCdOne;
	}

	public String getInterestCdTwo() {
		return interestCdTwo;
	}

	public void setInterestCdTwo(String interestCdTwo) {
		this.interestCdTwo = interestCdTwo;
	}

	public String getInterestCdThree() {
		return interestCdThree;
	}

	public void setInterestCdThree(String interestCdThree) {
		this.interestCdThree = interestCdThree;
	}

	public Boolean getIsBrightOccupation() {
		return isBrightOccupation;
	}

	public void setIsBrightOccupation(Boolean isBrightOccupation) {
		this.isBrightOccupation = isBrightOccupation;
	}

	public Boolean getIsGreenOccupation() {
		return isGreenOccupation;
	}

	public void setIsGreenOccupation(Boolean isGreenOccupation) {
		this.isGreenOccupation = isGreenOccupation;
	}

	public Boolean getIsStemOccupation() {
		return isStemOccupation;
	}

	public void setIsStemOccupation(Boolean isStemOccupation) {
		this.isStemOccupation = isStemOccupation;
	}
	
	public Boolean getIsHotOccupation() {
		return isHotOccupation;
	}
	
	public void setIsHotOccupation(Boolean isHotOccupation) {
		this.isHotOccupation = isHotOccupation;
	}
	
	public Boolean getIsAvailableInTheMilitary() {
		return isAvailableInTheMilitary;
	}
	
	public void setIsAvailableInTheMilitary(Boolean isAvailableInTheMilitary) {
		this.isAvailableInTheMilitary = isAvailableInTheMilitary;
	}

	public List<String> getAlternateTitles() { return alternateTitles; }

	public void setAlternateTitles(List<String> alternateTitles) { this.alternateTitles = alternateTitles; }
}
