package com.cep.spring.mvc.controller;

import com.cep.spring.dao.mybatis.LearnAboutYourselfDAO;
import com.cep.spring.model.Dashboard.MoreAboutMe;
import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/learn-about-yourself")
public class LearnAboutYourselfController {

    private final Logger logger = LogManager.getLogger();
    private final UserManager userManager;
    private final LearnAboutYourselfDAO learnAboutYourselfDao;

    @Autowired
    public LearnAboutYourselfController(LearnAboutYourselfDAO learnAboutYourselfDao, UserManager userManager) {
        this.learnAboutYourselfDao = learnAboutYourselfDao;
        this.userManager = userManager;
    }


    @RequestMapping(value = "/get-more-about-me", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<MoreAboutMe>> getMoreAboutMe() {
        List<MoreAboutMe> results;
        try {
            results = learnAboutYourselfDao.getMoreAboutMe(userManager.getUserId());
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/insert-more-about-me", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<MoreAboutMe> insertMoreAboutMe(@RequestBody MoreAboutMe model) {
        int results;
        try {
            model.setUserId(userManager.getUserId());
            results = learnAboutYourselfDao.insertMoreAboutMe(model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (results == 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    @RequestMapping(value = "/update-more-about-me", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updateMoreAboutMe(@RequestBody MoreAboutMe model) {
        int results;
        try {
            model.setUserId(userManager.getUserId());
            results = learnAboutYourselfDao.updateMoreAboutMe(model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (results == 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete-more-about-me/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteMoreAboutMe(@PathVariable int id) {
        int results;
        try {
            results = learnAboutYourselfDao.deleteMoreAboutMe(id, userManager.getUserId());
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (results == 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }
}
