package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.impl.SearchDAOImpl;
import com.cep.spring.model.search.Search;

@Controller
@RequestMapping("/search")
public class SearchController {

	private final Logger logger = LogManager.getLogger();
	private final SearchDAOImpl dao;

	@Autowired
	public SearchController(SearchDAOImpl dao) {
		this.dao = dao;
	}

	@RequestMapping(value = "/fileName/{searchString}", method = RequestMethod.GET)
	public @ResponseBody List<Search> getFileName(@PathVariable String searchString) {
	    logger.info("getFileName");
	    searchString = Jsoup.clean(searchString, Whitelist.basic());
		// append wildcard
		searchString = '%' + searchString + '%';

		List<Search> search = new ArrayList<>();
		try {
			search = dao.getFileName(searchString);
		} catch (Exception e) {
			logger.error("Error", e);
		}

		return search;
	}
}
