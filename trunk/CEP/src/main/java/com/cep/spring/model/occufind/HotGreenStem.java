package com.cep.spring.model.occufind;

import java.io.Serializable;

public class HotGreenStem implements Serializable {

	private static final long serialVersionUID = 832417219144435575L;
	
	private char isBrightOccupation;
	private char isGreenOccupation;
	private char isStemOccupation;
	
	public char getIsBrightOccupation() {
		return isBrightOccupation;
	}
	public void setIsBrightOccupation(char isBrightOccupation) {
		this.isBrightOccupation = isBrightOccupation;
	}
	public char getIsGreenOccupation() {
		return isGreenOccupation;
	}
	public void setIsGreenOccupation(char isGreenOccupation) {
		this.isGreenOccupation = isGreenOccupation;
	}
	public char getIsStemOccupation() {
		return isStemOccupation;
	}
	public void setIsStemOccupation(char isStemOccupation) {
		this.isStemOccupation = isStemOccupation;
	}
}
