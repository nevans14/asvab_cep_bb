package com.cep.spring.dao.mybatis;

import java.util.List;

import com.cep.spring.model.resources.ResourceQuickLinkTopic;
import com.cep.spring.model.resources.ResourceTopic;

public interface ResourceDAO {
	
	List<ResourceTopic> getResources() throws Exception;
	
	List<ResourceQuickLinkTopic> getQuickLinks() throws Exception;
	
}