package com.cep.spring.dao.mybatis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.ResourceMapper;
import com.cep.spring.model.resources.ResourceQuickLinkTopic;
import com.cep.spring.model.resources.ResourceTopic;

@Service
public class ResourceService {

	private final ResourceMapper mapper;

	@Autowired
	public ResourceService(ResourceMapper mapper) {
		this.mapper = mapper;
	}
	
	public List<ResourceTopic> getResources() {
		return mapper.getResources();
	}
	
	public List<ResourceQuickLinkTopic> getQuickLinks() {
		return mapper.getQuickLinks();
	}
}