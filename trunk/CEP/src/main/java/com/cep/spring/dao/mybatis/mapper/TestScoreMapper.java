package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;
import java.util.List;

import com.cep.spring.model.testscore.StudentAsvabScoreResults;
import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.testscore.ManualTestScore;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.StudentTestingProgamSelects;

public interface TestScoreMapper {

	int insertTestScore(@Param("testScoreObjects") List<StudentTestingProgam> testScoreObjects);
	
	StudentTestingProgamSelects getTestScoreWithAccessCode(@Param("accessCode") String accessCode);
	
	StudentTestingProgamSelects getTestScoreWithUserId(@Param("userId") Integer userId);
	
	StudentTestingProgamSelects getMockTestScoreWithAccessCode(@Param("accessCode") String accessCode);
	
	StudentTestingProgamSelects getMockTestScoreWithUserId(@Param("userId") Integer userId);

	StudentAsvabScoreResults getStudentASRByAccessCode(@Param("accessCode") String accessCode);
	
	int insertManualTestScore(@Param("manualTestScoreObj") ManualTestScore manualTestScoreObj);
	
	ManualTestScore getManualTestScore(@Param("userId") int userId);
	
	int updateManualTestScore(@Param("manualTestScoreObj") ManualTestScore manualTestScoreObj);

	int deleteManualTestScore(@Param("userId") int userId);
	
	ArrayList<StudentTestingProgamSelects> filterTestScore(@Param("administeredYear")  String administeredYear,
										 @Param("administeredMonth") String administeredMonth,
										 @Param("lastName")          String lastName,
										 @Param("gender")          	 String gender,
										 @Param("educationLevel")    String educationLevel,
										 @Param("schoolCode")        String schoolCode,
										 @Param("schoolAddress")     String schoolAddress,
										 @Param("schoolCity")        String schoolCity,
										 @Param("schoolState")       String schoolState,
										 @Param("schoolZipCode")     String schoolZipCode,
										 @Param("studentCity")       String studentCity,
										 @Param("studentState")      String studentState,
										 @Param("studentZipCode")    String studentZipCode);

	int deleteIcatDuplicates(@Param("sourceFileName") String sourceFileName,@Param("type") String type);
	
	int getRecordCount(@Param("sourceFileName") String sourceFileName, @Param("platform") String platform);
}
