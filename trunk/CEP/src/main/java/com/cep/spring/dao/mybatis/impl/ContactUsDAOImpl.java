package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.ContactUsDAO;
import com.cep.spring.dao.mybatis.service.ContactUsService;
import com.cep.spring.model.faq.EmailContactUs;
import com.cep.spring.model.faq.ScoreRequest;

@Primary
@Repository
public class ContactUsDAOImpl implements ContactUsDAO {

	private final ContactUsService service;

	@Autowired
	public ContactUsDAOImpl(ContactUsService service) {
		this.service = service;
	}

	@Override
	public ArrayList<String> getEmail(int roleId) { return service.getEmail(roleId); }

	@Override
	public int insertGeneralContactUs(EmailContactUs emailObject) { return service.insertGeneralContactUs(emailObject); }

	@Override
	public int insertScoreRquest(ScoreRequest emailObject) { return service.insertScoreRquest(emailObject); }
	
	@Override
	public boolean isSubscribed(String email) { return service.isSubscribed(email); }
}
