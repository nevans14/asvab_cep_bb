package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.ContactUsMapper;
import com.cep.spring.model.faq.EmailContactUs;
import com.cep.spring.model.faq.ScoreRequest;

@Service
public class ContactUsService {

	private final ContactUsMapper mapper;

	@Autowired
	public ContactUsService(ContactUsMapper mapper) {
		this.mapper = mapper;
	}

	public ArrayList<String> getEmail(int roleId) {
		return mapper.getEmail(roleId);
	}

	public int insertGeneralContactUs(EmailContactUs emailObject) {
		return mapper.insertGeneralContactUs(emailObject);
	}

	public int insertScoreRquest(ScoreRequest emailObject) {
		return mapper.insertScoreRquest(emailObject);
	}

	public boolean isSubscribed(String email) {
		boolean exists = false;
		if(mapper.isSubscribed(email) > 0)
			exists = true;
		return exists;
	}
}
