package com.cep.spring.model.Dashboard;

import java.io.Serializable;

public class MoreAboutMe implements Serializable {
    private static final long serialVersionUID = -1900022682127405121L;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getMoreAboutMe() {
        return moreAboutMe;
    }

    public void setMoreAboutMe(String moreAboutMe) {
        this.moreAboutMe = moreAboutMe;
    }

    private int id, userId, position;
    private String moreAboutMe;
}
