package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfileV2;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

public interface FindYourInterestMapper {

	int getProfileExist(@Param("userId") Integer userId);

	ArrayList<ScoreChoice> getProfile(@Param("userId") int userId);

	int insertProfile(@Param("userId") int userId);

	int updateProfile(@Param("userId") int userId, @Param("scoreObject") ScoreChoice scoreObject);

	int insertTest(@Param("fyiObject") FindYourInterestProfileV2 fyiObject);

	ArrayList<Scores> getCombinedAndGenderScore(@Param("userId") int userId);

	ArrayList<GenderScore> getGenderScore(@Param("gender") char gender, @Param("rRawScore") int rRawScore,
			@Param("iRawScore") int iRawScore, @Param("aRawScore") int aRawScore, @Param("sRawScore") int sRawScore,
			@Param("eRawScore") int eRawScore, @Param("cRawScore") int cRawScore);

	ArrayList<CombinedScore> getCombinedScore(@Param("rRawScore") int rRawScore, @Param("iRawScore") int iRawScore,
			@Param("aRawScore") int aRawScore, @Param("sRawScore") int sRawScore, @Param("eRawScore") int eRawScore,
			@Param("cRawScore") int cRawScore);

	Integer getNumberTestTaken(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);
	
	Integer getOldUserId(@Param("newUserId") int newUserId);

	String getScoreChoice(@Param("userId") int userId);

	TiedSelection getTiedSelection(@Param("userId") int userId);

	int insertTiedSelection(@Param("userId") int userId, @Param("tiedObject") TiedSelection tiedObject);

	int deleteTiedSelection(@Param("userId") int userId);

	int getWhichLastFyiTableUsed(@Param("oldUserId") int oldUserId, @Param("oldTestId") int oldTestId);

	int mergeProfile(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);
	
	int mergeTests(@Param("fyiObject") FYIMerge fyiObject);
	
	int mergeResults(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);

	int mergeAnswers(@Param("oldUserId") int oldUserId, @Param("newUserId") int newUserId, @Param("testId") int testId);

	int mergeResponse(@Param("oldUserId") int oldUserId, @Param("newTestId") int newTestId);

	FYIMerge getLastTestObject(@Param("oldUserId") Integer oldUserId, @Param("newUserId") int newUserId);
	
	int deleteProfile(@Param("userId") int userId);

}
