package com.cep.spring.dao.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.CEPAnalyticsMapper;

@Service
public class CEPAnalyticsService {

	private final CEPAnalyticsMapper mapper;

	@Autowired
	public CEPAnalyticsService(CEPAnalyticsMapper mapper) {
		this.mapper = mapper;
	}

	public int insertCEPShare(String email, String category) {
		return mapper.insertCEPShare(email, category);
	}
}
