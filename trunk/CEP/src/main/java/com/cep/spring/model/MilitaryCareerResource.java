package com.cep.spring.model;

import java.io.Serializable;

public class MilitaryCareerResource implements Serializable {

	private static final long serialVersionUID = -724399669312294940L;
	String mcId;
	String mcTitle;

	public String getMcId() {
		return mcId;
	}

	public void setMcId(String mcId) {
		this.mcId = mcId;
	}

	public String getMcTitle() {
		return mcTitle;
	}

	public void setMcTitle(String mcTitle) {
		this.mcTitle = mcTitle;
	}
}
