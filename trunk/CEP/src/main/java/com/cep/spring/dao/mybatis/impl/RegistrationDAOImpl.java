package com.cep.spring.dao.mybatis.impl;

import com.cep.spring.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.RegistrationDAO;
import com.cep.spring.dao.mybatis.service.RegistrationService;

import java.util.List;

@Primary
@Repository
public class RegistrationDAOImpl implements RegistrationDAO {

	private final RegistrationService service;

	@Autowired
	public RegistrationDAOImpl(RegistrationService service) {
		this.service = service;
	}
	
	public Users getUser(Long userId, String username) { return service.getUser(userId, username); }
	
	public Users getByUsername(String username) { return service.getByUsername(username); }
	
	public int insertUser(Users model) { return service.insertUser(model); }
	
	public int updateUser(Users model) { return service.updateUser(model); }

	public int updateUsername(Users model) { return service.updateUsername(model); }

	public int doesUsernameExists(String username) { return service.doesUsernameExists(username); }
	
	public int insertUserRole(String username, String role) { return service.insertUserRole(username, role); }

	public int deleteUserRole(String username) { return service.deleteUserRole(username); }
	
	public int insertAccessCode(AccessCodes newAccessCodeObj) {	return service.insertAccessCode(newAccessCodeObj); }
	
	public int updateAccessCode(AccessCodes model) { return service.updateAccessCode(model); }

	public void mergeAccounts(AccessCodesMerge registrationObject) { service.MergeAccounts(registrationObject); }

	public AccessCodes getAccessCodes(String accessCode, Integer userId) { return service.getAccessCodes(accessCode, userId); }

	public Users getUserId(String emailAddress) { return service.getUserId(emailAddress); }

	public void disableAccessCode(String accessCode) {
		service.disableAccessCode(accessCode);
	}

	public void changeAccessCodePointer(Integer userId, String emailAddress) { service.changeAccessCodePointer(userId, emailAddress); }

	public Meps getUsersMeps(String accessCode) {
		return service.getUsersMeps(accessCode);
	}
	
	public int insertAsvabLog(Integer userId) {
		return service.insertAsvabLog(userId);
	}

	public AccessCodes getLastPromoAccessCodeUsed(String accessCode) { return service.getLastPromoAccessCodeUsed(accessCode); }
	
	public int insertTeacherProfession(Integer userId, String subject) { return service.insertTeacherProfession(userId, subject); }
	
	public int insertSubscription(String emailAddress) { return service.insertSubscription(emailAddress); }

	public List<RegistrationDropDown> getFunctionList() { return service.getFunctionList(); }

	public List<RegistrationDropDown> getConferenceList() { return service.getConferenceList(); }

	public int insertCep123Registration(Integer userId, Registration model) { return service.insertCep123Registration(userId, model); }
}
