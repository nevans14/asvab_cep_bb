package com.cep.spring.model.resources;

import java.io.Serializable;

public class ResourceQuickLink implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 716014600141368197L;
	private Integer quickLinkId;
	private Short quickLinkTopicId, quickLinkTypeId;
	private String quickLinkTitle, quickLinkUrl, quickLinkGAVarName;
	private ResourceQuickLinkType quickLinkType;
	// accessors
	public Integer getQuickLinkId() { return quickLinkId; }
	public Short getQuickLinkTopicId() { return quickLinkTopicId; }
	public Short getQuickLinkTypeId() { return quickLinkTypeId; }
	public String getQuickLinkTitle() { return quickLinkTitle; }
	public String getQuickLinkUrl() { return quickLinkUrl; }
	public String getQuickLinkGAVarName() { return quickLinkGAVarName; }
	public ResourceQuickLinkType getQuickLinkType() { return quickLinkType; }
	// mutators
	public void setQuickLinkId(Integer quickLinkId) { this.quickLinkId = quickLinkId; }
	public void setQuickLinkTopicId(Short quickLinkTopicId) { this.quickLinkTopicId = quickLinkTopicId; }
	public void setQuickLinkTypeId(Short quickLinkTypeId) { this.quickLinkTypeId = quickLinkTypeId; }
	public void setQuickLinkTitle(String quickLinkTitle) { this.quickLinkTitle = quickLinkTitle; }
	public void setQuickLinkUrl(String quickLinkUrl) { this.quickLinkUrl = quickLinkUrl; }
	public void setQuickLinkGAVarName(String quickLinkGAVarName) { this.quickLinkGAVarName = quickLinkGAVarName; }
	public void setQuickLinkType(ResourceQuickLinkType quickLinkType) { this.quickLinkType = quickLinkType; }
}