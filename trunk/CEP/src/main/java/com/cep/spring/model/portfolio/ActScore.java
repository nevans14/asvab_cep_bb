package com.cep.spring.model.portfolio;

import java.io.Serializable;

public class ActScore implements Serializable {

	private static final long serialVersionUID = -8013860641424997401L;
	private Integer id;
	private double readingScore;
	private double mathScore;
	private double writingScore;
	private double scienceScore;
	private double englishScore;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public double getReadingScore() {
		return readingScore;
	}

	public void setReadingScore(double readingScore) {
		this.readingScore = readingScore;
	}

	public double getMathScore() {
		return mathScore;
	}

	public void setMathScore(double mathScore) {
		this.mathScore = mathScore;
	}

	public double getWritingScore() {
		return writingScore;
	}

	public void setWritingScore(double writingScore) {
		this.writingScore = writingScore;
	}

	public double getScienceScore() {
		return scienceScore;
	}

	public void setScienceScore(double scienceScore) {
		this.scienceScore = scienceScore;
	}

	public double getEnglishScore() {
		return englishScore;
	}

	public void setEnglishScore(double englishScore) {
		this.englishScore = englishScore;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(englishScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		temp = Double.doubleToLongBits(mathScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(readingScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(scienceScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(writingScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ActScore)) {
			return false;
		}
		ActScore other = (ActScore) obj;
		if (Double.doubleToLongBits(englishScore) != Double.doubleToLongBits(other.englishScore)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (Double.doubleToLongBits(mathScore) != Double.doubleToLongBits(other.mathScore)) {
			return false;
		}
		if (Double.doubleToLongBits(readingScore) != Double.doubleToLongBits(other.readingScore)) {
			return false;
		}
		if (Double.doubleToLongBits(scienceScore) != Double.doubleToLongBits(other.scienceScore)) {
			return false;
		}
		if (Double.doubleToLongBits(writingScore) != Double.doubleToLongBits(other.writingScore)) {
			return false;
		}
		return true;
	}
}
