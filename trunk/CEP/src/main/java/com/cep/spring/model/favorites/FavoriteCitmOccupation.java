package com.cep.spring.model.favorites;

public class FavoriteCitmOccupation {

	private Integer id;
	private String mcId;
	private String title;
	private String hotJob;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the mcId
	 */
	public String getMcId() {
		return mcId;
	}
	/**
	 * @param mcId the mcId to set
	 */
	public void setMcId(String mcId) {
		this.mcId = mcId;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the hotJob
	 */
	public String getHotJob() {
		return hotJob;
	}
	/**
	 * @param hotJob the hotJob to set
	 */
	public void setHotJob(String hotJob) {
		this.hotJob = hotJob;
	}
	
}
