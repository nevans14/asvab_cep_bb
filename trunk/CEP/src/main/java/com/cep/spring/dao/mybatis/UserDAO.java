package com.cep.spring.dao.mybatis;

import com.cep.spring.model.user.UserCompletion;

import java.util.List;

public interface UserDAO {

    List<UserCompletion> getUserCompletion(int userId);

    int updateUserCompletion(int userId, UserCompletion model);

    int deleteUserCompletion(int userId, String completionType);
}
