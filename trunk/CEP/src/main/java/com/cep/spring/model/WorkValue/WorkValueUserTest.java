package com.cep.spring.model.WorkValue;

import java.io.Serializable;

public class WorkValueUserTest implements Serializable {
    private static final long serialVersionUID = -2540947757642266274L;

    public Integer getTestId() {
        return testId;
    }

    public void setTestId(Integer testId) {
        this.testId = testId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(String dateTaken) {
        this.dateTaken = dateTaken;
    }

    private Integer testId, userId;
    private String dateTaken;
}
