package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.CareerPlan.CareerPlanAnswer;
import com.cep.spring.model.CareerPlan.CareerPlanSection;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;

public interface CareerPlanMapper {

    ArrayList<CareerPlanSection> getItems();

    CareerPlanAnswer getResponses(@Param("userId") Integer userId);

    int insertResponses(@Param("model") CareerPlanAnswer model);

    int updateResponses(@Param("model") CareerPlanAnswer model);

}
