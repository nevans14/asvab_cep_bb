package com.cep.spring.sso;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cep.spring.utils.Security;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/sso")
@PropertySource("classpath:application.properties")
public class ConfirmLoginController {

	private final SsoServerSettings ssoServerSettings;
	private final Security security;

	@Autowired
	public ConfirmLoginController(SsoServerSettings ssoServerSettings, Security security) {
		this.ssoServerSettings = ssoServerSettings;
		this.security = security;
	}

	@RequestMapping("/confirm_login")
	public void getSession(HttpSession session, @RequestParam("redirect") String redirect,
				HttpServletRequest request, HttpServletResponse response) throws IOException {
		RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
		// redirect to dashboard
		if (!ssoServerSettings.allow(redirect)) {
			throw new AccessDeniedException("Illegal redirect uri");
		}
		byte[] iv = security.generateSalt(16);
		String token = security.encrypt(session.getId(), iv);
		redirectStrategy.sendRedirect(request, response, redirect + "?token=" + token + "&salt=" + security.getString(iv));
	}
}
