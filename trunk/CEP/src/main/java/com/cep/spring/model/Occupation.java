package com.cep.spring.model;

import java.io.Serializable;
import java.util.List;

public class Occupation implements Serializable {

	private static final long serialVersionUID = 8729165396529779545L;
	String onetSoc;
	String occupationDescription;
	List<Abilities> abilitiesList;
	List<Knowledges> knowledgesList;
	List<Skills> skillsList;

	public String getOnetSoc() {
		return onetSoc;
	}

	public void setOnetSoc(String onetSoc) {
		this.onetSoc = onetSoc;
	}

	public String getOccupationDescription() {
		return occupationDescription;
	}

	public void setOccupationDescription(String occupationDescription) {
		this.occupationDescription = occupationDescription;
	}

	public List<Abilities> getAbilitiesList() {
		return abilitiesList;
	}

	public void setAbilitiesList(List<Abilities> abilitiesList) {
		this.abilitiesList = abilitiesList;
	}

	public List<Knowledges> getKnowledgesList() {
		return knowledgesList;
	}

	public void setKnowledgesList(List<Knowledges> knowledgesList) {
		this.knowledgesList = knowledgesList;
	}

	public List<Skills> getSkillsList() {
		return skillsList;
	}

	public void setSkillsList(List<Skills> skillsList) {
		this.skillsList = skillsList;
	}
}
