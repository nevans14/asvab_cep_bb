package com.cep.spring.model.ClassroomActivity;

public class Standard {
    private Integer id, activityId;
    private String title, url;

    public void setId(Integer id) { this.id = id; }
    public Integer getId() { return id; }

    public void setActivityId(Integer activityId) { this.activityId = activityId; }
    public Integer getActivityId() { return activityId; }

    public void setTitle(String title) { this.title = title; }
    public String getTitle() { return title; }

    public void setUrl(String url) { this.url = url; }
    public String getUrl() { return url; }
}
