package com.cep.spring.model.testscore;

public class TypeCount {

	int allCount = 0;
	
	int icatCount = 0;
	
	int pnpCount = 0;

	/**
	 * @return the allCount
	 */
	public int getAllCount() {
		return allCount;
	}

	/**
	 * @param allCount the allCount to set
	 */
	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}

	/**
	 * @return the icatCount
	 */
	public int getIcatCount() {
		return icatCount;
	}

	/**
	 * @param icatCount the icatCount to set
	 */
	public void setIcatCount(int icatCount) {
		this.icatCount = icatCount;
	}

	/**
	 * @return the stpCount
	 */
	public int getPnpCount() {
		return pnpCount;
	}

	/**
	 * @param stpCount the stpCount to set
	 */
	public void setPnpCount(int pnpCount) {
		this.pnpCount = pnpCount;
	}
	
}
