package com.cep.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.dao.mybatis.service.DbInfoService;
import com.cep.spring.model.DbInfo;

@Repository
public class DbInfoDAOImpl implements DbInfoDAO {

	private final DbInfoService service;

	@Autowired
	public DbInfoDAOImpl(DbInfoService service) {
		this.service = service;
	}
	
	public DbInfo getDbInfo(String name) {
		return service.getDbInfo(name);
	}
}