package com.cep.spring.dao.mybatis;

import java.util.ArrayList;
import java.util.List;

import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FavoriteOccupation;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

public interface PortfolioDAO {

	/**
	 * Returns 0 if no records found for user's portfolio.
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	List<String> isPortfolioStarted(Integer userId) throws Exception;

	/**
	 * Returns user's work experience records.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<WorkExperience> getWorkExperience(Integer userId);

	/**
	 * Insert user's work experience inputs.
	 * 
	 * @param workExperienceObject
	 * @return
	 * @throws Exception
	 */
	int insertWorkExperience(WorkExperience workExperienceObject, Integer userId) throws Exception;

	/**
	 * Delete user's work experience inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteWorkExperience(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's work experience.
	 * 
	 * @param workExperienceObject
	 * @return
	 * @throws Exception
	 */
	int updateWorkExperience(WorkExperience workExperienceObject, Integer userId) throws Exception;

	/**
	 * Returns user's education records.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<Education> getEducation(Integer userId);

	/**
	 * Insert user's education inputs.
	 * 
	 * @param educationObject
	 * @return
	 * @throws Exception
	 */
	int insertEducation(Education educationObject, Integer userId) throws Exception;

	/**
	 * Delete user's education inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteEducation(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's education.
	 * 
	 * @param educationObject
	 * @return
	 * @throws Exception
	 */
	int updateEducation(Education educationObject, Integer userId) throws Exception;

	/**
	 * Returns user's achievement records.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<Achievement> getAchievements(Integer userId);

	/**
	 * Insert user's achievement inputs.
	 * 
	 * @param achievementObject
	 * @return
	 * @throws Exception
	 */
	int insertAchievement(Achievement achievementObject, Integer userId) throws Exception;

	/**
	 * Delete user's achievement inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteAchievement(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's achievement inputs.
	 * 
	 * @param achievementObject
	 * @return
	 * @throws Exception
	 */
	int updateAchievement(Achievement achievementObject, Integer userId) throws Exception;

	/**
	 * Returns user's interest records.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<Interest> getInterests(Integer userId);

	/**
	 * Insert user's interest inputs.
	 * 
	 * @param interestObject
	 * @return
	 * @throws Exception
	 */
	int insertInterest(Interest interestObject, Integer userId) throws Exception;

	/**
	 * Delete user's interest inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteInterest(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's interest inputs.
	 * 
	 * @param interestObject
	 * @return
	 * @throws Exception
	 */
	int updateInterest(Interest interestObject, Integer userId) throws Exception;

	/**
	 * Returns user's skill records.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<Skill> getSkills(Integer userId);

	/**
	 * Insert user's skill inputs.
	 * 
	 * @param skillObject
	 * @return
	 * @throws Exception
	 */
	int insertSkill(Skill skillObject, Integer userId) throws Exception;

	/**
	 * Delete user's skill inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteSkill(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's skill inputs.
	 */
	int updateSkill(Skill skillObject, Integer userId) throws Exception;

	/**
	 * Returns user's work value records.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<WorkValue> getWorkValues(Integer userId);

	/**
	 * Insert user's work value inputs.
	 * 
	 * @param workValueObject
	 * @return
	 * @throws Exception
	 */
	int insertWorkValue(WorkValue workValueObject, Integer userId) throws Exception;

	/**
	 * Delete user's work value inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteWorkValue(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's work value inputs.
	 * 
	 * @param workValueObject
	 * @return
	 * @throws Exception
	 */
	int updateWorkValue(WorkValue workValueObject, Integer userId) throws Exception;

	/**
	 * Returns user's volunteer records.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<Volunteer> getVolunteers(Integer userId);

	/**
	 * Insert user's volunteer inputs.
	 * 
	 * @param volunteerObject
	 * @return
	 * @throws Exception
	 */
	int insertVolunteer(Volunteer volunteerObject, Integer userId) throws Exception;

	/**
	 * Delete user's volunteer inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteVolunteer(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's volunteer inputs.
	 * 
	 * @param volunteerObject
	 * @return
	 * @throws Exception
	 */
	int updateVolunteer(Volunteer volunteerObject, Integer userId) throws Exception;

	/**
	 * Returns user's ASVAB scores.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<AsvabScore> getAsvabScore(Integer userId);

	/**
	 * Insert user's ASVAB score.
	 * 
	 * @param asvabObject
	 * @return
	 * @throws Exception
	 */
	int insertAsvabScore(AsvabScore asvabObject, Integer userId) throws Exception;

	/**
	 * Delete user's ASVAB score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteAsvabScore(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's ASVAB score.
	 * 
	 * @param asvabObject
	 * @return
	 * @throws Exception
	 */
	int updateAsvabScore(AsvabScore asvabObject, Integer userId) throws Exception;

	/**
	 * Returns user's ACT scores.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<ActScore> getActScore(Integer userId);

	/**
	 * Insert user's ACT score.
	 * 
	 * @param actObject
	 * @return
	 * @throws Exception
	 */
	int insertActScore(ActScore actObject, Integer userId) throws Exception;

	/**
	 * Delete user's ACT score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteActScore(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's ACT score.
	 * 
	 * @param actObject
	 * @return
	 * @throws Exception
	 */
	int updateActScore(ActScore actObject, Integer userId) throws Exception;

	/**
	 * Returns user's SAT scores.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<SatScore> getSatScore(Integer userId);

	/**
	 * Insert user's SAT score.
	 * 
	 * @param satObject
	 * @return
	 * @throws Exception
	 */
	int insertSatScore(SatScore satObject, Integer userId) throws Exception;

	/**
	 * Delete user's SAT score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteSatScore(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's SAT score.
	 * 
	 * @param satObject
	 * @return
	 * @throws Exception
	 */
	int updateSatScore(SatScore satObject, Integer userId) throws Exception;

	/**
	 * Returns user's FYI scores.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<FyiScore> getFyiScore(Integer userId);

	/**
	 * Insert user's FYI score.
	 * 
	 * @param fyiObject
	 * @return
	 * @throws Exception
	 */
	int insertFyiScore(FyiScore fyiObject, Integer userId) throws Exception;

	/**
	 * Delete user's FYI score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteFyiScore(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's FYIScore.
	 * 
	 * @param fyiObject
	 * @return
	 * @throws Exception
	 */
	int updateFyiScore(FyiScore fyiObject, Integer userId) throws Exception;

	/**
	 * Returns user's other scores.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<OtherScore> getOtherScore(Integer userId);

	/**
	 * Insert user's other score.
	 * 
	 * @param otherObject
	 * @return
	 * @throws Exception
	 */
	int insertOtherScore(OtherScore otherObject, Integer userId) throws Exception;

	/**
	 * Delete user's other score.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deleteOtherScore(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's other score.
	 * 
	 * @param otherObject
	 * @return
	 * @throws Exception
	 */
	int updateOtherScore(OtherScore otherObject, Integer userId) throws Exception;

	/**
	 * Returns user's plan records.
	 * 
	 * @param userId
	 * @return
	 */
	ArrayList<Plan> getPlan(Integer userId);

	/**
	 * Insert user's plan inputs.
	 * 
	 * @param planObject
	 * @return
	 * @throws Exception
	 */
	int insertPlan(Plan planObject, Integer userId) throws Exception;

	/**
	 * Delete user's plan inputs.
	 * 
	 * @param userId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	int deletePlan(Integer userId, Integer id) throws Exception;

	/**
	 * Update user's plan inputs.
	 * 
	 * @param planObject
	 * @return
	 * @throws Exception
	 */
	int updatePlan(Plan planObject, Integer userId) throws Exception;

	/**
	 * Gets user's name and grade.
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	ArrayList<UserInfo> selectNameGrade(Integer userId) throws Exception;

	/**
	 * Insert user's name and gpa. If already exists, then updates records.
	 * 
	 * @param userId
	 * @param name
	 * @param gpa
	 * @return
	 * @throws Exception
	 */
	int updateInsertNameGrade(UserInfo userInfo, Integer userId) throws Exception;

	ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) throws Exception;

	String getAccessCode(Integer userId) throws Exception;

	int insertTeacherDemoTaken(Integer userId) throws Exception;

	int getTeacherDemoTaken(Integer userId) throws Exception;

}