package com.cep.spring.model.fyi;

import java.io.Serializable;

public class Scores implements Serializable {

	private static final long serialVersionUID = -548500095463210800L;
	private char interestCd;
	private double combinedScore;
	private double genderScore;

	public char getInterestCd() {
		return interestCd;
	}

	public void setInterestCd(char interestCd) {
		this.interestCd = interestCd;
	}

	public double getCombinedScore() {
		return combinedScore;
	}

	public void setCombinedScore(double combinedScore) {
		this.combinedScore = combinedScore;
	}

	public double getGenderScore() {
		return genderScore;
	}

	public void setGenderScore(double genderScore) {
		this.genderScore = genderScore;
	}

}
