package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.WorkValue.WorkValueUserProfile;
import com.cep.spring.model.WorkValue.WorkValueUserTest;
import org.apache.ibatis.annotations.Param;

public interface WorkValueMapper {

    int insertTest(@Param("model")WorkValueUserTest model);

    WorkValueUserProfile getProfile(@Param("userId") Integer userId);

}
