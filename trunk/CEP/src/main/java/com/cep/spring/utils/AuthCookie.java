package com.cep.spring.utils;

import com.cep.spring.model.CurrentUser;
import com.google.gson.Gson;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

public class AuthCookie {

    private CurrentUser currentUser;

    public void setCurrentUser(CurrentUser currentUser) { this.currentUser = currentUser; }

    public String getAuthData(Integer userId, String hash, String username) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        sdf.setTimeZone(TimeZone.getTimeZone("EST"));
        String text = sdf.format(date);
        return Base64.getEncoder()
                .encodeToString(
                        String.format(Locale.ENGLISH,
                            "%d:%s:%s:%s", userId, hash, text, username
                        ).getBytes());
    }

    public String getAuthCookieJson() throws Exception {
        return URLEncoder.encode(new Gson().toJson(this), "UTF-8");
    }
}
