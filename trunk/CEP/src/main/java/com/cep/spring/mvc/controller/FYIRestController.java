package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.cep.spring.dao.mybatis.RegistrationDAO;
import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.FindYourInterestDAO;
import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIRawScore;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfileV2;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

@Controller
@RequestMapping("/fyi")
public class FYIRestController {

	private final Logger logger = LogManager.getLogger();
	private final FindYourInterestDAO fyiDao;
	private final UserManager userManager;
	private final RegistrationDAO registrationDao;

	@Autowired
	public FYIRestController(FindYourInterestDAO dao, RegistrationDAO registrationDao, UserManager userManager) {
		this.fyiDao = dao;
		this.userManager = userManager;
		this.registrationDao = registrationDao;
	}

	@RequestMapping(value = "/save-test", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> saveTest()
	{
		boolean results;
		FindYourInterestProfileV2 model = new FindYourInterestProfileV2(userManager.getUserId());
		try {
			int numOfTestsTaken = getNumberTestTaken().getBody();
			boolean isUnlimitedFyi = registrationDao
					.getAccessCodes(null, model.getUserId())
					.getUnlimitedFyi() == 1;
			if (!isUnlimitedFyi && numOfTestsTaken >= 2) {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
			results = fyiDao.insertProfileAndTest(model);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		// if no test records were inserted, then throw error
		if (!results) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// return test id
		return new ResponseEntity<>(model.getTestId(), HttpStatus.OK);
	}

	@RequestMapping(value = "/getGenderScore/{gender}/", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<GenderScore>> getGenderScore(@RequestBody FYIRawScore rawScores,
			@PathVariable char gender) {
		List<GenderScore> results;
		try {
			results = fyiDao.getGenderScore(rawScores, gender);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/getCombinedScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<CombinedScore>> getCombinedScore(@RequestBody FYIRawScore rawScores) {
		List<CombinedScore> results;
		try {
			results = fyiDao.getCombinedScore(rawScores);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/NumberTestTaken", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> getNumberTestTaken() {
		int userId = userManager.getUserId();
		int results;
		try {
			results = fyiDao.getNumberTestTaken(fyiDao.getOldUserId(userId), userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/CombinedAndGenderScores", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<Scores>> getCombinedAndGenderScores() {
		ArrayList<Scores> results;
		try {
			results = fyiDao.getCombinedAndGenderScore(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/PostScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> postScore(@RequestBody ScoreChoice scoreChoice) {
		int rowUpdated;
		try {
			rowUpdated = fyiDao.updateProfile(userManager.getUserId(), scoreChoice);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowUpdated != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UserInterestCodes", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<ScoreChoice>> getProfile() {
		ArrayList<ScoreChoice> results;
		try {
			results = fyiDao.getProfile(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		int size = results.size();
		if (size > 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(results, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/CheckProfileExists", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> getProfileExist() {
		int numRowsSelected;
		try {
			numRowsSelected = fyiDao.getProfileExist(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(numRowsSelected, HttpStatus.OK);
	}

	@RequestMapping(value = "/ScoreChoice", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Map<String, String>> getScoreChoice() {
		String scoreChoice;
		try {
			scoreChoice = fyiDao.getScoreChoice(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(Collections.singletonMap("scoreChoice", scoreChoice), HttpStatus.OK);
	}

	@RequestMapping(value = "/get-tied-selection", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<TiedSelection> getTiedSelection() {
		TiedSelection result;
		try {
			result = fyiDao.getTiedSelection(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/insert-tied-selection", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> insertTiedSelection(@RequestBody TiedSelection tiedObject) {
		int rowsAffected;
		try {
			rowsAffected = fyiDao.insertTiedSelection(userManager.getUserId(), tiedObject);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (rowsAffected < 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/delete-tied-selection", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteTiedSelection() {
		int rowsAffected;
		try {
			rowsAffected = fyiDao.deleteTiedSelection(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/merge-account/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public @ResponseBody void mergeAccount(
			@PathVariable Integer oldUserId,
			@PathVariable Integer newUserId,
			FYIMerge fyiObject) {
		if(fyiObject != null && fyiObject.getUserId().intValue() != newUserId.intValue()) {
			try {
				int oldTestId = fyiObject.getTestId();
				// insert new profile
				fyiDao.mergeProfile(oldUserId, newUserId);
				fyiObject.setUserId(newUserId);
				// insert new test
				fyiDao.mergeTests(fyiObject);
				fyiDao.mergeResults(oldUserId, newUserId);
				// check to see if the user has old FYI
				if (fyiDao.getWhichLastFyiTableUsed(oldUserId, oldTestId) == 1) {
					fyiDao.mergeAnswers(oldUserId, newUserId, fyiObject.getTestId());
				} else {
					fyiDao.mergeResponse(oldUserId, fyiObject.getTestId());
				}
			} catch (Exception e) {
				logger.error("Error", e);
			}
		}
	}
}