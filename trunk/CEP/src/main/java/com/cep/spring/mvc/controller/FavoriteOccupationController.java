package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.FavorateOccupationDAO;
import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

@Controller
@RequestMapping("/favorite")
public class FavoriteOccupationController {

    private final Logger logger = LogManager.getLogger();
	private final UserManager userManager;
	private final FavorateOccupationDAO dao;

	@Autowired
	public FavoriteOccupationController(FavorateOccupationDAO dao, UserManager userManager) {
		this.dao = dao;
		this.userManager = userManager;
	}

	@RequestMapping(value = "/getFavoriteOccupation", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteOccupation>> getFavoriteOccupation() {
		ArrayList<FavoriteOccupation> results;
		try {
			results = dao.getFavoriteOccupation(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertFavoriteOccupation", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteOccupation> insertFavoriteOccupation(
			@RequestBody FavoriteOccupation favoriteObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertFavoriteOccupation(favoriteObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(favoriteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateFavoriteOccupationHearts", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> updateFavoriteOccupationHearts(@RequestBody FavoriteOccupation model) {
		int rowsAffected;
		try {
			rowsAffected = dao.updateFavoriteOccupationHearts(model, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (rowsAffected <= 0) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteFavoriteOccupation/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteOccupation(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteFavoriteOccupation(id, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getFavoriteCareerCluster", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteCareerCluster>> getFavoriteCareerCluster() {
		ArrayList<FavoriteCareerCluster> results;
		try {
			results = dao.getFavoriteCareerCluster(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error: ", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertFavoriteCareerCluster", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteCareerCluster> insertFavoriteCareerCluster(
			@RequestBody FavoriteCareerCluster favoriteObject) {
		int rowsInserted;
		try {
			rowsInserted = dao.insertFavoriteCareerCluster(favoriteObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(favoriteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/deleteFavoriteCareerCluster/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteCareerCluster(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteFavoriteCareerCluster(id, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getFavoriteSchool", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteSchool>> getFavoriteSchool() {
		ArrayList<FavoriteSchool> results;
		try {
			results = dao.getFavoriteSchool(userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/getFavoriteSchool/{socId}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<FavoriteSchool>> getFavoriteSchoolsBySocId(@PathVariable String socId) {
		ArrayList<FavoriteSchool> results;
		try {
			results = dao.getFavoriteSchoolBySocId(socId, userManager.getUserId());
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertFavoriteSchool", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<FavoriteSchool> insertFavoriteSchool(
			@RequestBody FavoriteSchool favoriteObject) {
	    // set the user id from session
		int rowsInserted;
		try {
			rowsInserted = dao.insertFavoriteSchool(favoriteObject, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(favoriteObject, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/deleteFavoriteSchool/{id}/", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> deleteFavoriteSchool(@PathVariable Integer id) {
		int result;
		try {
			result = dao.deleteFavoriteSchool(id, userManager.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (result != 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/merge-favorates/{oldUserId}/{newUserId}/", method = RequestMethod.GET)
	public void mergeFavorates(@PathVariable int oldUserId, @PathVariable int newUserId) {
		try {
			
			List<FavoriteCareerCluster> oldFavoratesCareerClusterList = dao.getFavoriteCareerCluster(oldUserId);
			List<FavoriteOccupation> oldFavoratesOccupationList  = dao.getFavoriteOccupation(oldUserId);
			List<FavoriteSchool> oldFavoriteSchoolList  = dao.getFavoriteSchool(oldUserId);
			List<FavoriteCitmOccupation> oldFavoratesCitmOccupationList  = dao.getFavoriteCitmOccupation(oldUserId);
			List<FavoriteMilitaryService> oldFavoriteMilitaryServiceList  = dao.getFavoriteService(oldUserId);
			
			if (oldFavoratesCareerClusterList.size()>0) {
				List<FavoriteCareerCluster> careerClusterList = new ArrayList<>();
				Set<String> uniqueCareerClusters = new HashSet<>();
				
				
				List<FavoriteCareerCluster> newFavoratesCareerClusterList = dao.getFavoriteCareerCluster(newUserId);
				newFavoratesCareerClusterList.forEach(a-> uniqueCareerClusters.add(a.getTitle()));
				

				oldFavoratesCareerClusterList.forEach(a-> { if(!uniqueCareerClusters.contains(a.getTitle())) 
															careerClusterList.add(a);});
				
				if (careerClusterList.size()>0)
					careerClusterList.forEach(a-> {
						try {
							dao.insertFavoriteCareerCluster(a, newUserId);
							} catch (Exception e) {
								logger.error("Error", e);
							}
					});
			}
			if(oldFavoratesOccupationList.size()>0) {
				List<FavoriteOccupation> occupationList = new ArrayList<>();
				Set<String> uniqueOccupations = new HashSet<>();
				
				List<FavoriteOccupation> newFavoratesOccupationList  = dao.getFavoriteOccupation(newUserId);
				newFavoratesOccupationList.forEach(a-> uniqueOccupations.add(a.getOnetSocCd()));

				oldFavoratesOccupationList.forEach(a->{ if(!uniqueOccupations.contains(a.getOnetSocCd())) 
														occupationList.add(a);});

				if (occupationList.size()>0)
					occupationList.forEach(a-> {
						try {
							dao.insertFavoriteOccupation(a, newUserId);
						} catch (Exception e) {
							logger.error("Error", e);
						}
					});
			}
			if(oldFavoriteSchoolList.size()>0) {
				List<FavoriteSchool> schoolList = new ArrayList<>();
				Set<Integer> uniqueSchools = new HashSet<>();

		
				List<FavoriteSchool> newFavoratesSchoolList  = dao.getFavoriteSchool(newUserId);
				newFavoratesSchoolList.forEach(a-> uniqueSchools.add(a.getUnitId()));
				

				oldFavoriteSchoolList.forEach(a->{ if(!uniqueSchools.contains(a.getUnitId())) 
														schoolList.add(a);});
				
				if (schoolList.size()>0)
					schoolList.forEach(a-> {
						try {
							dao.insertFavoriteSchool(a, newUserId);
						} catch (Exception e) {
							logger.error("Error", e);
						}
					});
			}
			if(oldFavoratesCitmOccupationList.size()>0) {
				List<FavoriteCitmOccupation> FavoriteCitmOccupationList = new ArrayList<>();
				Set<String> uniqueCitmOccupation = new HashSet<>();
				
				List<FavoriteCitmOccupation> newFavoratesCitmOccupationList  = dao.getFavoriteCitmOccupation(newUserId);
				newFavoratesCitmOccupationList.forEach(a-> uniqueCitmOccupation.add(a.getMcId()));
				

				oldFavoratesCitmOccupationList.forEach(a->{ if(!uniqueCitmOccupation.contains(a.getMcId())) 
														FavoriteCitmOccupationList.add(a);
													});
			
				if (FavoriteCitmOccupationList.size()>0)
					FavoriteCitmOccupationList.forEach(a-> {
						try {
							dao.insertFavoriteCitmOccupation(a, newUserId);
						} catch (Exception e) {
							logger.error("Error", e);
						}
					});
			}
			if(oldFavoriteMilitaryServiceList.size()>0) {
				List<FavoriteMilitaryService> FavoriteMilitaryServiceList = new ArrayList<>();
				Set<String> uniqueMilitaryService = new HashSet<>();
				
				List<FavoriteMilitaryService> newFavoriteMilitaryServiceList  = dao.getFavoriteService(newUserId);
				newFavoriteMilitaryServiceList.forEach(a-> uniqueMilitaryService.add(a.getSvcId()));
				

				oldFavoriteMilitaryServiceList.forEach(a->{ if(!uniqueMilitaryService.contains(a.getSvcId())) 
													FavoriteMilitaryServiceList.add(a);
													});
			
				if (FavoriteMilitaryServiceList.size()>0)
					FavoriteMilitaryServiceList.forEach(a-> {
						try {
							dao.insertFavoriteService(a, newUserId);
						} catch (Exception e) {
							logger.error("Error", e);
						}
					});
			}
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
}
