package com.cep.spring.utils;

import javax.servlet.http.Cookie;

/**
 * @author jcidras
 * Helper class to create cookies
 */
public class CookieHelper {

    /**
     * @param name The name of the cookie
     * @param value The value of the cookie
     * @param domain (Optional) the domain of the cookie. If no domain, set to null
     * @param path The path of the cookie. If for SESSION, set to base of server URL (e.g., /CEP/ or /CITM/)
     * @param maxAge The age of the cookie in seconds. If expiring, set to 0.
     * @param isSecure determines if the cookie will use SSL
     * @param isHttpOnly determines if the cookie should be HTTP Only
     * @return cookie
     */
    public static Cookie addCookie(String name,
                                   String value,
                                   String domain,
                                   String path,
                                   int maxAge,
                                   boolean isSecure,
                                   boolean isHttpOnly) {
        Cookie cookie = new Cookie(name, value);
        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (path != null) {
            cookie.setPath(path);
        }
        cookie.setMaxAge(maxAge);
        cookie.setSecure(isSecure);
        cookie.setHttpOnly(isHttpOnly);
        return cookie;
    }
}
