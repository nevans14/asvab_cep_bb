package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.ClassroomActivity.ClassroomActivity;
import com.cep.spring.model.ClassroomActivity.ClassroomActivityCategory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ClassroomActivityMapper {

    List<ClassroomActivityCategory> getClassroomActivityCategories();

    List<ClassroomActivity> getClassroomActivities(@Param("role") String role);

}
