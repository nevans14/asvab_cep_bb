package com.cep.spring.dao.mybatis;

import com.cep.spring.model.*;

import java.util.List;

public interface RegistrationDAO {

	Users getUser(Long userId, String username);
	
	Users getByUsername(String username);
	
	int insertUser(Users model);
	
	int updateUser(Users model);

	int updateUsername(Users model);

	int doesUsernameExists(String username);
	
	int insertUserRole(String username, String role);

	int deleteUserRole(String username);
	
	void mergeAccounts(AccessCodesMerge registrationObject);
	
	AccessCodes getAccessCodes(String accessCode, Integer userId);
	
	int insertAccessCode(AccessCodes newAccessCodeObj);
	
	int updateAccessCode(AccessCodes model);
	
	Users getUserId(String emailAddress);
	
	void disableAccessCode(String accessCode);
	
	void changeAccessCodePointer(Integer userId, String emailAddress);
	
	Meps getUsersMeps(String accessCode);
	
	int insertAsvabLog(Integer userId);
	
	AccessCodes getLastPromoAccessCodeUsed(String accessCode);
	
	int insertTeacherProfession(Integer userId, String subject);
	
	int insertSubscription(String emailAddress);

	List<RegistrationDropDown> getFunctionList();

	List<RegistrationDropDown> getConferenceList();

	int insertCep123Registration(Integer userId, Registration model);
}
