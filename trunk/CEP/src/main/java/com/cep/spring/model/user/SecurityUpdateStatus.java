package com.cep.spring.model.user;

public class SecurityUpdateStatus {
    private boolean isUsernameUpdated, isPasswordUpdated;
    private int statusCode;
    private String statusMessage;

    public void setIsUsernameUpdated(boolean isUsernameUpdated) { this.isUsernameUpdated = isUsernameUpdated; }
    public void setIsPasswordUpdated(boolean isPasswordUpdated) {
        this.isPasswordUpdated = isPasswordUpdated;
    }

    public boolean getIsUsernameUpdated() {
        return isUsernameUpdated;
    }
    public boolean getIsPasswordUpdated() {
        return isPasswordUpdated;
    }

    public void setStatusCode(int statusCode) { this.statusCode = statusCode; }
    public int getStatusCode() { return statusCode; }

    public void setStatusMessage(String statusMessage) { this.statusMessage = statusMessage; }
    public String getStatusMessage() { return statusMessage; }
}
