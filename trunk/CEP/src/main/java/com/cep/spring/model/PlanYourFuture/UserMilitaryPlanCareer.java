package com.cep.spring.model.PlanYourFuture;

import java.io.Serializable;

public class UserMilitaryPlanCareer implements Serializable {
    private static final long serialVersionUID = 8927622070854102849L;

    private int id, militaryPlanId;
    private String mcId, moc, svc, mpc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMilitaryPlanId() {
        return militaryPlanId;
    }

    public void setMilitaryPlanId(int militaryPlanId) {
        this.militaryPlanId = militaryPlanId;
    }

    public String getMcId() {
        return mcId;
    }

    public void setMcId(String mcId) {
        this.mcId = mcId;
    }

    public String getMoc() {
        return moc;
    }

    public void setMoc(String moc) {
        this.moc = moc;
    }

    public String getSvc() { return svc; }

    public void setSvc(String svc) { this.svc = svc; }

    public String getMpc() { return mpc; }

    public void setMpc(String mpc) { this.mpc = mpc; }
}
