package com.cep.spring.model.PlanYourFuture;

import com.cep.spring.model.occufind.SchoolProfile;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class UserCollegePlan implements Serializable {
    private static final long serialVersionUID = -4398759258791568289L;

    private int id, planId, unitId;
    private Boolean isSeekingInStateTuition;
    private String degree, major;
    private Double familyContribution, scholarshipContribution, savings, loan, other;
    private SchoolProfile schoolProfile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public Boolean getIsSeekingInStateTuition() {
        return isSeekingInStateTuition;
    }

    public void setIsSeekingInStateTuition(Boolean isSeekingInStateTuition) {
        this.isSeekingInStateTuition = isSeekingInStateTuition;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        if (degree != null && !degree.isEmpty()) {
            this.degree = Jsoup.clean(degree, Whitelist.basic());
        }
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        if (major != null && !major.isEmpty()) {
            this.major = Jsoup.clean(major, Whitelist.basic());
        }
    }

    public Double getFamilyContribution() {
        return familyContribution;
    }

    public void setFamilyContribution(Double familyContribution) {
        this.familyContribution = familyContribution;
    }

    public Double getScholarshipContribution() {
        return scholarshipContribution;
    }

    public void setScholarshipContribution(Double scholarshipContribution) {
        this.scholarshipContribution = scholarshipContribution;
    }

    public Double getSavings() {
        return savings;
    }

    public void setSavings(Double savings) {
        this.savings = savings;
    }

    public Double getLoan() {
        return loan;
    }

    public void setLoan(Double loan) {
        this.loan = loan;
    }

    public Double getOther() {
        return other;
    }

    public void setOther(Double other) {
        this.other = other;
    }

    public SchoolProfile getSchoolProfile() { return schoolProfile; }

    public void setSchoolProfile(SchoolProfile schoolProfile) { this.schoolProfile = schoolProfile; }
}
