package com.cep.spring.model.user;

public class AccessCode {
    private String accessCode, role;
    private Short unlimitedFyi;

    public AccessCode(String accessCode, String role, Short unlimitedFyi) {
        this.accessCode = accessCode;
        this.role = role;
        this.unlimitedFyi = unlimitedFyi;
    }

    public void setAccessCode(String accessCode) { this.accessCode = accessCode; }
    public String getAccessCode() { return accessCode; }

    public void setRole(String role) { this.role = role; }
    public String getRole() { return role; }

    public void setUnlimitedFyi(Short unlimitedFyi) { this.unlimitedFyi = unlimitedFyi; }
    public short getUnlimitedFyi() {
        // if, for whatever reason, this field is null,
        // then return 0 (NOT unlimited FYI)
        if (unlimitedFyi == null)
            return 0;
        return unlimitedFyi;
    }
}
