package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import com.cep.spring.model.fyi.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.FindYourInterestDAO;
import com.cep.spring.dao.mybatis.service.FindYourInterestService;
import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIRawScore;
import com.cep.spring.model.GenderScore;

@Repository
public class FindYourInterestDAOImpl implements FindYourInterestDAO {

	private final FindYourInterestService service;

	@Autowired
	public FindYourInterestDAOImpl(FindYourInterestService service) {
		this.service = service;
	}

	public boolean insertProfileAndTest(FindYourInterestProfileV2 fyiObject) {
		return service.insertProfileAndTest(fyiObject);
	}

	public int insertTest(FindYourInterestProfileV2 model) {
		return service.insertTest(model);
	}

	public ArrayList<Scores> getCombinedAndGenderScore(Integer userId) { return service.getCombinedAndGenderScore(userId); }

	public ArrayList<GenderScore> getGenderScore(FYIRawScore rawScores, char gender) {
		return service.getGenderScore(gender,
				rawScores.getrRawScore(),
				rawScores.getiRawScore(),
				rawScores.getaRawScore(),
				rawScores.getsRawScore(),
				rawScores.geteRawScore(),
				rawScores.getcRawScore());
	}

	public ArrayList<CombinedScore> getCombinedScore(FYIRawScore rawScores) {
		return service.getCombinedScore(
				rawScores.getrRawScore(),
				rawScores.getiRawScore(),
				rawScores.getaRawScore(),
				rawScores.getsRawScore(),
				rawScores.geteRawScore(),
				rawScores.getcRawScore());
	}

	public Integer getNumberTestTaken(Integer oldUserId, Integer newUserId) {
		if(oldUserId == null)
			oldUserId = 9090909;
		return service.getNumberTestTaken(oldUserId, newUserId);
	}

	public Integer getOldUserId(Integer newUserId) { return service.getOldUserId(newUserId); }

	public int updateProfile(int userId, ScoreChoice scoreChoice) {	return service.updateProfile(userId, scoreChoice); }

	public ArrayList<ScoreChoice> getProfile(Integer userId) { return service.getProfile(userId); }

	public Integer getProfileExist(Integer userId) { return service.getProfileExist(userId); }

	public String getScoreChoice(Integer userId) { return service.getScoreChoice(userId); }

	public TiedSelection getTiedSelection(Integer userId) { return service.getTiedSelection(userId); }

	public int insertTiedSelection(int userId, TiedSelection tiedObject) { return service.insertTiedSelection(userId, tiedObject); }

	public int deleteTiedSelection(int userId) { return service.deleteTiedSelection(userId); }

	public int getWhichLastFyiTableUsed(int oldUserId, int oldTestId) {return service.getWhichLastFyiTableUsed(oldUserId, oldTestId);}

	public int mergeProfile(int oldUserId, int newUserId) { return service.mergeProfile(oldUserId, newUserId); }

	public int mergeTests(FYIMerge fyiObject) {	return service.mergeTests(fyiObject); }

	public int mergeResults(int oldUserId, int newUserId) {	return service.mergeResults(oldUserId, newUserId); }

	public int mergeAnswers(int oldUserId, int newUserId, int testId) { return service.mergeAnswers(oldUserId, newUserId, testId);}

	public int mergeResponse(int oldUserId, int newTestId) { return service.mergeResponse(oldUserId, newTestId); }

	public FYIMerge getLastTestObject(int oldUserId, int newUserId) { return service.getLastTestObject(oldUserId, newUserId); }

	public int deleteProfile(int userId) { return service.deleteProfile(userId); }
}
