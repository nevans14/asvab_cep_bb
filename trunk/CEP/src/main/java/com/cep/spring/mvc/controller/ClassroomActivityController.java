package com.cep.spring.mvc.controller;

import com.cep.spring.dao.mybatis.ClassroomActivityDAO;
import com.cep.spring.model.ClassroomActivity.ClassroomActivity;
import com.cep.spring.model.ClassroomActivity.ClassroomActivityCategory;
import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping(value = "/classroom-activity")
@RestController
public class ClassroomActivityController {

    private final Logger logger = LogManager.getLogger();
    private final ClassroomActivityDAO dao;
    private final UserManager userManager;

    @Autowired
    public ClassroomActivityController(ClassroomActivityDAO dao, UserManager userManager) {
        this.dao = dao;
        this.userManager = userManager;
    }

    @RequestMapping(value = "/get-categories", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<ClassroomActivityCategory>> getCategories() {
        List<ClassroomActivityCategory> results;
        try {
            results = dao.getClassroomActivityCategories();
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/get-activities", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<ClassroomActivity>> getActivities() {
        List<ClassroomActivity> results;
        try {
            // get the user role
            String role = userManager.getUserRole();

            results = dao.getClassroomActivities(role);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }
}
