package com.cep.spring.model.ClassroomActivity;

public class ClassroomActivityPdf {

    private Integer id;
    private Integer activityId;
    private String role, title, s3Filename;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getS3Filename() {
        return s3Filename;
    }

    public void setS3Filename(String s3Filename) {
        this.s3Filename = s3Filename;
    }
}
