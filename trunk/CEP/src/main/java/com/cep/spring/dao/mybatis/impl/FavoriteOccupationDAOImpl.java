package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.FavorateOccupationDAO;
import com.cep.spring.dao.mybatis.service.FavoriteOccupationService;
import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

@Primary
@Repository
public class FavoriteOccupationDAOImpl implements FavorateOccupationDAO {

	private final FavoriteOccupationService service;

	@Autowired
	public FavoriteOccupationDAOImpl(FavoriteOccupationService service) {
		this.service = service;
	}

	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) {
		return service.getFavoriteOccupation(userId);
	}

	public int insertFavoriteOccupation(FavoriteOccupation favoriteObject, Integer userId) {
		return service.insertFavoriteOccupation(favoriteObject, userId);
	}

	public int updateFavoriteOccupationHearts(FavoriteOccupation model, Integer userId) {
		return service.updateFavoriteOccupationHearts(model, userId);
	}

	public int deleteFavoriteOccupation(Integer id, Integer userId) {
		return service.deleteFavoriteOccupation(id, userId);
	}

	public int deleteFavoriteOccupationByUserId(Integer UserId) {
		return service.deleteFavoriteOccupationByUserId(UserId);
	}

	public ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(Integer userId) {
		return service.getFavoriteCareerCluster(userId);
	}

	public int insertFavoriteCareerCluster(FavoriteCareerCluster favoriteObject, Integer userId) {
		return service.insertFavoriteCareerCluster(favoriteObject, userId);
	}

	public int deleteFavoriteCareerCluster(Integer id, Integer userId) {
		return service.deleteFavoriteCareerCluster(id, userId);
	}

	public int deleteFavoriteCareerClusterByUserId(Integer userId) {
		return service.deleteFavoriteCareerClusterByUserId(userId);
	}
	
	public ArrayList<FavoriteSchool> getFavoriteSchool(Integer userId) {
		return service.getFavoriteSchool(userId);
	}

	public ArrayList<FavoriteSchool> getFavoriteSchoolBySocId(String socId, int userId) { return service.getFavoriteSchoolBySocId(socId, userId); }

	public int insertFavoriteSchool(FavoriteSchool favoriteObject, Integer userId) {
		return service.insertFavoriteSchool(favoriteObject, userId);
	}


	public int deleteFavoriteSchool(Integer id, Integer userId) {
		return service.deleteFavoriteSchool(id, userId);
	}
	
	public int deleteFavoriteSchoolByUserId(Integer userId) {
		return service.deleteFavoriteSchoolByUserId(userId);
	}

	public int insertFavoriteCitmOccupation(FavoriteCitmOccupation favoriteObject, Integer userId) {
		return service.insertFavoriteCitmOccupation(favoriteObject, userId);
	}

	public int deleteFavoriteCitmOccupationByUserId(Integer userId) {
		return service.deleteFavoriteCitmOccupationByUserId(userId);
	}

	public ArrayList<FavoriteCitmOccupation> getFavoriteCitmOccupation(Integer userId) {
		return service.getFavoriteCitmOccupation(userId);
	}

	public int insertFavoriteService(FavoriteMilitaryService favoriteObject, Integer userId) {
		return service.insertFavoriteService(favoriteObject, userId);
	}

	public int deleteFavoriteServiceByUserId(Integer userId) {
		return service.deleteFavoriteServiceByUserId(userId);
	}

	public ArrayList<FavoriteMilitaryService> getFavoriteService(Integer userId) {
		return service.getFavoriteService(userId);
	}
}
