package com.cep.spring.model.favorites;

public class FavoriteMilitaryService {

	private Integer id;
	private String svcId;

	/**
	 * @return the svcId
	 */
	public String getSvcId() {
		return svcId;
	}
	/**
	 * @param svcId the svcId to set
	 */
	public void setSvcId(String svcId) {
		this.svcId = svcId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
