package com.cep.spring.model.PlanYourFuture;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class UserOccupationPlanCalendar implements Serializable {
    private static final long serialVersionUID = 840697950278574929L;

    private int id;
    private Integer planId;
    private String title;
    private String date;
    private boolean isGraduationDate;
    private ArrayList<UserOccupationPlanCalendarTask> tasks;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title != null && !title.isEmpty()) {
            this.title = Jsoup.clean(title, Whitelist.basic());
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) throws ParseException {
        Date dt;
        try {
            // coming from UI, if fails (maybe coming from db)
            dt = new SimpleDateFormat("dd/MM/yyyy").parse(date);
            this.date = new SimpleDateFormat("MM/dd/yyyy").format(dt);
        } catch (ParseException e) {
            // maybe coming from db, if fails, then invalid format
            dt = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            this.date = new SimpleDateFormat("dd/MM/yyyy").format(dt);
        }
    }

    public boolean getIsGraduationDate() { return isGraduationDate; }

    public void setIsGraduationDate(boolean isGraduationDate) { this.isGraduationDate = isGraduationDate; }

    public ArrayList<UserOccupationPlanCalendarTask> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<UserOccupationPlanCalendarTask> tasks) {
        this.tasks = tasks;
    }
}
