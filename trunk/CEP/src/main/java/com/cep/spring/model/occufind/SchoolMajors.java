package com.cep.spring.model.occufind;

import java.io.Serializable;
import java.util.List;

public class SchoolMajors implements Serializable {

	private static final long serialVersionUID = -3382657267813765763L;

	private String cipCode;
	private String cipTitle;
	private String cipDesctription;
	private String socCode;
	private String socTitle;
	private String onetCode;
	private String onetTitle;
	private String onetDescription;
	
	public String getCipCode() {
		return cipCode;
	}
	public void setCipCode(String cipCode) {
		this.cipCode = cipCode;
	}
	public String getCipTitle() {
		return cipTitle;
	}
	public void setCipTitle(String cipTitle) {
		this.cipTitle = cipTitle;
	}
	public String getSocCode() {
		return socCode;
	}
	public void setSocCode(String socCode) {
		this.socCode = socCode;
	}
	public String getSocTitle() {
		return socTitle;
	}
	public void setSocTitle(String socTitle) {
		this.socTitle = socTitle;
	}
	public String getOnetCode() {
		return onetCode;
	}
	public void setOnetCode(String onetCode) {
		this.onetCode = onetCode;
	}
	public String getOnetTitle() {
		return onetTitle;
	}
	public void setOnetTitle(String onetTitle) {
		this.onetTitle = onetTitle;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getOnetDescription() {
		return onetDescription;
	}
	public void setOnetDescription(String onetDescription) {
		this.onetDescription = onetDescription;
	}
	public String getCipDesctription() {
		return cipDesctription;
	}
	public void setCipDesctription(String cipDesctription) {
		this.cipDesctription = cipDesctription;
	}
}
