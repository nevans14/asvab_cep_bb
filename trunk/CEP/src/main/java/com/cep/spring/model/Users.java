package com.cep.spring.model;

import java.util.List;
import java.util.Date;

public class Users {

	private Integer userId;
    private String username, password, resetKey, system, studentId;
    private Short enabled;
    private Date resetExpireDate;    
    private AccessCodes accessCodes;
    private List<UserRoles> userRoles;
    // default constructor
    public Users() {}
    // parameterized constructor
    public Users (Integer userId, String username, String password, Short enabled, String system, String studentId) {
    	this.userId = userId;
    	this.username = username;
    	this.password = password;
    	this.enabled = enabled;
    	this.system = system;
    	this.studentId = studentId;
    }
    // accessors
	public Integer getUserId() { return userId;	}
	public String getUsername() { return username; }
	public String getPassword() { return password; }
	public Short getEnabled() { return enabled; }
	public String getResetKey() { return resetKey; }
	public Date getResetExpireDate() { return resetExpireDate; }
	public String getSystem() {	return system; }
	public String getStudentId() { return studentId; }
	public AccessCodes getAccessCodes() { return accessCodes; }
	public List<UserRoles> getUserRoles() { return userRoles; }
	// mutators
	public void setUserId(Integer userId) {	this.userId = userId; }	
	public void setUsername(String username) { this.username = username; }
	public void setPassword(String password) { this.password = password; }
	public void setEnabled(Short enabled) { this.enabled = enabled; }	
	public void setResetKey(String resetKey) { this.resetKey = resetKey; }
	public void setResetExpireDate(Date resetExprieDate) { this.resetExpireDate = resetExprieDate; }
	public void setSystem(String system) { this.system = system; }
	public void setStudentId(String studentId) { this.studentId = studentId; }
	public void setAccessCode(AccessCodes accessCodes) { this.accessCodes = accessCodes; }
	public void setUserRoles(List<UserRoles> userRoles) { this.userRoles = userRoles; }
}
