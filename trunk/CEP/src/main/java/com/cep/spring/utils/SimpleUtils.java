package com.cep.spring.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;


/**
 * SimpleUtils -  Simple Shared Utilities that are easy to implement but are nice not having to re-implement for
 *   each class that needs this functionality.  
 *   
 *   Note:
 *   Typically these methods are synchronized even if though they don't 
 *   have class level variables.  It maybe over kill but a good reminder for developers to recognizes these methods
 *   shared by many different threads.
 * 
 * @author Dave Springer
 *
 */
public class SimpleUtils {

	private static final String SIMPLE_DATE_FORMAT =  "yyyy-MM-dd HH:mm:ss.SSS";
	/**
     * calculateDuration : Simple output the number of hours, minutes, seconds and milliseconds in a Long representing milliseconds.
     * 
     * example usage:
     *  
        Long startTime = System.currentTimeMillis();
		    // Do some work to time here ....
		Long endTime = System.currentTimeMillis();
		String strDiff = SimpleUtils.calculateDuration(  endTime - startTime ) ;
		
		System.out.println(strDiff);
		
	 * Console output:
	 *  The Duration was : 0 hours 0 minutes 10 seconds and 218 milliseconds
     * 
     * @param timeDelta   Time of duration, typically EndTime - StartTime
     * @return            Returns string listing the number of hours, minutes, seconds and milliseconds
     * 
     * 
     */
    public synchronized static String calculateDuration(Long timeDelta){
        long mili = timeDelta;
        // Calculate hours 
        long hour = mili / 3600000L;
        // Calculate minutes 
        long minute = mili - (hour * 3600000L);
        // Calculate seconds 
        minute = minute / 60000L;
        long sec = mili - (hour * 3600000L) - (minute * 60000L);
        sec = sec / 1000L;
        // Calculate millis 
        mili = mili - (hour * 3600000L) - (minute * 60000L) - (sec * 1000L);

        return "The Durration was : " +  hour + " hours " +
                minute + " minutes " + sec + " seconds and " +
                mili + " milliseconds";
    }

	/**
	 * loadApplicationScenerioContext  -  Loads a XML string to contextMap with scenario as the key.
	 * @param customerScenerio = Key to the map representing the appContext defined as contextString.
	 * @param applicationContextString    = Spring XML appContext as as a string converted to an appContext and stored in map.
	 */
	public ApplicationContext loadApplicationScenerioContext(String customerScenerio, String applicationContextString) {
	   Resource resource = new ByteArrayResource(applicationContextString.getBytes());
	   GenericXmlApplicationContext springContext = new GenericXmlApplicationContext();
	   springContext.load(resource);
	   return springContext;
	}
	
	public static String removeLast(String source,String removeStr){
		return source.substring(0, source.lastIndexOf(removeStr));
	}

	public static Date getDateFromStringInStandardFormat(String dateString) {
		DateFormat format = new SimpleDateFormat(SIMPLE_DATE_FORMAT, Locale.ENGLISH);	
		Date date = null;
		 try {
			date = format.parse(dateString);
			format.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	

	public static String getStringInStandardFormatFromDate(Date date) {
		DateFormat format = new SimpleDateFormat(SIMPLE_DATE_FORMAT, Locale.ENGLISH);	
		return format.format(date);
	}
	
	public static Date datePlusDays( Date originalDate, Integer days ) {
		Calendar calendar = Calendar.getInstance();
		calendar .setTime(originalDate);
		calendar .add(Calendar.DATE, days); 
		return calendar.getTime();
	}
	
	public static Date datePlusMonths( Date originalDate, Integer months ) {
		Calendar calendar = Calendar.getInstance();
		calendar .setTime(originalDate);
		calendar .add(Calendar.MONTH, months); 
		return calendar.getTime();
	}

	public static String getCurrentYearMonth() {
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        return dateFormat.format(Calendar.getInstance().getTime());
	}

	/**
	 * 
	 * @param srcDate Source Date
	 * @param destDate Destination Date
	 * @return  0  if equal
	 * 			-  negative if srcDate is before destDate
	 * 			+  positive if srcDate is after destDate 
	 */
	public static Long dateCompare(Date srcDate, Date destDate) {		
		Calendar cSrcDate = Calendar.getInstance();
		Calendar cDestDate = Calendar.getInstance();
		
		cSrcDate.setTime(srcDate);
		cDestDate.setTime(destDate);
		
		LocalDate lSrcDate = LocalDate.of(cSrcDate.get(Calendar.YEAR), (1 + cSrcDate.get(Calendar.MONTH)), cSrcDate.get(Calendar.DAY_OF_MONTH));
		LocalDate lDestDate = LocalDate.of(cDestDate.get(Calendar.YEAR), (1 + cDestDate.get(Calendar.MONTH)), cDestDate.get(Calendar.DAY_OF_MONTH));
		
		return (long) lSrcDate.compareTo(lDestDate);
	}
}
