package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.notes.CareerClusterNotes;
import com.cep.spring.model.notes.Notes;
import com.cep.spring.model.notes.SchoolNotes;

public interface NotesMapper {

	ArrayList<Notes> getNotes(@Param("userId") Integer userId);

	int insertNote(@Param("noteObject") Notes noteObject, @Param("userId") Integer userId);

	int updateNote(@Param("noteObject") Notes noteObject, @Param("userId") Integer userId);
	
	int deleteNote(@Param("userId") Integer userId, @Param("noteId") Integer noteId);

	String getOccupationNotes(@Param("userId") Integer userId, @Param("socId") String socId);
	
	ArrayList<CareerClusterNotes> getCareerClusterNotes(@Param("userId") Integer userId);

	int insertCareerClusterNote(@Param("noteObject") CareerClusterNotes noteObject, @Param("userId") Integer userId);

	int updateCareerClusterNote(@Param("noteObject") CareerClusterNotes noteObject, @Param("userId") Integer userId);
	
	int deleteCareerClusterNote(@Param("userId") Integer userId, @Param("ccNoteId") Integer ccNoteId);

	String getCareerClusterNote(@Param("userId") Integer userId, @Param("ccId") Integer ccId);
	
	ArrayList<SchoolNotes> getSchoolNotes(@Param("userId") Integer userId);

	int insertSchoolNote(@Param("noteObject") SchoolNotes noteObject, @Param("userId") Integer userId);

	int updateSchoolNote(@Param("noteObject") SchoolNotes noteObject, @Param("userId") Integer userId);
	
	int deleteSchoolNote(@Param("userId") Integer userId, @Param("schoolNoteId") Integer schoolNoteId);

	String getSchoolNote(@Param("userId") Integer userId, @Param("unitId") Integer unitId);

}
