package com.cep.spring.mvc.controller; 

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.jdbctemplate.AccessCodesDao;
import com.cep.spring.model.AccessCodes;



/**
 * Example Controller to show the basic CRUD operation of REST services
 * and how they are implemented in Spring.
 * 
 * @author Dave Springer
 *
 */
@Controller
@RequestMapping("/admin")
    public class AccessCodesController {

    private final Logger logger = LogManager.getLogger();
	
	// CRUD 
	// Crude  -> SQL     -> HTML   -> URL 
	// Create -> Insert  -> Put    -> /CEP/accessCodes
	// Read   -> Select  -> Get    -> /CEP/accessCodes
	// Update -> Update  -> Post   -> /CEP/accessCodes
	// Delete -> Delete  -> Delete -> /CEP/accessCodes

	private final AccessCodesDao acd;
	private final JdbcTemplate jdbcTemplate ;
	
	@Autowired
	public AccessCodesController(AccessCodesDao acd, JdbcTemplate jdbcTemplate) {
		this.acd = acd;
		this.jdbcTemplate = jdbcTemplate;
	}

	// Read   -> Select  -> Get    -> /CEP/accessCodes
	@RequestMapping(value = "/accessCodes", method = RequestMethod.GET)
	public @ResponseBody
		AccessCodes selectAccessCodes() {
	    logger.info("testing 1 2 3 ...");

		AccessCodes ac = new AccessCodes();
		ac.setUserId(99L);
		ac.setRole("T");
		ac.setAccessCode("Test Url ...");
		
		return ac;

	}
	
	// Create -> Insert  -> Post    -> /CEP/accessCodes	
	@RequestMapping(value = "/accessCodes", method = RequestMethod.POST)
	public @ResponseBody List<AccessCodes>  
	    createAccessCode(@RequestBody AccessCodes ac) {
	    logger.info("createAccessCode");
		List<AccessCodes> lac = new ArrayList<>();
		try {
			lac =  acd.readAccessCodesWithObject(ac);
		} catch (Exception e) {
			logger.error("Error", e);
		}
	 	return lac;
	}

	@RequestMapping(value = "/accessCodes/q/{code}", method = RequestMethod.GET)
	public @ResponseBody
			List<AccessCodes> selectAccessCodes(
					@PathVariable String code) {
	    logger.info("selectAccessCodes");
		List<AccessCodes> lac = new ArrayList<>();
		try {
			lac = acd.readAccessCodess(code);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return lac;
	}

	// Update -> Update  -> Put   -> /CEP/accessCodes
	@RequestMapping(value = "/accessCodes", method = RequestMethod.PUT)
	public @ResponseBody
			AccessCodes updateAccessCodes(@RequestBody AccessCodes ac) {
	    logger.info("updateAccessCodes");
//		logger.info("Update -> Update  -> Put");
//		logger.info("UserId:" + ac.getUserId() );
//		logger.info("AccessCode:" + ac.getAccessCode() );
//		logger.info("Role:" + ac.getRole() );
//		logger.info("ExpireDate:" + ac.getExpireDate() );		
		 
		return new AccessCodes();
	}

	
	// Delete -> Delete  -> Delete -> /CEP/accessCodes
	@RequestMapping(value = "/accessCodes/d/{code}", method = RequestMethod.DELETE)
	public @ResponseBody 
	void getAnEmployeeFavorites(@PathVariable String code) {
	    logger.info("Don't want REST access to delete access codes.");
	    // acd.delete(2L);
	}

	
	// Create -> Insert  -> Post    -> /CEP/accessCodes	
	@RequestMapping(value = "/accessCodes/generate", method = RequestMethod.POST)
	public @ResponseBody AccessCodes generateAccessCodesAccessCode(@RequestBody AccessCodes ac) {
	 	return new AccessCodes();
	}

//	// Read   -> Select  -> Get    -> /CEP/accessCodes
	@RequestMapping(value = "/accessCodes/download/{role}", method = RequestMethod.GET)
	public @ResponseBody List<AccessCodes> selectMarketingAccessCodes(@PathVariable String role) {
	    logger.info("selectMarketingAccessCodes");
		List<AccessCodes> lac = new ArrayList<>();
		try {
			lac = acd.readAccessCodesByRole(role);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return lac;
	}
	
//	// Read   -> Select  -> Get    -> /CEP/accessCodes
	@RequestMapping(value = "/accessCodes/reserve/{role}/{quantity}", method = RequestMethod.GET)
	public @ResponseBody List<AccessCodes> reserveMarketingAccessCodes(@PathVariable("role") String role, @PathVariable("quantity") Integer quantity) {
	    logger.info("reserveMarketingAccessCodes");
		List<AccessCodes> lac = new ArrayList<>();
		try {
			lac = acd.reserveAccessCodesByRole(role,quantity);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return lac;
	}
	

//	// Read   -> Select  -> Get    -> /CEP/accessCodes
	@RequestMapping(value = "/accessCodes/availibleForReservation", method = RequestMethod.GET)
	public @ResponseBody AvailbleAccessCodes availibleForReservation() {
	    logger.info("availibleForReservation");
		AvailbleAccessCodes aac = new AvailbleAccessCodes();
		String sql = "SELECT COUNT(user_id) as NumAvailble FROM access_codes WHERE expire_date is NULL AND role = ?";		
		try {
			Long count =  jdbcTemplate.queryForObject(sql, Long.class, "R");
			aac.setReserved(count);
			count =  jdbcTemplate.queryForObject(sql, Long.class, "C");
			aac.setCounselor(count);
			count =  jdbcTemplate.queryForObject(sql, Long.class, "M");
			aac.setMarketing(count);
			count =  jdbcTemplate.queryForObject(sql, Long.class, "3");
			aac.setThreeWeek(count);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return aac;
	}

//  // Read   -> Select  -> Get    -> /CEP/accessCodes
    @RequestMapping(value = "/accessCodes/accesscodesUseageReport/{schoolYear}", method = RequestMethod.GET)
    public @ResponseBody
    Map<String,String> accesscodesUseageReport(@PathVariable("schoolYear") Integer schoolYear) {
        logger.info("accesscodesUseageReport");
        Map<String,String> report = new HashMap<>();
        try {
			Long totalNumberOfLogins = jdbcTemplate.queryForObject(
					" SELECT COUNT (*) " +
							" FROM [dbo].[asvab_log] " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' "
					,Long.class);
			report.put("TotalNumberOfLogins", totalNumberOfLogins.toString());
			Long allUtilization = jdbcTemplate.queryForObject(
					" SELECT count( DISTINCT(user_id) )  " +
							" FROM [dbo].[asvab_log] " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' "
					,Long.class);
			report.put("AllUtilization", allUtilization.toString());
			Long marketingUtilization = jdbcTemplate.queryForObject(
					" SELECT count( DISTINCT(ac.user_id) )  " +
							" FROM [dbo].[asvab_log] al  " +
							" left join [dbo].[access_codes] ac on al.user_id = ac.user_id  " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							" AND   ac.role = 'M';"
					,Long.class);
			report.put("MarketingUtilization", marketingUtilization.toString());
			Long counselorUtilization = jdbcTemplate.queryForObject(
					" SELECT count( DISTINCT(ac.user_id) )  " +
							" FROM [dbo].[asvab_log] al  " +
							" left join [dbo].[access_codes] ac on al.user_id = ac.user_id  " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							" AND   ac.role = 'C';"
					,Long.class);
			report.put("CounselorUtilization", counselorUtilization.toString());
			Long studentUtilization = jdbcTemplate.queryForObject(
					" SELECT count( DISTINCT(ac.user_id) )  " +
							" FROM [dbo].[asvab_log] al  " +
							" left join [dbo].[access_codes] ac on al.user_id = ac.user_id  " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							" AND   ac.role = 'S';"
					,Long.class);
			report.put("StudentUtilization", studentUtilization.toString());
			Long replacementUtilization = jdbcTemplate.queryForObject(
					" SELECT count( DISTINCT(ac.user_id) )  " +
							" FROM [dbo].[asvab_log] al  " +
							" left join [dbo].[access_codes] ac on al.user_id = ac.user_id  " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							" AND   ac.role = 'R';"
					,Long.class);
			report.put("ReplacementUtilization", replacementUtilization.toString());
			Long marketingRepeats = jdbcTemplate.queryForObject(
					" SELECT count(*) FROM ( " +
							" SELECT user_id, count( *) as visits  " +
							"  FROM [dbo].[asvab_log] " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							"   GROUP BY user_id) a " +
							"   LEFT JOIN access_codes ac on a.user_id = ac.user_id  " +
							"   WHERE a.visits >= 2  " +
							"   AND ac.role = 'M' "
					,Long.class);
			report.put("MarketingRepeats", marketingRepeats.toString());
			Long counselorRepeats = jdbcTemplate.queryForObject(
					" SELECT count(*) FROM ( " +
							" SELECT user_id, count( *) as visits  " +
							"  FROM [dbo].[asvab_log] " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							"   GROUP BY user_id) a " +
							"   LEFT JOIN access_codes ac on a.user_id = ac.user_id  " +
							"   WHERE a.visits >= 2  " +
							"   AND ac.role = 'C' "
					,Long.class);
			report.put("CounselorRepeats", counselorRepeats.toString());
			Long studentRepeats = jdbcTemplate.queryForObject(
					" SELECT count(*) FROM ( " +
							" SELECT user_id, count( *) as visits  " +
							"  FROM [dbo].[asvab_log] " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							"   GROUP BY user_id) a " +
							"   LEFT JOIN access_codes ac on a.user_id = ac.user_id  " +
							"   WHERE a.visits >= 2  " +
							"   AND ac.role = 'S' "
					,Long.class);
			report.put("StudentRepeats", studentRepeats.toString());
			Long replacementRepeats = jdbcTemplate.queryForObject(
					" SELECT count(*) FROM ( " +
							" SELECT user_id, count( *) as visits  " +
							"  FROM [dbo].[asvab_log] " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							"   GROUP BY user_id) a " +
							"   LEFT JOIN access_codes ac on a.user_id = ac.user_id  " +
							"   WHERE a.visits >= 2  " +
							"   AND ac.role = 'R' "
					,Long.class);
			report.put("ReplacementRepeats", replacementRepeats.toString());
			Long allRepeats = jdbcTemplate.queryForObject(
					" SELECT count(*) FROM ( " +
							" SELECT user_id, count( *) as visits  " +
							"  FROM [dbo].[asvab_log] " +
							" WHERE login_date > '" + new Integer(schoolYear - 1).toString() + "-07-01 00:00:00.000' " +
							" AND   login_date < '" + new Integer(schoolYear).toString() + "-06-30 00:00:00.000' " +
							"   GROUP BY user_id) a " +
							"   LEFT JOIN access_codes ac on a.user_id = ac.user_id  " +
							"   WHERE a.visits >= 2  "
					,Long.class);
			report.put("AllRepeats", allRepeats.toString());
		} catch (Exception e) {
        	logger.error("Error", e);
		}
        return report;
    }


	/**
	 * 
	 * @author Dave Springer
	 *
	 */
	public class AvailbleAccessCodes {
		private Long reserved;
		private Long counselor;
		private Long threeWeek;
		private Long marketing;
		
		public Long getReserved() {
			return reserved;
		}
		public Long getCounselor() {
			return counselor;
		}
		public Long getThreeWeek() {
			return threeWeek;
		}
		public Long getMarketing() {
			return marketing;
		}
		public void setReserved(Long reserved) {
			this.reserved = reserved;
		}
		public void setCounselor(Long counselor) {
			this.counselor = counselor;
		}
		public void setThreeWeek(Long threeWeek) {
			this.threeWeek = threeWeek;
		}
		public void setMarketing(Long marketing) {
			this.marketing = marketing;
		}
	}
}