package com.cep.spring.mvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cep.spring.model.testscore.*;
import com.cep.spring.utils.DbInfoUtils;
import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.cep.spring.dao.mybatis.TestScoreDao;
import com.cep.spring.dao.mybatis.service.TestScoreService;

@Controller
@RequestMapping("/testScore")
public class TestScoreController {

	private final Logger logger = LogManager.getLogger();

	private final TestScoreDao dao;
	private final UserManager userManager;
	private final CSVFileCreator csvFileCreator;
	private final TestScoreService testScoreService;
	private final DbInfoUtils dbInfoUtils;

	@Autowired
	public TestScoreController(TestScoreDao dao, UserManager userManager, CSVFileCreator csvFileCreator, TestScoreService testScoreService, DbInfoUtils dbInfoUtils) {
		this.dao = dao;
		this.userManager = userManager;
		this.csvFileCreator = csvFileCreator;
		this.testScoreService = testScoreService;
		this.dbInfoUtils = dbInfoUtils;
	}

	@RequestMapping(value = "/InsertTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> insertTestScore(
			@RequestBody List<StudentTestingProgam> testScoreObjects) {
		logger.info("insertTestScore");
		int rowsInserted ;
		try {
			rowsInserted = dao.insertTestScoreimpl(testScoreObjects);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted == -1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(rowsInserted, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/get-test-score", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StudentTestingProgamSelects> getTestScore() {
		logger.info("get-test-scores");
		// get user session info
		String accessCode = userManager.getUserAccessCode();
		String role = userManager.getUserRole();
		StudentTestingProgamSelects results;
		try {
			// if the user is a role of "T"eacher, then get a mock ASVAB Score
			if (role.equals("E") || role.equals("T") || role.equals("D")) {
				boolean isUsingScore50 = dbInfoUtils.isScore50(accessCode);
				String mockAccessCode = isUsingScore50 ? "SCORE50" : "KATE-A";
				// get mock test scores
				results = dao.getTestScoreWithAccessCode(mockAccessCode);
			} else if (role.equals("P")) {
				results = dao.getTestScoreWithAccessCode("KATE-A");
			} else {
				results = dao.getTestScoreWithAccessCode(accessCode);
			}
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/get-student-score-results", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<StudentAsvabScoreResults> getStudentASRByAccessCode() {
		StudentAsvabScoreResults results;
		try {
			results = dao.getStudentASRByAccessCode(userManager.getUserAccessCode());
		} catch (Exception e) {
			logger.error("Error", e);
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(results);
	}
	
	@RequestMapping(value = "/filter-results/{filterScoreObj}/", method = RequestMethod.POST)
	public void filterTestScores(@PathVariable FilterScoreObj filterScoreObj, HttpServletResponse response) {
	    logger.info("getTestScoreWithAccessCode");
	    List<StudentTestingProgamSelects> studentTestingProgramObjects;
		StringBuilder sb = new StringBuilder();
		try {
			studentTestingProgramObjects = dao.filterTestScore(filterScoreObj);
			sb.append(csvFileCreator.getHeader());
			sb.append("\n");
			for(StudentTestingProgamSelects stp: studentTestingProgramObjects) {
				sb.append(csvFileCreator.writeSingleRecord(stp));
				sb.append("\n");
			}
			System.out.println(sb.toString());
			testScoreService.downloadFile(response, sb.toString(), filterScoreObj.getFileName());
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
	
	@RequestMapping(value = "/insertManualTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> insertManualTestScore(
			@RequestBody ManualTestScore manualTestScoreObj) {
		logger.info("insertManualTestScore");
		int rowsInserted;
		manualTestScoreObj.setUserId(userManager.getUserId());
		try {
			rowsInserted = dao.insertManualTestScore(manualTestScoreObj);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowsInserted != 1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(rowsInserted, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getManualTestScore/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ManualTestScore> getManualTestScore() {
	    logger.info("getManaulTestScore");
	    ManualTestScore manualTestScoreObj;
	    Integer userId = userManager.getUserId();
		try {
			manualTestScoreObj = dao.getManualTestScore(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(manualTestScoreObj, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateManualTestScore", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> updateManualTestScore(
			@RequestBody ManualTestScore manualTestScoreObj) {
		logger.info("updateManualTestScore");
		int rowUpdated;
		try {
			rowUpdated = dao.updateManualTestScore(manualTestScoreObj);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (rowUpdated != 1) {
			logger.error("Unexpected Response");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(rowUpdated, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/deleteManualTestScore/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> deleteManualTestScore() {
	    logger.info("getManaulTestScore");
	    Integer userId = userManager.getUserId();
	    int rowDeleted;
		try {
			rowDeleted = dao.deleteManualTestScore(userId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(rowDeleted, HttpStatus.OK);
	}
	
	public StudentTestingProgam wsClient(){
		StudentTestingProgam results = null;
		RestTemplate restTemplate = new RestTemplate();
		try {
			results = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/ap", StudentTestingProgam.class);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
}
