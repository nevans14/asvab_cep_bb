package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.PlanYourFuture.*;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;

public interface PlanYourFutureMapper {

    ArrayList<UserOccupationPlan> selectOccupationPlans(@Param("userId") int userId);

    UserOccupationPlan selectOccupationPlan(@Param("userId") int userId, @Param("id") int id);

    int insertOccupationPlan(@Param("userId") int userId, @Param("model") UserOccupationPlan model);

    int updateOccupationPlan(@Param("userId") int userId, @Param("id") int id, @Param("model") UserOccupationPlan model);

    int deleteOccupationPlan(@Param("userId") int userId, @Param("id") int id);

    ArrayList<UserOccupationPlanCalendar> selectPlanCalendars(@Param("userId") int userId, @Param("planId") int planId);

    ArrayList<UserOccupationPlanCalendar> selectCalendars(@Param("userId") int userId);

    UserOccupationPlanCalendar selectGraduationDate(@Param("userId") int userId);

    int insertCalendar(@Param("userId") int userId, @Param("planId") Integer planId, @Param("model") UserOccupationPlanCalendar model);

    int updateCalendar(@Param("userId") int userId, @Param("planId") Integer planId, @Param("id") int id, @Param("model") UserOccupationPlanCalendar model);

    int deleteCalendar(@Param("userId") int userId, @Param("planId") Integer planId, @Param("id") int id);

    ArrayList<UserOccupationPlanCalendarTask> selectPlanTasks(@Param("userId") int userId, @Param("planId") int planId);

    ArrayList<UserOccupationPlanCalendarTask> selectTasks(@Param("userId") int userId);

    int insertTask(@Param("userId") int userId, @Param("planId") Integer planId, @Param("model") UserOccupationPlanCalendarTask model);

    int updateTask(@Param("userId") int userId, @Param("planId") Integer planId, @Param("id") int id, @Param("model") UserOccupationPlanCalendarTask model);

    int deleteTask(@Param("userId") int userId, @Param("planId") Integer planId, @Param("id") int id);

    UserSchoolPlan selectSchoolPlan(@Param("userId") int userId, @Param("planId") int planId);

    int insertSchoolPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserSchoolPlan model);

    int updateSchoolPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserSchoolPlan model);

    ArrayList<UserSchoolPlanCourse> selectSchoolCourses(@Param("userId") int userId, @Param("planId") int planId);

    int insertSchoolCourse(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserSchoolPlanCourse model);

    int updateSchoolCourse(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id, @Param("model") UserSchoolPlanCourse model);

    int deleteSchoolCourse(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id);

    ArrayList<UserSchoolPlanActivity> selectSchoolActivities(@Param("userId") int userId, @Param("planId") int planId);

    int insertSchoolActivity(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserSchoolPlanActivity model);

    int updateSchoolActivity(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id, @Param("model") UserSchoolPlanActivity model);

    int deleteSchoolActivity(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id);

    ArrayList<UserCollegePlan> selectCollegePlans(@Param("userId") int userId, @Param("planId") int planId);

    int insertCollegePlan(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserCollegePlan model);

    int updateCollegePlan(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id, @Param("model") UserCollegePlan model);

    int deleteCollegePlan(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id);

    ArrayList<UserMilitaryPlan> selectMilitaryPlans(@Param("userId") int userId, @Param("planId") int planId);

    int insertMilitaryPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserMilitaryPlan model);

    int updateMilitaryPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id, @Param("model") UserMilitaryPlan model);

    int deleteMilitaryPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id);

    int insertMilitaryPlanCareer(@Param("userId") int userId, @Param("militaryPlanId") int militaryPlanId, @Param("model") UserMilitaryPlanCareer model);

    int updateMilitaryPlanCareer(@Param("userId") int userId, @Param("militaryPlanId") int militaryPlanId, @Param("id") int id, @Param("model") UserMilitaryPlanCareer model);

    int deleteMilitaryPlanCareer(@Param("userId") int userId, @Param("militaryPlanId") int militaryPlanId, @Param("id") int id);

    int insertMilitaryPlanRotc(@Param("userId") int userId, @Param("militaryPlanId") int militaryPlanId, @Param("model") UserMilitaryPlanRotc model);

    int updateMilitaryPlanRotc(@Param("userId") int userId, @Param("militaryPlanId") int militaryPlanId, @Param("id") int id, @Param("model") UserMilitaryPlanRotc model);

    int insertMilitaryPlanServiceCollege(@Param("userId") int userId, @Param("militaryPlanId") int militaryPlanId, @Param("model") UserMilitaryPlanServiceCollege model);

    int deleteMilitaryPlanServiceCollege(@Param("userId") int userId, @Param("militaryPlanId") int militaryPlanId, @Param("id") int id);

    UserWorkPlan selectWorkPlan(@Param("userId") int userId, @Param("planId") int planId);

    int insertWorkPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserWorkPlan model);

    int updateWorkPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserWorkPlan model);

    ArrayList<RefGapYearPlans> selectGapYearOptions();

    ArrayList<RefGapYearTypes> selectGapYearTypes();

    ArrayList<UserGapYearPlan> selectGapYearPlans(@Param("userId") int userId, @Param("planId") int planId);

    int insertGapYearPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("model") UserGapYearPlan model);

    int insertGapYearPlanTypes(@Param("model") UserGapYearPlan model);

    int updateGapYearPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id, @Param("model") UserGapYearPlan model);

    int updateGapYearPlanTypes(@Param("id") int id, @Param("model") UserGapYearPlan model);

    int deleteGapYearPlan(@Param("userId") int userId, @Param("planId") int planId, @Param("id") int id);
}
