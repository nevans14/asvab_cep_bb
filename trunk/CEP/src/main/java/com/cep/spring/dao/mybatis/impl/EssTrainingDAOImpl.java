package com.cep.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.EssTrainingDAO;
import com.cep.spring.dao.mybatis.service.EssTrainingService;
import com.cep.spring.model.EssTraining.*;

@Primary
@Repository
public class EssTrainingDAOImpl implements EssTrainingDAO {

	private final EssTrainingService service;

	@Autowired
	public EssTrainingDAOImpl(EssTrainingService service) {
		this.service = service;
	}
	
	public EssTraining getEssTrainingById(int essTrainingId) { return service.getEssTrainingById(essTrainingId); }
	
	public EssTrainingEmail getEssTrainingEmailById(int essTrainingId) { return service.getEssTrainingEmailById(essTrainingId); }
	
	public int addEssTraining(EssTraining essTraining) { return service.addEssTraining(essTraining); }
	
	public int updateEssTraining(EssTraining essTraining) { return service.updateEssTraining(essTraining); }
	
	public int removeEssTraining(int essTrainingId) { return service.removeEssTraining(essTrainingId); }
	
	public int isUserRegistered(String emailAddress, int essTrainingId) { return service.isUserRegistered(emailAddress, essTrainingId); }
	
	public int addEssTrainingRegistration(EssTrainingRegistration essTrainingRegistration) { return service.addEssTrainingRegistration(essTrainingRegistration); }
}