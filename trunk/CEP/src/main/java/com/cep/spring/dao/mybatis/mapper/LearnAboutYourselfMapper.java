package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.Dashboard.MoreAboutMe;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LearnAboutYourselfMapper {

    List<MoreAboutMe> getMoreAboutMe(@Param("userId") int userId);

    int insertMoreAboutMe(@Param("model") MoreAboutMe model);

    int updateMoreAboutMe(@Param("model") MoreAboutMe model);

    int deleteMoreAboutMe(@Param("id") int id, @Param("userId") int userId);

}
