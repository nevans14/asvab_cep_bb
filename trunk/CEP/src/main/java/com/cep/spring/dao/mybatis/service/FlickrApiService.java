package com.cep.spring.dao.mybatis.service;


import com.google.gson.Gson;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import com.cep.spring.utils.JsonApiHelper;
import com.cep.spring.model.optionready.*;

@Service
@PropertySource("classpath:application.properties")
public class FlickrApiService {

	@Value("${security.flickr.key}")
	private String key;
	@Value("${security.flickr.userid}")
	private String userId;

	private final JsonApiHelper jsonApiHelper;

	@Autowired
	public FlickrApiService(JsonApiHelper jsonApiHelper) {
		this.jsonApiHelper = jsonApiHelper;
	}
	
	public ArrayList<FlickrPhotoInfo> getPhotoCollection(boolean isDebugging) throws Exception {
		HashMap<String, String> params;
		ArrayList<FlickrPhotoInfo> photos = new ArrayList<>();
		FlickrPhotoCollectionWrapper wrapper;
		FlickrPhotoCollectionInfo collection;
		Gson gson = new Gson();
		// hash params
		params = new HashMap<String, String>(){{
			put("method", "flickr.people.getPublicPhotos");
			put("api_key", key);
			put("user_id", userId);
			put("format", "json");
			put("nojsoncallback", "1");
		}};
		// build request url
		String jsonResults = sendRequest(params);
		// print out results
		if (isDebugging) {
			System.out.println("flickr results: " + jsonResults);
		}
		// parse response
		wrapper = gson.fromJson(jsonResults, FlickrPhotoCollectionWrapper.class);
		// iterate over photos
		if (wrapper != null) {
			collection = wrapper.getPhotos();
			if (collection != null && collection.getPhoto().size() > 0) {
				for (int i = 0; i < collection.getPhoto().size(); i++) {
					FlickrPhotoCollectionPhoto photo = collection.getPhoto().get(i);
					photos.add(getPhotoInfo(photo.getId(), isDebugging));
				}
			}
		}
		return photos;
	}
	
	public FlickrPhotoInfo getPhotoInfo(String photoId, boolean isDebugging) throws Exception {
		HashMap<String, String> params;
		FlickrPhotoInfoWrapper wrapper;
		Gson gson = new Gson();
		// hash params
		params = new HashMap<String, String>() {{
			put("method", "flickr.photos.getInfo");
			put("api_key", key);
			put("photo_id", photoId);
			put("format", "json");
			put("nojsoncallback", "1");
		}};
		String jsonResults = sendRequest(params);
		// print results
		if (isDebugging) {
			System.out.println("flickr results: " + jsonResults);
		}
		// parse response
		wrapper = gson.fromJson(jsonResults, FlickrPhotoInfoWrapper.class);
		return wrapper == null ? null : wrapper.getFlickrPhotoInfo();
	}

	private String sendRequest(HashMap<String, String> params) throws Exception {
		// build request url
		String url = jsonApiHelper.addParameters(true, "https://api.flickr.com/services/rest/", params);
		// build connection
		URL requestUrl = new URL(url);
		HttpsURLConnection connection = (HttpsURLConnection)requestUrl.openConnection();
		SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
		sslContext.init(null,null,null);
		// bypass cert
		connection.setSSLSocketFactory(sslContext.getSocketFactory());
		// send request and get results
		return jsonApiHelper.sendRequest(connection);
	}
}