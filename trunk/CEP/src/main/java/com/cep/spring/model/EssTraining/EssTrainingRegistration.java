package com.cep.spring.model.EssTraining;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class EssTrainingRegistration implements Serializable {

	private static final long serialVersionUID = -3217374445291721960L;
	private Integer essTrainingId, essTrainingRegistrationId;	
	private String registrationFirstName, 
		registrationLastName, 
		registrationEmailAddress, 
		registrationOrganization, 
		registrationRole,
		registrationComment, 
		registrationDietaryRestrictions, 
		registrationMobilityRestrictions;
	
	public void setEssTrainingRegistrationId(Integer essTrainingRegistrationId) {
		this.essTrainingRegistrationId = essTrainingRegistrationId;
	}
	
	public Integer getEssTrainingRegistrationId() {
		return this.essTrainingRegistrationId;
	}
	
	public void setEssTrainingId(Integer essTrainingId) {
		this.essTrainingId = essTrainingId;
	}
	
	public Integer getEssTrainingId() {
		return this.essTrainingId;
	}

	public String getRegistrationFirstName() {
		return registrationFirstName;
	}

	public void setRegistrationFirstName(String registrationFirstName) {
		this.registrationFirstName = Jsoup.clean(registrationFirstName, Whitelist.basic());
	}

	public String getRegistrationLastName() {
		return registrationLastName;
	}

	public void setRegistrationLastName(String registrationLastName) {
		this.registrationLastName = Jsoup.clean(registrationLastName, Whitelist.basic());
	}

	public String getRegistrationEmailAddress() {
		return registrationEmailAddress;
	}

	public void setRegistrationEmailAddress(String registrationEmailAddress) {
		this.registrationEmailAddress = Jsoup.clean(registrationEmailAddress, Whitelist.basic());
	}

	public String getRegistrationOrganization() {
		return registrationOrganization;
	}

	public void setRegistrationOrganization(String registrationOrganization) {
		this.registrationOrganization = Jsoup.clean(registrationOrganization, Whitelist.basic());
	}
	
	public String getRegistrationRole() {
		return this.registrationRole;
	}

	public void setRegistrationRole(String registrationRole) {
		this.registrationRole = Jsoup.clean(registrationRole, Whitelist.basic());
	}

	public String getRegistrationComment() {
		return registrationComment;
	}

	public void setRegistrationComment(String registrationComment) {
		this.registrationComment = Jsoup.clean(registrationComment, Whitelist.basic());
	}

	public String getRegistrationDietaryRestrictions() {
		return registrationDietaryRestrictions;
	}

	public void setRegistrationDietaryRestrictions(String registrationDietaryRestrictions) {
		this.registrationDietaryRestrictions = Jsoup.clean(registrationDietaryRestrictions, Whitelist.basic());
	}

	public String getRegistrationMobilityRestrictions() {
		return registrationMobilityRestrictions;
	}

	public void setRegistrationMobilityRestrictions(String registrationMobilityRestrictions) {
		this.registrationMobilityRestrictions = Jsoup.clean(registrationMobilityRestrictions, Whitelist.basic());
	}
}