package com.cep.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.service.CEPAnalyticsService;

@Repository
public class CEPAnalyticsDAOImpl {

	private final CEPAnalyticsService service;

	@Autowired
	public CEPAnalyticsDAOImpl(CEPAnalyticsService service) {
		this.service = service;
	}

	/**
	 * Tracks usage of bring ASVAB CEP to your school sharing feature.
	 * 
	 * @param email
	 * @param category
	 * @return
	 * @throws Exception
	 */
	public int insertCEPShare(String email, String category) throws Exception {
		return service.insertCEPShare(email, category);
	}
}
