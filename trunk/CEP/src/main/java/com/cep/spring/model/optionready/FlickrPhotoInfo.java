package com.cep.spring.model.optionready;

import java.util.List;

public class FlickrPhotoInfo {
	private String id, secret, farm, server, originalformat;
	private FlickrPhotoInfoUrlWrapper urls;
	private FlickrPhotoInfoTitle title;
	private FlickrPhotoInfoDescription description;
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setSecret(String secret) {
		this.secret = secret;
	}
	
	public void setFarm(String farm) {
		this.farm = farm;
	}
	
	public void setServer(String server) {
		this.server = server;
	}
	
	public void setOriginalFormat(String originalformat) {
		this.originalformat = originalformat;
	}
	
	public void setTitle(FlickrPhotoInfoTitle title) {
		this.title = title;
	}
	
	public void setUrls(FlickrPhotoInfoUrlWrapper urls) {
		this.urls = urls;
	}
	
	public String getId() {
		return id;
	}
	
	public String getSecret() {
		return secret;
	}
	
	public String getFarm() {
		return farm;
	}
	
	public String getServer() {
		return server;
	}
	
	public String getOriginalFormat() {
		return originalformat;
	}
	
	public FlickrPhotoInfoTitle getTitle() {
		return title;
	}
	
	public FlickrPhotoInfoDescription getDescription() {
		return description;
	}
	
	public FlickrPhotoInfoUrlWrapper getUrls() {
		return urls;
	}
	
	public class FlickrPhotoInfoOwner {
		private String nsid, username, realname, location;
		
		public void setNsId(String nsid) {
			this.nsid = nsid;
		}
		
		public void setUsername(String username) {
			this.username = username;
		}
		
		public void setRealname(String realname) {
			this.realname = realname;
		}
		
		public void setLocation(String location) {
			this.location = location;
		}
		
		public String getNsId() {
			return nsid;
		}
		
		public String getUsername() {
			return username;
		}
		
		public String getRealname() {
			return realname;
		}
		
		public String getLocation() {
			return location;
		}
	}
	
	public class FlickrPhotoInfoUrlWrapper {
		private List<FlickrPhotoInfoUrl> url;
		
		public void setUrl(List<FlickrPhotoInfoUrl> url) {
			this.url = url;
		}
		
		public List<FlickrPhotoInfoUrl> getUrl() {
			return url;
		}
	}
	
	public class FlickrPhotoInfoUrl {
		private String type, _content;
		
		public void setType(String type) {
			this.type = type;
		}
		
		public void setContent(String _content) {
			this._content = _content;
		}
		
		public String getType() {
			return type;
		}
		
		public String getContent() {
			return _content;
		}
	}
	
	public class FlickrPhotoInfoTitle {
		private String _content;
		
		public void setContent(String _content) {
			this._content = _content;
		}
		
		public String getContent() {
			return _content;
		}
	}
	
	public class FlickrPhotoInfoDescription {
		private String _content;
		
		public void setContent(String _content) {
			this._content = _content;
		}
		
		public String getContent() {
			return _content;
		}
	}
}