package com.cep.spring.mvc.controller;

import com.cep.spring.dao.mybatis.PlanYourFutureDAO;
import com.cep.spring.model.PlanYourFuture.*;
import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping(value = "/plan-your-future")
public class PlanYourFutureController {

    private final Logger logger = LogManager.getLogger();
    private final PlanYourFutureDAO dao;
    private final UserManager userManager;

    @Autowired
    public PlanYourFutureController(PlanYourFutureDAO dao, UserManager userManager) {
        this.dao = dao;
        this.userManager = userManager;
    }

    @RequestMapping(value = "/plans", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<UserOccupationPlan>> getPlans() {
        ArrayList<UserOccupationPlan> result;
        try {
            result = dao.selectOccupationPlans(userManager.getUserId());
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{id}/", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<UserOccupationPlan> getPlan(@PathVariable int id) {
        UserOccupationPlan result;
        try {
            result = dao.selectOccupationPlan(userManager.getUserId(), id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserOccupationPlan> insertPlan(@RequestBody UserOccupationPlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertOccupationPlan(userManager.getUserId(), model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/{id}/", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updatePlan(@PathVariable int id, @RequestBody UserOccupationPlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateOccupationPlan(userManager.getUserId(), id, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deletePlan(@PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteOccupationPlan(userManager.getUserId(), id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/calendars", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<UserOccupationPlanCalendar>> getPlanCalendars(@PathVariable int planId) {
        ArrayList<UserOccupationPlanCalendar> result;
        try {
            result = dao.selectPlanCalendars(userManager.getUserId(), planId);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/calendars", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserOccupationPlanCalendar> insertPlanCalendar(@PathVariable int planId, @RequestBody UserOccupationPlanCalendar model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertCalendar(userManager.getUserId(), planId, model);
            // if the user is creating a calendar with with tasks
            if (rowsAffected > 0 && model.getTasks() != null && model.getTasks().size() > 0) {
                for (UserOccupationPlanCalendarTask task : model.getTasks()) {
                    task.setCalendarId(model.getId());
                    rowsAffected += dao.insertTask(userManager.getUserId(), planId, task);
                }
            }
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/{planId}/calendars/{id}/", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updatePlanCalendar(@PathVariable int planId, @PathVariable int id, @RequestBody UserOccupationPlanCalendar model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateCalendar(userManager.getUserId(), planId, id, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/calendars/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deletePlanCalendar(@PathVariable int planId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteCalendar(userManager.getUserId(), planId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/calendars", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<UserOccupationPlanCalendar>> getCalendars() {
        ArrayList<UserOccupationPlanCalendar> result;
        try {
            result = dao.selectCalendars(userManager.getUserId());
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/calendars", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserOccupationPlanCalendar> insertCalendar(@RequestBody UserOccupationPlanCalendar model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertCalendar(userManager.getUserId(), null, model);
            // if the user is creating a calendar with with tasks
            if (rowsAffected > 0 && model.getTasks() != null && model.getTasks().size() > 0) {
                for (UserOccupationPlanCalendarTask task : model.getTasks()) {
                    task.setCalendarId(model.getId());
                    rowsAffected += dao.insertTask(userManager.getUserId(), null, task);
                }
            }
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/calendars/{id}/", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updateCalendar(@PathVariable int id, @RequestBody UserOccupationPlanCalendar model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateCalendar(userManager.getUserId(), null, id, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/calendars/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteCalendar(@PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteCalendar(userManager.getUserId(), null, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/tasks", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<UserOccupationPlanCalendarTask>> getPlanTasks(@PathVariable int planId) {
        ArrayList<UserOccupationPlanCalendarTask> result;
        try {
            result = dao.selectPlanTasks(userManager.getUserId(), planId);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/tasks", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserOccupationPlanCalendarTask> insertPlanTask(@PathVariable int planId, @RequestBody UserOccupationPlanCalendarTask model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertTask(userManager.getUserId(), planId, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/{planId}/tasks/{id}/", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updatePlanTask(@PathVariable int planId, @PathVariable int id, @RequestBody UserOccupationPlanCalendarTask model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateTask(userManager.getUserId(), planId, id, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/tasks/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deletePlanTask(@PathVariable int planId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteTask(userManager.getUserId(), planId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/tasks", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<UserOccupationPlanCalendarTask>> getTasks() {
        ArrayList<UserOccupationPlanCalendarTask> result;
        try {
            result = dao.selectTasks(userManager.getUserId());
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/tasks", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserOccupationPlanCalendarTask> insertTask(@RequestBody UserOccupationPlanCalendarTask model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertTask(userManager.getUserId(), null, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/tasks/{id}/", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updateTask(@PathVariable int id, @RequestBody UserOccupationPlanCalendarTask model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateTask(userManager.getUserId(), null, id, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/tasks/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteTask(@PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteTask(userManager.getUserId(), null, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/school-plans", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<UserSchoolPlan> getSchoolPlan(@PathVariable int planId) {
        UserSchoolPlan result;
        try {
            result = dao.selectSchoolPlan(userManager.getUserId(), planId);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/school-plans", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Integer> insertSchoolPlan(@PathVariable int planId) {
        int rowsAffected;
        UserSchoolPlan model = new UserSchoolPlan();
        try {
            UserOccupationPlanCalendar graduationDate = dao.selectGraduationDate(userManager.getUserId());
            if (graduationDate != null) {
                model.setGraduationDate(graduationDate.getDate());
            }
            rowsAffected = dao.insertSchoolPlan(userManager.getUserId(), planId, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/{planId}/school-plans", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<UserSchoolPlan> updateSchoolPlan(@PathVariable int planId, @RequestBody UserSchoolPlan model) {
        int rowsAffected = 0;
        try {
            if (model.getGraduationDate() != null) {
                rowsAffected = dao.updateSchoolPlan(userManager.getUserId(), planId, model);
            }
            if (model.getCourses() != null && model.getCourses().size() > 0) {
                for (UserSchoolPlanCourse course : model.getCourses()) {
                    if (course.getId() > 0) {
                        rowsAffected += dao.updateSchoolCourse(userManager.getUserId(), planId, course.getId(), course);
                    } else {
                        rowsAffected += dao.insertSchoolCourse(userManager.getUserId(), planId, course);
                    }
                }
            }
            if (model.getActivities() != null && model.getActivities().size() > 0) {
                for (UserSchoolPlanActivity activity : model.getActivities()) {
                    if (activity.getId() > 0) {
                        rowsAffected += dao.updateSchoolActivity(userManager.getUserId(), planId, activity.getId(), activity);
                    } else {
                        rowsAffected += dao.insertSchoolActivity(userManager.getUserId(), planId, activity);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/school-courses/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteSchoolCourse(@PathVariable int planId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteSchoolCourse(userManager.getUserId(), planId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/school-activities/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteSchoolActivity(@PathVariable int planId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteSchoolActivity(userManager.getUserId(), planId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/college-plans", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<UserCollegePlan>> getCollegePlans(@PathVariable int planId) {
        ArrayList<UserCollegePlan> result;
        try {
            result = dao.selectCollegePlans(userManager.getUserId(), planId);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/college-plans", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserCollegePlan> insertCollegePlan(@PathVariable int planId, @RequestBody UserCollegePlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertCollegePlan(userManager.getUserId(), planId, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/{planId}/college-plans/{id}/", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updateCollegePlan(@PathVariable int planId, @PathVariable int id, @RequestBody UserCollegePlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateCollegePlan(userManager.getUserId(), planId, id, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/college-plans/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteCollegePlan(@PathVariable int planId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteCollegePlan(userManager.getUserId(), planId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/military-plans", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<UserMilitaryPlan>> getMilitaryPlans(@PathVariable int planId) {
        ArrayList<UserMilitaryPlan> result;
        try {
            result = dao.selectMilitaryPlans(userManager.getUserId(), planId);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/military-plans", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserMilitaryPlan> insertMilitaryPlan(@PathVariable int planId, @RequestBody UserMilitaryPlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertMilitaryPlan(userManager.getUserId(), planId, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/{planId}/military-plans/{id}/", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<UserMilitaryPlan> updateMilitaryPlan(@PathVariable int planId, @PathVariable int id, @RequestBody UserMilitaryPlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateMilitaryPlan(userManager.getUserId(), planId, id, model);
            if (model.getRotcPrograms() != null && model.getRotcPrograms().size() > 0) {
                for (UserMilitaryPlanRotc rotc : model.getRotcPrograms()) {
                    if (rotc.getId() <= 0) {
                        rowsAffected += dao.insertMilitaryPlanRotc(userManager.getUserId(), id, rotc);
                    } else {
                        rowsAffected += dao.updateMilitaryPlanRotc(userManager.getUserId(), id, rotc.getId(), rotc);
                    }
                }
            }
            if (model.getSelectedCareers() != null && model.getSelectedCareers().size() > 0) {
                for (UserMilitaryPlanCareer career : model.getSelectedCareers()) {
                    if (career.getId() <= 0) {
                        rowsAffected += dao.insertMilitaryPlanCareer(userManager.getUserId(), id, career);
                    } else {
                        rowsAffected += dao.updateMilitaryPlanCareer(userManager.getUserId(), id, career.getId(), career);
                    }
                }
            }
            if (model.getServiceColleges() != null && model.getServiceColleges().size() > 0) {
                for (UserMilitaryPlanServiceCollege serviceCollege : model.getServiceColleges()) {
                    if (serviceCollege.getId() == 0) {
                        rowsAffected += dao.insertMilitaryPlanServiceCollege(userManager.getUserId(), id, serviceCollege);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/military-plans/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteMilitaryPlan(@PathVariable int planId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteMilitaryPlan(userManager.getUserId(), planId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/military-plans/{militaryPlanId}/careers/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteMilitaryPlanCareer(@PathVariable int militaryPlanId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteMilitaryPlanCareer(userManager.getUserId(), militaryPlanId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/military-plans/{militaryPlanId}/service-colleges/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteMilitaryPlanServiceCollege(@PathVariable int militaryPlanId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteMilitaryPlanServiceCollege(userManager.getUserId(), militaryPlanId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/work-plans", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<UserWorkPlan> getWorkPlan(@PathVariable int planId) {
        UserWorkPlan result;
        try {
            result = dao.selectWorkPlan(userManager.getUserId(), planId);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/{planId}/work-plans", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserWorkPlan> insertWorkPlan(@PathVariable int planId, @RequestBody UserWorkPlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertWorkPlan(userManager.getUserId(), planId, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/plans/{planId}/work-plans", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updateWorkPlan(@PathVariable int planId, @RequestBody UserWorkPlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateWorkPlan(userManager.getUserId(), planId, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (rowsAffected <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }

    @RequestMapping(value = "/plans/gap-year-plan-types", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<RefGapYearTypes>> getGapYearTypes() {
        ArrayList<RefGapYearTypes> result;
        try {
            result = dao.selectGapYearTypes();
        } catch (Exception e) {
            logger.error("Error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/plans/gap-year-plan-options", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<RefGapYearPlans>> getGapYearOptions() {
        ArrayList<RefGapYearPlans> result;
        try {
            result = dao.selectGapYearOptions();
        } catch (Exception e) {
            logger.error("Error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/plans/{planId}/gap-year-plans", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<UserGapYearPlan>> getGapYearPlans(@PathVariable int planId) {
        ArrayList<UserGapYearPlan> result;
        try {
            result = dao.selectGapYearPlans(userManager.getUserId(), planId);
        } catch (Exception e) {
            logger.error("Error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/plans/{planId}/gap-year-plans", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<UserGapYearPlan> insertGapYearPlan(@PathVariable int planId, @RequestBody UserGapYearPlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.insertGapYearPlan(userManager.getUserId(), planId, model);
            rowsAffected += dao.insertGapYearPlanTypes(model);
        } catch (Exception e) {
            logger.error("Error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        if (rowsAffected <= 0) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(model);
    }

    @RequestMapping(value = "/plans/{planId}/gap-year-plans/{id}/", method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<Integer> updateGapYearPlan(@PathVariable int planId, @PathVariable int id, @RequestBody UserGapYearPlan model) {
        int rowsAffected;
        try {
            rowsAffected = dao.updateGapYearPlan(userManager.getUserId(), planId, id, model);
            rowsAffected += dao.updateGapYearPlanTypes(id, model);
        } catch (Exception e) {
            logger.error("Error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        if (rowsAffected <= 0) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(rowsAffected);
    }

    @RequestMapping(value = "/plans/{planId}/gap-year-plans/{id}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteGapYearPlan(@PathVariable int planId, @PathVariable int id) {
        int rowsAffected;
        try {
            rowsAffected = dao.deleteGapYearPlan(userManager.getUserId(), planId, id);
        } catch (Exception e) {
            logger.error("Error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        if (rowsAffected <= 0) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(rowsAffected);
    }
}
