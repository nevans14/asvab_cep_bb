package com.cep.spring.mvc.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.cep.spring.dao.mybatis.ContactUsDAO;
import com.fasterxml.jackson.core.JsonParser;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.cep.spring.CEPMailer;
import com.cep.spring.dao.mybatis.TestScoreDao;
import com.cep.spring.model.testscore.Status;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.TypeCount;
import com.cep.spring.utils.ConductSSLHandShake;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

@Configuration
@RequestMapping("/")
@PropertySource("classpath:application.properties")
public class RestClientController {

	private final Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("RestClientController");
	private static final String PROD_URL = "https://cepts.dpac.mil/cepts";
	private static final int CHUNK_SIZE = 2000;
	private static final String PNP = "PSTS";
	private static final String ICAT = "ISTS";

	@Value("${application.url}")
	private String appUrl;

	private final JdbcTemplate jdbcTemplate;
	private final CEPMailer mailer;
	private final TestScoreDao dao;
	private final ContactUsDAO contactUsDao;
	private final ConductSSLHandShake conductSSLHandShake;

	@Autowired
	public RestClientController(JdbcTemplate jdbcTemplate, CEPMailer mailer, TestScoreDao dao, ContactUsDAO contactUsDao, ConductSSLHandShake conductSSLHandShake) {
		this.jdbcTemplate = jdbcTemplate;
		this.mailer = mailer;
		this.dao = dao;
		this.contactUsDao = contactUsDao;
		this.conductSSLHandShake = conductSSLHandShake;
	}

	private static final String CEP_PROD_URL = "http://www.asvabprogram.com/";
	
	@Scheduled(fixedDelay = 300000, initialDelay = 6000)
	public void client() {

		if(prodCheck()) {
			logger.debug("Starting");
	
			boolean isWebServiceOn = Boolean.parseBoolean(jdbcTemplate.queryForObject("SELECT value FROM dbInfo WHERE name='turn_on_web_service'", String.class));
			
			logger.debug("isWebServiceOn: "+ (isWebServiceOn));
			if (!isWebServiceOn) {
				logger.debug("Currently on webservice is turned off. Terminating DMDC call and updating test scores.");
				return;
			}
			
			LocalDateTime date = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");
			logger.debug("STARTING: " + date.format(formatter));
	
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

			RestTemplate restTemplate = conductSSLHandShake.doHandShake();
	
			TypeCount count = getCount(restTemplate, entity);
			logger.debug("AllCount: " + count.getAllCount()+" iCat: " + count.getIcatCount()+" PNP: " + count.getPnpCount());
			
			if (count.getAllCount() > 0) {
				if (count.getPnpCount() > 0) {
					List<StudentTestingProgam> stplist = null;
					if(isReady(restTemplate, entity, PNP)){
						if (count.getPnpCount() < CHUNK_SIZE)
							stplist = getScoreByType(restTemplate, entity, "PNP", 0, count.getPnpCount(), 1);
						else {
							int runs =  (int) Math.ceil((double)count.getPnpCount()/(double)CHUNK_SIZE);
							logger.debug("Runs:"+ runs);
							int to = count.getPnpCount();
							int from = to;
							int finalCount = 0;
							try {
								for(int i = 0; i < runs; i++) {
									if(from-CHUNK_SIZE < 0) {
										to = from;
										from = 0;
										finalCount = 1;
									}else {
										from = from-CHUNK_SIZE;
									}
									logger.debug("From: " + from+" To: "+to);
									stplist = getScoreByType(restTemplate, entity, "PNP", from, to,  finalCount);
									to -= CHUNK_SIZE+1;
								}
							} catch (Exception e) {
								logger.error("Error", e);
								throw new RuntimeException(e);
							}
						}
						deleteDuplicates(stplist, "STP");
						stplist.clear();
						try {
							logger.debug("Sending Email");
							// Avishek's change as of 9/15/2020, updated by Jason 9/23/2020
							ArrayList<String> emails = contactUsDao.getEmail(13);
							for (String email : emails) {
								mailer.sendEmail(email, "AllCount: " + count.getAllCount()+" iCat: " + count.getIcatCount()+" PNP: " + count.getPnpCount() ,"STP nightly count", null);
							}
						} catch (Exception e) {
							logger.error("Error", e);
							throw new RuntimeException(e);
						}
					}
				}
				if (count.getIcatCount() > 0) {
					if(isReady(restTemplate, entity, ICAT)){
						List<StudentTestingProgam> stplist = getScoreByType(restTemplate, entity, "ICAT", 0, count.getIcatCount(), 1);
						deleteDuplicates(stplist, "iCAT");
						stplist.clear();
					}
				}
			}
		}
	}
	
	private void deleteDuplicates(List<StudentTestingProgam> stplist, String type) {
		Set<String> uniqueResources = new HashSet<>();
		stplist.forEach(a-> uniqueResources.add(a.getStudentTestProgram().getResource()));
		uniqueResources.forEach(a->logger.debug("Duplicates Removed for "+a+": "+dao.deleteIcatDuplicates(a, type)));
		uniqueResources.clear();
	}

	@RequestMapping(value = "/getTestScoreByAccessCode/{accessCode}/", method = RequestMethod.GET)
	public @ResponseBody void getTestScoreByAccessCode(@PathVariable("accessCode") String accessCode) {
		boolean isWebServiceOn = Boolean.parseBoolean(jdbcTemplate.queryForObject("SELECT value FROM dbInfo WHERE name='turn_on_web_service_accesscode'", String.class));
		if (!isWebServiceOn) {
			logger.debug("Currently on webservice is turned off. Terminating DMDC call and updating test scores.");
			return;
		}
		// trim the accessCode
		accessCode = accessCode.trim();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		RestTemplate restTemplate = conductSSLHandShake.doHandShake();
		try {
			ResponseEntity<String> stpResponses = restTemplate.exchange(PROD_URL+"/lookupScore?accessCode="+accessCode, HttpMethod.POST, entity, new ParameterizedTypeReference<String>() {});
			if(stpResponses.getBody().length()>25)
				insertTestScores(convertToObj(stpResponses));
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
	

	private TypeCount getCount(RestTemplate restTemplate, HttpEntity<String> entity) {
		try {
			String countString = restTemplate.exchange(PROD_URL+"/count", HttpMethod.POST, entity, new ParameterizedTypeReference<String>() {}).getBody();
			logger.debug("countString: "+countString);
			ObjectMapper objectMapper = new ObjectMapper().configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			return objectMapper.readValue(countString, TypeCount.class);
		} catch (Exception e) {
			logger.error("Error", e);
			throw new RuntimeException(e);
		}
	}
	
	private List<StudentTestingProgam> getScoreByType(RestTemplate restTemplate, HttpEntity<String> entity, String type, int from, int to, int finalRun ){
		int rowsInserted;
		String deletedCount;
		List<StudentTestingProgam> stplist = getTestScore(restTemplate, entity, type, from, to );
		logger.debug("STPlist Count: " + stplist.size());
		rowsInserted = insertTestScores(stplist);
		logger.debug("Rows inserted: "+rowsInserted);
		try {
			if (finalRun == 1) {
				deletedCount = deleteDmdcCache(restTemplate, type);
				logger.debug("deletedCount: " + deletedCount);
			}
		} catch (Exception e) {
			logger.error("Error", e);
			throw new RuntimeException(e);
		}
		return stplist;
	}
	
	private String deleteDmdcCache(RestTemplate restTemplate, String type) {
		return restTemplate.getForObject(PROD_URL+"/delete?type="+type, String.class);
	}
	
	private int insertTestScores(List<StudentTestingProgam> stplist) {
		int rowsInserted = 0;
		logger.info("SIZE: " + stplist.size());
		int chunk = 10;
		try {
			List<List<StudentTestingProgam>> sublists = Lists.partition(stplist, chunk);
			for (List<StudentTestingProgam> sublist:sublists)
				rowsInserted += dao.insertTestScoreimpl(sublist);

		} catch (Exception e) {
			logger.error("Error", e);
			throw new RuntimeException(e);
		}
		logger.debug("rowsInserted: "+rowsInserted);
		return rowsInserted;
	}
	
	private boolean isReady(RestTemplate restTemplate, HttpEntity<String> entity, String type){
		ObjectMapper objectMapper = new ObjectMapper();
		boolean status = false;
		try {
			String statusString = restTemplate.exchange(PROD_URL+"/selectProperties?type="+type, HttpMethod.POST, entity, new ParameterizedTypeReference<String>() {}).getBody();
			if(type.equalsIgnoreCase(ICAT)){
				status = objectMapper.readValue(statusString, Status.class).isIcat_status();
			}else if(type.equalsIgnoreCase(PNP)){
				status = objectMapper.readValue(statusString, Status.class).isPnp_status();
			}
		} catch (Exception e) {
			logger.error("Error", e);
			throw new RuntimeException(e);
		}
		return status;
	}
	
	private List<StudentTestingProgam> convertToObj(ResponseEntity<String> stpResponses){
		try {
			ObjectMapper objectMapper = new ObjectMapper().configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			String jsonString = stpResponses.getBody();
			return objectMapper.readValue(jsonString,objectMapper.getTypeFactory().constructCollectionType(List.class, StudentTestingProgam.class));
		} catch (Exception e) {
			logger.error("Error", e);
			throw new RuntimeException(e);
		}
	}

	private List<StudentTestingProgam> getTestScore(RestTemplate restTemplate, HttpEntity<String> entity ,String type, int from, int to ) {
		List<StudentTestingProgam> stplist;
		try {
			logger.debug("WS From: "+ from+ " To: "+to );
			ResponseEntity<String> stpResponses = restTemplate.exchange(PROD_URL+"/nightlyScores?type="+type+"&from="+from+"&to="+to, HttpMethod.POST, entity, new ParameterizedTypeReference<String>() {});
			logger.debug("End web service call");
			stplist = convertToObj(stpResponses);
			logger.debug("stplist size:" + stplist.size());
		} catch (Exception e) {
			logger.error("Error", e);
			throw new RuntimeException(e);
		}
		return stplist;
	}
	
	private boolean prodCheck() {
		return CEP_PROD_URL.contains(appUrl);
	}
}