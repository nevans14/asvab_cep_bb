package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import com.cep.spring.dao.mybatis.DbInfoDAO;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.CEPMailer;
import com.cep.spring.dao.mybatis.ContactUsDAO;
import com.cep.spring.dao.mybatis.impl.CEPAnalyticsDAOImpl;
import com.cep.spring.model.ShareToCounselorForm;
import com.cep.spring.model.BringAsvabToSchoolForm;
import com.cep.spring.model.faq.EmailContactUs;
import com.cep.spring.model.faq.ScoreRequest;
import com.cep.spring.utils.VerifyRecaptcha;

@Controller
@RequestMapping("/contactUs")
public class ContactUs {

	private final Logger logger = LogManager.getLogger();
	private final static String ASVAB_SHARE_PDF = "C:\\Program Files\\Apache Software Foundation\\Tomcat 8.5\\CEP\\static\\CEP_PDF_Contents\\ASVAB_CEP_Fact_Sheet.pdf";

	private final CEPMailer cepMailer;
	private final CEPAnalyticsDAOImpl analyticsDao;
	private final ContactUsDAO contactUsDao;
	private final DbInfoDAO dbInfoDao;
	private final VerifyRecaptcha verifyRecaptcha;

	@Autowired
	public ContactUs(CEPMailer cepMailer, CEPAnalyticsDAOImpl analyticsDao, ContactUsDAO contactUsDao, DbInfoDAO dbInfoDao, VerifyRecaptcha verifyRecaptcha) {
		this.cepMailer = cepMailer;
		this.analyticsDao = analyticsDao;
		this.contactUsDao = contactUsDao;
		this.dbInfoDao = dbInfoDao;
		this.verifyRecaptcha = verifyRecaptcha;
	}

	/**
	 * Sends contact us information to help desk email and confirmation email to user.
	 */
	@RequestMapping(value = "/email", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> email(@RequestBody EmailContactUs emailObject) {
		try {
			// Verify recaptcha with Google.
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptchaResponse());
			// If verify then proceed with sending inquire email.
			if (isVerified) {
				String message = emailObject.getUserType() + "\n"
						+ emailObject.getFirstName() + "\n"
						+ emailObject.getLastName() + "\n"
						+ emailObject.getEmail() + "\n"
						+ emailObject.getTopics() + "\n"
						+ emailObject.getMessage() + "\n";
				String subject = "Someone is trying to contact us";
				// Get administrator emails.
				ArrayList<String> emails = contactUsDao.getEmail(1);
				// Send inquire to administrators.
				for (String email : emails) {
					cepMailer.sendEmail(email, emailObject.getEmail(), message, subject, null);
				}
				// Store inquiry in database.
				int i = contactUsDao.insertGeneralContactUs(emailObject);
				// If zero records were inserted then throw exception.
				if (i == 0) {
					throw new RuntimeException("Error: Inserted zero records.");
				}
			} else {
				// If Google recaptcha is not valid, then return error message.
				return new ResponseEntity<>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// Send confirmation receipt to user.
		String userEmail = emailObject.getEmail();
		String message = dbInfoDao.getDbInfo("cep_gen_contact_conf").getValue2();
		String subject = "ASVAB CEP Received Your Inquiry";
		try {
			cepMailer.sendEmail(userEmail, message, subject, null, true);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Sends email to help desk with information from bring ASVAB CEP to your
	 * school form.
	 */
	@RequestMapping(value = "/scheduleEmail", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> scheduleEmail(
			@RequestBody BringAsvabToSchoolForm emailObject) {
		try {
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptcha());
			if (isVerified) {
				String message = "Title: " + emailObject.getTitle() + 
						"\nPosition: " + emailObject.getPosition()
						+ "\nFirst Name: " + emailObject.getFirstName() 
						+ "\nLast Name: " + emailObject.getLastName() 
						+ "\nEmail: " + emailObject.getEmail() 
						+ "\nPhone: " + emailObject.getPhone() 							
						+ "\nHow they heard about us: " + (emailObject.getHeardUs() != null && !emailObject.getHeardUs().isEmpty() ? emailObject.getHeardUs() : "") + (emailObject.getHeardUsOther() != null && !emailObject.getHeardUsOther().isEmpty() ? " : " + emailObject.getHeardUsOther() : "")
						+ "\nSchool Name: " + emailObject.getSchoolName()
						+ "\nSchool Address: " + emailObject.getSchoolAddress() 
						+ "\nSchool City: " + emailObject.getSchoolCity() 
						+ "\nSchool County: " + emailObject.getSchoolCounty()
						+ "\nSchool State: " + emailObject.getSchoolState() 
						+ "\nSchool Zip: " + emailObject.getSchoolZip();
				String subject = "Someone wants to schedule CEP test at their school";
				// Get administrator emails.
				ArrayList<String> emails = contactUsDao.getEmail(3);
				// Send inquire to administrators.
				for (String email : emails) {
					cepMailer.sendEmail(email, emailObject.getEmail(), message, subject, null);
				}
			} else {
				return new ResponseEntity<>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);
			}
			try {
				analyticsDao.insertCEPShare(emailObject.getEmail(), "Counselor");
			} catch (Exception e) {
				logger.error("Error", e);
			}
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		try {
			String message = dbInfoDao.getDbInfo("cep_schedule_conf").getValue2();
			message = message.replace("[FIRST_NAME]", emailObject.getFirstName());
			message = message.replace("[LAST_NAME]", emailObject.getLastName());
			cepMailer.sendEmail(emailObject.getEmail(),
					message,
					"Bring ASVAB CEP to your school ",
					ASVAB_SHARE_PDF,
					true);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Sends email to counselor with information about the ASVAB Career
	 * Exploration Program.
	 */
	@RequestMapping(value = "/shareWithCounselor", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> shareWithCounselor(@RequestBody ShareToCounselorForm emailObject) {
		try {
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptcha());
			boolean isNotified = false;
			if (isVerified) {
				// Send request to school administrator/counselor
				try {
					String message = dbInfoDao.getDbInfo("cep_counselor_contact").getValue2();
					cepMailer.sendEmail(emailObject.getEmail(),
							message,
							"Bring ASVAB CEP to your school",
							ASVAB_SHARE_PDF,
							true);
					isNotified = true;
				} catch (Exception e) {
					logger.error("Error, Email: " + emailObject.getEmail(), e);
				}
				// Send inquire to administrators.
				String emailMessage = "School Counselor " + emailObject.getEmail() + " was notified of our CEP website. The student or parent heard about the CEP via " + emailObject.getStudentHeardUs() + (emailObject.getStudentHeardUsOther() != null && !emailObject.getStudentHeardUsOther().isEmpty() ? " : " + emailObject.getStudentHeardUsOther() : "") + ".";				
				try {
					if (isNotified) {
						cepMailer.sendEmail("asvabcep@gmail.com",emailMessage,"Bring ASVAB CEP to your school",null);	
					}
				} catch (Exception e) {
					logger.error("Error on notifying ASVAB CEP", e);
				}				
			} else {
				return new ResponseEntity<>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);
			}
			int rowsAffected = analyticsDao.insertCEPShare(emailObject.getEmail(), "Student or Parent");
			if (rowsAffected <= 0) {
				throw new RuntimeException("Failed to insert");
			}
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Sends email to help desk with user's score request information.
	 */
	@RequestMapping(value = "/scoreRequest", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, String>> scoreRequest(@RequestBody ScoreRequest emailObject) {
		try {
			// Verify recaptcha with Google.
			boolean isVerified = verifyRecaptcha.verify(emailObject.getRecaptchaResponse());
			if (isVerified) {
				String message = "UserType: " + emailObject.getUserType() 
						+ "\nFirst Name: " + emailObject.getFirstName() 
						+ "\nLast Name: " + emailObject.getLastName() 
						+ "\nEmail: " + emailObject.getEmail() 
						+ "\nSchool Name: " + emailObject.getSchoolName()
						+ "\nCity: " + emailObject.getCity()
						+ "\nCounty: " + emailObject.getCounty()
						+ "\nState: " + emailObject.getState() 
						+ "\nCountry:" + emailObject.getCountry()
						+ "\nTest Date: " + emailObject.getDt() 
						+ "\nMessage: " + emailObject.getMessage();
				String subject = "--- SCORE REQUEST ---";
				// Get administrator emails.
				ArrayList<String> emails = contactUsDao.getEmail(2);
				// Send inquire to administrators.
				for (String email : emails) {
					cepMailer.sendEmail(email, emailObject.getEmail(), message, subject, null);
				}
				// Store inquiry in database.
				int i = contactUsDao.insertScoreRquest(emailObject);
				// If zero records were inserted then throw exception.
				if (i == 0) {
					throw new RuntimeException("Error: Inserted zero records.");
				}
			} else {
				return new ResponseEntity<>(
						Collections.singletonMap("errorMessage", "Robots Not Allowed!"), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
