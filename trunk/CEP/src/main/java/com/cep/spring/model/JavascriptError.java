package com.cep.spring.model;

import java.io.Serializable;

public class JavascriptError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2208396711463392417L;
	public String errorUrl, errorMessage, errorStackTrace;

	public String getErrorUrl() {
		return errorUrl;
	}

	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorStackTrace() {
		return errorStackTrace;
	}

	public void setErrorStackTrace(String errorStackTrace) {
		this.errorStackTrace = errorStackTrace;
	}
}
