package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.service.MediaCenterService;
import com.cep.spring.model.media.MediaCenter;

@Repository
public class MediaCenterDAOImpl {

	private final MediaCenterService service;

	@Autowired
	public MediaCenterDAOImpl(MediaCenterService service) {
		this.service = service;
	}

	public ArrayList<MediaCenter> getMediaCenterList() {
		return service.getMediaCenterList();
	}
	
	public ArrayList<MediaCenter> getMediaCenterByCategoryId(Integer categoryId) {
		return service.getMediaCenterByCategoryId(categoryId);
	}

	public MediaCenter getArticleById(int mediaId) {
		return service.getArticleById(mediaId);
	}
}
