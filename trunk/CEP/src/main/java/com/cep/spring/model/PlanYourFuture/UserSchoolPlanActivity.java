package com.cep.spring.model.PlanYourFuture;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class UserSchoolPlanActivity implements Serializable {
    private static final long serialVersionUID = 6610831642262348542L;

    private int id, planId;
    private String activity, location, benefit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        if (activity != null && !activity.isEmpty()) {
            this.activity = Jsoup.clean(activity, Whitelist.basic());
        }
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        if (location != null && !location.isEmpty()) {
            this.location = Jsoup.clean(location, Whitelist.basic());
        }
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        if (benefit != null && !benefit.isEmpty()) {
            this.benefit = Jsoup.clean(benefit, Whitelist.basic());
        }
    }
}
