package com.cep.spring.model.user;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;
import java.util.Locale;

public class UserCompletion implements Serializable {
    private static final long serialVersionUID = -8798834931923301789L;

    public String getCompletionType() {
        if (completionType == null) {
            return null;
        }
        // scrubs the completionType
        return Jsoup.clean(completionType.toUpperCase(Locale.US), Whitelist.basic());
    }

    public void setCompletionType(String completionType) {
        this.completionType = completionType;
    }

    public String getValue() {
        if (value == null) {
            return null;
        }
        return Jsoup.clean(value, Whitelist.basic());
    }

    public void setValue(String value) { this.value = value; }

    private String completionType, value;
}
