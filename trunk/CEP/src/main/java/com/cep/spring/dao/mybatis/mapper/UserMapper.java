package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.user.UserCompletion;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {

    List<UserCompletion> getUserCompletion(@Param("userId") int userId);

    int insertUserCompletion(@Param("userId") int userId, @Param("model") UserCompletion model);

    int updateUserCompletion(@Param("userId") int userId, @Param("model") UserCompletion model);

    int deleteUserCompletion(@Param("userId") int userId, @Param("completionType") String completionType);

    int getUserCompletionByType(@Param("userId") int userId, @Param("completionType") String completionType);
}
