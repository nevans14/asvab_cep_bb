package com.cep.spring.model.occufind;

import java.io.Serializable;

public class TitleDescription implements Serializable{

	private static final long serialVersionUID = 444000944834762799L;
	private String title;
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
