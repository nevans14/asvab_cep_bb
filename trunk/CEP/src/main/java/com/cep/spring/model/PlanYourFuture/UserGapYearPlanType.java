package com.cep.spring.model.PlanYourFuture;

import java.io.Serializable;

public class UserGapYearPlanType implements Serializable {
    private static final long serialVersionUID = 3357167220443113071L;

    private byte id;
    private String name;
    private Boolean isSelected;

    public byte getId() { return id; }
    public void setId(byte id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) {
        if (name != null && !name.isEmpty()) {
            this.name = name;
        }
    }

    public Boolean getIsSelected() { return isSelected; }
    public void setIsSelected(Boolean isSelected) { this.isSelected = isSelected; }
}
