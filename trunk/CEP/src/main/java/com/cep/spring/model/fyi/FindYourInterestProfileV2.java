package com.cep.spring.model.fyi;

import java.io.Serializable;

public class FindYourInterestProfileV2 implements Serializable {

    private static final long serialVersionUID = -7534876801130746472L;
    private int userId, testId;

    public FindYourInterestProfileV2(int userId) {
        this.userId = userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    public int getUserId() {
        return userId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }
    public int getTestId() {
        return testId;
    }
}
