package com.cep.spring.dao.mybatis.service;

import com.cep.spring.dao.mybatis.mapper.WorkValueMapper;
import com.cep.spring.model.WorkValue.WorkValueUserProfile;
import com.cep.spring.model.WorkValue.WorkValueUserTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkValueService {

    private final WorkValueMapper mapper;

    @Autowired
    public WorkValueService(WorkValueMapper mapper) {
        this.mapper = mapper;
    }

    public int insertTest(WorkValueUserTest model) {
        return mapper.insertTest(model);
    }

    public WorkValueUserProfile getProfile(Integer userId) {
        return mapper.getProfile(userId);
    }

}
