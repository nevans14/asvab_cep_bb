package com.cep.spring.model.optionready;

import com.cep.spring.model.optionready.FlickrPhotoCollectionInfo;

public class FlickrPhotoCollectionWrapper {
	
	private FlickrPhotoCollectionInfo photos;
	
	public void setPhotos(FlickrPhotoCollectionInfo photos) {
		this.photos = photos;
	}
	
	public FlickrPhotoCollectionInfo getPhotos () {
		return photos;
	}	
}