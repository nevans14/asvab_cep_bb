package com.cep.spring.dao.mybatis;

import com.cep.spring.model.PlanYourFuture.*;

import java.util.ArrayList;

public interface PlanYourFutureDAO {

    ArrayList<UserOccupationPlan> selectOccupationPlans(int userId);

    UserOccupationPlan selectOccupationPlan(int userId, int id);

    int insertOccupationPlan(int userId, UserOccupationPlan model);

    int updateOccupationPlan(int userId, int id, UserOccupationPlan model);

    int deleteOccupationPlan(int userId, int id);

    ArrayList<UserOccupationPlanCalendar> selectPlanCalendars(int userId, int planId);

    ArrayList<UserOccupationPlanCalendar> selectCalendars(int userId);

    UserOccupationPlanCalendar selectGraduationDate(int userId);

    int insertCalendar(int userId, Integer planId, UserOccupationPlanCalendar model);

    int updateCalendar(int userId, Integer planId, int id, UserOccupationPlanCalendar model);

    int deleteCalendar(int userId, Integer planId, int id);

    ArrayList<UserOccupationPlanCalendarTask> selectPlanTasks(int userId, int planId);

    ArrayList<UserOccupationPlanCalendarTask> selectTasks(int userId);

    int insertTask(int userId, Integer planId, UserOccupationPlanCalendarTask model);

    int updateTask(int userId, Integer planId, int id, UserOccupationPlanCalendarTask model);

    int deleteTask(int userId, Integer planId, int id);

    UserSchoolPlan selectSchoolPlan(int userId, int planId);

    int insertSchoolPlan(int userId, int planId, UserSchoolPlan model);

    int updateSchoolPlan(int userId, int planId, UserSchoolPlan model);

    ArrayList<UserSchoolPlanCourse> selectSchoolCourses(int userId, int planId);

    int insertSchoolCourse(int userId, int planId, UserSchoolPlanCourse model);

    int updateSchoolCourse(int userId, int planId, int id, UserSchoolPlanCourse model);

    int deleteSchoolCourse(int userId, int planId, int id);

    ArrayList<UserSchoolPlanActivity> selectSchoolActivities(int userId, int planId);

    int insertSchoolActivity(int userId, int planId, UserSchoolPlanActivity model);

    int updateSchoolActivity(int userId, int planId, int id, UserSchoolPlanActivity model);

    int deleteSchoolActivity(int userId, int planId, int id);

    ArrayList<UserCollegePlan> selectCollegePlans(int userId, int planId);

    int insertCollegePlan(int userId, int planId, UserCollegePlan model);

    int updateCollegePlan(int userId, int planId, int id, UserCollegePlan model);

    int deleteCollegePlan(int userId, int planId, int id);

    ArrayList<UserMilitaryPlan> selectMilitaryPlans(int userId, int planId);

    int insertMilitaryPlan(int userId, int planId, UserMilitaryPlan model);

    int updateMilitaryPlan(int userId, int planId, int id, UserMilitaryPlan model);

    int deleteMilitaryPlan(int userId, int planId, int id);

    int insertMilitaryPlanCareer(int userId, int militaryPlanId, UserMilitaryPlanCareer model);

    int updateMilitaryPlanCareer(int userId, int militaryPlanId, int id, UserMilitaryPlanCareer model);

    int deleteMilitaryPlanCareer(int userId, int militaryPlanId, int id);

    int insertMilitaryPlanRotc(int userId, int militaryPlanId, UserMilitaryPlanRotc model);

    int updateMilitaryPlanRotc(int userId, int militaryPlanId, int id, UserMilitaryPlanRotc model);

    int insertMilitaryPlanServiceCollege(int userId, int militaryPlanId, UserMilitaryPlanServiceCollege model);

    int deleteMilitaryPlanServiceCollege(int userId, int militaryPlanId, int id);

    UserWorkPlan selectWorkPlan(int userId, int planId);

    int insertWorkPlan(int userId, int planId, UserWorkPlan model);

    int updateWorkPlan(int userId, int planId, UserWorkPlan model);

    ArrayList<RefGapYearTypes> selectGapYearTypes();

    ArrayList<RefGapYearPlans> selectGapYearOptions();

    ArrayList<UserGapYearPlan> selectGapYearPlans(int userId, int planId);

    int insertGapYearPlan(int userId, int planId, UserGapYearPlan model);

    int insertGapYearPlanTypes(UserGapYearPlan model);

    int updateGapYearPlan(int userId, int planId, int id, UserGapYearPlan model);

    int updateGapYearPlanTypes(int id, UserGapYearPlan model);

    int deleteGapYearPlan(int userId, int planId, int id);

}
