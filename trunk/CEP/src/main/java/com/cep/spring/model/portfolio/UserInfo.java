package com.cep.spring.model.portfolio;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class UserInfo implements Serializable {

	private static final long serialVersionUID = 3586472287299344754L;
	private String name, gpa;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = Jsoup.clean(name, Whitelist.basic());
	}

	public String getGpa() {
		return gpa;
	}

	public void setGpa(String gpa) {
		this.gpa = Jsoup.clean(gpa, Whitelist.basic());
	}
}
