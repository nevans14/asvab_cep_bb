package com.cep.spring.model.testscore;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class ManualTestScore {
	
	private int userId;
	
	private String verbal;
	
	private String math;
	
	private String scienceTechnology;
	
	private String afqt;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the verbal
	 */
	public String getVerbal() {
		return verbal;
	}

	/**
	 * @param verbal the verbal to set
	 */
	public void setVerbal(String verbal) {
		this.verbal = Jsoup.clean(verbal, Whitelist.basic());
	}

	/**
	 * @return the math
	 */
	public String getMath() {
		return math;
	}

	/**
	 * @param math the math to set
	 */
	public void setMath(String math) {
		this.math = Jsoup.clean(math, Whitelist.basic());
	}

	/**
	 * @return the scienceTechnology
	 */
	public String getScienceTechnology() {
		return scienceTechnology;
	}

	/**
	 * @param scienceTechnology the scienceTechnology to set
	 */
	public void setScienceTechnology(String scienceTechnology) {
		this.scienceTechnology = Jsoup.clean(scienceTechnology, Whitelist.basic());
	}

	/**
	 * @return the afqt
	 */
	public String getAfqt() {
		return afqt;
	}

	/**
	 * @param afqt the afqt to set
	 */
	public void setAfqt(String afqt) {
		this.afqt = Jsoup.clean(afqt, Whitelist.basic());
	}
}
