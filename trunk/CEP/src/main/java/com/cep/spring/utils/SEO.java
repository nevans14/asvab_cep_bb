//package com.cep.spring.utils;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//
//import javax.servlet.ServletConfig;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.util.StringUtils;
//import org.springframework.web.context.support.SpringBeanAutowiringSupport;
//
//import com.cep.spring.dao.mybatis.impl.MediaCenterDAOImpl;
//import com.cep.spring.dao.mybatis.impl.OccufindDAOImpl;
//
//public class SEO extends HttpServlet {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1328977028542486L;
//
//	private Logger logger = LogManager.getLogger(this.getClass().getName());
//
//	@Autowired
//	private MediaCenterDAOImpl mediaDao;
//
//	@Autowired
//	private OccufindDAOImpl occufindDao;
//
//	public void init(ServletConfig config) throws ServletException {
//		super.init(config);
//		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
//	}
//
//	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//		String userAgent = request.getHeader("user-agent");
//
//		// Full URL.
//		String url = request.getRequestURL().toString();
//
//		if (!StringUtils.isEmpty(userAgent)) {
//
//			// Google adwords.
//			if (userAgent.toLowerCase().contains("adsbot") || userAgent.toLowerCase().contains("mediapartners")) {
//				response.sendError(307);
//			}
//			
//			// Checks to see if the user is a crawler.
//			if (userAgent.toLowerCase().contains("googlebot") || userAgent.toLowerCase().contains("bingbot")
//					|| userAgent.toLowerCase().contains("slurp")) {
//
//				logger.debug(userAgent + ": This is a bot");
//
//				// Crawler is trying to access Find Your Interest score page.
//				if (request.getRequestURI().toLowerCase().contains("student-program")) {
//
//					String title = "ASVAB Test | ASVAB Career Exploration Program";
//					String description = "The ASVAB test measures strengths and potential for success in future academic and vocational endeavors.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-post-test")) {
//
//					String title = "Post-Test Activities | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program gives students tools to explore your interests, skills, and work values and see how those relate to careers.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-learn-skills")) {
//
//					String title = "Skills | ASVAB Career Exploration Program";
//					String description = "Identify your strengths and abilities to help you find job types that best match your skills.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-learn-interests")) {
//
//					String title = "Interests | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program helps students� identify interests so they can choose a career they�ll love.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-learn-work-values")) {
//
//					String title = "Work Values | ASVAB Career Exploration Program";
//					String description = "Values are crucial to career success and job satisfaction. Consider each work value and decide which is most important with the ASVAB Career Exploration Program.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-learn")) {
//
//					String title = "Choosing the Right Career | ASVAB Career Exploration Program";
//					String description = "Before students make decisions about what to do after high school, it is important that you spend time thinking about your interests, work values, and abilities.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-explore-occu-find")) {
//
//					String title = "Find Your Dream Job | ASVAB Career Exploration Program";
//					String description = "Students can find career options aligned with their skills and interests in the OCCU-Find. Find your dream job with the ASVAB Career Exploration Program.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-explore-action-steps")) {
//
//					String title = "Career Planning Tools | ASVAB Career Exploration Program";
//					String description = "Together with your parents, the ASVAB Career Exploration Program gives students tools to determine if their skills and interests match skills needed for a job.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-explore")) {
//
//					String title = "Explore Careers | ASVAB Career Exploration Program";
//					String description = "Choosing a job is never an easy decision. The ASVAB Career Exploration Program lets students search for, find, and plan for the careers that best fit you.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-plan-understanding")) {
//
//					String title = "Understand Your Options | ASVAB Career Exploration Program";
//					String description = "There are a variety of occupations and career paths students can pursue after high school. Explore all of the options with the ASVAB Career Exploration Program";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-plan-tools")) {
//
//					String title = "Career Planning Tools | ASVAB Career Exploration Program";
//					String description = "Once you know the entry requirements of an occupation, the ASVAB Career Exploration Program can help you develop a plan to meet your career goals.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student-plan")) {
//
//					String title = "Plan for Your Future | ASVAB Career Exploration Program";
//					String description = "There may be several different paths to get students to their dream job. Map out a plan with the ASVAB Career Exploration Program.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("educators-information-bring")) {
//
//					String title = "Bring the ASVAB Career Exploration Program to Your School";
//					String description = "Educators can give students a complete career planning resource for free! Bring the ASVAB Career Exploration Program to your school.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("educators-post-test")) {
//
//					String title = "Post-Test Activities | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program gives students access to tools to explore their interests, skills, and work values and see how those relate occupations.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("educators-test")) {
//
//					String title = "ASVAB Test | ASVAB Career Exploration Program";
//					String description = "The ASVAB is an aptitude test that measures strengths and potential for success in future academic and vocational endeavors, regardless of postsecondary plans.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("educators")) {
//
//					String title = "Choose Careers | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program helps students identify their skills and interest, explore occupations, and develop plans to realize career goals.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("parents-information")) {
//
//					String title = "Help Your Child Choose a Career | ASVAB Career Exploration Program";
//					String description = "Request ASVAB Career Exploration Program at your child�s school. Explore occupations related to their skills and interests and develop after high school plans.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("parents-post-test")) {
//
//					String title = "Post-Test | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program gives your child access to tools to explore their interests, skills, and work values and see how those relate occupations.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("parents-test")) {
//
//					String title = "ASVAB Test | ASVAB Career Exploration Program";
//					String description = "The ASVAB is an aptitude test that measures strengths and potential for success in future academic and vocational endeavors, regardless of postsecondary plans.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("parents")) {
//
//					String title = "Help Your Child Choose a Career | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program helps your child identify their skills and interest, explore occupations, and develop plans to realize career goals.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("media-center-article-list/5")) {
//
//					String title = "Tutorials | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program offers video tutorials to help students understand scores, find their interest, and explore careers in the OCCU-Find.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("media-center-article-list/2")) {
//
//					String title = "Resources for Parents | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to help parents help their children plan for the future.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("media-center-article-list/4")) {
//
//					String title = "Resources for Educators | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to help school counselors advise students on career planning.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("media-center-article-list/1")) {
//
//					String title = "Resources for Students | ASVAB Career Exploration Program";
//					String description = "The ASVAB Career Exploration Program provides a collection of tips, videos, articles, and tools to assist students in career planning.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("media-center")) {
//
//					String title = "Media Center | ASVAB Career Exploration Program";
//					String description = "Explore articles and content to assist you in career planning. Plan your best future with this collection of data, guidelines and other resources.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("general-occufind-view-all")) {
//
//					String title = "OCCU-Find | ASVAB Career Exploration Program";
//					String description = "Find your dream job. Search careers by interest, skill area, keywords, etc. in the OCCU-Find from the ASVAB Career Exploration Program.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("general-resources")) {
//
//					String title = "Resources | ASVAB Career Exploration Program";
//					String description = "Explore scholarly articles, communication templates, activities, and other resources to assist in career planning from the ASVAB Career Exploration Program.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else if (request.getRequestURI().toLowerCase().contains("student")) {
//
//					String title = "Students| ASVAB Career Exploration Program";
//					String description = "ASVAB Career Exploration Program helps students figure out what you're good at, what is important to you, and the jobs that match your skills and interests.";
//					makeSEOBotPage(request, response, title, description);
//
//				} else {
//					response.sendError(308);
//					//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//				}
//
//			} else {
//				response.sendError(308);
//				//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//				// throw new ServletException("Not Crawler.");
//			}
//
//		} else {
//			response.sendError(308);
//			//response.sendError(HttpServletResponse.SC_TEMPORARY_REDIRECT);
//			// throw new ServletException("Not Crawler.");
//		}
//
//	}
//
//	/**
//	 * Creates and returns crawler friendly HTML page with title and descript
//	 * set for SEO purpose.
//	 * 
//	 * @param request
//	 * @param response
//	 * @param occupation
//	 * @param url
//	 * @param socId
//	 * @throws IOException
//	 */
//	private void makeSEOBotPage(HttpServletRequest request, HttpServletResponse response, String title,
//			String description) throws IOException {
//
//		PrintWriter out = response.getWriter();
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"UTF-8\">");
//		out.println("<title>" + title + "</title>");
//		out.println("<meta name=\"description\" content=\"" + description + "\" />");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("<h1>SEO Crawler Page</h1>");
//		out.println("</body>");
//		out.println("</html>");
//	}
//
//}
