package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.CareerClusterDAO;
import com.cep.spring.dao.mybatis.service.CareerClusterService;
import com.cep.spring.model.CareerCluster;
import com.cep.spring.model.CareerClusterOccupation;
import com.cep.spring.model.OccupationEducationLevel;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.RelatedOccupation;
import com.cep.spring.model.SkillImportance;
import com.cep.spring.model.Task;
import com.cep.spring.model.occufind.OccufindSearch;

@Repository
public class CareerClusterDAOImpl implements CareerClusterDAO {

	private final CareerClusterService service;

	@Autowired
	public CareerClusterDAOImpl(CareerClusterService service) {
		this.service = service;
	}

	/**
	 * Returns career cluster list.
	 * @return a list of Career Clusters
	 */
	public ArrayList<CareerCluster> getCareerCluster() { return service.getCareerCluster(); }

	/**
	 * Returns career cluster title based off of career cluster identifier.
	 * @param ccId the Career Cluster ID
	 * @return the details of a single Career Cluster
	 */
	public CareerCluster getCareerClusterById(int ccId) { return service.getCareerClusterById(ccId); }

	/**
	 * Returns career occupations of the selected career cluster.
	 * @param ccId the Career Cluster ID
	 * @return a list of occupations associated to the given Career Cluster
	 */
	public ArrayList<CareerClusterOccupation> getCareerClusterOccupation(int ccId) { return service.getCareerClusterOccupation(ccId); }

	/**
	 * Returns list of Career Cluster occupations.
	 * @param ccId the Career Cluster ID
	 * @return a list of occupations associated to the given Career Cluster
	 */
	public ArrayList<OccufindSearch> getCareerClusterOccupations(int ccId) { return service.getCareerClusterOccupations(ccId); }

	/**
	 * Returns skill importance in decimal values for math, verbal and
	 * science/technical.
	 * @param onetSoc the O*NET SOC code
     * @return the skill importance
	 */
	public SkillImportance getSkillImportance(String onetSoc) {	return service.getSkillImportance(onetSoc); }

	/**
	 * Returns the occupation tasks with a limit of 5 records.
	 * @param onetSoc the O*NET SOC code of a given occupation
     * @return a list of tasks
	 */
	public ArrayList<Task> getTasks(String onetSoc) { return service.getTasks(onetSoc); }

	/**
	 * Returns interest codes for an occupation.
     * @param onetSoc the O*NET SOC code of a given occupation
     * @return interest codes
	 */
	public OccupationInterestCode getOccupationInterestCodes(String onetSoc) { return service.getOccupationInterestCodes(onetSoc); }

	/**
	 * Returns required education level data for an occupation to create pie
	 * chart.
     * @param onetSoc the O*NET SOC code of a given occupation
     * @return a list of education level
	 */
	public ArrayList<OccupationEducationLevel> getEducationLevel(String onetSoc) { return service.getEducationLevel(onetSoc); }

	/**
	 * Returns a list of related careers.
     * @param onetSoc the O*NET SOC code of a given occupation
     * @return a list of related occupations
     *  
	 */
	public ArrayList<RelatedOccupation> getRelatedOccupations(String onetSoc) { return service.getRelatedOccupations(onetSoc); }
}
