package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.portfolio.FavoriteOccupation;
import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

public interface PortfolioMapper {

	List<String> isPortfolioStarted(@Param("userId") Integer userId);

	ArrayList<WorkExperience> getWorkExperience(@Param("userId") Integer userId);

	int insertWorkExperience(@Param("workExperienceObject") WorkExperience workExperienceObject, @Param("userId") Integer userId);

	int deleteWorkExperience(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateWorkExperience(@Param("workExperienceObject") WorkExperience workExperienceObject, @Param("userId") Integer userId);

	ArrayList<Education> getEducation(@Param("userId") Integer userId);

	int insertEducation(@Param("educationObject") Education educationObject, @Param("userId") Integer userId);

	int deleteEducation(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateEducation(@Param("educationObject") Education educationObject, @Param("userId") Integer userId);

	ArrayList<Achievement> getAchievements(@Param("userId") Integer userId);

	int insertAchievement(@Param("achievementObject") Achievement achievementObject, @Param("userId") Integer userId);

	int deleteAchievement(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateAchievement(@Param("achievementObject") Achievement achievementObject, @Param("userId") Integer userId);

	ArrayList<Interest> getInterests(@Param("userId") Integer userId);

	int insertInterest(@Param("interestObject") Interest interestObject, @Param("userId") Integer userId);

	int deleteInterest(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateInterest(@Param("interestObject") Interest interestObject, @Param("userId") Integer userId);

	ArrayList<Skill> getSkills(@Param("userId") Integer userId);

	int insertSkill(@Param("skillObject") Skill skillObject, @Param("userId") Integer userId);

	int deleteSkill(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateSkill(@Param("skillObject") Skill skillObject, @Param("userId") Integer userId);

	ArrayList<WorkValue> getWorkValues(@Param("userId") Integer userId);

	int insertWorkValue(@Param("workValueObject") WorkValue workValueObject, @Param("userId") Integer userId);

	int deleteWorkValue(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateWorkValue(@Param("workValueObject") WorkValue workValueObject, @Param("userId") Integer userId);

	ArrayList<Volunteer> getVolunteers(@Param("userId") Integer userId);

	int insertVolunteer(@Param("volunteerObject") Volunteer volunteerObject, @Param("userId") Integer userId);

	int deleteVolunteer(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateVolunteer(@Param("volunteerObject") Volunteer volunteerObject, @Param("userId") Integer userId);

	ArrayList<AsvabScore> getAsvabScore(@Param("userId") Integer userId);

	int insertAsvabScore(@Param("asvabObject") AsvabScore asvabObject, @Param("userId") Integer userId);

	int deleteAsvabScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateAsvabScore(@Param("asvabObject") AsvabScore asvabObject, @Param("userId") Integer userId);

	ArrayList<ActScore> getActScore(@Param("userId") Integer userId);

	int insertActScore(@Param("actObject") ActScore actObject, @Param("userId") Integer userId);

	int deleteActScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateActScore(@Param("actObject") ActScore actObject, @Param("userId") Integer userId);

	ArrayList<SatScore> getSatScore(@Param("userId") Integer userId);

	int insertSatScore(@Param("satObject") SatScore satObject, @Param("userId") Integer userId);

	int deleteSatScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateSatScore(@Param("satObject") SatScore satObject, @Param("userId") Integer userId);

	ArrayList<FyiScore> getFyiScore(@Param("userId") Integer userId);

	int insertFyiScore(@Param("fyiObject") FyiScore fyiObject, @Param("userId") Integer userId);

	int deleteFyiScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateFyiScore(@Param("fyiObject") FyiScore fyiObject, @Param("userId") Integer userId);

	ArrayList<OtherScore> getOtherScore(@Param("userId") Integer userId);

	int insertOtherScore(@Param("otherObject") OtherScore otherObject, @Param("userId") Integer userId);

	int deleteOtherScore(@Param("userId") Integer userId, @Param("id") Integer id);

	int updateOtherScore(@Param("otherObject") OtherScore otherObject, @Param("userId") Integer userId);

	ArrayList<Plan> getPlan(@Param("userId") Integer userId);

	int insertPlan(@Param("planObject") Plan planObject, @Param("userId") Integer userId);

	int deletePlan(@Param("userId") Integer userId, @Param("id") Integer id);

	int updatePlan(@Param("planObject") Plan planObject, @Param("userId") Integer userId);
	
	ArrayList<UserInfo> selectNameGrade(@Param("userId") Integer userId);

	int updateInsertNameGrade(@Param("userInfo") UserInfo userInfo, @Param("userId") Integer userId);
	
	ArrayList<FavoriteOccupation> getFavoriteOccupation(@Param("userId") Integer userId);
	
	String getAccessCode(@Param("userId") Integer userId);
	
	int insertTeacherDemoTaken(@Param("userId") Integer userId);
	
	int getTeacherDemoTaken(@Param("userId") Integer userId);
}
