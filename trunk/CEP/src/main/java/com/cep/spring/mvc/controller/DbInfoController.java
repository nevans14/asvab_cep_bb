package com.cep.spring.mvc.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.model.DbInfo;

@RestController
@RequestMapping("/db-info")
public class DbInfoController {
	
	private final Logger logger = LogManager.getLogger();
	private final DbInfoDAO dao;

	@Autowired
	public DbInfoController(DbInfoDAO dao) {
		this.dao = dao;
	}
	
	@RequestMapping(value = "/get-by-name/{name}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<DbInfo> getByName(@PathVariable String name) {
		DbInfo result;
		try {
			result = dao.getDbInfo(name);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}