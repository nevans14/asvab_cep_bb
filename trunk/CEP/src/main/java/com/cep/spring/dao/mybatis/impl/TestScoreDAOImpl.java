package com.cep.spring.dao.mybatis.impl;

import java.util.List;

import com.cep.spring.model.testscore.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.TestScoreDao;
import com.cep.spring.dao.mybatis.service.TestScoreService;

@Primary
@Repository
public class TestScoreDAOImpl implements TestScoreDao {

	private final TestScoreService service;

	@Autowired
	public TestScoreDAOImpl(TestScoreService service) {
		this.service = service;
	}
    
	/**
	 * Insert student's test score record.
	 * @param testScoreObjects List<StudentTestingProgam>
	 * @return how many rows affected by the insert
	 */
	public int insertTestScoreimpl(List<StudentTestingProgam> testScoreObjects) {
		return service.insertTestScore(testScoreObjects);
	}

	/**
	 * Retrieve student's test score record with access code.
	 * 
	 * @param accessCode the User's MEPCOM Access Code
	 * @return the test score for a user by Access Code
	 */
	public StudentTestingProgamSelects getTestScoreWithAccessCode(String accessCode) {
		return service.getTestScoreWithAccessCode(accessCode);
	}
	
	/**
	 * Retrieve student's test score record with user id.
	 * 
	 * @param userId the User's ID
	 * @return the test score for a user by User ID
	 */
	public StudentTestingProgamSelects getTestScoreWithUserId(Integer userId) {
		return service.getTestScoreWithUserId(userId);
	}

	public StudentAsvabScoreResults getStudentASRByAccessCode(String accessCode) {
		return service.getStudentASRByAccessCode(accessCode);
	}
	
	@Override
	public int insertManualTestScore(ManualTestScore manualTestScoreObj) {
		return service.insertManualTestScore(manualTestScoreObj);
	}

	@Override
	public ManualTestScore getManualTestScore(int userId) {
		return service.getManualTestScore(userId);
	}

	@Override
	public int updateManualTestScore(ManualTestScore manualTestScoreObj) {
		return  service.updateManualTestScore(manualTestScoreObj);
	}

	@Override
	public int deleteManualTestScore(int userId) {
		return service.deleteManualTestScore(userId);
	}

	@Override
	public List<StudentTestingProgamSelects> filterTestScore(FilterScoreObj filterScoreObj) {
		return service.filterTestScore(filterScoreObj);
	}

	@Override
	public int deleteIcatDuplicates(String sourceFileName, String type) {
		return service.deleteIcatDuplicates(sourceFileName, type);
	}

	@Override
	public int getRecordCount(String sourceFileName, String platform) {
		return service.getRecordCount(sourceFileName, platform);
	}
}
