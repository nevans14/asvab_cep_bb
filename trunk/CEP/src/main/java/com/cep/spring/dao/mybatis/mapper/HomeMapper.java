package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.HomepageImage;

public interface HomeMapper {
	HomepageImage getCurrentHomepageImg();
}