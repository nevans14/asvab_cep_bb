package com.cep.spring.sso;

import com.cep.spring.utils.CookieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.session.ExpiringSession;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.Session;
import org.springframework.session.security.SpringSessionBackedSessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.*;
import java.util.Collection;
import java.util.Enumeration;

@Controller
@RequestMapping(value = "/auth")
@PropertySource(value = {"classpath:SsoProperties.properties","classpath:application.properties"})
public class AuthController {

    @SuppressWarnings("rawtypes")
    private final FindByIndexNameSessionRepository sessionRepository;
    private final FindByIndexNameSessionRepository<? extends ExpiringSession> sessions;
    private final SsoLogout ssoLogout;

    @Value("${citm.domain}")
    private String ctmDomain;
    @Value("${security.cookie.secure}")
    private boolean isSecure;

    @Autowired
    @SuppressWarnings("rawtypes")
    public AuthController(FindByIndexNameSessionRepository sessionRepository,
                          FindByIndexNameSessionRepository<? extends ExpiringSession> sessions,
                          SsoLogout ssoLogout) {
        this.sessionRepository = sessionRepository;
        this.sessions = sessions;
        this.ssoLogout = ssoLogout;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Boolean> logout(HttpServletRequest request, HttpServletResponse response) {
        final String GLOBALS = "globals";
        final String PATH = "/";
        final int MAX_AGE = 0;
        final boolean IS_HTTP_ONLY = false;
        HttpSession httpSession = request.getSession();
        SecurityContext context = (SecurityContext) httpSession.getAttribute("SPRING_SECURITY_CONTEXT");
        Authentication authentication = context != null ? context.getAuthentication() : null;
        // expire session in redis
        expireSession(httpSession);
        // clear/invalidate server session
        invalidateSession(httpSession);
        // logout
        ssoLogout.onLogoutSuccess(request, response, authentication);
        // add globals to be deleted
        response.addCookie(CookieHelper.addCookie(
                GLOBALS,
                null,
                ctmDomain,
                PATH,
                MAX_AGE,
                isSecure,
                IS_HTTP_ONLY));
        // return OK
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Boolean.TRUE);
    }

    /***
     * Expires session within redis
     * @param session the current User session
     */
    @SuppressWarnings("unchecked")
    private void expireSession(HttpSession session) {
        // get principal from auth
        String username = (String) session.getAttribute("USERNAME");
        // set up session registry
        SpringSessionBackedSessionRegistry registry = new SpringSessionBackedSessionRegistry(sessionRepository);
        // get collection of user sessions
        Collection<? extends ExpiringSession> userSessions =
                sessions.findByIndexNameAndIndexValue(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME, username)
                        .values();
        // iterate over sessions and expire them
        for (Session userSession : userSessions) {
            SessionInformation info = registry.getSessionInformation(userSession.getId());
            if (info != null)
                info.expireNow();
        }
    }

    /***
     * Invalidates user session by removing all attributes and security context
     * @param session the current User session
     */
    @SuppressWarnings("rawtypes")
    private void invalidateSession(HttpSession session) {
        clearSecurityContext(session);
        Enumeration attrs = session.getAttributeNames();
        while (attrs.hasMoreElements()) {
            String name = (String) attrs.nextElement();
            session.removeAttribute(name);
        }
    }

    /***
     * Clears the security context by key in the session
     * @param session the current User session
     */
    private void clearSecurityContext(HttpSession session) {
        SecurityContext securityContext = (SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT");
        // check to see if the context is null (this means the user timed out and tries to logout)
        if (securityContext != null) {
            SecurityContextHolder.setContext(securityContext);
            SecurityContextHolder.clearContext();
        }
    }
}
