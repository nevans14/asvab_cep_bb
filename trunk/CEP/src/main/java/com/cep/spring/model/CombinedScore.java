package com.cep.spring.model;

import java.io.Serializable;

public class CombinedScore implements Serializable {

	private static final long serialVersionUID = -5144304407993902591L;

	double cScore;

	public double getcScore() {
		return cScore;
	}

	public void setcScore(double cScore) {
		this.cScore = cScore;
	}

	public char getInterestCd() {
		return interestCd;
	}

	public void setInterestCd(char interestCd) {
		this.interestCd = interestCd;
	}

	char interestCd;
}
