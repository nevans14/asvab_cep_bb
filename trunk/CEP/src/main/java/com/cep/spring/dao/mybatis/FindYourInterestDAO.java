package com.cep.spring.dao.mybatis;

import java.util.ArrayList;

import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.FYIRawScore;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfileV2;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

public interface FindYourInterestDAO {

	/**
	 * Returns boolean value depending if all records are inserted or not.
	 * 
	 * @param fyiObject FYI object
	 * @return determines if everything was inserted correctly
	 */
	boolean insertProfileAndTest(FindYourInterestProfileV2 fyiObject);

	/**
	 * Update user selected score choice.
	 *
	 * @param scoreChoice the selected score choice between combined or gender
	 * @return rows affected in the database
	 */
	int updateProfile(int userId, ScoreChoice scoreChoice);
	/**
	 * Inserts a new test record into the database for the given User
	 * @param model the logged in user's ID
	 * @return rows affected in the database
	 */
	int insertTest(FindYourInterestProfileV2 model);

	/**
	 * Returns calculated combined and gender scores.
	 * 
	 * @param userId the logged in user's ID
	 * @return a list of scores
	 */
	ArrayList<Scores> getCombinedAndGenderScore(Integer userId);

	/**
	 * Returns gender score for each interest code using raw score test results.
	 * 
	 * @param rawScores Raw Scores
	 * @return a list of gender scores
	 */
	ArrayList<GenderScore> getGenderScore(FYIRawScore rawScores, char gender);

	/**
	 * Returns combined score for each interest code using raw score test
	 * results.
	 * 
	 * @param rawScores raw scores
	 * @return a list of combined scores
	 */
	ArrayList<CombinedScore> getCombinedScore(FYIRawScore rawScores);

	/**
	 * Returns number of test taken depending on user's identification number.
	 * 
	 * @param oldUserId the old user ID before the user merged accounts
	 * @param newUserId the new user ID after the user merged accounts
	 * @return how many times the user has taken the FYI.
	 * If the user has not merged accounts, then the user should have at most 2 tests.
	 * If the user has merged accounts, then the user should have at most 3 tests.
	 */
	Integer getNumberTestTaken(Integer oldUserId, Integer newUserId);

	/**
	 * Returns the old user ID of a user if the user has merged accounts.
	 * @param newUserId the logged in user ID
	 * @return the old user ID if the user has merged accounts
	 */
	Integer getOldUserId(Integer newUserId);

	/**
	 * Returns user's interest codes.
	 * 
	 * @param userId the logged in user ID
	 * @return a list of scores based on their selection (either gender or combined)
	 */
	ArrayList<ScoreChoice> getProfile(Integer userId);

	/**
	 * Returns number of rows in FYI profile.
	 * 
	 * @param userId the logged in user ID
	 * @return how many records are associated to the logged in user
	 */
	Integer getProfileExist(Integer userId);

	/**
	 * Returns user's score choice.
	 * 
	 * @param userId the logged in user ID
	 * @return the user's selected score choice (either gender or combined)
	 */
	String getScoreChoice(Integer userId);

	/**
	 * Returns the top 3 interest codes that are tied
	 * @param userId the logged in user ID
	 * @return top 3 interest codes that are tied
	 */
	TiedSelection getTiedSelection(Integer userId);

	/**
	 * Inserts or updates the top 3 interest codes that are or were tied
	 *
	 * @param userId the logged in user ID
	 * @param tiedObject encapsulated object that contains the user ID and the top 3 gender/combine interest codes that were tied
	 * @return rows affected in the database
	 */
	int insertTiedSelection(int userId, TiedSelection tiedObject);

	/**
	 * Deletes the user's tied interest code selection from the database
	 * @param userId the logged in user ID
	 * @return rows affected in the database
	 */
	int deleteTiedSelection(int userId);

	/**
	 * Determines which FYI response table was used last.
	 * @param oldUserId the old user ID before the user merged accounts
	 * @param oldTestId the last test the user has taken before merging accounts
	 * @return 1 - saveFYI, 2 - FyiV2ItemResponse
	 */
	int getWhichLastFyiTableUsed(int oldUserId, int oldTestId);

	/**
	 * Inserts a new FYI profile record based on the user's last profile
	 * @param oldUserId the old user ID before the user merged accounts
	 * @param newUserId the new user ID after the user merged accounts
	 * @return rows affected in the database
	 */
	int mergeProfile(int oldUserId, int newUserId);

	/**
	 * Inserts a new FYI test record based on the user's last test
	 * @param fyiObject an encapsulated object that contains the UserId and TestId. The TestId will be set after the query is executed.
	 * @return rows affected in the database
	 */
	int mergeTests(FYIMerge fyiObject);

	/**
	 * Inserts a new FYI test results record based on the user's last test results
	 * @param oldUserId the old user ID before the user merged accounts
	 * @param newUserId the new user ID after the user merged accounts
	 * @return rows affected in the database
	 */
	int mergeResults(int oldUserId, int newUserId);

	/**
	 * (Depreciated) Inserts a new FYI test response record based on the user's last test responses
	 * @param oldUserId the old user ID before the user merged accounts
	 * @param newUserId the new user ID after the user merged accounts
	 * @return rows affected in the database
	 */
	int mergeAnswers(int oldUserId, int newUserId, int testId);

	/**
	 * Inserts a new FYI test response record based on the user's last test responses
	 * @param oldUserId the old user ID before the user merged accounts
	 * @param newTestId the new user ID after the user merged accounts
	 * @return rows affected in the database
	 */
	int mergeResponse(int oldUserId, int newTestId);

	/**
	 * Returns the last test record of a given account
	 * @param oldUserId the old user ID before the user merged accounts
	 * @param newUserId the new user ID after the user merged accounts
	 * @return an encapsulated object that contains the user ID and the Time Stamp
	 */
	FYIMerge getLastTestObject(int oldUserId, int newUserId);

	int deleteProfile(int userId);

}