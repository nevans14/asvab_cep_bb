package com.cep.spring.dao.mybatis.impl;

import com.cep.spring.dao.mybatis.CareerPlanDAO;
import com.cep.spring.dao.mybatis.service.CareerPlanService;
import com.cep.spring.model.CareerPlan.CareerPlanAnswer;
import com.cep.spring.model.CareerPlan.CareerPlanSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class CareerPlanDAOImpl implements CareerPlanDAO {

    private final CareerPlanService service;

    @Autowired
    public CareerPlanDAOImpl(CareerPlanService service) {
        this.service = service;
    }

    public ArrayList<CareerPlanSection> getItems() {
        return service.getItems();
    }

    public CareerPlanAnswer getResponses(Integer userId) {
        return service.getResponses(userId);
    }

    public int updateResponses(CareerPlanAnswer model) {
        return service.updateResponses(model);
    }
}
