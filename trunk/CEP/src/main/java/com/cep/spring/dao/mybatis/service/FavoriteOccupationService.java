package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.FavoriteOccupationMapper;
import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

@Service
public class FavoriteOccupationService {

	private final FavoriteOccupationMapper mapper;

	@Autowired
	public FavoriteOccupationService(FavoriteOccupationMapper mapper) {
		this.mapper = mapper;
	}

	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) {
		return mapper.getFavoriteOccupation(userId);
	}

	public int insertFavoriteOccupation(FavoriteOccupation favoriteObject, Integer userId) {
		return mapper.insertFavoriteOccupation(favoriteObject, userId);
	}

	public int updateFavoriteOccupationHearts(FavoriteOccupation model, Integer userId) {
		return mapper.updateFavoriteOccupationHearts(model, userId);
	}

	public int deleteFavoriteOccupation(int id, int userId) {
		return mapper.deleteFavoriteOccupation(id, userId);
	}

	public ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(int userId) {
		return mapper.getFavoriteCareerCluster(userId);
	}

	public int insertFavoriteCareerCluster(FavoriteCareerCluster favoriteObject, Integer userId) {
		return mapper.insertFavoriteCareerCluster(favoriteObject, userId);
	}

	public int deleteFavoriteCareerCluster(int id, int userId) {
		return mapper.deleteFavoriteCareerCluster(id, userId);
	}
	
	public ArrayList<FavoriteSchool> getFavoriteSchool(int userId) {
		return mapper.getFavoriteSchool(userId);
	}

	public ArrayList<FavoriteSchool> getFavoriteSchoolBySocId(String socId, int userId) { return mapper.getFavoriteSchoolBySocId(socId, userId);}

	public int insertFavoriteSchool(FavoriteSchool favoriteObject, Integer userId) {
		return mapper.insertFavoriteSchool(favoriteObject, userId);
	}

	public int deleteFavoriteSchool(int id, int userId) {
		return mapper.deleteFavoriteSchool(id, userId);
	}

	public int deleteFavoriteCareerClusterByUserId(int userId) {
		return mapper.deleteFavoriteCareerClusterByUserId(userId);
	}

	public int deleteFavoriteOccupationByUserId(int userId) {
		return  mapper.deleteFavoriteOccupationByUserId(userId);
	}

	public int deleteFavoriteSchoolByUserId(int userId) {
		return mapper.deleteFavoriteSchoolByUserId(userId);
	}

	public int deleteFavoriteCitmOccupationByUserId(int userId) {
		return mapper.deleteFavoriteCitmOccupationByUserId(userId);
	}

	public int insertFavoriteCitmOccupation(FavoriteCitmOccupation favoriteObject, Integer userId) {
		return mapper.insertFavoriteCitmOccupation(favoriteObject, userId);
	}

	public ArrayList<FavoriteCitmOccupation> getFavoriteCitmOccupation(int userId) {
		return mapper.getFavoriteCitmOccupation(userId);
	}

	public int insertFavoriteService(FavoriteMilitaryService favoriteObject, Integer userId) {
		return mapper.insertFavoriteService(favoriteObject, userId);
	}

	public int deleteFavoriteServiceByUserId(Integer userId) {
		return mapper.deleteFavoriteServiceByUserId(userId);
	}

	public ArrayList<FavoriteMilitaryService> getFavoriteService(Integer userId) {
		return mapper.getFavoriteService(userId);
	}
}
