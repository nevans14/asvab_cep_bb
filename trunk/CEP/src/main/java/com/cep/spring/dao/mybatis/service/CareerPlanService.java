package com.cep.spring.dao.mybatis.service;

import com.cep.spring.dao.mybatis.mapper.CareerPlanMapper;
import com.cep.spring.model.CareerPlan.CareerPlanAnswer;
import com.cep.spring.model.CareerPlan.CareerPlanSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CareerPlanService {

    private final CareerPlanMapper mapper;

    @Autowired
    public CareerPlanService(CareerPlanMapper mapper) {
        this.mapper = mapper;
    }

    public ArrayList<CareerPlanSection> getItems() {
        return mapper.getItems();
    }

    public CareerPlanAnswer getResponses(Integer userId) {
        return mapper.getResponses(userId);
    }

    public int updateResponses(CareerPlanAnswer model) {
        CareerPlanAnswer hasResponses = getResponses(model.getUserId());
        if (hasResponses == null) {
            return mapper.insertResponses(model);
        }
        return mapper.updateResponses(model);
    }
}
