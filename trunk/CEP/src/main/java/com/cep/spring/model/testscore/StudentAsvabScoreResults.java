package com.cep.spring.model.testscore;

import com.cep.spring.model.AccessCodes;

import java.io.Serializable;

public class StudentAsvabScoreResults implements Serializable {

    private AccessCodes accessCodes;
    private ControlInformation controlInformation;
    private HighSchoolInformation highSchoolInformation;
    private AFQTRawSSComposites afqtRawComposites;
    private VerbalAbility verbalAbility;
    private MathematicalAbility mathematicalAbility;
    private ScienceTechnicalAbility scienceTechnicalAbility;
    private GeneralScience generalScience;
    private ArithmeticReasoning arithmeticReasoning;
    private WordKnowledge wordKnowledge;
    private ParagraphComprehension paragraphComprehension;
    private MathematicsKnowledge mathematicsKnowledge;
    private ElectronicsInformation electronicsInformation;
    private AutoShopInformation autoShopInformation;
    private MechanicalComprehension mechanicalComprehension;

    public AccessCodes getAccessCodes() { return accessCodes; }
    public void setAccessCodes(AccessCodes accessCodes) { this.accessCodes = accessCodes; }

    public ControlInformation getControlInformation() { return controlInformation; }
    public void setControlInformation(ControlInformation controlInformation) { this.controlInformation = controlInformation; }

    public HighSchoolInformation getHighSchoolInformation() { return highSchoolInformation; }
    public void setHighSchoolInformation(HighSchoolInformation highSchoolInformation) { this.highSchoolInformation = highSchoolInformation; }

    public AFQTRawSSComposites getAfqtRawComposites() { return afqtRawComposites; }
    public void setAfqtRawComposites(AFQTRawSSComposites afqtRawComposites) { this.afqtRawComposites = afqtRawComposites; }

    public VerbalAbility getVerbalAbility() { return verbalAbility; }
    public void setVerbalAbility(VerbalAbility verbalAbility) { this.verbalAbility = verbalAbility; }

    public MathematicalAbility getMathematicalAbility() { return mathematicalAbility; }
    public void setMathematicalAbility(MathematicalAbility mathematicalAbility) { this.mathematicalAbility = mathematicalAbility; }

    public ScienceTechnicalAbility getScienceTechnicalAbility() { return scienceTechnicalAbility; }
    public void setScienceTechnicalAbility(ScienceTechnicalAbility scienceTechnicalAbility) { this.scienceTechnicalAbility = scienceTechnicalAbility; }

    public GeneralScience getGeneralScience() { return generalScience; }
    public void setGeneralScience(GeneralScience generalScience) { this.generalScience = generalScience; }

    public ArithmeticReasoning getArithmeticReasoning() { return arithmeticReasoning; }
    public void setArithmeticReasoning(ArithmeticReasoning arithmeticReasoning) { this.arithmeticReasoning = arithmeticReasoning; }

    public WordKnowledge getWordKnowledge() { return wordKnowledge; }
    public void setWordKnowledge(WordKnowledge wordKnowledge) { this.wordKnowledge = wordKnowledge; }

    public ParagraphComprehension getParagraphComprehension() { return paragraphComprehension; }
    public void setParagraphComprehension(ParagraphComprehension paragraphComprehension) { this.paragraphComprehension = paragraphComprehension; }

    public MathematicsKnowledge getMathematicsKnowledge() { return mathematicsKnowledge; }
    public void setMathematicsKnowledge(MathematicsKnowledge mathematicsKnowledge) { this.mathematicsKnowledge = mathematicsKnowledge; }

    public ElectronicsInformation getElectronicsInformation() { return electronicsInformation; }
    public void setElectronicsInformation(ElectronicsInformation electronicsInformation) { this.electronicsInformation = electronicsInformation; }

    public AutoShopInformation getAutoShopInformation() { return autoShopInformation; }
    public void setAutoShopInformation(AutoShopInformation autoShopInformation) { this.autoShopInformation = autoShopInformation; }

    public MechanicalComprehension getMechanicalComprehension() { return mechanicalComprehension; }
    public void setMechanicalComprehension(MechanicalComprehension mechanicalComprehension) { this.mechanicalComprehension = mechanicalComprehension; }
}
