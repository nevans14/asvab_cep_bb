package com.cep.spring.model.testscore;

public class StudentResultSheetScores {

	private	int	milCareerScore;
	private	int	milCareerScoreCategory;
	/**
	 * @return the milCareerScore
	 */
	public int getMilCareerScore() {
		return milCareerScore;
	}
	/**
	 * @param milCareerScore the milCareerScore to set
	 */
	public void setMilCareerScore(int milCareerScore) {
		this.milCareerScore = milCareerScore;
	}
	/**
	 * @return the milCareerScoreCategory
	 */
	public int getMilCareerScoreCategory() {
		return milCareerScoreCategory;
	}
	/**
	 * @param milCareerScoreCategory the milCareerScoreCategory to set
	 */
	public void setMilCareerScoreCategory(int milCareerScoreCategory) {
		this.milCareerScoreCategory = milCareerScoreCategory;
	}

	
}
