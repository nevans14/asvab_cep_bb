package com.cep.spring.model.PlanYourFuture;

import java.io.Serializable;

public class RefGapYearTypes implements Serializable {
    private static final long serialVersionUID = -5104815305358147004L;

    private byte id;
    private String name;

    public byte getId() { return id; }

    public void setId(byte id) {
        this.id = id;
    }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }
}
