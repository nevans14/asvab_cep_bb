package com.cep.spring.model.portfolio;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class AsvabScore implements Serializable {

	private static final long serialVersionUID = -3330669023150097347L;
	private Integer id;
	private double verbalScore;
	private double mathScore;
	private double scienceScore;
	private double afqtScore;
	private String gender;
	private Integer grade;
	private String testLocation;
	private Integer testDay;
	private Integer testMonth;
	private Integer testYear;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the verbalScore
	 */
	public double getVerbalScore() {
		return verbalScore;
	}

	/**
	 * @param verbalScore the verbalScore to set
	 */
	public void setVerbalScore(double verbalScore) {
		this.verbalScore = verbalScore;
	}

	/**
	 * @return the mathScore
	 */
	public double getMathScore() {
		return mathScore;
	}

	/**
	 * @param mathScore the mathScore to set
	 */
	public void setMathScore(double mathScore) {
		this.mathScore = mathScore;
	}

	/**
	 * @return the scienceScore
	 */
	public double getScienceScore() {
		return scienceScore;
	}

	/**
	 * @param scienceScore the scienceScore to set
	 */
	public void setScienceScore(double scienceScore) {
		this.scienceScore = scienceScore;
	}

	/**
	 * @return the afqtScore
	 */
	public double getAfqtScore() {
		return afqtScore;
	}

	/**
	 * @param afqtScore the afqtScore to set
	 */
	public void setAfqtScore(double afqtScore) {
		this.afqtScore = afqtScore;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		if (gender != null && !gender.isEmpty()) {
			this.gender = Jsoup.clean(gender, Whitelist.basic());
		}
	}

	/**
	 * @return the grade
	 */
	public Integer getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(Integer grade) {
		this.grade = grade;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the testLocation
	 */
	public String getTestLocation() {
		return testLocation;
	}

	/**
	 * @param testLocation the testLocation to set
	 */
	public void setTestLocation(String testLocation) {
		if (testLocation != null && !testLocation.isEmpty()) {
			this.testLocation = Jsoup.clean(testLocation, Whitelist.basic());
		}
	}

	/**
	 * @return the testDay
	 */
	public Integer getTestDay() {
		return testDay;
	}

	/**
	 * @param testDay the testDay to set
	 */
	public void setTestDay(Integer testDay) {
		this.testDay = testDay;
	}

	/**
	 * @return the testMonth
	 */
	public Integer getTestMonth() {
		return testMonth;
	}

	/**
	 * @param testMonth the testMonth to set
	 */
	public void setTestMonth(Integer testMonth) {
		this.testMonth = testMonth;
	}

	/**
	 * @return the testYear
	 */
	public Integer getTestYear() {
		return testYear;
	}

	/**
	 * @param testYear the testYear to set
	 */
	public void setTestYear(Integer testYear) {
		this.testYear = testYear;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(afqtScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((grade == null) ? 0 : grade.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		temp = Double.doubleToLongBits(mathScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(scienceScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((testDay == null) ? 0 : testDay.hashCode());
		result = prime * result + ((testLocation == null) ? 0 : testLocation.hashCode());
		result = prime * result + ((testMonth == null) ? 0 : testMonth.hashCode());
		result = prime * result + ((testYear == null) ? 0 : testYear.hashCode());
		temp = Double.doubleToLongBits(verbalScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AsvabScore)) {
			return false;
		}
		AsvabScore other = (AsvabScore) obj;
		if (Double.doubleToLongBits(afqtScore) != Double.doubleToLongBits(other.afqtScore)) {
			return false;
		}
		if (gender == null) {
			if (other.gender != null) {
				return false;
			}
		} else if (!gender.equals(other.gender)) {
			return false;
		}
		if (grade == null) {
			if (other.grade != null) {
				return false;
			}
		} else if (!grade.equals(other.grade)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (Double.doubleToLongBits(mathScore) != Double.doubleToLongBits(other.mathScore)) {
			return false;
		}
		if (Double.doubleToLongBits(scienceScore) != Double.doubleToLongBits(other.scienceScore)) {
			return false;
		}
		if (testDay == null) {
			if (other.testDay != null) {
				return false;
			}
		} else if (!testDay.equals(other.testDay)) {
			return false;
		}
		if (testLocation == null) {
			if (other.testLocation != null) {
				return false;
			}
		} else if (!testLocation.equals(other.testLocation)) {
			return false;
		}
		if (testMonth == null) {
			if (other.testMonth != null) {
				return false;
			}
		} else if (!testMonth.equals(other.testMonth)) {
			return false;
		}
		if (testYear == null) {
			if (other.testYear != null) {
				return false;
			}
		} else if (!testYear.equals(other.testYear)) {
			return false;
		}
		if (Double.doubleToLongBits(verbalScore) != Double.doubleToLongBits(other.verbalScore)) {
			return false;
		}
		return true;
	}

}
