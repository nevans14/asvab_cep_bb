package com.cep.spring.model.fyi;

public class FYIMerge {

	private Integer userId;
	private String datestamp;
	private Integer testId;
	
	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * @return the datestamp
	 */
	public String getDatestamp() {
		return datestamp;
	}
	/**
	 * @param datestamp the datestamp to set
	 */
	public void setDatestamp(String datestamp) {
		this.datestamp = datestamp;
	}
	public Integer getTestId() {
		return testId;
	}
	public void setTestId(Integer testId) {
		this.testId = testId;
	}
	
}
