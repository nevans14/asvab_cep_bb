package com.cep.spring.model.CareerPlan;

import java.util.ArrayList;

public class CareerPlanSection {
    private Integer id;
    private String name, introduction;
    private Short order;
    private ArrayList<CareerPlanItem> items;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
    public void setOrder(Short order) {
        this.order = order;
    }
    public void setItems(ArrayList<CareerPlanItem> items) {
        this.items = items;
    }

    public Integer getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getIntroduction() {
        return introduction;
    }
    public Short getOrder() {
        return order;
    }
    public ArrayList<CareerPlanItem> getItems() {
        return items;
    }
}
