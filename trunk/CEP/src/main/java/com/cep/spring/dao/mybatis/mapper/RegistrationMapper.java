package com.cep.spring.dao.mybatis.mapper;

import com.cep.spring.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RegistrationMapper {

	Users getUser(@Param("userId") Long userId, @Param("username") String username);
	
	Users getByUsername(@Param("username") String username);
	
	int insertUser(@Param("model") Users model);
	
	int updateUser(@Param("model") Users model);

	int updateUsername(@Param("model") Users model);

	int doesUsernameExists(@Param("username") String username);
	
	int insertUserRole(@Param("username") String username, @Param("role") String role);

	int deleteUserRole(@Param("username") String username);
	
	int mergeAccounts(@Param("registrationObject") AccessCodesMerge registrationObject);

	AccessCodes getAccessCodes(@Param("accessCode") String accessCode, @Param("userId") Integer userId);

	Users getUserId(@Param("emailAddress") String emailAddress);

	void changeAccessCodePointer(@Param("userId") Integer userId, @Param("emailAddress") String emailAddress);

	void disableAccessCode(@Param("accessCode") String accessCode);

	Meps getUsersMeps(String accessCode);
	
	int insertAccessCode(@Param("newAccessCodeObj") AccessCodes newAccessCodeObj);
	
	int updateAccessCode(@Param("model") AccessCodes model);
	
	int insertAsvabLog(@Param("userId") Integer userId);	
	
	AccessCodes getLastPromoAccessCodeUsed(@Param("accessCode") String accessCode);
	
	int insertTeacherProfession(@Param("userId") Integer userId, @Param("subject") String subject);
	
	int insertSubscription(@Param("emailAddress") String emailAddress);

	List<RegistrationDropDown> getFunctionList();

	List<RegistrationDropDown> getConferenceList();

	int insertCep123Registration(@Param("userId") Integer userId, @Param("registration") Registration model);
}
