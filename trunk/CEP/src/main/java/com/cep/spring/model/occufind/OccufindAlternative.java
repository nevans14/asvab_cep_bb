package com.cep.spring.model.occufind;

import java.io.Serializable;

public class OccufindAlternative implements Serializable{

	private static final long serialVersionUID = -7616260614482015388L;
	private String description, relatedSocId, relatedTitle;
	private int isBrightOccupation;
	private int isGreenOccupation;
	private int isStemOccupation;

	public String getRelatedTitle() {
		return relatedTitle;
	}

	public void setRelatedTitle(String relatedTitle) {
		this.relatedTitle = relatedTitle;
	}

	public int getIsBrightOccupation() {
		return isBrightOccupation;
	}

	public void setIsBrightOccupation(int isBrightOccupation) {
		this.isBrightOccupation = isBrightOccupation;
	}

	public int getIsGreenOccupation() {
		return isGreenOccupation;
	}

	public void setIsGreenOccupation(int isGreenOccupation) {
		this.isGreenOccupation = isGreenOccupation;
	}

	public int getIsStemOccupation() {
		return isStemOccupation;
	}

	public void setIsStemOccupation(int isStemOccupation) {
		this.isStemOccupation = isStemOccupation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRelatedSocId() {
		return relatedSocId;
	}

	public void setRelatedSocId(String relatedSocId) {
		this.relatedSocId = relatedSocId;
	}
}
