package com.cep.spring.model;

import java.io.Serializable;

public class CareerCluster implements Serializable {

	private static final long serialVersionUID = 1423135192096276445L;
	int ccId;
	String ccTitle, ccDescription;

	public String getCcDescription() {
		return ccDescription;
	}

	public void setCcDescription(String ccDescription) {
		this.ccDescription = ccDescription;
	}

	public int getCcId() {
		return ccId;
	}

	public void setCcId(int ccId) {
		this.ccId = ccId;
	}

	public String getCcTitle() {
		return ccTitle;
	}

	public void setCcTitle(String ccTitle) {
		this.ccTitle = ccTitle;
	}

}
