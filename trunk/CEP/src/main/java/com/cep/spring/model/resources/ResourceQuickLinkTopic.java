package com.cep.spring.model.resources;

import java.io.Serializable;
import java.util.ArrayList;

public class ResourceQuickLinkTopic implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7811851744697926156L;
	private Short quickLinkTopicId;
	private String quickLinkTopicTitle;
	private ArrayList<ResourceQuickLink> quickLinks;
	
	// accessors
	public Short getQuickLinkTopicId() { return quickLinkTopicId; }
	public String getQuickLinkTopicTitle() { return quickLinkTopicTitle; }
	public ArrayList<ResourceQuickLink> getQuickLinks() { return quickLinks; }
	// mutators
	public void setQuickLinkTopicId(Short quickLinkTopicId) { this.quickLinkTopicId = quickLinkTopicId; }
	public void setQuickLinkTopicTitle(String quickLinkTopicTitle) { this.quickLinkTopicTitle = quickLinkTopicTitle; }
	public void setQuickLinks(ArrayList<ResourceQuickLink> quickLinks) { this.quickLinks = quickLinks; }
}