package com.cep.spring.model.portfolio;

import java.io.Serializable;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import javax.json.Json;

public class Education implements Serializable {

	private static final long serialVersionUID = -5012967073681559890L;
	private Integer id;
	private double gpa;
	private String schoolName;
	private String startDate;
	private String endDate;
	private String activities;
	private String achievements;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) {
		this.gpa = gpa;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = Jsoup.clean(schoolName, Whitelist.basic());
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = Jsoup.clean(startDate, Whitelist.basic());
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = Jsoup.clean(endDate, Whitelist.basic());
	}

	public String getActivities() {
		return activities;
	}

	public void setActivities(String activities) {
		this.activities = Jsoup.clean(activities, Whitelist.basic());
	}

	public String getAchievements() {
		return achievements;
	}

	public void setAchievements(String achievements) {
		this.achievements = Jsoup.clean(achievements, Whitelist.basic());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((achievements == null) ? 0 : achievements.hashCode());
		result = prime * result + ((activities == null) ? 0 : activities.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		long temp;
		temp = Double.doubleToLongBits(gpa);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((schoolName == null) ? 0 : schoolName.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Education)) {
			return false;
		}
		Education other = (Education) obj;
		if (achievements == null) {
			if (other.achievements != null) {
				return false;
			}
		} else if (!achievements.equals(other.achievements)) {
			return false;
		}
		if (activities == null) {
			if (other.activities != null) {
				return false;
			}
		} else if (!activities.equals(other.activities)) {
			return false;
		}
		if (endDate == null) {
			if (other.endDate != null) {
				return false;
			}
		} else if (!endDate.equals(other.endDate)) {
			return false;
		}
		if (Double.doubleToLongBits(gpa) != Double.doubleToLongBits(other.gpa)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (schoolName == null) {
			if (other.schoolName != null) {
				return false;
			}
		} else if (!schoolName.equals(other.schoolName)) {
			return false;
		}
		if (startDate == null) {
			if (other.startDate != null) {
				return false;
			}
		} else if (!startDate.equals(other.startDate)) {
			return false;
		}
		return true;
	}
}
