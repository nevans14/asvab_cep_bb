package com.cep.spring.mvc.controller;

import com.cep.spring.CEPMailer;
import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.dao.mybatis.RegistrationDAO;
import com.cep.spring.dao.mybatis.UserDAO;
import com.cep.spring.model.user.SecurityUpdateStatus;
import com.cep.spring.model.user.UpdateCredential;
import com.cep.spring.model.user.UserCompletion;
import com.cep.spring.model.user.AccessCode;
import com.cep.spring.model.AccessCodes;
import com.cep.spring.model.Users;
import com.cep.spring.model.UserRoles;
import com.cep.spring.model.DbInfo;
import com.cep.spring.utils.DbInfoUtils;
import com.cep.spring.utils.UserManager;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    enum Status {
        SUCCESS(0, "Success"),
        NO_DATA(1, "No data"),
        USERNAME_TAKEN(1001, "Username is already taken."),
        USERNAME_NOT_UPDATED(1002, "Username not updated. Please try again later."),
        PASSWORD_NOT_UPDATED(1003, "Password not updated. Please try again later."),
        GENERIC_ERROR(1004, "An error occurred. Please try again later."),
        REQUIRES_CREDENTIALS(1005, "Please provide password to change your credentials."),
        INVALID_CREDENTIALS(1006, "Invalid credentials."),
        INVALID_USERNAME(1007, "Please provide a valid Email Address.");

        private final int statusCode;
        private final String statusMessage;

        Status(int statusCode, String statusMessage) {
            this.statusCode = statusCode;
            this.statusMessage = statusMessage;
        }

        public int getStatusCode() { return statusCode; }
        public String getStatusMessage() { return statusMessage; }
    }

    private final Logger logger = LogManager.getLogger();
    private final RegistrationDAO registrationDao;
    private final DbInfoUtils dbInfoUtils;
    private final DbInfoDAO dbInfoDao;
    private final UserManager userManager;
    private final CEPMailer cepMailer;
    private final BCryptPasswordEncoder encoder;
    private final UserDAO userDao;

    @Autowired
    public UserController(RegistrationDAO registrationDao, DbInfoUtils dbInfoUtils, DbInfoDAO dbInfoDao, UserManager userManager, CEPMailer cepMailer, BCryptPasswordEncoder encoder, UserDAO userDao) {
        this.registrationDao = registrationDao;
        this.dbInfoUtils = dbInfoUtils;
        this.dbInfoDao = dbInfoDao;
        this.userManager = userManager;
        this.cepMailer = cepMailer;
        this.encoder = encoder;
        this.userDao = userDao;
    }

    /***
     * Used by the frontend to ping the server to reset the idle timer of a user's session.
     * @return String "pong" as the front-end as pinged the server
     */
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<String> ping() {
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    /***
     * Used by frontend to check to see if the User's access code is an unregistrable access code. This is
     * to prevent the "Don't Forget to Register Your Access Code" modal when someone logs in with this kind
     * of access code.
     * @return Boolean, determining if the User's access code is an unregistrable access code
     */
    @RequestMapping(value = "/isUsingUnregistrableAccesscode", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Boolean> isUsingUnregistrableAccessCode() {
        boolean isUsingUnregistrableAccessCode;
        try {
            isUsingUnregistrableAccessCode =
                    dbInfoUtils.isAccessCodeUnregistrable(userManager.getUserAccessCode());
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(isUsingUnregistrableAccessCode, HttpStatus.OK);
    }

    /***
     * Returns the user's username if the user is registered
     * @return Users object if the user is registered, then return with username in session, else null
     */
    @RequestMapping(value = "/getUsername", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Users> getUsername() {
        Users user = new Users();
        user.setUsername((userManager.isRegistered()) ? userManager.getUserName() : null);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    /***
     * Returns the user's access code data in the database to be used in the frontend
     * @return AccessCodes object what information is in the database, such as FYI, Role, etc...
     */
    @RequestMapping(value = "/getAccessCode", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<AccessCode> GetAccessCodeByUserId() {
        AccessCode model;
        try {
            AccessCodes result = registrationDao.getAccessCodes(null, userManager.getUserId());
            if (result == null)
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            model = new AccessCode(result.getAccessCode(), result.getRole(), result.getUnlimitedFyi());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    /**
     * Updates the user's credentials
     * @param model the user
     * @return SecurityUpdateStatus security update payload
     */
    @RequestMapping(value = "/updateUsername", method = RequestMethod.POST)
    public @ResponseBody SecurityUpdateStatus updateUserInfo (@RequestBody UpdateCredential model) {
        SecurityUpdateStatus securityUpdateStatus = new SecurityUpdateStatus();
        // check if request object is null or don't have posted credentials
        if (model == null ||
            (model.getUpdatedPassword() == null || model.getUpdatedPassword().isEmpty()) &&
            (model.getUpdatedUsername() == null || model.getUpdatedUsername().isEmpty())) {
            securityUpdateStatus.setStatusCode(Status.NO_DATA.getStatusCode());
            securityUpdateStatus.setStatusMessage(Status.NO_DATA.getStatusMessage());
            return securityUpdateStatus;
        }
        // check if the password is sent up with the request
        if (model.getPassword() == null || model.getPassword().isEmpty()) {
            securityUpdateStatus.setStatusCode(Status.REQUIRES_CREDENTIALS.getStatusCode());
            securityUpdateStatus.setStatusMessage(Status.REQUIRES_CREDENTIALS.getStatusMessage());
            return securityUpdateStatus;
        }
        try {
            // get website url from db
            DbInfo url = dbInfoDao.getDbInfo("website_url");
            // get current user info in the database
            Users currentUser = registrationDao.getUser(Long.valueOf(userManager.getUserId()), null);
            // validate credentials
            if (!encoder.matches(model.getPassword(), currentUser.getPassword())) {
                securityUpdateStatus.setStatusCode(Status.INVALID_CREDENTIALS.getStatusCode());
                securityUpdateStatus.setStatusMessage(Status.INVALID_CREDENTIALS.getStatusMessage());
                return securityUpdateStatus;
            }
            // clean input from user & update user info accordingly...
            if (!model.getUpdatedUsername().equalsIgnoreCase(userManager.getUserName())) {
                model.setUpdatedUsername(Jsoup.clean(model.getUpdatedUsername(), Whitelist.basic()));
                updateUsername(currentUser, model, securityUpdateStatus);
                // if username was not updated, return error message
                if (!securityUpdateStatus.getIsUsernameUpdated()) {
                    return securityUpdateStatus;
                }
            }
            // update user password
            if (!model.getUpdatedPassword().isEmpty()) {
                model.setUpdatedPassword(Jsoup.clean(model.getUpdatedPassword(), Whitelist.basic()));
                updatePassword(currentUser, model, securityUpdateStatus);
                // if password was not updated, return error message
                if (!securityUpdateStatus.getIsUsernameUpdated()) {
                    return securityUpdateStatus;
                }
            }
            // if anything was updated successfully, then send an email
            if (securityUpdateStatus.getIsPasswordUpdated() || securityUpdateStatus.getIsUsernameUpdated()) {
                // send email
                sendSecurityUpdateEmail(currentUser.getUsername(), url.getValue(), securityUpdateStatus);
            }
            // everything was successful
            securityUpdateStatus.setStatusCode(Status.SUCCESS.getStatusCode());
            securityUpdateStatus.setStatusMessage(Status.SUCCESS.getStatusMessage());
        } catch (Exception e) {
            securityUpdateStatus.setStatusCode(Status.GENERIC_ERROR.getStatusCode());
            securityUpdateStatus.setStatusMessage(Status.GENERIC_ERROR.getStatusMessage());
            logger.error("Error", e);
        }
        return securityUpdateStatus;
    }

    @RequestMapping(value = "/get-user-completion", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<UserCompletion>> getUserCompletion() {
        List<UserCompletion> results;
        try {
            results = userDao.getUserCompletion(userManager.getUserId());
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/set-user-completion", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Integer> updateUserCompletion(@RequestBody UserCompletion model) {
        int results;
        try {
            results = userDao.updateUserCompletion(userManager.getUserId(), model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (results == 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete-user-completion/{completionType}/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Integer> deleteUserCompletion(@PathVariable String completionType) {
        int results;
        try {
            // scrub completionType
            completionType = Jsoup.clean(completionType.toUpperCase(Locale.US), Whitelist.basic());
            // delete record
            results = userDao.deleteUserCompletion(userManager.getUserId(), completionType);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (results == 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    /**
     * Updates the current User's username
     * @param currentUser the current user reference in the database
     * @param model the current user reference sent from the client
     */
    private void updateUsername(Users currentUser, UpdateCredential model, SecurityUpdateStatus securityUpdateStatus) {
        // first, check if the username already exists
        int rows = registrationDao.doesUsernameExists(model.getUpdatedUsername());
        if (rows > 0) {
            securityUpdateStatus.setStatusCode(Status.USERNAME_TAKEN.getStatusCode());
            securityUpdateStatus.setStatusMessage(Status.USERNAME_TAKEN.getStatusMessage());
            securityUpdateStatus.setIsUsernameUpdated(false);
            return;
        }
        // second, check if the username is valid
        EmailValidator validator = EmailValidator.getInstance();
        if (!validator.isValid(model.getUpdatedUsername())) {
            securityUpdateStatus.setStatusCode(Status.INVALID_USERNAME.getStatusCode());
            securityUpdateStatus.setStatusMessage(Status.INVALID_USERNAME.getStatusMessage());
            securityUpdateStatus.setIsUsernameUpdated(false);
            return;
        }
        String oldUsername = currentUser.getUsername();
        List<UserRoles> currentRoles = registrationDao
                    .getByUsername(oldUsername)
                    .getUserRoles();
        // update the username
        currentUser.setUsername(model.getUpdatedUsername());
        int rowsAffected =  registrationDao.updateUsername(currentUser);
        // no update, return false
        if (rowsAffected <= 0) {
            securityUpdateStatus.setStatusCode(Status.USERNAME_NOT_UPDATED.getStatusCode());
            securityUpdateStatus.setStatusMessage(Status.USERNAME_NOT_UPDATED.getStatusMessage());
            securityUpdateStatus.setIsUsernameUpdated(false);
            return;
        }
        // delete roles
        registrationDao.deleteUserRole(oldUsername);
        // insert new roles
        for (UserRoles role : currentRoles) {
            registrationDao.insertUserRole(currentUser.getUsername(), role.getRole());
        }
        // update session
        userManager.setUserName(currentUser.getUsername());
        securityUpdateStatus.setIsUsernameUpdated(true);
    }

    /**
     * Updates the current user credentials
     * @param currentUser the current user reference from the database
     * @param model the current user reference from the client
     */
    private void updatePassword(Users currentUser, UpdateCredential model, SecurityUpdateStatus securityUpdateStatus) {
        // hash and set new credentials
        currentUser.setPassword(encoder.encode(model.getUpdatedPassword()));
        // update and return results
        int rowsAffected = registrationDao.updateUser(currentUser);
        if (rowsAffected == 0) {
            securityUpdateStatus.setIsPasswordUpdated(false);
            securityUpdateStatus.setStatusCode(Status.PASSWORD_NOT_UPDATED.getStatusCode());
            securityUpdateStatus.setStatusMessage(Status.PASSWORD_NOT_UPDATED.getStatusMessage());
            return;
        }
        securityUpdateStatus.setIsPasswordUpdated(true);
    }

    /**
     * sends a security email notification to the user about the security updates
     * @param email the email address of the user
     * @param currentUrl the url of the given application (e.g., environment)
     * @param securityUpdateStatus the status payload of what was updated
     */
    private void sendSecurityUpdateEmail(String email, String currentUrl, SecurityUpdateStatus securityUpdateStatus) {
        String subject = "Your ASVAB CEP Account was Updated";
        String updateMessage =
                securityUpdateStatus.getIsUsernameUpdated() && securityUpdateStatus.getIsPasswordUpdated()
                        ? "username & password"
                        : securityUpdateStatus.getIsUsernameUpdated()
                        ? "username"
                        : "password";
        String message =
                "<p>You have updated your account information at <a href='" + currentUrl + "'>asvabprogram.com</a>.</p>" +
                        "<p>We are confirming you updated your " + updateMessage + ".</p>" +
                        "<p>If this was you, then you can safely disregard this message.</p>" +
                        "<p>If you did not make this change, contact us immediately at <a href='mailto:asvabcep@gmail.com'>asvabcep@gmail.com</a>.</p>" +
                        "<p>The ASVAB CEP team</p>";
        try {
            cepMailer.sendEmail(email, message, subject, null, true);
        } catch (Exception e) {
            // log
            logger.error("Error", e);
        }
    }
}
