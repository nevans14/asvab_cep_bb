package com.cep.spring.model.testscore;

public class ElectronicsInformation {

	private	int	EI_YP_Score;
	private	int	EI_GS_Percentage;
	private	int	EI_GOS_Percentage;
	private	int	EI_COMP_Percentage;
	private	int	EI_TP_Percentage;
	private	int	EI_SGS;
	private	int	EI_USL;
	private	int	EI_LSL;
	/**
	 * @return the eI_YP_Score
	 */
	public int getEI_YP_Score() {
		return EI_YP_Score;
	}
	/**
	 * @param eI_YP_Score the eI_YP_Score to set
	 */
	public void setEI_YP_Score(int eI_YP_Score) {
		EI_YP_Score = eI_YP_Score;
	}
	/**
	 * @return the eI_GS_Percentage
	 */
	public int getEI_GS_Percentage() {
		return EI_GS_Percentage;
	}
	/**
	 * @param eI_GS_Percentage the eI_GS_Percentage to set
	 */
	public void setEI_GS_Percentage(int eI_GS_Percentage) {
		EI_GS_Percentage = eI_GS_Percentage;
	}
	/**
	 * @return the eI_GOS_Percentage
	 */
	public int getEI_GOS_Percentage() {
		return EI_GOS_Percentage;
	}
	/**
	 * @param eI_GOS_Percentage the eI_GOS_Percentage to set
	 */
	public void setEI_GOS_Percentage(int eI_GOS_Percentage) {
		EI_GOS_Percentage = eI_GOS_Percentage;
	}
	/**
	 * @return the eI_COMP_Percentage
	 */
	public int getEI_COMP_Percentage() {
		return EI_COMP_Percentage;
	}
	/**
	 * @param eI_COMP_Percentage the eI_COMP_Percentage to set
	 */
	public void setEI_COMP_Percentage(int eI_COMP_Percentage) {
		EI_COMP_Percentage = eI_COMP_Percentage;
	}
	/**
	 * @return the eI_TP_Percentage
	 */
	public int getEI_TP_Percentage() {
		return EI_TP_Percentage;
	}
	/**
	 * @param eI_TP_Percentage the eI_TP_Percentage to set
	 */
	public void setEI_TP_Percentage(int eI_TP_Percentage) {
		EI_TP_Percentage = eI_TP_Percentage;
	}
	/**
	 * @return the eI_SGS
	 */
	public int getEI_SGS() {
		return EI_SGS;
	}
	/**
	 * @param eI_SGS the eI_SGS to set
	 */
	public void setEI_SGS(int eI_SGS) {
		EI_SGS = eI_SGS;
	}
	/**
	 * @return the eI_USL
	 */
	public int getEI_USL() {
		return EI_USL;
	}
	/**
	 * @param eI_USL the eI_USL to set
	 */
	public void setEI_USL(int eI_USL) {
		EI_USL = eI_USL;
	}
	/**
	 * @return the eI_LSL
	 */
	public int getEI_LSL() {
		return EI_LSL;
	}
	/**
	 * @param eI_LSL the eI_LSL to set
	 */
	public void setEI_LSL(int eI_LSL) {
		EI_LSL = eI_LSL;
	}
	
}
