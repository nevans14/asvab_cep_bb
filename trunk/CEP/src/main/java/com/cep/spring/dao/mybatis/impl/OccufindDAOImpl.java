package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.OccufindDAO;
import com.cep.spring.dao.mybatis.service.OccufindService;
import com.cep.spring.model.MilitaryCareerResource;
import com.cep.spring.model.OOHResource;
import com.cep.spring.model.occufind.OccufindSearch;
import com.cep.spring.model.occufind.OccufindSearchArguments;
import com.cep.spring.model.occufind.RelatedCareerCluster;
import com.cep.spring.model.occufind.SchoolMoreDetails;
import com.cep.spring.model.occufind.SchoolProfile;
import com.cep.spring.model.occufind.ServiceOfferingCareer;
import com.cep.spring.model.occufind.TitleDescription;
import com.cep.spring.model.Occupation;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.StateSalary;
import com.cep.spring.model.StateSalaryEntry;
import com.cep.spring.model.occufind.AlternateTitles;
import com.cep.spring.model.occufind.CertificateExplanation;
import com.cep.spring.model.occufind.EmploymentMoreDetails;
import com.cep.spring.model.occufind.HotGreenStem;
import com.cep.spring.model.occufind.JobZone;
import com.cep.spring.model.occufind.MilitaryHotJobs;
import com.cep.spring.model.occufind.MilitaryMoreDetails;
import com.cep.spring.model.occufind.OccufindAlternative;
import com.cep.spring.model.occufind.OccufindMoreResources;

@Repository
public class OccufindDAOImpl implements OccufindDAO {

	private final OccufindService service;

	@Autowired
	public OccufindDAOImpl(OccufindService service) {
		this.service = service;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccufindSearch(com.cep.spring.model.occufind.OccufindSearchArguments)
	 */
	@Override
	public ArrayList<OccufindSearch> getOccufindSearch(OccufindSearchArguments userSearchArguments) { return service.getOccufindSearch(userSearchArguments); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getCertificateExplanation(java.lang.String)
	 */
	@Override
	public CertificateExplanation getCertificateExplanation(String onetSoc) { return service.getCertificateExplanation(onetSoc); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getAlternateTitles(java.lang.String)
	 */
	@Override
	public List<AlternateTitles> getAlternateTitles(String onetSoc) { return service.getAlternateTitles(onetSoc); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccupationTitleDescription(java.lang.String)
	 */
	@Override
	public TitleDescription getOccupationTitleDescription(String onetSoc) {	return service.getOccupationTitleDescription(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccupationASK(java.lang.String)
	 */
	@Override
	public ArrayList<Occupation> getOccupationASK(String onetSoc) {	return service.getOccupationASK(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getMilitaryCareerResources(java.lang.String)
	 */
	@Override
	public ArrayList<MilitaryCareerResource> getMilitaryCareerResources(String onetSoc) { return service.getMilitaryCareerResources(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOOHResources(java.lang.String)
	 */
	@Override
	public ArrayList<OOHResource> getOOHResources(String onetSoc) { return service.getOOHResources(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getAvgSalaryByState(java.lang.String)
	 */
	@Override
	public ArrayList<StateSalary> getAvgSalaryByState(String onetSocTrimmed) { return service.getAvgSalaryByState(onetSocTrimmed); }
	
	/**
	 * Return a list of entry level salary for the occupation by states 
	 */
	@Override
	public ArrayList<StateSalaryEntry> getEntrySalaryByState(String onetSocTrimmed) { return service.getEntrySalaryByState(onetSocTrimmed); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getNationalSalary(java.lang.String)
	 */
	@Override
	public Integer getNationalSalary(String onetSocTrimmed) { return service.getNationalSalary(onetSocTrimmed); }
	
	/**
	 * Returns the national entry level salary for an occupation 
	 */
	@Override
	public Integer getNationalEntrySalary(String onetSocTrimmed) { return service.getNationalEntrySalary(onetSocTrimmed); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getBLSTitle(java.lang.String)
	 */
	@Override
	public String getBLSTitle(String onetSocTrimmed) { return service.getBLSTitle(onetSocTrimmed); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccupationInterestCodes(java.lang.String)
	 */
	@Override
	public ArrayList<OccupationInterestCode> getOccupationInterestCodes(String onetSoc) { return service.getOccupationInterestCodes(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getHotGreenStemFlags(java.lang.String)
	 */
	@Override
	public HotGreenStem getHotGreenStemFlags(String onetSoc) { return service.getHotGreenStemFlags(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getServiceOfferingCareer(java.lang.String)
	 */
	@Override
	public ArrayList<ServiceOfferingCareer> getServiceOfferingCareer(String onetSoc) { return service.getServiceOfferingCareer(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getRelatedCareerCluster(java.lang.String)
	 */
	@Override
	public ArrayList<RelatedCareerCluster> getRelatedCareerCluster(String onetSoc) { return service.getRelatedCareerCluster(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getSchoolMoreDetails(java.lang.String)
	 */
	@Override
	public ArrayList<SchoolMoreDetails> getSchoolMoreDetails(String onetSoc) { return service.getSchoolMoreDetails(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getStateSalaryYear()
	 */
	@Override
	public Integer getStateSalaryYear() { return service.getStateSalaryYear(); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getEmploymentMoreDetails(java.lang.String)
	 */
	@Override
	public ArrayList<EmploymentMoreDetails> getEmploymentMoreDetails(String trimmedOnetSoc) { return service.getEmploymentMoreDetails(trimmedOnetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getMilitaryMoreDetails(java.lang.String)
	 */
	@Override
	public ArrayList<MilitaryMoreDetails> getMilitaryMoreDetails(String onetSoc) { return service.getMilitaryMoreDetails(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getMilitaryHotJobs(java.lang.String)
	 */
	@Override
	public ArrayList<MilitaryHotJobs> getMilitaryHotJobs(String onetSoc) { return service.getMilitaryHotJobs(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getOccufindMoreResources(java.lang.String)
	 */
	@Override
	public ArrayList<OccufindMoreResources> getOccufindMoreResources(String onetSoc) { return service.getOccufindMoreResources(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getDashboardBrightOutlook(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ArrayList<OccufindSearch> getDashboardBrightOutlook(String interestCdOne, String interestCdTwo,
			String interestCdThree) { return service.getDashboardBrightOutlook(interestCdOne, interestCdTwo, interestCdThree); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getDashboardStemCareers(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ArrayList<OccufindSearch> getDashboardStemCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) { return service.getDashboardStemCareers(interestCdOne, interestCdTwo, interestCdThree); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getDashboardGreenCareers(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ArrayList<OccufindSearch> getDashboardGreenCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) { return service.getDashboardGreenCareers(interestCdOne, interestCdTwo, interestCdThree); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#viewAllOccupations()
	 */
	@Override
	public ArrayList<OccufindSearch> viewAllOccupations() { return service.viewAllOccupations(); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#viewMilitaryOccupations()
	 */
	@Override
	public ArrayList<OccufindSearch> viewMilitaryOccupations() { return service.viewMilitaryOccupations(); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#viewHotOccupations()
	 */
	@Override
	public ArrayList<OccufindSearch> viewHotOccupations() { return service.viewHotOccupations(); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#onetHideDetails(java.lang.String)
	 */
	@Override
	public ArrayList<OccufindAlternative> onetHideDetails(String onetSoc) { return service.onetHideDetails(onetSoc); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getImageAltText(java.lang.String)
	 */
	@Override
	public String getImageAltText(String onetSoc) { return service.getImageAltText(onetSoc); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.OccufindDAO#getJobZone(java.lang.String)
	 */
	@Override
	public JobZone getJobZone(String onetSoc) { return service.getJobZone(onetSoc); }

	@Override
	public SchoolProfile getSchoolProfile(int unitId) {
		return service.getSchoolProfile(unitId);
	}

	@Override
	public List<String> getCareerMajors(String onetCode) {
		return service.getCareerMajors(onetCode);
	}
}
