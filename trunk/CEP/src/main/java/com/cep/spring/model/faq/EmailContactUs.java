package com.cep.spring.model.faq;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class EmailContactUs implements Serializable {

	private static final long serialVersionUID = 874683555665428550L;
	String userType, firstName, lastName, email, topics, message, recaptchaResponse;

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getRecaptchaResponse() {
		return recaptchaResponse;
	}

	public void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = Jsoup.clean(firstName, Whitelist.basic());
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = Jsoup.clean(lastName, Whitelist.basic());
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = Jsoup.clean(email, Whitelist.basic());
	}

	public String getTopics() {
		return topics;
	}

	public void setTopics(String topics) {
		this.topics = Jsoup.clean(topics, Whitelist.basic());
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = Jsoup.clean(message, Whitelist.basic());
	}

}
