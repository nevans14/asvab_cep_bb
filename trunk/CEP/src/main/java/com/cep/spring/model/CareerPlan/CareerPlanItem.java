package com.cep.spring.model.CareerPlan;

import java.io.Serializable;
import java.util.ArrayList;

public class CareerPlanItem implements Serializable {



    private Integer id;
    private String name, stem;
    private Short order, typeId;
    private ArrayList<CareerPlanItem> children;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setStem(String stem) {
        this.stem = stem;
    }
    public void setOrder(Short order) {
        this.order = order;
    }
    public void setTypeId(Short typeId) {
        this.typeId = typeId;
    }
    public void setChildren(ArrayList<CareerPlanItem> children) {
        this.children = children;
    }

    public Integer getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getStem() {
        return stem;
    }
    public Short getOrder() {
        return order;
    }
    public Short getTypeId() {
        return typeId;
    }
    public ArrayList<CareerPlanItem> getChildren() {
        return children;
    }
}
