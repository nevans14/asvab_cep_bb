package com.cep.spring.dao.mybatis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cep.spring.model.MilitaryCareerResource;
import com.cep.spring.model.OOHResource;
import com.cep.spring.model.Occupation;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.StateSalary;
import com.cep.spring.model.StateSalaryEntry;
import com.cep.spring.model.occufind.AlternateTitles;
import com.cep.spring.model.occufind.CertificateExplanation;
import com.cep.spring.model.occufind.EmploymentMoreDetails;
import com.cep.spring.model.occufind.HotGreenStem;
import com.cep.spring.model.occufind.JobZone;
import com.cep.spring.model.occufind.MilitaryHotJobs;
import com.cep.spring.model.occufind.MilitaryMoreDetails;
import com.cep.spring.model.occufind.OccufindAlternative;
import com.cep.spring.model.occufind.OccufindMoreResources;
import com.cep.spring.model.occufind.OccufindSearch;
import com.cep.spring.model.occufind.OccufindSearchArguments;
import com.cep.spring.model.occufind.RelatedCareerCluster;
import com.cep.spring.model.occufind.SchoolMajors;
import com.cep.spring.model.occufind.SchoolMoreDetails;
import com.cep.spring.model.occufind.SchoolProfile;
import com.cep.spring.model.occufind.ServiceOfferingCareer;
import com.cep.spring.model.occufind.TitleDescription;

public interface OccufindDAO {

	ArrayList<OccufindSearch> getOccufindSearch(OccufindSearchArguments userSearchArguments);

	CertificateExplanation getCertificateExplanation(String onetSoc);

	List<AlternateTitles> getAlternateTitles(String onetSoc);

	TitleDescription getOccupationTitleDescription(String onetSoc);

	ArrayList<Occupation> getOccupationASK(String onetSoc);

	ArrayList<MilitaryCareerResource> getMilitaryCareerResources(String onetSoc);

	ArrayList<OOHResource> getOOHResources(String onetSoc);

	ArrayList<StateSalary> getAvgSalaryByState(String onetSocTrimmed);
	
	ArrayList<StateSalaryEntry> getEntrySalaryByState(String onetSocTrimmed);

	Integer getNationalSalary(String onetSocTrimmed);
	
	Integer getNationalEntrySalary(String onetSocTrimmed);

	String getBLSTitle(String onetSocTrimmed) throws Exception;

	ArrayList<OccupationInterestCode> getOccupationInterestCodes(String onetSoc);

	HotGreenStem getHotGreenStemFlags(String onetSoc);

	ArrayList<ServiceOfferingCareer> getServiceOfferingCareer(String onetSoc);

	ArrayList<RelatedCareerCluster> getRelatedCareerCluster(String onetSoc);

	ArrayList<SchoolMoreDetails> getSchoolMoreDetails(String onetSoc);

	Integer getStateSalaryYear() throws Exception;

	ArrayList<EmploymentMoreDetails> getEmploymentMoreDetails(String trimmedOnetSoc);

	ArrayList<MilitaryMoreDetails> getMilitaryMoreDetails(String onetSoc);

	ArrayList<MilitaryHotJobs> getMilitaryHotJobs(String onetSoc);

	ArrayList<OccufindMoreResources> getOccufindMoreResources(String onetSoc) throws Exception;

	ArrayList<OccufindSearch> getDashboardBrightOutlook(String interestCdOne, String interestCdTwo,
			String interestCdThree) throws Exception;

	ArrayList<OccufindSearch> getDashboardStemCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) throws Exception;

	ArrayList<OccufindSearch> getDashboardGreenCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) throws Exception;

	ArrayList<OccufindSearch> viewAllOccupations() throws Exception;

	ArrayList<OccufindSearch> viewMilitaryOccupations() throws Exception;

	ArrayList<OccufindSearch> viewHotOccupations() throws Exception;

	ArrayList<OccufindAlternative> onetHideDetails(String onetSoc) throws Exception;

	String getImageAltText(String onetSoc) throws Exception;

	JobZone getJobZone(String onetSoc) throws Exception;

	SchoolProfile getSchoolProfile(int unitId);
	
	List<String> getCareerMajors(String onetCode);
}