package com.cep.spring.model.PlanYourFuture;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class UserSchoolPlan implements Serializable {
    private static final long serialVersionUID = 2329259431064716001L;

    private int planId;
    private String graduationDate;
    private ArrayList<UserSchoolPlanCourse> courses;
    private ArrayList<UserSchoolPlanActivity> activities;

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(String graduationDate) throws ParseException {
        if (graduationDate != null) {
            Date date;
            try {
                // coming from UI, if fails (maybe coming from db)
                date = new SimpleDateFormat("dd/MM/yyyy").parse(graduationDate);
                this.graduationDate = new SimpleDateFormat("MM/dd/yyyy").format(date);
            } catch (ParseException e) {
                // maybe coming from db, if fails, then invalid format
                date = new SimpleDateFormat("yyyy-MM-dd").parse(graduationDate);
                this.graduationDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
            }
        }
    }

    public ArrayList<UserSchoolPlanCourse> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<UserSchoolPlanCourse> courses) {
        this.courses = courses;
    }

    public ArrayList<UserSchoolPlanActivity> getActivities() {
        return activities;
    }

    public void setActivities(ArrayList<UserSchoolPlanActivity> activities) {
        this.activities = activities;
    }
}
