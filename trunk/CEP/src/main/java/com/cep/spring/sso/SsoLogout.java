package com.cep.spring.sso;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

import com.cep.spring.CustomCookieCsrfTokenRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

/**
 * This class deletes cookie used by Angular.
 *
 */
@PropertySource(value={"classpath:SsoProperties.properties", "classpath:application.properties"})
public class SsoLogout extends SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {

	private final Logger logger = LogManager.getLogger();
	private final CustomCookieCsrfTokenRepository cookieCsrfTokenRepository;

	@Value("${security.cookie.secure}")
	private boolean isSecure;

	@Autowired
	public SsoLogout(CustomCookieCsrfTokenRepository cookieCsrfTokenRepository) {
		this.cookieCsrfTokenRepository = cookieCsrfTokenRepository;
	}
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		try {
			// clear cookies
			for (Cookie cookie : request.getCookies()) {
				cookie.setValue(null); 		// null value
				cookie.setMaxAge(0); 		// ends immediately
				cookie.setSecure(isSecure); // set is secure
				response.addCookie(cookie);
			}
			// clears out the current CSRF token and creates a new one
			cookieCsrfTokenRepository.saveToken(null, request, response);
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}
}
