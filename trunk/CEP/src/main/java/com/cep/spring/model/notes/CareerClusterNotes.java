package com.cep.spring.model.notes;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class CareerClusterNotes implements Serializable {

	private static final long serialVersionUID = -922654814722844955L;
	private Integer noteId, ccId;
	private String notes, title;

	public Integer getNoteId() {
		return noteId;
	}

	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}

	public Integer getCcId() {
		return ccId;
	}

	public void setCcId(Integer ccId) {
		this.ccId = ccId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = Jsoup.clean(notes, Whitelist.basic());
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = Jsoup.clean(title, Whitelist.basic());
	}
}
