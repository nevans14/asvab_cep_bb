package com.cep.spring.model.user;

public class UpdateCredential {
    private String password, updatedUsername, updatedPassword;

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setUpdatedUsername(String updatedUsername) {
        this.updatedUsername = updatedUsername;
    }

    public String getUpdatedUsername() {
        return updatedUsername;
    }

    public void setUpdatedPassword(String updatedPassword) {
        this.updatedPassword = updatedPassword;
    }

    public String getUpdatedPassword() {
        return updatedPassword;
    }
}
