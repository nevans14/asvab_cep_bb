package com.cep.spring.dao.mybatis.impl;

import com.cep.spring.dao.mybatis.ClassroomActivityDAO;
import com.cep.spring.dao.mybatis.service.ClassroomActivityService;
import com.cep.spring.model.ClassroomActivity.ClassroomActivity;
import com.cep.spring.model.ClassroomActivity.ClassroomActivityCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClassroomActivityDAOImpl implements ClassroomActivityDAO {

    private final ClassroomActivityService service;

    @Autowired
    public ClassroomActivityDAOImpl(ClassroomActivityService service) {
        this.service = service;
    }

    public List<ClassroomActivityCategory> getClassroomActivityCategories() {
        return service.getClassroomActivityCategories();
    }

    public List<ClassroomActivity> getClassroomActivities(String role) {
        return service.getClassroomActivities(role);
    }

}
