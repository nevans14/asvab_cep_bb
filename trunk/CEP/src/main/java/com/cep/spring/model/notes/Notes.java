package com.cep.spring.model.notes;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class Notes implements Serializable {

	private static final long serialVersionUID = 3068501387085565254L;
	private Integer noteId;
	private String socId, notes, title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = Jsoup.clean(title, Whitelist.basic());
	}

	public Integer getNoteId() {
		return noteId;
	}

	public void setNoteId(Integer noteId) {
		this.noteId = noteId;
	}

	public String getSocId() {
		return socId;
	}

	public void setSocId(String socId) {
		this.socId = Jsoup.clean(socId, Whitelist.basic());
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = Jsoup.clean(notes, Whitelist.basic());
	}
}
