package com.cep.spring.mvc.controller;

import com.cep.spring.dao.mybatis.CareerPlanDAO;
import com.cep.spring.model.CareerPlan.CareerPlanAnswer;
import com.cep.spring.model.CareerPlan.CareerPlanSection;
import com.cep.spring.utils.UserManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping(value = "/career-plan")
public class CareerPlanController {

    private final Logger logger = LogManager.getLogger();
    private final CareerPlanDAO careerPlanDAO;
    private final UserManager userManager;

    @Autowired
    public CareerPlanController(CareerPlanDAO careerPlanDAO, UserManager userManager) {
        this.careerPlanDAO = careerPlanDAO;
        this.userManager = userManager;
    }

    @RequestMapping(value = "/get-items", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<ArrayList<CareerPlanSection>> getItems() {
        ArrayList<CareerPlanSection> results;
        try {
            results = careerPlanDAO.getItems();
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/get-responses", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<CareerPlanAnswer> getResponses() {
        Integer userId = userManager.getUserId();
        CareerPlanAnswer results;
        if (userId != null) {
            try {
                results = careerPlanDAO.getResponses(userId);
            } catch (Exception e) {
                logger.error("Error", e);
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(results, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/update-responses", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Integer> updateResponses(@RequestBody CareerPlanAnswer model) {
        int rowsAffected;
        // set model with user id
        model.setUserId(userManager.getUserId());
        try {
            rowsAffected = careerPlanDAO.updateResponses(model);
        } catch (Exception e) {
            logger.error("Error", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(rowsAffected, HttpStatus.OK);
    }
}
