package com.cep.spring;

import java.io.InputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.cep.spring.dao.mybatis.EssTrainingDAO;
import com.cep.spring.dao.mybatis.ContactUsDAO;
import com.cep.spring.utils.SimpleFileUtils;

import com.cep.spring.model.EssTraining.EssTrainingEmail;

@Component
public class CEPMailer {

    private final Logger logger = LogManager.getLogger();
    private final JavaMailSender mailSender;
    private final SimpleMailMessage templateMessage;
    private final ContactUsDAO contactUsDAO;
    private final EssTrainingDAO essTrainingDAO;
    private final String ASVAB_CEP_EMAIL = "postmaster@asvabprogram.com";
    
    @Autowired
    public CEPMailer(JavaMailSender mailSender, SimpleMailMessage templateMessage, ContactUsDAO contactUsDAO, EssTrainingDAO essTrainingDAO) {
        this.mailSender = mailSender;
        this.templateMessage = templateMessage;
        this.contactUsDAO = contactUsDAO;
        this.essTrainingDAO = essTrainingDAO;
        
    }

    /**
     * sendEmailFromCep : Utilize spring to send e-mails.
     * 
     * @param emailAddr
     *            e-mail address to send the email.
     * @param emailMessage
     *            body of the e-mail sent to above address.
     * @param subject
     *            subject for the email.
     */
    public void sendEmailFromCep(String emailAddr, String emailMessage, String subject) {
    	if(isSubscribed(emailAddr))
    		return;
        logger.info("sendEmailFromCep");
        // Create a thread safe "copy" of the template message and customize it
        SimpleMailMessage msg = new SimpleMailMessage(this.templateMessage);
        msg.setTo(emailAddr);
        msg.setText(emailMessage);
        msg.setSubject(subject);        
        try {
            logger.debug("email Address" + msg.getTo());
            this.mailSender.send(msg);
        } catch (MailException e) {
            logger.error("Mail Error", e);
        } catch (Exception e) {
            logger.error("Error", e);
        }
    }
    
    /***
     * Sends an confirmation email to the registered user for ESS Training
     * @param emailAddress to whom to send the email to
     */
    public void sendEssTrainingRegistrationConfirmationEmail(String emailAddress, int essTrainingId) throws Exception {    	
    	// get ess training info from db
    	EssTrainingEmail model = essTrainingDAO.getEssTrainingEmailById(essTrainingId);
    	if (model == null) {
    		throw new Exception("Confirmation Email does not exist for ESS Training ID: " + essTrainingId);
    	}
    	// set up variables
    	MimeMessage mimeMessage ;
    	MimeMessageHelper mimeMessageHelper;
    	FileSystemResource file;
    	// set up properties
    	mimeMessage = mailSender.createMimeMessage();
    	mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
    	mimeMessageHelper.setText(model.getContent(), true);
    	mimeMessageHelper.setTo(emailAddress);
    	mimeMessageHelper.setSubject(model.getTitle());
    	mimeMessageHelper.setFrom(ASVAB_CEP_EMAIL);
    	// add file attachment
    	if (model.getAttachmentLocation() != null && !model.getAttachmentLocation().isEmpty()) {
    		file = new FileSystemResource(model.getAttachmentLocation());
            mimeMessageHelper.addAttachment(file.getFilename(), file);
    	}        
    	// send message
    	this.mailSender.send(mimeMessage);
    }

    /**
     * Utilizes Mail Sender to send out emails.
     * 
     * @param emailAddr to whom to send the email to
     * @param emailMessage the body of the email
     * @param subject the subject of the email
     * @param attachement attachment (e.g., file/pdf)
     * @throws Exception possible send failure
     */
    public void sendEmail(String emailAddr, String emailMessage, String subject, String attachement) throws Exception {
        this.sendEmail(emailAddr, "", emailMessage, subject, attachement);
    }

    /**
     * Utilizes Mail Sender to send out emails.
     *
     * @param emailAddr to whom to send the email to
     * @param emailMessage the body of the email
     * @param subject the subject of the email
     * @param attachment attachment (e.g., file/pdf)
     * @param isHtml send the email in HTML?
     * @throws Exception possible send failure
     */
    public void sendEmail(String emailAddr, String emailMessage, String subject, String attachment, boolean isHtml) throws Exception {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(emailAddr);
        helper.setFrom(ASVAB_CEP_EMAIL);
        helper.setSubject(subject);
        helper.setText(emailMessage);
        if (isHtml) {
            MimeMultipart multipart = new MimeMultipart("related");
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(emailMessage, "text/html");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
        }
        if (attachment != null) {
            FileSystemResource file = new FileSystemResource(attachment);
            helper.addAttachment(file.getFilename(), file);
        }
        this.mailSender.send(message);
    }

    /**
     * Utilizes Mail Sender to send out emails.
     * 
     * @param toEmailAddr to whom to send the email to
     * @param fromEmailAddr whom is this from
     * @param emailMessage the body of the email
     * @param subject the subject of the email
     * @param attachement the attachment (e.g., file/pdf)
     * @throws Exception possible send failure
     */
    public void sendEmail(String toEmailAddr, String fromEmailAddr, String emailMessage, String subject, String attachement) throws Exception {    	
        logger.info("sendEmail");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(toEmailAddr);
        helper.setText(emailMessage);
        helper.setSubject(subject);
        helper.setFrom(ASVAB_CEP_EMAIL);

        if (!fromEmailAddr.contentEquals("")) {
            helper.setReplyTo(fromEmailAddr);
        } else {
            helper.setReplyTo("asvabcep@gmail.com");
        }
        // add file attachment
        if (attachement != null) {
            FileSystemResource file = new FileSystemResource(attachement);
            helper.addAttachment(file.getFilename(), file);
        }
        this.mailSender.send(message);
    }

    /**
     * Utilizes Mail Sender to send out emails.
     * 
     * @param toEmailAddr to whom to send this email to
     * @param fromEmailAddr who is the email from
     * @param emailMessage the body of the email
     * @param subject the subject of the email
     * @param attachement the attachment of the email (e.g., file/pdf)
     * @throws Exception possible send failure
     */
    public synchronized void sendResetRegisterHtmlEmail(String toEmailAddr, String fromEmailAddr, String emailMessage, String subject, String attachement) throws Exception {
        logger.info("sendEmail");
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(toEmailAddr);
        helper.setFrom(ASVAB_CEP_EMAIL);
        helper.setText(emailMessage);
        helper.setSubject(subject);
        final String CONTENT_ID = "Content-ID";
    	final String IMAGE_PNG = "image/png";
    
        if (!fromEmailAddr.contentEquals(""))
            helper.setFrom(fromEmailAddr);

        MimeMultipart multipart = new MimeMultipart("related");
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(emailMessage, "text/html");
        multipart.addBodyPart(messageBodyPart);
        InputStream imageStream = null;
        try {
            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/email-header.png");
            DataSource fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), IMAGE_PNG);
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader(CONTENT_ID, "<emailHeader>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/twitter.png");
            fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), IMAGE_PNG);
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader(CONTENT_ID, "<twitter>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/youtube.png");
            fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), IMAGE_PNG);
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader(CONTENT_ID, "<youtube>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/facebook.png");
            fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), IMAGE_PNG);
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader(CONTENT_ID, "<facebook>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            messageBodyPart = new MimeBodyPart();
            imageStream = SimpleFileUtils.getInputStreamFromFileOnClasspath("/linkedin.png");
            fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), IMAGE_PNG);
            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader(CONTENT_ID, "<linkedin>");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            this.mailSender.send(message);
        } catch (MailException e) {
            logger.error("Mail Error", e);
        } catch (Exception e) {
            logger.error("Error", e);
        } finally {
            SimpleFileUtils.safeClose(imageStream);
        }
    }
    
    private boolean isSubscribed(String email) {
    	return contactUsDAO.isSubscribed(email);
    }
}
