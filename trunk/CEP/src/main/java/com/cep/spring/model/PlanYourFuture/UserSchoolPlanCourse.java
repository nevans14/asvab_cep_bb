package com.cep.spring.model.PlanYourFuture;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class UserSchoolPlanCourse implements Serializable {
    private static final long serialVersionUID = -4884445642654059952L;

    private int id, planId;
    private String type, course, location, benefit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (type != null && !type.isEmpty()) {
            this.type = Jsoup.clean(type, Whitelist.basic());
        }
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        if (course != null && !course.isEmpty()) {
            this.course = Jsoup.clean(course, Whitelist.basic());
        }
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        if (location != null && !location.isEmpty()) {
            this.location = Jsoup.clean(location, Whitelist.basic());
        }
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        if (benefit != null && !benefit.isEmpty()) {
            this.benefit = Jsoup.clean(benefit, Whitelist.basic());
        }
    }
}
