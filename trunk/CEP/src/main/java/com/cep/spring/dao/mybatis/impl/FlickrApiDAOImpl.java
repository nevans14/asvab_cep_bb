package com.cep.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

import com.cep.spring.dao.mybatis.FlickrApiDAO;
import com.cep.spring.dao.mybatis.service.FlickrApiService;
import com.cep.spring.model.optionready.FlickrPhotoInfo;

@Repository
public class FlickrApiDAOImpl implements FlickrApiDAO {

	private final FlickrApiService service;

	@Autowired
	public FlickrApiDAOImpl(FlickrApiService service) {
		this.service = service;
	}

	public ArrayList<FlickrPhotoInfo> getPhotoCollection(boolean isDebugging) throws Exception {
		return service.getPhotoCollection(isDebugging);
	}
}