package com.cep.spring.model.testscore;

public class Status {

	
	boolean icat_status;
	boolean pnp_status;
	
	public boolean isIcat_status() {
		return icat_status;
	}

	public void setIcat_status(boolean icat_status) {
			this.icat_status = icat_status;
	}

	public boolean isPnp_status() {
		return pnp_status;
	}

	public void setPnp_status(boolean pnp_status) {
			this.pnp_status = pnp_status;
	}

	
}
