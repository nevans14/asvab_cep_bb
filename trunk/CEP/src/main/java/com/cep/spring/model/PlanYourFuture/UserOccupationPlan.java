package com.cep.spring.model.PlanYourFuture;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserOccupationPlan implements Serializable {
    private static final long serialVersionUID = -2506824773592657647L;

    private int id;
    private String socId, pathway, additionalNotes;
    private ArrayList<UserOccupationPlanCalendar> calendarDates;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSocId() {
        return socId;
    }

    public void setSocId(String socId) {
        if (socId != null && !socId.isEmpty()) {
            Pattern pattern = Pattern.compile("^(\\d){2}-(\\d){4}\\.(\\d){2}$");
            Matcher matcher = pattern.matcher(socId);
            boolean matchFound = matcher.find();
            if (!matchFound) {
                throw new IllegalArgumentException("SOC ID needs to be in format XX-XXXX.XX");
            }
            this.socId = socId;
        }
    }

    public String getPathway() {
        return pathway;
    }

    public void setPathway(String pathway) {
        if (pathway != null && !pathway.isEmpty()) {
            this.pathway = Jsoup.clean(pathway, Whitelist.basic());
        }
    }

    public String getAdditionalNotes() {
        return additionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        if (additionalNotes != null && !additionalNotes.isEmpty()) {
            this.additionalNotes = Jsoup.clean(additionalNotes, Whitelist.basic());
        }
    }

    public ArrayList<UserOccupationPlanCalendar> getCalendarDates() {
        return calendarDates;
    }

    public void setCalendarDates(ArrayList<UserOccupationPlanCalendar> calendarDates) {
        this.calendarDates = calendarDates;
    }
}
