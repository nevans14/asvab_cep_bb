package com.cep.spring.model.occufind;

import java.io.Serializable;

public class EmploymentMoreDetails implements Serializable{

	private static final long serialVersionUID = 1508374091356308299L;
	
	private double baseNum, projectedNum, changePercentage;

	public double getBaseNum() {
		return baseNum;
	}

	public void setBaseNum(double baseNum) {
		this.baseNum = baseNum;
	}

	public double getProjectedNum() {
		return projectedNum;
	}

	public void setProjectedNum(double projectedNum) {
		this.projectedNum = projectedNum;
	}

	public double getChangePercentage() {
		return changePercentage;
	}

	public void setChangePercentage(double changePercentage) {
		this.changePercentage = changePercentage;
	}

}
