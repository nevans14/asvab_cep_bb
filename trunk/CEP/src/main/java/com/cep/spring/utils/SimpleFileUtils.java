package com.cep.spring.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class SimpleFileUtils {

	private static final Logger logger = LogManager.getLogger();
    
	public synchronized static InputStream getInputStreamFromFileOnClasspath(String path) {
		return SimpleFileUtils.class.getResourceAsStream(path);
	}

	public static synchronized void appendStringToFile(String fileName, String textToWrite) {
		BufferedWriter bw = null;
		FileWriter fw = null;
		try	{
			File file = new File(fileName);
    		//if file doesnt exists, then create it
    		if(!file.exists()){
    			file.createNewFile();
    		}
    		fw = new FileWriter(fileName, true);
			bw = new BufferedWriter(fw);
			bw.write(textToWrite);
		} catch (IOException e) {
			logger.error("Error", e);
		} finally {
			safeClose(bw);
			safeClose(fw);
		}
	}

	/***
	 * closes a stream to prevent stream leakage
	 * @param closeable stream
	 */
	public static synchronized void safeClose(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				logger.error("IO Error", e);
			} catch (Exception e) {
				logger.error("Error", e);
			}
		}
	}
}
