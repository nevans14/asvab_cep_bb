package com.cep.spring.filter;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

public class SessionFilter implements Filter {

    @Override
    public void init(FilterConfig config) throws ServletException { }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            Cookie session = Arrays.stream(cookies).filter(x -> x.getName().equals("SESSION")).findFirst().orElse(null);
            if (session != null) {
                boolean isSecure = Boolean.parseBoolean(
                    getResourceProperty("application.properties", "security.cookie.secure").toString()
                );
                String domain = getResourceProperty("SsoProperties.properties", "cep.domain").toString();
                if (!domain.equals("localhost")) {
                    session.setPath("/");
                } else {
                    session.setPath("/CEP/");
                }
                session.setSecure(isSecure);
                session.setHttpOnly(true);
                res.addCookie(session);
            }
        }
        chain.doFilter(req, res);
    }

    @Override
    public void destroy() { }

    private Object getResourceProperty(String resourceName, String propName) throws IOException {
        Resource resource = new ClassPathResource(resourceName);
        Properties props = PropertiesLoaderUtils.loadProperties(resource);
        return props.get(propName);
    }
}
