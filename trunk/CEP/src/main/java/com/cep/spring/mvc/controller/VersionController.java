package com.cep.spring.mvc.controller; 

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * VersionController class is very light weight web service that provides
 * the application version defined in the pom.xml file via the 
 * application.properties file.
 * 
 * @author Dave Springer
 *
 */
@RestController
@RequestMapping("/")
@PropertySource("classpath:application.properties")
public class VersionController  {

	@Value("${application.version}")
	private String version;
	@Value("${application.last.updated}")
	private String lastUpdated;

	@RequestMapping(value = "get-token", method = RequestMethod.GET)
	public void getToken () { }

	@RequestMapping(value = "app-info", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, String> appInfo() {
		return new HashMap<String, String>() {			 
			private static final long serialVersionUID = -2153803449788140982L;
			{
				put("version", version);
				put("lastUpdated", lastUpdated);
			}};
	}
}