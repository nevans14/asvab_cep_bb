package com.cep.spring.model.PlanYourFuture;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class UserWorkPlan implements Serializable {
    private static final long serialVersionUID = 6672574679132490590L;

    private int planId;
    private String workPlan;

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getWorkPlan() {
        return workPlan;
    }

    public void setWorkPlan(String workPlan) {
        if (workPlan != null && !workPlan.isEmpty()) {
            this.workPlan = Jsoup.clean(workPlan, Whitelist.basic());
        }
    }
}
