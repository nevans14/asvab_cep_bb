package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.service.SearchService;
import com.cep.spring.model.search.Search;

@Repository
public class SearchDAOImpl {

	private final SearchService service;

	@Autowired
	public SearchDAOImpl(SearchService service) {
		this.service = service;
	}

	public ArrayList<Search> getFileName(String searchString) { return service.getFileName(searchString); }
}
