package com.cep.spring.model.occufind;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class OccufindSearchArguments implements Serializable {

	private static final long serialVersionUID = 5536248566332999666L;
	private char interestCodeOne;
	private char interestCodeTwo;

	@JsonProperty
	private boolean brightOccupationFlag;

	@JsonProperty
	private boolean greenOccupationFlag;

	@JsonProperty
	private boolean stemOccupationFlag;

	@JsonProperty
	private boolean hotOccupationFlag;
	
	private String userSearchString;

	public char getInterestCodeOne() {
		return interestCodeOne;
	}

	public void setInterestCodeOne(char interestCodeOne) {
		this.interestCodeOne = interestCodeOne;
	}

	public char getInterestCodeTwo() {
		return interestCodeTwo;
	}

	public void setInterestCodeTwo(char interestCodeTwo) {
		this.interestCodeTwo = interestCodeTwo;
	}

	public String getUserSearchString() {
		return userSearchString;
	}

	public void setUserSearchString(String userSearchString) {
		this.userSearchString = Jsoup.clean(userSearchString, Whitelist.basic());
	}

	public boolean isBrightOccupationFlag() {
		return brightOccupationFlag;
	}

	public void setBrightOccupationFlag(boolean brightOccupationFlag) {
		this.brightOccupationFlag = brightOccupationFlag;
	}

	public boolean isGreenOccupationFlag() {
		return greenOccupationFlag;
	}

	public void setGreenOccupationFlag(boolean greenOccupationFlag) {
		this.greenOccupationFlag = greenOccupationFlag;
	}

	public boolean isStemOccupationFlag() {
		return stemOccupationFlag;
	}

	public void setStemOccupationFlag(boolean stemOccupationFlag) {
		this.stemOccupationFlag = stemOccupationFlag;
	}
	
	public boolean isHotOccupationFlag() {
		return hotOccupationFlag;
	}

	public void setHotOccupationFlag(boolean hotOccupationFlag) {
		this.hotOccupationFlag = hotOccupationFlag;
	}
}
