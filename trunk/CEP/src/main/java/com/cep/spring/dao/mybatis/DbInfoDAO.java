package com.cep.spring.dao.mybatis;

import com.cep.spring.model.DbInfo;

public interface DbInfoDAO {
	
	DbInfo getDbInfo(String name);
	
}