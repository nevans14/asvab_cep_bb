package com.cep.spring.model.WorkValue;

import java.io.Serializable;

public class WorkValueUserProfile implements Serializable {

    private static final long serialVersionUID = -5913408889929303388L;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Byte getWorkValueOne() {
        return workValueOne;
    }

    public void setWorkValueOne(Byte workValueOne) {
        this.workValueOne = workValueOne;
    }

    public Byte getWorkValueTwo() {
        return workValueTwo;
    }

    public void setWorkValueTwo(Byte workValueTwo) {
        this.workValueTwo = workValueTwo;
    }

    public Byte getWorkValueThree() {
        return workValueThree;
    }

    public void setWorkValueThree(Byte workValueThree) {
        this.workValueThree = workValueThree;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    private Integer userId;
    private Byte workValueOne, workValueTwo, workValueThree;
    private String dateUpdated;

}
