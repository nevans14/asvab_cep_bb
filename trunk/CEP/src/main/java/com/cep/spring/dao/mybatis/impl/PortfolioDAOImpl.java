package com.cep.spring.dao.mybatis.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.PortfolioDAO;
import com.cep.spring.dao.mybatis.service.PortfolioService;
import com.cep.spring.model.portfolio.FavoriteOccupation;
import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

@Primary
@Repository
public class PortfolioDAOImpl implements PortfolioDAO {

	private final PortfolioService service;

	@Autowired
	public PortfolioDAOImpl(PortfolioService service) {
		this.service = service;
	}

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#isPortfolioStarted(java.lang.Integer)
	 */
	@Override
	public List<String> isPortfolioStarted(Integer userId) { return service.isPortfolioStarted(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getWorkExperience(java.lang.Integer)
	 */
	@Override
	public ArrayList<WorkExperience> getWorkExperience(Integer userId) { return service.getWorkExperience(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertWorkExperience(com.cep.spring.model.portfolio.WorkExperience)
	 */
	@Override
	public int insertWorkExperience(WorkExperience workExperienceObject, Integer userId) { return service.insertWorkExperience(workExperienceObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteWorkExperience(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteWorkExperience(Integer userId, Integer id) { return service.deleteWorkExperience(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateWorkExperience(com.cep.spring.model.portfolio.WorkExperience)
	 */
	@Override
	public int updateWorkExperience(WorkExperience workExperienceObject, Integer userId) { return service.updateWorkExperience(workExperienceObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getEducation(java.lang.Integer)
	 */
	@Override
	public ArrayList<Education> getEducation(Integer userId) { return service.getEducation(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertEducation(com.cep.spring.model.portfolio.Education)
	 */
	@Override
	public int insertEducation(Education educationObject, Integer userId) { return service.insertEducation(educationObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteEducation(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteEducation(Integer userId, Integer id) { return service.deleteEducation(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateEducation(com.cep.spring.model.portfolio.Education)
	 */
	@Override
	public int updateEducation(Education educationObject, Integer userId) { return service.updateEducation(educationObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getAchievements(java.lang.Integer)
	 */
	@Override
	public ArrayList<Achievement> getAchievements(Integer userId) { return service.getAchievements(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertAchievement(com.cep.spring.model.portfolio.Achievement)
	 */
	@Override
	public int insertAchievement(Achievement achievementObject, Integer userId) { return service.insertAchievement(achievementObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteAchievement(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteAchievement(Integer userId, Integer id) { return service.deleteAchievement(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateAchievement(com.cep.spring.model.portfolio.Achievement)
	 */
	@Override
	public int updateAchievement(Achievement achievementObject, Integer userId) { return service.updateAchievement(achievementObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getInterests(java.lang.Integer)
	 */
	@Override
	public ArrayList<Interest> getInterests(Integer userId) { return service.getInterests(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertInterest(com.cep.spring.model.portfolio.Interest)
	 */
	@Override
	public int insertInterest(Interest interestObject, Integer userId) { return service.insertInterest(interestObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteInterest(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteInterest(Integer userId, Integer id) { return service.deleteInterest(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateInterest(com.cep.spring.model.portfolio.Interest)
	 */
	@Override
	public int updateInterest(Interest interestObject, Integer userId) { return service.updateInterest(interestObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getSkills(java.lang.Integer)
	 */
	@Override
	public ArrayList<Skill> getSkills(Integer userId) { return service.getSkills(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertSkill(com.cep.spring.model.portfolio.Skill)
	 */
	@Override
	public int insertSkill(Skill skillObject, Integer userId) { return service.insertSkill(skillObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteSkill(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteSkill(Integer userId, Integer id) { return service.deleteSkill(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateSkill(com.cep.spring.model.portfolio.Skill)
	 */
	@Override
	public int updateSkill(Skill skillObject, Integer userId) { return service.updateSkill(skillObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getWorkValues(java.lang.Integer)
	 */
	@Override
	public ArrayList<WorkValue> getWorkValues(Integer userId) { return service.getWorkValues(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertWorkValue(com.cep.spring.model.portfolio.WorkValue)
	 */
	@Override
	public int insertWorkValue(WorkValue workValueObject, Integer userId) { return service.insertWorkValue(workValueObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteWorkValue(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteWorkValue(Integer userId, Integer id) { return service.deleteWorkValue(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateWorkValue(com.cep.spring.model.portfolio.WorkValue)
	 */
	@Override
	public int updateWorkValue(WorkValue workValueObject, Integer userId) { return service.updateWorkValue(workValueObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getVolunteers(java.lang.Integer)
	 */
	@Override
	public ArrayList<Volunteer> getVolunteers(Integer userId) { return service.getVolunteers(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertVolunteer(com.cep.spring.model.portfolio.Volunteer)
	 */
	@Override
	public int insertVolunteer(Volunteer volunteerObject, Integer userId) { return service.insertVolunteer(volunteerObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteVolunteer(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteVolunteer(Integer userId, Integer id) { return service.deleteVolunteer(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateVolunteer(com.cep.spring.model.portfolio.Volunteer)
	 */
	@Override
	public int updateVolunteer(Volunteer volunteerObject, Integer userId) { return service.updateVolunteer(volunteerObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getAsvabScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<AsvabScore> getAsvabScore(Integer userId) { return service.getAsvabScore(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertAsvabScore(com.cep.spring.model.portfolio.AsvabScore)
	 */
	@Override
	public int insertAsvabScore(AsvabScore asvabObject, Integer userId) { return service.insertAsvabScore(asvabObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteAsvabScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteAsvabScore(Integer userId, Integer id) { return service.deleteAsvabScore(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateAsvabScore(com.cep.spring.model.portfolio.AsvabScore)
	 */
	@Override
	public int updateAsvabScore(AsvabScore asvabObject, Integer userId) { return service.updateAsvabScore(asvabObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getActScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<ActScore> getActScore(Integer userId) { return service.getActScore(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertActScore(com.cep.spring.model.portfolio.ActScore)
	 */
	@Override
	public int insertActScore(ActScore actObject, Integer userId) { return service.insertActScore(actObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteActScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteActScore(Integer userId, Integer id) { return service.deleteActScore(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateActScore(com.cep.spring.model.portfolio.ActScore)
	 */
	@Override
	public int updateActScore(ActScore actObject, Integer userId) { return service.updateActScore(actObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getSatScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<SatScore> getSatScore(Integer userId) { return service.getSatScore(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertSatScore(com.cep.spring.model.portfolio.SatScore)
	 */
	@Override
	public int insertSatScore(SatScore satObject, Integer userId) { return service.insertSatScore(satObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteSatScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteSatScore(Integer userId, Integer id) { return service.deleteSatScore(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateSatScore(com.cep.spring.model.portfolio.SatScore)
	 */
	@Override
	public int updateSatScore(SatScore satObject, Integer userId) { return service.updateSatScore(satObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getFyiScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<FyiScore> getFyiScore(Integer userId) { return service.getFyiScore(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertFyiScore(com.cep.spring.model.portfolio.FyiScore)
	 */
	@Override
	public int insertFyiScore(FyiScore fyiObject, Integer userId) { return service.insertFyiScore(fyiObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteFyiScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteFyiScore(Integer userId, Integer id) { return service.deleteFyiScore(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateFyiScore(com.cep.spring.model.portfolio.FyiScore)
	 */
	@Override
	public int updateFyiScore(FyiScore fyiObject, Integer userId) { return service.updateFyiScore(fyiObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getOtherScore(java.lang.Integer)
	 */
	@Override
	public ArrayList<OtherScore> getOtherScore(Integer userId) { return service.getOtherScore(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertOtherScore(com.cep.spring.model.portfolio.OtherScore)
	 */
	@Override
	public int insertOtherScore(OtherScore otherObject, Integer userId) { return service.insertOtherScore(otherObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deleteOtherScore(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deleteOtherScore(Integer userId, Integer id) { return service.deleteOtherScore(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateOtherScore(com.cep.spring.model.portfolio.OtherScore)
	 */
	@Override
	public int updateOtherScore(OtherScore otherObject, Integer userId) { return service.updateOtherScore(otherObject, userId); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getPlan(java.lang.Integer)
	 */
	@Override
	public ArrayList<Plan> getPlan(Integer userId) { return service.getPlan(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertPlan(com.cep.spring.model.portfolio.Plan)
	 */
	@Override
	public int insertPlan(Plan planObject, Integer userId) { return service.insertPlan(planObject, userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#deletePlan(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public int deletePlan(Integer userId, Integer id) { return service.deletePlan(userId, id); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updatePlan(com.cep.spring.model.portfolio.Plan)
	 */
	@Override
	public int updatePlan(Plan planObject, Integer userId) { return service.updatePlan(planObject, userId); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#selectNameGrade(java.lang.Integer)
	 */
	@Override
	public ArrayList<UserInfo> selectNameGrade(Integer userId) { return service.selectNameGrade(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#updateInsertNameGrade(com.cep.spring.model.portfolio.UserInfo)
	 */
	@Override
	public int updateInsertNameGrade(UserInfo userInfo, Integer userId) { return service.updateInsertNameGrade(userInfo, userId); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getFavoriteOccupation(java.lang.Integer)
	 */
	@Override
	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) { return service.getFavoriteOccupation(userId); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getAccessCode(java.lang.Integer)
	 */
	@Override
	public String getAccessCode(Integer userId) { return service.getAccessCode(userId); }
	
	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#insertTeacherDemoTaken(java.lang.Object)
	 */
	@Override
	public int insertTeacherDemoTaken(Integer userId) { return service.insertTeacherDemoTaken(userId); }

	/* (non-Javadoc)
	 * @see com.cep.spring.dao.mybatis.impl.PortfolioDAO#getTeacherDemoTaken(java.lang.Integer)
	 */
	@Override
	public int getTeacherDemoTaken(Integer userId) { return service.getTeacherDemoTaken(userId); }
}
