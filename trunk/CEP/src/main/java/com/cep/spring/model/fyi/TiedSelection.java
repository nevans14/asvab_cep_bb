package com.cep.spring.model.fyi;

import java.io.Serializable;

public class TiedSelection implements Serializable {

	private static final long serialVersionUID = 619581336729072063L;
	String genderInterestOne, genderInterestTwo, genderInterestThree, combineInterestOne, combineInterestTwo,
			combineInterestThree;

	public String getGenderInterestOne() {
		return genderInterestOne;
	}

	public void setGenderInterestOne(String genderInterestOne) {
		this.genderInterestOne = genderInterestOne;
	}

	public String getGenderInterestTwo() {
		return genderInterestTwo;
	}

	public void setGenderInterestTwo(String genderInterestTwo) {
		this.genderInterestTwo = genderInterestTwo;
	}

	public String getGenderInterestThree() {
		return genderInterestThree;
	}

	public void setGenderInterestThree(String genderInterestThree) {
		this.genderInterestThree = genderInterestThree;
	}

	public String getCombineInterestOne() {
		return combineInterestOne;
	}

	public void setCombineInterestOne(String combineInterestOne) {
		this.combineInterestOne = combineInterestOne;
	}

	public String getCombineInterestTwo() {
		return combineInterestTwo;
	}

	public void setCombineInterestTwo(String combineInterestTwo) {
		this.combineInterestTwo = combineInterestTwo;
	}

	public String getCombineInterestThree() {
		return combineInterestThree;
	}

	public void setCombineInterestThree(String combineInterestThree) {
		this.combineInterestThree = combineInterestThree;
	}

}
