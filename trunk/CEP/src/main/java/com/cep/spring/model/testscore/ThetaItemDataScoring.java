package com.cep.spring.model.testscore;

public class ThetaItemDataScoring {

	private	int	gS_CompletionCd;
	private	int	gS_UnroundedTheta;
	private	int	gS_Iterations;
	private	int	gS_ActualItemResponse ;
	private	int	gS_DichotomousItemScoring ;
	private	int	aR_CompletionCd;
	private	int	aR_UnroundedTheta;
	private	int	aR_Iterations;
	private	int	aR_ActualItemResponse ;
	private	int	aR_DichotomousItemScoring ;
	private	int	wK_CompletionCd;
	private	int	wK_UnroundedTheta;
	private	int	wK_Iterations;
	private	int	wK_ActualItemResponse ;
	private	int	wK_DichotomousItemScoring ;
	private	int	pC_CompletionCd;
	private	int	pC_UnroundedTheta;
	private	int	pC_Iterations;
	private	int	pC_ActualItemResponse ;
	private	int	pC_DichotomousItemScoring ;
	private	int	mK_CompletionCd;
	private	int	mK_UnroundedTheta;
	private	int	mK_Iterations;
	private	int	mK_ActualItemResponse ;
	private	int	mK_DichotomousItemScoring;
	private	int	eI_CompletionCd;
	private	int	eI_UnroundedTheta;
	private	int	eI_Iterations;
	private	int	eI_ActualItemResponse;
	private	int	eI_DichotomousItemScoring;
	private	int	aS_CompletionCd;
	private	int	aS_UnroundedTheta;
	private	int	aS_Iterations;
	private	int	aS_ActualItemResponse;
	private	int	aS_DichotomousItemScoring;
	private	int	mC_CompletionCd;
	private	int	mC_UnroundedTheta;
	private	int	mC_Iterations;
	private	int	mC_ActualItemResponse;
	private	int	mC_DichotomousItemScoring;
	private	int	aO_CompletionCd;
	private	int	aO_UnroundedTheta;
	private	int	aO_Iterations;
	private	int	aO_ActualItemResponse;
	private	int	aO_DichotomousItemScoring;
	private	int	vE_CompletionCd;
	private	int	dataEntryType;
	/**
	 * @return the gS_CompletionCd
	 */
	public int getgS_CompletionCd() {
		return gS_CompletionCd;
	}
	/**
	 * @param gS_CompletionCd the gS_CompletionCd to set
	 */
	public void setgS_CompletionCd(int gS_CompletionCd) {
		this.gS_CompletionCd = gS_CompletionCd;
	}
	/**
	 * @return the gS_UnroundedTheta
	 */
	public int getgS_UnroundedTheta() {
		return gS_UnroundedTheta;
	}
	/**
	 * @param gS_UnroundedTheta the gS_UnroundedTheta to set
	 */
	public void setgS_UnroundedTheta(int gS_UnroundedTheta) {
		this.gS_UnroundedTheta = gS_UnroundedTheta;
	}
	/**
	 * @return the gS_Iterations
	 */
	public int getgS_Iterations() {
		return gS_Iterations;
	}
	/**
	 * @param gS_Iterations the gS_Iterations to set
	 */
	public void setgS_Iterations(int gS_Iterations) {
		this.gS_Iterations = gS_Iterations;
	}
	/**
	 * @return the gS_ActualItemResponse
	 */
	public int getgS_ActualItemResponse() {
		return gS_ActualItemResponse;
	}
	/**
	 * @param gS_ActualItemResponse the gS_ActualItemResponse to set
	 */
	public void setgS_ActualItemResponse(int gS_ActualItemResponse) {
		this.gS_ActualItemResponse = gS_ActualItemResponse;
	}
	/**
	 * @return the gS_DichotomousItemScoring
	 */
	public int getgS_DichotomousItemScoring() {
		return gS_DichotomousItemScoring;
	}
	/**
	 * @param gS_DichotomousItemScoring the gS_DichotomousItemScoring to set
	 */
	public void setgS_DichotomousItemScoring(int gS_DichotomousItemScoring) {
		this.gS_DichotomousItemScoring = gS_DichotomousItemScoring;
	}
	/**
	 * @return the aR_CompletionCd
	 */
	public int getaR_CompletionCd() {
		return aR_CompletionCd;
	}
	/**
	 * @param aR_CompletionCd the aR_CompletionCd to set
	 */
	public void setaR_CompletionCd(int aR_CompletionCd) {
		this.aR_CompletionCd = aR_CompletionCd;
	}
	/**
	 * @return the aR_UnroundedTheta
	 */
	public int getaR_UnroundedTheta() {
		return aR_UnroundedTheta;
	}
	/**
	 * @param aR_UnroundedTheta the aR_UnroundedTheta to set
	 */
	public void setaR_UnroundedTheta(int aR_UnroundedTheta) {
		this.aR_UnroundedTheta = aR_UnroundedTheta;
	}
	/**
	 * @return the aR_Iterations
	 */
	public int getaR_Iterations() {
		return aR_Iterations;
	}
	/**
	 * @param aR_Iterations the aR_Iterations to set
	 */
	public void setaR_Iterations(int aR_Iterations) {
		this.aR_Iterations = aR_Iterations;
	}
	/**
	 * @return the aR_ActualItemResponse
	 */
	public int getaR_ActualItemResponse() {
		return aR_ActualItemResponse;
	}
	/**
	 * @param aR_ActualItemResponse the aR_ActualItemResponse to set
	 */
	public void setaR_ActualItemResponse(int aR_ActualItemResponse) {
		this.aR_ActualItemResponse = aR_ActualItemResponse;
	}
	/**
	 * @return the aR_DichotomousItemScoring
	 */
	public int getaR_DichotomousItemScoring() {
		return aR_DichotomousItemScoring;
	}
	/**
	 * @param aR_DichotomousItemScoring the aR_DichotomousItemScoring to set
	 */
	public void setaR_DichotomousItemScoring(int aR_DichotomousItemScoring) {
		this.aR_DichotomousItemScoring = aR_DichotomousItemScoring;
	}
	/**
	 * @return the wK_CompletionCd
	 */
	public int getwK_CompletionCd() {
		return wK_CompletionCd;
	}
	/**
	 * @param wK_CompletionCd the wK_CompletionCd to set
	 */
	public void setwK_CompletionCd(int wK_CompletionCd) {
		this.wK_CompletionCd = wK_CompletionCd;
	}
	/**
	 * @return the wK_UnroundedTheta
	 */
	public int getwK_UnroundedTheta() {
		return wK_UnroundedTheta;
	}
	/**
	 * @param wK_UnroundedTheta the wK_UnroundedTheta to set
	 */
	public void setwK_UnroundedTheta(int wK_UnroundedTheta) {
		this.wK_UnroundedTheta = wK_UnroundedTheta;
	}
	/**
	 * @return the wK_Iterations
	 */
	public int getwK_Iterations() {
		return wK_Iterations;
	}
	/**
	 * @param wK_Iterations the wK_Iterations to set
	 */
	public void setwK_Iterations(int wK_Iterations) {
		this.wK_Iterations = wK_Iterations;
	}
	/**
	 * @return the wK_ActualItemResponse
	 */
	public int getwK_ActualItemResponse() {
		return wK_ActualItemResponse;
	}
	/**
	 * @param wK_ActualItemResponse the wK_ActualItemResponse to set
	 */
	public void setwK_ActualItemResponse(int wK_ActualItemResponse) {
		this.wK_ActualItemResponse = wK_ActualItemResponse;
	}
	/**
	 * @return the wK_DichotomousItemScoring
	 */
	public int getwK_DichotomousItemScoring() {
		return wK_DichotomousItemScoring;
	}
	/**
	 * @param wK_DichotomousItemScoring the wK_DichotomousItemScoring to set
	 */
	public void setwK_DichotomousItemScoring(int wK_DichotomousItemScoring) {
		this.wK_DichotomousItemScoring = wK_DichotomousItemScoring;
	}
	/**
	 * @return the pC_CompletionCd
	 */
	public int getpC_CompletionCd() {
		return pC_CompletionCd;
	}
	/**
	 * @param pC_CompletionCd the pC_CompletionCd to set
	 */
	public void setpC_CompletionCd(int pC_CompletionCd) {
		this.pC_CompletionCd = pC_CompletionCd;
	}
	/**
	 * @return the pC_UnroundedTheta
	 */
	public int getpC_UnroundedTheta() {
		return pC_UnroundedTheta;
	}
	/**
	 * @param pC_UnroundedTheta the pC_UnroundedTheta to set
	 */
	public void setpC_UnroundedTheta(int pC_UnroundedTheta) {
		this.pC_UnroundedTheta = pC_UnroundedTheta;
	}
	/**
	 * @return the pC_Iterations
	 */
	public int getpC_Iterations() {
		return pC_Iterations;
	}
	/**
	 * @param pC_Iterations the pC_Iterations to set
	 */
	public void setpC_Iterations(int pC_Iterations) {
		this.pC_Iterations = pC_Iterations;
	}
	/**
	 * @return the pC_ActualItemResponse
	 */
	public int getpC_ActualItemResponse() {
		return pC_ActualItemResponse;
	}
	/**
	 * @param pC_ActualItemResponse the pC_ActualItemResponse to set
	 */
	public void setpC_ActualItemResponse(int pC_ActualItemResponse) {
		this.pC_ActualItemResponse = pC_ActualItemResponse;
	}
	/**
	 * @return the pC_DichotomousItemScoring
	 */
	public int getpC_DichotomousItemScoring() {
		return pC_DichotomousItemScoring;
	}
	/**
	 * @param pC_DichotomousItemScoring the pC_DichotomousItemScoring to set
	 */
	public void setpC_DichotomousItemScoring(int pC_DichotomousItemScoring) {
		this.pC_DichotomousItemScoring = pC_DichotomousItemScoring;
	}
	/**
	 * @return the mK_CompletionCd
	 */
	public int getmK_CompletionCd() {
		return mK_CompletionCd;
	}
	/**
	 * @param mK_CompletionCd the mK_CompletionCd to set
	 */
	public void setmK_CompletionCd(int mK_CompletionCd) {
		this.mK_CompletionCd = mK_CompletionCd;
	}
	/**
	 * @return the mK_UnroundedTheta
	 */
	public int getmK_UnroundedTheta() {
		return mK_UnroundedTheta;
	}
	/**
	 * @param mK_UnroundedTheta the mK_UnroundedTheta to set
	 */
	public void setmK_UnroundedTheta(int mK_UnroundedTheta) {
		this.mK_UnroundedTheta = mK_UnroundedTheta;
	}
	/**
	 * @return the mK_Iterations
	 */
	public int getmK_Iterations() {
		return mK_Iterations;
	}
	/**
	 * @param mK_Iterations the mK_Iterations to set
	 */
	public void setmK_Iterations(int mK_Iterations) {
		this.mK_Iterations = mK_Iterations;
	}
	/**
	 * @return the mK_ActualItemResponse
	 */
	public int getmK_ActualItemResponse() {
		return mK_ActualItemResponse;
	}
	/**
	 * @param mK_ActualItemResponse the mK_ActualItemResponse to set
	 */
	public void setmK_ActualItemResponse(int mK_ActualItemResponse) {
		this.mK_ActualItemResponse = mK_ActualItemResponse;
	}
	/**
	 * @return the mK_DichotomousItemScoring
	 */
	public int getmK_DichotomousItemScoring() {
		return mK_DichotomousItemScoring;
	}
	/**
	 * @param mK_DichotomousItemScoring the mK_DichotomousItemScoring to set
	 */
	public void setmK_DichotomousItemScoring(int mK_DichotomousItemScoring) {
		this.mK_DichotomousItemScoring = mK_DichotomousItemScoring;
	}
	/**
	 * @return the eI_CompletionCd
	 */
	public int geteI_CompletionCd() {
		return eI_CompletionCd;
	}
	/**
	 * @param eI_CompletionCd the eI_CompletionCd to set
	 */
	public void seteI_CompletionCd(int eI_CompletionCd) {
		this.eI_CompletionCd = eI_CompletionCd;
	}
	/**
	 * @return the eI_UnroundedTheta
	 */
	public int geteI_UnroundedTheta() {
		return eI_UnroundedTheta;
	}
	/**
	 * @param eI_UnroundedTheta the eI_UnroundedTheta to set
	 */
	public void seteI_UnroundedTheta(int eI_UnroundedTheta) {
		this.eI_UnroundedTheta = eI_UnroundedTheta;
	}
	/**
	 * @return the eI_Iterations
	 */
	public int geteI_Iterations() {
		return eI_Iterations;
	}
	/**
	 * @param eI_Iterations the eI_Iterations to set
	 */
	public void seteI_Iterations(int eI_Iterations) {
		this.eI_Iterations = eI_Iterations;
	}
	/**
	 * @return the eI_ActualItemResponse
	 */
	public int geteI_ActualItemResponse() {
		return eI_ActualItemResponse;
	}
	/**
	 * @param eI_ActualItemResponse the eI_ActualItemResponse to set
	 */
	public void seteI_ActualItemResponse(int eI_ActualItemResponse) {
		this.eI_ActualItemResponse = eI_ActualItemResponse;
	}
	/**
	 * @return the eI_DichotomousItemScoring
	 */
	public int geteI_DichotomousItemScoring() {
		return eI_DichotomousItemScoring;
	}
	/**
	 * @param eI_DichotomousItemScoring the eI_DichotomousItemScoring to set
	 */
	public void seteI_DichotomousItemScoring(int eI_DichotomousItemScoring) {
		this.eI_DichotomousItemScoring = eI_DichotomousItemScoring;
	}
	/**
	 * @return the aS_CompletionCd
	 */
	public int getaS_CompletionCd() {
		return aS_CompletionCd;
	}
	/**
	 * @param aS_CompletionCd the aS_CompletionCd to set
	 */
	public void setaS_CompletionCd(int aS_CompletionCd) {
		this.aS_CompletionCd = aS_CompletionCd;
	}
	/**
	 * @return the aS_UnroundedTheta
	 */
	public int getaS_UnroundedTheta() {
		return aS_UnroundedTheta;
	}
	/**
	 * @param aS_UnroundedTheta the aS_UnroundedTheta to set
	 */
	public void setaS_UnroundedTheta(int aS_UnroundedTheta) {
		this.aS_UnroundedTheta = aS_UnroundedTheta;
	}
	/**
	 * @return the aS_Iterations
	 */
	public int getaS_Iterations() {
		return aS_Iterations;
	}
	/**
	 * @param aS_Iterations the aS_Iterations to set
	 */
	public void setaS_Iterations(int aS_Iterations) {
		this.aS_Iterations = aS_Iterations;
	}
	/**
	 * @return the aS_ActualItemResponse
	 */
	public int getaS_ActualItemResponse() {
		return aS_ActualItemResponse;
	}
	/**
	 * @param aS_ActualItemResponse the aS_ActualItemResponse to set
	 */
	public void setaS_ActualItemResponse(int aS_ActualItemResponse) {
		this.aS_ActualItemResponse = aS_ActualItemResponse;
	}
	/**
	 * @return the aS_DichotomousItemScoring
	 */
	public int getaS_DichotomousItemScoring() {
		return aS_DichotomousItemScoring;
	}
	/**
	 * @param aS_DichotomousItemScoring the aS_DichotomousItemScoring to set
	 */
	public void setaS_DichotomousItemScoring(int aS_DichotomousItemScoring) {
		this.aS_DichotomousItemScoring = aS_DichotomousItemScoring;
	}
	/**
	 * @return the mC_CompletionCd
	 */
	public int getmC_CompletionCd() {
		return mC_CompletionCd;
	}
	/**
	 * @param mC_CompletionCd the mC_CompletionCd to set
	 */
	public void setmC_CompletionCd(int mC_CompletionCd) {
		this.mC_CompletionCd = mC_CompletionCd;
	}
	/**
	 * @return the mC_UnroundedTheta
	 */
	public int getmC_UnroundedTheta() {
		return mC_UnroundedTheta;
	}
	/**
	 * @param mC_UnroundedTheta the mC_UnroundedTheta to set
	 */
	public void setmC_UnroundedTheta(int mC_UnroundedTheta) {
		this.mC_UnroundedTheta = mC_UnroundedTheta;
	}
	/**
	 * @return the mC_Iterations
	 */
	public int getmC_Iterations() {
		return mC_Iterations;
	}
	/**
	 * @param mC_Iterations the mC_Iterations to set
	 */
	public void setmC_Iterations(int mC_Iterations) {
		this.mC_Iterations = mC_Iterations;
	}
	/**
	 * @return the mC_ActualItemResponse
	 */
	public int getmC_ActualItemResponse() {
		return mC_ActualItemResponse;
	}
	/**
	 * @param mC_ActualItemResponse the mC_ActualItemResponse to set
	 */
	public void setmC_ActualItemResponse(int mC_ActualItemResponse) {
		this.mC_ActualItemResponse = mC_ActualItemResponse;
	}
	/**
	 * @return the mC_DichotomousItemScoring
	 */
	public int getmC_DichotomousItemScoring() {
		return mC_DichotomousItemScoring;
	}
	/**
	 * @param mC_DichotomousItemScoring the mC_DichotomousItemScoring to set
	 */
	public void setmC_DichotomousItemScoring(int mC_DichotomousItemScoring) {
		this.mC_DichotomousItemScoring = mC_DichotomousItemScoring;
	}
	/**
	 * @return the aO_CompletionCd
	 */
	public int getaO_CompletionCd() {
		return aO_CompletionCd;
	}
	/**
	 * @param aO_CompletionCd the aO_CompletionCd to set
	 */
	public void setaO_CompletionCd(int aO_CompletionCd) {
		this.aO_CompletionCd = aO_CompletionCd;
	}
	/**
	 * @return the aO_UnroundedTheta
	 */
	public int getaO_UnroundedTheta() {
		return aO_UnroundedTheta;
	}
	/**
	 * @param aO_UnroundedTheta the aO_UnroundedTheta to set
	 */
	public void setaO_UnroundedTheta(int aO_UnroundedTheta) {
		this.aO_UnroundedTheta = aO_UnroundedTheta;
	}
	/**
	 * @return the aO_Iterations
	 */
	public int getaO_Iterations() {
		return aO_Iterations;
	}
	/**
	 * @param aO_Iterations the aO_Iterations to set
	 */
	public void setaO_Iterations(int aO_Iterations) {
		this.aO_Iterations = aO_Iterations;
	}
	/**
	 * @return the aO_ActualItemResponse
	 */
	public int getaO_ActualItemResponse() {
		return aO_ActualItemResponse;
	}
	/**
	 * @param aO_ActualItemResponse the aO_ActualItemResponse to set
	 */
	public void setaO_ActualItemResponse(int aO_ActualItemResponse) {
		this.aO_ActualItemResponse = aO_ActualItemResponse;
	}
	/**
	 * @return the aO_DichotomousItemScoring
	 */
	public int getaO_DichotomousItemScoring() {
		return aO_DichotomousItemScoring;
	}
	/**
	 * @param aO_DichotomousItemScoring the aO_DichotomousItemScoring to set
	 */
	public void setaO_DichotomousItemScoring(int aO_DichotomousItemScoring) {
		this.aO_DichotomousItemScoring = aO_DichotomousItemScoring;
	}
	/**
	 * @return the vE_CompletionCd
	 */
	public int getvE_CompletionCd() {
		return vE_CompletionCd;
	}
	/**
	 * @param vE_CompletionCd the vE_CompletionCd to set
	 */
	public void setvE_CompletionCd(int vE_CompletionCd) {
		this.vE_CompletionCd = vE_CompletionCd;
	}
	/**
	 * @return the dataEntryType
	 */
	public int getDataEntryType() {
		return dataEntryType;
	}
	/**
	 * @param dataEntryType the dataEntryType to set
	 */
	public void setDataEntryType(int dataEntryType) {
		this.dataEntryType = dataEntryType;
	}
}