package com.cep.spring.dao.mybatis.impl;

import com.cep.spring.dao.mybatis.UserDAO;
import com.cep.spring.dao.mybatis.service.UserService;
import com.cep.spring.model.user.UserCompletion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    private final UserService service;

    @Autowired
    public UserDAOImpl(UserService service) {
        this.service = service;
    }

    /**
     * Inserts or updates a record to keep track what activity that the user has completed
     * @param userId the logged in user ID.
     * @param model an encapsulated object that contains data about the completion type
     * @return how many records were affected in the database
     */
    public int updateUserCompletion(int userId, UserCompletion model) {
        return service.updateUserCompletion(userId, model);
    }

    /**
     * Deletes an existing activity completion record
     * @param userId the logged in user ID.
     * @param completionType the completion type
     * @return how many records were affected in the database
     */
    public int deleteUserCompletion(int userId, String completionType) {
        return service.deleteUserCompletion(userId, completionType);
    }

    /**
     * Returns the user completion record by type
     * @param userId the logged in user ID
     * @return a list of user completions
     */
    public List<UserCompletion> getUserCompletion(int userId) {
        return service.getUserCompletion(userId);
    }

}
