package com.cep.spring.mvc.controller; 

import javax.servlet.http.Cookie;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cep.spring.dao.mybatis.CareerPlanDAO;
import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.model.*;
import com.cep.spring.model.CareerPlan.CareerPlanAnswer;
import com.cep.spring.utils.*;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.MethodParameter;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.cep.spring.CEPMailer;
import com.cep.spring.dao.mybatis.FindYourInterestDAO;
import com.cep.spring.dao.mybatis.RegistrationDAO;
import com.cep.spring.model.fyi.FYIMerge;

import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.Math.toIntExact;

/**
 * Controller that allows for users to login, register, and reset credentials.
 *
 * @author Dave Springer
 *
 */
@Controller
@RequestMapping("/applicationAccess")
@PropertySource(value={"classpath:SsoProperties.properties", "classpath:application.properties"})
@Transactional(propagation=Propagation.REQUIRED) 
public class LoginRegistrationController implements HandlerMethodArgumentResolver {

	/***
	 * Status Enum to associate status codes and names
	 * @author jcidras
	 */
	enum Status
	{
		SUCCESS(0, "Success"),
		GENERIC_ERROR(997, "An error occurred. Please try again later."),
		GENERIC_ERROR_LOGIN(998, "An error occurred while logging in. Please try again later."),
		GENERIC_ERROR_REGISTER(999, "An error occurred while registering your account. Please try again later."),
		GENERIC_ERROR_ACCOUNT_ERROR(1000, "There is something wrong with your account. Please contact the site administrator."),
		ACCESS_CODE_NOT_FOUND(1001, "Access Code is not found."),
		EMAILADDRESS_NOT_REGISTERED(1002, "The provided Email Address is not registered."),
		USERNAME_ALREADY_TAKEN(1003, "This username is already taken."),
		USERNAME_NOT_VALID(1004, "Please provide a valid Email Address."),
		RESET_REQUEST_NOT_MADE(1005, "A reset request has not been made."),
		INVALID_RESET_KEY(1006, "An invalid reset request key was detected. Please check your email for a more recent reset invitation."),
		RESET_KEY_EXPIRED(1007, "Reset request has expired."),
		INVALID_CREDENTIALS(1008, "Invalid Username or Password."),
		NO_ROLE_ASSIGNED(1009, "A role was not assigned to this user."),
		ACCOUNT_IS_LOCKED(1010, "Account has been locked."),
		INVALID_LOGIN_METHOD(1011, "This Access Code was previously registered. Use email & password to login."),
		ACCESS_CODE_EXPIRED(1021, "Access Code has expired."),
		INVALID_ACCESS_CODE(1024, "Access Code is invalid."),
		ACCESS_CODE_UNREGISTRABLE(1025, "This Access Code is not able to be registered."),
		REGISTRATION_INCOMPLETE(1023, "Registration incomplete, please register this email again."),
		REGISTRATION_COMPLETED(1027, "Registration is already completed. Use credentials to login."),
		REGISTRATION_COMPLETED_WITH_DIFFERENT_PASSWORD(1008, "Registration was completed with a different password. Try resetting it."),
		REGISTRATION_COMPLETED_WITH_DIFFERENT_USERNAME(1029, "Registration was completed with a different email address."),
		INVALID_RECAPTCHA(1030, "Invalid recaptcha response."),
		FAILED_TO_UPDATE_ACCOUNT(1031, "Failed to update account. Please try again later."),
		FAILED_TO_CREATE_ACCOUNT(1031, "Failed to create account. Please try again later.");
		// fields
		private final int statusCode;
		private final String statusName;
		// constructor
		Status(int statusCode, String statusName) {
			this.statusCode = statusCode;
			this.statusName = statusName;
		}
		// accessors
		public int getStatusCode() { return this.statusCode; }
		public String getStatusName() { return this.statusName;	}
	}

	/***
	 * Security Role for Spring
	 * @author jcidras
	 */
	enum SecurityRole {
		USER("ROLE_USER"),
		ADMIN("ROLE_ADMIN");
		// fields
		private final String roleName;
		// constructor
		SecurityRole(String roleName) {this.roleName = roleName;}
		// accessors
		public String getRoleName() { return this.roleName; }
	}

	/***
	 * Roles Enum to associate role name to code
	 * @author jcidras
	 */
	enum Role {
		STUDENT("S"),
		TEACHER("E"),
		ADMIN("A"),
		PTI("P"),
		DIRECT_MAIL("D"); // aka, parents
		// fields
		private final String roleName;
		// constructor
		Role(String roleName) {
			this.roleName = roleName;
		}
		// accessors
		public String getRoleName() { return this.roleName; }
	}

	private final Logger logger = LogManager.getLogger();
	@Value("${cep.domain}")
	private String domain;
	@Value("${security.cookie.secure}")
	private boolean isSecure;

	private final JdbcTemplate jdbcTemplate;
	private final CEPMailer cepMailer;
	private final BCryptPasswordEncoder encoder;
	private final RegistrationDAO registrationDao;
	private final FYIRestController fyi;
	private final FindYourInterestDAO fyiDao;
	private final NotesController notes;
	private final PortfolioController portfolio;
	private final FavoriteOccupationController favorate;
	private final DbInfoDAO dbInfoDao;
	private final DbInfoUtils dbInfoUtils;
	private final CareerPlanDAO careerPlanDAO;
	private final VerifyRecaptcha verifyRecaptcha;

	@Autowired
    public LoginRegistrationController(JdbcTemplate jdbcTemplate, CEPMailer cepMailer, BCryptPasswordEncoder encoder, RegistrationDAO registrationDao, FYIRestController fyi, FindYourInterestDAO fyiDao, NotesController notes, PortfolioController portfolio, FavoriteOccupationController favorate, DbInfoDAO dbInfoDao, DbInfoUtils dbInfoUtils, CareerPlanDAO careerPlanDAO, VerifyRecaptcha verifyRecaptcha) {
	    this.jdbcTemplate = jdbcTemplate;
	    this.cepMailer = cepMailer;
	    this.encoder = encoder;
	    this.registrationDao = registrationDao;
	    this.fyi = fyi;
	    this.fyiDao = fyiDao;
	    this.notes = notes;
	    this.portfolio = portfolio;
	    this.favorate = favorate;
	    this.dbInfoDao = dbInfoDao;
	    this.dbInfoUtils = dbInfoUtils;
	    this.careerPlanDAO = careerPlanDAO;
	    this.verifyRecaptcha = verifyRecaptcha;
    }

	@RequestMapping(value = "/loginAccessCode", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus loginAccessCode(
			@RequestBody Registration registration,
			HttpServletRequest request,
			HttpServletResponse response) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		AccessCodes accessCode;
        List<GrantedAuthority> authorities;
	    // Determine if this Access Code was generated by CITM 
        String aCode = registration.getAccessCode().trim();
        if ( aCode.length() == 10 && aCode.endsWith("CITM")) {
            rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
            rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());         
            return rls;
        }
		// get access code
		try {
			accessCode = registrationDao.getAccessCodes(aCode, null);
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_LOGIN.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_LOGIN.getStatusName());
			return rls;
		}
		// determine if the access code exists
		if (accessCode != null) { // access code exists
			rls.setUser_id(String.valueOf(accessCode.getUserId()));
			rls.setRole(accessCode.getRole());
			rls.setAccessCode(accessCode.getAccessCode());
			// check if the access code has expired
			if (accessCode.getExpireDate() == null) {
				// update access to have 21 more days
				accessCode.setExpireDate(SimpleUtils.datePlusDays(new Date(), 21));
				try {
					registrationDao.updateAccessCode(accessCode);
				} catch (Exception e) {
					logger.error("Error", e);
				}
                // continue login
			} else if (0 < SimpleUtils.dateCompare(new Date(), accessCode.getExpireDate())) {
				rls.setStatusNumber(Status.ACCESS_CODE_EXPIRED.getStatusCode());
				rls.setStatus(Status.ACCESS_CODE_EXPIRED.getStatusName());
				return rls;	
			}
			DbInfo denyLoginAfterRegistration;
			Users user;
			try {
				denyLoginAfterRegistration = dbInfoDao.getDbInfo("deny_login_after_registration");
				user = registrationDao.getUser(accessCode.getUserId(), null);
			} catch (Exception e) {
				logger.error("Error", e);
				rls.setStatusNumber(Status.GENERIC_ERROR_LOGIN.getStatusCode());
				rls.setStatus(Status.GENERIC_ERROR_LOGIN.getStatusName());
				return rls;
			}
			if (user != null &&
					(denyLoginAfterRegistration != null &&
					denyLoginAfterRegistration.getValue() != null &&
					denyLoginAfterRegistration.getValue().toLowerCase(Locale.ENGLISH).trim().equals("true"))) {
				// unable to continue, must login via user credentials
				rls.setStatusNumber(Status.INVALID_LOGIN_METHOD.getStatusCode());
				rls.setStatus(Status.INVALID_LOGIN_METHOD.getStatusName());
				return rls;
			}
			// set the email from the user from the db
			if (user != null) {
			    registration.setEmail(user.getUsername());
            }
			// get/set MEPS
			getAndSetMeps(rls, registration.getAccessCode());
			// get security role based on role
			SecurityRole role = getSecurityRole(accessCode.getRole());
			// check role if admin, need to add a new role for USER
			authorities = getAuthoritiesByRole(accessCode.getRole());
			// set spring security context
            setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, role.getRoleName(), accessCode.getRole(), accessCode.getUserId(), user != null, request, response);
            // log login
			try {
				registrationDao.insertAsvabLog(toIntExact(accessCode.getUserId()));
			} catch (Exception e) {
				logger.error("Error", e);
			}
            // set status
            rls.setStatusNumber(Status.SUCCESS.getStatusCode());
            rls.setStatus(Status.SUCCESS.getStatusName());
		} else if (isValidStudentCode(registration.getAccessCode())) { // access code does not exist, but is valid
			// get user id
			long userId = createStudentAccessCode(registration.getAccessCode());
			if (userId == 0) {
				rls.setStatus(Status.GENERIC_ERROR_LOGIN.getStatusName());
				rls.setStatusNumber(Status.GENERIC_ERROR_LOGIN.getStatusCode());
				return rls;
			}
			// get/set MEPS
			getAndSetMeps(rls, registration.getAccessCode());
			// get authorities, default role
			authorities = getAuthorities(SecurityRole.USER.getRoleName());
			// set spring security context
			setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, SecurityRole.USER.getRoleName(), Role.STUDENT.getRoleName(), userId, false, request, response);
			// insert login record
			try {
				registrationDao.insertAsvabLog(toIntExact(userId));
			} catch (Exception e) {
				logger.error("Error", e);
			}
			// set status
			rls.setUser_id(String.valueOf(userId));
			rls.setRole(Role.STUDENT.getRoleName());
			rls.setAccessCode(registration.getAccessCode());
			rls.setStatusNumber(Status.SUCCESS.getStatusCode());
            rls.setStatus(Status.SUCCESS.getStatusName());
		} else { // access code does not exist and is not valid
			rls.setStatusNumber(Status.ACCESS_CODE_NOT_FOUND.getStatusCode());
			rls.setStatus(Status.ACCESS_CODE_NOT_FOUND.getStatusName());
		}
		// return status
		return rls;
	}
	
	@RequestMapping(value = "/loginEmail", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus loginEmail(
			@RequestBody Registration registration,
			HttpServletRequest request,
			HttpServletResponse response) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		Users user;
        // get user info
		try {
			user = registrationDao.getByUsername(registration.getEmail());
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_LOGIN.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_LOGIN.getStatusName());
			return rls;
		}
		// invalid username OR if credentials do not match, display error
		if (user == null || (!encoder.matches(registration.getPassword(), user.getPassword()))) {
			rls.setStatusNumber(Status.INVALID_CREDENTIALS.getStatusCode());
			rls.setStatus(Status.INVALID_CREDENTIALS.getStatusName());
			return rls;
		}
		// check if the user id is 0 (incomplete registration)
		if (user.getUserId().equals(0)) {
			rls.setStatusNumber(Status.REGISTRATION_INCOMPLETE.getStatusCode());
			rls.setStatus(Status.REGISTRATION_INCOMPLETE.getStatusName());
			return rls;
		}
		// determine if the user account is associated with an access code
		if (user.getAccessCodes() == null) {
			rls.setStatusNumber(Status.GENERIC_ERROR_ACCOUNT_ERROR.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_ACCOUNT_ERROR.getStatusName());
			return rls;
		}
        // Determine if this Access Code was generated by CITM 
        String aCode = user.getAccessCodes().getAccessCode().trim();
        if (aCode.length() == 10 && aCode.endsWith("CITM")){
            rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
            rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());         
            return rls;
        }		
		// test to see if the user is locked out
		if (user.getEnabled().equals((short)0)) {
			rls.setStatusNumber(Status.ACCOUNT_IS_LOCKED.getStatusCode());
			rls.setStatus(Status.ACCOUNT_IS_LOCKED.getStatusName()); 		
			return rls;
		}
		// has the access code expired
		if (0 < SimpleUtils.dateCompare(new Date(), user.getAccessCodes().getExpireDate())) {
			rls.setStatusNumber(Status.ACCESS_CODE_EXPIRED.getStatusCode());
			rls.setStatus(Status.ACCESS_CODE_EXPIRED.getStatusName());
			return rls;
		}
		// Get Roles		
		if (user.getUserRoles().isEmpty()) {
			rls.setStatusNumber(Status.NO_ROLE_ASSIGNED.getStatusCode());
			rls.setStatus(Status.NO_ROLE_ASSIGNED.getStatusName()); 	
			return rls;
		}
		// create authority list
		List<GrantedAuthority> authorities = getAuthorities(user.getUserRoles());
		// get security role
		SecurityRole role = getSecurityRole(user.getAccessCodes().getRole());
		// set security session info
		setSessionInfo(user.getUsername(), user.getPassword(), user.getAccessCodes().getAccessCode(), authorities, role.getRoleName(), user.getAccessCodes().getRole(), (long)user.getUserId(), true, request, response);
		// get user MEPS
		getAndSetMeps(rls, user.getAccessCodes().getAccessCode());
		// set status info
		rls.setUser_id(String.valueOf(user.getUserId()));
		rls.setRole(user.getAccessCodes().getRole());
		rls.setAccessCode(user.getAccessCodes().getAccessCode());
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus(Status.SUCCESS.getStatusName());
		// save login info
		try {
			registrationDao.insertAsvabLog(user.getUserId());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		// return status
		return rls ;		
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus register(
			@RequestBody Registration registration,
			HttpServletRequest request,
			HttpServletResponse response) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		if (!isRecaptchaVerified(registration, rls)) {
			return rls;
		}
		List<GrantedAuthority> authorities;
		String rawPwd = registration.getPassword();
		registration.setPassword(encoder.encode(registration.getPassword()));		
		// Determine if this Access Code was generated by CITM 
        String aCode = registration.getAccessCode().trim();
        if ( aCode.length() == 10 && aCode.endsWith("CITM") ){
            // This access code is for CITM use only.            
            rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
            rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());         
            return rls;
        }
        // Determine if this Access Code cannot be registered
        if (dbInfoUtils.isAccessCodeUnregistrable(registration.getAccessCode())) {
        	// This access code cannot be registered
			rls.setStatusNumber(Status.ACCESS_CODE_UNREGISTRABLE.getStatusCode());
			rls.setStatus(Status.ACCESS_CODE_UNREGISTRABLE.getStatusName());
			return rls;
		}
        // Determine if the registering username is valid
		EmailValidator validator = EmailValidator.getInstance();
        if (!validator.isValid(registration.getEmail())) {
			rls.setStatusNumber(Status.USERNAME_NOT_VALID.getStatusCode());
			rls.setStatus(Status.USERNAME_NOT_VALID.getStatusName());
			return rls;
		}
		// User Id
		Long userId;
        AccessCodes accessCode;
		// get access code
		try {
			accessCode = registrationDao.getAccessCodes(aCode, null);
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
        // check if the access code exists
		if (accessCode != null) {
			userId = accessCode.getUserId();
			rls.setRole(accessCode.getRole());
			// check dates
            if (accessCode.getExpireDate() == null) { // null, push expire date to 21 days ahead
                accessCode.setExpireDate(SimpleUtils.datePlusDays(new Date(), 21));
                try {
					registrationDao.updateAccessCode(accessCode);
				} catch (Exception e) {
					logger.error("Error", e);
					rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
					rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
					return rls;
				}
                // continue with registration
            } else if (0 < SimpleUtils.dateCompare(new Date(), accessCode.getExpireDate())) { // expire, return error message                
        	    rls.setStatusNumber(Status.ACCESS_CODE_EXPIRED.getStatusCode());
                rls.setStatus(Status.ACCESS_CODE_EXPIRED.getStatusName()); 
                return rls;            	                           
            }	
            // check if the access code has been registered to the inbound email
			Users user;
            try {
				user = registrationDao.getUser(userId, null);
			} catch (Exception e) {
            	logger.error("Error", e);
            	rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
            	rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
            	return rls;
			}
            if (user != null) { 
        		if (user.getUsername().equalsIgnoreCase(registration.getEmail())) {                    	
            		// return message to indicate the user should use their credentials to login
                	Status status = (encoder.matches(rawPwd, user.getPassword()) 
                			? Status.REGISTRATION_COMPLETED
        					: Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_PASSWORD);
                	rls.setStatusNumber(status.getStatusCode());
                	rls.setStatus(status.getStatusName());
        		} else {
        			// access code was registered with a different email address
        			rls.setStatusNumber(Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_USERNAME.getStatusCode());
        			rls.setStatus(Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_USERNAME.getStatusName());
        		}
        		// return status
            	return rls;
            }
            // continue with registration, assuming user ran into an issue in the registration process     
		} else if (isValidStudentCode(aCode)) {
			// create student access code and get the user ID
			userId = createStudentAccessCode(aCode);
			// set status
			rls.setRole(Role.STUDENT.getRoleName());
            rls.setStatusNumber(Status.SUCCESS.getStatusCode()); 
            rls.setStatus(Status.SUCCESS.getStatusName());
		} else {
			rls.setStatusNumber(Status.ACCESS_CODE_NOT_FOUND.getStatusCode());
			rls.setStatus(Status.ACCESS_CODE_NOT_FOUND.getStatusName());
			return rls;
		}
		// check to see if access code was created
		if (userId == 0) {
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			return rls;
		}
		rls.setUser_id(String.valueOf(userId));
		rls.setAccessCode(registration.getAccessCode());
		// get access code
		try {
			accessCode = registrationDao.getAccessCodes(null, toIntExact(userId));
		} catch (Exception e) {
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
		boolean updateUserRecord = false;
		Users existingUser;
		try {
			existingUser = registrationDao.getUser(null, registration.getEmail());
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
		// check if user already exists in system (e.g., from a previous administration)
		if (existingUser != null) {
			boolean doesntMatches;
			try {
				doesntMatches = jdbcTemplate.query("SELECT 1 FROM [dbo].[access_codes_merged] where [user_id] = ? and [new_access_code]  = ?", new ResultSetExtractor<Boolean>() {
							public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {return !rs.next();}
						}, existingUser.getUserId(), aCode);
			} catch (Exception e) {
				logger.error("Error", e);
				rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
				rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
				return rls;
			}
			String oldAccessCode;
			try {
				oldAccessCode = registrationDao.getAccessCodes(null, existingUser.getUserId()).getAccessCode();
			} catch (Exception e) {
				logger.error("Error", e);
				rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
				rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
				return rls;
			}
			// if the access code and user ID haven't been merged and if the old/new access codes don't match, check if the credentials match
		    if (doesntMatches && !aCode.equalsIgnoreCase(oldAccessCode))  {
		    	// if credentials don't match from registration, display error
				if (!encoder.matches(rawPwd, existingUser.getPassword())) {
					rls.setStatusNumber(Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_PASSWORD.getStatusCode());
					rls.setStatus(Status.REGISTRATION_COMPLETED_WITH_DIFFERENT_PASSWORD.getStatusName());
					return rls;
				}
				// Assume we have registration issue from before and update
				updateUserRecord = true;
		    } else {		    
    			// Error already have an account ...
    			rls.setStatusNumber(Status.USERNAME_ALREADY_TAKEN.getStatusCode());
    			rls.setStatus(Status.USERNAME_ALREADY_TAKEN.getStatusName());
    			return rls;
		    }
		} 		
		// determine if we need to update the record
		if (updateUserRecord) {
			// get authorities
			authorities = getAuthoritiesByRole(accessCode.getRole());
			// set spring security context
            setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, SecurityRole.USER.getRoleName(), accessCode.getRole(), userId, true, request, response);
			// set meps info
		    rls.setStatusNumber(Status.USERNAME_ALREADY_TAKEN.getStatusCode());
            rls.setStatus(Status.USERNAME_ALREADY_TAKEN.getStatusName());
            return rls;
		}
		// prepare new user and insert to database
		Users userToInsert = new Users(toIntExact(userId), registration.getEmail(), registration.getPassword(), (short)1, "CEP-CITM", registration.getStudentId());
		try {
			int rowsAffected = registrationDao.insertUser(userToInsert);
			if (rowsAffected == 0) {
				rls.setStatusNumber(Status.FAILED_TO_CREATE_ACCOUNT.getStatusCode());
				rls.setStatus(Status.FAILED_TO_CREATE_ACCOUNT.getStatusName());
				return rls;
			}
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
		// Insert new user role
		try {
			registrationDao.insertUserRole(registration.getEmail(), SecurityRole.USER.getRoleName());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		// check if the user wants to opts out of communications
		if (!registration.isOptedInCommunications()) {
			try {
				registrationDao.insertSubscription(registration.getEmail());
			} catch (Exception e) {
				logger.error("Error", e);
			}
		}
		// get authorities
		authorities = getAuthoritiesByRole(accessCode.getRole());
		// set spring security context ....
		setSessionInfo(registration.getEmail(), registration.getPassword(), registration.getAccessCode(), authorities, SecurityRole.USER.getRoleName(), accessCode.getRole(), userId, true, request, response);
		// so far success
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus(Status.SUCCESS.getStatusName());
		// record login
		try {
			registrationDao.insertAsvabLog(toIntExact(userId));
		} catch (Exception e) {
			logger.error("Error", e);
		}
		// get and set MEPs
		getAndSetMeps(rls, registration.getAccessCode());
		// Send registration email	      
        String userEmail = registration.getEmail();
		String websiteUrl, htmlEmailMessage;
		// get url and message from db
		try {
			websiteUrl = dbInfoDao.getDbInfo("website_url").getValue();
			htmlEmailMessage = dbInfoDao.getDbInfo("registration_email").getValue2();
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR.getStatusName());
			return rls;
		}
		htmlEmailMessage = htmlEmailMessage.replaceAll("#WEBSITE_URL#", websiteUrl);   
		htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");		
		try {
		    cepMailer.sendResetRegisterHtmlEmail(userEmail, "" , htmlEmailMessage, "Welcome to ASVAB CEP", null );
		} catch (Exception e) {
		    // Log and move on ....  This always fails locally 
		    logger.error("Error", e);
		}
		return rls;
	}
	
	@Transactional
	@RequestMapping(value = "/passwordResetRequest", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus  passwordResetRequest(@RequestBody Registration registration) {
		RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		Users user;
		// get user by username
		try {
			user = registrationDao.getUser(null, registration.getEmail());
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR.getStatusName());
			return rls;
		}
		// test if the user doesn't exist
		if (user == null) {
			rls.setStatusNumber(Status.EMAILADDRESS_NOT_REGISTERED.getStatusCode());
			rls.setStatus(Status.EMAILADDRESS_NOT_REGISTERED.getStatusName());
			return rls;
		}
		// create reset flag
		UUID resetFlag = UUID.randomUUID();
		// get username (email)
        String username = user.getUsername();		
		// calculate two days from now.		
		Date expireDate = SimpleUtils.datePlusDays(new Date(), 2);
		// update user model
		user.setEnabled((short)0);
		user.setResetKey(resetFlag.toString());
		user.setResetExpireDate(expireDate);
		// update user
		try {
			int rowsAffected = registrationDao.updateUser(user);
			if (rowsAffected == 0) {
				throw new Exception("Zero records updated: " + user.toString());
			}
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatus(Status.FAILED_TO_UPDATE_ACCOUNT.getStatusName());
			rls.setStatusNumber(Status.FAILED_TO_UPDATE_ACCOUNT.getStatusCode());
			return rls;
		}
		// Compose reset URL
		String resetUrl, htmlEmailMessage;
		try {
			resetUrl = dbInfoDao.getDbInfo("password_reset_url").getValue();
			htmlEmailMessage = dbInfoDao.getDbInfo("password_reset_email_message").getValue2();
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR.getStatusName());
			return rls;
		}
		// set reset URL
		resetUrl = resetUrl + resetFlag + "&username=" + username;
	    // compose email
        htmlEmailMessage = htmlEmailMessage.replace("#resetUrl#", resetUrl);
        htmlEmailMessage = htmlEmailMessage.replace("#userEmail#", username);      
        htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");
        // try to send email to user
        try {
            cepMailer.sendResetRegisterHtmlEmail(username, "" , htmlEmailMessage, "Password Reset ...", null );
        } catch (Exception e) {
            logger.error("Error", e);
        }
        // return status
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus("Email sent to complete password reset.");
		return rls;
	}

	@RequestMapping(value = "/passwordReset", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus passwordReset(@RequestBody Registration registration) {		
		RegistrationLoginStatus rls = new RegistrationLoginStatus();
		Users user;
		// Look up ResetKey
		try {
			user = registrationDao.getUser(null, registration.getEmail());
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR.getStatusName());
			return rls;
		}
		// test if the user doesn't exist
		if (user == null) {
			rls.setStatusNumber(Status.EMAILADDRESS_NOT_REGISTERED.getStatusCode());
			rls.setStatus(Status.EMAILADDRESS_NOT_REGISTERED.getStatusName());
			return rls;
		}
		// If the reset-key is null
		if (user.getResetKey() == null) {
			rls.setStatusNumber(Status.RESET_REQUEST_NOT_MADE.getStatusCode());
			rls.setState(Status.RESET_REQUEST_NOT_MADE.getStatusName());
			return rls;
		}
		// If the keys do not match
		if (!user.getResetKey().equals(registration.getResetKey())) {
			rls.setStatusNumber(Status.INVALID_RESET_KEY.getStatusCode());
			rls.setStatus(Status.INVALID_RESET_KEY.getStatusName());
			return rls;		
		}		
		// If now is after resetExpireDate error 
		if ( 0L <= SimpleUtils.dateCompare(new Date(), user.getResetExpireDate())) {
			rls.setStatusNumber(Status.RESET_KEY_EXPIRED.getStatusCode());
			rls.setStatus(Status.RESET_KEY_EXPIRED.getStatusName());
			return rls;		
		}
		registration.setPassword(encoder.encode(registration.getPassword()));
		// update user model
		user.setPassword(registration.getPassword());
		user.setResetKey(null);
		user.setResetExpireDate(null);
		user.setEnabled((short)1);
		// save changes
		try {
			int rowsAffected = registrationDao.updateUser(user);
			if (rowsAffected == 0) {
				throw new Exception("Zero records updated.");
			}
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatus(Status.FAILED_TO_UPDATE_ACCOUNT.getStatusName());
			rls.setStatusNumber(Status.FAILED_TO_UPDATE_ACCOUNT.getStatusCode());
		}
		// set and return status
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus(Status.SUCCESS.getStatusName());
		return rls;		
	}
	
	/*********************************************************************************
	 *       Teacher Access Code Feature                                             *
	 *********************************************************************************/
	@RequestMapping(value = "/testTeacherAccessCode", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus isThisAValidTeacherPromoAccessCode(@RequestBody Registration registration) {
		RegistrationLoginStatus rls =new RegistrationLoginStatus();
		try {
			boolean isValid =
					(
						dbInfoUtils.isPromoCode(registration.getAccessCode()) ||
					 	dbInfoUtils.isParentCode(registration.getAccessCode())
					);
			if (isValid) {
				rls.setStatusNumber(Status.SUCCESS.getStatusCode());
				rls.setStatus(Status.SUCCESS.getStatusName());
				return rls;
			}
		} catch (Exception e) {
			logger.error("Error", e);
		}
		rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
		rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());
		return rls;
	}

	@RequestMapping(value = "/get-function-list", method = RequestMethod.GET)
	public @ResponseBody List<RegistrationDropDown> getFunctionList() {
		List<RegistrationDropDown> results = new ArrayList<>();
		try {
			results = registrationDao.getFunctionList();
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}

	@RequestMapping(value = "/get-conference-list", method = RequestMethod.GET)
	public @ResponseBody List<RegistrationDropDown> getConferenceList() {
		List<RegistrationDropDown> results = new ArrayList<>();
		try {
			results = registrationDao.getConferenceList();
		} catch (Exception e) {
			logger.error("Error", e);
		}
		return results;
	}
	
	@Transactional
	@RequestMapping(value = "/teacherRegistration", method = RequestMethod.POST)
	public @ResponseBody RegistrationLoginStatus teacherRegistrationForPromo(@RequestBody Registration registration, HttpServletRequest request, HttpServletResponse response) {
		RegistrationLoginStatus rls =new RegistrationLoginStatus();
		if (!isRecaptchaVerified(registration, rls)) {
			return rls;
		}
		// gets promo code from form
		String promoCode = registration.getAccessCode().trim().toUpperCase(Locale.ENGLISH);
		// check to see if promo code is valid
		if (!dbInfoUtils.isPromoCode (promoCode) && !dbInfoUtils.isParentCode(promoCode)) {
			rls.setStatusNumber(Status.INVALID_ACCESS_CODE.getStatusCode());
			rls.setStatus(Status.INVALID_ACCESS_CODE.getStatusName());
			return rls;
		}
		EmailValidator validator = EmailValidator.getInstance();
		// determine if the email is valid
		if (!validator.isValid(registration.getEmail())) {
			rls.setStatusNumber(Status.USERNAME_NOT_VALID.getStatusCode());
			rls.setStatus(Status.USERNAME_NOT_VALID.getStatusName());
			return rls;
		}
		Users user;
		// check if the user has already registered
		try {
			user = registrationDao.getUser(null, registration.getEmail());
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
		if (user != null) {
			rls.setStatusNumber(Status.USERNAME_ALREADY_TAKEN.getStatusCode());
			rls.setStatus(Status.USERNAME_ALREADY_TAKEN.getStatusName());
			return rls;
		}
		// create a new teacher access code
		AccessCodes accessCodes = createPromoAccessCode(promoCode);
		// if the access code was not created, then return an error
		if (accessCodes == null) {
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
		registration.setPassword(encoder.encode(registration.getPassword()));
		// insert new user
		user = new Users(accessCodes.getUserId().intValue(),
				registration.getEmail(),
				registration.getPassword(),
				(short)1,
				"CEP-CITM", // able to login both CEP and CTM
				registration.getStudentId());
		try {
			int rowsAffected = registrationDao.insertUser(user);
			if (rowsAffected == 0)
			{
				rls.setStatusNumber(Status.FAILED_TO_CREATE_ACCOUNT.getStatusCode());
				rls.setStatus(Status.FAILED_TO_CREATE_ACCOUNT.getStatusName());
				return rls;
			}
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
		// check if the user wants to opts out of communications
		if (!registration.isOptedInCommunications()) {
			try {
				registrationDao.insertSubscription(registration.getEmail());
			} catch (Exception e) {
				logger.error("Error", e);
			}
		}
		// insert role for teacher
		try {
			registrationDao.insertUserRole(registration.getEmail(), SecurityRole.USER.getRoleName());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		// insert additional information
		try {
			registrationDao.insertCep123Registration(accessCodes.getUserId().intValue(), registration);
		} catch (Exception e) {
			logger.error("Error", e);
		}
		// get authorities
		List<GrantedAuthority> authorities = getAuthoritiesByRole(accessCodes.getRole());
        // set security session
        setSessionInfo(registration.getEmail(),
				registration.getPassword(),
				accessCodes.getAccessCode(),
				authorities,
				SecurityRole.USER.getRoleName(),
				accessCodes.getRole(),
				accessCodes.getUserId(),
				true,
				request,
				response);
        // insert asvab log
		try {
			registrationDao.insertAsvabLog(accessCodes.getUserId().intValue());
		} catch (Exception e) {
			logger.error("Error", e);
		}
		// prepare to send email
		String websiteUrl, htmlEmailMessage;
		try {
			websiteUrl = dbInfoDao.getDbInfo("website_url").getValue();
			htmlEmailMessage = dbInfoDao.getDbInfo("registration_email-teacher").getValue2();
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
        // Compose Registration email
        htmlEmailMessage = htmlEmailMessage.replaceAll("#WEBSITE_URL#", websiteUrl);   
        htmlEmailMessage = htmlEmailMessage.replaceAll("#NEW_LINE#", "\n");    
        try {
        	cepMailer.sendResetRegisterHtmlEmail(registration.getEmail(), "" , htmlEmailMessage, "Welcome to ASVAB CEP", null );
        } catch (Exception e) {
        	// Log and move on ....  This always fails locally 
        	logger.error("Error", e);
        }
		// set status
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus(Status.SUCCESS.getStatusName());
		rls.setRole(accessCodes.getRole());
		rls.setUser_id(String.valueOf(accessCodes.getUserId()));
		rls.setAccessCode(registration.getAccessCode());
		return rls;
	}

	@RequestMapping(value = "/merge/{emailAddress}/{newAccessCode}/{profileFlagString}/{careerPlanFlagString}/", method = RequestMethod.GET)
	public @ResponseBody RegistrationLoginStatus  MergeAccounts(
			@PathVariable String emailAddress,
			@PathVariable String newAccessCode,
			@PathVariable String profileFlagString,
			@PathVariable String careerPlanFlagString) {
        RegistrationLoginStatus rls =  new RegistrationLoginStatus();
		boolean profileFlag = false,
				careerPlanFlag = false;
		try {
			profileFlag = Boolean.parseBoolean(profileFlagString);
		} catch(Exception e) {
			rls.setStatusNumber(1011);
			rls.setStatus("fyi and/or profile flag is not a boolean");
            logger.error("fyi and/or profile flag is not a boolean", e);
		}
		try {
			careerPlanFlag = Boolean.parseBoolean(careerPlanFlagString);
		} catch (Exception e) {
			rls.setStatusNumber(1011);
			rls.setStatus("career plan flag is not a boolean");
			logger.error("career plan flag is not a boolean", e);
		}
		int oldUserId, newUserId;
		AccessCodes oldAccessCodeObj, newAccessCodeObj;
		// get old/new access code/user id from db
		try {
			oldUserId = registrationDao.getUserId(emailAddress).getUserId();
			oldAccessCodeObj = registrationDao.getAccessCodes(null,oldUserId);
			newAccessCodeObj = registrationDao.getAccessCodes(newAccessCode, null);
			newUserId = newAccessCodeObj.getUserId().intValue();
		} catch (Exception e) {
			logger.error("Error", e);
			rls.setStatusNumber(Status.GENERIC_ERROR_REGISTER.getStatusCode());
			rls.setStatus(Status.GENERIC_ERROR_REGISTER.getStatusName());
			return rls;
		}
		/*
		Boolean doesntMatches = jdbcTemplate.query("SELECT 1 FROM [dbo].[access_codes_merged] where [user_id] = ? and [new_access_code]  = ?", new ResultSetExtractor<Boolean>() {
	        public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {return rs.next() ? false: true;}
	    }, newAccessCodeObj.getUserId(), newAccessCodeObj.getAccessCode().trim());
		
        if(oldAccessCodeObj.getAccessCode().equalsIgnoreCase(newAccessCodeObj.getAccessCode()) || doesntMatches){
        	rls.setStatusNumber(0);
            rls.setStatus("Success");
            return rls;   
        }
		*/
		
		if(oldUserId != 0) {
			FYIMerge fyiObject = null;
			CareerPlanAnswer careerPlanAnswer = null;
			try {
				fyiObject = fyiDao.getLastTestObject(oldUserId, newUserId);
				careerPlanAnswer = careerPlanDAO.getResponses(oldUserId);
			} catch (Exception e) {
				logger.error("Error", e);
			}
			AccessCodesMerge registrationObject = new AccessCodesMerge();
			registrationObject.setUserId(((Integer)oldUserId).longValue());
			registrationObject.setOldAccessCode(oldAccessCodeObj.getAccessCode());
			registrationObject.setNewAccessCode(newAccessCode);
			if(fyiObject != null &&  fyiObject.getUserId() != newUserId)
				registrationObject.setFyiMovedToNewAccessCode("Y");
			else
				registrationObject.setFyiMovedToNewAccessCode("N");
			
			try {
				registrationDao.mergeAccounts(registrationObject);
				// if user's career plan is null, no need to merge nothing
				if (careerPlanFlag && careerPlanAnswer != null)
				{
					// set career plan to new user id
					careerPlanAnswer.setUserId(newUserId);
					careerPlanDAO.updateResponses(careerPlanAnswer);
				}
				fyi.mergeAccount(oldUserId, newUserId, fyiObject);
				notes.mergeNotes(oldUserId, newUserId);
				if(profileFlag) {
					portfolio.mergePortfolio(oldUserId, newUserId);
					favorate.mergeFavorates(oldUserId, newUserId);
				}
				registrationDao.changeAccessCodePointer(newUserId, emailAddress);
				registrationDao.disableAccessCode(registrationObject.getOldAccessCode());
				logger.error("Successfully Merged");
			} catch (DuplicateKeyException e) {
				rls.setStatusNumber(1012);
				rls.setStatus("Access Code already registered " + e);
	            logger.error("Access Code already registered", e);			
	            return rls;
			}
		}
		// get and set MEPS
		getAndSetMeps(rls, newAccessCode);
		// insert login record
		try {
			registrationDao.insertAsvabLog(toIntExact(newAccessCodeObj.getUserId()));
		} catch (Exception e) {
			logger.error("Error", e);
		}
        rls.setUser_id(String.valueOf(newAccessCodeObj.getUserId()));
        rls.setRole(newAccessCodeObj.getRole());
		rls.setStatusNumber(Status.SUCCESS.getStatusCode());
		rls.setStatus(Status.SUCCESS.getStatusName());
		return rls;		
	}

	/**
	 * Inner class for return payload.
	 * 
	 * 
	 * @author Dave Springer
	 *
	 */
	public static class RegistrationLoginStatus {

		private String status, role, user_id, accessCode, schoolCode, schoolName, city, state, zipCode, mepsId, mepsName;
		private Integer statusNumber = 0;
		
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public Integer getStatusNumber() {
			return statusNumber;
		}
		public void setStatusNumber(Integer statusNumber) {
			this.statusNumber = statusNumber;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public String getUser_id() {
			return user_id;
		}
		public void setUser_id(String user_id) {
			this.user_id = user_id;
		}
		public String getAccessCode() { return accessCode; }
		public void setAccessCode(String accessCode) { this.accessCode = accessCode; }
		/**
		 * @return the schoolCode
		 */
		public String getSchoolCode() {
			return schoolCode;
		}
		/**
		 * @param schoolCode the schoolCode to set
		 */
		public void setSchoolCode(String schoolCode) {
			this.schoolCode = schoolCode;
		}
		/**
		 * @return the schoolName
		 */
		public String getSchoolName() {
			return schoolName;
		}
		/**
		 * @param schoolName the schoolName to set
		 */
		public void setSchoolName(String schoolName) {
			this.schoolName = schoolName;
		}
		/**
		 * @return the city
		 */
		public String getCity() {
			return city;
		}
		/**
		 * @param city the city to set
		 */
		public void setCity(String city) {
			this.city = city;
		}
		/**
		 * @return the state
		 */
		public String getState() {
			return state;
		}
		/**
		 * @param state the state to set
		 */
		public void setState(String state) {
			this.state = state;
		}
		/**
		 * @return the zipCode
		 */
		public String getZipCode() {
			return zipCode;
		}
		/**
		 * @param zipCode the zipCode to set
		 */
		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}
		/**
		 * @return the mepsId
		 */
		public String getMepsId() {
			return mepsId;
		}
		/**
		 * @param mepsId the mepsId to set
		 */
		public void setMepsId(String mepsId) {
			this.mepsId = mepsId;
		}
		/**
		 * @return the mepsName
		 */
		public String getMepsName() {
			return mepsName;
		}
		/**
		 * @param mepsName the mepsName to set
		 */
		public void setMepsName(String mepsName) {
			this.mepsName = mepsName;
		}
	}
	
	/***
	 * Create a new student access code record
	 * @param accessCode the given access code upon registration
	 * @return user id
	 */
	private long createStudentAccessCode(String accessCode) {
		// get access code suffix
		String suffix = accessCode.substring(accessCode.length()-1);
		// get/set school year plus two years
		Integer schoolYear = GenerateAccessCodes.schoolYearOfSuffix(suffix);
		if (schoolYear == null) return 0;
		schoolYear += 2;
		// set expire date
	    String expireDate = String.valueOf(schoolYear).trim() + "-06-30 00:00:00.000";
	    // instantiate new access code object
	    AccessCodes newAccessCode = new AccessCodes(accessCode, SimpleUtils.getDateFromStringInStandardFormat(expireDate), "S");
	    // add new access code to db
		int rowsAffected = 0;
		try {
			// return user id
			rowsAffected = registrationDao.insertAccessCode(newAccessCode);
		} catch (Exception e) {
			logger.error("Error", e);
		}
	    return rowsAffected == 0 ? 0 : newAccessCode.getUserId();
	}
	
	/***
	 * Creates a new access code for a teacher
	 * @param accessCode the given access code upon registration
	 * @return user id
	 */
	private AccessCodes createPromoAccessCode(String accessCode) {
		// get last promo code used based on the given access code
		AccessCodes lastPromoAccessCodeUsed = null;
		try {
			lastPromoAccessCodeUsed = registrationDao.getLastPromoAccessCodeUsed(accessCode);
		} catch (Exception e) {
			logger.error("Error", e);
		}
        long accessCodeIdPrefix = (lastPromoAccessCodeUsed == null) ?
        	-1L :
        	Long.parseLong(lastPromoAccessCodeUsed.getAccessCode().substring(0, 6));
        // format new access code
        String formattedAccessCode = String.format(Locale.ENGLISH, "%06d%s", (++accessCodeIdPrefix), accessCode);
        // set school year
        Integer schoolYear = GenerateAccessCodes.determineSchoolYear();
        String expireDate = schoolYear.toString().trim() + "-06-30 00:00:00.000";
        // instantiate new access code
		String role = dbInfoUtils.isPromoCode(accessCode) ?
						Role.TEACHER.getRoleName() :
						Role.DIRECT_MAIL.getRoleName();
        AccessCodes teacherAccessCode = new AccessCodes(formattedAccessCode,
				SimpleUtils.getDateFromStringInStandardFormat(expireDate),
				role,
				(short)0);
        // insert new access code
		int rowsAffected = 0;
		try {
			rowsAffected = registrationDao.insertAccessCode(teacherAccessCode);
		} catch (Exception e) {
			logger.error("Error", e);
		}
        // return the user id
        return rowsAffected == 0 ? null : teacherAccessCode;
	}
	
	/**
	 * Sets user session with authorities
	 * @param username User's credential
	 * @param password User's credential
	 * @param authorities Authorities (user roles)
	 * @param securityRole Security role (ROLE_USER, ROLE_ADMIN)
	 * @param userRole ASVAB CEP Role (A, S, T, R, etc...)
	 * @param userId User ID
	 * @param request Server Request
	 */
	private void setSessionInfo(String username,
								String password,
								String accessCode,
								List<GrantedAuthority> authorities,
								String securityRole,
								String userRole,
								Long userId,
								boolean isRegistered,
								HttpServletRequest request,
								HttpServletResponse response) {
		final String GLOBALS = "globals";
		final String PATH = "/";
		final int MAX_AGE = -1;
		final boolean IS_HTTP_ONLY = false; // because angular needs to access it
		// create authenticationToken
        UsernamePasswordAuthenticationToken authenticationToken = 
                new UsernamePasswordAuthenticationToken(username, password, authorities);
        // authenticationToken.isAuthenticated();
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        logger.debug("User authenticated");
		// Create a new session and add the security context.
		HttpSession session = request.getSession(true);
		// user role (S, E, A, R, D, etc...)
		session.setAttribute("USER_ROLE", userRole);
		// access code
		session.setAttribute("USER_ACCESS_CODE", accessCode);
		// spring security
		session.setAttribute("USERNAME", username);
		session.setAttribute("ASVAB_ROLE", securityRole);
		session.setAttribute("ASVAB_USER_ID", userId.toString());
		session.setAttribute("IS_REGISTERED", isRegistered);
		AuthCookie authCookie = new AuthCookie();
		CurrentUser currentUser = new CurrentUser(userRole, authCookie.getAuthData(userId.intValue(), password, username) , isRegistered);
		authCookie.setCurrentUser(currentUser);
		try {
			Cookie cookie = CookieHelper.addCookie(
					GLOBALS,
					authCookie.getAuthCookieJson(),
					domain,
					PATH,
					MAX_AGE,
					isSecure,
					IS_HTTP_ONLY);
			response.addCookie(cookie);
		} catch (Exception ex) {
			logger.error("error: " + ex.getMessage());
		}
	}
	
	/***
	 * Returns a list of authorities
	 * @param userRoles a list of user roles
	 * @return a list of authorities for the user
	 */
	private List<GrantedAuthority> getAuthorities(List<UserRoles> userRoles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (UserRoles role : userRoles) {
			authorities.add(new SimpleGrantedAuthority(role.getRole()));
		}
		return authorities;
	}
	
	/***
	 * Returns a list of authorities
	 * @param roles a list of roles for a given user
	 * @return a list of authorities for the application
	 */
	private List<GrantedAuthority> getAuthorities(String... roles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

	/***
	 * Returns a list of authorities based on the Access Code role
	 * @param role the given role of the user
	 * @return a list of authorities for the application
	 */
	private List<GrantedAuthority> getAuthoritiesByRole(String role) {
		if (role.equals(SecurityRole.ADMIN.getRoleName())) {
			return getAuthorities(SecurityRole.ADMIN.getRoleName(), SecurityRole.USER.getRoleName());
		} else {
			return getAuthorities(SecurityRole.USER.getRoleName());
		}
	}
	
	/***
	 * Returns the security role based on the User's role
	 * @param role the given role of the user
	 * @return returns the security role based on the ASVAB Role
	 */
	private SecurityRole getSecurityRole(String role) {
		return role.equalsIgnoreCase("A")
				? SecurityRole.ADMIN
				: SecurityRole.USER;
	}
	
	/**
	 * Sets the user MEPS information for the return status
	 * @param rls the registration/login status model
	 * @param mepsInfo meps info of the given student
	 */
	private void setMepsInfo(RegistrationLoginStatus rls, Meps mepsInfo) {
		if (mepsInfo != null) {
			rls.setMepsId(mepsInfo.getMepsId());
			rls.setMepsName(mepsInfo.getMepsName());
			rls.setSchoolCode(mepsInfo.getSchoolCode());
			rls.setSchoolName(mepsInfo.getSchoolName());
			rls.setCity(mepsInfo.getCity());
			rls.setState(mepsInfo.getState());
			rls.setZipCode(mepsInfo.getZipCode());
		}
	}
	
	/**
	 *  Needed to implement HandlerMethodArgumentResolver interface.
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		// Implementation not needed for this use case.
		return false;
	}

	/**
	 *  Needed to implement HandlerMethodArgumentResolver interface.
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
		// Implementation not needed for this use case.
		return null;
	}

	/***
	 * Gets and Sets the MEPS (Military Entrance Processing Station) from the database based on Access Code
	 * @param rls status to be set
	 * @param accessCode access code that's provided upon login/registration
	 */
	private void getAndSetMeps(RegistrationLoginStatus rls, String accessCode) {
		try {
			Meps mepsInfo = registrationDao.getUsersMeps(accessCode);
			setMepsInfo(rls, mepsInfo);
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}

	/***
	 * Determines if the access code is a valid student access code
	 * @param accessCode the access code that was provided upon registration/login
	 * @return true/false
	 */
	private boolean isValidStudentCode(String accessCode) {
		try {
		    int stringLength = accessCode.length();
		    String suffix = accessCode.substring( accessCode.length() - 1) ;
		    if ( stringLength != 10) return false;
	        Integer schoolYear = GenerateAccessCodes.schoolYearOfSuffix(suffix);
		    return (schoolYear != null);
		} catch (StringIndexOutOfBoundsException e) {
			logger.error("Error", e);
			return false;
		}
	}

	/**
	 * Determines if the Google Recaptcha is valid
	 * @param model registration object
	 * @param rls registration response payload if an error has occurred
	 * @return boolean: is valid recaptcha or not
	 */
	private boolean isRecaptchaVerified(Registration model, RegistrationLoginStatus rls) {
		// verify recaptcha
		try {
			if (verifyRecaptcha.verify(model.getRecaptchaResponse())) {
				return true;
			} else {
				rls.setStatusNumber(Status.INVALID_RECAPTCHA.getStatusCode());
				rls.setStatus(Status.INVALID_RECAPTCHA.getStatusName());
			}
		} catch (Exception ex) {
			rls.setStatusNumber(Status.INVALID_RECAPTCHA.getStatusCode());
			rls.setStatus(Status.INVALID_RECAPTCHA.getStatusName());
		}
		return false;
	}
}