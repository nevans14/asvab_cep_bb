package com.cep.spring.model;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class ShareToCounselorForm implements Serializable {
	private static final long serialVersionUID = -1467977326576970352L;
	private String email, studentHeardUs, studentHeardUsOther, recaptcha;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = Jsoup.clean(email, Whitelist.basic());
	}
	
	public String getStudentHeardUs() {
		return studentHeardUs;
	}
	
	public void setStudentHeardUs(String studentHeardUs) {
		this.studentHeardUs = Jsoup.clean(studentHeardUs, Whitelist.basic());
	}
	
	public String getStudentHeardUsOther() {
		return studentHeardUsOther;
	}
	
	public void setStudentHeardUsOther(String studentHeardUsOther) {
		this.studentHeardUsOther = Jsoup.clean(studentHeardUsOther, Whitelist.basic());
	}
	
	public String getRecaptcha() {
		return recaptcha;
	}
	
	public void setRecaptcha (String recaptcha) {
		this.recaptcha = recaptcha;
	}
}