package com.cep.spring.model.EssTraining;

import java.io.Serializable;

public class EssTrainingLocation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4546683941366610216L;
	private int essTrainingId;
	private String city;
	private String state;
	private String zipCode;
	private String building;
	private String buildingAddress;
	
	public void setEssTrainingId(int essTrainingId) {
		this.essTrainingId = essTrainingId;
	}
	
	public int getEssTrainingId() {
		return this.essTrainingId;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCity() {
		return this.city;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getState() {
		return this.state;
	}
	
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String getZipCode() {
		return this.zipCode;
	}
	
	public void setBuilding (String building) {
		this.building = building;
	}
	
	public String getBuilding () {
		return this.building;
	}
	
	public void setBuildingAddress(String buildingAddress) {
		this.buildingAddress = buildingAddress;
	}
	
	public String getBuildingAddress() {
		return this.buildingAddress;
	}
}