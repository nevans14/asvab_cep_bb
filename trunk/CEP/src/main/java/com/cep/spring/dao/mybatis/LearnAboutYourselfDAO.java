package com.cep.spring.dao.mybatis;

import com.cep.spring.model.Dashboard.MoreAboutMe;

import java.util.List;

public interface LearnAboutYourselfDAO {

    List<MoreAboutMe> getMoreAboutMe(int userId);

    int insertMoreAboutMe(MoreAboutMe model);

    int updateMoreAboutMe(MoreAboutMe model);

    int deleteMoreAboutMe(int id, int userId);

}
