package com.cep.spring.dao.mybatis.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.cep.spring.dao.mybatis.HomeDAO;
import com.cep.spring.dao.mybatis.service.HomeService;
import com.cep.spring.model.HomepageImage;

@Primary
@Repository
public class HomeDAOImpl implements HomeDAO {

	private final HomeService service;

	@Autowired
	public HomeDAOImpl(HomeService service) {
		this.service = service;
	}
	
	public HomepageImage getCurrentHomepageImg() { return service.getCurrentHomepageImg(); }
}