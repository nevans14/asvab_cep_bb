package com.cep.spring.dao.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.HomeMapper;
import com.cep.spring.model.HomepageImage;

@Service
public class HomeService {

	private final HomeMapper mapper;

	@Autowired
	public HomeService(HomeMapper mapper) {
		this.mapper = mapper;
	}
	
	public HomepageImage getCurrentHomepageImg() {
		return mapper.getCurrentHomepageImg();
	}
}