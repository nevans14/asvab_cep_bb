package com.cep.spring.sso;

import java.util.ArrayList;
import java.util.List;

import com.cep.spring.utils.Security;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;

@Configuration
public class SsoConfig {

	private final RedisOperationsSessionRepository sessionRepository;
	private final Security security;

	@Autowired
    public SsoConfig(RedisOperationsSessionRepository sessionRepository, Security security) {
	    this.sessionRepository = sessionRepository;
	    this.security = security;
    }

	@Bean
	public SsoClientLoginCallbackFilter ssoClientLoginCallbackFilter() {
		return new SsoClientLoginCallbackFilter(sessionRepository, requestCache(), security);
	}

	@Bean
	public RequestCache requestCache() {
		return new CustomRequestCache();
	}
	
	// To resolve ${} in @Value
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	public static class CustomRequestCache extends HttpSessionRequestCache {

		@Override
		public void setRequestMatcher(RequestMatcher requestMatcher) {
			List<RequestMatcher> matchers = new ArrayList<>();
			super.setRequestMatcher(new OrRequestMatcher(matchers));
		}
	}
}
