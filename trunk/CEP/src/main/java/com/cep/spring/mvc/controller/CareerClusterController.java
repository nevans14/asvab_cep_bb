package com.cep.spring.mvc.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.dao.mybatis.CareerClusterDAO;
import com.cep.spring.model.CareerCluster;
import com.cep.spring.model.CareerClusterOccupation;
import com.cep.spring.model.OccupationEducationLevel;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.RelatedOccupation;
import com.cep.spring.model.SkillImportance;
import com.cep.spring.model.Task;
import com.cep.spring.model.occufind.OccufindSearch;

@Controller
@RequestMapping("/careerCluster")
public class CareerClusterController {

    private final Logger logger = LogManager.getLogger();
	private final CareerClusterDAO dao;

	@Autowired
	public CareerClusterController(CareerClusterDAO dao) {
		this.dao = dao;
	}

	@RequestMapping(value = "/careerClusterList", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<CareerCluster>> getCareerCluster() {
	    logger.info("getCareerCluster");
		List<CareerCluster> results;
		try {
			results = dao.getCareerCluster();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/careerClusterOccupationList/{ccId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<CareerClusterOccupation>> getCareerClusterOccupation(@PathVariable int ccId) {
	    logger.info("getCareerClusterOccupation");
		List<CareerClusterOccupation> results;
		try {
			results = dao.getCareerClusterOccupation(ccId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/occupationSkillImportance/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<SkillImportance> getSkillImportance(@PathVariable String onetSoc) {
	    logger.info("getSkillImportance");
		SkillImportance results;
		try {
			results = dao.getSkillImportance(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/careerClusterOccupationTaskList/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Task>> getTasks(@PathVariable String onetSoc) {
        logger.info("");
		List<Task> results;
		try {
			results = dao.getTasks(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/occupationInterestCodes/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<OccupationInterestCode> getOccupationInterestCodes(@PathVariable String onetSoc) {
	    logger.info("getOccupationInterestCodes");
		OccupationInterestCode results;
		try {
			results = dao.getOccupationInterestCodes(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/occupationEducationLevel/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccupationEducationLevel>> getEducationLevel(@PathVariable String onetSoc) {
	    logger.info("getEducationLevel");
		List<OccupationEducationLevel> results;
		try {
			results = dao.getEducationLevel(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/relatedOccupation/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<RelatedOccupation>> getRelatedOccupations(@PathVariable String onetSoc) {
	    logger.info("getRelatedOccupations");
		List<RelatedOccupation> results;
		try {
			results = dao.getRelatedOccupations(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/CareerClusterOccupations/{ccId}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> getCareerClusterOccupations(@PathVariable Integer ccId) {
		List<OccufindSearch> results;
		try {
			results = dao.getCareerClusterOccupations(ccId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (results.size() == 0) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(results, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/CareerClusterById/{ccId}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<CareerCluster> getCareerClusterById(@PathVariable Integer ccId) {

		CareerCluster results;
		try {
			results = dao.getCareerClusterById(ccId);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (results == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<>(results, HttpStatus.OK);
		}
	}
}
