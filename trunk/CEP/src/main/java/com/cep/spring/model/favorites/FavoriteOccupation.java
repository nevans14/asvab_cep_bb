package com.cep.spring.model.favorites;

import java.io.Serializable;

public class FavoriteOccupation implements Serializable {

	private static final long serialVersionUID = 8202604746765012670L;
	private Integer id, hearts;
	private String onetSocCd, title;

	public Integer getHearts() { return hearts; }

	public void setHearts(Integer hearts) {	this.hearts = hearts; }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getOnetSocCd() {
		return onetSocCd;
	}

	public void setOnetSocCd(String onetSocCd) {
		this.onetSocCd = onetSocCd;
	}

}
