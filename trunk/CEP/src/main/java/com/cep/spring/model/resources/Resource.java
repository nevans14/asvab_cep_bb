package com.cep.spring.model.resources;

import java.io.Serializable;

public class Resource implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2393052621611178098L;
	private Integer resourceId;
	private Short orderInTopic, resourceTopicId, resourceTypeId;
	private String resourceTitle, resourceThumbnailUri, resourceUri, spanishResourceUri;
	private ResourceType resourceType;
	// accessors
	public Integer getResourceId() { return resourceId; }
	public Short getOrderInTopic() { return orderInTopic; }
	public Short getResourceTopicId() { return resourceTopicId; }
	public Short getResourceTypeId() { return resourceTypeId; }
	public String getResourceTitle() { return resourceTitle; }
	public String getResourceThumbnailUri() { return resourceThumbnailUri; }
	public String getResourceUri() { return resourceUri; }
	public String getSpanishResourceUri() { return spanishResourceUri; }
	public ResourceType getResourceType() { return resourceType; }
	// mutators
	public void setResourceId(Integer resourceId) { this.resourceId = resourceId; }
	public void setOrderInTopic(Short orderInTopic) { this.orderInTopic = orderInTopic; }
	public void setResourceTopicId(Short resourceTopicId) { this.resourceTopicId = resourceTopicId; }
	public void setResourceTypeId(Short resourceTypeId) { this.resourceTypeId = resourceTypeId; }
	public void setResourceTitle(String resourceTitle) { this.resourceTitle = resourceTitle; }
	public void setResourceThumbnailUri(String resourceThumbnailUri) { this.resourceThumbnailUri = resourceThumbnailUri; }
	public void setResourceUri(String resourceUri) { this.resourceUri = resourceUri; }
	public void setSpanishResourceUri(String spanishResourceUri) { this.spanishResourceUri = spanishResourceUri; }
	public void setResourceType(ResourceType resourceType) { this.resourceType = resourceType; }
}