package com.cep.spring.dao.mybatis.impl;

import com.cep.spring.dao.mybatis.LearnAboutYourselfDAO;
import com.cep.spring.dao.mybatis.service.LearnAboutYourselfService;
import com.cep.spring.model.Dashboard.MoreAboutMe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LearnAboutYourselfDAOImpl implements LearnAboutYourselfDAO {

    private final LearnAboutYourselfService service;

    @Autowired
    public LearnAboutYourselfDAOImpl(LearnAboutYourselfService service) {
        this.service = service;
    }

    public List<MoreAboutMe> getMoreAboutMe(int userId) {
        return service.getMoreAboutMe(userId);
    }

    public int insertMoreAboutMe(MoreAboutMe model) {
        return service.insertMoreAboutMe(model);
    }

    public int updateMoreAboutMe(MoreAboutMe model) {
        return service.updateMoreAboutMe(model);
    }

    public int deleteMoreAboutMe(int id, int userId) {
        return service.deleteMoreAboutMe(id, userId);
    }
}
