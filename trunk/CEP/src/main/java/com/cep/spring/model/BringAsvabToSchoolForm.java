package com.cep.spring.model;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class BringAsvabToSchoolForm implements Serializable {

	private static final long serialVersionUID = -1467977326576970352L;
	String title, position, firstName, lastName, phone,  heardUs, heardUsOther, email, schoolName, schoolAddress, schoolCity, schoolCounty,
			schoolState, schoolZip, recaptcha;

	public String getRecaptcha() {
		return recaptcha;
	}

	public void setRecaptcha(String recaptcha) {
		this.recaptcha = recaptcha;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = Jsoup.clean(title, Whitelist.basic());
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = Jsoup.clean(position, Whitelist.basic());
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = Jsoup.clean(firstName, Whitelist.basic());
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = Jsoup.clean(lastName, Whitelist.basic());
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = Jsoup.clean(phone, Whitelist.basic());
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = Jsoup.clean(email, Whitelist.basic());
	}
	
	public String getHeardUs() {
		return heardUs;
	}
	
	public void setHeardUs(String heardUs) {
		this.heardUs = Jsoup.clean(heardUs, Whitelist.basic());
	}
	
	public String getHeardUsOther() {
		return heardUsOther;
	}
	
	public void setHeardUsOther(String heardUsOther) {
		this.heardUsOther = Jsoup.clean(heardUsOther, Whitelist.basic());
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = Jsoup.clean(schoolName, Whitelist.basic());
	}

	public String getSchoolAddress() {
		return schoolAddress;
	}

	public void setSchoolAddress(String schoolAddress) {
		this.schoolAddress = Jsoup.clean(schoolAddress, Whitelist.basic());
	}

	public String getSchoolCity() {
		return schoolCity;
	}

	public void setSchoolCity(String schoolCity) {
		this.schoolCity = Jsoup.clean(schoolCity, Whitelist.basic());
	}

	public String getSchoolCounty() {
		return schoolCounty;
	}

	public void setSchoolCounty(String schoolCounty) {
		this.schoolCounty = Jsoup.clean(schoolCounty, Whitelist.basic());
	}

	public String getSchoolState() {
		return schoolState;
	}

	public void setSchoolState(String schoolState) {
		this.schoolState = Jsoup.clean(schoolState, Whitelist.basic());
	}

	public String getSchoolZip() {
		return schoolZip;
	}

	public void setSchoolZip(String schoolZip) {
		this.schoolZip = Jsoup.clean(schoolZip, Whitelist.basic());
	}
}
