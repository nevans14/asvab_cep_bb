package com.cep.spring.model.EssTraining;

import java.io.Serializable;
import java.util.Date;

public class EssTraining implements Serializable {
	
	private static final long serialVersionUID = 4546683941366610216L;
	private int essTrainingId;
	private String essTrainingTitle;
	private String essTrainingDescription;
	private Date essStartDate;
	private Date essEndDate;
	private Date essCutOffDate;
	private int essMaxNumberOfParticipants;
	private int numberOfParticipants;
	private EssTrainingLocation location;
	
	public void setEssTrainingId(int essTrainingId) { this.essTrainingId = essTrainingId; }	
	public int getEssTrainingId() {	return this.essTrainingId; }
	
	public void setEssTrainingTitle(String essTrainingTitle) { this.essTrainingTitle = essTrainingTitle; }	
	public String getEssTrainingTitle() { return this.essTrainingTitle;	}
	
	public void setEssTrainingDescription(String essTrainingDescription) { this.essTrainingDescription = essTrainingDescription; }	
	public String getEssTrainingDescription() {	return this.essTrainingDescription;	}
	
	public void setEssStartDate(Date essStartDate) { this.essStartDate = essStartDate; }	
	public Date getEssStartDate() {	return this.essStartDate; }
	
	public void setEssEndDate(Date essEndDate) { this.essEndDate = essEndDate; }	
	public Date getEssEndDate() { return this.essEndDate; }
	
	public void setEssCutOffDate(Date essCutOffDate) { this.essCutOffDate = essCutOffDate; }	
	public Date getEssCutOffDate() { return this.essCutOffDate;	}
	
	public void setEssMaxNumberOfParticipants(int essMaxNumberOfParticipants) { this.essMaxNumberOfParticipants = essMaxNumberOfParticipants; }	
	public int getEssMaxNumberOfParticipants() { return this.essMaxNumberOfParticipants; }
	
	public void setNumberOfParticipants(int numberOfParticipants) { this.numberOfParticipants = numberOfParticipants; }
	public int getNumberOfParticipants() { return numberOfParticipants; }
	
	public void setLocation(EssTrainingLocation location) {	this.location = location; }	
	public EssTrainingLocation getLocation() { return this.location; }
}