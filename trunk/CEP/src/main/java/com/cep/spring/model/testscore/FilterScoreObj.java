package com.cep.spring.model.testscore;

public class FilterScoreObj {
	
		private String administeredYear = null;
		private String administeredMonth = null;
		private String lastName = null;
		private String gender = null;
		private String educationLevel = null;
		private String schoolCode = null;
		private String schoolAddress = null;
		private String schoolCity = null;
		private String schoolState = null;
		private String schoolZipCode = null;
		private String studentCity = null;
		private String studentState = null;
		private String studentZipCode = null;
		private String fileName;
		/**
		 * @return the administeredYear
		 */
		public String getAdministeredYear() {
			return administeredYear;
		}
		/**
		 * @param administeredYear the administeredYear to set
		 */
		public void setAdministeredYear(String administeredYear) {
			this.administeredYear = administeredYear;
		}
		/**
		 * @return the administeredMonth
		 */
		public String getAdministeredMonth() {
			return administeredMonth;
		}
		/**
		 * @param administeredMonth the administeredMonth to set
		 */
		public void setAdministeredMonth(String administeredMonth) {
			this.administeredMonth = administeredMonth;
		}
		/**
		 * @return the lastName
		 */
		public String getLastName() {
			return lastName ==null? null:"%"+lastName+"%";
		}
		/**
		 * @param lastName the lastName to set
		 */
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		/**
		 * @return the educationLevel
		 */
		public String getEducationLevel() {
			return educationLevel;
		}
		/**
		 * @param educationLevel the educationLevel to set
		 */
		public void setEducationLevel(String educationLevel) {
			this.educationLevel = educationLevel;
		}
		/**
		 * @return the schoolCode
		 */
		public String getSchoolCode() {
			return schoolCode;
		}
		/**
		 * @param schoolCode the schoolCode to set
		 */
		public void setSchoolCode(String schoolCode) {
			this.schoolCode = schoolCode;
		}
		/**
		 * @return the schoolAddress
		 */
		public String getSchoolAddress() {
			return  schoolAddress ==null? null:"%"+schoolAddress+"%";
		}
		/**
		 * @param schoolAddress the schoolAddress to set
		 */
		public void setSchoolAddress(String schoolAddress) {
			this.schoolAddress = schoolAddress;
		}
		public String getSchoolCity() {
			return schoolCity ==null? null:"%"+schoolCity+"%";
		}
		public void setSchoolCity(String schoolCity) {
			this.schoolCity = schoolCity;
		}
		/**
		 * @return the schoolState
		 */
		public String getSchoolState() {
			return schoolState ==null? null:"%"+schoolState+"%";
		}
		/**
		 * @param schoolState the schoolState to set
		 */
		public void setSchoolState(String schoolState) {
			this.schoolState = schoolState;
		}
		/**
		 * @return the schoolZipCode
		 */
		public String getSchoolZipCode() {
			return schoolZipCode;
		}
		/**
		 * @param schoolZipCode the schoolZipCode to set
		 */
		public void setSchoolZipCode(String schoolZipCode) {
			this.schoolZipCode = schoolZipCode;
		}
		/**
		 * @return the studentCity
		 */
		public String getStudentCity() {
			return studentCity ==null? null:"%"+studentCity+"%";
		}
		/**
		 * @param studentCity the studentCity to set
		 */
		public void setStudentCity(String studentCity) {
			this.studentCity = studentCity;
		}
		/**
		 * @return the studentState
		 */
		public String getStudentState() {
			return studentState ==null? null:"%"+studentState+"%";
		}
		/**
		 * @param studentState the studentState to set
		 */
		public void setStudentState(String studentState) {
			this.studentState = studentState;
		}
		/**
		 * @return the studentZipCode
		 */
		public String getStudentZipCode() {
			return studentZipCode;
		}
		/**
		 * @param studentZipCode the studentZipCode to set
		 */
		public void setStudentZipCode(String studentZipCode) {
			this.studentZipCode = studentZipCode;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
}
