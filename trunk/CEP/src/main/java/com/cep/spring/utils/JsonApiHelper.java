package com.cep.spring.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.UnknownServiceException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Map.Entry;

/***
 * <strong>JsonApiHelper</strong>
 * How to use:<br>
 * 1. Know the API REST URL, this will be the <code>baseUrl</code><br>
 * 2. Create a <code>HashMap<String,String></code> object to store parameters<br>
 * 3. Call the <code>addParameters</code> method, returns a formatted URL<br>
 * 4. Call the <code>sendRequest</code> method, returns a JSON Response String<br>
 * @author jcidras
 *
 */
@Component
public class JsonApiHelper {

	private final Logger logger = LogManager.getLogger();

	/***
	 * Builds a REST URL with the given base URL and parameters
	 * @param hasQuestionMark starts the query string with a question mark
	 * @param baseUrl base URL of the API
	 * @param params a set of key/value pair parameters
	 * @return Formatted URL (String)
	 */
	public String addParameters(boolean hasQuestionMark, String baseUrl, Map<String, String> params) {
		StringBuilder sb = new StringBuilder();
		sb.append(baseUrl);
		int index = 0;
		for (Entry<String, String> entry : params.entrySet()) {
			if (index == 0) {
				sb.append(hasQuestionMark ? "?" : "")
						.append(entry.getKey())
						.append("=")
						.append(entry.getValue());
			} else {
				sb.append("&")
						.append(entry.getKey())
						.append("=")
						.append(entry.getValue());
			}
			index++;
		}
		return sb.toString();
	}
	
	/***
	 * Sends a request with the given URL.
	 * @param connection the https connection to requested target
	 * @return Response (JSON String)
	 */
	public String sendRequest(HttpURLConnection connection) {
		Reader isr = null;
		BufferedReader br = null;
		try {
			isr = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
			br = new BufferedReader(isr);
			return readAll(br);
		} catch(UnknownServiceException e) {
			logger.error("UnknownService Error", e);
		} catch (IOException e) {
			logger.error("IO Error", e);
		} finally {
			SimpleFileUtils.safeClose(isr);
			SimpleFileUtils.safeClose(br);
		}
		return null;
	}
	
	/***
	 * Reads the JSON response into a String
	 * @param rd reader
	 * @return String
	 * @throws IOException possible I/O exception
	 */
	public String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
		
		return sb.toString();
	}
}