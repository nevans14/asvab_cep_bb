package com.cep.spring.dao.mybatis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.DbInfoMapper;
import com.cep.spring.model.DbInfo;

@Service
public class DbInfoService {

	private final DbInfoMapper mapper;

	@Autowired
	public DbInfoService(DbInfoMapper mapper) {
		this.mapper = mapper;
	}
	
	public DbInfo getDbInfo(String name) {
		return mapper.getDbInfo(name);
	}
}