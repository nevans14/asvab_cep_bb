package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.NotesMapper;
import com.cep.spring.model.notes.CareerClusterNotes;
import com.cep.spring.model.notes.Notes;
import com.cep.spring.model.notes.SchoolNotes;

@Service
public class NotesService {

	private final NotesMapper mapper;

	@Autowired
	public NotesService(NotesMapper mapper) {
		this.mapper = mapper;
	}

	public ArrayList<Notes> getNotes(Integer userId) {
		return mapper.getNotes(userId);
	}

	public int insertNote(Notes noteObject, Integer userId) {
		return mapper.insertNote(noteObject, userId);
	}

	public int updateNote(Notes noteObject, Integer userId) {
		return mapper.updateNote(noteObject, userId);
	}
	
	public int deleteNote(Integer userId, Integer noteId) {
		return mapper.deleteNote(userId, noteId);
	}

	public String getOccupationNotes(Integer userId, String socId) {
		return mapper.getOccupationNotes(userId, socId);
	}
	
	public ArrayList<CareerClusterNotes> getCareerClusterNotes(Integer userId) {
		return mapper.getCareerClusterNotes(userId);
	}

	public int insertCareerClusterNote(CareerClusterNotes noteObject, Integer userId) {
		return mapper.insertCareerClusterNote(noteObject, userId);
	}

	public int updateCareerClusterNote(CareerClusterNotes noteObject, Integer userId) {
		return mapper.updateCareerClusterNote(noteObject, userId);
	}
	
	public int deleteCareerClusterNote(Integer userId, Integer ccNoteId) {
		return mapper.deleteCareerClusterNote(userId, ccNoteId);
	}

	public String getCareerClusterNote(Integer userId, Integer ccId) {
		return mapper.getCareerClusterNote(userId, ccId);
	}
	
	public ArrayList<SchoolNotes> getSchoolNotes(Integer userId) {
		return mapper.getSchoolNotes(userId);
	}

	public int insertSchoolNote(SchoolNotes noteObject, Integer userId) {
		return mapper.insertSchoolNote(noteObject, userId);
	}

	public int updateSchoolNote(SchoolNotes noteObject, Integer userId) {
		return mapper.updateSchoolNote(noteObject, userId);
	}
	
	public int deleteSchoolNote(Integer userId, Integer schoolNoteId) {
		return mapper.deleteSchoolNote(userId, schoolNoteId);
	}

	public String getSchoolNote(Integer userId, Integer unitId) {
		return mapper.getSchoolNote(userId, unitId);
	}

}
