package com.cep.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cep.spring.dao.mybatis.DbInfoDAO;
import com.cep.spring.dao.mybatis.OccufindDAO;
import com.cep.spring.model.occufind.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cep.spring.model.MilitaryCareerResource;
import com.cep.spring.model.OOHResource;
import com.cep.spring.model.Occupation;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.StateSalary;
import com.cep.spring.model.StateSalaryEntry;

@Controller
@RequestMapping("/occufind")
public class OccufindRestController {

	private final Logger logger = LogManager.getLogger();
	private final OccufindDAO dao;
	private final DbInfoDAO dbInfoDao;

	@Autowired
	public OccufindRestController(OccufindDAO dao, DbInfoDAO dbInfoDao) {
		this.dao = dao;
		this.dbInfoDao = dbInfoDao;
	}

	@RequestMapping(value = "/getOccufindSearch", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> getOccufindSearch(@RequestBody OccufindSearchArguments userSearch) {
		logger.info("getOccufindSearch");
		// append wild card character to search string
		String userSearchString = userSearch.getUserSearchString();
		if (!StringUtils.isEmpty(userSearchString)) {
			userSearch.setUserSearchString("%" + userSearchString + "%");
		}
		List<OccufindSearch> results;
		try {
			results = dao.getOccufindSearch(userSearch);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/certificateExplanation/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<CertificateExplanation> getCertificateExplanation(@PathVariable String onetSoc) {
		logger.info("getCertificateExplanation");
		CertificateExplanation certificateExplanation;
		try {
			certificateExplanation = dao.getCertificateExplanation(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(certificateExplanation, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/alternateTitles/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<AlternateTitles>> getAlternateTitles(@PathVariable String onetSoc) {
		logger.info("getCertificateExplanation");
		List<AlternateTitles> alternateTitles;
		try {
			alternateTitles = dao.getAlternateTitles(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(alternateTitles, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/occupationTitleDescription/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<TitleDescription> getOccufindTitleDescription(@PathVariable String onetSoc) {
		logger.info("getOccufindTitleDescription");
		TitleDescription occufindResults;
		try {
			occufindResults = dao.getOccupationTitleDescription(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(occufindResults, HttpStatus.OK);
	}

	@RequestMapping(value = "/occupationASK/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Occupation> getOccufindASK(@PathVariable String onetSoc) {
		logger.info("getOccufindASK");
		List<Occupation> results;
		try {
			results = dao.getOccupationASK(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (results != null && results.size() > 0) {
			return new ResponseEntity<>(results.get(0), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/militaryCareerResource/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<MilitaryCareerResource>> getMilitaryCareerResources(@PathVariable String onetSoc) {
		logger.info("getMilitaryCareerResources");
		List<MilitaryCareerResource> results;
		try {
			results = dao.getMilitaryCareerResources(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/OOHResource/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OOHResource>> getOOHResources(@PathVariable String onetSoc) {
		logger.info("getOOHResources");
		List<OOHResource> results;
		try {
			results = dao.getOOHResources(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/StateSalary/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<StateSalary>> getAvgSalaryByState(@PathVariable String onetSocTrimmed) {
		logger.info("getAvgSalaryByState");
		onetSocTrimmed = onetSocTrimmed + "%";
		List<StateSalary> results;
		try {
			results = dao.getAvgSalaryByState(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/StateEntrySalary/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<StateSalaryEntry>> getEntrySalaryByState(@PathVariable String onetSocTrimmed) {
		logger.info("getEntrySalaryByState");
		onetSocTrimmed += "%";
		List<StateSalaryEntry> results;
		try {
			results = dao.getEntrySalaryByState(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/NationalSalary/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> getNationalSalary(@PathVariable String onetSocTrimmed) {
		logger.info("getNationalSalary");
		Integer results;
		try {
			results = dao.getNationalSalary(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/NationalEntrySalary/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> getNationalEntrySalary(@PathVariable String onetSocTrimmed) {
		logger.info("getNationalEntrySalary");
		Integer results;
		try {
			results = dao.getNationalEntrySalary(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/BLSTitle/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Map<String, String>> getBLSTitle(@PathVariable String onetSocTrimmed) {
		logger.info("getBLSTitle");
		String blsTitle;
		try {
			blsTitle = dao.getBLSTitle(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(Collections.singletonMap("blsTitle", blsTitle), HttpStatus.OK);
	}

	@RequestMapping(value = "/OccupationInterestCodes/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccupationInterestCode>> getOccupationInterestCodes(@PathVariable String onetSoc) {
		logger.info("getOccupationInterestCodes");
		List<OccupationInterestCode> results;
		try {
			results = dao.getOccupationInterestCodes(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/HotGreenStemFlags/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<HotGreenStem> getHotGreenStemFlags(@PathVariable String onetSoc) {
		logger.info("getHotGreenStemFlags");
		HotGreenStem results;
		try {
			results = dao.getHotGreenStemFlags(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/ServiceOfferingCareer/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ServiceOfferingCareer>> getServiceOfferingCareer(@PathVariable String onetSoc) {
		logger.info("getServiceOfferingCareer");
		List<ServiceOfferingCareer> results;
		try {
			results = dao.getServiceOfferingCareer(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/RelatedCareerCluster/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<RelatedCareerCluster>> getRelatedCareerCluster(@PathVariable String onetSoc) {
		logger.info("getRelatedCareerCluster");
		List<RelatedCareerCluster> results;
		try {
			results = dao.getRelatedCareerCluster(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/SchoolMoreDetails/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<SchoolMoreDetails>> getSchoolMoreDetails(@PathVariable String onetSoc) {
		logger.info("getSchoolMoreDetails");
		List<SchoolMoreDetails> results;
		try {
			results = dao.getSchoolMoreDetails(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SchoolProfile/{unitId}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<SchoolProfile> getSchoolProfile(@PathVariable int unitId) {
		logger.info("getSchoolProfile");
		SchoolProfile results;
		try {
			results = dao.getSchoolProfile(unitId);
			Map<String, List<cip>> majorSet = new HashMap<>();
			for(HashMap<String, Object> major: results.getMajors()) {
				String key = (String)major.get("cipFamily");
				cip newcip = new cip();
				newcip.setCipTitle((String)major.get("cipTitle"));
				newcip.setCipDescrpition((String)major.get("cipDescription"));

				if(majorSet.containsKey(key)) {
					majorSet.get(key).add(newcip);
				} else {
					List<cip> cipList = new ArrayList<>();
					cipList.add(newcip);
					majorSet.put(key, cipList);
				}
			}
			results.setCollegeMajors(majorSet);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/IpedsYearUpdated", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> getIpedsYearUpdated() {
		String results;
		try {
			results = dbInfoDao.getDbInfo("ipeds_year_updated").getValue();
		} catch (Exception e) {
			logger.error("Error", e);
			return ResponseEntity.badRequest().body("");
		}
		return ResponseEntity.ok(results);
	}
	
	@RequestMapping(value = "/SchoolMajors/{onetCode}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<String>> getCareerMajors(@PathVariable String onetCode) {
		logger.info("getCareerMajors");
		List<String> results;
		try {
			results =  dao.getCareerMajors(onetCode);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/StateSalaryYear", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> getStateSalaryYear() {
		logger.info("getStateSalaryYear");
		int results;
		try {
			results = dao.getStateSalaryYear();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/EmploymentMoreDetails/{onetSocTrimmed}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<EmploymentMoreDetails>> getEmploymentMoreDetails(@PathVariable String onetSocTrimmed) {
		logger.info("getEmploymentMoreDetails");
		List<EmploymentMoreDetails> results;
		try {
			results = dao.getEmploymentMoreDetails(onetSocTrimmed);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/MilitaryMoreDetails/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<MilitaryMoreDetails>> getMilitaryMoreDetails(@PathVariable String onetSoc) {
		logger.info("getMilitaryMoreDetails");
		List<MilitaryMoreDetails> results;
		try {
			results = dao.getMilitaryMoreDetails(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/MilitaryHotJobs/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<MilitaryHotJobs>> getMilitaryHotJobs(@PathVariable String onetSoc) {
		logger.info("getMilitaryHotJobs");
		List<MilitaryHotJobs> results = new ArrayList<>();

		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	/**
	 * Returns a list of more resources for a particular occupation.	 *
	 * @param onetSoc the O*NET SOC Code of the occupation
	 * @return a list of occufind resources
	 */
	@RequestMapping(value = "/OccufindMoreResources/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindMoreResources>> getOccufindMoreResources(@PathVariable String onetSoc) {
		logger.info("getOccufindMoreResources");
		List<OccufindMoreResources> results;
		try {
			results = dao.getOccufindMoreResources(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	/**
	 * Returns bright outlook occupations ordered by the user's FYI results.
	 */
	@RequestMapping(value = "/DashboardBrightOutlook/{interestCdOne}/{interestCdTwo}/{interestCdThree}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> getDashboardBrightOutlook(
			@PathVariable String interestCdOne,
			@PathVariable String interestCdTwo,
			@PathVariable String interestCdThree) {
		logger.info("getDashboardBrightOutlook");
		List<OccufindSearch> results;
		try {
			results = dao.getDashboardBrightOutlook(interestCdOne, interestCdTwo, interestCdThree);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	/**
	 * Returns STEM careers ordered by the user's FYI results
	 */
	@RequestMapping(value = "/DashboardStemCareers/{interestCdOne}/{interestCdTwo}/{interestCdThree}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> getDashboardStemCareers(
			@PathVariable String interestCdOne,
			@PathVariable String interestCdTwo,
			@PathVariable String interestCdThree) {
		logger.info("getDashboardBrightOutlook");
		List<OccufindSearch> results;
		try {
			results = dao.getDashboardStemCareers(interestCdOne, interestCdTwo, interestCdThree);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	/**
	 * Returns Green career list ordered by the user's FYI results.
	 */
	@RequestMapping(value = "/DashboardGreenCareers/{interestCdOne}/{interestCdTwo}/{interestCdThree}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> getDashboardGreenCareers(
			@PathVariable String interestCdOne,
			@PathVariable String interestCdTwo,
			@PathVariable String interestCdThree) {
		logger.info("getDashboardGreenOutlook");
		List<OccufindSearch> results;
		try {
			results = dao.getDashboardGreenCareers(interestCdOne, interestCdTwo, interestCdThree);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	/**
	 * Returns all career list.
	 */
	@RequestMapping(value = "/ViewAllOccupations/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> viewAllOccupations() {
		logger.info("viewAllOccupations");
		List<OccufindSearch> results;
		try {
			results = dao.viewAllOccupations();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	/**
	 * Returns all career list.
	 */
	@RequestMapping(value = "/ViewMilitaryOccupations/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> viewMilitaryOccupations() {
		logger.info("viewMilitaryOccupations");
		List<OccufindSearch> results;
		try {
			results = dao.viewMilitaryOccupations();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	/**
	 * Returns hot military careers.
	 */
	@RequestMapping(value = "/ViewHotOccupations/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindSearch>> viewHotOccupations() {
		logger.info("viewMilitaryOccupations");
		List<OccufindSearch> results;
		try {
			results = dao.viewHotOccupations();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/OccufindAlternativeView/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<OccufindAlternative>> onetHideDetails(@PathVariable String onetSoc) {
		logger.info("onetHideDetails");
		List<OccufindAlternative> results;
		try {
			results = dao.onetHideDetails(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@RequestMapping(value = "/ImageAltText/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Map<String, String>> getImageAltText(@PathVariable String onetSoc) {
		logger.info("getImageAltText");
		String results;
		try {
			results = dao.getImageAltText(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(Collections.singletonMap("imageAltText", results), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/job-zone/{onetSoc}/", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<JobZone> getJobZone(@PathVariable String onetSoc) {
		logger.info("getJobZone");
		JobZone results;
		try {
			results = dao.getJobZone(onetSoc);
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

}