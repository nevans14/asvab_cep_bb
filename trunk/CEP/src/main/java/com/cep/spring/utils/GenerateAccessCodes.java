package com.cep.spring.utils;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;


/**
 * 
 * @author Dave Springer
 * 
 * Based on the document "2016 ASVAB Website Passcode System"
 * This class generates Access Codes in the following types and formats.
 * 
 * Marketing Codes   Ex.  ###[A-Z][A-Z] 
 * Counselor Codes   Ex.  C#####
 * Reserved Codes    Ex.  #####[A-E]  Depends on year 
 * Three Week Codes  Ex.  [A-Z][A-Z][A-Z][A-Z][A-Z]
 * 
 * Note: [A-Z] is letters from A to Z minus L, I, O!
 * 
 */
public class GenerateAccessCodes {
	/**
	 * generateMarketingCodes : 
	 * 
	 * Marketing Codes   Ex.  ###[A-Z][A-Z] 
	 * 
	 * @param setSize  The number of ids to generate.
	 * @return A set of unique access codes of the size setSize.
	 */
	public static Set<String> generateMarketingCodes(Integer setSize) {
		Set<String> set = new HashSet<>();
		while ( set.size() < setSize ){
			set.add(getNextRandomNumber(1, 999, "%03d") + getNextRandomCharacters(2)   );
		}
		return set;
	}

	
	/**
	 * generateCounselorCodes
	 * 
	 *  * Counselor Codes   Ex.  C#####
	 * 
	 * @param setSize  The number of ids to generate.
	 * @return A set of unique access codes of the size setSize.
	 */
	public static Set<String> generateCounselorCodes(Integer setSize) {
		Set<String> set = new HashSet<>();
		while ( set.size() < setSize ){
			if ( set.size() == 10000) break;
			set.add("C" +  getNextRandomNumber(1, 10000, "%05d")  );
		}
		return set;
	}

	/**
	 * generateReservedCodes
	 * Reserved Codes    Ex.  #####[A-E]  Depends on year 
	 * 
	 * @param setSize number of Ids to be generated
	 * @return the set of Ids
	 */
	public static Set<String> generateReservedCodes(Integer setSize) {
		Set<String> set = new HashSet<>();
		// Determine the suffix 
		String suffix1 = "";
		String suffix2 = "";
		Calendar date = new GregorianCalendar();
		int year = date.get(Calendar.YEAR);  // 2012
		
		if ( year % 3 == 0 ) {
			suffix1 = "E";
			suffix2 = "A";
		} else if ( year % 3 == 1 ) {
			suffix1 = "A";
			suffix2 = "B";
		} else if ( year % 3 == 2 ) {
			suffix1 = "C";
			suffix2 = "D";	
		} 
		while ( set.size() < setSize ){
			if ( set.size() == 30000) break;
			if ( set.size() % 2 == 0 ) 
				set.add( getNextRandomNumber(1, 100000, "%05d") + suffix1 );
			else 
				set.add( getNextRandomNumber(1, 100000, "%05d") + suffix2 );

		}
		return set;
	}
	
	/**
	 * generateThreeWeekCodes 
	 *  
	 * Three Week Codes  Ex.  [A-Z][A-Z][A-Z][A-Z][A-Z]
	 * 
	 * @param setSize the number of Ids to be generated
	 * @return the set of Ids
	 */
	public static Set<String> generateThreeWeekCodes(Integer setSize) {
		Set<String> set = new HashSet<>();
		while ( set.size() < setSize ){
			set.add( getNextRandomCharacters(5) );
		}
		return set;
	}
	
	/* Helper Functions */
	/**
	 * getNextRandomNumber - Generates a random number between the min and max.
	 * 
	 * 
	 * @param min    Low end of range.
	 * @param max    High end of range.
	 * @param format "%05d"
	 * @return a random number
	 */
	public static String getNextRandomNumber(Integer min, Integer max, String format) {
		SecureRandom random = new SecureRandom();
		return String.format(Locale.ENGLISH, format, random.nextInt(max - min + 1) + min );
	}
	
	/**
	 * getNextRandomCharacters - gets a random character string with out I,L,O
	 * 
	 * @param size   Number of random characters to return.
	 * @return random characters
	 */
	private static String getNextRandomCharacters(Integer size) {
		StringBuilder sb = new StringBuilder();
		int ii = 'I'; // 73 
		int ll = 'L';  // 76
		int oo = 'O';  // 79		
		int a = 65;
		int z = 90;
		int count = 0;
		
		while (count < size ) {
			SecureRandom random = new SecureRandom();
			int randC = random.nextInt(z - a + 1) + a ;
			if (randC != ii || randC != ll || randC != oo) {
				sb.append((char) randC);
			}
			count++;
		}
		return sb.toString() ;
	}
	
	   /**
     * generateNewMarketingCodes 
     *  
     * New Marketing codes  [00000-99999]+ "MARK"
     * 
     * @param setSize the number of Ids to be generated
     * @return the set of Ids
     */
    public Set<String> generateCustomMarketingCodes(String prefix_name, String postfix_name, Integer setSize, String type, String creator, DataSource dataSource) {
        Set<String> set = new HashSet<>();
/*        while ( set.size() < setSize ){
            set.add( getNextRandomCharacters(5) + "MARK" );
        }
*/        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        for ( int i = 1 ; i <= setSize; i++) {
            Set<String> ss = generateMarketingCodes(1);
            String str = getNextRandomNumber(1, 999, "%03d") + getNextRandomCharacters(2);
            String access_code = prefix_name + str + postfix_name;
            int exists = jdbcTemplate.queryForObject("SELECT count(*) FROM [ASVAB_CEP_2018].[dbo].[access_codes] where [access_code]  = ?", new Object[] {access_code}, Integer.class);
            if(exists == 0) {
            	set.add(access_code);
            	jdbcTemplate.update("INSERT INTO [ASVAB_CEP_2018].[dbo].[access_codes] (access_code, role) VALUES ("+access_code+", "+type+")");
            	jdbcTemplate.update("INSERT INTO [ASVAB_CEP_2018].[dbo].[access_code_log] (access_code, creation_date, creator) VALUES ("+access_code+", "+dateFormat.format(cal)+", "+creator+")");
            } else {
				i--;
			}
        }
        return set;
    }
    
    
    /** Given todays date determine what the Modulus year would be.
     * 
     * Modulus 0 SchoolYear2016 = "F,G,H,J,K,L"  
     * Modulus 1 SchoolYear2017 = "M,N,P,Q,R,S" 
     * Modulus 2 SchoolYear2018 = "T,U,V,W,X,Y" 
     * 
     * @param schoolYear the school year
     * @return list of suffixes
     */
    public static List<String> getSchoolYearSuffixes(Integer schoolYear ) {
        ArrayList<String> list = null;
        int remainder = schoolYear % 3;
        
        if ( remainder == 0 ) {
            list = new ArrayList<String>() {{
                add("F");
                add("G");
                add("H");
                add("J");
                add("K");
                add("L");
                add("f");
                add("g");
                add("h");
                add("j");
                add("k");
                add("l");         }};
        } else if ( remainder == 1 ) {
            list = new ArrayList<String>() {{
                add("M");
                add("N");
                add("P");
                add("Q");
                add("R");
                add("S");
                add("m");
                add("n");
                add("p");
                add("q");
                add("r");
                add("s");
         }};
        } else if ( remainder == 2 ) {
            list = new ArrayList<String>() {{
                add("T");
                add("U");
                add("V");
                add("W");
                add("X");
                add("Y");
                add("t");
                add("u");
                add("v");
                add("w");
                add("x");
                add("y");
         }};
        }
        return list;
    }

    public static boolean isSuffixInCurrentYear(String suffix){
        Integer  year = determineSchoolYear(); 
        List<String> ls = getSchoolYearSuffixes(year % 3);
		return ls.contains(suffix);
    }
    
    public static Integer schoolYearOfSuffix(String suffix){
        //  Today is in what school year?
        Integer  thisYear = determineSchoolYear(); 
        Integer  lastYear = thisYear - 1 ;
        Integer  twoYearsAgo = thisYear - 2;

        if (getSchoolYearSuffixes(thisYear ).contains(suffix)) return thisYear;
        if (getSchoolYearSuffixes(lastYear ).contains(suffix)) return lastYear;
        if (getSchoolYearSuffixes(twoYearsAgo ).contains(suffix)) return twoYearsAgo;
        return null;
    }
    
    /**
     * Returns what the school year.
     * @return the school year
     */
    public static Integer determineSchoolYear(){
        String yearMonth = SimpleUtils.getCurrentYearMonth();
        String [] yearMonthComponents = yearMonth.split("-");
        Integer year = Integer.parseInt(yearMonthComponents[0]);
        int month = Integer.parseInt(yearMonthComponents[1]);
        // If month is greater than six then the school year is equal to year + 1.
        // If month is seven or more then the school year is equal to year.
        if ( month > 6 ){ 
            year++;
        }
        return year;
    }
}
