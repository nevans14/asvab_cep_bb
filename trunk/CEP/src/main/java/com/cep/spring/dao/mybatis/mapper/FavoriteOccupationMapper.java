package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.favorites.FavoriteCareerCluster;
import com.cep.spring.model.favorites.FavoriteCitmOccupation;
import com.cep.spring.model.favorites.FavoriteMilitaryService;
import com.cep.spring.model.favorites.FavoriteOccupation;
import com.cep.spring.model.favorites.FavoriteSchool;

public interface FavoriteOccupationMapper {

	ArrayList<FavoriteOccupation> getFavoriteOccupation(@Param("userId") Integer userId);

	int insertFavoriteOccupation(@Param("favoriteObject") FavoriteOccupation favoriteObject, @Param("userId") Integer userId);

	int updateFavoriteOccupationHearts(@Param("model") FavoriteOccupation model, @Param("userId") Integer userId);

	int deleteFavoriteOccupation(@Param("id") Integer id, @Param("userId") Integer userId);
	
	ArrayList<FavoriteCareerCluster> getFavoriteCareerCluster(@Param("userId") Integer userId);

	int insertFavoriteCareerCluster(@Param("favoriteObject") FavoriteCareerCluster favoriteObject, @Param("userId") Integer userId);

	int deleteFavoriteCareerCluster(@Param("id") Integer id, @Param("userId") Integer userId);
	
	ArrayList<FavoriteSchool> getFavoriteSchool(@Param("userId") Integer userId);

	ArrayList<FavoriteSchool> getFavoriteSchoolBySocId(@Param("socId") String socId, @Param("userId") int userId);

	int insertFavoriteSchool(@Param("favoriteObject") FavoriteSchool favoriteObject, @Param("userId") Integer userId);

	int deleteFavoriteSchool(@Param("id") Integer id, @Param("userId") Integer userId);

	int deleteFavoriteCareerClusterByUserId(Integer userId);

	int deleteFavoriteOccupationByUserId(Integer userId);

	int deleteFavoriteSchoolByUserId(Integer userId);
	
	ArrayList<FavoriteCitmOccupation> getFavoriteCitmOccupation(@Param("userId") Integer userId);
	
	int insertFavoriteCitmOccupation(@Param("favoriteObject") FavoriteCitmOccupation favoriteObject, @Param("userId") Integer userId);

	int deleteFavoriteCitmOccupationByUserId(@Param("userId") Integer userId);

	int insertFavoriteService(@Param("favoriteObject") FavoriteMilitaryService favoriteObject, @Param("userId") Integer userId);
	
	int deleteFavoriteServiceByUserId(@Param("userId") Integer userId);

	ArrayList<FavoriteMilitaryService> getFavoriteService(@Param("userId") Integer userId);

}
