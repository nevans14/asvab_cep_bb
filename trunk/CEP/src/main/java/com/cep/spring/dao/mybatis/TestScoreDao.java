package com.cep.spring.dao.mybatis;

import java.util.List;

import com.cep.spring.model.testscore.*;

public interface TestScoreDao {

	int insertTestScoreimpl(List<StudentTestingProgam> testScore);
	
	StudentTestingProgamSelects getTestScoreWithAccessCode(String accessCode);
	
	StudentTestingProgamSelects getTestScoreWithUserId(Integer userId);

	StudentAsvabScoreResults getStudentASRByAccessCode(String accessCode);
	
	int insertManualTestScore(ManualTestScore manualTestScoreObj);
	
	ManualTestScore getManualTestScore(int userId);
	
	int updateManualTestScore(ManualTestScore manualTestScoreObj);
	
	int deleteManualTestScore(int userId);
	
	List<StudentTestingProgamSelects> filterTestScore(FilterScoreObj filterScoreObj);
	
	int deleteIcatDuplicates(String sourceFileName, String type);
	
	int getRecordCount(String sourceFileName, String platform);
}
