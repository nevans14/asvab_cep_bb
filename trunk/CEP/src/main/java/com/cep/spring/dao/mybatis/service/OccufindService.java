package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.OccufindMapper;
import com.cep.spring.model.MilitaryCareerResource;
import com.cep.spring.model.OOHResource;
import com.cep.spring.model.occufind.AlternateTitles;
import com.cep.spring.model.occufind.CertificateExplanation;
import com.cep.spring.model.occufind.EmploymentMoreDetails;
import com.cep.spring.model.occufind.HotGreenStem;
import com.cep.spring.model.occufind.JobZone;
import com.cep.spring.model.occufind.MilitaryHotJobs;
import com.cep.spring.model.occufind.MilitaryMoreDetails;
import com.cep.spring.model.occufind.OccufindAlternative;
import com.cep.spring.model.occufind.OccufindMoreResources;
import com.cep.spring.model.occufind.OccufindSearch;
import com.cep.spring.model.occufind.OccufindSearchArguments;
import com.cep.spring.model.occufind.RelatedCareerCluster;
import com.cep.spring.model.occufind.SchoolMajors;
import com.cep.spring.model.occufind.SchoolMoreDetails;
import com.cep.spring.model.occufind.SchoolProfile;
import com.cep.spring.model.occufind.ServiceOfferingCareer;
import com.cep.spring.model.occufind.TitleDescription;
import com.cep.spring.model.Occupation;
import com.cep.spring.model.OccupationInterestCode;
import com.cep.spring.model.StateSalary;
import com.cep.spring.model.StateSalaryEntry;

@Service
public class OccufindService {

	private final OccufindMapper mapper;

	@Autowired
	public OccufindService(OccufindMapper mapper) {
		this.mapper = mapper;
	}

	public ArrayList<OccufindSearch> getOccufindSearch(OccufindSearchArguments userSearchArguments) {
		return mapper.getOccufindSearch(userSearchArguments);
	}

	public TitleDescription getOccupationTitleDescription(String onetSoc) {
		return mapper.getOccupationTitleDescription(onetSoc);
	}

	public ArrayList<Occupation> getOccupationASK(String onetSoc) {
		return mapper.getOccupationASK(onetSoc);
	}

	public ArrayList<MilitaryCareerResource> getMilitaryCareerResources(String onetSoc) {
		return mapper.getMilitaryCareerResources(onetSoc);
	}

	public ArrayList<OOHResource> getOOHResources(String onetSoc) {
		return mapper.getOOHResources(onetSoc);
	}

	public ArrayList<StateSalary> getAvgSalaryByState(String onetSocTrimmed) {
		return mapper.getAvgSalaryByState(onetSocTrimmed);
	}
	
	public ArrayList<StateSalaryEntry> getEntrySalaryByState(String onetSocTrimmed) {
		return mapper.getEntrySalaryByState(onetSocTrimmed);
	}

	public Integer getNationalSalary(String onetSocTrimmed) {
		return mapper.getNationalSalary(onetSocTrimmed);
	}
	
	public Integer getNationalEntrySalary(String onetSocTrimmed) {
		return mapper.getNationalEntrySalary(onetSocTrimmed);
	}

	public String getBLSTitle(String onetSocTrimmed) {
		return mapper.getBLSTitle(onetSocTrimmed);
	}

	public ArrayList<OccupationInterestCode> getOccupationInterestCodes(String onetSoc) {
		return mapper.getOccupationInterestCodes(onetSoc);
	}

	public HotGreenStem getHotGreenStemFlags(String onetSoc) {
		return mapper.getHotGreenStemFlags(onetSoc);
	}

	public ArrayList<ServiceOfferingCareer> getServiceOfferingCareer(String onetSoc) {
		return mapper.getServiceOfferingCareer(onetSoc);
	}

	public ArrayList<RelatedCareerCluster> getRelatedCareerCluster(String onetSoc) {
		return mapper.getRelatedCareerCluster(onetSoc);
	}

	public ArrayList<SchoolMoreDetails> getSchoolMoreDetails(String onetSoc) {
		return mapper.getSchoolMoreDetails(onetSoc);
	}

	public Integer getStateSalaryYear() {
		return mapper.getStateSalaryYear();
	}

	public ArrayList<EmploymentMoreDetails> getEmploymentMoreDetails(String trimmedOnetSoc) {
		return mapper.getEmploymentMoreDetails(trimmedOnetSoc);
	}

	public ArrayList<MilitaryMoreDetails> getMilitaryMoreDetails(String onetSoc) {
		return mapper.getMilitaryMoreDetails(onetSoc);
	}

	public ArrayList<MilitaryHotJobs> getMilitaryHotJobs(String onetSoc) {
		return mapper.getMilitaryHotJobs(onetSoc);
	}

	public ArrayList<OccufindMoreResources> getOccufindMoreResources(String onetSoc) {
		return mapper.getOccufindMoreResources(onetSoc);
	}

	public ArrayList<OccufindSearch> getDashboardBrightOutlook(String interestCdOne, String interestCdTwo,
			String interestCdThree) {
		return mapper.getDashboardBrightOutlook(interestCdOne, interestCdTwo, interestCdThree);
	}

	public ArrayList<OccufindSearch> getDashboardStemCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) {
		return mapper.getDashboardStemCareers(interestCdOne, interestCdTwo, interestCdThree);
	}

	public ArrayList<OccufindSearch> getDashboardGreenCareers(String interestCdOne, String interestCdTwo,
			String interestCdThree) {
		return mapper.getDashboardGreenCareers(interestCdOne, interestCdTwo, interestCdThree);
	}

	public ArrayList<OccufindSearch> viewAllOccupations() {
		return mapper.viewAllOccupations();
	}
	
	public ArrayList<OccufindSearch> viewMilitaryOccupations() {
		return mapper.viewMilitaryOccupations();
	}

	public ArrayList<OccufindSearch> viewHotOccupations() {
		return mapper.viewHotOccupations();
	}
	
	public ArrayList<OccufindAlternative> onetHideDetails(String onetSoc) {
		return mapper.onetHideDetails(onetSoc);
	}

	public String getImageAltText(String onetSoc) {
		return mapper.getImageAltText(onetSoc);
	}
	
	public JobZone getJobZone(String onetSoc) {
		return mapper.getJobZone(onetSoc);
	}

	public CertificateExplanation getCertificateExplanation(String onetSoc) {
		return mapper.getCertificateExplanation(onetSoc);
	}

	public List<AlternateTitles> getAlternateTitles(String onetSoc) {
		return mapper.getAlternateTitles(onetSoc);
	}

	public SchoolProfile getSchoolProfile(int unitId) {
		return mapper.getSchoolProfile(unitId);
	}

	public List<String> getCareerMajors(String onetCode) {
		return mapper.getCareerMajors(onetCode);
	}

}
