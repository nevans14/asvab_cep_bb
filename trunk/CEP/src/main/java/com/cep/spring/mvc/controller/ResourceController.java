package com.cep.spring.mvc.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cep.spring.dao.mybatis.ResourceDAO;
import com.cep.spring.model.resources.ResourceQuickLinkTopic;
import com.cep.spring.model.resources.ResourceTopic;

@RestController
@RequestMapping("/resources")
public class ResourceController {

	private final Logger logger = LogManager.getLogger();
	private final ResourceDAO dao;

	@Autowired
	public ResourceController(ResourceDAO dao) {
		this.dao = dao;
	}
	
	@RequestMapping(value = "/get-resources", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ResourceTopic>> getResources() {
		List<ResourceTopic> results;
		try {
			results = dao.getResources();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-quick-links", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ResourceQuickLinkTopic>> getResourceTopics() {
		List<ResourceQuickLinkTopic> results;
		try {
			results = dao.getQuickLinks();
		} catch (Exception e) {
			logger.error("Error", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
}