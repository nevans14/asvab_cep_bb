package com.cep.spring.dao.mybatis.service;

import com.cep.spring.dao.mybatis.mapper.LearnAboutYourselfMapper;
import com.cep.spring.model.Dashboard.MoreAboutMe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LearnAboutYourselfService {

    private final LearnAboutYourselfMapper mapper;

    @Autowired
    public LearnAboutYourselfService(LearnAboutYourselfMapper mapper) {
        this.mapper = mapper;
    }

    public List<MoreAboutMe> getMoreAboutMe(int userId) {
        return mapper.getMoreAboutMe(userId);
    }

    public int insertMoreAboutMe(MoreAboutMe model) {
        return mapper.insertMoreAboutMe(model);
    }

    public int updateMoreAboutMe(MoreAboutMe model) {
        return mapper.updateMoreAboutMe(model);
    }

    public int deleteMoreAboutMe(int id, int userId) {
        return mapper.deleteMoreAboutMe(id, userId);
    }
}
