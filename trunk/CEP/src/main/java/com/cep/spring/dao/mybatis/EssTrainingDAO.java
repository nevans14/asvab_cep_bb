package com.cep.spring.dao.mybatis;

import com.cep.spring.model.EssTraining.EssTraining;
import com.cep.spring.model.EssTraining.EssTrainingEmail;
import com.cep.spring.model.EssTraining.EssTrainingRegistration;

public interface EssTrainingDAO {
	
	EssTraining getEssTrainingById(int essTrainingId) throws Exception;
	
	EssTrainingEmail getEssTrainingEmailById(int essTrainingId) throws Exception;
	
	int addEssTraining(EssTraining essTraining) throws Exception;
	
	int updateEssTraining(EssTraining essTraining) throws Exception;
	
	int removeEssTraining(int essTrainingId) throws Exception;
	
	int isUserRegistered(String emailAddress, int essTrainingId) throws Exception;
	
	int addEssTrainingRegistration(EssTrainingRegistration essTrainingRegistration) throws Exception;
	
}