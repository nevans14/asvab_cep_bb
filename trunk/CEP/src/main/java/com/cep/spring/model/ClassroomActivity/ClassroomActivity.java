package com.cep.spring.model.ClassroomActivity;

import java.util.List;

public class ClassroomActivity {
    private Integer id;
    private String title, shortDescription, longDescription;
    private List<Integer> categoryIds;
    private List<Standard> standards;
    private List<ClassroomActivityPdf> pdfs;

    public void setId(Integer id) { this.id = id; }
    public Integer getId() { return id; }

    public void setCategoryIds(List<Integer> categoryIds) { this.categoryIds = categoryIds; }
    public List<Integer> getCategoryIds() { return categoryIds; }

    public void setTitle(String title) { this.title = title; }
    public String getTitle() { return title; }

    public void setShortDescription(String shortDescription) { this.shortDescription = shortDescription; }
    public String getShortDescription() { return shortDescription; }

    public void setLongDescription(String longDescription) { this.longDescription = longDescription; }
    public String getLongDescription() { return longDescription; }

    public void setStandards(List<Standard> standards) { this.standards = standards; }
    public List<Standard> getStandards() { return standards; }

    public void setPdfs(List<ClassroomActivityPdf> pdfs) { this.pdfs = pdfs; }
    public List<ClassroomActivityPdf> getPdfs() { return pdfs; }
}
