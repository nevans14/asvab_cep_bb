package com.cep.spring.dao.mybatis.mapper;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.StaticPage;

public interface StaticPageMapper {
	
	StaticPage getPageById(int pageId);
    
    StaticPage getPageByPageNameOld(String pageName);

    StaticPage getPageByPageName(@Param("pageName") String pageName, @Param("website") String website);
	
}