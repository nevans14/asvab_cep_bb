package com.cep.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.util.Assert;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@PropertySource("classpath:application.properties")
public final class CustomCookieCsrfTokenRepository implements CsrfTokenRepository {

    static final String DEFAULT_CSRF_COOKIE_NAME = "XSRF-TOKEN";

    private final CookieCsrfTokenRepository csrfTokenRepository;
    private String cookieDomain;
    private boolean cookieHttpOnly;
    private String cookiePath;

    @Value("${security.cookie.secure}")
    private boolean isSecure;

    public CustomCookieCsrfTokenRepository() {
        this.csrfTokenRepository = new CookieCsrfTokenRepository();
    }

    @Override
    public CsrfToken generateToken(HttpServletRequest request) {
        return this.csrfTokenRepository.generateToken(request);
    }

    @Override
    public void saveToken(CsrfToken token, HttpServletRequest request, HttpServletResponse response) {
        String tokenValue = token == null ? "" : token.getToken();
        if (tokenValue.isEmpty()) {
            tokenValue = this.generateToken(request).getToken();
        }
        Cookie cookie = new Cookie(DEFAULT_CSRF_COOKIE_NAME, tokenValue);
        cookie.setSecure(isSecure);
        cookie.setDomain(cookieDomain);
        cookie.setPath(cookiePath);
        if (token == null) {
            cookie.setMaxAge(0);
        } else {
            cookie.setMaxAge(-1);
        }
        cookie.setHttpOnly(cookieHttpOnly);
        response.addCookie(cookie);
    }

    @Override
    public CsrfToken loadToken(HttpServletRequest request) {
        return this.csrfTokenRepository.loadToken(request);
    }

    public String getCookieDomain() {
        return this.cookieDomain;
    }

    public void setCookieDomain(final String cookieDomain) {
        Assert.notNull(cookieDomain, "cookieDomain is not null");
        this.cookieDomain = cookieDomain;
    }

    public boolean getCookieHttpOnly() {
        return this.cookieHttpOnly;
    }

    public void setCookieHttpOnly(final boolean cookieHttpOnly) {
        this.cookieHttpOnly = cookieHttpOnly;
    }

    public void setCookiePath(String cookiePath) {
        this.cookiePath = cookiePath;
    }
}
