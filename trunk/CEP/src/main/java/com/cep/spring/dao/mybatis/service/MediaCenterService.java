package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cep.spring.dao.mybatis.mapper.MediaCenterMapper;
import com.cep.spring.model.media.MediaCenter;

@Service
public class MediaCenterService {

	private final MediaCenterMapper mapper;

	@Autowired
	public MediaCenterService(MediaCenterMapper mapper) {
		this.mapper = mapper;
	}

	public ArrayList<MediaCenter> getMediaCenterList() {
		return mapper.getMediaCenterList();
	}
	
	public ArrayList<MediaCenter> getMediaCenterByCategoryId(Integer categoryId) {
		return mapper.getMediaCenterByCategoryId(categoryId);
	}

	public MediaCenter getArticleById(int mediaId) {
		return mapper.getArticleById(mediaId);
	}
}
