package com.cep.spring.model.optionready;

public class FlickrPhotoCollectionPhoto {
	
	private String id, secret, owner, title;
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setSecret(String secret) {
		this.secret = secret;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getId() {
		return id;
	}
	
	public String getSecret() {
		return secret;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public String getTitle() {
		return title;
	}
}