package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.cep.spring.dao.mybatis.mapper.FindYourInterestMapper;
import com.cep.spring.model.CombinedScore;
import com.cep.spring.model.GenderScore;
import com.cep.spring.model.fyi.FYIMerge;
import com.cep.spring.model.fyi.FindYourInterestProfileV2;
import com.cep.spring.model.fyi.ScoreChoice;
import com.cep.spring.model.fyi.Scores;
import com.cep.spring.model.fyi.TiedSelection;

@Service
public class FindYourInterestService {

	private final Logger logger = LogManager.getLogger();
	private final FindYourInterestMapper mapper;

	@Autowired
	public FindYourInterestService(FindYourInterestMapper mapper) {
		this.mapper = mapper;
	}

	// TODO: Investigate why transactional not working and fix it
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean insertProfileAndTest(FindYourInterestProfileV2 fyiObject) {
		logger.info("inside the insertProfile method. is wrapped in transaction: "
				+ TransactionSynchronizationManager.isActualTransactionActive());
		// check to see if user has a find your interest profile
		int numRowSelectedProfile = mapper.getProfileExist(fyiObject.getUserId());
		int numRowInsertedProfile = 0;
		// if fyi user profile doesn't exist then create one
		if (numRowSelectedProfile == 0) {
			numRowInsertedProfile = mapper.insertProfile(fyiObject.getUserId());
		}
		// test id is created here
		int numRowInsertedTest = mapper.insertTest(fyiObject);
		// check to see if all necessary data were inserted
		return (((numRowSelectedProfile == 0 && numRowInsertedProfile == 1) || (numRowSelectedProfile == 1)) && numRowInsertedTest == 1);
	}

	public ArrayList<Scores> getCombinedAndGenderScore(Integer userId) {
		return mapper.getCombinedAndGenderScore(userId);
	}

	@Transactional
	public int updateProfile(int userId, ScoreChoice scoreChoice) {
		return mapper.updateProfile(userId, scoreChoice);
	}
	@Transactional
	public int insertTest(FindYourInterestProfileV2 model) {
		return mapper.insertTest(model);
	}

	public ArrayList<GenderScore> getGenderScore(char gender, int rRawScore, int iRawScore, int aRawScore,
			int sRawScore, int eRawScore, int cRawScore) {
		return mapper.getGenderScore(gender, rRawScore, iRawScore, aRawScore, sRawScore, eRawScore,
				cRawScore);
	}

	public ArrayList<CombinedScore> getCombinedScore(int rRawScore, int iRawScore, int aRawScore, int sRawScore,
			int eRawScore, int cRawScore) {
		return mapper.getCombinedScore(rRawScore, iRawScore, aRawScore, sRawScore, eRawScore,
				cRawScore);
	}

	public Integer getNumberTestTaken(int oldUserId, int newUserId) {
		return mapper.getNumberTestTaken(oldUserId, newUserId);
	}


	public ArrayList<ScoreChoice> getProfile(int userId) {
		return mapper.getProfile(userId);
	}

	public int getProfileExist(Integer userId) {
		return mapper.getProfileExist(userId);
	}

	public String getScoreChoice(int userId) {
		return mapper.getScoreChoice(userId);
	}

	public TiedSelection getTiedSelection(int userId) {
		return mapper.getTiedSelection(userId);
	}

	@Transactional
	public int insertTiedSelection(int userId, TiedSelection tiedObject) {
		return mapper.insertTiedSelection(userId, tiedObject);
	}

	@Transactional
	public int deleteTiedSelection(int userId) {
		return mapper.deleteTiedSelection(userId);
	}

	public int getWhichLastFyiTableUsed(int oldUserId, int oldTestId) {
		return mapper.getWhichLastFyiTableUsed(oldUserId, oldTestId);
	}

	@Transactional
	public int mergeProfile(int oldUserId, int newUserId) {
		return mapper.mergeProfile(oldUserId, newUserId);
	}

	@Transactional
	public int mergeTests(FYIMerge fyiObject) {
		return mapper.mergeTests(fyiObject);
	}
	
	@Transactional
	public int mergeResults(int oldUserId, int newUserId) {
		return mapper.mergeResults(oldUserId, newUserId);
	}

	@Transactional
	public int mergeAnswers(int oldUserId, int newUserId, int testId) {return mapper.mergeAnswers(oldUserId, newUserId, testId);}

	@Transactional
	public int mergeResponse(int oldUserId, int newTestId) { return mapper.mergeResponse(oldUserId, newTestId); }

	public FYIMerge getLastTestObject(int oldUserId, int newUserId) {
		return mapper.getLastTestObject(oldUserId, newUserId);
	}

	public int deleteProfile(int userId) {
		return mapper.deleteProfile(userId);
	}

	public Integer getOldUserId(Integer newUserId) {
		return mapper.getOldUserId(newUserId);
	}
}
