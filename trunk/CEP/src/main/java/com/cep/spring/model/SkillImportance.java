package com.cep.spring.model;

import java.io.Serializable;

/**
 * Skill importance level in decimal value for verbal, math and
 * science/technical.
 *
 */
public class SkillImportance implements Serializable {

	private static final long serialVersionUID = 3195956015722998580L;
	private double verbal, math, sciTech;

	public double getVerbal() {
		return verbal;
	}

	public void setVerbal(double verbal) {
		this.verbal = verbal;
	}

	public double getMath() {
		return math;
	}

	public void setMath(double math) {
		this.math = math;
	}

	public double getSciTech() {
		return sciTech;
	}

	public void setSciTech(double sciTech) {
		this.sciTech = sciTech;
	}

}
