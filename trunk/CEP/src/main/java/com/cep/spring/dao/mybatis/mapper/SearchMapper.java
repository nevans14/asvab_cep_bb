package com.cep.spring.dao.mybatis.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Param;

import com.cep.spring.model.search.Search;

public interface SearchMapper {

	ArrayList<Search> getFileName(@Param("searchString") String searchString);
}
