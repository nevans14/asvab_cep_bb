package com.cep.spring.model.PlanYourFuture;

import java.io.Serializable;

public class UserMilitaryPlanServiceCollege implements Serializable {
    private static final long serialVersionUID = 1566490257678754318L;

    private int id, militaryPlanId, unitId;
    private short collegeTypeId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMilitaryPlanId() {
        return militaryPlanId;
    }

    public void setMilitaryPlanId(int militaryPlanId) {
        this.militaryPlanId = militaryPlanId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public short getCollegeTypeId() {
        return collegeTypeId;
    }

    public void setCollegeTypeId(short collegeTypeId) {
        this.collegeTypeId = collegeTypeId;
    }
}
