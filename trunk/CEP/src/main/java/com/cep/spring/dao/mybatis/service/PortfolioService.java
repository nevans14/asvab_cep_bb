package com.cep.spring.dao.mybatis.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cep.spring.dao.mybatis.mapper.PortfolioMapper;
import com.cep.spring.model.portfolio.Achievement;
import com.cep.spring.model.portfolio.ActScore;
import com.cep.spring.model.portfolio.AsvabScore;
import com.cep.spring.model.portfolio.Education;
import com.cep.spring.model.portfolio.FavoriteOccupation;
import com.cep.spring.model.portfolio.FyiScore;
import com.cep.spring.model.portfolio.Interest;
import com.cep.spring.model.portfolio.OtherScore;
import com.cep.spring.model.portfolio.Plan;
import com.cep.spring.model.portfolio.SatScore;
import com.cep.spring.model.portfolio.Skill;
import com.cep.spring.model.portfolio.UserInfo;
import com.cep.spring.model.portfolio.Volunteer;
import com.cep.spring.model.portfolio.WorkExperience;
import com.cep.spring.model.portfolio.WorkValue;

@Service
public class PortfolioService {

	private final PortfolioMapper mapper;

	@Autowired
	public PortfolioService(PortfolioMapper mapper) {
		this.mapper = mapper;
	}

	public List<String> isPortfolioStarted(Integer userId) {
		return mapper.isPortfolioStarted(userId);
	}

	public ArrayList<WorkExperience> getWorkExperience(Integer userId) {
		return mapper.getWorkExperience(userId);
	}

	@Transactional
	public int insertWorkExperience(WorkExperience workExperienceObject, Integer userId) {
		return mapper.insertWorkExperience(workExperienceObject, userId);
	}

	@Transactional
	public int deleteWorkExperience(Integer userId, Integer id) {
		return mapper.deleteWorkExperience(userId, id);
	}

	@Transactional
	public int updateWorkExperience(WorkExperience workExperienceObject, Integer userId) {
		return mapper.updateWorkExperience(workExperienceObject, userId);
	}

	public ArrayList<Education> getEducation(Integer userId) {
		return mapper.getEducation(userId);
	}

	@Transactional
	public int insertEducation(Education educationObject, Integer userId) {
		return mapper.insertEducation(educationObject, userId);
	}

	@Transactional
	public int deleteEducation(Integer userId, Integer id) {
		return mapper.deleteEducation(userId, id);
	}

	@Transactional
	public int updateEducation(Education educationObject, Integer userId) {
		return mapper.updateEducation(educationObject, userId);
	}

	public ArrayList<Achievement> getAchievements(Integer userId) {
		return mapper.getAchievements(userId);
	}

	@Transactional
	public int insertAchievement(Achievement achievementObject, Integer userId) {
		return mapper.insertAchievement(achievementObject, userId);
	}

	@Transactional
	public int deleteAchievement(Integer userId, Integer id) {
		return mapper.deleteAchievement(userId, id);
	}

	@Transactional
	public int updateAchievement(Achievement achievementObject, Integer userId) {
		return mapper.updateAchievement(achievementObject, userId);
	}

	public ArrayList<Interest> getInterests(Integer userId) {
		return mapper.getInterests(userId);
	}

	@Transactional
	public int insertInterest(Interest interestObject, Integer userId) {
		return mapper.insertInterest(interestObject, userId);
	}

	@Transactional
	public int deleteInterest(Integer userId, Integer id) {
		return mapper.deleteInterest(userId, id);
	}

	@Transactional
	public int updateInterest(Interest interestObject, Integer userId) {
		return mapper.updateInterest(interestObject, userId);
	}

	public ArrayList<Skill> getSkills(Integer userId) {
		return mapper.getSkills(userId);
	}

	@Transactional
	public int insertSkill(Skill skillObject, Integer userId) {
		return mapper.insertSkill(skillObject, userId);
	}

	@Transactional
	public int deleteSkill(Integer userId, Integer id) {
		return mapper.deleteSkill(userId, id);
	}

	@Transactional
	public int updateSkill(Skill skillObject, Integer userId) {
		return mapper.updateSkill(skillObject, userId);
	}

	public ArrayList<WorkValue> getWorkValues(Integer userId) {
		return mapper.getWorkValues(userId);
	}

	@Transactional
	public int insertWorkValue(WorkValue workValueObject, Integer userId) {
		return mapper.insertWorkValue(workValueObject, userId);
	}

	@Transactional
	public int deleteWorkValue(Integer userId, Integer id) {
		return mapper.deleteWorkValue(userId, id);
	}

	@Transactional
	public int updateWorkValue(WorkValue workValueObject, Integer userId) {
		return mapper.updateWorkValue(workValueObject, userId);
	}

	public ArrayList<Volunteer> getVolunteers(Integer userId) {
		return mapper.getVolunteers(userId);
	}

	@Transactional
	public int insertVolunteer(Volunteer volunteerObject, Integer userId) {
		return mapper.insertVolunteer(volunteerObject, userId);
	}

	@Transactional
	public int deleteVolunteer(Integer userId, Integer id) {
		return mapper.deleteVolunteer(userId, id);
	}

	@Transactional
	public int updateVolunteer(Volunteer volunteerObject, Integer userId) {
		return mapper.updateVolunteer(volunteerObject, userId);
	}

	public ArrayList<AsvabScore> getAsvabScore(Integer userId) {
		return mapper.getAsvabScore(userId);
	}

	@Transactional
	public int insertAsvabScore(AsvabScore asvabObject, Integer userId) {
		return mapper.insertAsvabScore(asvabObject, userId);
	}

	@Transactional
	public int deleteAsvabScore(Integer userId, Integer id) {
		return mapper.deleteAsvabScore(userId, id);
	}

	@Transactional
	public int updateAsvabScore(AsvabScore asvabObject, Integer userId) {
		return mapper.updateAsvabScore(asvabObject, userId);
	}

	public ArrayList<ActScore> getActScore(Integer userId) {
		return mapper.getActScore(userId);
	}

	@Transactional
	public int insertActScore(ActScore actObject, Integer userId) {
		return mapper.insertActScore(actObject, userId);
	}

	@Transactional
	public int deleteActScore(Integer userId, Integer id) {
		return mapper.deleteActScore(userId, id);
	}

	@Transactional
	public int updateActScore(ActScore actObject, Integer userId) {
		return mapper.updateActScore(actObject, userId);
	}

	public ArrayList<SatScore> getSatScore(Integer userId) {
		return mapper.getSatScore(userId);
	}

	@Transactional
	public int insertSatScore(SatScore satObject, Integer userId) {
		return mapper.insertSatScore(satObject, userId);
	}

	@Transactional
	public int deleteSatScore(Integer userId, Integer id) {
		return mapper.deleteSatScore(userId, id);
	}

	@Transactional
	public int updateSatScore(SatScore satObject, Integer userId) {
		return mapper.updateSatScore(satObject, userId);
	}

	public ArrayList<FyiScore> getFyiScore(Integer userId) {
		return mapper.getFyiScore(userId);
	}

	@Transactional
	public int insertFyiScore(FyiScore fyiObject, Integer userId) {
		return mapper.insertFyiScore(fyiObject, userId);
	}

	@Transactional
	public int deleteFyiScore(Integer userId, Integer id) {
		return mapper.deleteFyiScore(userId, id);
	}

	@Transactional
	public int updateFyiScore(FyiScore fyiObject, Integer userId) {
		return mapper.updateFyiScore(fyiObject, userId);
	}

	public ArrayList<OtherScore> getOtherScore(Integer userId) {
		return mapper.getOtherScore(userId);
	}

	@Transactional
	public int insertOtherScore(OtherScore otherObject, Integer userId) {
		return mapper.insertOtherScore(otherObject, userId);
	}

	@Transactional
	public int deleteOtherScore(Integer userId, Integer id) {
		return mapper.deleteOtherScore(userId, id);
	}

	@Transactional
	public int updateOtherScore(OtherScore otherObject, Integer userId) {
		return mapper.updateOtherScore(otherObject, userId);
	}

	public ArrayList<Plan> getPlan(Integer userId) {
		return mapper.getPlan(userId);
	}

	@Transactional
	public int insertPlan(Plan planObject, Integer userId) {
		return mapper.insertPlan(planObject, userId);
	}

	@Transactional
	public int deletePlan(Integer userId, Integer id) {
		return mapper.deletePlan(userId, id);
	}

	@Transactional
	public int updatePlan(Plan planObject, Integer userId) {
		return mapper.updatePlan(planObject, userId);
	}

	public ArrayList<UserInfo> selectNameGrade(Integer userId) {
		return mapper.selectNameGrade(userId);
	}

	@Transactional
	public int updateInsertNameGrade(UserInfo userInfo, Integer userId) {
		return mapper.updateInsertNameGrade(userInfo, userId);
	}
	
	public ArrayList<FavoriteOccupation> getFavoriteOccupation(Integer userId) {
		return mapper.getFavoriteOccupation(userId);
	}
	
	public String getAccessCode(Integer userId) {
		return mapper.getAccessCode(userId);
	}
	
	@Transactional
	public int insertTeacherDemoTaken(Integer userId) {
		return mapper.insertTeacherDemoTaken(userId);
	}

	public int getTeacherDemoTaken(Integer userId) {
		return mapper.getTeacherDemoTaken(userId);
	}

}
