package com.cep.spring.dao.mybatis;

import com.cep.spring.model.CareerPlan.CareerPlanAnswer;
import com.cep.spring.model.CareerPlan.CareerPlanSection;

import java.util.ArrayList;

public interface CareerPlanDAO {

    ArrayList<CareerPlanSection> getItems();

    CareerPlanAnswer getResponses(Integer userId);

    int updateResponses(CareerPlanAnswer model);

}
