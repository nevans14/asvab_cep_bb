package com.cep.spring.dao.mybatis.service;

import com.cep.spring.dao.mybatis.mapper.UserMapper;
import com.cep.spring.model.user.UserCompletion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserMapper mapper;

    @Autowired
    public UserService(UserMapper mapper) {
        this.mapper = mapper;
    }

    public List<UserCompletion> getUserCompletion(int userId) {
        return mapper.getUserCompletion(userId);
    }

    public int updateUserCompletion(int userId, UserCompletion model) {
        int rows = mapper.getUserCompletionByType(userId, model.getCompletionType());
        if (rows <= 0) {
            return mapper.insertUserCompletion(userId, model);
        }
        return mapper.updateUserCompletion(userId, model);
    }

    public int deleteUserCompletion(int userId, String completionType) {
        return mapper.deleteUserCompletion(userId, completionType);
    }
}
