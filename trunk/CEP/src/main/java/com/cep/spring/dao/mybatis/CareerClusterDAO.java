package com.cep.spring.dao.mybatis;

import java.util.ArrayList;

import com.cep.spring.model.*;
import com.cep.spring.model.occufind.OccufindSearch;

public interface CareerClusterDAO {

	ArrayList<CareerCluster> getCareerCluster();

	CareerCluster getCareerClusterById(int ccId);

	ArrayList<CareerClusterOccupation> getCareerClusterOccupation(int ccId);

	ArrayList<OccufindSearch> getCareerClusterOccupations(int ccId);

	SkillImportance getSkillImportance(String onetSoc);

	ArrayList<Task> getTasks(String onetSoc);

	OccupationInterestCode getOccupationInterestCodes(String onetSoc);

	ArrayList<OccupationEducationLevel> getEducationLevel(String onetSoc);

	ArrayList<RelatedOccupation> getRelatedOccupations(String onetSoc);
}
