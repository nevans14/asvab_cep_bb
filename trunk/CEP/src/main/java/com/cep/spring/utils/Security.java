package com.cep.spring.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

@Component
@PropertySource("classpath:application.properties")
public class Security {

    private final Logger logger = LogManager.getLogger();
    @Value("${security.encrypt.key}")
    private String encryptKey;

    public String encrypt(String token, byte[] iv) {
        try {
            // create key, iv, and cipher
            Key aesKey = new SecretKeySpec(encryptKey.getBytes(), "AES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            // encrypt
            cipher.init(Cipher.ENCRYPT_MODE, aesKey, ivParameterSpec);
            // encode
            byte[] encrypted = cipher.doFinal(token.getBytes());
            return new String(Base64.getUrlEncoder().encode(encrypted));
        } catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
                | NoSuchPaddingException | InvalidKeyException e) {
            logger.error("Encryption Error", e);
        } catch (Exception e) {
            logger.error("Error", e);
        }
        return null;
    }

    public String decrypt(String token, byte[] iv) {
        try {
            // create key, iv, and cipher
            Key aesKey = new SecretKeySpec(encryptKey.getBytes(), "AES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            // decrypt
            cipher.init(Cipher.DECRYPT_MODE, aesKey, ivParameterSpec);
            // decode
            byte[] decoded = Base64.getUrlDecoder().decode(token);
            return new String(cipher.doFinal(decoded));
        } catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException
                | InvalidKeyException e) {
            logger.error("Decryption Error", e);
        } catch (Exception e) {
            logger.error("Error", e);
        }
        return null;
    }

    public byte[] generateSalt(int numOfBytes) {
        SecureRandom secureRandom = new SecureRandom();
        return secureRandom.generateSeed(numOfBytes);
    }

    public String getString(byte[] iv) {
        Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        return encoder.encodeToString(iv);
    }

    public byte[] getBytes(String iv) {
        return Base64.getUrlDecoder().decode(iv);
    }
}
