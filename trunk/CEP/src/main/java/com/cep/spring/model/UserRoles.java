package com.cep.spring.model;

public class UserRoles {
	private int userRoleId;
	private String username, role;
	// accessors
	public int getUserRoleId() { return this.userRoleId; }
	public String getUsername() { return this.username; }
	public String getRole() { return this.role; }
	// mutators
	public void setUserRoleId(int userRoleId) { this.userRoleId = userRoleId; }
	public void setUsername(String username) { this.username = username; }
	public void setRole(String role) { this.role = role; }
}