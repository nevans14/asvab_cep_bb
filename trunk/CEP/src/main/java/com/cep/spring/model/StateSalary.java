package com.cep.spring.model;

import java.io.Serializable;

public class StateSalary implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 279254682051506673L;
	private String occupationCd;
	private String state;
	private Integer avgSalary;

	public String getOccupationCd() {
		return occupationCd;
	}

	public void setOccupationCd(String occupationCd) {
		this.occupationCd = occupationCd;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getAvgSalary() {
		return avgSalary;
	}

	public void setAvgSalary(Integer avgSalary) {
		this.avgSalary = avgSalary;
	}
}
