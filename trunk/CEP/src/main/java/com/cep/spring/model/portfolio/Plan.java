package com.cep.spring.model.portfolio;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.Serializable;

public class Plan implements Serializable {

	private static final long serialVersionUID = 5425318940220775606L;
	private Integer id;
	private String plan, description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = Jsoup.clean(plan, Whitelist.basic());
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = Jsoup.clean(description, Whitelist.basic());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((plan == null) ? 0 : plan.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Plan)) {
			return false;
		}
		Plan other = (Plan) obj;
		if (other.description == null) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (plan == null) {
			if (other.plan != null) {
				return false;
			}
		} else if (!plan.equals(other.plan)) {
			return false;
		}
		return true;
	}
}
