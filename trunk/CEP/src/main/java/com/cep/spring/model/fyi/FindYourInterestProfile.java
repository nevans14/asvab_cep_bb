package com.cep.spring.model.fyi;

import java.io.Serializable;
import java.util.ArrayList;

public class FindYourInterestProfile implements Serializable {

	private static final long serialVersionUID = 5943367138818204586L;
	private Integer userId;
	private Integer testId;
	private char gender;
	private ArrayList<FYIAnswers> answers;
	private ArrayList<FYIRawScores> rawScores;

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public ArrayList<FYIAnswers> getAnswers() {
		return answers;
	}

	public void setAnswers(ArrayList<FYIAnswers> answers) {
		this.answers = answers;
	}

	public ArrayList<FYIRawScores> getRawScores() {
		return rawScores;
	}

	public void setRawScores(ArrayList<FYIRawScores> rawScores) {
		this.rawScores = rawScores;
	}

}
