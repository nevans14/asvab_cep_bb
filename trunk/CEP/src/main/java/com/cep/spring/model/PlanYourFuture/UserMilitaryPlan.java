package com.cep.spring.model.PlanYourFuture;

import java.io.Serializable;
import java.util.ArrayList;

public class UserMilitaryPlan implements Serializable {
    private static final long serialVersionUID = -3751173630150099026L;

    private int id, planId;
    private boolean didSelectEnlisted, didSelectOfficer, didSelectArmy, didSelectNavy, didSelectAirForce, didSelectMarineCorps, didSelectCoastGuard, didSelectSpaceForce;
    private Boolean isInterestedInActiveDuty, isInterestedInReserves, isInterestedInNationalGuard, isNotSure;
    private ArrayList<UserMilitaryPlanCareer> selectedCareers;
    private ArrayList<UserMilitaryPlanRotc> rotcPrograms;
    private ArrayList<UserMilitaryPlanServiceCollege> serviceColleges;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public boolean getDidSelectEnlisted() {
        return didSelectEnlisted;
    }

    public void setDidSelectEnlisted(boolean didSelectEnlisted) {
        this.didSelectEnlisted = didSelectEnlisted;
    }

    public boolean getDidSelectOfficer() {
        return didSelectOfficer;
    }

    public void setDidSelectOfficer(boolean didSelectOfficer) {
        this.didSelectOfficer = didSelectOfficer;
    }

    public boolean getDidSelectArmy() {
        return didSelectArmy;
    }

    public void setDidSelectArmy(boolean didSelectArmy) {
        this.didSelectArmy = didSelectArmy;
    }

    public boolean getDidSelectNavy() {
        return didSelectNavy;
    }

    public void setDidSelectNavy(boolean didSelectNavy) {
        this.didSelectNavy = didSelectNavy;
    }

    public boolean getDidSelectAirForce() {
        return didSelectAirForce;
    }

    public void setDidSelectAirForce(boolean didSelectAirForce) {
        this.didSelectAirForce = didSelectAirForce;
    }

    public boolean getDidSelectMarineCorps() {
        return didSelectMarineCorps;
    }

    public void setDidSelectMarineCorps(boolean didSelectMarineCorps) {
        this.didSelectMarineCorps = didSelectMarineCorps;
    }

    public boolean getDidSelectCoastGuard() {
        return didSelectCoastGuard;
    }

    public void setDidSelectCoastGuard(boolean didSelectCoastGuard) {
        this.didSelectCoastGuard = didSelectCoastGuard;
    }

    public boolean getDidSelectSpaceForce() { return didSelectSpaceForce; }

    public void setDidSelectSpaceForce(boolean didSelectSpaceForce) { this.didSelectSpaceForce = didSelectSpaceForce; }

    public Boolean getIsInterestedInActiveDuty() {
        return isInterestedInActiveDuty;
    }

    public void setIsInterestedInActiveDuty(Boolean isInterestedInActiveDuty) {
        this.isInterestedInActiveDuty = isInterestedInActiveDuty;
    }

    public Boolean getIsInterestedInReserves() {
        return isInterestedInReserves;
    }

    public void setIsInterestedInReserves(Boolean isInterestedInReserves) {
        this.isInterestedInReserves = isInterestedInReserves;
    }

    public Boolean getIsInterestedInNationalGuard() {
        return isInterestedInNationalGuard;
    }

    public void setIsInterestedInNationalGuard(Boolean isInterestedInNationalGuard) {
        this.isInterestedInNationalGuard = isInterestedInNationalGuard;
    }

    public Boolean getIsNotSure() {
        return isNotSure;
    }

    public void setIsNotSure(Boolean isNotSure) { this.isNotSure = isNotSure; }

    public ArrayList<UserMilitaryPlanCareer> getSelectedCareers() {
        return selectedCareers;
    }

    public void setSelectedCareers(ArrayList<UserMilitaryPlanCareer> selectedCareers) {
        this.selectedCareers = selectedCareers;
    }

    public ArrayList<UserMilitaryPlanRotc> getRotcPrograms() {
        return rotcPrograms;
    }

    public void setRotcPrograms(ArrayList<UserMilitaryPlanRotc> rotcPrograms) {
        this.rotcPrograms = rotcPrograms;
    }

    public ArrayList<UserMilitaryPlanServiceCollege> getServiceColleges() { return serviceColleges; }

    public void setServiceColleges(ArrayList<UserMilitaryPlanServiceCollege> serviceColleges) { this.serviceColleges = serviceColleges; }
}
