package com.cep.spring.model;

import java.io.Serializable;

public class Abilities implements Serializable {

	private static final long serialVersionUID = 1022252357098192520L;
	String abilityElementId;
	String abilityDescription;

	public String getAbilityElementId() {
		return abilityElementId;
	}

	public void setAbilityElementId(String abilityElementId) {
		this.abilityElementId = abilityElementId;
	}

	public String getAbilityDescription() {
		return abilityDescription;
	}

	public void setAbilityDescription(String abilityDescription) {
		this.abilityDescription = abilityDescription;
	}

}
