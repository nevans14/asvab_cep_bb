package com.cep.spring.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

@Component
@PropertySource("classpath:application.properties")
public class VerifyRecaptcha {

	private final Logger logger = LogManager.getLogger();
	private final JsonApiHelper jsonApiHelper;
	@Value("${security.google.verify.url}")
	private String url;
	@Value("${security.google.verify.key}")
	private String secret;

	@Autowired
	public VerifyRecaptcha(JsonApiHelper jsonApiHelper) {
		this.jsonApiHelper = jsonApiHelper;
	}

	public boolean verify(String gRecaptchaResponse) {
		if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
			return false;
		}
		JsonReader reader = null;
		DataOutputStream writer = null;
		try {
			URL requestUrl = new URL(url);
			HttpsURLConnection connection = (HttpsURLConnection)requestUrl.openConnection();
			connection.setRequestProperty("User-Agent", "");
			connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null,null,null);
			// bypass cert
			connection.setSSLSocketFactory(sslContext.getSocketFactory());
			String postParams = "secret=" + secret + "&response=" + gRecaptchaResponse;
			// Send post request
			connection.setDoOutput(true);
			writer = new DataOutputStream(connection.getOutputStream());
			writer.writeBytes(postParams);
			// get response
			String jsonResponse = jsonApiHelper.sendRequest(connection);
			reader = Json.createReader(new StringReader(jsonResponse));
			JsonObject obj = reader.readObject();
			return obj.getBoolean("success");
		} catch (NoSuchAlgorithmException e) {
			logger.error("NoSuchAlgorithm Error", e);
		} catch (KeyManagementException e) {
			logger.error("KeyManagement Error", e);
		} catch (JsonParsingException e) {
			logger.error("JsonParsing Error", e);
		} catch (JsonException e) {
			logger.error("Json Error", e);
		} catch (IllegalStateException e) {
			logger.error("IllegalState Error", e);
		} catch (IOException e) {
			logger.error("IO Error", e);
		} finally {
			SimpleFileUtils.safeClose(reader);
			SimpleFileUtils.safeClose(writer);
		}
		return false;
	}
}