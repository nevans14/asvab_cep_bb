package com.cep.spring.model.occufind;

import java.io.Serializable;

public class ServiceOfferingCareer implements Serializable {

	private static final long serialVersionUID = -7567006569148946403L;

	private char serviceCode;

	public char getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(char serviceCode) {
		this.serviceCode = serviceCode;
	}
}
