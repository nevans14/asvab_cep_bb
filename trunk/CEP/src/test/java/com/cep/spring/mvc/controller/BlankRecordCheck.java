package com.cep.spring.mvc.controller;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cep.spring.mvc.controller.UserObj;

@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration("classpath:controllerContext-test-prod-db.xml")
public class BlankRecordCheck {
	
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    
    private String encrypt(String password) {
    	return bCryptPasswordEncoder.encode(password);
    }
    
    private boolean comparePasswords(String rawPassword, String encodedPassword) {
    	return bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
    }
    
    @Test
    public void checkForViolators() {
//    	System.out.println(encrypt(""));
    	
//    	System.out.println(comparePasswords("","$2a$10$GemQCcmMaBTfzY5LFneejOsw15A4V5zi8X2.jZJXqmV81kmdjLn6O"));
    	
    	Path path = Paths.get("blankPasswordAccounts.txt");
    	try (
    			BufferedWriter writer = Files.newBufferedWriter(path)) {
    		
		    	List<UserObj> violators = new ArrayList<>();
		    	List<UserObj> users = jdbcTemplate.query("SELECT [user_id], [username], [password] FROM [ASVAB_CEP_2018].[dbo].[users]", 
						new BeanPropertyRowMapper<>(UserObj.class)); 
		    	System.out.println("Table size:"+ users.size());
		    	writer.write("Table size:"+ users.size());
		    	for(UserObj user: users) {
		    		if (comparePasswords("", user.getPassword())) {
		    			violators.add(user);
		    			System.out.println("UserId: "+user.getUserId()+" User name:"+user.getUsername());
		    			writer.write("UserId: "+user.getUserId()+" User name:"+user.getUsername());
		    		}
		    	}
		    	writer.close();

    	violators.forEach(a->{System.out.println("User name:"+a.getUsername());});
    	} catch (IOException e) {
			throw new RuntimeException(e);
		}
    }
}