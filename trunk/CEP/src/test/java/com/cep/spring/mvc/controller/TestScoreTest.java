package com.cep.spring.mvc.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import com.cep.spring.utils.UserManager;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cep.spring.model.testscore.ManualTestScore;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.cep.spring.model.testscore.StudentTestingProgamSelects;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.PropertyAccessor.FIELD;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestScoreTest {

	@Autowired
	TestScoreController testScoreController;
	@Autowired
	UserManager userManager;
	
	@Test
	public void InsertTestScore() {
		Stream<Path> paths = null;
		try {
			paths = Files.list(Paths.get("C:\\workspace\\sandbox\\CEPProject\\CEP\\resource\\"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Iterator<Path> it = paths.iterator();
		List<StudentTestingProgam> stps = new ArrayList<>();
		
		while (it.hasNext()) {
			Path path = (Path) it.next();
			ObjectMapper mapper = new ObjectMapper().setVisibility(FIELD, ANY);
			Iterator<StudentTestingProgam> thingsIterator = null;
			try {
				BufferedReader br = new BufferedReader(new FileReader(new File(path.toString())));  
				String line = null;  
				while ((line = br.readLine()) != null)  
				{	  
				thingsIterator = mapper.reader(StudentTestingProgam.class).readValues(line);

					while (thingsIterator.hasNext()) {
						stps.add(thingsIterator.next());
					}
				}
			} catch (JsonProcessingException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
//			stps.forEach((a) -> System.out.println(a.getStudentTestProgram().getControlInformation().getAccessCode()));
			ResponseEntity<Integer> insertTestScoreStatus = testScoreController.insertTestScore(stps);
			System.out.println("Test Score insert status: " + insertTestScoreStatus.getBody().intValue());			
		}
	}
	
	@Test
	public void SelectTestScoresByAccessCode() {
		userManager.createMock();
		ResponseEntity<StudentTestingProgamSelects> access = testScoreController.getTestScore();
		if (access.getBody().getResource() != null)
			System.out.println("Access: "+access.getBody().getResource());
	}
		
	@Test
	public void SelectTestScoresByUserId() {
		userManager.createMock();
		ResponseEntity<StudentTestingProgamSelects> user = testScoreController.getTestScore();
		if (user.getBody().getResource() != null)
			System.out.println("User: "+user.getBody().getResource());
	}
	
	@Test
	public void InsertManualTestScore() {
		ManualTestScore manualTestScoreObj = new ManualTestScore();
		manualTestScoreObj.setUserId(1234567);
		manualTestScoreObj.setAfqt("12");
		manualTestScoreObj.setMath("13");
		manualTestScoreObj.setScienceTechnology("14");
		manualTestScoreObj.setVerbal("15");
		ResponseEntity<Integer> insertStatus = testScoreController.insertManualTestScore(manualTestScoreObj);
		System.out.println("Manual Score insert status: "+ insertStatus.getBody().intValue());
	}
	
	@Test
	public void SelectManualTestScoreEntry() {
		userManager.setUserId(1234567);
		ResponseEntity<ManualTestScore> manual = testScoreController.getManualTestScore();
		if (manual.getBody().getVerbal() != null)
			System.out.println("Manual: "+manual.getBody().getVerbal());
	}
	
	@Test 
	public void UpdateManualTestScore() {
		ManualTestScore manualTestScoreObjUpdate = new ManualTestScore();
		manualTestScoreObjUpdate.setUserId(1234567);
		manualTestScoreObjUpdate.setAfqt("11");
		manualTestScoreObjUpdate.setMath("11");
		manualTestScoreObjUpdate.setScienceTechnology("11");
		manualTestScoreObjUpdate.setVerbal("11");
		ResponseEntity<Integer> updateStatus = testScoreController.updateManualTestScore(manualTestScoreObjUpdate);
		System.out.println("Manual Score update status: "+ updateStatus.getBody().intValue());
	}
	
	
	@Test
	public void DeleteManualTestScore() {
		userManager.setUserId(1234567);
		testScoreController.deleteManualTestScore();
	}
}
