package com.cep.spring.mvc.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import com.cep.spring.model.testscore.RecordCount;
import com.cep.spring.model.testscore.StudentTestingProgam;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class jsonCheck {
 
	
	public static void main(String[] args) {
		ObjectMapper objectMapper = new ObjectMapper();

//		String jsonInString = "[{\"studentTestProgram\":{\"controlInformation\":{\"accessCode\":\"9999999A\",\"platform\":\"ICAT\",\"ansSeqNum\":null,\"mepsId\":\"390275580\",\"metSiteId\":\"176654\",\"gender\":\"M\",\"educationLevel\":\"12\",\"certification\":null,\"testForm\":\"03E\",\"scoringMethod\":null,\"administeredYear\":\"2018\",\"administeredMonth\":\"12\",\"administeredDay\":\"4\"},\"sSASVABCombineControl\":{\"seqNum\":\"13\"},\"aFQTRawSSComposites\":{\"testCompletionCd\":\"0\",\"gs\":\"64\",\"ar\":\"64\",\"wk\":\"48\",\"pc\":\"50\",\"as\":\"56\",\"mk\":\"58\",\"mc\":\"66\",\"el\":\"69\",\"ve\":\"49\",\"ao\":\"61\",\"afqt\":\"70\",\"gt_army\":\"114\",\"cl_army\":\"118\",\"co_army\":\"125\",\"el_army\":\"123\",\"fa_army\":\"125\",\"gm_army\":\"127\",\"mm_army\":\"125\",\"of_army\":\"125\",\"sc_army\":\"122\",\"st_army\":\"121\",\"gt_navy\":\"113\",\"el_navy\":\"255\",\"bee_navy\":\"244\",\"eng_navy\":\"114\",\"mec_navy\":\"186\",\"mec2_navy\":\"191\",\"nuc_navy\":\"237\",\"ops_navy\":\"232\",\"hm_navy\":\"171\",\"adm_navy\":\"107\",\"m_af\":\"79\",\"a_af\":\"63\",\"g_af\":\"76\",\"e_af\":\"95\",\"mm_mc\":\"132\",\"gt_mc\":\"122\",\"el_mc\":\"132\",\"cl_mc\":\"108\",\"bookletNum\":null},\"highSchoolInformation\":{\"schoolCode\":\"390275580\",\"schoolSessionNbr\":\"575884\",\"schoolSpecialInstructions\":null,\"releaseToServiceDate\":null,\"schoolCouncellorCd\":null,\"schoolSiteName\":\"LAUREL OAKS CDC (CLINTON)\",\"schoolCityName\":\"WILMINGTON\",\"schoolStateName\":\"OH\",\"schoolZipCode\":\"45177\"},\"studentInformation\":{\"studentName\":\"BARGER, CASEY A\",\"lastName\":\"BARGER\",\"firstName\":\"CASEY\",\"middleInitial\":\"A\",\"studentCity\":\"hillsboro\",\"studentState\":\"OH\",\"studentZipCode\":\"45133\",\"studentIntentions\":\"1\"},\"studentResultSheetScores\":{\"milCareerScore\":\"1\",\"milCareerScoreCategory\":\"12\"},\"verbalAbility\":{\"va_TP_Percentage\":\"44\",\"va_GS_Percentage\":\"51\",\"va_GOS_Percentage\":\"56\",\"va_COMP_Percentage\":\"54\",\"va_YP_Score\":\"49\",\"va_SGS\":\"52\",\"va_USL\":\"55\",\"va_LSL\":\"49\"},\"mathematicalAbility\":{\"ma_TP_Percentage\":\"80\",\"ma_GS_Percentage\":\"84\",\"ma_GOS_Percentage\":\"89\",\"ma_COMP_Percentage\":\"87\",\"ma_YP_Score\":\"62\",\"ma_SGS\":\"62\",\"ma_USL\":\"64\",\"ma_LSL\":\"60\"},\"scienceTechnicalAbility\":{\"tec_TP_Percentage\":\"80\",\"tec_GS_Percentage\":\"96\",\"tec_GOS_Percentage\":\"99\",\"tec_COMP_Percentage\":\"98\",\"tec_YP_Score\":\"69\",\"tec_SGS\":\"70\",\"tec_USL\":\"73\",\"tec_LSL\":\"67\"},\"generalScience\":{\"gs_TP_Percentage\":\"80\",\"gs_GS_Percentage\":\"91\",\"gs_GOS_Percentage\":\"98\",\"gs_COMP_Percentage\":\"94\",\"gs_YP_Score\":\"64\",\"gs_SGS\":\"65\",\"gs_USL\":\"69\",\"gs_LSL\":\"61\"},\"arithmeticReasoning\":{\"ar_TP_Percentage\":\"80\",\"ar_GS_Percentage\":\"91\",\"ar_GOS_Percentage\":\"96\",\"ar_COMP_Percentage\":\"93\",\"ar_YP_Score\":\"64\",\"ar_SGS\":\"64\",\"ar_USL\":\"67\",\"ar_LSL\":\"61\"},\"wordKnowledge\":{\"wk_TP_Percentage\":\"43\",\"wk_GS_Percentage\":\"46\",\"wk_GOS_Percentage\":\"55\",\"wk_COMP_Percentage\":\"50\",\"wk_YP_Score\":\"48\",\"wk_SGS\":\"51\",\"wk_USL\":\"54\",\"wk_LSL\":\"48\"},\"paragraphComprehension\":{\"pc_TP_Percentage\":\"45\",\"pc_GS_Percentage\":\"53\",\"pc_GOS_Percentage\":\"53\",\"pc_COMP_Percentage\":\"53\",\"pc_YP_Score\":\"50\",\"pc_SGS\":\"52\",\"pc_USL\":\"57\",\"pc_LSL\":\"47\"},\"mathematicsKnowledge\":{\"mk_TP_Percentage\":\"77\",\"mk_GS_Percentage\":\"71\",\"mk_GOS_Percentage\":\"75\",\"mk_COMP_Percentage\":\"73\",\"mk_YP_Score\":\"58\",\"mk_SGS\":\"57\",\"mk_USL\":\"60\",\"mk_LSL\":\"54\"},\"electronicsInformation\":{\"ei_TP_Percentage\":\"80\",\"ei_GS_Percentage\":\"95\",\"ei_GOS_Percentage\":\"99\",\"ei_COMP_Percentage\":\"98\",\"ei_YP_Score\":\"69\",\"ei_SGS\":\"72\",\"ei_USL\":\"77\",\"ei_LSL\":\"67\"},\"autoShopInformation\":{\"as_TP_Percentage\":\"75\",\"as_GS_Percentage\":\"76\",\"as_GOS_Percentage\":\"99\",\"as_COMP_Percentage\":\"87\",\"as_YP_Score\":\"56\",\"as_SGS\":\"61\",\"as_USL\":\"65\",\"as_LSL\":\"57\"},\"mechanicalComprehension\":{\"mc_TP_Percentage\":\"80\",\"mc_GS_Percentage\":\"94\",\"mc_GOS_Percentage\":\"99\",\"mc_COMP_Percentage\":\"97\",\"mc_YP_Score\":\"66\",\"mc_SGS\":\"68\",\"mc_USL\":\"72\",\"mc_LSL\":\"64\"},\"assemblingObjects\":{\"ao_TP_Percentage\":\"80\",\"ao_GS_Percentage\":\"86\",\"ao_GOS_Percentage\":\"92\",\"ao_COMP_Percentage\":\"89\",\"ao_YP_Score\":\"61\",\"ao_SGS\":\"62\",\"ao_USL\":\"65\",\"ao_LSL\":\"59\"},\"raceFields\":{\"americanIndian\":\"0\",\"asian\":\"0\",\"africanAmerican\":\"0\",\"nativeHawiian\":\"0\",\"white\":\"1\",\"reserved\":\"0\",\"hispanic\":\"0\",\"notHispanic\":\"1\"},\"resource\":\"20181204\"}}]";
		String stpTypeCount = "{\"fileName\": 20190104.bak, \"count\": \"65\"},{\"fileName\": 20190102.bak, \"count\": \"147\"}";
		stpTypeCount = "["+stpTypeCount.replaceAll("\"fileName\": ", Matcher.quoteReplacement("\"fileName\": \"")).replaceAll(", \"count\"", Matcher.quoteReplacement("\", \"count\"")).replaceAll(".bak","")+"]";

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDate localDate = LocalDate.now();
		dtf.format(localDate);
		try {

//		    List<StudentTestingProgam> navigation = objectMapper.readValue(jsonInString,objectMapper.getTypeFactory().constructCollectionType(List.class, StudentTestingProgam.class));
//			navigation.forEach(a-> System.out.println(a.getStudentTestProgram().getStudentInformation().getFirstName()));

			List<RecordCount> pnpRecordCount = objectMapper.readValue(stpTypeCount,objectMapper.getTypeFactory().constructCollectionType(List.class, RecordCount.class));
			for(RecordCount fileName: pnpRecordCount) {
				if(dtf.format(localDate).toString().equals(fileName.getFileName()))
					System.out.println(fileName.getCount());
			}
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		System.out.println(stpTypeCount.substring(stpTypeCount.lastIndexOf(',')).replaceAll("}", "").trim());
	}
}
