package com.cep.spring.utils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.text.html.HTMLEditorKit;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class ParseCdata extends HTMLEditorKit.ParserCallback  {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	int id = 1;
	
	@Test
	public void loadData() throws Exception {
		String socId = null;

		Map<String, DataSet> dataSetList = new TreeMap<>();
		
		File file = new File("data.xml");
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(file);

		NodeList ohhNodes = doc.getElementsByTagName("ooh");
		for (int a = 0; a < ohhNodes.getLength(); a++) {
			Element ohhNode = (Element) ohhNodes.item(a);
			NodeList occupationNodes = ohhNode.getElementsByTagName("occupation");
			for (int b = 0; b < occupationNodes.getLength(); b++) {
				Element occupationNode = (Element) occupationNodes.item(b);
				NodeList socCoverageNodes = occupationNode.getElementsByTagName("soc_coverage");
				Element socCoverageNode = (Element) socCoverageNodes.item(0);
				NodeList socCodeNode = socCoverageNode.getElementsByTagName("soc_code");
				NodeList whatTheyDoNodes = occupationNode.getElementsByTagName("how_to_become_one");

				for (int c = 0; c < whatTheyDoNodes.getLength(); c++) {
					Element whatTheyDoNode = (Element) whatTheyDoNodes.item(c);
					NodeList sectionBodyNodes = whatTheyDoNode.getElementsByTagName("section_body");
					Element sectionBodyNode = (Element) sectionBodyNodes.item(0);

					org.jsoup.nodes.Document cData = Jsoup.parse(getCharacterDataFromElement(sectionBodyNode));
					Elements cDataElements = cData.getAllElements();

					Elements headings = cDataElements.get(0).select("h3");

					for (org.jsoup.nodes.Element heading : headings) {
						if (heading.text().matches("Licenses, Certifications, and Registrations") || heading.text().matches("Training")) {

							if (socCodeNode.item(0) != null) {
								for (int d = 0; d < socCodeNode.getLength(); d++) {
									String newSocId = socCodeNode.item(d).getChildNodes().item(0).getNodeValue();
									List<String>socList = getOnetList(newSocId+"%");
									if(socList == null)
										socList = Arrays.asList(socId+".00");
									DataSet dataSet = new ParseCdata().new DataSet();
									if(socId != newSocId)
									for (String soc:socList){
										Elements paragraphs = heading.getAllElements().nextAll();
										
										if (paragraphs != null)
											for (org.jsoup.nodes.Element paragraph : paragraphs) {
												if (!headings.contains(paragraph)) {
													if (dataSetList.containsKey(soc))
														dataSet = dataSetList.get(soc);
													if(heading.text().matches("Licenses, Certifications, and Registrations")) {
														dataSet.setLicensing(paragraph.outerHtml().replaceAll("'","''"));
													} else {
														dataSet.setTraining(paragraph.outerHtml().replaceAll("'","''"));
													}
												}
												else
													break;
											}
										dataSetList.put(soc, dataSet);
									}
									socId = newSocId;
								}
							}
						}
					}
				}
			}
		}
		dataSetList.forEach((a, b)-> insertData(a, b));
	}
    
	private String getCharacterDataFromElement(Element e) {
    NodeList children = e.getChildNodes();
    for (int d = 0; d < children.getLength(); d++) {
	    if (children.item(d) instanceof CDATASection) {
	    	CDATASection cd = (CDATASection) children.item(d);
	      return cd.getData();
	    }
    }
    return "";
  }
  
  private void insertData(String a, DataSet b) {
      String insertSql = "INSERT INTO [ASVAB_HUMRRO_ONET_2018].[dbo].[IPEDS_EDU_CERT_EXPLANATIONS]([id],[career_id],[licensing],[training]) VALUES( " +
      		id++ +",'"+a+"', '"+ b.getLicensing()+"', '"+ b.getTraining() + "')";
      jdbcTemplate.execute(insertSql);
  }
  
  private List<String> getOnetList(String socId){
		String sql = "SELECT [onetsoc_code] " +
				" FROM [ASVAB_HUMRRO_ONET_2018].[dbo].[occupation_data_Avishek] " +
				" WHERE [onetsoc_code] like  ?";
		return jdbcTemplate.queryForList(sql, String.class, socId);
  }
  
  private class DataSet {
	  	
		private String licensing = "";
		private String training = "";

		public String getLicensing() {
			return licensing;
		}
		public void setLicensing(String licensing) {
			this.licensing = licensing;
		}
		public String getTraining() {
			return training;
		}
		public void setTraining(String paragraph) {
			this.training = paragraph;
		}
  }
  
}
