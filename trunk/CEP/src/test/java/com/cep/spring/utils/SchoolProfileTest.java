package com.cep.spring.utils;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cep.spring.model.occufind.SchoolProfile;
import com.cep.spring.mvc.controller.OccufindRestController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class SchoolProfileTest {

	@Autowired
	OccufindRestController controller;
	
	@Test
	public void getProfile() {
		ResponseEntity<SchoolProfile> results = controller.getSchoolProfile(218539);
		System.out.println(results.getBody().getCollegeMajors());
		ResponseEntity<List<String>> profiles = controller.getCareerMajors("13-2011.00");
		System.out.println(profiles.getBody());
	}
}
