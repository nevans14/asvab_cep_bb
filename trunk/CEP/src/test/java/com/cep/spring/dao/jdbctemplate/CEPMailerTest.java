package com.cep.spring.dao.jdbctemplate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cep.spring.CEPMailer;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class CEPMailerTest {
	
	@Autowired
	CEPMailer mailer;
	
	@Autowired 
	JdbcTemplate jdbcTemplate;
	
	
	@Test
	public void simpleMailerTest() {
		mailer.sendEmailFromCep("ceptsproject@gmail.com", "Maus Haus is awesome","Subject" );
	}
	
	
	

}
