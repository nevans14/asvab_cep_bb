package com.cep.spring.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cep.spring.dao.jdbctemplate.GenericRowMapper;



@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration("classpath:noredisContext.xml")
// @Transactional
public class GenerateAccessCodes_PROD_2018 {
    private static     String outputDir = "C:/eclipse/access_codes/";
    @Autowired 
    DataSource dataSource;
 
    
    
 @Test
// @Ignore
 public void test500MarketingCodesForArmyProd2018(String prefix_name, String postfix_name, String type, int count, String creator) {
     Set armyMarketingCodes = new GenerateAccessCodes().generateCustomMarketingCodes(prefix_name.trim(), postfix_name.trim(), count, type, creator, dataSource);
     
     // Write to Excel sheet ...
     writeFiles(count, prefix_name, new ArrayList<String>(armyMarketingCodes) );
    
     // Write as insert statements ....
     writeFilesMInserts(new ArrayList<String>(armyMarketingCodes), prefix_name, type); 
     
 }    
    
 
 public static void main(String[] args) {
	new GenerateAccessCodes_PROD_2018().test500MarketingCodesForArmyProd2018("","", "M", 100, "Avishek");
}
 @Test
 @Ignore
 public void testMarketingCodesForProd2018() {
     JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
     
 
     List<String> myMarketingCodes = jdbcTemplate.queryForList("select top(43500) access_code from access_codes where role='M' and expire_date is null", String.class);
     System.out.println("*** testCodesFor2017!!! Codes ***");       

     List<String> n_marketing = new ArrayList<>(); 
     List<String> t_marketing = new ArrayList<>(); 
     List<String> aa_marketing = new ArrayList<>(); 
     
     
     int counter = 0; 
     for ( String accessCode : myMarketingCodes ){
         
         if ( counter < 21600 ) {
             n_marketing.add(accessCode);
         }
         if ( counter >= 21600 && counter < (21600 + 200) ) {
             t_marketing.add(accessCode);
             
         }
         
     if ( counter >= (21600 + 200) && counter < (21600 +  200 + 21600) ) {
           aa_marketing.add(accessCode);
             
         }
     
         counter++;
     }
   
     writeFiles(3600, "n_marketing", n_marketing);
     writeFiles(200,"t_marketing", t_marketing);
     writeFiles(3600,"aa_marketing", aa_marketing);

     // Rec
     Set<String> recSet = GenerateAccessCodes.generateMarketingCodes(2300);
     List<String> recList = new ArrayList<>();
     for( String code : recSet) {
         recList.add(code + "REC");
     }
     
     List<String> r_marketing = new ArrayList<>(); 
     List<String> x_marketing = new ArrayList<>(); 
     List<String> ee_marketing = new ArrayList<>(); 
     int codeCounter = 0 ;
     for( String code : recList) {
         if (  codeCounter < 900 ) {
             r_marketing.add(code);
         }
         if (  codeCounter >= 900 &&  codeCounter < 900+ 500 ) {
             x_marketing.add(code);
         }
         if (  codeCounter >= 900 + 500  ) {
             ee_marketing.add(code);
         }

         codeCounter++;
     }
     
         writeFilesMInserts(recList, "rec_Marketing", "M"); 
         writeFiles(150,"r_marketing", r_marketing);
         writeFiles(100,"x_marketing", x_marketing);
         writeFiles(150,"ee_marketing", ee_marketing);
     
         customAccessCodes("ACTE", 200, "hh_marketing", "M"); 
         customAccessCodes("ASCA", 200, "ii_marketing","M"); 
         customAccessCodes("NAPCSM", 200, "jj_marketing","M"); 
         customAccessCodes("NPCM", 200, "ll_marketing","M"); 
         
     }

 
 @Test
 public void testAdminCodesESSForProd2018() {
     // Rec
     Set<String> recSet = GenerateAccessCodes.generateMarketingCodes(226);
     List<String> recList = new ArrayList<>();
     for( String code : recSet) {
         recList.add(code + "ESS");
     }
     
     List<String> o_marketing = new ArrayList<>(); 
     List<String> u_marketing = new ArrayList<>(); 
     List<String> z_marketing = new ArrayList<>(); 
     List<String> bb_marketing = new ArrayList<>(); 
     
     int codeCounter = 0 ;
     for( String code : recList) {
         if (  codeCounter < 72 ) {
             o_marketing.add(code);
         }
         if (  codeCounter >= 72 &&  codeCounter < 72+ 12 ) {
             u_marketing.add(code);
         }
         if (  codeCounter >= 72+ 12 &&  codeCounter < 72+ 12 + 70 ) {
             z_marketing.add(code);
         }
         if (  codeCounter >= 72+ 12 + 70  ) {
             bb_marketing.add(code);
         }

         codeCounter++;
     }     
     writeFilesMInserts(recList, "ess_Admin", "A"); 
     writeFiles(12,"o_admin", o_marketing);
     writeFiles(12,"u_admin", u_marketing);
     writeFiles(70,"z_admin", z_marketing);
     writeFiles(12,"bb_admin", bb_marketing);
     
 }
 @Test 
 //@Ignore  
 public void testAdminCodesCEPCForProd2018() {
     // Rec
     Set<String> recSet = GenerateAccessCodes.generateMarketingCodes(100);
     List<String> recList = new ArrayList<>();
     for( String code : recSet) {
         recList.add(code + "CEPC");
     }
     
     writeFilesMInserts(recList, "cepc_Admin", "A"); 
     writeFiles(100,"mm_admin", recList);     
 }
 
    public void customAccessCodes(String suffix, Integer count, String fileName, String role) {
        Set<String> recSet = GenerateAccessCodes.generateMarketingCodes(count);
        List<String> recList = new ArrayList<>();
        for( String code : recSet) {
            recList.add(code + suffix);
        }
        writeFiles(count, fileName, recList);
        writeFilesMInserts(recList,  fileName, role) ;
    }
 
    public static void writeFiles(Integer fileSize, String fileName, List<String> codes) {

        int fileNumber = 1;

        List<String> tempList = new ArrayList<>();
        for (int i = 1; i <= codes.size() ; i++) {
            tempList.add(codes.get(i - 1));
            if (i % fileSize == 0) {
//                WriteExcelDemo.writeExcelDocFromList(
//                        outputDir + "/" + fileName + "." + Integer.toString(fileNumber).trim() + ".xlsx",
//                        (ArrayList<String>) tempList);
                fileNumber++;
                tempList.clear();
                tempList = null;
                tempList = new ArrayList<>();
            }

        }
    }
    
    
    @Test
    public void testReserveCodesCEPCForProd2018() {
       Set<String> ss = GenerateAccessCodes.generateReservedCodes(21800);
       List<String> p_reserve = new ArrayList<>(); 
       List<String> v_reserve = new ArrayList<>(); 
       List<String> cc_reserve = new ArrayList<>(); 
       
       int codeCounter = 0 ;
       
       for ( String code : ss ) {
           if (  codeCounter < 10800 ) {
               p_reserve.add(code);
           }
           if (  codeCounter >= 10800 &&  codeCounter < 10800 + 200 ) {
               v_reserve.add(code);
           }
           if (  codeCounter >= 10800 + 200 ) {
               cc_reserve.add(code);
           }
           codeCounter++;
       }
       List<String> recList = new ArrayList<>();
       recList.addAll(ss);
       writeFilesMInserts( recList , "reserveCodes", "R"); 
       writeFiles(1800,"p_reserve", p_reserve );
       writeFiles(200,"v_reserve", v_reserve );
       writeFiles(1800,"cc_reserve", cc_reserve );
       
       Set<String> sCert = GenerateAccessCodes.generateReservedCodes(1250);
       List<String> certList = new ArrayList<>();
       List<String> s_reserve = new ArrayList<>(); 
       List<String> y_reserve = new ArrayList<>(); 
       List<String> ff_reserve = new ArrayList<>(); 
       codeCounter = 0 ;
       for ( String code : sCert) {
           certList.add(code + "CERT");
       }
       for ( String code : certList ) {
           if (  codeCounter < 600 ) {
               s_reserve.add(code);
           }
           if (  codeCounter >= 600 &&  codeCounter < 600 + 50 ) {
               y_reserve.add(code);
           }
           if (  codeCounter >= 600 + 50 ) {
               ff_reserve.add(code);
           }
           codeCounter++;
       }
       
       writeFilesMInserts( certList , "reserveCodes-Cert", "R"); 
       writeFiles(100 ,"s_reserve", s_reserve );
       writeFiles(50 ,"y_reserve", y_reserve );
       writeFiles(100 ,"ff_reserve", ff_reserve );
       
       Set<String> sNetc = GenerateAccessCodes.generateReservedCodes(50);
       List<String> netcList = new ArrayList<>();
       List<String> kk_reserve = new ArrayList<>(); 
       for ( String code : sNetc) {
           netcList.add(code + "NETC");
       }
       writeFilesMInserts( netcList , "reserveCodes-Cert-NETC", "R"); 
       writeFiles(50 ,"kk_reserve", netcList );
       
    }
    
    @Test
    public void testCounselorCodesCEPCForProd2018() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        
        
        List<String> myCounselorCodes = jdbcTemplate.queryForList("SELECT TOP(5200) access_code FROM access_codes where role = 'C' and expire_date is null ", String.class);
        System.out.println("*** testCodesFor2017!!! Codes ***");       
        
        List<String> q_counselor = new ArrayList<>(); 
        List<String> w_counselor = new ArrayList<>(); 
        List<String> dd_counselor = new ArrayList<>(); 
        
    
    int codeCounter = 0 ;
    for ( String code : myCounselorCodes ) {
        if (  codeCounter < 2400 ) {
            q_counselor.add(code);
        }
        if (  codeCounter >= 2400 &&  codeCounter < 2400 + 400 ) {
            w_counselor.add(code);
        }
        if (  codeCounter >= 2400 + 400 ) {
            dd_counselor.add(code);
        }
        codeCounter++;
    }
    
    
    writeFiles(400 ,"q_counselor", q_counselor );
    writeFiles(400 ,"w_counselor", w_counselor );
    writeFiles(400 ,"dd_counselor", dd_counselor );
        
    }
    public static void writeFilesMInserts(List<String> codes, String fileName,String role) {

        for ( String code : codes ){
            SimpleFileUtils.appendStringToFile(outputDir + "/" + fileName + ".sql", 
            "INSERT INTO [dbo].[access_codes] ([access_code],[role]) VALUES ('" + code.trim() + "' , '"+ role.trim() + "'); \n"
            );
        }
    }
        
        
    @Ignore 	@Test
	public void testGenerateThreeWeekCodes() {
		System.out.println("*** Three Week Codes ***");
		Set<String> ls = GenerateAccessCodes.generateThreeWeekCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}
	
    @Ignore 	@Test
	public void testCounselorCodes() {
		System.out.println("*** Counselor Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateCounselorCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}		
	}

    @Ignore 	@Test
	public void testMarketingCodes() {
		System.out.println("*** Marketing Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateMarketingCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}
	
    @Ignore 	@Test
	public void testReservedCodes() {
		System.out.println("*** Reserved Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateReservedCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}

	
    
    /** Given todays date determine what the Modulus year would be.
     * 
     * Modulus 0 SchoolYear2016 = "F,G,H,J,K,L"
     * Modulus 1 SchoolYear2017 = "M,N,P,Q,R,S"
     * Modulus 2 SchoolYear2018 = "T,U,V,W,X,Y"
     * 
     */
    @Ignore @Test
    public void getSchoolYearSuffixesTest() {
        
        List<String> l2016 = GenerateAccessCodes.getSchoolYearSuffixes( 2016 );
        Assert.assertEquals("",true,l2016.contains("f"));
        Assert.assertEquals("",true,l2016.contains("F"));
        Assert.assertEquals("",true,l2016.contains("g"));
        Assert.assertEquals("",true,l2016.contains("G"));
        Assert.assertEquals("",true,l2016.contains("h"));
        Assert.assertEquals("",true,l2016.contains("H"));
        Assert.assertEquals("",true,l2016.contains("j"));
        Assert.assertEquals("",true,l2016.contains("J"));
        Assert.assertEquals("",true,l2016.contains("k"));
        Assert.assertEquals("",true,l2016.contains("K"));
        Assert.assertEquals("",true,l2016.contains("l"));
        Assert.assertEquals("",true,l2016.contains("L"));
        List<String> l2017 = GenerateAccessCodes.getSchoolYearSuffixes( 2017 ); 
        Assert.assertEquals("",true,l2017.contains("m"));
        Assert.assertEquals("",true,l2017.contains("M"));
        Assert.assertEquals("",true,l2017.contains("n"));
        Assert.assertEquals("",true,l2017.contains("N"));
        Assert.assertEquals("",true,l2017.contains("p"));
        Assert.assertEquals("",true,l2017.contains("P"));
        Assert.assertEquals("",true,l2017.contains("q"));
        Assert.assertEquals("",true,l2017.contains("Q"));
        Assert.assertEquals("",true,l2017.contains("r"));
        Assert.assertEquals("",true,l2017.contains("R"));
        Assert.assertEquals("",true,l2017.contains("s"));
        Assert.assertEquals("",true,l2017.contains("S"));
        List<String> l2018 = GenerateAccessCodes.getSchoolYearSuffixes( 2018 );  
        Assert.assertEquals("",true,l2018.contains("t"));
        Assert.assertEquals("",true,l2018.contains("T"));
        Assert.assertEquals("",true,l2018.contains("u"));
        Assert.assertEquals("",true,l2018.contains("U"));
        Assert.assertEquals("",true,l2018.contains("v"));
        Assert.assertEquals("",true,l2018.contains("V"));
        Assert.assertEquals("",true,l2018.contains("w"));
        Assert.assertEquals("",true,l2018.contains("W"));
        Assert.assertEquals("",true,l2018.contains("x"));
        Assert.assertEquals("",true,l2018.contains("X"));
        Assert.assertEquals("",true,l2018.contains("y"));
        Assert.assertEquals("",true,l2018.contains("Y"));
    }
        
    
    @Ignore  @Test
    public void isSuffixInCurrentYearTest(){
        // The current school year is 2017 
        // This test breaks every new school year.
        Assert.assertEquals("",true, GenerateAccessCodes.isSuffixInCurrentYear("m"));
    }
    @Ignore  @Test
    public void schoolYearOfSuffixTest(){
        // The current school year is 2017 
        // This test breaks every three year.        
        Assert.assertEquals("", new Integer(2017) ,GenerateAccessCodes.schoolYearOfSuffix("M"));
    }

    /**
     * Returns what the school year.
     * 
     */
    @Ignore @Test
    public void  determineSchoolYearTest(){
        // The current school year is 2017 
        // This test breaks every new school year.
        Assert.assertEquals("", new Integer(2017),GenerateAccessCodes.determineSchoolYear());
    }

	
	
	
}
