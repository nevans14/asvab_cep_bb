package com.cep.spring.dao.jdbctemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cep.spring.utils.SimpleUtils;

// @Ignore
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration("/applicationContext.xml")
@ContextConfiguration("/controllerContext-test-prod-db.xml")

public class FlushAccessCodesTest {
	
	@Autowired
	FlushAccessCodes fac;
	@Autowired 
	JdbcTemplate jdbcTemplate;
	
	
	@Test
	@Ignore
	public void selectAccessCodesTest() {
		
		String sql = "select top 10 user_id from access_codes order by user_id asc";
		
		List<String> ls = new ArrayList<>();

		List<Map<String,String>> lhm = jdbcTemplate.query(sql, new GenericRowMapper());
		for(Map<String,String> row : lhm ) {
			ls.add(row.get("user_id") ) ;
		}
		
		fac.selectAccessCodes(ls);		
	}
	
	
	@Test
	@Ignore
	public void removeAccessCodesTest() {
		
		String sql = "select top 2 user_id from access_codes order by user_id desc";
		List<String> ls = new ArrayList<>();

		List<Map<String,String>> lhm = jdbcTemplate.query(sql, new GenericRowMapper());
		for(Map<String,String> row : lhm ) {
			ls.add(row.get("user_id") ) ;
		}
		
		fac.removeAccessCodes(ls);		
	}
	

	@Test
  @Ignore
	public void removeAccessCodesBatchTest() {
		
//		String sql = "select top 2 user_id from access_codes order by user_id desc";
		Date start = new Date();
		
/*		String sql = " SELECT TOP (2000) [user_id] " +
		    " FROM [ASVAB_CEP_2018].[dbo].[access_codes] " +
		    " WHERE expire_date < '2016-06-30 00:00:00.000' " +
		    " AND expire_date is not NULL "  +
		    " ORDER BY expire_date ASC";*/
		
String sql = "SELECT user_id " +
"  FROM [ASVAB_CEP_2018].[dbo].[access_codes] " +
"  where access_code in " + 
"('01892A','02960A','03192A','00564A','01762A','01761A','00563A','01894A','04397A','06661A','08929A','03199A','01635A','04262A','01504A','02833A','04263A','00438A','08182A','06624A','05558A','08184A','09381A','00791A','00660A','06891A','07829A','04362A','00565A','01896A','03063A','01630A','01765A','07711A','05314A','07978A','02816A','07846A','04247A','05444A','04377A','03294A','01867A','03162A','01860A','02092A','01993A','00532A','00664A','09287A','06789A','04258A','04126A','05443A','04245A','08914A','05575A','04242A','05573A','03045A','08071A','08070A','04119A','09271A','06786A','04124A','01626A','04122A','04255A','01628A','07982A','07850A','07983A','08083A','06529A','07851A','07721A','00685A','00681A','01623A','02956A','03188A','02820A','05582A','01758A','00426A','03186A','03050A','04382A','02952A','00556A')";


        List<String> ls = new ArrayList<>();

		List<Map<String,String>> lhm = jdbcTemplate.query(sql, new GenericRowMapper());
		for(Map<String,String> row : lhm ) {
			ls.add(row.get("user_id") ) ;
			
			
		}
		
		fac.removeAccessCodesBatch(ls);		
		
		Date end = new Date();
		Long timeDelta = end.getTime() - start.getTime() ;
		
		System.out.println(SimpleUtils.calculateDuration(timeDelta)  );
		
	}
	
	
	@Test 
	// @Ignore
	public void testForMissing() throws IOException{
	    String x = "C:/Users/dave/Desktop/accessCode2017Prod/CEP-CITM-accessCodes-2017Prod/p_reserve.6.txt";
	    
	    File f = new File(x);
	    
        BufferedReader b = new BufferedReader(new FileReader(f));
        List<String> lostAc = new ArrayList<>();
        String readLine = "";

        System.out.println("Reading file using Buffered Reader");

        int count = 1 ; 
        while ((readLine = b.readLine()) != null) {
            System.out.println(readLine + "    -> " + count);
            List<String> lac =  jdbcTemplate.queryForList("select access_code from access_codes where access_code = ? " , String.class,  readLine );
            if (lac.size() == 0 ) {
                lostAc.add(readLine);
            }
            count++;
        }	    
	    
        System.out.println("***********************************");
        
        for ( String ac : lostAc) {
            System.out.println("INSERT INTO [dbo].[access_codes]  ([access_code],[expire_date],[role]) VALUES ( '" + ac + "'  , NULL , 'R')");
        }
        
	}
	
	
	
	
	
	
}
