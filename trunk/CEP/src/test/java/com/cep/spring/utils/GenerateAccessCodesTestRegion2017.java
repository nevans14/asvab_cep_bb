package com.cep.spring.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cep.spring.dao.jdbctemplate.GenericRowMapper;



@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration("classpath:noredisContext.xml")
// @Transactional
public class GenerateAccessCodesTestRegion2017 {
    private     String outputDir = "C:/Users/dave/Desktop/accessCode2017Test";
    @Autowired 
    DataSource dataSource;
    
 @Test
 public void testMarketingCodesFor2017() {
     JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
     
 
     List<String> myMarketingCodes = jdbcTemplate.queryForList("select top(920) access_code from access_codes where role='M' and expire_date is null", String.class);
     System.out.println("*** testCodesFor2017!!! Codes ***");       

     List<String> marketing = new ArrayList<>(); 
     List<String> recruiter = new ArrayList<>(); 
     List<String> ess = new ArrayList<>(); 
     
     int counter = 0; 
     for ( String accessCode : myMarketingCodes ){
         
         accessCode = accessCode.trim();
         if( counter < 200) {
             // make Market files
             SimpleFileUtils.appendStringToFile(outputDir + "/marketingCodes.txt", accessCode + "\r\n");
             marketing.add(accessCode); 

         }
         if( counter >=  200 && counter < 400) {
             // make Recruiter files
             SimpleFileUtils.appendStringToFile(outputDir + "/recruiterCodes.txt", accessCode + "\r\n");
             recruiter.add(accessCode); 
         }
         if( counter >= 400 ) {
             // make ESS files
             SimpleFileUtils.appendStringToFile(outputDir + "/essCodes.txt", accessCode + "\r\n");
             ess.add(accessCode);
         }
         
         
         // Generate update scripts
         
         SimpleFileUtils.appendStringToFile(outputDir + "/updateCodes_toAdminRole.txt", 
                 "UPDATE [dbo].[access_codes] SET [role] = 'A' WHERE access_code = '" + accessCode + "';" + "\r\n"
                 );

         SimpleFileUtils.appendStringToFile(outputDir + "/updateCodes_withExpireDate.txt", 
                 " UPDATE [dbo].[access_codes] SET [expire_date] = '2017-09-01 00:00:00.000'  WHERE access_code = '" + accessCode + "';"  + "\r\n");
         counter++;
     }
     int fileNumber = 1;
     List<String> tempList = new ArrayList<>();
     for (int i = 1 ; i <= 200 ; i++){
         tempList.add(marketing.get(i-1));
         if ( i % 10 == 0 ) {
             //WriteExcelDemo.writeExcelDocFromList(outputDir + "/A_marketing."+ Integer.toString(fileNumber).trim() + ".xlsx", (ArrayList<String>) tempList);
             fileNumber++;
             tempList.clear();
             tempList = null;
             tempList = new ArrayList<>();
         }
     }
     
     fileNumber = 1;
     tempList = new ArrayList<>();
     for (int i = 1 ; i <= 200 ; i++){
         tempList.add(recruiter.get(i-1));
         if ( i % 10 == 0 ) {
             //WriteExcelDemo.writeExcelDocFromList(outputDir + "/B_recruiter."+ Integer.toString(fileNumber).trim() + ".xlsx", (ArrayList<String>) tempList);
             fileNumber++;
             tempList.clear();
             tempList = null;
             tempList = new ArrayList<>();
         }
     }
     fileNumber = 1;
     tempList = new ArrayList<>();
     for (int i = 1 ; i <= 500; i++){
         tempList.add(ess.get(i-1));
         if ( i % 25 == 0 ) {
             //WriteExcelDemo.writeExcelDocFromList(outputDir + "/C_ess."+ Integer.toString(fileNumber).trim() +  ".xlsx", (ArrayList<String>) tempList);
             
             fileNumber++;
             tempList.clear();
             tempList = null;
             tempList = new ArrayList<>();
         }
     }    
     
     fileNumber = 1;
     tempList = new ArrayList<>();
     for (int i = 1 ; i <= 10 ; i++){
         tempList.add(ess.get(i-1));
         if ( i % 25 == 0 ) {
             //WriteExcelDemo.writeExcelDocFromList(outputDir + "/H_marketing."+ Integer.toString(fileNumber).trim() +  ".xlsx", (ArrayList<String>) tempList);
             
             fileNumber++;
             tempList.clear();
             tempList = null;
             tempList = new ArrayList<>();
         }
     }    
     
     fileNumber = 1;
     tempList = new ArrayList<>();
     for (int i = 1 ; i <= 10 ; i++){
         tempList.add(ess.get(i-1));
         if ( i % 25 == 0 ) {
             //WriteExcelDemo.writeExcelDocFromList(outputDir + "/M_marketing."+ Integer.toString(fileNumber).trim() +  ".xlsx", (ArrayList<String>) tempList);
             
             fileNumber++;
             tempList.clear();
             tempList = null;
             tempList = new ArrayList<>();
         }
     }    
     
 }

 
    @Test
    public void testStudentCodesFor2017() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "SELECT TOP (100) ac.*,[ADMINISTERED_YEAR] ,[ADMINISTERED_MONTH] ,[ADMINISTERED_DAY] ,[SEQ_NUM] " +
                        " FROM [ASVAB_CITM_2018].[dbo].[test_scores] ts " +
                     " JOIN ASVAB_CEP_2018.dbo.access_codes  ac on ac.access_code = ts.ACCESS_CODE "
                     + "WHERE len(ac.access_code) = 10 and expire_date > '2017-07-16 00:00:00.000'" ; 
   
//        Map<String,String> resuts =  jdbcTemplate.queryForList("select top(900) access_code from access_codes where role='M' and expire_date is null", String.class);
        List<Map<String, String>> accessCodeList =  jdbcTemplate.query(sql, new GenericRowMapper() );

        for ( Map<String, String> map : accessCodeList){
            for(String key : map.keySet()) {
                System.out.println(map.get(key));
            }
            
        }
        sql = "SELECT TOP (100) ac.access_code " +
                " FROM [ASVAB_CITM_2018].[dbo].[test_scores] ts " +
             " JOIN ASVAB_CEP_2018.dbo.access_codes  ac on ac.access_code = ts.ACCESS_CODE "
             + "WHERE len(ac.access_code) = 10 and expire_date > '2017-07-16 00:00:00.000'" ; 
        
   List< String> accessCodeList2 =  jdbcTemplate.queryForList(sql, String.class);
        
        
        
        Integer fileNumber = 1;
        ArrayList<String> tempList = new ArrayList<>();
        for (int i = 1 ; i <= 10 ; i++){
            tempList.add(accessCodeList2.get(i-1));
            if ( i % 10 == 0 ) {
                //WriteExcelDemo.writeExcelDocFromList(outputDir + "/D_student."+ Integer.toString(fileNumber).trim() +  ".xlsx", (ArrayList<String>) tempList);
                
                fileNumber++;
                tempList.clear();
                tempList = null;
                tempList = new ArrayList<>();
            }
        }   
        
       fileNumber = 2;
        tempList = new ArrayList<>();
        for (int i = 11 ; i <= 20 ; i++){
            tempList.add(accessCodeList2.get(i-1));
            if ( i  % 10 == 0 ) {
                //WriteExcelDemo.writeExcelDocFromList(outputDir + "/I_student." +  ".xlsx", (ArrayList<String>) tempList);
                
                fileNumber++;
                tempList.clear();
                tempList = null;
                tempList = new ArrayList<>();
            }
        } 
        
        fileNumber = 3;
        tempList = new ArrayList<>();
        for (int i = 21 ; i <= 30 ; i++){
            tempList.add(accessCodeList2.get(i-1));
            if ( i % 10 == 0 ) {
                //WriteExcelDemo.writeExcelDocFromList(outputDir + "/J_student." +  ".xlsx", (ArrayList<String>) tempList);
                
                fileNumber++;
                tempList.clear();
                tempList = null;
                tempList = new ArrayList<>();
            }
        } 
        
        
        sql = "SELECT TOP (10)  ac.access_code , [afqt_score] " 
          + " FROM [ASVAB_CEP_2018].[dbo].[PORTFOLIO_TEST_ASVAB] pta "
          + " join access_codes ac on pta.user_id = ac.user_id "
          + " where ac.role = 'S';";       

        
        
        List<Map<String, String>> accessCodeList3 =  jdbcTemplate.query(sql, new GenericRowMapper() );        
        tempList = new ArrayList<>();
        for ( Map<String, String> map : accessCodeList3){
            tempList.add(map.get("access_code") + ":" + map.get("afqt_score"));
        }
        //WriteExcelDemo.writeExcelDocFromList(outputDir + "/E_student." +  ".xlsx", (ArrayList<String>) tempList);

        

        sql = "SELECT TOP (10) ac.access_code, u.username , pta.afqt_score "
                + "FROM [ASVAB_CEP_2018].[dbo].[PORTFOLIO_TEST_ASVAB] pta "
                + " join access_codes ac on pta.user_id = ac.user_id "
                + " join users u on u.user_id = ac.user_id  "
                + " where ac.role = 'S' ";
        List<Map<String, String>> accessCodeList4 =  jdbcTemplate.query(sql, new GenericRowMapper() );
       
        tempList = new ArrayList<>();
        for ( Map<String, String> map : accessCodeList4){
            tempList.add(map.get("access_code") + ":" + map.get("username") + ":" + map.get("afqt_score"));
        }
        //WriteExcelDemo.writeExcelDocFromList(outputDir + "/F_student." + ".xlsx", (ArrayList<String>) tempList);

    }
    
    @Test
    public void testCounselorCodesFor2017() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "SELECT TOP (30) ac.* " +
                     " FROM ASVAB_CEP_2018.dbo.access_codes  ac "
                     + " WHERE ac.role = 'C'" ; 
   
        List<Map<String, String>> accessCodeList =  jdbcTemplate.query(sql, new GenericRowMapper() );

        for ( Map<String, String> map : accessCodeList){
            for(String key : map.keySet()) {
                System.out.println(map.get(key));
            }
            
        }
        
        sql = "SELECT TOP (30) ACCESS_CODE " +
                " FROM ASVAB_CEP_2018.dbo.access_codes  ac "
                + " WHERE ac.role = 'C'" ; 

   List< String> accessCodeList2 =  jdbcTemplate.queryForList(sql, String.class);
        
        
        
        Integer fileNumber = 1;
        ArrayList<String> tempList = new ArrayList<>();
        for (int i = 1 ; i <= 10 ; i++){
            tempList.add(accessCodeList2.get(i-1));
            if ( i % 10 == 0 ) {
                //WriteExcelDemo.writeExcelDocFromList(outputDir + "/G_counselor."+ Integer.toString(fileNumber).trim() +  ".xlsx", (ArrayList<String>) tempList);
                
                fileNumber++;
                tempList.clear();
                tempList = null;
                tempList = new ArrayList<>();
            }
        }   
        
       fileNumber = 2;
        tempList = new ArrayList<>();
        for (int i = 11 ; i <= 20 ; i++){
            tempList.add(accessCodeList2.get(i-1));
            if ( i  % 10 == 0 ) {
                //WriteExcelDemo.writeExcelDocFromList(outputDir + "/K_counselor."+ Integer.toString(fileNumber).trim() +  ".xlsx", (ArrayList<String>) tempList);
                
                fileNumber++;
                tempList.clear();
                tempList = null;
                tempList = new ArrayList<>();
            }
        } 
        
        fileNumber = 3;
        tempList = new ArrayList<>();
        for (int i = 21 ; i <= 30 ; i++){
            tempList.add(accessCodeList2.get(i-1));
            if ( i % 10 == 0 ) {
                //WriteExcelDemo.writeExcelDocFromList(outputDir + "/L_counselor."+ Integer.toString(fileNumber).trim() +  ".xlsx", (ArrayList<String>) tempList);
                
                fileNumber++;
                tempList.clear();
                tempList = null;
                tempList = new ArrayList<>();
            }
        } 
    }

        
        
        
        
        
    @Ignore 	@Test
	public void testGenerateThreeWeekCodes() {
		System.out.println("*** Three Week Codes ***");
		Set<String> ls = GenerateAccessCodes.generateThreeWeekCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}
	
    @Ignore 	@Test
	public void testCounselorCodes() {
		System.out.println("*** Counselor Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateCounselorCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}		
	}

    @Ignore 	@Test
	public void testMarketingCodes() {
		System.out.println("*** Marketing Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateMarketingCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}
	
    @Ignore 	@Test
	public void testReservedCodes() {
		System.out.println("*** Reserved Codes ***");		
		Set<String> ls = GenerateAccessCodes.generateReservedCodes(10);
		for ( String s : ls) {
			System.out.println(s);
		}
	}

	
    
    /** Given todays date determine what the Modulus year would be.
     * 
     * Modulus 0 SchoolYear2016 = "F,G,H,J,K,L"
     * Modulus 1 SchoolYear2017 = "M,N,P,Q,R,S"
     * Modulus 2 SchoolYear2018 = "T,U,V,W,X,Y"
     * 
     */
    @Ignore @Test
    public void getSchoolYearSuffixesTest() {
        
        List<String> l2016 = GenerateAccessCodes.getSchoolYearSuffixes( 2016 );
        Assert.assertEquals("",true,l2016.contains("f"));
        Assert.assertEquals("",true,l2016.contains("F"));
        Assert.assertEquals("",true,l2016.contains("g"));
        Assert.assertEquals("",true,l2016.contains("G"));
        Assert.assertEquals("",true,l2016.contains("h"));
        Assert.assertEquals("",true,l2016.contains("H"));
        Assert.assertEquals("",true,l2016.contains("j"));
        Assert.assertEquals("",true,l2016.contains("J"));
        Assert.assertEquals("",true,l2016.contains("k"));
        Assert.assertEquals("",true,l2016.contains("K"));
        Assert.assertEquals("",true,l2016.contains("l"));
        Assert.assertEquals("",true,l2016.contains("L"));
        List<String> l2017 = GenerateAccessCodes.getSchoolYearSuffixes( 2017 ); 
        Assert.assertEquals("",true,l2017.contains("m"));
        Assert.assertEquals("",true,l2017.contains("M"));
        Assert.assertEquals("",true,l2017.contains("n"));
        Assert.assertEquals("",true,l2017.contains("N"));
        Assert.assertEquals("",true,l2017.contains("p"));
        Assert.assertEquals("",true,l2017.contains("P"));
        Assert.assertEquals("",true,l2017.contains("q"));
        Assert.assertEquals("",true,l2017.contains("Q"));
        Assert.assertEquals("",true,l2017.contains("r"));
        Assert.assertEquals("",true,l2017.contains("R"));
        Assert.assertEquals("",true,l2017.contains("s"));
        Assert.assertEquals("",true,l2017.contains("S"));
        List<String> l2018 = GenerateAccessCodes.getSchoolYearSuffixes( 2018 );  
        Assert.assertEquals("",true,l2018.contains("t"));
        Assert.assertEquals("",true,l2018.contains("T"));
        Assert.assertEquals("",true,l2018.contains("u"));
        Assert.assertEquals("",true,l2018.contains("U"));
        Assert.assertEquals("",true,l2018.contains("v"));
        Assert.assertEquals("",true,l2018.contains("V"));
        Assert.assertEquals("",true,l2018.contains("w"));
        Assert.assertEquals("",true,l2018.contains("W"));
        Assert.assertEquals("",true,l2018.contains("x"));
        Assert.assertEquals("",true,l2018.contains("X"));
        Assert.assertEquals("",true,l2018.contains("y"));
        Assert.assertEquals("",true,l2018.contains("Y"));
    }
        
    
    @Ignore  @Test
    public void isSuffixInCurrentYearTest(){
        // The current school year is 2017 
        // This test breaks every new school year.
        Assert.assertEquals("",true, GenerateAccessCodes.isSuffixInCurrentYear("m"));
    }
    @Ignore  @Test
    public void schoolYearOfSuffixTest(){
        // The current school year is 2017 
        // This test breaks every three year.        
        Assert.assertEquals("", new Integer(2017) ,GenerateAccessCodes.schoolYearOfSuffix("M"));
    }

    /**
     * Returns what the school year.
     * 
     */
    @Ignore @Test
    public void  determineSchoolYearTest(){
        // The current school year is 2017 
        // This test breaks every new school year.
        Assert.assertEquals("", new Integer(2017),GenerateAccessCodes.determineSchoolYear());
    }

	
	
	
}
