package com.cep.spring.mvc.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.validator.routines.EmailValidator;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cep.spring.model.Registration;
import com.cep.spring.mvc.controller.LoginRegistrationController.RegistrationLoginStatus;
import com.cep.spring.utils.SimpleUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
@Transactional
public class LoginRegistrationControllerTest implements ApplicationContextAware {

    @Autowired
    LoginRegistrationController lrc;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    
    @Test
    @Ignore
    public void testLoginAccessCode_Found_expire_date_null() {

        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', NULL ,'Z')");

        Registration registration = new Registration();
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        String dateString = jdbcTemplate.queryForObject(
                "SELECT expire_date FROM [dbo].[access_codes] WHERE [access_code] = 'BOBBY-ORE'", String.class);
        dateString = dateString.substring(0, dateString.indexOf(' '));

        String expectedExpireDate = SimpleUtils
                .getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 21));
        expectedExpireDate = expectedExpireDate.substring(0, expectedExpireDate.indexOf(' '));

        Assert.assertEquals("message", 0, expectedExpireDate.compareTo(dateString));

    }

    @Test
    @Ignore
    public void testLoginAccessCode_Found_expire_date_before_today() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), -1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        Registration registration = new Registration();
        registration.setAccessCode("BOBBY-ORE");
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, null, null);

        // Registration Not Successful
        Assert.assertEquals("message", new Integer(1021), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testLoginAccessCode_Found_expire_date_after_today() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        Registration registration = new Registration();
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testLoginAccessCode_Not_Found_less_then_10_characters() {

        Registration registration = new Registration();
        registration.setAccessCode("98765432F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest, mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());

    }

    @Test
    @Ignore
    public void testLoginAccessCode_Not_Found_more_then_10_characters() {

        Registration registration = new Registration();
        registration.setAccessCode("9876543210F");
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, null, null);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testLoginAccessCode_Not_Found_suffix_not_valid_letter() {
        Registration registration = new Registration();
        registration.setAccessCode("987654321A");
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, null, null);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testLoginAccessCode_Not_Found_suffix_in_from_mod_0() {
        Registration registration = new Registration();
        registration.setAccessCode("987654321F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testLoginAccessCode_Not_Found_suffix_in_from_mod_1() {
        Registration registration = new Registration();
        registration.setAccessCode("987654321M");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testLoginAccessCode_Not_Found_suffix_in_from_mod_2() {
        Registration registration = new Registration();
        registration.setAccessCode("987654321T");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.loginAccessCode(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testLoginEmail_Email_NotFound() {

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword("joe.blow.from.idaho");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.loginEmail(registration, mockRequest, mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1002), rls.getStatusNumber());

    }

    @Test
    @Ignore
    public void testLoginEmail_Email_RegistrationFailed() {

        jdbcTemplate.update("INSERT INTO [dbo].[users]  ([user_id]  ,[username] ,[password] ,[enabled] ,[reset_key],[reset_expire_date]) " +
                " VALUES (0 ,'joe.blow@idaho.gov' ,'mypassword'  ,1,NULL ,NULL)");


        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
                     
        reg.setPassword("joe.blow.from.idaho");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg,mockRequest,mockResponse);
  
        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1023), rlsLogin.getStatusNumber());
        

        
    }
    @Test
    @Ignore
    public void testLoginEmail_Email_Locked() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration,mockRequest,mockResponse);

        // Registration successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        jdbcTemplate.update("UPDATE users SET enabled=0 where username='joe.blow@idaho.gov'");

        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setPassword("joe.blow.from.idaho");
        
        mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg,mockRequest,mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1010), rlsLogin.getStatusNumber());

    }

    @Test
    @Ignore
    public void testLoginEmail_Email_Password_No_Match() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow-from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest,mockResponse);

        // Registration successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setPassword("joe.blow.from.idaho");
        mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg,mockRequest,mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1008), rlsLogin.getStatusNumber());

    }

    @Test
    @Ignore
    public void testLoginEmail_Email_Password_Match() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setPassword("joe.blow.from.idaho");
        mockRequest = new MockHttpServletRequest();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg, mockRequest, mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(0), rlsLogin.getStatusNumber());

    }

    /**
     *  Attempt to login with CITM reserved Access Code !!!
     */
    @Test
    @Ignore
    public void testLoginEmail_CITM_Email_Password_Match() {
        // Register user as CITM user with out real access code.
        // 1. Insert in Acccess Code
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        Map<String,Object> newAccessCodeParameters = new HashMap<>();

       String myAccessCode = jdbcTemplate.queryForObject("select TOP (1) access_code from access_codes where access_code like '%CITM' order by user_id desc", String.class);
         Long myNumber = Long.parseLong(myAccessCode.substring(0, 6));
         String formattedAccesscode = String.format(Locale.ENGLISH, "%06d", myNumber + 1);
       newAccessCodeParameters.put("access_code", formattedAccesscode + "CITM");
       newAccessCodeParameters.put("expire_date", expireDate);
       newAccessCodeParameters.put("role", "Z");
        SimpleJdbcInsert insert =  new SimpleJdbcInsert(jdbcTemplate).withTableName("access_codes").usingGeneratedKeyColumns("user_id");
        Number key = insert.executeAndReturnKey(newAccessCodeParameters);
        System.out.println(key.intValue());
        System.out.println(key.longValue());
        System.out.println(key.doubleValue());
        System.out.println(key.floatValue());
        Long userId = key.longValue(); 

        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        // 2. Insert Usernames
        String sql = "INSERT INTO [dbo].[users] ([user_id],[username],[password],[enabled],[system]) VALUES "
                + " (?,?,?,1,'CITM')";

        jdbcTemplate.update(sql, userId, "joe.blow@idaho.gov",encodedPassWord);
        // 3. Insert user_roles
        jdbcTemplate.update("INSERT INTO [dbo].[user_roles] ([username],[role]) VALUES (?,'ROLE_USER')","joe.blow@idaho.gov");


        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setPassword("joe.blow.from.idaho");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rlsLogin = lrc.loginEmail(reg, mockRequest, mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1024), rlsLogin.getStatusNumber());

    }
    
    /**
     *  Attempt to register with CITM reserved Access Code !!!
     */
    @Test
    @Ignore
    public void testRegisterEmail_CITM_Email_Password_Match() {
        // Register user as CITM user with out real access code.
        // 1. Insert in Acccess Code
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        Map<String,Object> newAccessCodeParameters = new HashMap<>();

       String myAccessCode = jdbcTemplate.queryForObject("select TOP (1) access_code from access_codes where access_code like '%CITM' order by user_id desc", String.class);
         Long myNumber = Long.parseLong(myAccessCode.substring(0, 6));
         String formattedAccesscode = String.format(Locale.ENGLISH, "%06d", myNumber + 1) + "CITM";
       newAccessCodeParameters.put("access_code", formattedAccesscode );
       newAccessCodeParameters.put("expire_date", expireDate);
       newAccessCodeParameters.put("role", "Z");
        SimpleJdbcInsert insert =  new SimpleJdbcInsert(jdbcTemplate).withTableName("access_codes").usingGeneratedKeyColumns("user_id");
        Number key = insert.executeAndReturnKey(newAccessCodeParameters);
        Long userId = key.longValue(); 

        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");
        // 2. Insert Usernames
        String sql = "INSERT INTO [dbo].[users] ([user_id],[username],[password],[enabled],[system]) VALUES "
                + " (?,?,?,1,'CITM')";

        jdbcTemplate.update(sql, userId, "joe.blow@idaho.gov",encodedPassWord);
        // 3. Insert user_roles
        jdbcTemplate.update("INSERT INTO [dbo].[user_roles] ([username],[role]) VALUES (?,'ROLE_USER')","joe.blow@idaho.gov");


        Registration reg = new Registration();
        reg.setAccessCode(formattedAccesscode);
        reg.setEmail("joe.the.plumber@idaho.gov");
        reg.setPassword("joe.blow.from.idaho");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rlsLogin = lrc.register(reg, mockRequest, mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("Attempted to register with CITM access Code ", new Integer(1024), rlsLogin.getStatusNumber());

    }
    
    /**
     *  Attempt to Login with CITM reserved Access Code !!!
     */
    @Test
    @Ignore
    public void testLogin_CITM_Access_Code() {
        // Register user as CITM user with out real access code.
        // 1. Insert in Acccess Code
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        Map<String,Object> newAccessCodeParameters = new HashMap<>();

       String formattedAccesscode = "123456CITM";
       newAccessCodeParameters.put("access_code", formattedAccesscode );
       newAccessCodeParameters.put("expire_date", expireDate);
       newAccessCodeParameters.put("role", "Z");
        SimpleJdbcInsert insert =  new SimpleJdbcInsert(jdbcTemplate).withTableName("access_codes").usingGeneratedKeyColumns("user_id");
        Number key = insert.executeAndReturnKey(newAccessCodeParameters);
        Long userId = key.longValue(); 


        Registration reg = new Registration();
        reg.setAccessCode(formattedAccesscode);
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rlsLogin = lrc.loginAccessCode(reg, mockRequest, mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("Attempted to register with CITM access Code ", new Integer(1024), rlsLogin.getStatusNumber());

    }

    
    
    @Test
    @Ignore
    public void testRegistration_AccessCode_Found_expire_date_after_today() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);


        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");
        
        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration Not Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testRegistration_AccessCode_Found_expire_date_before_today() {
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), -1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration Not Successful
        Assert.assertEquals("message", new Integer(1021), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testRegistration_AccessCode_Found_expire_date_null() {
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', NULL ,'Z')");

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("BOBBY-ORE");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

        String dateString = jdbcTemplate.queryForObject(
                "SELECT expire_date FROM [dbo].[access_codes] WHERE [access_code] = 'BOBBY-ORE'", String.class);
        dateString = dateString.substring(0, dateString.indexOf(' '));

        String expectedExpireDate = SimpleUtils
                .getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 21));
        expectedExpireDate = expectedExpireDate.substring(0, expectedExpireDate.indexOf(' '));

        Assert.assertEquals("message", 0, expectedExpireDate.compareTo(dateString));

    }

    @Test
    @Ignore
    public void testRegistration_AccessCode_Not_Found_less_then_10_characters() {

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("98765432F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());

    }

    @Test
    @Ignore
    public void testRegistration_AccessCode_Not_Found_more_then_10_characters() {

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("9876543210F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration Unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());

    }

    @Test
    @Ignore
    public void testRegistration_AccessCode_Not_Found_suffix_in_from_mod_0() {

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

    }

    @Test
    @Ignore
    public void testRegistration_AccessCode_Not_Found_suffix_in_from_mod_1() {
        


        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321M");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

    }

    @Test
    @Ignore
    public void testRegistration_AccessCode_Not_Found_suffix_in_from_mod_2() {



        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321T");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());

    }

    @Test
    @Ignore
    public void testRegistration_AccessCode_Not_Found_suffix_not_valid_letter() {

 

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321A");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testRegistration_PasswordBlank() {


        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("987654321F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
    }

    
    @Test
    @Ignore
    public void testRegistration_Email_RegistrationFailed_Update() {
        
        String expireDate = SimpleUtils.getStringInStandardFormatFromDate(SimpleUtils.datePlusDays(new Date(), 1));
        jdbcTemplate.update("INSERT INTO [dbo].[access_codes] ([access_code] ,[expire_date] ,[role]) VALUES "
                + " ('BOBBY-ORE', ? ,'Z')", expireDate);

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("joe.blow.from.idaho");
        
        jdbcTemplate.update("INSERT INTO [dbo].[users]  ([user_id]  ,[username] ,[password] ,[enabled] ,[reset_key],[reset_expire_date]) " +
                " VALUES (0 ,'joe.blow@idaho.gov' , ?  , 1, NULL ,NULL)", encodedPassWord);

        Registration reg = new Registration();
        reg.setEmail("joe.blow@idaho.gov");
        reg.setAccessCode("BOBBY-ORE");
        reg.setPassword(bCryptPasswordEncoder.encode("duke"));
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rlsLogin = lrc.register(reg, mockRequest, mockResponse);
  
        // Registration Successful
        Assert.assertEquals("message", new Integer(0), rlsLogin.getStatusNumber());
        
        Long userId = jdbcTemplate.queryForObject("select user_id from users where username = 'joe.blow@idaho.gov'", Long.class);

        Assert.assertNotEquals( new Long( 0 ) , userId );
        
        String encodedPasswordFromDb = jdbcTemplate.queryForObject("SELECT password FROM users WHERE username = ?", String.class, reg.getEmail());
        
        Assert.assertEquals("message", true , bCryptPasswordEncoder.matches("duke", encodedPasswordFromDb) );
        
        
        
    }
    
    @Test
    @Ignore
    public void testRegistration_EmailBlank() {

        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("");

        Registration registration = new Registration();
        registration.setEmail("");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("98765432F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());
    }

    @Test
    @Ignore
    public void testRegistration_EmailNo_at_Symbol() {
        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("");

        Registration registration = new Registration();
        registration.setEmail("joe.blow.idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("98765432F");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(1001), rls.getStatusNumber());

    }
    @Test
    @Ignore
    public void test_isThisAValidTeacherPromoAccessCode_Fail() {
        Registration registration = new Registration();
        // Valid Student Code 
        registration.setAccessCode("98765432F");
        RegistrationLoginStatus rls = lrc.isThisAValidTeacherPromoAccessCode(registration);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(805), rls.getStatusNumber());
        Assert.assertEquals("message", "Not A valid teacher access code." , rls.getStatus());

    }

    @Test
    @Ignore
    public void test_isThisAValidTeacherPromoAccessCode_SuccessWith_ID1() {
        Registration registration = new Registration();
        registration.setAccessCode("CEP123");
        
        RegistrationLoginStatus rls = lrc.isThisAValidTeacherPromoAccessCode(registration);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
        Assert.assertEquals("message", "Success" , rls.getStatus());

    }
    
    @Test
    @Ignore
    public void test_isThisAValidTeacherPromoAccessCode_SuccessWith_ID2() {
        Registration registration = new Registration();
        registration.setAccessCode("CEP456");
        
        RegistrationLoginStatus rls = lrc.isThisAValidTeacherPromoAccessCode(registration);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
        Assert.assertEquals("message", "Success" , rls.getStatus());

    }

    @Test
    @Ignore
    public void test_teacherRegistrationForPromo_Success_AccessCode1() {
        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("CEP123");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.teacherRegistrationForPromo(registration, mockRequest, mockResponse);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
        Assert.assertEquals("message", "Success" , rls.getStatus());

    }    
    
    
    @Test
    @Ignore
    public void test_teacherRegistrationForPromo_SuccessAccessCode2() {
        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("");

        Registration registration = new Registration();
        registration.setEmail("joe.blow@idaho.gov");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("CEP456");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.teacherRegistrationForPromo(registration, mockRequest, mockResponse);

        // Registration unsuccessful
        Assert.assertEquals("message", new Integer(0), rls.getStatusNumber());
        Assert.assertEquals("message", "Success" , rls.getStatus());

    }    

    
    @Test
    @Ignore
    public void test_teacherRegistrationForPromo_EmailExists_Error() {
        // Note client side does this with javascript version of the encoder.
        String encodedPassWord = bCryptPasswordEncoder.encode("");

        Registration registration = new Registration();
        registration.setEmail("Avishek.Roy@mail.mil");
        registration.setPassword(encodedPassWord);
        registration.setAccessCode("CEP123");
        registration.setRecaptchaResponse("");
        MockHttpServletRequest mockRequest = new MockHttpServletRequest();
        MockHttpServletResponse mockResponse = new MockHttpServletResponse();
        RegistrationLoginStatus rls = lrc.register(registration, mockRequest, mockResponse);

        // Registration unsuccessful
        System.out.println(rls.getStatusNumber());
        System.out.println(rls.getStatus());  
        RegistrationLoginStatus rlss = lrc.register(registration, mockRequest, mockResponse);
        
        Assert.assertEquals("message", new Integer(1003), rlss.getStatusNumber());
        Assert.assertEquals("message", "This user name is already taken." , rlss.getStatus());

    }

    @Test
    public void test_emailValidator_ValidEmails() {
        EmailValidator validator = EmailValidator.getInstance();
        Assert.assertTrue(validator.isValid("example@example.com"));
        Assert.assertTrue(validator.isValid("example+example@example.com"));
        Assert.assertTrue(validator.isValid("example+123@example.com"));
        Assert.assertTrue(validator.isValid("j@example.com"));
        Assert.assertTrue(validator.isValid("j_+example@example.com"));
        Assert.assertTrue(validator.isValid("j.example@example.com"));
    }

    @Test
    public void test_emailValidator_InvalidEmail_SingleCharacter() {
        EmailValidator validator = EmailValidator.getInstance();
        Assert.assertFalse(validator.isValid("f"));
        Assert.assertFalse(validator.isValid("@"));
    }

    @Test
    public void test_emailValidator_InvalidEmail_NoDomain() {
        EmailValidator validator = EmailValidator.getInstance();
        Assert.assertFalse(validator.isValid("example@"));
    }

    @Test
    public void test_emailValidator_InvalidEmail_NoLocalPart() {
        EmailValidator validator = EmailValidator.getInstance();
        Assert.assertFalse(validator.isValid("@example.com"));
    }

    @Test
    public void test_emailValidator_InvalidEmail_NoAtSign() {
        EmailValidator validator = EmailValidator.getInstance();
        Assert.assertFalse(validator.isValid("example-example.com"));
    }

    @Test
    public void test_emailValidator_InvalidEmail_ConsecutiveSpecialCharacters() {
        EmailValidator validator = EmailValidator.getInstance();
        Assert.assertFalse(validator.isValid("f..@"));
        Assert.assertFalse(validator.isValid("f..@example"));
        Assert.assertFalse(validator.isValid("f..@example.com"));
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        String[] beans = applicationContext.getBeanDefinitionNames();
        // for ( int i = 0 ; i < beans.length ; i++){
        // System.out.println(beans[i]);
        // }
    }

}
