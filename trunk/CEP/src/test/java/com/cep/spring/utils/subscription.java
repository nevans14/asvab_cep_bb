package com.cep.spring.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cep.spring.dao.mybatis.ContactUsDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:controllerContext.xml")
public class subscription {

    @Autowired
    private ContactUsDAO contactUsDAO;
    
	@Test
	public void test() {
		System.out.println("Response: "+contactUsDAO.isSubscribed("Avishek.Roy.CTR@mail.mil"));
	}

}
