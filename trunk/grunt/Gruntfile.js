module.exports = function(grunt) {
    //grunt wrapper function
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
          //grunt task configuration will go here

		ngAnnotate: {
			options: {
				singleQuotes: true
			},
			app: {
				files: {
					'js/cep/cep.module.js': [
					'../CEP/src/main/webapp/cep/cep.module.js'],
					'js/cep/cep.routes.js': [
					'../CEP/src/main/webapp/cep/cep.routes.js'],
					'js/cep/service/authenticationFactory.js': [
					'../CEP/src/main/webapp/cep/service/authenticationFactory.js'],
					'js/cep/service/user-factory.js': [
					'../CEP/src/main/webapp/cep/service/user-factory.js'],
					'js/cep/service/angular-response-observer-factory.js': [
					'../CEP/src/main/webapp/cep/service/angular-response-observer-factory.js'],
					'js/cep/service/error-log-service.js': [
					'../CEP/src/main/webapp/cep/service/error-log-service.js'],
					'js/cep/controller/google-analytics-controller.js': [
					'../CEP/src/main/webapp/cep/controller/google-analytics-controller.js'],
					'js/cep/service/modal-service.js': [
					'../CEP/src/main/webapp/cep/service/modal-service.js'],
					'js/cep/service/video-modal-service.js': [
					'../CEP/src/main/webapp/cep/service/video-modal-service.js'],
					'js/cep/service/youtube-modal-service.js': [
					'../CEP/src/main/webapp/cep/service/youtube-modal-service.js'],
					'js/cep/service/session-modal-service.js': [
					'../CEP/src/main/webapp/cep/service/session-modal-service.js'],
					'js/cep/service/learn-more-modal-service.js': [
					'../CEP/src/main/webapp/cep/service/learn-more-modal-service.js'],
					'js/cep/service/bring-to-school-modal-service.js': [
					'../CEP/src/main/webapp/cep/service/bring-to-school-modal-service.js'],
					'js/cep/service/utility-service.js': [
					'../CEP/src/main/webapp/cep/service/utility-service.js'],
					'js/cep/components/login/controllers/login-controller.js': [
					'../CEP/src/main/webapp/cep/components/login/controllers/login-controller.js'],
					'js/cep/components/login/services/login-modal-service.js': [
					'../CEP/src/main/webapp/cep/components/login/services/login-modal-service.js'],
					'js/cep/components/login/services/merge-account-modal-service.js': [
					'../CEP/src/main/webapp/cep/components/login/services/merge-account-modal-service.js'],
					'js/cep/components/career-one-stop/services/apprenticeship-list-modal-service.js': ['../CEP/src/main/webapp/cep/components/career-one-stop/services/apprenticeship-list-modal-service.js'],
					'js/cep/components/career-one-stop/services/certification-list-modal-service.js': ['../CEP/src/main/webapp/cep/components/career-one-stop/services/certification-list-modal-service.js'],
					'js/cep/components/career-one-stop/services/certification-details-modal-service.js': ['../CEP/src/main/webapp/cep/components/career-one-stop/services/certification-details-modal-service.js'],
					'js/cep/components/career-one-stop/services/licenses-list-modal-service.js': ['../CEP/src/main/webapp/cep/components/career-one-stop/services/licenses-list-modal-service.js'],
					'js/cep/components/career-one-stop/services/license-details-modal-service.js': ['../CEP/src/main/webapp/cep/components/career-one-stop/services/license-details-modal-service.js'],
					'js/cep/controller/header-controller.js': [
					'../CEP/src/main/webapp/cep/controller/header-controller.js'],
					'js/cep/controller/footer-controller.js': [
					'../CEP/src/main/webapp/cep/controller/footer-controller.js'],
					'js/cep/controller/left-navigation-controller.js': [
					'../CEP/src/main/webapp/cep/controller/left-navigation-controller.js'],
					'js/cep/controller/occufind-add-favorites-controller.js': [
					'../CEP/src/main/webapp/cep/controller/occufind-add-favorites-controller.js'],
					'js/cep/controller/occufind-grid-add-favorites-controller.js':[
					'../CEP/src/main/webapp/cep/controller/occufind-grid-add-favorites-controller.js'],
					'js/cep/controller/school-add-favorites-controller.js': [
					'../CEP/src/main/webapp/cep/controller/school-add-favorites-controller.js'],
					'js/cep/controller/career-cluster-add-favorites-controller.js': [
					'../CEP/src/main/webapp/cep/controller/career-cluster-add-favorites-controller.js'],
					'js/cep/controller/log-out-controller.js': [
					'../CEP/src/main/webapp/cep/controller/log-out-controller.js'],
					'js/cep/controller/view-detail-modal-controller.js': [
						'../CEP/src/main/webapp/cep/controller/view-detail-modal-controller.js'],
					'js/cep/controller/mobile-interest-codes-controller.js': [
					'../CEP/src/main/webapp/cep/controller/mobile-interest-codes-controller.js'],
					'js/cep/components/search/controllers/search-controller.js': [
					'../CEP/src/main/webapp/cep/components/search/controllers/search-controller.js'],
					'js/cep/components/search/controllers/search-result-controller.js': [
					'../CEP/src/main/webapp/cep/components/search/controllers/search-result-controller.js'],
					'js/cep/components/search/services/search-rest-factory.js': [
					'../CEP/src/main/webapp/cep/components/search/services/search-rest-factory.js'],
					'js/cep/components/home/controllers/home-controller.js': [
					'../CEP/src/main/webapp/cep/components/home/controllers/home-controller.js'],
					'js/cep/components/home/services/home-rest-factory.js': [
					'../CEP/src/main/webapp/cep/components/home/services/home-rest-factory.js'],
					'js/cep/components/password-reset/controllers/password-reset-controller.js': ['../CEP/src/main/webapp/cep/components/password-reset/controllers/password-reset-controller.js'],
					'js/cep/components/notes/service/notes-modal-service.js': [
					'../CEP/src/main/webapp/cep/components/notes/service/notes-modal-service.js'],
					'js/cep/components/notes/controller/notes-controller.js': [
					'../CEP/src/main/webapp/cep/components/notes/controller/notes-controller.js'],
					'js/cep/components/notes/service/notes-service.js': [
					'../CEP/src/main/webapp/cep/components/notes/service/notes-service.js'],
					'js/cep/components/notes/service/notes-rest-factory.js': [
					'../CEP/src/main/webapp/cep/components/notes/service/notes-rest-factory.js'],
					'js/cep/components/notes/service/career-cluster-notes-modal-service.js': ['../CEP/src/main/webapp/cep/components/notes/service/career-cluster-notes-modal-service.js'],
					'js/cep/components/notes/controller/career-cluster-notes-controller.js': ['../CEP/src/main/webapp/cep/components/notes/controller/career-cluster-notes-controller.js'],
					'js/cep/components/notes/service/career-cluster-notes-service.js': [
					'../CEP/src/main/webapp/cep/components/notes/service/career-cluster-notes-service.js'],
					'js/cep/components/notes/service/school-notes-modal-service.js': [
					'../CEP/src/main/webapp/cep/components/notes/service/school-notes-modal-service.js'],
					'js/cep/components/notes/controller/school-notes-controller.js': [
					'../CEP/src/main/webapp/cep/components/notes/controller/school-notes-controller.js'],
					'js/cep/components/notes/service/school-notes-service.js': [
					'../CEP/src/main/webapp/cep/components/notes/service/school-notes-service.js'],
					'js/cep/components/find-your-interest/controllers/find-your-interest-controller.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/controllers/find-your-interest-controller.js'],
					'js/cep/components/find-your-interest/controllers/find-your-interest-tied-controller.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/controllers/find-your-interest-tied-controller.js'],
					'js/cep/components/find-your-interest/controllers/find-your-interest-test-controller.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/controllers/find-your-interest-test-controller.js'],
					'js/cep/components/find-your-interest/controllers/find-your-interest-score-controller.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/controllers/find-your-interest-score-controller.js'],
					'js/cep/components/find-your-interest/controllers/find-your-interest-score-shared-controller.js': [					
					'../CEP/src/main/webapp/cep/components/find-your-interest/controllers/find-your-interest-score-shared-controller.js'],
					'js/cep/components/find-your-interest/controllers/my-asvab-summary-results-controller.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/controllers/my-asvab-summary-results-controller.js'],
					'js/cep/components/find-your-interest/controllers/my-asvab-summary-results-manual-controller.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/controllers/my-asvab-summary-results-manual-controller.js'],
					'js/cep/components/find-your-interest/controllers/find-your-interest-tied-score-controller.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/controllers/find-your-interest-tied-score-controller.js'],
					'js/cep/components/find-your-interest/services/find-your-interest-service.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/services/find-your-interest-service.js'],
					'js/cep/components/find-your-interest/services/find-your-interest-rest-factory.js': ['../CEP/src/main/webapp/cep/components/find-your-interest/services/find-your-interest-rest-factory.js'],
					'js/cep/components/faq/controllers/faq-controller.js': [
					'../CEP/src/main/webapp/cep/components/faq/controllers/faq-controller.js'],
					'js/cep/components/faq/services/faq-rest-factory.js': [
					'../CEP/src/main/webapp/cep/components/faq/services/faq-rest-factory.js'],
					'js/cep/components/faq/services/score-request-modal-service.js': [
					'../CEP/src/main/webapp/cep/components/faq/services/score-request-modal-service.js'],
					'js/cep/components/occufind/controllers/occufind-occupation-search-controller.js': ['../CEP/src/main/webapp/cep/components/occufind/controllers/occufind-occupation-search-controller.js'],
					'js/cep/components/occufind/controllers/occufind-occupation-search-results-controller.js': ['../CEP/src/main/webapp/cep/components/occufind/controllers/occufind-occupation-search-results-controller.js'],
					'js/cep/components/occufind/controllers/occufind-occupation-details-controller.js': ['../CEP/src/main/webapp/cep/components/occufind/controllers/occufind-occupation-details-controller.js'],
					'js/cep/components/occufind/controllers/occufind-education-details-controller.js': ['../CEP/src/main/webapp/cep/components/occufind/controllers/occufind-education-details-controller.js'],
					'js/cep/components/occufind/controllers/occufind-school-details-controller.js': ['../CEP/src/main/webapp/cep/components/occufind/controllers/occufind-school-details-controller.js'],
					'js/cep/components/occufind/controllers/occufind-employment-details-controller.js': ['../CEP/src/main/webapp/cep/components/occufind/controllers/occufind-employment-details-controller.js'],
					'js/cep/components/occufind/controllers/occufind-military-details-controller.js': ['../CEP/src/main/webapp/cep/components/occufind/controllers/occufind-military-details-controller.js'],
					'js/cep/components/occufind/services/occufind-occupation-search-service.js': ['../CEP/src/main/webapp/cep/components/occufind/services/occufind-occupation-search-service.js'],
					'js/cep/components/occufind/services/occufind-rest-factory.js': [
					'../CEP/src/main/webapp/cep/components/occufind/services/occufind-rest-factory.js'],
					'js/cep/components/occufind/services/occufind-military-details-modal-service.js': ['../CEP/src/main/webapp/cep/components/occufind/services/occufind-military-details-modal-service.js'],
					'js/cep/components/occufind/services/occufind-career-one-factory.js': ['../CEP/src/main/webapp/cep/components/occufind/services/occufind-career-one-factory.js'],
					'js/cep/components/dashboard/controllers/dashboard-controller.js': [
					'../CEP/src/main/webapp/cep/components/dashboard/controllers/dashboard-controller.js'],
					'js/cep/components/dashboard/service/fyi-manual-input-service.js': [
					'../CEP/src/main/webapp/cep/components/dashboard/service/fyi-manual-input-service.js'],
					'js/cep/components/dashboard/service/cep-manual-input-service.js': [
					'../CEP/src/main/webapp/cep/components/dashboard/service/cep-manual-input-service.js'],
					'js/cep/components/green-stem-bright/controllers/green-stem-bright-results-controller.js': ['../CEP/src/main/webapp/cep/components/green-stem-bright/controllers/green-stem-bright-results-controller.js'],
					'js/cep/components/green-stem-bright/services/green-stem-bright-rest-factory.js': ['../CEP/src/main/webapp/cep/components/green-stem-bright/services/green-stem-bright-rest-factory.js'],
					'js/cep/components/career-cluster/controllers/career-cluster-controller.js': ['../CEP/src/main/webapp/cep/components/career-cluster/controllers/career-cluster-controller.js'],
					'js/cep/components/career-cluster/controllers/career-cluster-occupation-results-controller.js': ['../CEP/src/main/webapp/cep/components/career-cluster/controllers/career-cluster-occupation-results-controller.js'],
					'js/cep/components/career-cluster/controllers/career-cluster-occupation-details-controller.js': ['../CEP/src/main/webapp/cep/components/career-cluster/controllers/career-cluster-occupation-details-controller.js'],
					'js/cep/components/career-cluster/services/career-cluster-rest-factory.js': ['../CEP/src/main/webapp/cep/components/career-cluster/services/career-cluster-rest-factory.js'],
					'js/cep/components/career-cluster/services/career-cluster-service.js': ['../CEP/src/main/webapp/cep/components/career-cluster/services/career-cluster-service.js'],
					'js/cep/components/career-cluster/controllers/career-cluster-public-controller.js': ['../CEP/src/main/webapp/cep/components/career-cluster/controllers/career-cluster-public-controller.js'],
					'js/cep/components/career-cluster/controllers/career-cluster-pathway-controller.js': ['../CEP/src/main/webapp/cep/components/career-cluster/controllers/career-cluster-pathway-controller.js'],
					'js/cep/components/portfolio/controllers/portfolio-directions-controller.js': ['../CEP/src/main/webapp/cep/components/portfolio/controllers/portfolio-directions-controller.js'],
					'js/cep/components/portfolio/controllers/portfolio-controller.js': [
					'../CEP/src/main/webapp/cep/components/portfolio/controllers/portfolio-controller.js'],
					'js/cep/components/portfolio/services/portfolio-rest-factory.js': [
					'../CEP/src/main/webapp/cep/components/portfolio/services/portfolio-rest-factory.js'],
					'js/cep/components/portfolio/services/portfolio-service.js': [
					'../CEP/src/main/webapp/cep/components/portfolio/services/portfolio-service.js'],
					'js/cep/components/portfolio/services/portfolio-tips-modal-service.js': ['../CEP/src/main/webapp/cep/components/portfolio/services/portfolio-tips-modal-service.js'],
					'js/cep/components/favorites/controllers/favorites-controller.js': [
					'../CEP/src/main/webapp/cep/components/favorites/controllers/favorites-controller.js'],
					'js/cep/components/favorites/services/favorites-rest-factory.js': [
					'../CEP/src/main/webapp/cep/components/favorites/services/favorites-rest-factory.js'],
					'js/cep/components/compare-occupation/controllers/compare-occupation-controller.js': ['../CEP/src/main/webapp/cep/components/compare-occupation/controllers/compare-occupation-controller.js'],
					'js/cep/components/media-center/controllers/media-center-controller.js': ['../CEP/src/main/webapp/cep/components/media-center/controllers/media-center-controller.js'],
					'js/cep/components/media-center/controllers/media-center-article-list-controller.js': ['../CEP/src/main/webapp/cep/components/media-center/controllers/media-center-article-list-controller.js'],
					'js/cep/components/media-center/controllers/media-center-article-controller.js': ['../CEP/src/main/webapp/cep/components/media-center/controllers/media-center-article-controller.js'],
					'js/cep/components/media-center/services/media-center-rest-factory.js': ['../CEP/src/main/webapp/cep/components/media-center/services/media-center-rest-factory.js'],
					'js/cep/components/media-center/services/media-center-service.js': [
					'../CEP/src/main/webapp/cep/components/media-center/services/media-center-service.js'],
					'js/cep/components/static-pages/controllers/static-pages-master-controller.js': ['../CEP/src/main/webapp/cep/components/static-pages/controllers/static-pages-master-controller.js'],
					'js/cep/components/static-pages/services/asvab-zoom-modal-service.js': ['../CEP/src/main/webapp/cep/components/static-pages/services/asvab-zoom-modal-service.js'],
					'js/cep/components/static-pages/services/asvab-sample-test-modal-service.js': ['../CEP/src/main/webapp/cep/components/static-pages/services/asvab-sample-test-modal-service.js'],
					'js/cep/components/static-pages/services/asvab-icat-sample-test-modal-service.js': ['../CEP/src/main/webapp/cep/components/static-pages/services/asvab-icat-sample-test-modal-service.js'],
					'js/cep/components/static-pages/services/strategy-taking-asvab-service.js': ['../CEP/src/main/webapp/cep/components/static-pages/services/strategy-taking-asvab-service.js'],
					'js/cep/components/static-pages/services/view-detail-service.js': ['../CEP/src/main/webapp/cep/components/static-pages/services/view-detail-service.js'],
					'js/cep/components/login/services/teacher-registration-modal-service.js': ['../CEP/src/main/webapp/cep/components/login/services/teacher-registration-modal-service.js'],
					'js/cep/components/static-pages/controllers/teacher-landing-controller.js': ['../CEP/src/main/webapp/cep/components/static-pages/controllers/teacher-landing-controller.js'],
					'js/cep/components/dashboard/controllers/classroom-modal-controller.js': ['../CEP/src/main/webapp/cep/components/dashboard/controllers/classroom-modal-controller.js'],
					'js/cep/components/dashboard/service/classroom-modal-service.js': [
					'../CEP/src/main/webapp/cep/components/dashboard/service/classroom-modal-service.js'],
					'js/cep/components/ess-training/controllers/ess-training-controller.js': [
					'../CEP/src/main/webapp/cep/components/ess-training/controllers/ess-training-controller.js'],
					'js/cep/components/ess-training/services/ess-training-rest-factory.js': ['../CEP/src/main/webapp/cep/components/ess-training/services/ess-training-rest-factory.js'],
					'js/cep/components/ess-training/services/ess-training-service.js': [
					'../CEP/src/main/webapp/cep/components/ess-training/services/ess-training-service.js'],
					'js/cep/components/careers-in-the-military/controller/citm-controller.js': [
					'../CEP/src/main/webapp/cep/components/careers-in-the-military/controller/citm-controller.js']
				}
			}
		},
		concat: {
			js: { //target
				src: ['js/**/*.js'],
				dest: '../CEP/src/main/webapp/cep/min/app.js'
			}
		},
		uglify: {
			js: { //target
				src: ['../CEP/src/main/webapp/cep/min/app.js'],
				dest: '../CEP/src/main/webapp/cep/min/cep-1-12-035.js'// UPDATE VERSION FOR EACH PRODUCTION RELEASE
			}
		},
		cssmin: {
			target: {
				files: [{
				expand: true,
				src: ['../CEP/src/main/webapp/css/app.css', '../CEP/src/main/webapp/css/style.css'],
				dest: '',
				ext: '-1-12-035.min.css'// UPDATE VERSION FOR EACH PRODCTION RELEASE
				}]
			}
		}
    });

	//load grunt tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	//register grunt default task
    grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify', 'cssmin']);
}